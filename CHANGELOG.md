# CHANGELOG

## 0.0.7 ()

* CoAuthor configuration

## 0.0.6 ( 2024-05-31 )

* Pipeline upload test

## 0.0.5 ( 2024-05-31 )

* Pipeline upload test

## 0.0.4 ( 2024-04-19 )

* Create a Simple Software Repository for Ansible
* Designing a Flexible Software Repository for Ansible
* Streamlining RHEL Registration and Subscription Automation
* Install Vagrant and Vagrant Plugins: updated; added optional step RHEL
  registration.

## 0.0.1 ( 2023-12-02 )

* Initial release.
