# Website

[![Pipeline Status](https://gitlab.com/c2platform/website/badges/master/pipeline.svg)](https://gitlab.com/c2platform/website/-/commits/master)
[![Latest Release](https://gitlab.com/c2platform/website/-/badges/release.svg)](https://gitlab.com/c2platform/website/-/releases)

Welcome to the repository for the C2 Platform website, available at
https://c2platform.org. This site is built using
[Hugo](https://gohugo.io/),
a powerful static site generator, and styled with the with the
[Docsy Theme](https://github.com/google/docsy),
a documentation theme for technical documentation sites.

- [Development Workflow](#development-workflow)
  - [CI/CD Pipeline](#cicd-pipeline)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Local Development](#local-development)
- [Advanced Setup](#advanced-setup)
  - [PlantUML Server](#plantuml-server)
  - [Hugo in Podman Container](#hugo-in-podman-container)
- [Verification](#verification)
- [PlantUML support](#plantuml-support)
- [Pre-commit Hook](#pre-commit-hook)
- [Troubleshooting](#troubleshooting)
- [Links](#links)


## Development Workflow

### CI/CD Pipeline

Our CI/CD pipeline facilitates automatic builds and deployments:

1. **Production Build:** Commits to the `master` branch trigger a build and
   deployment to the main website at https://c2platform.org.
2. **Development Build:** Commits to the `development` branch generate a preview
   version accessible at https://next.c2platform.org.

## Getting Started

### Prerequisites

Ensure you have the following installed:

* Hugo (Extended Version): Required for local development.
* Node.js: Necessary for managing JavaScript dependencies.
* Git: For version control.

### Local Development

To set up your environment and start contributing:

1. **Install Dependencies:** Make sure you have `snapd` and `podman` installed.

    ```bash
    sudo apt install snapd podman -y
    sudo snap install hugo
    ```
2. **Clone the Repository:**

   ```bash
   git clone git@gitlab.com:c2platform/website.git
   cd website
   ```
3. **Install Node Version Manager ( NVM ) and NodeJS:**

   ```bash
   curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
   nano ~/.bashrc
   source ~/.bashrc
   nvm install 18
   npm install
   ```
4. **Install JavaScript Dependencies:**

   ```bash
   npm install
   ```

6. Start the Hugo Server:
   ```bash
   hugo server
   ```
Now, you can access the site at http://localhost:1313 and see your changes in
real-time.

## Advanced Setup

### PlantUML Server

To use or view PlantUML diagrams locally, start a PlantUML server:

```bash
podman run -d -p 8080:8080 docker.io/plantuml/plantuml-server:tomcat
```

### Hugo in Podman Container

Run the website in a Docker container without installing dependencies:

```bash
sudo apt install podman -y
podman run --rm -it -v $(pwd):/src -p 1313:1313 docker.io/klakegg/hugo:ext-ubuntu server
```

## Verification

After starting the Hugo server or Docker container, visit
`http://localhost:1313` to view the site. Live reload is enabled, so changes you
make will automatically reflect in the browser.

## PlantUML support

This site supports PlantUML and Mermaid diagrams through Docsy. Note that for
reliability, PlantUML diagrams are pre-rendered to PNG files during the CI/CD
process. For more information about PlantUML and other diagrams, visit the
[Docsy documentation on diagrams](https://www.docsy.dev/docs/adding-content/diagrams-and-formulae/#uml-diagrams-with-plantuml).

## Pre-commit Hook

```bash
#!/bin/sh
rm -rf public || true
hugo
python3 scripts/htmldataproofer/ignore.py
python3 scripts/htmldataproofer/list.py
cd public
htmlproofer --allow-missing-href --ignore-empty-alt --ignore-missing-alt --no-enforce-https  2>&1 | tee ../htmlproofer.log
if grep -q "following failures were found" ../htmlproofer.log; then
    echo "Failures found. Exiting."
    exit 1
fi
```

## Troubleshooting

If you encounter issues, consult the
[Docsy Example](https://github.com/google/docsy-example)
project for common troubleshooting tips.

## Links

* [Pipelines](https://gitlab.com/c2platform/website/-/pipelines)
* [Docsy user guide](https://docsy.dev/docs)
* [Docsy](https://github.com/google/docsy)
* [example.docsy.dev](https://example.docsy.dev)
* [Hugo](https://gohugo.io)
* [Hugo theme module](https://gohugo.io/hugo-modules/use-modules/#use-a-module-for-a-theme)
