enableRobotsTXT = true

# Hugo allows theme composition (and inheritance). The precedence is from left to right.
theme = ["docsy"]

# Will give values to .Lastmod etc.
enableGitInfo = true

# Language settings
contentDir = "content/en"
defaultContentLanguage = "en"
defaultContentLanguageInSubdir = false
# Useful when translating.
enableMissingTranslationPlaceholders = true

# Comment out to enable taxonomies in Docsy
# disableKinds = ["taxonomy", "taxonomyTerm"]

# You can add your own taxonomies
[taxonomies]
category = "categories"
tag = "tags"

[params.taxonomy]
# set taxonomyCloud = [] to hide taxonomy clouds
taxonomyCloud = ["categories", "tags"]

# If used, must have same length as taxonomyCloud
taxonomyCloudTitle = ["Categories", "Tag Cloud"]

# set taxonomyPageHeader = [] to hide taxonomies on the page headers
taxonomyPageHeader = ["categories", "tags"]

# Highlighting config
pygmentsCodeFences = true
pygmentsUseClasses = false
# Use the new Chroma Go highlighter in Hugo.
pygmentsUseClassic = false
#pygmentsOptions = "linenos=table"
# See https://help.farbox.com/pygments.html
pygmentsStyle = "tango"

# Configure how URLs look like per section.
[permalinks]
blog = "/:section/:year/:month/:day/:slug/"

# TODO blackfriday parser C2-463

# Image processing configuration.
[imaging]
resampleFilter = "CatmullRom"
quality = 75
anchor = "Smart"

[services]
[services.googleAnalytics]
# Comment out the next line to disable GA tracking. Also disables the feature described in [params.ui.feedback].
# id = "UA-00000000-0"

# Language configuration
[languages]

[languages.nl]
title = "C2 Platform"
languageName = "Nederlands"
contentDir = "content/nl"

[languages.nl.params]
description = "Automation platform en community"
time_format_default = "2006.01.02"
time_format_blog = "2006.01.02"

[languages.en]
title = "C2 Platform"
languageName ="English"
weight = 1

[languages.en.params]
description = "Automation platform and community"

[markup]
  [markup.goldmark]
    [markup.goldmark.renderer]
      unsafe = true
  [markup.highlight]
    # See a complete list of available styles at https://xyproto.github.io/splash/docs/all.html
    style = "tango"
    # Uncomment if you want your chosen highlight style used for code blocks without a specified language
    # guessSyntax = "true"

# Everything below this are Site Params

# Comment out if you don't want the "print entire section" link enabled.
[outputs]
section = ['HTML', 'RSS']
home = ['HTML', 'Search', 'SearchIndex']

[params]
copyright = ""
privacy_policy = ""

# First one is picked as the Twitter card image if not set on page.
# images = ["images/project-illustration.png"]

# Menu title if your navbar has a versions selector to access old versions of your site.
# This menu appears only if you have at least one [params.versions] set.
version_menu = "Versions"

[[params.versions]]
  version = "current"
  url = "https://c2platform.org"

[[params.versions]]
  version = "next"
  url = "https://next.c2platform.org"

# [[params.versions]]  # TODO C2-513
#   version = "0.0.2"
#   url = "https://c2platform.org/0.0.1"

# Flag used in the "version-banner" partial to decide whether to display a
# banner on every page indicating that this is an archived version of the docs.
# Set this flag to "true" if you want to display the banner.
archived_version = true

# The version number for the version of the docs represented in this doc set.
# Used in the "version-banner" partial to display a version number for the
# current doc set.
# version = "current"

# A link to latest version of the docs. Used in the "version-banner" partial to
# point people to the main doc site.
url_latest_version = "https://c2platform.org"

# Repository configuration (URLs for in-page links to opening issues and suggesting changes)
# github_repo = "https://gitlab.com/c2platform/website"
# An optional link to a related project repo. For example, the sibling repository where your product code lives.
# github_project_repo = "https://gitlab.com/c2platform"

# Specify a value here if your content directory is not in your repo's root directory
# github_subdir = ""

# Uncomment this if your GitHub repo does not have "main" as the default branch,
# or specify a new value if you want to reference another branch in your GitHub links

# Google Custom Search Engine ID. Remove or comment out to disable search.
# gcs_engine_id = "d72aa9b2712488cc3"

# Enable Algolia DocSearch
algolia_docsearch = false

# Enable Lunr.js offline search
offlineSearch = false

# Enable syntax highlighting and copy buttons on code blocks with Prism
prism_syntax_highlighting = false


[params.plantuml]
enable = true
svg_image_url = "http://localhost:8080/svg/"

# User interface configuration
[params.ui]
#  Set to true to disable breadcrumb navigation.
breadcrumb_disable = false
# Set to true to disable the About link in the site footer
footer_about_disable = true
# Set to false if you don't want to display a logo (/assets/icons/logo.svg) in the top navbar
navbar_logo = false
# Set to true if you don't want the top navbar to be translucent when over a `block/cover`, like on the homepage.
navbar_translucent_over_cover_disable = false
# Enable to show the side bar menu in its compact state.
sidebar_menu_compact = true
# Set to true to hide the sidebar search box (the top nav search box will still be displayed if search is enabled)
sidebar_search_disable = false
# Set to true to make the sidebar foldable
sidebar_menu_foldable = true

# Adds a H2 section titled "Feedback" to the bottom of each doc. The responses are sent to Google Analytics as events.
# This feature depends on [services.googleAnalytics] and will be disabled if "services.googleAnalytics.id" is not set.
# If you want this feature, but occasionally need to remove the "Feedback" section from a single page,
# add "hide_feedback: true" to the page's front matter.
[params.ui.feedback]
enable=true
url="https://gitlab.com/c2platform/website/-/issues/service_desk" # custom / not a Docsy parameter
# The responses that the user sees after clicking "yes" (the page was helpful) or "no" (the page was not helpful).
# yes = 'Glad to hear it! Please <a href="https://github.com/USERNAME/REPOSITORY/issues/new">tell us how we can improve</a>.'
# no = 'Sorry to hear that. Please <a href="https://github.com/USERNAME/REPOSITORY/issues/new">tell us how we can improve</a>.'
# Adds a reading time to the top of each doc.
# If you want this feature, but occasionally need to remove the Reading time from a single page,
# add "hide_readingtime: true" to the page's front matter
[params.ui.readingtime]
enable=false

[params.links]
# End user relevant links. These will show up on left side of footer and in the community page if you have one.
#[[params.links.user]]
#  name = "User mailing list"
#  url = "https://example.org/mail"
#  icon = "fa fa-envelope"
#  desc = "Discussion and help from your fellow users"
#[[params.links.user]]
#  name ="Twitter"
#  url = "https://example.org/twitter"
#  icon = "fab fa-twitter"
#  desc = "Follow us on Twitter to get the latest news!"
#[[params.links.user]]
#  name = "Stack Overflow"
#  url = "https://example.org/stack"
#  icon = "fab fa-stack-overflow"
#  desc = "Practical questions and curated answers"
# Developer relevant links. These will show up on right side of footer and in the community page if you have one.
[[params.links.developer]]
  name = "GitLab"
  url = "https://gitlab.com/c2platform"
  icon = "fab fa-gitlab"
  desc = "Development takes place here!"
#[[params.links.developer]]
#  name = "Slack"
#  url = "https://example.org/slack"
#  icon = "fab fa-slack"
#  desc = "Chat with other project developers"
#[[params.links.developer]]
#  name = "Developer mailing list"
#  url = "https://example.org/mail"
#  icon = "fa fa-envelope"
#  desc = "Discuss development issues around the project"


[params.search]
  modal_toggle_selector = '.td-search'

# hugo module configuration

[module]
  # uncomment line below for temporary local development of module
  # replacements = "github.com/google/docsy -> ../../docsy"
  [module.hugoVersion]
    extended = true
    min = "0.75.0"
  [[module.imports]]
    disable = false
    path = 'github.com/hugomods/search'
  [[module.imports]]
    path = "github.com/google/docsy"
    disable = false
  [[module.imports]]
    path = "github.com/google/docsy/dependencies"
    disable = false
  [[module.imports]]
    path = 'github.com/hugomods/images'
    disable = false
