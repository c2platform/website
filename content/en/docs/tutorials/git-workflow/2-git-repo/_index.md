---
title: "Setting Up the Git Repositories"
linkTitle: "Setting Up the Git Repositories"
translate: false
weight: 1
description: >
   Different types of Ansible projects / Git repositories, including Ansible configuration projects, Ansible collections.
---

{{< under_construction >}}
