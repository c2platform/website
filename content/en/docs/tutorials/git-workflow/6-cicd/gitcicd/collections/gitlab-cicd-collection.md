---
title: "Example CI/CD Pipeline for an Ansible Collection"
linkTitle: "Example Ansible Collection Pipeline"
translate: false
categories: ["Example"]
tags: [ci/cd, gitlab, ansible, collection, .gitlab-ci.yml]
weight: 6
description: >
   Example CI/CD Pipeline for an Ansible Collection
---

Projects: [`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

---

A GitLab pipeline for an
[Ansible collection project]({{< relref
path="/docs/concepts/ansible/projects/collections" >}}) is
realized by creating a file `.gitlab-ci.yml` in the root of the Git repository.

For the most current version of this script, you can download and utilize the
one included in the
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})
Ansible collection.

```yaml
---
before_script:
  - apt-get update -qy
  - apt-get install -y python3-dev python3-pip jq
  - apt install python3.11-venv -y
  - python3 -m venv /tmp/c2
  - source /tmp/c2/bin/activate
  - pip3 install yq yamllint==1.32.0 ansible-lint==6.17.1
  - pip3 install ansible-core==2.15.0
  - ansible-galaxy collection install -r requirements.yml
  - python3 --version
  - pip3 --version
  - ansible --version
  - ansible-lint --version
  - yamllint --version

workflow:  # run the pipeline only on MRs and default branch
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

stages:
  - build
  - linters
  - galaxy
  - release

prepare:
  stage: build
  script:
    - C2_VERSION=$(yq '.version' galaxy.yml | sed 's/"//g')
    - C2_NAME=$(yq '.name' galaxy.yml | sed 's/"//g')
    - C2_NAMESPACE=$(yq '.namespace' galaxy.yml | sed 's/"//g')
    - echo "C2_VERSION=$C2_VERSION" >> variables.env
    - echo "C2_NAME=$C2_NAME" >> variables.env
    - echo "C2_NAMESPACE=$C2_NAMESPACE" >> variables.env
  artifacts:
    reports:
      dotenv: variables.env

build:
  stage: build
  needs: [prepare]
  script:
    - cat README-GALAXY.md > README.md  # readme property in galaxy.yml is ignored by galaxy website
    - ansible-galaxy collection build . --force
  artifacts:
    paths:
      - $C2_NAMESPACE-$C2_NAME-$C2_VERSION.tar.gz

yamllint:
  stage: linters
  script:
    - yamllint -c .yamllint .

ansible-lint:
  stage: linters
  needs: [prepare, build]
  script:
    - ansible-galaxy collection install $C2_NAMESPACE-$C2_NAME-$C2_VERSION.tar.gz
    - ansible-lint -c .ansible-lint

publish:
  stage: galaxy
  needs: [prepare, build, yamllint, ansible-lint]
  script:
    - cat README-GALAXY.md > README.md  # readme property in galaxy.yml is ignored by galaxy website
    - ansible-galaxy collection build . --force
    - ansible-galaxy collection publish $C2_NAMESPACE-$C2_NAME-$C2_VERSION.tar.gz --api-key $GALAXY_API_KEY
  when: manual

gitlab-release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs: [publish, prepare]
  before_script: []
  script:
    - echo "Create release for $C2_NAMESPACE.$C2_NAME $C2_VERSION"
  release:
    name: $C2_NAMESPACE.$C2_NAME $C2_VERSION
    description: './CHANGELOG.md'
    tag_name: $C2_VERSION
    ref: $CI_COMMIT_SHA
    assets:
      links:
        - name: $C2_NAMESPACE.$C2_NAME
          url: https://galaxy.ansible.com/$C2_NAMESPACE/$C2_NAME
```