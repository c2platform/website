---
title: "CI/CD Pipelines for Ansible Collections"
linkTitle: "Ansible Collections"
translate: false
tags: [ci/cd, gitlab, ansible, collection, .gitlab-ci.yml]
weight: 6
description: >
   Streamline the development and release process of Ansible collections with a
   CI/CD pipeline to Ansible Galaxy.
---

To streamline the development and release process of Ansible collections, you
can create a CI/CD pipeline that includes code validation and manual steps for
releasing a new version of an Ansible collection to [Ansible Galaxy]({{< relref
path="/docs/concepts/ansible/galaxy.md" >}}).

## Pipeline description

To streamline the development and release process of Ansible collections, you can integrate Git with CI/CD pipelines. The example [`.gitlab-ci.yml`]({{< relref path="./gitlab-cicd-collection.md" >}}) file attached to this page provides a template pipeline that includes code validation and manual steps for releasing a new version of an Ansible collection to Ansible Galaxy.

The file contains the configuration for the pipeline stages and jobs:

### Build

The `prepare` job in the build stage extracts the collection version, name, and namespace from the `galaxy.yml` file and saves them as environment variables for later use. It creates a `variables.env` file to store these variables as artifacts.

The `build` job packages the Ansible collection and generates the `README.md` file from `README-GALAXY.md`. The resulting collection is stored as an artifact.

### Linters

The `yamllint` job runs the `yamllint` tool to validate the YAML code.

The `ansible-lint` job installs the collection built in the previous stage and runs the `ansible-lint` tool to validate the Ansible code.

### Galaxy

The `publish` job publishes the Ansible collection to Ansible Galaxy. It packages the collection, generates the `README.md` file from `README-GALAXY.md`, and publishes it to Ansible Galaxy using the provided API key.

### Release

The `gitlab-release` job creates a release in GitLab for the Ansible collection. It uses the information from `galaxy.yml`, including the version and changelog, to create the release. The release is associated with the commit and can be accessed through the provided release name.

## Usage

To use this pipeline as a template for your Ansible collections, follow these steps:

1. Set up your GitLab repository with the necessary configuration files: `.gitlab-ci.yml` and `galaxy.yml`.
2. Configure the stages and jobs in the `.gitlab-ci.yml` file based on your specific requirements. Copy the updated pipeline configuration provided below.
3. Update the `galaxy.yml` file with the relevant information for your Ansible collection, such as the namespace, name, version, description, and links to documentation and issues.
4. Ensure you have the required dependencies installed, including Python, Pip, Ansible, yq, yamllint, and ansible-lint. Adjust the package installation commands in the `before_script` section of the `.gitlab-ci.yml` file if needed.
5. Set up the necessary environment variables and secrets in your GitLab CI/CD settings, including the `GALAXY_API_KEY` for publishing to Ansible Galaxy.
6. Commit and push your changes to trigger the pipeline. The pipeline will run on merge requests and the default branch.

## Benefits

By integrating Git with CI/CD pipelines for Ansible collections, you can achieve the following benefits:

- Ensure code quality by validating the YAML and Ansible code.
- Automate the process of building and publishing collections to Ansible Galaxy.
- Track and create releases in GitLab, associating them with specific commits and providing changelog information.

## Examples

* [example]({{< relref path="./gitlab-cicd-collection.md" >}}) attached to  attached to this page.
* See the `.gitlab-ci.yml` of the Ansible collection [`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}). This project also includes an Azure CI/CD pipeline see `azure-pipelines.yml`.

