---
categories: ["Guideline"]
tags: [group_vars, merge, hash_behaviour, ansible]
title: "Managing Dictionary Merging in C2 Platform Ansible Projects"
linkTitle: "Dictionary Merging"
translate: false
weight: 1
description: >
    Best practices for utilizing dictionary merging in C2 Ansible inventory projects.
---

Projects: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}),
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform/rws/ansible-collection-wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform/rws/ansbile-collection-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---


In C2 Platform
[Ansible inventory projects]({{< relref path="/docs/concepts/ansible/projects/inventory" >}})
, utilizing the `hash_behaviour = merge` setting in `ansible.cfg` facilitates
more flexible data structuring. See for examples the `ansible.cfg` files in
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})
and
[`c2platform/rws/ansbile-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}).
This approach is particularly beneficial when managing configurations for
Windows systems with roles like `c2platform.wincore.win` and
`c2platform.win.linux`. To harness this flexibility fully and ensure seamless
dictionary merging, consider the following enhanced guidelines and examples.

## Guidelines for Effective Dictionary Merging

1. **Examine and Document Your Inventory Structure**: Clearly document where and
   how `hash_behaviour = merge` is applied within your Ansible projects. It's
   crucial to maintain an updated inventory structure documentation, noting any
   instances of merged dictionaries.
1. **Use Merging Judiciously**: Employ dictionary merging primarily for host or
   group variables that benefit from this method. Overuse can lead to complexity
   and obscure data relationships.
1. **Thoughtful and Unique Keys**: When using dictionaries ensure the keys are
   well chosen and unique within your inventory project.
1. **Rigorous Testing**: Thoroughly test playbooks to validate that merged
   dictionaries behave as expected. This step is vital to catch any merging
   issues or unintended consequences early in the development cycle.
1. **Understand Variable Precedence**: Familiarize yourself with the rules of
   variable precedence in Ansible. Knowledge of how variables from different
   levels (e.g., group_vars, host_vars) merge is critical for predicting the
   final configuration state.
1. **Conflict Resolution Strategy**: Develop and document strategies for
   handling key conflicts in merged dictionaries. This plan ensures consistent
   handling of conflicts across projects.
1. **Securing Sensitive Data**: When merging dictionaries involves sensitive
   information, use encryption methods like Ansible Vault. This practice ensures
   that sensitive data is securely managed while allowing non-sensitive
   information to be merged as needed.

## Practical Examples

### Merging `win_chocolatey` Configurations

When utilizing `win_chocolatey` variable of the `c2platform.wincore.win` Ansible
role for different server groups within your Ansible inventory, utilizing
"dictionaries" enables a more dynamic and flexible approach compared to simple
"lists". This method ensures servers in multiple groups receive all necessary
packages without configuration conflicts.

#### Example Configuration in `group_vars/windows/main.yml`:

```yaml
win_chocolatey:
  windows:
    - name: nuget.commandline
```

#### Example Configuration in `group_vars/age_pro/main.yml`:

```yaml
win_chocolatey:
  age_pro:
    - name: firefox
```

When a server belongs to both the `windows` and `age_pro` groups, the merged
configuration for `win_chocolatey` will include both the NuGet command line and
Firefox, ensuring comprehensive package management across server roles.

#### Merged Configuration Result

```yaml
win_chocolatey:
  windows:
    - name: nuget.commandline
  age_pro:
    - name: firefox
```

#### Flexibility in Key Naming

You can use any descriptive key names for categorizing configurations within
dictionaries, enhancing readability and organizational clarity. We can change
the example to use keys `whatever` and `some_other_key`.

```yaml
win_chocolatey:
  whatever:
    - name: nuget.commandline
```

```yaml
win_chocolatey:
  some_other_key:
    - name: firefox
```

#### Limitations in Key Naming

You can choose any key name you want but should be aware that only
"dictionaries" can be merged. Simple "lists" cannot be merged.

1. In  `group_vars/windows/main.yml`:
  ```yaml
  win_chocolatey:
    tools:
      - name: nuget.commandline
  ```
1. In  `group_vars/age_pro/main.yml`:
  ```yaml
  win_chocolatey:
    tools:
      - name: firefox
  ```

{{< alert type="warning" >}}
The merged result will have `firefox` or `nuget.commandline` and not both! You
cannot have multiple occurrences in your inventory project for a variable like
`win_chocolatey` with the same key in this case `tools`.
{{< /alert >}}

## Additional Information

For additional insights and guidance:

* The how-to
  [Review Dictionary Merging in Ansible Projects]({{< relref path="/docs/howto/rws/merge" >}})
  gives some practical information on how you can review merge behavior in an inventory project.