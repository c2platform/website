---
categories: [Example]
tags: [tags, skip-tags, ansible]
title: Using Tags with the Linux Play
linkTitle: Example Linux Play
translate: false
weight: 1
toc_hide: true
hide_summary: true
description: >
  Learn how to use "always", "system", and "install" tags effectively with the
  Ansible Linux role for optimized playbook execution.
---

Projects:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

---

The Linux role {{< ansible-role-link role_name="c2platform.core.linux" >}},
similar to the Windows role {{< ansible-role-link role_name="c2platform.wincore.win" subgroup="rws" >}},
is a versatile, flexible, and generic role designed for targeting Linux and
Windows hosts, respectively. This document outlines how the tagging scheme, as
described in the guideline
{{< rellink path="/docs/guidelines/coding/tags" >}},
can be used and implemented for the Linux role.

Consider the following Ansible play:

```yaml
---
- name: Linux role
  hosts: localhost
  become: true

  roles:
    - { role: c2platform.core.secrets }  # → always
    - { role: c2platform.core.linux }  # → install, system, linux

  vars:
    linux_resources:
      - name: system_install_task.txt
        dest: /tmp/system_install_task.txt
        type: copy
        content: |
          whatever
```

Note this play is part of the
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})
project see `plays/examples/linux.yml`.

## List Tasks

To list possible/available tags that can be used with the Ansible play, run the
following command:

```bash
ansible-playbook $PLAY -i $INVENTORY $LIMIT --list-tasks
```

You should see output similar to the below. Notice that the tasks that are part
of the {{< ansible-role-link role_name="c2platform.core.secrets" >}}
role are tagged with `always`. This role decrypts the
{{< rellink path="/docs/howto/dev-environment/vault" >}}
to make secrets available. This role uses the `always` tag to ensure that
secrets are consistently made available to the tasks.

```txt
playbook: plays/examples/linux.yml

  play #1 (localhost): Linux role	TAGS: []
    tasks:
      c2platform.core.secrets : Set fact for common_secrets_dirs	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Deprecated variable	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Include secrets	TAGS: [always, secrets, vault]
      c2platform.core.vagrant_hosts : Set fact vagrant_hosts_content	TAGS: [config, development, system, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage /etc/hosts of Vagrant Host	TAGS: [config, development, system, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Linux guest	TAGS: [config, development, system, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [config, development, system, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [config, development, system, vagrant_hosts]
      include_tasks	TAGS: []
      include_tasks	TAGS: []
      c2platform.core.apt_repo : Add APT key	TAGS: [debian, install, linux, system]
      c2platform.core.apt_repo : Add APT repository	TAGS: [debian, install, linux, system]
      c2platform.core.yum : Add YUM repository	TAGS: [install, linux, redhat, system, yum]
      c2platform.core.bootstrap : Pip conf	TAGS: [bootstrap, install, linux, system]
      c2platform.core.bootstrap : Pip trust store	TAGS: [bootstrap, install, linux, system]
      c2platform.core.bootstrap : Pip ca bundle	TAGS: [bootstrap, install, linux, system]
      c2platform.core.bootstrap : Include package tasks	TAGS: [bootstrap, install, linux, system]
      c2platform.core.bootstrap : Deprecation message	TAGS: [bootstrap, install, linux, system]
      c2platform.core.os_trusts : CA distribute ( RedHat)	TAGS: [config, linux, os_trusts, system]
      c2platform.core.os_trusts : Execute update-ca-trust	TAGS: [config, linux, os_trusts, system]
      c2platform.core.os_trusts : CA distribute ( Debian )	TAGS: [config, linux, os_trusts, system]
      c2platform.core.os_trusts : Execute update-ca-certificates ( Debian )	TAGS: [config, linux, os_trusts, system]
      c2platform.core.secrets : Set fact for common_secrets_dirs	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Deprecated variable	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Include secrets	TAGS: [always, secrets, vault]
      c2platform.core.lcm : Ansible roles names without prefix	TAGS: [ansible]
      c2platform.core.lcm : lcm_roles_node	TAGS: [ansible]
      c2platform.core.lcm : Determine LCM info facts	TAGS: [ansible]
      c2platform.core.lcm : LCM info facts	TAGS: [ansible]
      c2platform.core.lvm : Get disks	TAGS: [install, linux, lvm, system]
      c2platform.core.lvm : Get usable devices	TAGS: [install, linux, lvm, system]
      c2platform.core.lvm : Get nodes lvm roles	TAGS: [install, linux, lvm, system]
      include_tasks	TAGS: [install, linux, lvm, system]
      c2platform.core.lvm : Volume group	TAGS: [install, linux, lvm, system]
      include_tasks	TAGS: [install, linux, lvm, system]
      c2platform.core.hosts : Include OS-specific variables.	TAGS: [hosts, install, linux, network, system]
      include_tasks	TAGS: [hosts, install, linux, network, system]
      include_tasks	TAGS: [hosts, install, linux, network, system]
      c2platform.core.alias : Set fact node roles	TAGS: [alias, config, linux, system]
      c2platform.core.alias : Alias file	TAGS: [alias, config, linux, system]
      c2platform.core.linux : Include linux_resources_types	TAGS: [install, linux, linux_acl, linux_alternatives, linux_apk, linux_apt, linux_apt_key, linux_apt_repo, linux_apt_repository, linux_apt_rpm, linux_archive, linux_at, linux_authorized_key, linux_blockinfile, linux_bundler, linux_capabilities, linux_command, linux_copy, linux_cpanm, linux_cron, linux_cronvar, linux_crypttab, linux_dconf, linux_deb822_repository, linux_debconf, linux_dnf, linux_dnf5, linux_dnf_versionlock, linux_dpkg_divert, linux_dpkg_selections, linux_easy_install, linux_fail, linux_fetch, linux_file, linux_filesize, linux_filesystem, linux_firewalld, linux_flatpak, linux_flatpak_remote, linux_gconftool2, linux_gconftool2_info, linux_gem, linux_get_url, linux_git, linux_git_config, linux_haproxy, linux_hostname, linux_htpasswd, linux_include_role, linux_iptables, linux_java_cert, linux_java_keystore, linux_known_hosts, linux_lineinfile, linux_locale_gen, linux_lvg, linux_lvg_rename, linux_lvol, linux_lxc_container, linux_lxd_container, linux_lxd_profile, linux_lxd_project, linux_mail, linux_mount, linux_nmcli, linux_npm, linux_package, linux_pacman, linux_pacman_key, linux_pam_limits, linux_pamd, linux_parted, linux_patch, linux_pause, linux_pip, linux_reboot, linux_replace, linux_rhsm_release, linux_rhsm_repository, linux_rpm_key, linux_runit, linux_script, linux_seboolean, linux_sefcontext, linux_selinux, linux_selinux_permissive, linux_selogin, linux_service, linux_shell, linux_shutdown, linux_slurp, linux_snap, linux_snap_alias, linux_ssh_config, linux_sudoers, linux_synchronize, linux_sysctl, linux_syslogger, linux_systemd, linux_systemd_service, linux_sysvinit, linux_tempfile, linux_template, linux_timezone, linux_ufw, linux_unarchive, linux_uri, linux_user, linux_wait_for, linux_wait_for_connection, linux_xml, linux_yum, linux_yum_repository, linux_yum_versionlock, linux_zfs, linux_zfs_delegate_admin, linux_zfs_facts, system]
      c2platform.core.linux : Include linux_resources	TAGS: [install, linux, linux_acl, linux_alternatives, linux_apk, linux_apt, linux_apt_key, linux_apt_repo, linux_apt_repository, linux_apt_rpm, linux_archive, linux_at, linux_authorized_key, linux_blockinfile, linux_bundler, linux_capabilities, linux_command, linux_copy, linux_cpanm, linux_cron, linux_cronvar, linux_crypttab, linux_dconf, linux_deb822_repository, linux_debconf, linux_dnf, linux_dnf5, linux_dnf_versionlock, linux_dpkg_divert, linux_dpkg_selections, linux_easy_install, linux_fail, linux_fetch, linux_file, linux_filesize, linux_filesystem, linux_firewalld, linux_flatpak, linux_flatpak_remote, linux_gconftool2, linux_gconftool2_info, linux_gem, linux_get_url, linux_git, linux_git_config, linux_haproxy, linux_hostname, linux_htpasswd, linux_include_role, linux_iptables, linux_java_cert, linux_java_keystore, linux_known_hosts, linux_lineinfile, linux_locale_gen, linux_lvg, linux_lvg_rename, linux_lvol, linux_lxc_container, linux_lxd_container, linux_lxd_profile, linux_lxd_project, linux_mail, linux_mount, linux_nmcli, linux_npm, linux_package, linux_pacman, linux_pacman_key, linux_pam_limits, linux_pamd, linux_parted, linux_patch, linux_pause, linux_pip, linux_reboot, linux_replace, linux_rhsm_release, linux_rhsm_repository, linux_rpm_key, linux_runit, linux_script, linux_seboolean, linux_sefcontext, linux_selinux, linux_selinux_permissive, linux_selogin, linux_service, linux_shell, linux_shutdown, linux_slurp, linux_snap, linux_snap_alias, linux_ssh_config, linux_sudoers, linux_synchronize, linux_sysctl, linux_syslogger, linux_systemd, linux_systemd_service, linux_sysvinit, linux_tempfile, linux_template, linux_timezone, linux_ufw, linux_unarchive, linux_uri, linux_user, linux_wait_for, linux_wait_for_connection, linux_xml, linux_yum, linux_yum_repository, linux_yum_versionlock, linux_zfs, linux_zfs_delegate_admin, linux_zfs_facts, system]
```

## Provision

Execute the following command to run the playbook:

```bash
ansible-playbook plays/examples/linux.yml -i hosts.ini
```

As expected, both the secrets and Linux roles should be provisioned. The Linux
role will create the file `/tmp/system_install_task.txt`.

<details>
  <summary><kbd>Show me</kbd></summary>

```bash
θ97° [:ansible-gis]└2 master(+15/-11)+* ± ansible-playbook $PLAY -i $INVENTORY $LIMIT | tee provision.log

PLAY [Linux role] **************************************************************

TASK [Gathering Facts] *********************************************************
ok: [localhost]

TASK [c2platform.core.secrets : Stat secret dir] *******************************
ok: [localhost] => (item=/home/onknows/git/gitlab/c2/ansible-gis/secret_vars/development)
ok: [localhost] => (item=/runner/project/secret_vars/development)

TASK [c2platform.core.secrets : Include secrets] *******************************
ok: [localhost] => (item=/home/onknows/git/gitlab/c2/ansible-gis/secret_vars/development)

TASK [c2platform.core.linux : Include linux_resources] *************************
included: /home/onknows/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/linux/tasks/copy.yml for localhost => (item= system_install_task.txt)

TASK [c2platform.core.linux : Copy files to remote locations] ******************
ok: [localhost] => (item=/tmp/system_install_task.txt)

PLAY RECAP *********************************************************************
localhost                  : ok=5    changed=0    unreachable=0    failed=0    skipped=49   rescued=0    ignored=0
```

</details>

## Verify

In this very simple example, you can verify that you run the play using `--tags
linux_copy`, `--tags system`, `--tags install` and `--tags linux`. In each case,
note that the **Include secrets** task of the Ansible Secrets / Vault role
`c2platform.core.secrets` will run.

## Review

1. If you look at the `tasks/main.yml` file in the
   {{< ansible-role-link role_name="c2platform.core.secrets" >}}
   role, note that the tasks are tagged with the special Ansible tag `always`.

## Additional Information

* {{< rellink path="/docs/howto/rws/arcgis/fme_flow/tags" desc=true >}}
* {{< external-link url="https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_tags.html" text="Tags — Ansible Community Documentation" htmlproofer_ignore="false" >}}