---
categories: ["Guideline"]
tags: [tags]
title: "Enhancing Ansible Playbooks with Tags"
linkTitle: "Tags in Ansible"
translate: false
weight: 4
description: >
  Implement a structured tagging system in Ansible playbooks to increase task flexibility, enhance maintainability, and improve reusability, thus optimizing both development and operational efficiency.
---

## Problem

Managing large Ansible playbooks can be cumbersome and time-consuming,
especially when executing the entire playbook for development or regular
operations. This inefficiency is particularly noticeable during development,
where rapid testing of specific playbook parts is necessary, or in production,
where operations may demand varying execution frequencies.

## Solution

Enhance the flexibility and manageability of your playbooks by implementing a
structured and meaningful tagging scheme for Ansible roles and tasks. By
adopting a well-defined tagging approach, you can efficiently handle common
setup and configuration stages pertinent to deployment processes.

### Tagging Scheme

Consider adopting the following tagging strategy for task execution timing and
operational levels:

| Tag Category              | Tag           | Description                                                                                                                                                          |
|---------------------------|---------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `always` or `never`       | `always`      | For tasks that must consistently execute, crucial for security, compliance, or serving as prerequisites (e.g., decrypting vault items or fundamental initial steps). |
|                           | `never`       | For tasks that should not run unless explicitly specified with `--tags never`.                                                                                       |
| `install` or `config`     | `install`     | Tasks performed infrequently, primarily during initial system setup or application installation.                                                                     |
|                           | `config`      | Tasks that frequently modify resources for updates or optimizations.                                                                                                 |
| `system` or `application` | `system`      | Tasks targeting the operating system or system-level configurations.                                                                                                 |
|                           | `application` | Tasks affecting application-level configurations, directly impacting applications on the system.                                                                     |
| Configuration Approach    | `config_api`  | Tasks configured via the product's API.                                                                                                                              |
| Role Identification       | Role Name     | Tasks, except `always` or `never` tasks[^1], in a role are tagged with the role name, such as `linux` for tasks in the `c2platform.core.linux` role.                 |

Additional specific module tags or customized tags can be included as per unique
requirements.

The advantages of a clear tagging scheme include:

- **Flexibility**: Seamlessly run specific segments of deployment or maintenance
  processes.
- **Maintainability**: Simplify the understanding and upkeep of playbooks with
  clear tags.
- **Reusability**: Adapt roles for various operational requirements without
  modifying the role itself.

{{< alert type="warning" title="Important Note on Lifecycle Management (LCM) Operations" >}}

Tasks related to Lifecycle Management (LCM) operations such as upgrades,
backups, restores, downgrades, and rollbacks are critical and potentially
disruptive. To avoid accidental inclusion in broader task runs when managing
these tasks solely with tags:

- Employ specific configuration variables (defaulted to safe values, e.g.,
  false) to activate these tasks.
- Document safe execution procedures and verify user intent.

{{< /alert >}}

## Example and Implementation

1. In the `tasks/main.yml` file of the
   {{< ansible-role-link role_name="c2platform.core.secrets" >}}
   role, note that tasks are tagged with the special Ansible tag `always`. This
   ensures that essential secrets, such as passwords in
   {{< rellink path="/docs/howto/dev-environment/vault" >}},
   are always available. Another role using the `always` tag is the Ansible
   Certificates / CA role
   {{< ansible-role-link role_name="c2platform.core.cacerts2" >}},
   where it ensures the execution of "facts" tasks (e.g., in `tasks/certs.yml`).

2. Review the files `tasks/main.yml` and `tasks/main_base.yml` of the Ansible
   Windows role
   {{< ansible-role-link role_name="c2platform.wincore.win" subgroup="rws" >}}.
   In `main.yml`, all tasks are tagged with `install`, `system`, and `win`.

   ```yaml
   ---
   - name: Include Windows system installation tasks (install, system, win)
     ansible.builtin.import_tasks: main_base.yml
     tags: [install, system, win]
   ```

3. In the Ansible FME Flow role
   {{< ansible-role-link role_name="c2platform.gis.fme_flow" subgroup="rws" >}}
   in the file `tasks/main.yml`, the following code is found:

   ```yaml
   - name: Include role win
     ansible.builtin.import_role:
       name: c2platform.wincore.win
       tasks_from: main_base
     vars:
       win_resources: "{{ fme_flow_win_resources }}"
       win_resources_types: "{{ fme_flow_win_resources_types }}"
       win_resource_groups_disabled: "{{ fme_flow_win_resource_groups_disabled }}"
     tags: [config, application, fme_flow]
   ```

   The code incorporates the Windows role into the FME Flow role and tags all
   tasks with `config`, `application`, and `fme_flow`. Note the use of
   `import_role`. Using `include_role` instead of `import_role` results in
   different behavior regarding task tagging.

   Consider this simple FME play:

   ```yaml
   - name: FME
     hosts: fme

     roles:
       - { role: c2platform.wincore.win }
       - { role: c2platform.gis.java }
       - { role: c2platform.gis.fme_flow }
   ```

   This play includes the Windows role directly as `c2platform.wincore.win` and
   indirectly via `c2platform.gis.fme_flow` with different tags.

For implementation examples and further guidance, please refer to the Linux
tagging example
{{< rellink path="/docs/guidelines/coding/tags/linux" desc=false >}}
or for a more comprehensive example see
{{< rellink path="/docs/howto/rws/arcgis/fme_flow/tags" desc=false >}}.
For additional reference and information about Ansible tags see
{{< external-link url="https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_tags.html" text="Tags — Ansible Community Documentation" htmlproofer_ignore="false" >}}

## Footnotes

[^1]: Avoid tagging tasks with the `always` tag with the Ansible role name, as
    this would greatly diminish the value of the role name tag. In such cases,
    using the role name with `--skip-tags` would inadvertently skip the `always`
    tags.
