---
categories: ["Guideline"]
tags: [group_vars]
title: "Variable prefix"
linkTitle: "Variable prefix"
translate: false
weight: 3
description: >
  Prefix variable names with role or project prefix.
---

---
> Prefix variables in Ansible roles / collections with the role name. For example `harbor_hostname`. Prefix other / project variables with a project prefix for example `c2_cacerts2_ca_dir`.

---

## Problem

Without a project of role prefix it is not always clear where variables are used. Without a prefix there is also a chance of conflicting duplicating variables.

## Context

In a typical project typically all kinds of variables are created / used. Some variables are used in Ansible roles but some do not. This can be confusing and lead to mistakes / problems.

## Solution

1. Prefix all variables used in an Ansible role with the role name for example `harbor_`.
2. Prefix all other / project variables - that only exists in `group_vars` or `host_vars` folder - with a project prefix for example `c2_`.

## Examples and implementation

1. For example `harbor_hostname` for the hostname of the Harbor instance. The
  prefix is `harbor_` because this is a variable that is part of the
  [`c2platform.mgmt.harbor`]({{< ref "/docs/gitlab/c2platform/ansible-collection-mgmt" >}})
  Ansible role.
2. In C2 Inventory project [`c2platform/ansible`]({{< relref
   "/docs/gitlab/c2platform/ansible" >}}) `c2_` is used as a prefix see for
   example `c2_cacerts2_ca_dir` in `group_vars/all/smallca.yml`.
3. In the RWS Inventory project [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) `gs_` is used as prefix. See [Configuring a Web Proxy Server Example for MS Windows Hosts]({{< relref path="/docs/howto/rws/windows/proxy" >}}) for an example on how `gs_proxy_regedit` is utilized to configure a web proxy.
