---
categories: ["Guideline"]
tags: [group_vars]
title: "Lazy naming convention"
linkTitle: "Lazy naming convention"
translate: false
weight: 10
description: >
  Naming with the least amount of keystrokes.
---

---
> Use only lowercase with `-` to separate words. Avoid using uppercase characters, underscore `_`, or camelcase as they require extra keystrokes.

---

## Problem

Naming conventions are important for maintaining code readability and consistency. However, many naming conventions can be verbose and require extra keystrokes.

## Context

In software development, we need to name files, projects, branches, and other entities.

## Solution

Use a naming convention that is easy on the eye and requires the least amount of typing. Use only lowercase with `-` to separate words. Avoid using uppercase characters, underscore `_`, or camelcase as they require extra keystrokes.

This approach has following benefits:

1. Names are easy to read and remember.
2. Typing effort is minimized, reducing typing errors and saving time.
3. This guideline is easy to remember. When in doubt about a name, choose the option with the least amount of keystrokes.

## Example and implementation

Examples of file names using this convention:

1. `index.html`
2. `style.css`
3. `main.js`

Examples of project names using this convention:

1. `my-project`
2. `web-app`
3. `e-commerce`

Examples of branch names using this convention:

1. `feature/add-new-feature`
2. `fix/bug-123`
3. `hotfix/security-issue`
