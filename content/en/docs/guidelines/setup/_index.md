---
title: "Project Structure and Organizations"
linkTitle: "Structure"
translate: false
weight: 2
description: >
  Guidelines for Ansible content organization, directory layout.
---
