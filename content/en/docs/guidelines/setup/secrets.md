---
categories: ["Guideline"]
tags: [group_vars, secrets, vault, secret_vars, app, awx]
title: "Managing Secrets with Ansible Vault in AAP / AWX"
linkTitle: "Ansible Vault and AAP / AWX"
translate: false
weight: 3
description: >
  Learn how to effectively manage secrets using Ansible Vault in Ansible projects, particularly in the context of Red Hat Automation Platform (AAP) and AWX.
---

---
> Manage secrets using Ansible Vault in a custom folder `secret_vars`. Create a generic Ansible role to read secrets from this directory.

---

## Problem

[AAP]({{< relref path="/docs/concepts/ansible/aap" >}}) does not have built-in support for {{< external-link url="https://docs.ansible.com/ansible/latest/user_guide/vault.html" text="Ansible Vault" htmlproofer_ignore="false" >}}, which poses challenges when integrating Vault-encrypted files in the `group_vars` directory. This limitation affects inventory projects that rely on Git as the foundation for AAP / AWX deployments, causing update failures due to the inability to configure an Ansible vault secret for such projects. It is crucial to address this issue and find a workaround.

{{< alert title="Note:" >}}Implementing built-in support for Ansible Vault in AAP / AWX is complex since it would expose secrets to AAP / AWX users, compromising their confidentiality.{{< /alert >}}

## Context

Managing secrets is a critical task in Ansible projects, and Ansible Vault offers a standard and straightforward solution. However, using Ansible Vault effectively within the context of AAP / AWX requires specific setup and considerations.

## Solution

To overcome the challenges mentioned above, follow these steps:

1. Create a custom folder named `secret_vars` to store secrets that can be included using `include_vars`. This folder will serve as an alternative for storing secrets to the default `group_vars` directory.
2. Develop a generic and flexible Ansible role capable of utilizing the `secret_vars` folder. This role should be compatible with both AAP and the Ansible CLI.

In addition to the above solution, it is recommended to use Ansible Vault during development, as managing secrets is integral to inventory projects. The following guidelines are suggested:
1. Within Ansible roles, set the default password as `supersecure`.
2. Use `secret` as the default password for passwords added `secrets_vars` / vault folder.
3. If technical password requirements prevent the use of `secret`, employ `supersecret`. In cases where `supersecret` is still not sufficiently strong, opt for a valid custom password.

Implementing the above approach provides the following benefits:
1. Development passwords are easy, saving time during the development / testing process.
2. Secrets are documented upon identification, thanks to the use of Ansible Vault even during development.
3. Any missed passwords or undocumented secrets become evident during development, as the configured password will be `supersecret` instead of the expected `secret`.
4. The solution is compatible with Red Hat Automation Platform (AAP), AWX, and the Ansible CLI.

## Examples and implementation

Refer to the `secrets` role within the [`c2platform.core`](<{{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}>) collection for an implementation example. The role utilizes the `common_secrets_dirs` list, which can be configured with multiple locations for the `secret_vars` folder. The following example works for both the Ansible CLI and AAP / AWX. When using AAP, AAP will place the vault in the specific location `/runner/project/secret_vars/development`.

```yaml
common_secrets_dirs:
  - "{{ inventory_dir }}/secret_vars/development"
  - "/runner/project/secret_vars/development"  #  awx / aap
```

To view or edit the secrets/vault, use the following command:

```bash
EDITOR=nano ANSIBLE_CONFIG=ansible-dev.cfg ansible-vault edit secret_vars/development/main.yml --vault-password-file vpass
```

{{< alert title="Note:" >}}The `vpass` file is automatically created by [Vagrant]({{< relref path="/docs/concepts/dev/vagrant" >}}). Refer to the Vagrantfile in the [`c2platform/ansible`](<{{< relref "/docs/gitlab/c2platform/ansible" >}}>) inventory project for an example.{{< /alert >}}

For convenience, create an alias to simplify vault management and ensure no secrets are overlooked.

```bash
alias c2d-secrets='EDITOR=nano ANSIBLE_CONFIG=ansible-dev.cfg ansible-vault edit secret_vars/development/main.yml --vault-password-file vpass'
```
