---
categories: [Guideline]
tags: [ansible,internal roles, inventory-project]
title: "Ansible Guideline: Using Internal Roles"
linkTitle: Using Internal Roles
translate: false
weight: 10
description: >
    This guideline is tailored for teams that are relatively new to Ansible and wish
    to gain experience by using internal roles.
---

## Introduction

This guideline is tailored for teams that are relatively new to Ansible and wish
to gain experience by using internal roles. Internal roles exist within the
inventory project alongside playbooks and group_vars, offering a straightforward
way to organize and manage your Ansible code without the need for maintaining
Ansible collections.

## Benefits of Using Internal Roles

1. **Simplified Structure**: Internal roles reside in the same repository as
   your playbooks and group_vars, providing a coherent and straightforward
   directory structure that is easy to navigate.

2. **Fast Iterations**: Without the overhead of release management associated
   with Ansible collections, teams can focus on rapidly iterating and improving
   their roles based on immediate feedback.

3. **Single Repository**: Having all your playbooks, roles, and configurations
   in one repository simplifies version control and collaboration, making it
   easier for teams to coordinate and share their work.

4. **Incremental Learning**: Teams can progressively enhance their knowledge of
   Ansible by making small and manageable changes, without needing to adhere to
   the higher standards required for reusable collections.

## Directory Structure

Here's a recommended directory structure for an inventory project that uses
internal roles:

```
project-root/
├── group_vars/
│   ├── all.yml
│   └── webservers.yml
├── host_vars/
│   └── web01.yml
├── inventory/
├── playbooks/
│   ├── site.yml
│   └── webserver.yml
└── roles/
    ├── internal/                     # Internal roles directory
    │   ├── role1/
    │   ├── role2/
    │   └── ...
    └── external/                     # External roles directory for locally patched/customized roles
        ├── community_role1/          # Example external role with local enhancements
        │   ├── tasks/
        │   ├── templates/
        │   ├── files/
        │   ├── vars/
        │   ├── defaults/
        │   └── handlers/
        └── community_role2/          # Another external role
            ├── tasks/
            └── ...

```

### Explanation of the Directory Structure

- **`group_vars/` and `host_vars/`**: These directories contain variable files
  for groups of hosts and individual hosts, respectively.

- **`inventory/`**: This is the inventory file or directory where you define the
  hosts and groups of hosts that Ansible will manage.

- **`playbooks/`**: This directory holds your Ansible playbooks, which define
  the orchestration of tasks across your hosts.

- **`roles/`**: The roles directory contains all the roles available to your
  playbooks.

  - **`internal/`**: This subdirectory is specifically for internal roles. These
    roles are meant for use within this project and are not intended for
    external sharing or reuse. Each internal role contains standard
    subdirectories like `tasks`, `templates`, `files`, `vars`, `defaults`, and
    `handlers`.

  - **`roles/external/`**: This directory is dedicated to roles that have been
    sourced externally—either from Ansible Galaxy or other repositories. Instead
    of utilizing these roles directly, this directory allows for a controlled,
    local modification process where roles can be patched or extended according
    to your specific needs.

    - **Purpose**:
      - **Local Adjustments**: If an external role has issues or lacks certain
        functionality, local copies can be modified to address these needs while
        enabling you to continue utilizing the role.
      - **Custom Enhancements**: Add features or customize the behavior of the
        role to better fit your infrastructure requirements.
      - **Compliance with Policies**: Adheres to company policies that may
        restrict direct use of internet-sourced resources by maintaining a local
        version that has been reviewed or approved.

    - **Version Control**: By maintaining these roles in version control, you
      can track your modifications, making it easier to integrate upstream
      changes when they become available.

    - **Parallel Contribution Process**: As you make changes or enhancements
      locally, you can also engage in a parallel process of submitting bug
      reports, feature requests, or pull requests to the original role
      maintainers. This potentially helps the upstream role improve while
      allowing you to keep using the role in its modified form.

## Practices for Internal Roles

1. **Local Development**: Since everything is in one repository, you can easily
   test your changes locally before pushing them. Use `ansible-playbook`
   commands to verify tasks without needing to handle packaging or distribution.

2. **Consistent Naming**: Use clear and descriptive names for your roles and
   playbooks to make it easier for your team to understand their purpose and
   usage.

3. **Incremental Changes**: Start with simple tasks and gradually add complexity
   as the team becomes more confident with Ansible. Constantly refactor and
   improve your roles.

4. **Documentation and Comments**: Even though these roles are internal, it's
   helpful to include comments and brief documentation within your tasks and
   roles to describe their functionality.

5. **Focus on Role Functionality**: Since these are not designed for reuse
   across multiple projects, focus on the specific needs of your project. This
   might mean roles are more tailored and less generic compared to those in a
   collection.

6. **Peer Reviews**: Encourage team members to review each other's Ansible code.
   This promotes learning through shared insights and suggestions for
   improvements.

7. **Version Control Practices**: Use branches effectively for new features or
   experiments, and ensure mainline branches are stable. Use pull requests and
   code reviews as part of a collaborative workflow.

## Transitioning to Ansible Collections

As your team grows more comfortable with Ansible and the quality of your roles
improves, consider the following steps to transition to collections if needed:

- **Standardize Role Development**: Start adopting best practices for role
  development and documentation, aligning more closely with community standards.
- **Separation of Concerns**: Distinguish between roles that could be reusable
  and those that are inherently project-specific.
- **Establish Release Processes**: Introduce versioning and release processes as
  roles mature and are used beyond their initial scope.

## Conclusion

For teams just starting with Ansible, using internal roles within a single
repository provides a manageable and effective way to develop and maintain
infrastructure automation. This approach allows for rapid iteration and
learning, laying a solid foundation for future growth and potential expansion
into Ansible collections.

