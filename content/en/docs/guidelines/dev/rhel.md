---
categories: ["Guideline"]
tags: [rhel, redhat, subscription]
title: "Streamlining RHEL Registration and Subscription Automation"
linkTitle: "RHEL Automated Registration"
translate: false
weight: 3
description: >
    Learn how the automation of Red Hat Linux registration and subscription enhances
    the C2 Platform Development environment, simplifying access to necessary
    resources and services.
---


Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---
> Automate the registration and unregistration of RHEL machines using Vagrant in
> conjunction with the `vagrant-registration` plugin, streamlining development
> workflows.

---

## Problem

Accessing Red Hat services and software repositories is crucial for developers
working in environments based on Red Hat Enterprise Linux (RHEL). Automating the
system registration and subscription management pr0ocesses is key to ensuring
uninterrupted access to these essential resources.

## Context

In environments utilizing RHEL 9, the need arises for an efficient mechanism to
register systems with Red Hat Subscription Management (RHSM) and manage
subscriptions. This enables developers to seamlessly install software packages,
receive updates, and utilize Red Hat services.

## Solution

Leveraging the Red Hat Developer Subscription, developers can cost-effectively
manage their RHEL 9 system registrations within development settings. This
involves registering the system with RHSM and attaching a developer
subscription, thereby unlocking access to crucial Red Hat software repositories
and services.

To automate this process within the C2Platform development environment, the
{{< external-link
url="https://github.com/projectatomic/adb-vagrant-registration"
text="vagrant-registration" htmlproofer_ignore="false" >}}
is pre-installed. This facilitates a smooth, automated registration experience.

## Examples and implementation

The example below illustrates the process of registering a RHEL 9 system and
managing subscriptions through terminal commands:

```bash
subscription-manager register --username <username>
subscription-manager attach
```

For detailed automation examples, refer to the
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})
project. Key points include:

1. The `Vagrantfile` incorporates a code snippet for handling registration. If
   you configure your
   {{< external-link
   url="https://developers.redhat.com"
   text="Red Hat Developer"
   htmlproofer_ignore="true" >}}
   account credentials using variables `RHEL_DEV_ACCOUNT` and `RHEL_DEV_SECRET`,
   the setup will be fully automated. If you don't configure these credentials,
   Vagrant will prompt you for the credentials each time you create a RHEL based
   VM.
2. The `Vagrantfile.yml` includes a `rhel9` box definition with a `registration`
   flag, indicating that systems based on this box will be automatically
   registered upon creation and unregistered upon destruction.

This setup guarantees that systems spun up with the rhel9 configuration will
automatically handle registration and unregistration, streamlining the
development process and ensuring compliance with Red Hat's subscription
management.


**Registration and Unregistration in Action:**

The following terminal outputs demonstrate the automatic registration and
unregistration process when managing a Vagrant box:

1. Upon executing `vagrant up`, the system registers with Red Hat, facilitated by
   the vagrant-registration plugin.
1. Running `vagrant destroy` triggers the system's unregistration, maintaining a
   clean state and ensuring no unnecessary subscriptions remain active.


<p><details>
  <summary><kbd>Show me</kbd></summary><p>

Note the lines below with "Registering box with vagrant-registration..." and
"Unregistering box with vagrant-registration..."


```bash
[:ansible-gis]└2 master(+15/-1,+1/-1) ± vagrant up gsd-ansible-repo --no-provision
Bringing machine 'gsd-ansible-repo' up with 'virtualbox' provider...
==> gsd-ansible-repo: Importing base box 'generic/rhel9'...
==> gsd-ansible-repo: Matching MAC address for NAT networking...
==> gsd-ansible-repo: Checking if box 'generic/rhel9' version '4.3.12' is up to date...
==> gsd-ansible-repo: Setting the name of the VM: ansible-gis_gsd-ansible-repo_1708418067798_69697
==> gsd-ansible-repo: Fixed port collision for 22 => 2222. Now on port 2206.
==> gsd-ansible-repo: Clearing any previously set network interfaces...
==> gsd-ansible-repo: Preparing network interfaces based on configuration...
    gsd-ansible-repo: Adapter 1: nat
    gsd-ansible-repo: Adapter 2: hostonly
==> gsd-ansible-repo: Forwarding ports...
    gsd-ansible-repo: 22 (guest) => 2206 (host) (adapter 1)
==> gsd-ansible-repo: Running 'pre-boot' VM customizations...
==> gsd-ansible-repo: Booting VM...
==> gsd-ansible-repo: Waiting for machine to boot. This may take a few minutes...
    gsd-ansible-repo: SSH address: 127.0.0.1:2206
    gsd-ansible-repo: SSH username: vagrant
    gsd-ansible-repo: SSH auth method: private key
==> gsd-ansible-repo: Machine booted and ready!
==> gsd-ansible-repo: Registering box with vagrant-registration...
==> gsd-ansible-repo: Checking for guest additions in VM...
==> gsd-ansible-repo: Setting hostname...
==> gsd-ansible-repo: Configuring and enabling network interfaces...
==> gsd-ansible-repo: Mounting shared folders...
    gsd-ansible-repo: /vagrant => /home/ostraaten/git/gitlab/c2/ansible-gis
    gsd-ansible-repo: /arcgis-software-repo => /software/projects/rws
    gsd-ansible-repo: /ansible-dev-collections => /home/ostraaten/git/gitlab/c2/ansible-dev-collections
==> gsd-ansible-repo: Machine not provisioned because `--no-provision` is specified.
θ66° [:ansible-gis]└2 master(+15/-1,+1/-1) ±

```

```bash
[:ansible-gis]└2 master(+15/-1,+1/-1) ± vagrant destroy gsd-ansible-repo
==> gsd-ansible-repo: Unregistering box with vagrant-registration...
    gsd-ansible-repo: Are you sure you want to destroy the 'gsd-ansible-repo' VM? [y/N] y
==> gsd-ansible-repo: Forcing shutdown of VM...
==> gsd-ansible-repo: Destroying VM and associated drives...
[:ansible-gis]└2 master(+15/-1,+1/-1) ±
```

</p></details></p>


The following two guides utilize Red Hat boxes with automatic registration:

* [Create a Simple Software Repository for Ansible]({{< relref path="/docs/howto/rws/software" >}})
* [Setup Ansible Control Node]({{< relref path="/docs/howto/rws/control-node" >}})
