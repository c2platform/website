---
categories: ["Guideline"]
tags: [vagrant]
title: "Vagrant Sync Folders"
linkTitle: "Vagrant Sync Folders"
translate: false
weight: 1
description: >
  Learn how to effortlessly synchronize a folder from your host machine to the
  guest machine.
---


Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---
> Create a local and hidden file named `.sync_folders.yml` to efficiently
> synchronize a folder from your host machine to the guest machine.

---

## Problem

During the development process, it is often necessary to make files and folders
from your host machine available within a guest machine provisioned using
Ansible.

## Context

In the context of development, there are scenarios where you need to access files
or directories on the host machine within a guest machine. This typically arises
when provisioning a guest machine using Ansible.

## Solution

{{< external-link url="https://developer.hashicorp.com/vagrant/docs/synced-folders" text="Vagrant Synced Folders" htmlproofer_ignore="false" >}}
enable you to seamlessly work on your project's files on your host machine while
harnessing the guest machine's resources for compilation or execution.

By default, Vagrant shares the project folder (the directory containing the
`Vagrantfile`). However, you can customize this behavior by modifying the
`Vagrantfile` to sync additional folders based on the configuration defined in a
file named `.sync_folders.yml`. This file is intentionally ignored by Git to
prevent accidental inclusion in your version control repository.

## Examples and implementation

For a practical demonstration of a modified `Vagrantfile` that takes advantage
of the `.sync_folders.yml` file, refer to the example in
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})
This Vagrantfile showcases how you can configure folder synchronization. The
`.sync_folders.yml` file can contain YAML configurations like the following:

```yaml
---
- src: /software/projects/rws/
  target: /arcgis-software-repo
- src: ../ansible-dev-collections/
  target: /ansible-dev-collections
```

In this configuration, two folders are synchronized. The first entry in the list
makes the ArcGIS Software available inside the guest machine at
`C:\arcgis-software-repo`. On the host machine, this software resides in the
`/software/projects/rws/` directory.

Additionally, review the `group_vars/all/software-repo.yml` file, which introduces a
[project variable]({{< relref path="/docs/guidelines/coding/var-prefix" >}})
called `gs_software_repo`. This variable is used to configure the location of the
software repository, enabling software downloads depending on the environment:

```yaml
gs_software_repo:
  development_strix: http://192.168.88.5/software
  development: file:///C:/arcgis-software-repo
```

Additionally, the second entry in the `.sync_folders.yml` file deals with
Ansible Collections that are being modified within the guest OS. It places these
collections at the path `C:\ansible-dev-collections` on MS Windows guests and
`/ansible-dev-collections` on Linux systems. This allows you to use them for
Ansible provisioning from within the guest machine. For further details on using
Ansible without Vagrant, please refer to
[Using Ansible without Vagrant]({{< relref path="/docs/howto/dev-environment/ansible-without-vagrant" >}}).


