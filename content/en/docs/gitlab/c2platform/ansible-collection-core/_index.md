---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-core
tags: ['ansible', 'c2platform', 'collection', 'common', 'core', 'linux', 'windows']
title: "Ansible Collection - c2platform.core"
weight: 14
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-collection-core"><code>c2platform/ansible-collection-core</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-core/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-core/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-core/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-core/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.core-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/core/)

C2 Platform generic roles that are used by all or some other roles. These roles
typically don't create services / processes on target node but are dependencies
e.g. packages required by those roles. Or these roles help with Ansible
provisioning for example offers generic Ansible modules, filters etc.

## Roles

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/linux" text="linux" htmlproofer_ignore="false" >}} manage of Linux systems by leveraging +112 modules
  from the the `ansible.builtin`, `ansible.posix`, and `community.general`
  collections.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/server_update" text="server_update" htmlproofer_ignore="false" >}} update yum / apt cache and /or
  upgrade packages.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/bootstrap" text="bootstrap" htmlproofer_ignore="false" >}} bootstrap your nodes with os and pip
  packages.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/mount" text="mount" htmlproofer_ignore="false" >}} configure / manage mount points.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/secrets" text="secrets" htmlproofer_ignore="false" >}} workaround for lack of support for vault when
  using AWX.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/os_trusts" text="os_trusts" htmlproofer_ignore="false" >}} Manage OS trust store.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/cacerts2" text="cacerts2" htmlproofer_ignore="false" >}} create your own smallCA.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/cacerts2_client" text="cacerts2_client" htmlproofer_ignore="false" >}} generate ownca certificates.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/apt_repo" text="apt_repo" htmlproofer_ignore="false" >}} add APT keys, repositories.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/yum" text="yum" htmlproofer_ignore="false" >}} add / remove Yum repositories.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/files" text="files" htmlproofer_ignore="false" >}} manage files, directories, ACL.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/users" text="users" htmlproofer_ignore="false" >}} manage Linux accounts.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/service" text="service" htmlproofer_ignore="false" >}} create systemd services.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/java" text="java" htmlproofer_ignore="false" >}} install java, manage keystores.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/facts" text="facts" htmlproofer_ignore="false" >}} gather facts.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/lcm" text="lcm" htmlproofer_ignore="false" >}} facts for LCM operations for other roles to build upon.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/lvm" text="lvm" htmlproofer_ignore="false" >}} manage data disks for roles using
  {{< external-link url="https://en.wikipedia.org/wiki/Logical_Volume_Manager_%28Linux%29" text="LVM" htmlproofer_ignore="false" >}}.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/rest" text="rest" htmlproofer_ignore="false" >}} interact with REST webservices.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/postgresql_tasks" text="postgresql_tasks" htmlproofer_ignore="false" >}} include tasks for PostgreSQL
  database operations.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/postgresql_client" text="postgresql_client" htmlproofer_ignore="false" >}}.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/restart" text="restart" htmlproofer_ignore="false" >}} orchestrate stop, start and restart over multiple
  nodes.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/env_test/" text="env_test" htmlproofer_ignore="false" >}} network test role.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/facts_cmdb/" text="facts_cmdb" htmlproofer_ignore="false" >}} gather facts for `ansible-cmdb`.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/vagrant_hosts/" text="vagrant_hosts" htmlproofer_ignore="false" >}} manage hosts file on Windows and
  Linux guests.

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
{{< external-link url="https://galaxy.ansible.com/ui/repo/published/c2platform/core/docs/" text="Ansible Galaxy" htmlproofer_ignore="false" >}}
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.core
ansible-doc -t filter --list c2platform.core
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```


---


<!--
id | 39048659
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-collection-core" data-proofer-ignore>c2platform/ansible-collection-core&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
