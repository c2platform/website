---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-dev
tags: ['ansible', 'c2platform', 'collection', 'common', 'dev', 'linux', 'windows']
title: "Ansible Collection - c2platform.dev"
weight: 15
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-collection-dev"><code>c2platform/ansible-collection-dev</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-dev/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-dev/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-dev/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-dev/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.dev-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/dev/)

C2 Platform Ansible Collection with development roles. These roles are not intended to be used for production systems!

## Roles

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-dev/-/blob/master/roles/oracle_database/" text="oracle_database" htmlproofer_ignore="false" >}}.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-dev/-/blob/master/roles/desktop/" text="desktop" htmlproofer_ignore="false" >}}.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-dev/-/blob/master/roles/graylog/" text="graylog" htmlproofer_ignore="false" >}}.

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
{{< external-link url="https://galaxy.ansible.com/ui/repo/published/c2platform/dev/docs/" text="Ansible Galaxy" htmlproofer_ignore="false" >}}
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.dev
ansible-doc -t filter --list c2platform.dev
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```


---


<!--
id | 40905588
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-collection-dev" data-proofer-ignore>c2platform/ansible-collection-dev&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
