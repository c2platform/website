---
categories: GitLab Projects
description: "None"
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: plantuml
tags: []
title: "C2 Platform PlantUML Repository"
weight: 10
---

GitLab: <a href="https://gitlab.com/c2platform/c2/plantuml"><code>c2platform/c2/plantuml</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


This repository contains PlantUML (`puml`) files used for the
{{< external-link url="https://c2platform.org" text="c2platform.org" htmlproofer_ignore="false" >}} website.

## Usage

### Using C4-PlantUML

If you want to use
{{< external-link url="https://github.com/plantuml-stdlib/C4-PlantUML" text="C4-PlantUML" htmlproofer_ignore="false" >}}, include
{{< external-link url="https://gitlab.com/c2platform/c2/plantuml/-/blob/master/c4.puml" text="c4.puml" htmlproofer_ignore="false" >}} in your PlantUML files by using the following line:

```
!include {{< external-link url="https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads" htmlproofer_ignore="false" >}}
```

### Using Sprites Only

If you only want to use the sprites, include {{< external-link url="https://gitlab.com/c2platform/c2/plantuml/-/blob/master/sprites.puml" text="sprites.puml" htmlproofer_ignore="false" >}}
with the following line:

```
!include {{< external-link url="https://gitlab.com/c2platform/c2/plantuml/-/raw/master/sprites.puml?ref_type=heads" htmlproofer_ignore="false" >}}
```

## Development

This repository includes scripts for generating PlantUML (`puml`) files based on
`svg` files.

### Scripts

- {{< external-link url="https://gitlab.com/c2platform/c2/plantuml/-/blob/master/scripts/icon-watcher-converter.sh" text="icon-watcher-converter.sh" htmlproofer_ignore="false" >}}: Uses
  `inotifywait` to detect changes and automatically convert `svg` to `png`,
  `png` to `puml`, and then update the {{< external-link url="https://gitlab.com/c2platform/c2/plantuml/-/blob/master/sprites.puml" text="sprites.puml" htmlproofer_ignore="false" >}} file.
- {{< external-link url="https://gitlab.com/c2platform/c2/plantuml/-/blob/master/scripts/svg2png.py" text="svg2png.py" htmlproofer_ignore="false" >}}: Converts `svg` files to `png`.
- {{< external-link url="https://gitlab.com/c2platform/c2/plantuml/-/blob/master/scripts/png2puml.py" text="png2puml.py" htmlproofer_ignore="false" >}}: Converts `png` files to `puml`.

### Running the Converter

To start the icon watcher converter:

```bash
./scripts/icon-watcher-converter.sh
```

After starting `icon-watcher-converter.sh`, you can drop `svg` files into the
`icons/svg` directory, and the files will be generated and updated
automatically.

### Naming Conventions

- `svg` files should use underscores (`_`) instead of hyphens (`-`) in their
  names.
- For ease of use, it is recommended to use lowercase letters and separate words
  with underscores (`_`).

## Preview

This section provides a preview of the available PlantUML sprites in this
repository.

### Using Sprites

You can use the sprites with the `$sprite` attribute as shown below:

```
System(ide_min50_system, "ide_min50", "ide_min50", $sprite="ide_min50")
```

### Using Tags

You can also use tags:

```
AddSystemTag("GIS_System", $sprite="arcgis", $legendText="GIS platform")
```


```plantuml
@startuml sprites-preview
!include {{< external-link url="https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads" htmlproofer_ignore="false" >}}
!include {{< external-link url="https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml" htmlproofer_ignore="false" >}}
'!include sprites.puml
'start-generated-plantuml



Boundary(vagrant_plus_ansible_min50, "vagrant_plus_ansible_min50") {
    System(vagrant_plus_ansible_min50_system, "vagrant_plus_ansible_min50", "vagrant_plus_ansible_min50", $sprite="vagrant_plus_ansible_min50")
    System_Ext(vagrant_plus_ansible_min50_system_ext, "vagrant_plus_ansible_min50", "vagrant_plus_ansible_min50", $sprite="vagrant_plus_ansible_min50")
    Container(vagrant_plus_ansible_min50_container, "vagrant_plus_ansible_min50", "vagrant_plus_ansible_min50", $sprite="vagrant_plus_ansible_min50")
    Container_Ext(vagrant_plus_ansible_min50_container_ext, "vagrant_plus_ansible_min50", "vagrant_plus_ansible_min50", $sprite="vagrant_plus_ansible_min50")
    Component(vagrant_plus_ansible_min50_component, "vagrant_plus_ansible_min50", "vagrant_plus_ansible_min50", $sprite="vagrant_plus_ansible_min50")
    Component_Ext(vagrant_plus_ansible_min50_component_ext, "vagrant_plus_ansible_min50", "vagrant_plus_ansible_min50", $sprite="vagrant_plus_ansible_min50")
    Lay_Right(vagrant_plus_ansible_min50_system, vagrant_plus_ansible_min50_container)
    Lay_Right(vagrant_plus_ansible_min50_container, vagrant_plus_ansible_min50_component)
    Lay_Right(vagrant_plus_ansible_min50_component, vagrant_plus_ansible_min50_system_ext)
    Lay_Right(vagrant_plus_ansible_min50_system_ext, vagrant_plus_ansible_min50_container_ext)
    Lay_Right(vagrant_plus_ansible_min50_container_ext, vagrant_plus_ansible_min50_component_ext)
}

Boundary(jenkins_min50, "jenkins_min50") {
    System(jenkins_min50_system, "jenkins_min50", "jenkins_min50", $sprite="jenkins_min50")
    System_Ext(jenkins_min50_system_ext, "jenkins_min50", "jenkins_min50", $sprite="jenkins_min50")
    Container(jenkins_min50_container, "jenkins_min50", "jenkins_min50", $sprite="jenkins_min50")
    Container_Ext(jenkins_min50_container_ext, "jenkins_min50", "jenkins_min50", $sprite="jenkins_min50")
    Component(jenkins_min50_component, "jenkins_min50", "jenkins_min50", $sprite="jenkins_min50")
    Component_Ext(jenkins_min50_component_ext, "jenkins_min50", "jenkins_min50", $sprite="jenkins_min50")
    Lay_Right(jenkins_min50_system, jenkins_min50_container)
    Lay_Right(jenkins_min50_container, jenkins_min50_component)
    Lay_Right(jenkins_min50_component, jenkins_min50_system_ext)
    Lay_Right(jenkins_min50_system_ext, jenkins_min50_container_ext)
    Lay_Right(jenkins_min50_container_ext, jenkins_min50_component_ext)
    Lay_Up(jenkins_min50_system, vagrant_plus_ansible_min50_system)
}

Boundary(ide_min50, "ide_min50") {
    System(ide_min50_system, "ide_min50", "ide_min50", $sprite="ide_min50")
    System_Ext(ide_min50_system_ext, "ide_min50", "ide_min50", $sprite="ide_min50")
    Container(ide_min50_container, "ide_min50", "ide_min50", $sprite="ide_min50")
    Container_Ext(ide_min50_container_ext, "ide_min50", "ide_min50", $sprite="ide_min50")
    Component(ide_min50_component, "ide_min50", "ide_min50", $sprite="ide_min50")
    Component_Ext(ide_min50_component_ext, "ide_min50", "ide_min50", $sprite="ide_min50")
    Lay_Right(ide_min50_system, ide_min50_container)
    Lay_Right(ide_min50_container, ide_min50_component)
    Lay_Right(ide_min50_component, ide_min50_system_ext)
    Lay_Right(ide_min50_system_ext, ide_min50_container_ext)
    Lay_Right(ide_min50_container_ext, ide_min50_component_ext)
    Lay_Up(ide_min50_system, jenkins_min50_system)
}

Boundary(fme_flow_hosted, "fme_flow_hosted") {
    System(fme_flow_hosted_system, "fme_flow_hosted", "fme_flow_hosted", $sprite="fme_flow_hosted")
    System_Ext(fme_flow_hosted_system_ext, "fme_flow_hosted", "fme_flow_hosted", $sprite="fme_flow_hosted")
    Container(fme_flow_hosted_container, "fme_flow_hosted", "fme_flow_hosted", $sprite="fme_flow_hosted")
    Container_Ext(fme_flow_hosted_container_ext, "fme_flow_hosted", "fme_flow_hosted", $sprite="fme_flow_hosted")
    Component(fme_flow_hosted_component, "fme_flow_hosted", "fme_flow_hosted", $sprite="fme_flow_hosted")
    Component_Ext(fme_flow_hosted_component_ext, "fme_flow_hosted", "fme_flow_hosted", $sprite="fme_flow_hosted")
    Lay_Right(fme_flow_hosted_system, fme_flow_hosted_container)
    Lay_Right(fme_flow_hosted_container, fme_flow_hosted_component)
    Lay_Right(fme_flow_hosted_component, fme_flow_hosted_system_ext)
    Lay_Right(fme_flow_hosted_system_ext, fme_flow_hosted_container_ext)
    Lay_Right(fme_flow_hosted_container_ext, fme_flow_hosted_component_ext)
    Lay_Up(fme_flow_hosted_system, ide_min50_system)
}

Boundary(fme_form, "fme_form") {
    System(fme_form_system, "fme_form", "fme_form", $sprite="fme_form")
    System_Ext(fme_form_system_ext, "fme_form", "fme_form", $sprite="fme_form")
    Container(fme_form_container, "fme_form", "fme_form", $sprite="fme_form")
    Container_Ext(fme_form_container_ext, "fme_form", "fme_form", $sprite="fme_form")
    Component(fme_form_component, "fme_form", "fme_form", $sprite="fme_form")
    Component_Ext(fme_form_component_ext, "fme_form", "fme_form", $sprite="fme_form")
    Lay_Right(fme_form_system, fme_form_container)
    Lay_Right(fme_form_container, fme_form_component)
    Lay_Right(fme_form_component, fme_form_system_ext)
    Lay_Right(fme_form_system_ext, fme_form_container_ext)
    Lay_Right(fme_form_container_ext, fme_form_component_ext)
    Lay_Up(fme_form_system, fme_flow_hosted_system)
}

Boundary(browser_min50, "browser_min50") {
    System(browser_min50_system, "browser_min50", "browser_min50", $sprite="browser_min50")
    System_Ext(browser_min50_system_ext, "browser_min50", "browser_min50", $sprite="browser_min50")
    Container(browser_min50_container, "browser_min50", "browser_min50", $sprite="browser_min50")
    Container_Ext(browser_min50_container_ext, "browser_min50", "browser_min50", $sprite="browser_min50")
    Component(browser_min50_component, "browser_min50", "browser_min50", $sprite="browser_min50")
    Component_Ext(browser_min50_component_ext, "browser_min50", "browser_min50", $sprite="browser_min50")
    Lay_Right(browser_min50_system, browser_min50_container)
    Lay_Right(browser_min50_container, browser_min50_component)
    Lay_Right(browser_min50_component, browser_min50_system_ext)
    Lay_Right(browser_min50_system_ext, browser_min50_container_ext)
    Lay_Right(browser_min50_container_ext, browser_min50_component_ext)
    Lay_Up(browser_min50_system, fme_form_system)
}

Boundary(internet2, "internet2") {
    System(internet2_system, "internet2", "internet2", $sprite="internet2")
    System_Ext(internet2_system_ext, "internet2", "internet2", $sprite="internet2")
    Container(internet2_container, "internet2", "internet2", $sprite="internet2")
    Container_Ext(internet2_container_ext, "internet2", "internet2", $sprite="internet2")
    Component(internet2_component, "internet2", "internet2", $sprite="internet2")
    Component_Ext(internet2_component_ext, "internet2", "internet2", $sprite="internet2")
    Lay_Right(internet2_system, internet2_container)
    Lay_Right(internet2_container, internet2_component)
    Lay_Right(internet2_component, internet2_system_ext)
    Lay_Right(internet2_system_ext, internet2_container_ext)
    Lay_Right(internet2_container_ext, internet2_component_ext)
    Lay_Up(internet2_system, browser_min50_system)
}

Boundary(politie_min50, "politie_min50") {
    System(politie_min50_system, "politie_min50", "politie_min50", $sprite="politie_min50")
    System_Ext(politie_min50_system_ext, "politie_min50", "politie_min50", $sprite="politie_min50")
    Container(politie_min50_container, "politie_min50", "politie_min50", $sprite="politie_min50")
    Container_Ext(politie_min50_container_ext, "politie_min50", "politie_min50", $sprite="politie_min50")
    Component(politie_min50_component, "politie_min50", "politie_min50", $sprite="politie_min50")
    Component_Ext(politie_min50_component_ext, "politie_min50", "politie_min50", $sprite="politie_min50")
    Lay_Right(politie_min50_system, politie_min50_container)
    Lay_Right(politie_min50_container, politie_min50_component)
    Lay_Right(politie_min50_component, politie_min50_system_ext)
    Lay_Right(politie_min50_system_ext, politie_min50_container_ext)
    Lay_Right(politie_min50_container_ext, politie_min50_component_ext)
    Lay_Up(politie_min50_system, internet2_system)
}

Boundary(lxc, "lxc") {
    System(lxc_system, "lxc", "lxc", $sprite="lxc")
    System_Ext(lxc_system_ext, "lxc", "lxc", $sprite="lxc")
    Container(lxc_container, "lxc", "lxc", $sprite="lxc")
    Container_Ext(lxc_container_ext, "lxc", "lxc", $sprite="lxc")
    Component(lxc_component, "lxc", "lxc", $sprite="lxc")
    Component_Ext(lxc_component_ext, "lxc", "lxc", $sprite="lxc")
    Lay_Right(lxc_system, lxc_container)
    Lay_Right(lxc_container, lxc_component)
    Lay_Right(lxc_component, lxc_system_ext)
    Lay_Right(lxc_system_ext, lxc_container_ext)
    Lay_Right(lxc_container_ext, lxc_component_ext)
    Lay_Up(lxc_system, politie_min50_system)
}

Boundary(azure, "azure") {
    System(azure_system, "azure", "azure", $sprite="azure")
    System_Ext(azure_system_ext, "azure", "azure", $sprite="azure")
    Container(azure_container, "azure", "azure", $sprite="azure")
    Container_Ext(azure_container_ext, "azure", "azure", $sprite="azure")
    Component(azure_component, "azure", "azure", $sprite="azure")
    Component_Ext(azure_component_ext, "azure", "azure", $sprite="azure")
    Lay_Right(azure_system, azure_container)
    Lay_Right(azure_container, azure_component)
    Lay_Right(azure_component, azure_system_ext)
    Lay_Right(azure_system_ext, azure_container_ext)
    Lay_Right(azure_container_ext, azure_component_ext)
    Lay_Up(azure_system, lxc_system)
}

Boundary(laptop, "laptop") {
    System(laptop_system, "laptop", "laptop", $sprite="laptop")
    System_Ext(laptop_system_ext, "laptop", "laptop", $sprite="laptop")
    Container(laptop_container, "laptop", "laptop", $sprite="laptop")
    Container_Ext(laptop_container_ext, "laptop", "laptop", $sprite="laptop")
    Component(laptop_component, "laptop", "laptop", $sprite="laptop")
    Component_Ext(laptop_component_ext, "laptop", "laptop", $sprite="laptop")
    Lay_Right(laptop_system, laptop_container)
    Lay_Right(laptop_container, laptop_component)
    Lay_Right(laptop_component, laptop_system_ext)
    Lay_Right(laptop_system_ext, laptop_container_ext)
    Lay_Right(laptop_container_ext, laptop_component_ext)
    Lay_Up(laptop_system, azure_system)
}

Boundary(internet3, "internet3") {
    System(internet3_system, "internet3", "internet3", $sprite="internet3")
    System_Ext(internet3_system_ext, "internet3", "internet3", $sprite="internet3")
    Container(internet3_container, "internet3", "internet3", $sprite="internet3")
    Container_Ext(internet3_container_ext, "internet3", "internet3", $sprite="internet3")
    Component(internet3_component, "internet3", "internet3", $sprite="internet3")
    Component_Ext(internet3_component_ext, "internet3", "internet3", $sprite="internet3")
    Lay_Right(internet3_system, internet3_container)
    Lay_Right(internet3_container, internet3_component)
    Lay_Right(internet3_component, internet3_system_ext)
    Lay_Right(internet3_system_ext, internet3_container_ext)
    Lay_Right(internet3_container_ext, internet3_component_ext)
    Lay_Up(internet3_system, laptop_system)
}

Boundary(internet, "internet") {
    System(internet_system, "internet", "internet", $sprite="internet")
    System_Ext(internet_system_ext, "internet", "internet", $sprite="internet")
    Container(internet_container, "internet", "internet", $sprite="internet")
    Container_Ext(internet_container_ext, "internet", "internet", $sprite="internet")
    Component(internet_component, "internet", "internet", $sprite="internet")
    Component_Ext(internet_component_ext, "internet", "internet", $sprite="internet")
    Lay_Right(internet_system, internet_container)
    Lay_Right(internet_container, internet_component)
    Lay_Right(internet_component, internet_system_ext)
    Lay_Right(internet_system_ext, internet_container_ext)
    Lay_Right(internet_container_ext, internet_component_ext)
    Lay_Up(internet_system, internet3_system)
}

Boundary(rws, "rws") {
    System(rws_system, "rws", "rws", $sprite="rws")
    System_Ext(rws_system_ext, "rws", "rws", $sprite="rws")
    Container(rws_container, "rws", "rws", $sprite="rws")
    Container_Ext(rws_container_ext, "rws", "rws", $sprite="rws")
    Component(rws_component, "rws", "rws", $sprite="rws")
    Component_Ext(rws_component_ext, "rws", "rws", $sprite="rws")
    Lay_Right(rws_system, rws_container)
    Lay_Right(rws_container, rws_component)
    Lay_Right(rws_component, rws_system_ext)
    Lay_Right(rws_system_ext, rws_container_ext)
    Lay_Right(rws_container_ext, rws_component_ext)
    Lay_Up(rws_system, internet_system)
}

Boundary(bkwi, "bkwi") {
    System(bkwi_system, "bkwi", "bkwi", $sprite="bkwi")
    System_Ext(bkwi_system_ext, "bkwi", "bkwi", $sprite="bkwi")
    Container(bkwi_container, "bkwi", "bkwi", $sprite="bkwi")
    Container_Ext(bkwi_container_ext, "bkwi", "bkwi", $sprite="bkwi")
    Component(bkwi_component, "bkwi", "bkwi", $sprite="bkwi")
    Component_Ext(bkwi_component_ext, "bkwi", "bkwi", $sprite="bkwi")
    Lay_Right(bkwi_system, bkwi_container)
    Lay_Right(bkwi_container, bkwi_component)
    Lay_Right(bkwi_component, bkwi_system_ext)
    Lay_Right(bkwi_system_ext, bkwi_container_ext)
    Lay_Right(bkwi_container_ext, bkwi_component_ext)
    Lay_Up(bkwi_system, rws_system)
}

Boundary(kubernetes, "kubernetes") {
    System(kubernetes_system, "kubernetes", "kubernetes", $sprite="kubernetes")
    System_Ext(kubernetes_system_ext, "kubernetes", "kubernetes", $sprite="kubernetes")
    Container(kubernetes_container, "kubernetes", "kubernetes", $sprite="kubernetes")
    Container_Ext(kubernetes_container_ext, "kubernetes", "kubernetes", $sprite="kubernetes")
    Component(kubernetes_component, "kubernetes", "kubernetes", $sprite="kubernetes")
    Component_Ext(kubernetes_component_ext, "kubernetes", "kubernetes", $sprite="kubernetes")
    Lay_Right(kubernetes_system, kubernetes_container)
    Lay_Right(kubernetes_container, kubernetes_component)
    Lay_Right(kubernetes_component, kubernetes_system_ext)
    Lay_Right(kubernetes_system_ext, kubernetes_container_ext)
    Lay_Right(kubernetes_container_ext, kubernetes_component_ext)
    Lay_Up(kubernetes_system, bkwi_system)
}

Boundary(vagrant_ansible, "vagrant_ansible") {
    System(vagrant_ansible_system, "vagrant_ansible", "vagrant_ansible", $sprite="vagrant_ansible")
    System_Ext(vagrant_ansible_system_ext, "vagrant_ansible", "vagrant_ansible", $sprite="vagrant_ansible")
    Container(vagrant_ansible_container, "vagrant_ansible", "vagrant_ansible", $sprite="vagrant_ansible")
    Container_Ext(vagrant_ansible_container_ext, "vagrant_ansible", "vagrant_ansible", $sprite="vagrant_ansible")
    Component(vagrant_ansible_component, "vagrant_ansible", "vagrant_ansible", $sprite="vagrant_ansible")
    Component_Ext(vagrant_ansible_component_ext, "vagrant_ansible", "vagrant_ansible", $sprite="vagrant_ansible")
    Lay_Right(vagrant_ansible_system, vagrant_ansible_container)
    Lay_Right(vagrant_ansible_container, vagrant_ansible_component)
    Lay_Right(vagrant_ansible_component, vagrant_ansible_system_ext)
    Lay_Right(vagrant_ansible_system_ext, vagrant_ansible_container_ext)
    Lay_Right(vagrant_ansible_container_ext, vagrant_ansible_component_ext)
    Lay_Up(vagrant_ansible_system, kubernetes_system)
}

Boundary(jenkins_max50, "jenkins_max50") {
    System(jenkins_max50_system, "jenkins_max50", "jenkins_max50", $sprite="jenkins_max50")
    System_Ext(jenkins_max50_system_ext, "jenkins_max50", "jenkins_max50", $sprite="jenkins_max50")
    Container(jenkins_max50_container, "jenkins_max50", "jenkins_max50", $sprite="jenkins_max50")
    Container_Ext(jenkins_max50_container_ext, "jenkins_max50", "jenkins_max50", $sprite="jenkins_max50")
    Component(jenkins_max50_component, "jenkins_max50", "jenkins_max50", $sprite="jenkins_max50")
    Component_Ext(jenkins_max50_component_ext, "jenkins_max50", "jenkins_max50", $sprite="jenkins_max50")
    Lay_Right(jenkins_max50_system, jenkins_max50_container)
    Lay_Right(jenkins_max50_container, jenkins_max50_component)
    Lay_Right(jenkins_max50_component, jenkins_max50_system_ext)
    Lay_Right(jenkins_max50_system_ext, jenkins_max50_container_ext)
    Lay_Right(jenkins_max50_container_ext, jenkins_max50_component_ext)
    Lay_Up(jenkins_max50_system, vagrant_ansible_system)
}

Boundary(awx_max50, "awx_max50") {
    System(awx_max50_system, "awx_max50", "awx_max50", $sprite="awx_max50")
    System_Ext(awx_max50_system_ext, "awx_max50", "awx_max50", $sprite="awx_max50")
    Container(awx_max50_container, "awx_max50", "awx_max50", $sprite="awx_max50")
    Container_Ext(awx_max50_container_ext, "awx_max50", "awx_max50", $sprite="awx_max50")
    Component(awx_max50_component, "awx_max50", "awx_max50", $sprite="awx_max50")
    Component_Ext(awx_max50_component_ext, "awx_max50", "awx_max50", $sprite="awx_max50")
    Lay_Right(awx_max50_system, awx_max50_container)
    Lay_Right(awx_max50_container, awx_max50_component)
    Lay_Right(awx_max50_component, awx_max50_system_ext)
    Lay_Right(awx_max50_system_ext, awx_max50_container_ext)
    Lay_Right(awx_max50_container_ext, awx_max50_component_ext)
    Lay_Up(awx_max50_system, jenkins_max50_system)
}

Boundary(awx_min50, "awx_min50") {
    System(awx_min50_system, "awx_min50", "awx_min50", $sprite="awx_min50")
    System_Ext(awx_min50_system_ext, "awx_min50", "awx_min50", $sprite="awx_min50")
    Container(awx_min50_container, "awx_min50", "awx_min50", $sprite="awx_min50")
    Container_Ext(awx_min50_container_ext, "awx_min50", "awx_min50", $sprite="awx_min50")
    Component(awx_min50_component, "awx_min50", "awx_min50", $sprite="awx_min50")
    Component_Ext(awx_min50_component_ext, "awx_min50", "awx_min50", $sprite="awx_min50")
    Lay_Right(awx_min50_system, awx_min50_container)
    Lay_Right(awx_min50_container, awx_min50_component)
    Lay_Right(awx_min50_component, awx_min50_system_ext)
    Lay_Right(awx_min50_system_ext, awx_min50_container_ext)
    Lay_Right(awx_min50_container_ext, awx_min50_component_ext)
    Lay_Up(awx_min50_system, awx_max50_system)
}

Boundary(vagrant_plus_ansible_max50, "vagrant_plus_ansible_max50") {
    System(vagrant_plus_ansible_max50_system, "vagrant_plus_ansible_max50", "vagrant_plus_ansible_max50", $sprite="vagrant_plus_ansible_max50")
    System_Ext(vagrant_plus_ansible_max50_system_ext, "vagrant_plus_ansible_max50", "vagrant_plus_ansible_max50", $sprite="vagrant_plus_ansible_max50")
    Container(vagrant_plus_ansible_max50_container, "vagrant_plus_ansible_max50", "vagrant_plus_ansible_max50", $sprite="vagrant_plus_ansible_max50")
    Container_Ext(vagrant_plus_ansible_max50_container_ext, "vagrant_plus_ansible_max50", "vagrant_plus_ansible_max50", $sprite="vagrant_plus_ansible_max50")
    Component(vagrant_plus_ansible_max50_component, "vagrant_plus_ansible_max50", "vagrant_plus_ansible_max50", $sprite="vagrant_plus_ansible_max50")
    Component_Ext(vagrant_plus_ansible_max50_component_ext, "vagrant_plus_ansible_max50", "vagrant_plus_ansible_max50", $sprite="vagrant_plus_ansible_max50")
    Lay_Right(vagrant_plus_ansible_max50_system, vagrant_plus_ansible_max50_container)
    Lay_Right(vagrant_plus_ansible_max50_container, vagrant_plus_ansible_max50_component)
    Lay_Right(vagrant_plus_ansible_max50_component, vagrant_plus_ansible_max50_system_ext)
    Lay_Right(vagrant_plus_ansible_max50_system_ext, vagrant_plus_ansible_max50_container_ext)
    Lay_Right(vagrant_plus_ansible_max50_container_ext, vagrant_plus_ansible_max50_component_ext)
    Lay_Up(vagrant_plus_ansible_max50_system, awx_min50_system)
}

Boundary(arcgis, "arcgis") {
    System(arcgis_system, "arcgis", "arcgis", $sprite="arcgis")
    System_Ext(arcgis_system_ext, "arcgis", "arcgis", $sprite="arcgis")
    Container(arcgis_container, "arcgis", "arcgis", $sprite="arcgis")
    Container_Ext(arcgis_container_ext, "arcgis", "arcgis", $sprite="arcgis")
    Component(arcgis_component, "arcgis", "arcgis", $sprite="arcgis")
    Component_Ext(arcgis_component_ext, "arcgis", "arcgis", $sprite="arcgis")
    Lay_Right(arcgis_system, arcgis_container)
    Lay_Right(arcgis_container, arcgis_component)
    Lay_Right(arcgis_component, arcgis_system_ext)
    Lay_Right(arcgis_system_ext, arcgis_container_ext)
    Lay_Right(arcgis_container_ext, arcgis_component_ext)
    Lay_Up(arcgis_system, vagrant_plus_ansible_max50_system)
}

Boundary(fme_flow, "fme_flow") {
    System(fme_flow_system, "fme_flow", "fme_flow", $sprite="fme_flow")
    System_Ext(fme_flow_system_ext, "fme_flow", "fme_flow", $sprite="fme_flow")
    Container(fme_flow_container, "fme_flow", "fme_flow", $sprite="fme_flow")
    Container_Ext(fme_flow_container_ext, "fme_flow", "fme_flow", $sprite="fme_flow")
    Component(fme_flow_component, "fme_flow", "fme_flow", $sprite="fme_flow")
    Component_Ext(fme_flow_component_ext, "fme_flow", "fme_flow", $sprite="fme_flow")
    Lay_Right(fme_flow_system, fme_flow_container)
    Lay_Right(fme_flow_container, fme_flow_component)
    Lay_Right(fme_flow_component, fme_flow_system_ext)
    Lay_Right(fme_flow_system_ext, fme_flow_container_ext)
    Lay_Right(fme_flow_container_ext, fme_flow_component_ext)
    Lay_Up(fme_flow_system, arcgis_system)
}

Boundary(internet_www, "internet_www") {
    System(internet_www_system, "internet_www", "internet_www", $sprite="internet_www")
    System_Ext(internet_www_system_ext, "internet_www", "internet_www", $sprite="internet_www")
    Container(internet_www_container, "internet_www", "internet_www", $sprite="internet_www")
    Container_Ext(internet_www_container_ext, "internet_www", "internet_www", $sprite="internet_www")
    Component(internet_www_component, "internet_www", "internet_www", $sprite="internet_www")
    Component_Ext(internet_www_component_ext, "internet_www", "internet_www", $sprite="internet_www")
    Lay_Right(internet_www_system, internet_www_container)
    Lay_Right(internet_www_container, internet_www_component)
    Lay_Right(internet_www_component, internet_www_system_ext)
    Lay_Right(internet_www_system_ext, internet_www_container_ext)
    Lay_Right(internet_www_container_ext, internet_www_component_ext)
    Lay_Up(internet_www_system, fme_flow_system)
}

Boundary(ide_max50, "ide_max50") {
    System(ide_max50_system, "ide_max50", "ide_max50", $sprite="ide_max50")
    System_Ext(ide_max50_system_ext, "ide_max50", "ide_max50", $sprite="ide_max50")
    Container(ide_max50_container, "ide_max50", "ide_max50", $sprite="ide_max50")
    Container_Ext(ide_max50_container_ext, "ide_max50", "ide_max50", $sprite="ide_max50")
    Component(ide_max50_component, "ide_max50", "ide_max50", $sprite="ide_max50")
    Component_Ext(ide_max50_component_ext, "ide_max50", "ide_max50", $sprite="ide_max50")
    Lay_Right(ide_max50_system, ide_max50_container)
    Lay_Right(ide_max50_container, ide_max50_component)
    Lay_Right(ide_max50_component, ide_max50_system_ext)
    Lay_Right(ide_max50_system_ext, ide_max50_container_ext)
    Lay_Right(ide_max50_container_ext, ide_max50_component_ext)
    Lay_Up(ide_max50_system, internet_www_system)
}

Boundary(browser_max50, "browser_max50") {
    System(browser_max50_system, "browser_max50", "browser_max50", $sprite="browser_max50")
    System_Ext(browser_max50_system_ext, "browser_max50", "browser_max50", $sprite="browser_max50")
    Container(browser_max50_container, "browser_max50", "browser_max50", $sprite="browser_max50")
    Container_Ext(browser_max50_container_ext, "browser_max50", "browser_max50", $sprite="browser_max50")
    Component(browser_max50_component, "browser_max50", "browser_max50", $sprite="browser_max50")
    Component_Ext(browser_max50_component_ext, "browser_max50", "browser_max50", $sprite="browser_max50")
    Lay_Right(browser_max50_system, browser_max50_container)
    Lay_Right(browser_max50_container, browser_max50_component)
    Lay_Right(browser_max50_component, browser_max50_system_ext)
    Lay_Right(browser_max50_system_ext, browser_max50_container_ext)
    Lay_Right(browser_max50_container_ext, browser_max50_component_ext)
    Lay_Up(browser_max50_system, ide_max50_system)
}

Boundary(politie_max50, "politie_max50") {
    System(politie_max50_system, "politie_max50", "politie_max50", $sprite="politie_max50")
    System_Ext(politie_max50_system_ext, "politie_max50", "politie_max50", $sprite="politie_max50")
    Container(politie_max50_container, "politie_max50", "politie_max50", $sprite="politie_max50")
    Container_Ext(politie_max50_container_ext, "politie_max50", "politie_max50", $sprite="politie_max50")
    Component(politie_max50_component, "politie_max50", "politie_max50", $sprite="politie_max50")
    Component_Ext(politie_max50_component_ext, "politie_max50", "politie_max50", $sprite="politie_max50")
    Lay_Right(politie_max50_system, politie_max50_container)
    Lay_Right(politie_max50_container, politie_max50_component)
    Lay_Right(politie_max50_component, politie_max50_system_ext)
    Lay_Right(politie_max50_system_ext, politie_max50_container_ext)
    Lay_Right(politie_max50_container_ext, politie_max50_component_ext)
    Lay_Up(politie_max50_system, browser_max50_system)
}

Boundary(terraform, "terraform") {
    System(terraform_system, "terraform", "terraform", $sprite="terraform")
    System_Ext(terraform_system_ext, "terraform", "terraform", $sprite="terraform")
    Container(terraform_container, "terraform", "terraform", $sprite="terraform")
    Container_Ext(terraform_container_ext, "terraform", "terraform", $sprite="terraform")
    Component(terraform_component, "terraform", "terraform", $sprite="terraform")
    Component_Ext(terraform_component_ext, "terraform", "terraform", $sprite="terraform")
    Lay_Right(terraform_system, terraform_container)
    Lay_Right(terraform_container, terraform_component)
    Lay_Right(terraform_component, terraform_system_ext)
    Lay_Right(terraform_system_ext, terraform_container_ext)
    Lay_Right(terraform_container_ext, terraform_component_ext)
    Lay_Up(terraform_system, politie_max50_system)
}

'end-generated-plantuml

@enduml
```


---


<!--
id | 54964459
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/c2/plantuml" data-proofer-ignore>c2platform/c2/plantuml&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
