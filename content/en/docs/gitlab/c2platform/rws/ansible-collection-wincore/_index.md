---
categories: ['GitLab Projects', 'Ansible Collection']
description: "Ansible Collection for Windows hosts"
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-wincore
tags: ['ansible', 'c2platform', 'chocolatey', 'collection', 'core', 'windows']
title: "Ansible Collection - c2platform.wincore"
weight: 11
---

GitLab: <a href="https://gitlab.com/c2platform/rws/ansible-collection-wincore"><code>c2platform/rws/ansible-collection-wincore</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/rws/ansible-collection-wincore/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/commits/master)
[![Latest Release](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/releases)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.wincore-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/wincore/)

C2 Platform generic roles for MS Windows that are used by all or some other
roles. These roles typically don't create services / processes on target node
but are dependencies e.g. packages required by those roles. Or these roles help
with Ansible provisioning for example offers generic Ansible modules, filters
etc.

## Roles

* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/blob/master/roles/win" text="win" htmlproofer_ignore="false" >}} Manage MS Windows systems using more than 117+ Ansible
modules from the `ansible.windows`, `community.windows` and
`chocolatey.chocolatey` collection.
* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/blob/master/roles/download" text="download" htmlproofer_ignore="false" >}} manage downloads.
* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/blob/master/roles/ad" text="ad" htmlproofer_ignore="false" >}} Active Directory ( AD) management using
  {{< external-link url="https://docs.ansible.com/ansible/latest/collections/microsoft/ad/" text="microsoft.ad" htmlproofer_ignore="false" >}}
  Ansible collection.

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
{{< external-link url="https://galaxy.ansible.com/ui/repo/published/c2platform/wincore/docs/" text="Ansible Galaxy" htmlproofer_ignore="false" >}}
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.wincore
ansible-doc -t filter --list c2platform.wincore
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```



---


<!--
id | 46695745
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/rws/ansible-collection-wincore" data-proofer-ignore>c2platform/rws/ansible-collection-wincore&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
