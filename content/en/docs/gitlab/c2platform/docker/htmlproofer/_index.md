---
categories: GitLab Projects
description: "HTMLProofer is a set of tests to validate your HTML output. HTMLProofer is a Ruby Gem. This project creates a Docker image with this gem installed. The project was created to be able to validate the C2 Platform website using a current HTMLProofer version."
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: htmlproofer
tags: ['docker', 'htmlproofer', 'image', 'ruby']
title: "htmlproofer"
weight: 12
---

GitLab: <a href="https://gitlab.com/c2platform/docker/htmlproofer"><code>c2platform/docker/htmlproofer</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


---


<!--
id | 45803144
default_branch | main
web_url | <a href="https://gitlab.com/c2platform/docker/htmlproofer" data-proofer-ignore>c2platform/docker/htmlproofer&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
