---
categories: ["Slides"]
tags: [gitops, pipeline, ansible-inventory]
title: "GIS Platform GitOps Pipeline"
linkTitle: "GitOps Pipeline"
translate: false
draft: true
weight: 1
description: >
    Presentation of the GitOps pipeline for the GIS platform, including inventory strategy, Git branching, and Ansible automation.
---

<iframe src="https://c2platform.org/slides/oorae4Ko4i/gitops-pipeline/" width="600" height="400" style="border:none;"></iframe>
