---
title: "UWV TAM  ( 2022 to present )"
linkTitle: "UWV TAM"
translate: false
weight: 4
description: >
  UWV Technical Application Management ( TAM )
---

{{< under_construction >}}

|Category||Tools|
|-----------------------|----|-----------|
|Event-Driven Automation|    |           |
|Orchestration          |    | |
|Code Pipelines         |   | Azure DevOps [^1]   |
|Policy-As-Code         |   |Ansible CLI, |
|Configuration-As-Code  |   |Ansible CLI, Azure DevOps [^1]   |
|Infrastructure-As-Code |   |Ansible    |

[^1]: Self-hosted Azure DevOps.