---
title: "UWV ASC ( 2016 to 2018 )"
linkTitle: "UWV ASC"
translate: false
weight: 7
description: >
    At the Agile Systems Development Center (ASC) of UWV, it has been found that it should be ["open, unless."]({{< relref path="docs/concepts/oss" >}}) Otherwise, there is a risk of "accidental IP," with all the associated consequences.
---

{{< under_construction >}}

|Category||Tools|
|-----------------------|----|-----------|
|Event-Driven Automation|    |           |
|Orchestration          |    |           |
|Code Pipelines         |    |           |
|Policy-As-Code         |✔   |Chef       |
|Configuration-As-Code  |✔   |Chef       |
|Infrastructure-As-Code |✔   |Chef       |