---
title: "Projects"
linkTitle: "Projects"
translate: false
weight: 3
description: >
  An overview of projects from 2016 up to the present that utilized what C2
  Platform Project so some degree or the other. From the first tiny steps to the
  more advanced approach underway
---
