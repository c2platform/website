---
categories: ["Diagram", "Project"]
title: "C2 Platform ( 2022 to present )"
linkTitle: "C2"
translate: false
weight: 2
description: >
  C2 Platform
---

Projects:
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

---

```plantuml
@startuml system
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads
title [System Context] C2 Platform
AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

Person(c2_engineer, "C2 Ansible Engineer", "Open Source Contributor \nC2 Platform")
Person_Ext(site_engineer, "Ansible Site Engineer", "Create / maintains Ansible roles / collections")

Person_Ext(site_operator, "Ansible Site Operator", "")

System(ansible, "C2 Platform", "Open\nAutomation Platform")
System_Ext(gis, "GIS_LABEL", "Provides GEO information and processing services")
System_Ext(suwinet, "Suwinet", "Gegevensuitwisseling UWV, SVB en gemeenten")
System_Ext(cd, "Police CD Platform", "Continuous Delivery Platform voor 90+ Scrum teams of Dutch Police")

Rel_L(site_engineer, c2_engineer, "Dual role", "", $tags="optional")
Rel(c2_engineer, ansible, "Uses and\ncontributes to", "")
'Rel(site_engineer, ansible, "Uses and\ncontributes to", "")
Rel(site_operator, ansible, "Uses", "")

Rel(ansible, gis, "Automates service delivery and management", "Infrastructure-As-Code")
Rel(ansible, suwinet, "", "")
Rel(ansible, cd, "", "")
Rel(ansible, ansible, "Automates", "")

LAYOUT_WITH_LEGEND()
@enduml
```