---
categories: ["System"]
title: "System Diagram C2 Platform"
linkTitle: "System Diagram"
draft: true
translate: false
weight: 1
description: >
  C2 Platform
---

```plantuml
@startuml overview
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads

'System_Ext(rws_cert, "RWS CA", "Issues RWS SSL/TLS certificates")

AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

title [System] C2 Platform
Person(admin, "Ansible Operator")
System_Boundary(c2_platform, "C2 Platform") {
    Container(Galaxy_Container,"Galaxy\ngalaxy.ansible.com" , "website, ansible", "Host all Ansible content, roles, collections, modules")
    Container(vagrant_cloud, "Vagrant Cloud\napp.vagrantup.com", "vagrant,virtualbox,lxc", "Hosts all C2 Platform images")
    Container(laptop,"Developer Laptop" , "Dell Precision 7670,\n32GB RAM, 1TB SSD", "High-end developer laptop with Vagrant and Ansible and VS Code")

'    Container(Galaxy_Container,"" , "", $tags="")

    ' Container(linux, "RWS GIS CA", "Linux, RHEL9", "Issues, creates and handles\nSSL/TLS certificates, Java Keystores, etc.", $tags="")
    ' ContainerDb_Ext(cert_share, "Certificates", "Windows Share", "Stores all RWS and GIS CA certificates..") {
    ' }
    ' Container_Ext(ansible, "Ansible Controller", "AWX, Ansible Control Node", "Manages infrastructure automation with Ansible playbooks.")
}
System(GIS_System, "GIS_LABEL", "Delivers GEO information services")


' Rel_L(admin, cert_share, "Uploads certificates")
' Rel(ansible, linux, "Manages certificate tasks", "")
' Rel(ansible, GIS_System, "Deploys temporary GIS CA certificates")
' Rel(ansible, GIS_System, "Updates to RWS certificates", $tags="optional")
' Rel(linux, cert_share, "Accesses certificate storage", "CIFS")
' Rel(admin, rws_cert, "Requests certificates", "topdesk")
' Rel(rws_cert, admin, "Delivers\ncertificates", "email")

LAYOUT_WITH_LEGEND()
@enduml
```