---
title: "Bureau Keteninformatisering Werk en Inkomen ( BKWI ) ( 2020 to 2022)"
linkTitle: "BKWI"
translate: false
weight: 5
description: >
  At Bureau Keteninformatisering Werk en Inkomen (BKWI), there is a refinement of the approach and building upon the automation of the Police.
---

{{< under_construction >}}

|Category||Tools|
|-----------------------|----|-----------|
|Event-Driven Automation|✔   |Zabbix     |
|Orchestration          |✔   |Kubernetes |
|Code Pipelines         |✔   |GitLab     |
|Policy-As-Code         |✔   |Ansible, AWX [^1] |
|Configuration-As-Code  |✔   |Ansible, AWX |
|Infrastructure-As-Code |✔   |Ansible, AWX |

[^1]: Older, open source version van Ansible Automation Platform ( AAP )