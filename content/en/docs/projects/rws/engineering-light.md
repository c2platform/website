---
categories: ["Diagram", "PlantUML", "C4", "gis"]
title: Engineering with a "Pseudo" Development Environment
linkTitle: "Engineering Light"
translate: false
draft: false
weight: 4
description: >
  Engineering Ansible collections and roles in the RWS Domain/DC is termed as
  "engineering light" due to its use of a "pseudo" development environment.
  This setup poses specific challenges such as limited access to resets.
---

The Ansible development environment within the RWS Domain/DC is recognized as
a "pseudo" environment for two primary reasons. Firstly, performing resets and
snapshots of VMs is not straightforward. These actions require a formal request
to the infrastructure team via a TopDesk change request, which typically takes
a minimum of one day to address. For intensive Ansible engineering work, the
capability to reset the environment multiple times per day is vital.

Secondly, this environment is shared among various engineers, including those
not in the GIS Platform team. This can result in considerable periods of
unavailability, sometimes extending for a week or more.

```plantuml
@startuml rws-gis-engineering-light
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads
!include <office/Devices/workstation>
!include <material/laptop>

AddContainerTag("rhel_desktop", $sprite="workstation", $legendText="virtual-desktop")
AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

title [Container] GIS Platform Automation Engineering Light

Person_Ext(rws_infra, "RWS Infra Team", "Provides infrastructure services to RWS projects and system teams")
Person(rws_engineer_light, "RWS Ansible Engineer Light", "Similar to RWS Ansible Engineer but uses a pseudo development environment.")
System_Ext(azure_devops, "Azure DevOps\ndev.azure.com/Rijkswaterstaat", "Hosts Ansible projects for the GIS Platform")
System_Ext(gitlab, "GitLab\ngitlab.com/c2platform/rws", "Hosts all open source RWS projects for GIS Platform")
System_Ext(galaxy,"ANSIBLE_GALAXY_LABEL" , "ANSIBLE_GALAXY_DESC", $tags="Galaxy_System_Ext")

Boundary(aap_gis, "RWS DC", $type="domain") {
  Container(rhel_desktop, "Ansible Dev Desktop", "VDI, RHEL9, RDP, VS Code", $tags="rhel_desktop")
  Container(ansible, "Ansible", "" , "Ansible CLI")
  WithoutPropertyHeader()
  AddProperty("Environment", "LAB")
  Container(gis, "GIS Platform", "dc,vmware", "Provides GEO information and processing services", $tags="$arcgis")
  Boundary(git_local, "Git") {
    ContainerDb(ansible_collections, "GIS_COLLECTIONS_LABEL", "GIS_COLLECTIONS_TECH", "GIS_COLLECTIONS_DESC")
    ContainerDb(ansible_inventory, "GIS_INVENTORY_LABEL", "GIS_INVENTORY_TECH", "GIS_INVENTORY_DESC")
  }
}

Rel_U(ansible, gis ,"Provision and verify changes to Ansible roles and configuration", "")
Rel(rws_engineer_light,rhel_desktop, "Accesses using RDP-in-RDP connection the")
Rel(rhel_desktop, ansible, "Which has installed", "")
Rel(rws_infra, gis, "Reset VM's, create / restore snapshot", "VMWare")
Rel_L(rws_engineer_light, rws_infra, "Request reset / restore of LAB VM's / snapshots", "TopDesk")
Rel(ansible, ansible_collections, "Uses roles from various Ansible Collections","")
Rel(ansible, ansible_inventory, "Uses Ansible Inventory for LAB environment","")
Lay_R(ansible_inventory,ansible_collections)
Rel_L(git_local, azure_devops, "Push / pull changes", "Git, SSH or HTTPS")
Rel_R(ansible, galaxy, "Download\nAnsible content", "")
Rel_U(azure_devops, gitlab, "OSS Git repo synced / mirrored to", "Git,HTTPS", $tags="optional")

LAYOUT_WITH_LEGEND()
@enduml
```

## Additional Information

For additional insights and guidance:

* Discover the unique Ansible project types within the C2 Platform approach by
  visiting
  [Ansible Projects]({{< relref path="/docs/concepts/ansible/projects" >}}).
* RWS adopts an ["open, unless"]({{< relref path="/docs/concepts/oss" >}})
  policy, leading to the GIS Platform's Execution Environment (EE) being
  included in the GitLab Open Source Program. This enables access to
  [GitLab Ultimate]({{< relref path="/docs/concepts/gitlab" >}}).
* Understand the critical differences between operating (using) and engineering
  with Ansible
  [here]({{< relref path="/docs/concepts/ansible/engineering-vs-configuring" >}}).
* {{< rellink path="/docs/concepts/dev" desc=true >}}