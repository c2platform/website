---
categories: ["Diagram", "PlantUML", "C4", "gis"]
title: "RWS GIS Automation Platform"
linkTitle: "GIS Automation Platform"
draft: false
translate: false
weight: 1
description: >
  System context diagram for RWS GIS Automation Platform which is based on the C2
  Platform approach and runs within the RWS domain on the Red Hat Automation
  Platform (AAP).
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

---

```plantuml
@startuml context-aap
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads
'!define C4_NO_STEREOTYPE
AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

title [System Context] RWS GIS Automation Platform

Person(rws_operator, "Ansible Operator", "RWS GIS Platform engineer with basic Ansible knowledge")
Person(rws_engineer, "Ansible Engineer", "RWS GIS Platform engineer with Ansible knowledge")
Person(rws_engineer_light, "Ansible Engineer Light", 'Uses a "pseudo" dev environment within RWS domain.')
Person_Ext(rws_infra, "RWS Infra Team", "Provides infrastructure services to RWS projects and system teams")
Person(c2_engineer, "C2 Ansible Engineer", "Open Source Contributor \nC2 Platform")

'Person_Ext(site_engineer, "Ansible Site Engineer", "Create / maintains Ansible roles / collections")

System(gis_automation, "GIS_AUTOMATION_LABEL", "GIS_AUTOMATION_DESC")
System(gis, "GIS_LABEL", "Provides GEO information and processing services")

'System_Ext(azure_devops, "Azure DevOps\ndev.azure.com/Rijkswaterstaat", "Hosts Ansible projects of the GIS Platform")
'System_Ext(gitlab, "GitLab\ngitlab.com/c2platform/rws", "Hosts the open source RWS projects of the GIS Platform")

Rel_U(rws_engineer, c2_engineer, "is a", "", $tags="optional")
Lay_R(rws_operator,rws_engineer)
Lay_R(rws_engineer,rws_engineer_light)
Rel_U(rws_infra, gis_automation, "Delivers the platform to run automation on for", "RedHat Ansible Automation Platform (AAP),Ansible Controller (AWX), Ansible Automation Hub (Galaxy NG)")
Rel_U(rws_infra, gis, "VM's, network, LB's etc", "VMWare, NetScaler")
'Rel(rws_operator,azure_devops,"Maintains a complete definition of the GIS Platform in an Ansible Inventory project in", "Browser, Web Edit/IDE, Git Push and Pull")
'Rel(gis_automation, azure_devops, "1) Source Control Update and \n2) Inventory Sync", "https,git,vault")
'Rel(gis_automation, gitlab, "3) Pull Ansible Execution Environment (EE)", "docker image")
Rel_R(gis_automation, gis, "Automates service delivery and management", "Ansible, Infrastructure-As-Code")
Rel(rws_engineer, gis_automation, "Develops GIS Ansible collections and roles and a Ansible Execution (EE) for","C2 Dev Environment, Galaxy")
Rel(rws_engineer_light, gis_automation, "Develops GIS Ansible collections and roles", "Ansible,Citrix VDI → RHEL9 Desktop")
Rel(rws_operator, gis_automation, "Performs LCM and TAM tasks using the Ansible Inventory project", "Browser, Web Edit/IDE, Git Push and Pull")
Rel(rws_operator, gis_automation, "Uses Ansible Controller (AWX) to configure or launch jobs of","Browser")

LAYOUT_WITH_LEGEND()
@enduml
```
