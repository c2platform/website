---
categories: [Example]
title: "GitOps Pipeline for the GIS Platform"
tags: [DTAP, Promotion Model, GitOps, Ansible-Inventory]
linkTitle: "GitOps Pipeline"
translate: false
draft: true
weight: 8
description: >
    The [Group-based Environments]({{< relref path="/docs/guidelines/setup/group-based-environments" >}}) approach of the GIS Platform Inventory Project allows a pure GitOps approach for promoting and controlling changes in a controlled way using only Git Merges.
---

Projects:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

---



## Introduction

The C2 Platform approach for [Ansible Inventory Projects]({{< relref path="/docs/concepts/ansible/projects/inventory" >}}) supports a pure GitOps approach that allows promotion of changes using only Git Merges; it does not require the use of external merge tools[^1]. This is very convient because with this setup is becomes possible to promote changes using a web interface, using the functionality that tools like Azure DevOps and GitLab typically have. To create merge requests and introduce a workflow with approval processes.

## Branching Strategy

The Git diagram below visualizes the setup of an inventory project for the GIS Platform with three environments: `development`, `staging` and `production`. The setup shown is typical and nothing special, we see for example:

1. A release being created on `development` branch with tag `1.0.0`.
2. This tag is promoted / merged to `staging` and `production`.
3. This tag is then merged to the `main` branch. This makes it clear that the
   `main` branch is used for production-ready code, which is the recommended
   practice.
4. A hotfix branch `hotfix-12345-fix-something` is created for creating a hotfix
   for the production environment.
5. The hotfix is tagged `1.0.1-hotfix-issue-12345` and merged back to `main`,
   `development` and `staging`.

```mermaid
gitGraph
    commit id: "Initial commit"

    branch development
    checkout development

    branch staging
    checkout staging

    branch production
    checkout production

    checkout development
    commit
    commit
    commit id: " " tag: "1.0.0"
    checkout staging
    merge development tag: "1.0.0"
    checkout production
    merge staging tag: "1.0.0"
    commit id: "Production release 1.0.0"

    checkout main
    merge production tag: "1.0.0"

    checkout production
    branch hotfix-12345-fix-something
    commit id: "Hotfix applied" tag: "1.0.1-hotfix-issue-12345"

    checkout main
    merge hotfix-12345-fix-something  tag: "1.0.1-hotfix-issue-12345"

    checkout development
    merge hotfix-12345-fix-something tag: "1.0.1-hotfix-issue-12345"

    checkout staging
    merge hotfix-12345-fix-something tag: "1.0.1-hotfix-issue-12345"
```




[^3]

## Notes

[^1]: This is different from Ansible Recommended Approach link TODO
[^2]: There are two flavours at RWS for Ansible development: engineering and
    "light" engineering. The first utilizes the [open source development
    environment]({{< relref path="/docs/howto/dev-environment/setup" >}}). For
    engineering light a "pseudo" development environment inside the RWS domain
    is used. For additional information, refer to:

    * [Engineering the GIS Platform using Ansible]({{< relref
      path="/docs/projects/rws/engineering" >}})
    * [Engineering using a "pseudo" Development Environment]({{< relref
      path="/docs/projects/rws/engineering-light" >}})
    * [Development Environment]({{< relref path="/docs/concepts/dev" >}})