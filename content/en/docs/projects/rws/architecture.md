---
categories: ["Diagram", "Project"]
title: "RWS Architecture"
linkTitle: "RWS Architecture"
translate: false
draft: true
weight: 1
description: >
  Rijkswaterstaat ( RWS ) GIS Platform Architecture Diagram
---

```plantuml
@startuml system
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads
'!define C4_NO_STEREOTYPE
AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

title [Container] RWS GIS Platform

System_Boundary(gis, "GIS_LABEL") {
    Container(etl_server, "ETL Server", "FME", "TODO")
    Container(etl_desktop, "ETL Desktop", "FME", "TODO")
    Container(age_server, "GIS Server", "ArcGIS Server, ArcGIS Portal", "TODO")
}

Person(power_user, "Power User", "RWS user with\nexpert GIS Knowledge")
'Person(self, "Self Service User", "RWS user with\nbasic GIS Knowledge")
'Person(user, "User", "RWS user with\nlimited GIS knowledge")

'Person_Ext(external_user, "External User", "External users (contractors) with GIS knowledge")
'Person_Ext(citizen, "Citizen", "Users with\n no GIS knowledge")


'System(gis, "GIS_LABEL", "GIS_DESC")
'System(gis, "GIS_LABEL", "Provides GEO information and processing services")
'
'System_Ext(iam, "IAM", "Identity / Access Management", "")
'System_Ext(geo_ruimte, "Geo Data", "Geo data as services", "fddf")
SystemDb_Ext(file_service, "File Service", "FME Repository Storage and\nGeo data files", "fddf")
'SystemQueue_Ext(esb, "ESB", "External data sources")
System_Ext(apps, "RWS Applications", "Various RWS applications", "")
System_Ext(ansible, "AAP", "Ansible\nAutomation Platform")

Rel(power_user, etl_desktop, "Uses", "desktop,etl,gis")
Rel(etl_desktop, etl_server, "Uses")
Rel(etl_server, file_service, "TODO")

'
'Rel(power_user, gis, "Ensures data quality, using ETL processes for efficient data management and 'dissemination.", "desktop,https,rdp, etl")
'Rel(self, gis, "Collaborative field / offline work using GeoWeb and simple maps", "browser,https")
'Rel(user, gis, "Collaborative field / offline work using GeoWeb", "browser,https")
'Rel(citizen, gis, "Accesses maps via rws.nl or pdok.nl", "browser,https")
'Rel(external_user, gis, "Collaborative field / offline / project work, data collection", "browser,'https")
'
'Rel(gis, geo_ruimte, "Uses / consumes / transforms data from", "https, wcs")
'Rel(gis, esb, "(Provide) access to data sources", $tags="optional")
'Rel(gis, file_service, "Publish data for internal\nand external use", "CIFS/SMB")
''Rel_R(geo_ruimte, file_service, "Storage", "CIFS/SMB")
'Rel(gis, iam, "Uses for authentication / authorization")
'
Rel(apps, age_server, "Access Geo services, GeoWeb maps, ETL", "https,etl")
Rel(ansible, gis, "Automates service delivery and management", "Infrastructure-As-Code")


LAYOUT_WITH_LEGEND()
@enduml
```