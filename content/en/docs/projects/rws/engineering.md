---
categories: ["Diagram", "PlantUML", "C4", "gis"]
title: "Engineering the GIS Platform using Ansible"
linkTitle: "Engineering the GIS Platform"
translate: false
draft: false
weight: 3
description: >
  This diagram visualizes the Ansible engineering of Ansible content. Use the
  standard C2 Platform approach this is completely done in the open source domain utilizing
  local development and a open source "reference implementation" of the GIS Platform.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

---

```plantuml
@startuml rws-gis-engineering
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads
!include <office/Devices/workstation>
'!include <tupadr3/font-awesome-5/laptop>
!include <material/laptop>

AddContainerTag("rhel_desktop", $sprite="workstation", $legendText="virtual-desktop")
AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())


title [Container] GIS Platform Automation Engineering

'Person(rws_operator, "RWS Ansible Operator", "Performs LCM and TAM tasks for the GIS Platform using the Ansible Controller and Azure DevOps.")
Person(rws_engineer, "Ansible Engineer", "RWS GIS Platform engineer with Ansible knowledge")
Person(c2_engineer, "C2 Ansible Engineer", "Open Source Contributor \nC2 Platform")

'System_Ext(azure_devops, "Azure DevOps\ndev.azure.com/Rijkswaterstaat", "Hosts Ansible projects for the GIS Platform")
System_Ext(gitlab, "GitLab\ngitlab.com/c2platform/rws", "Hosts all open source RWS projects for GIS Platform")
System_Ext(vagrant_cloud, "Vagrant Cloud", "Hosts all C2 Platform VirtualBox and LXD Images")
System_Ext(galaxy,"ANSIBLE_GALAXY_LABEL" , "ANSIBLE_GALAXY_DESC", $tags="Galaxy_System_Ext")

Boundary(c2, "C2 Platform Open Source / Local Development", $type="domain") {
  WithoutPropertyHeader()
  AddProperty("Environment", "DEV")
  Container(gis_dev, "GIS Platform", "LXD, VirtualBox" , "Provides GEO information and processing services", $tags="$arcgis")
  note left : Open Source GIS Platform\n"Reference Implementation"
  Container(laptop, "Developer Laptop", "Ubuntu 22, VS Code, VirtualBox, LXD", "Key and integral part of the open C2 Platform approach", $tags="$laptop")
  'Container(git_local, "Git", "Git")
  Container(vagrant, "Vagrant + Ansible", "" , "VAGRANT_DESC")
  Boundary(git_local, "Git") {
    ContainerDb(ansible_collections, "GIS_COLLECTIONS_LABEL", "GIS_COLLECTIONS_TECH", "GIS_COLLECTIONS_DESC")
    ContainerDb(ansible_ee, "GIS_EE_LABEL", "GIS_EE_TECH", "GIS_EE_DESC")
    ContainerDb(ansible_inventory, "GIS_INVENTORY_LABEL", "GIS_INVENTORY_TECH", "GIS_INVENTORY_DESC_REF")
  }
}

Rel(rws_engineer,gis_dev,'Local development of a GIS Platform "reference implementation"', "")
Rel_R(gis_dev,laptop,"Which requires a", "")
Rel_L(vagrant, vagrant_cloud, "Downloads Windows, RedHat and Ubuntu images from", "HTTPS")
Rel_L(ansible_ee,gitlab,"Build and release EE", "CI/CD Pipeline")
Rel(laptop,vagrant,"With two key components installed", "")
Rel(gis_dev, vagrant, "Defined, created and maintained using")
Rel(vagrant, ansible_collections, "Uses roles from various Ansible Collections","")
Rel_D(ansible_collections, ansible_ee, "Packaged in","Docker Build")
Rel(vagrant, ansible_inventory, "Uses Ansible Inventory that defines the reference specification","")
Rel_L(git_local, gitlab, "Push / pull changes", "Git, SSH or HTTPS")
Rel_D(gitlab, galaxy, "Publish / release collections to", "CI/CD Pipeline")
Rel_R(rws_engineer, c2_engineer, "is a", "", $tags="optional")

LAYOUT_WITH_LEGEND()
@enduml
```

## Additional Information

For additional insights and guidance:

* The second key concept of C2 Platform approach the is the
  [Development Environment]({{< relref "/docs/concepts/dev" >}})
  which offers flexibility and productivity through local development. Which
  becomes possible because of the first key concept: the
  [Open, unless](/docs/concepts/oss) approach.
* Discover the unique Ansible project types within the C2 Platform approach by
  visiting
  [Ansible Projects]({{< relref path="/docs/concepts/ansible/projects" >}}).
* Understand the critical differences between operating (using) and engineering
  with Ansible
  [here]({{< relref path="/docs/concepts/ansible/engineering-vs-configuring" >}}).




