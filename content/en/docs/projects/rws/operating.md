---
categories: ["Diagram", "PlantUML", "C4", "gis"]
title: "Operating the GIS Platform using Ansible"
linkTitle: "Operating the GIS Platform"
translate: false
draft: false
weight: 2
description: >
  The diagram on this page visualizes how LCM and TAM tasks of the GIS platform are performed using Ansible. Two key components are the Ansible Inventory Project hosted by Azure DevOps and the Ansible Execution Environment ( EE ) hosted by GitLab.
---

Projects: [`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})

---

```plantuml
@startuml rws-gis-automation
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads

title [Container] GIS Platform Automation for Operations

AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

Person(rws_operator, "RWS Ansible Operator", "RWS GIS Platform engineer with basic Ansible knowledge")

System(gis, "GIS_LABEL", "Provides GEO information and processing services")
System_Ext(azure_devops, "Azure DevOps\ndev.azure.com/Rijkswaterstaat", "Hosts Ansible projects of the GIS Platform")
System_Ext(gitlab, "GitLab\ngitlab.com/c2platform/rws", "Hosts the open source RWS projects of the GIS Platform")

Boundary(aap_gis, "GIS Platform Automation") {
    ContainerDb(gis_inventory, "GIS_INVENTORY_LABEL", "GIS_INVENTORY_TECH", "GIS_INVENTORY_DESC")
    ContainerDb(gis_ee, "GIS_EE_LABEL", "GIS_EE_TECH", "GIS_EE_DESC")
    'ContainerDb(gis_collections, "Ansible Collections", "Ansible, Git")
  'Boundary(aap_rhel, "RedHat Ansible Automation Platform (AAP)") {
    Container_Ext(aap, "Automation Controller", "RedHat, K8s, AWX", "Manages nodes and deploys Ansible playbooks across environments, providing a user interface, REST API, and a task engine")
    Container_Ext(app_automation_hub, "Automation Hub", "RedHat, K8s, Galaxy NG", "Offers verified content\n(collections, roles, modules, plugins) to automate IT environments")
 '}
  'Container(software, "Software Server", "Linux, RHEL9, Ansible", "Hosts and serves all software installers, binaries and archives.")
  'Container(api_automation_server, "API Tasks Server", "Linux, RHEL9, Ansible, Python", "Linux server with suitable Python environment and Python library dependencies.")
  'Container(ca_server, "CA Server", "Linux, RHEL9, Ansible", "Issues,creates and handles SSL/TLS Certificates, Java Keystores, etc.")
}

'Lay(gitlab,gis)
'Lay(rws_operator, rws_engineer)
'Lay_R(azure_devops, gitlab)
'Rel(aap,software,"Downloads software\nfrom","https")
'Rel(aap,api_automation_server,"Delegate API automation to","ansible delegate_to")
'Rel(aap,ca_server,"Delegate certificate / Java KeyStore tasks to","Ansible delegate_to")

Lay_R(aap, app_automation_hub)
Rel(aap, gis_inventory, "On launch of a (scheduled) job:\n1) Source Control Update and \n2) Inventory Sync", "https,git,vault")
Rel(aap, gis_ee, "3) Download\nRWS GIS Ansible EE", "Docker pull")
Rel(aap, gis, "4) Deploy, update, upgrade services of")
'Rel_U(infra, aap, "Provides\nas a service RedHat Ansible Automation Platform (AAP)", "")
Rel(rws_operator,gis_inventory,"Performs LCM and TAM tasks by making changes to", "Browser, Web Edit/IDE, Git Push and Pull")
'Rel(c2_engineer,gitlab,"Maintains an open source Ansible Execution Environment ( EE )", "git push / pull")
'Rel_R(user,gis, "Accesses the GIS platform","")
'Rel-DR(user_ext,gis, "", "")
Rel(rws_operator, aap, "Execute jobs\nview logs etc.", "browser, https")
'Rel_L(rws_operator, c2_engineer, "Can have \na dual role as", "", $tags="optional")

Rel(gis_ee, gitlab, "Part of C2 Platform / GitLab Open Source Program", "GitLab Ultimate")
Rel(gis_inventory, azure_devops, "In RWS closed-source Git repository hosted by", "Git")

LAYOUT_WITH_LEGEND()
@enduml
```

## Additional Information

For additional insights and guidance:

* Discover the unique Ansible project types within the C2 Platform approach by
  visiting
  [Ansible Projects]({{< relref path="/docs/concepts/ansible/projects" >}}).
* RWS adopts an ["open, unless"]({{< relref path="/docs/concepts/oss" >}})
  policy, leading to the GIS Platform's Execution Environment (EE) being
  included in the GitLab Open Source Program. This enables access to
  [GitLab Ultimate]({{< relref path="/docs/concepts/gitlab" >}}).
* Understand the critical differences between operating (using) and engineering
  with Ansible
  [here]({{< relref path="/docs/concepts/ansible/engineering-vs-configuring" >}}).
