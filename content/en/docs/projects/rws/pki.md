---
categories: ["Example"]
tags: [ansible, certificates, pki, java, keystore, tomcat, sysprep]
title: "Implementing PKI for RWS GIS with Ansible"
linkTitle: "RWS GIS PKI Overview"
translate: false
weight: 5
description: >
   Learn how to automate the generation and management of RWS SSL/TLS certificates
   and Java KeyStores with Ansible, integrating manual processes effortlessly.
---

Projects:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

```plantuml
@startuml overview
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads

System_Ext(rws_cert, "RWS CA", "Issues RWS SSL/TLS certificates")

AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

title RWS Ansible PKI Design
Person(admin, "Ansible Operator")
    System_Boundary(gisBoundaryMngt, "Ansible Automation Platform (AAP)") {
        Container(linux, "RWS GIS CA", "Linux, RHEL9", "Issues, creates and handles\nSSL/TLS certificates, Java Keystores, etc.", $tags="")
        ContainerDb_Ext(cert_share, "Certificates", "Windows Share", "Stores all RWS and GIS CA certificates..") {
        }
        Container_Ext(ansible, "Ansible Controller", "AWX, Ansible Control Node", "Manages infrastructure automation with Ansible playbooks.")
    }
    System(GIS_System, "GIS_LABEL", "Delivers GEO information services")


Rel_L(admin, cert_share, "Uploads certificates")
Rel(ansible, linux, "Manages certificate tasks", "")
Rel(ansible, GIS_System, "Deploys temporary GIS CA certificates")
Rel(ansible, GIS_System, "Updates to RWS certificates", $tags="optional")
Rel(linux, cert_share, "Accesses certificate storage", "CIFS")
Rel(admin, rws_cert, "Requests certificates", "topdesk")
Rel(rws_cert, admin, "Delivers\ncertificates", "email")

LAYOUT_WITH_LEGEND()
@enduml
```

In line with common practices among Dutch government organizations, the
responsibility for SSL/TLS certificate creation is often delegated to a separate
unit. The typical procedure involves generating a Certificate Signing Request
(CSR), forwarding it to the designated unit via TopDesk, and receiving an official
RWS certificate in return via email.

Unfortunately, automation is often hindered in such scenarios due to the absence
of APIs.

This section outlines the PKI architecture for the RWS GIS Platform, emphasizing
the streamlined generation and management of SSL/TLS certificates and Java
KeyStores. The process is designed for full automation, minimizing manual
interventions.

While there are some manual steps involved, they are neither essential nor
impediments to provisioning a fully automated GIS Platform. These steps can be
independently performed to comply with security and compliance standards.

The approach employs Ansible's `community.crypto` collection to set up a simple
Certificate Authority (CA). This CA server is straightforward, with minimal
requirements, and is driven entirely by Ansible without any running processes.

For detailed guidance, see the
{{< external-link
url="https://docs.ansible.com/ansible/latest/collections/community/crypto/docsite/guide_ownca.html"
text="How to create a small CA — Ansible Documentation"
htmlproofer_ignore="true" >}}.
The `c2platform.core.cacerts2` role leverages the `community.crypto` collection
to implement the CA.

For hands-on exploration, refer to:
* [Tomcat SSL/TLS and Java Keystore and TrustStore Configuration for Linux and Windows Hosts]({{< relref path="/docs/howto/rws/certs" >}})
* [Create a Simple CA Server using Ansible]({{< relref path="/docs/howto/rws/ca" >}})

---

## Step 1: GIS CA Certificates

Below is a sequence diagram showing the automated provisioning of SSL/TLS
certificates and Java KeyStores for services like Tomcat on an FME Core server.
The process is generalizable to any service provisioning. The Tomcat role uses
the `c2platform.core.cacerts2` role from the `c2platform.core` collection to
delegate certificate and Java KeyStore creation to a CA Server.

Any Linux server can serve as the CA Server, with Linux preferred for its
comprehensive Ansible module support. This strategic choice centralizes
certificate management and issuance, enhancing security by isolating the CA's
private key from target machines.

The outcome is a system ready for operation with no manual steps required.

```plantuml
@startuml step1

!define ArrowColor #000000
!define BoxColor #A9DCDF
!define ActorColor #FFA500
!define ActorFontSize 14
!define BoxFontSize 14

actor "AAP" as AAP
participant "FME Core" as FME
database "CA Server" as OWNCA
actor "GIS Platform\nTeam" as GEO
actor "RWS CSP" as CSP

'autonumber
'IVP_TB_GEO->AAP: Delegeer beheer GIS\nPlatform beheer
AAP->FME: Provision Tomcat\n(c2platform.gis.tomcat)
FME->OWNCA: Delegate provision\n(c2platform.core.cacerts2)
OWNCA->OWNCA: Create OwnCA\nCSR, certificate,\nJava KeyStore
OWNCA-> FME: Java KeyStore
FME->FME: Configure HTTPS
@enduml
```

## Step 2: RWS Certificates

This sequence outlines the process of replacing temporary certificates with
official RWS certificates, complying with RWS security policies that require
HTTPS and RWS certificates for all communications.

```plantuml
@startuml step2

!define ArrowColor #000000
!define BoxColor #A9DCDF
!define ActorColor #FFA500
!define ActorFontSize 14
!define BoxFontSize 14

actor "AAP" as AAP
participant "FME Core" as FME
database "CA Server" as OWNCA
actor "GIS Platform\nTeam" as GEO
actor "RWS CSP" as CSP

'autonumber
OWNCA->GEO: <<scp>> CSR
GEO->CSP: <<email>> CSR
CSP->GEO: <<email>> RWS Certificate
GEO->OWNCA: <<scp>> RWS Certificate
AAP->FME: Provision Tomcat\n(c2platform.gis.tomcat)
FME->OWNCA: Delegate provision\n(c2platform.core.cacerts2)
OWNCA->OWNCA: Update Java KeyStore\nwith RWS Certificate
OWNCA-> FME: Java KeyStore
FME->FME: Configure HTTPS
@enduml
```

With the integration of RWS certificates, the platform achieves full compliance
and is ready for secure production deployment, facilitated entirely through
Ansible automation.
