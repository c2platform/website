---
title: "SZW Venus ( 2023 to present )"
linkTitle: "SZW Venus"
translate: false
weight: 3
description: >
  Ministerie van Social Zaken en Werkgelegenheid ( SZW )
---

{{< under_construction >}}

|Category||Tools|
|-----------------------|----|-----------|
|Event-Driven Automation|    |           |
|Orchestration          | ✔   | Rancher |
|Code Pipelines         | ✔  | GitLab ( OSS, self-hosted) [^1]  |
|Policy-As-Code         |   | |
|Configuration-As-Code  |   |    |
|Infrastructure-As-Code |   |    |

[^1]: self-hosted free / community edition