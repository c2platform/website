---
categories: ["Concept"]
tags: []
title: "Reference Implementations"
linkTitle: "Reference Implementations"
translate: false
weight: 3
description: >
  Unlock the power of the development environment and create fully
  integrated, comprehensive systems. With a few simple commands you can explore
  for example the automation driving deployment and management of a
  GIS Platform at Rijkswaterstaat (RWS).
---

In the realm of system engineering and infrastructure-as-code (IaC), the term
"reference implementation" transcends the status of a mere blueprint. It evolves
into a fully functional, seamlessly integrated system. This exemplar not only
serves as a model but also as a standardized, well-documented exemplar,
showcasing how to efficiently deploy and manage a specific system, with
[Ansible]({{< relref path="/docs/concepts/ansible" >}}) taking center stage.

Ansible, a linchpin of any
[automation]({{< relref path="/docs/concepts/automation" >}})
initiatives, plays a pivotal role in this context.

Within the context of the C2 Platform, a reference implementation conjoins two
pivotal components: the
[Ansible Inventory Project]({{< relref path="/docs/concepts/ansible/projects/inventory" >}})
and the
[Vagrant Project]({{< relref path="/docs/concepts/dev/vagrant" >}}).
The synergy of Vagrant and Ansible is particularly potent, offering a holistic
solution. Vagrant orchestrates the creation of virtual machines, including
network configurations and disk allocations, while Ansible takes charge of
installing and managing various nodes and virtual machines. For instance, in the
case of the GIS Platform, this encompasses deploying components like ArcGIS
Enterprise Web Adaptor, Portal, Server, Datastore, FME, and more.

As depicted in the diagram below, the C2 Platform engineer can effortlessly
construct complete environments via Vagrant, capitalizing on various cloud
services:

1. [Vagrant Cloud]({{< relref path="/docs/concepts/dev/vagrant/vagrant-cloud" >}})
   houses C2 images compatible with LXD, VirtualBox, and more.
1. [GitLab.com]({{< relref path="/docs/concepts/gitlab" >}})
   serves as the repository for all C2 Projects, offering a myriad of resources.
1. [Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}) functions as
   the repository for open-source automation roles and collections, including
   those designed for the C2 Platform in the realm of GIS.

This all is made possible through an
["open, unless"]({{< relref path="/docs/concepts/oss" >}}) approach.
This approach permits the exploration and test-driving of the GIS Platform,
mirroring its deployment at the RWS Data Center, by leveraging its open-source
counterpart reference implementation.

In the context of an open-source Ansible project, a reference implementation
stands as a prime example project, offering insights into how [Ansible
Collections]({{< relref path="/docs/concepts/ansible/projects/collections" >}})
and [Ansible Roles]({{< relref path="/docs/concepts/ansible/projects/roles" >}})
are configured within an
[Ansible Inventory Project]({{< relref path="/docs/concepts/ansible/projects/inventory" >}}),
often referred to as a playbook project. This configuration equips users to deploy a fully functional
system. The reference implementation doesn't merely serve as an abstract
reference but as a practical framework, openly accessible for similar
deployments by clients and organizations.

Let's delve into the core attributes of this concept:

1. **Comprehensive Implementation:** A reference implementation encapsulates the
   entirety of a functional and integrated system. It meticulously covers all
   configurations, settings, and interdependencies required for seamless
   operation. This comprehensiveness assures that the reference implementation
   mirrors the intended system in entirety.
1. **Parity with Production:** For instance, the GIS Platform Reference
   Implementation serves as the open-source counterpart to the GIS Platform
   within Rijkswaterstaat (RWS). This implies that it closely emulates the
   production environment, ensuring alignment with real-world setups.
1. **Detailed Documentation and Best Practices:** Similar to conventional
   reference implementations, a complete reference implementation features
   extensive documentation. This documentation elucidates the system's purpose,
   configurations, best practices, and management guidelines. It stands as an
   invaluable resource for team members involved in deploying and maintaining
   the system.
1. **Training and Consistency:** The reference implementation also serves as a
   vital resource for training and learning, aiding team members in grasping how
   to efficiently deploy and manage the entire system. Additionally, it enforces
   consistency within your infrastructure, mitigating the risk of errors or
   misconfigurations.

In the context of Ansible and Vagrant, a comprehensive reference implementation
may encompass Ansible playbooks for system configuration and management, along
with Vagrantfiles delineating the VM infrastructure. These resources are
thoroughly documented and rigorously tested to ensure they faithfully represent
the target system.

To sum up, a reference implementation is not a mere sketch but a fully
functional, fully integrated system. It guarantees efficient and consistent
management of infrastructure components while aligning with the reality of
production environments. This is particularly significant in scenarios like the
GIS Platform Reference Implementation, where it aims to replicate an existing
system within organizations such as Rijkswaterstaat (RWS).

The purpose of the reference implementation extends to elucidating how software
can be employed and configured, serving as a launching pad for customizing and
configuring software to meet specific requirements. By leveraging open-source
building blocks, like Ansible roles, organizations can leverage the work of
others, expediting software development and implementation. This promotes
uniformity in deployments and fosters collaborative efforts among organizations
employing the same software.

An exemplar of a reference implementation is the
[`ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" lang="en" >}})
configuration and playbook project for the Rijkswaterstaat GIS Platform. This
project facilitates the local deployment of a comprehensive, functional
environment for the Rijkswaterstaat GIS Platform. To accomplish this, the
project leverages Ansible collections such as
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" lang="en" >}})
and
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" lang="en" >}}).

```plantuml
@startuml ri-gis-platform
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads

LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()

Person(engineer, "Engineer")

Boundary(laptop, "high-end developer laptop", $type="ubuntu-22") {
  System(gsd, "GIS_LABEL", "GIS_DESC", "", $tags="GIS_System") {
  }
  Container(vagrant, "Vagrant", "", $tags="Vagrant_Container")
  Container(ansible_local, "Ansible", "", $tags="Ansible_Container")
}

System_Ext(galaxy_website, "ANSIBLE_GALAXY_LABEL", "ANSIBLE_GALAXY_DESC", $tags="Ansible_System_Ext")
System_Ext(gitlab, "GITLAB_LABEL", "GITLAB_DESC", $tags="Gitlab_System_Ext") {
   Container_Ext(gis_inventory, "GIS_REF_IMPL_LABEL", "GIS_REF_IMPL_DESC", $tags="GIS_Container_Ext")
}
System_Ext(vagrant_cloud, "VAGRANT_CLOUD_LABEL", "VAGRANT_CLOUD_DESC", $tags="Vagrant_System_Ext")

Rel(ansible_local, gsd, "Provision ArcGIS Portal, WebAdaptor, Server, Datastore, FME, etc", "")
Rel(vagrant, gsd, "Create VM's\nnetwork, disks etc", "")
Rel(engineer, vagrant, "Create VM's, provision services using Vagrant CLI", "")
Rel_D(vagrant, ansible_local, "", "")
Rel_R(ansible_local, galaxy_website, "Download GIS automation / collection", "")
Rel(ansible_local, gitlab, "", "")
Rel_R(vagrant, vagrant_cloud, "Download Vagrant \nboxes ( images )", "")
@enduml
```
