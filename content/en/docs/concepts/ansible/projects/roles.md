---
categories: ["Term"]
tags: [ansible, galaxy, ansible-galaxy, collection, role]
title: "Ansible Role Project ( deprecated )"
linkTitle: "Role"
translate: false
weight: 10
description: >
    An Ansible Role project is a structured and reusable collection of tasks, variables, and configurations that provide specific functionality.
---

{{< alert title="Note:" >}}Nowadays, roles are often distributed via [Ansible Collections]({{< relref path="./collections" >}}). Collections are a later addition to Ansible and are beneficial because we often produce many smaller roles. Without collections, we would have to develop separate Git repositories, pipelines, and so on for each individual role.{{< /alert >}}

An Ansible role is a structured and reusable unit within the Ansible framework. It groups and manages tasks, variables, and configurations in an organized manner, making complex system configurations easier. A typical role structure includes directories such as "tasks" for tasks, "defaults" for default variable values, and optionally other directories such as "handlers" and "templates". With roles, system administrators and developers can make configurations consistent and repeatable, reduce errors, and simplify the management of complex systems.

Roles can be combined and integrated into playbooks to build complex system configurations. This allows configurations to be easily maintained and extended as system requirements change. The use of roles promotes reusability, consistency, and scalability of configurations, enabling system administrators and developers to work more efficiently and improve configuration management automation.
