---
categories: ["Concept"]
tags: [lxd, laptop, vagrant]
title: LXD
linkTitle: Linux Containers ( LXD )
translate: false
weight: 3
description: >
    Lightweight and agile, LXD offers swift virtualization of VMs, combining the
    benefits of traditional VMs with the agility of Docker containers.
---


