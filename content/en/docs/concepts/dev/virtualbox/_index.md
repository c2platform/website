---
categories: ["Concept"]
tags: [laptop, ubuntu, vagrant, virtualbox]
title: VirtualBox
linkTitle: VirtualBox
translate: false
weight: 4
description: >
  In cases where LXD is not feasible, VirtualBox offers a reliable alternative. It
  provides pre-configured VM images, including options for Microsoft Windows
  targets, ensuring compatibility across diverse environments.
---


[LXD]({{< relref path="../lxd/" >}}) is the default virtualization technique for
the development environment, but in cases where LXD is not available, such as MS
Windows, VirtualBox images are also available in the
[Vagrant Cloud]({{< relref path="../vagrant/vagrant-cloud.md" >}}).
