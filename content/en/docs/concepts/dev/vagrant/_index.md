---
categories: ["Concept"]
tags: [vagrant, ansible]
title: Vagrant
linkTitle: Vagrant
translate: false
weight: 2
description: >
  Vagrant provides a user-friendly platform for creating and managing simple local
  development environments, streamlining the setup process.
---

Vagrant is a tool developed by HashiCorp for building and managing virtual
machine environments in a single workflow. It provides an easy-to-use workflow
and focuses on automation, lowering setup time for development environments and
increasing production parity.

Here's how Vagrant is commonly used:

1. **Configuration Management**: Vagrant allows you to create and configure
   lightweight, reproducible, and portable development environments. It uses a
   simple configuration file (called Vagrantfile) to describe the type of
   machine you want, the software that needs to be installed, and how they
   should be configured.
1. **Provisioning**: Vagrant can automatically install software on the virtual
   machine as soon as it's created, using provisioning tools like shell scripts,
   Ansible, Chef, or Puppet. This is especially useful for setting up complete
   development environments.
1. **Provider Agnostic**: While Vagrant primarily works with VirtualBox, it supports
   other providers such as VMware, AWS, and others. This flexibility allows you
   to run your virtual machines on different providers using the same
   configuration.
1. **Networking**: Vagrant includes easy-to-configure, built-in support to setup
   network interfaces to access your virtual machine more easily from your host
   machine or from other machines on the same network.
1. **Synced Folders**: Vagrant syncs your project files to the virtual machine,
   allowing you to continue working on your project's files on your host
   machine, but use the resources in the virtual machine to compile or run your
   project.
1. **Portable Work Environments**: Vagrant boxes are completely disposable. If
   something goes wrong, you can destroy and recreate the environment in minutes
   without any worry.

Vagrant is particularly popular among software developers, system
administrators, and DevOps engineers because it aids in creating a consistent
environment that can be shared across teams, helping to mitigate the "it works
on my machine" syndrome.

* {{< external-link url="https://www.vagrantup.com/" text="Vagrant" htmlproofer_ignore="true" >}}
* See the C2 example project [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" lang="en" >}}) for an example of a Vagrant project using [LXD]({{< relref "../lxd" >}} "Concept: Linux Containers (LXD)"), [VirtualBox]({{< relref "../virtualbox" >}} "Concept: VirtualBox"), and [Ansible]({{< relref "../../ansible" >}} "Concept: Ansible Automation Platform").
* {{< external-link url="https://docs.ansible.com/ansible/latest/scenario_guides/guide_vagrant.html" text="Vagrant Guide - Ansible Documentation" htmlproofer_ignore="true" >}}
