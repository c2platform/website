---
categories: ["Book"]
tags: [ansible, devops, geerlingguy, vagrant]
title: Ansible for DevOps
linkTitle: Ansible for DevOps
translate: false
weight: 1
description: >
  Dit boek, Ansible for DevOps, vormt een technische basis voor de aanpak van het C2 Platform.
---

Het boek [Ansible for DevOps](https://www.ansiblefordevops.com/) van Jeff Geerling technische vormt een technische basis voor de aanpak van het C2 Platform, met Vagrant en Ansible.

> Vagrant is an excellent tool ... to mimic real-world infrastructure locally (or in the cloud), and ... Ansible is an excellent tool for provisioning servers, managing their configuration, and deploying applications, even on my local workstation!

[![Ansible for DevOps](ansible-for-devops.png?width=200px#float-end)](https://www.ansiblefordevops.com/ "Ansible for DevOps")
