---
categories: ["Concept"]
tags: [vagrant, image]
title: Vagrant Cloud
linkTitle: Vagrant Cloud
translate: false
weight: 2
description: >
  C2 Platform images for LXD, VirtualBox with Ubuntu, RedHat Enterprise Linux and Windows 2022 Server.
---

C2 Platform images are published and distributed using the {{< external-link url="https://app.vagrantup.com/c2platform/" text="Vagrant Cloud" htmlproofer_ignore="true" >}}
