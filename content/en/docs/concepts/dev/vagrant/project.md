---
categories: ["Term"]
tags: [vagrant, vagrantfile, vagrantfile.yml]
title: "Vagrant Project"
linkTitle: "Vagrant Project"
translate: false
weight: 2
description: >
  The C2 Platform offers a optimized, flexible and generic approach for utilizing Vagrant.
---

In context of the C2 Platform approach, a Vagrant project is also a
[Ansible Reference Implementation Project]({{< relref path="/docs/concepts/ansible/projects/refimpl" >}}).

As a Vagrant project C2 Platform uses a generic `Vagrantfile` that is
complemented with a YAML file `Vagrantfile.yml` which defines the development
environment nodes, network etc.

Numerous optimizations and enhancements have been made to the C2 Vagrant setup
over the years. Notably, the Sysprep of Windows nodes and the
subscription/registration of Red Hat Enterprise Linux nodes are now fully automated.
For more details, please refer to the relevant documentation:
* [Streamlining RHEL Registration and Subscription Automation]({{< relref path="/docs/guidelines/dev/rhel" >}}).
* Automation of Sysprep of Windows nodes see
  [Install Vagrant and Vagrant Plugins]({{< relref path="/docs/howto/dev-environment/setup/vagrant" >}}).

For examples of such Vagrant project see:

* [`rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}): RWS GIS Platform Reference Implementation.
* [`ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}): C2 Platform Reference Implementation.
