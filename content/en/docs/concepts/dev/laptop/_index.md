---
categories: ["Concept"]
tags: [laptop, ubuntu]
title: High-end Developer Laptop
linkTitle: Laptop
translate: false
weight: 1
description: >
  An advanced developer laptop running Ubuntu 22.04 serves as the foundation for
  the local development environment. Its capabilities empower engineers to tackle
  demanding tasks efficiently.
---

The [open approach]({{< relref "../../oss" >}} "Concept: ODM / OSS") and the advanced and comprehensive [software development platform]({{< relref "../../gitlab" >}} "Concept: GitLab Ultimate") are combined with a local development environment running on a powerful developer laptop with Ubuntu 22. This is typically achieved by using an additional laptop alongside the official company laptop. For example, a Dell Precision 7670 with a minimum of 32 GB of RAM is highly suitable for replicating complete environments.

An official company laptop is rarely suitable because it typically runs on MS Windows and is often heavily secured, making Ansible engineering challenging.

Of course, other Linux distributions can also be used. However, the [instructions]({{< relref "../../../howto" >}} "Instructions") are always based on Ubuntu 22.04. For instance, the [instruction for setting up]({{< relref path="/docs/howto/dev-environment" lang="en" >}}
"How-to: Set Up a Development Environment on Ubuntu 22") the laptop is specifically focused on Ubuntu 22.

<br/>

![Dell precision 7670](laptop-ubuntu-2.png?width=300px)
![Dell precision 7670](laptop-cgi.png?width=200px)

<!-- TODO RWS-121 On Windows, Git does not handle files in the working tree larger than 4
gigabytes. -->
