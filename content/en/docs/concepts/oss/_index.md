---
categories: ["Concept"]
tags: [odm, oss, concept]
title: "Open, unless"
linkTitle: "Open, unless"
translate: false
weight: 1
description: >
  An open approach facilitates the reuse of ideas and code, not only within projects but even across different organizations.
---

Open Source is more than a license; it is also a way of working. Adopting an
"open" approach is crucial in the context of automation projects for the Dutch
government to achieve the necessary productivity and flexibility.

An open approach within automation projects not only promotes the sharing of
code and ideas within a project but also between different organizations. This
enhances collaboration and enables working together on common challenges and
solutions.

Within the Dutch government, open source software (OSS) plays a crucial role in
fostering an open approach. By utilizing open source software, organizations can
benefit from existing solutions, contribute to the community, and collaborate on
the development of robust and reliable systems.

The C2 Platform, as an open automation platform for the Dutch government,
supports and encourages the use of open source software and an open approach. By
sharing knowledge and experiences, fostering collaboration, and promoting code
reuse, the platform contributes to the growth and innovation of automation
projects across the government.

With an open approach, automation projects can evolve from
[Infrastructure-as-Code to Event-driven Automation]({{< relref path="/docs/concepts/automation" >}}),
enabling the Dutch government to systematically automate and improve its IT
service delivery and management. The result is a more efficient and flexible IT
infrastructure that can better adapt to changes and meet the needs of the
citizens and organizations served by the government.

