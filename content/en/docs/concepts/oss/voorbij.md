---
categories: ["Reference"]
tags: [odm, oss, bzk]
title: "Open Source Work: Beyond Voluntarism"
linkTitle: "BZK Open Source Strategy Report"
translate: false
weight: 3
description: >
    Report on the open source strategy of the Ministry of the Interior and Kingdom Relations (BZK).
---

{{< external-link
url="https://www.digitaleoverheid.nl/nieuws/opensourcewerken-de-vrijblijvendheid-voorbij/"
text="Opensourcewerken: de vrijblijvendheid voorbij ( Dutch )"
htmlproofer_ignore="false" >}}
