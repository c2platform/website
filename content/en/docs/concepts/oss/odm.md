---
categories: ["Reference"]
tags: [odm, oss]
title: "Avoiding Abandon-Ware: Getting to Grips with the Open Development Method"
linkTitle: "Avoiding Abandon-Ware"
translate: false
weight: 3
description: >
    The Open Development Method: a collaborative and community-driven approach
    to software development, and its potential impact on education, emphasizing the
    need for long-term commitment and adaptation to new practices.
---

The article discusses the concept of the Open Development Method (ODM) in the
context of open source software development and its potential implications for
education. It emphasizes that ODM focuses on transparency, collaboration, and
community involvement, moving beyond licensing and distribution concerns. The
article also highlights the challenges and opportunities ODM presents for
education, suggesting that it could revolutionize the way knowledge products are
created and shared.

{{< external-link url="http://oss-watch.ac.uk/resources/odm"
text="Avoiding Abandon-Ware: Getting to Grips with the Open Development Method"
htmlproofer_ignore="false" >}}
