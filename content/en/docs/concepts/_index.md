---
title: "Concepts"
linkTitle: "Concepts"
weight: 3
translate: false
description: >
  Overview of the most important concepts of the platform.
---
