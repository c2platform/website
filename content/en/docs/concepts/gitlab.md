---
categories: ["Concept"]
tags: [gitlab, concept, oss]
title: "GitLab Ultimate"
linkTitle: "GitLab Ultimate"
translate: false
weight: 6
description: >
  Unlimited and unrestricted usage of an advanced software development platform
  for all disciplines, from version control to project management and CI/CD
  workloads.
---

The C2 Platform is part of the
{{< external-link url="https://about.gitlab.com/solutions/open-source/join/" text="GitLab Open Source Program" htmlproofer_ignore="true" >}},
which means C2 Platform projects have standard access to GitLab Ultimate.

GitLab Ultimate provides developers and teams with a comprehensive set of tools
and features to streamline and optimize the software development, testing, and
deployment process. GitLab Ultimate is an enhanced version of the GitLab
software development platform, offering additional features and capabilities not
available in the standard version. Some of the key features of GitLab Ultimate
include:

1. **Security**: GitLab Ultimate offers advanced security features, such as
   integrated security scanning and access rights management.
2. **DevOps**: GitLab Ultimate includes a complete DevOps platform, enabling
   teams to work on software development, testing, and deployment from a single
   interface.
3. **High availability**: GitLab Ultimate provides high availability, ensuring
   the platform is always accessible to developers and their teams.
4. **Analytics**: GitLab Ultimate offers powerful analytics features that allow
   teams to gain insights into their performance and identify areas for
   improvement.
5. **Kubernetes**: GitLab Ultimate includes advanced integrations with
   Kubernetes, making it easy to manage and deploy container applications.
