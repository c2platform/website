---
categories: ["Example"]
tags: [rws, ansible, inventory, gitlab, azure-devops, git, project]
title: "Ansible Projects"
linkTitle: "Ansible Projects"
translate: false
weight: 6
description: >
    Ansible Inventory projects and Ansible Collection projects used RWS.
---

RWS uses the following [Ansible Inventory and Collection Projects]({{< relref path="/docs/guidelines/setup/git" >}} "Guideline: Ansible Inventory and Collection Projects")


| OSS | RWS ( private ) | Type |
| ------ | ------ | ------ |
| {{< external-link url="https://gitlab.com/c2platform/rws/ansible-gis" text="ansible-gis" htmlproofer_ignore="false" >}} [![pipeline status](https://gitlab.com/c2platform/rws/ansible-gis/badges/master/pipeline.svg)](https://gitlab.com/c2platform/rws/ansible-gis/-/commits/master) | | Ansible Inventory GIS Platform|
| | {{< external-link url="https://dev.azure.com/Rijkswaterstaat/IVP-TB-Geo/_git/ansible-gis" text="ansible-argcis" htmlproofer_ignore="true" >}} [![Build Status](https://dev.azure.com/Rijkswaterstaat/IVP-TB-Geo/_apis/build/status%2Fansible-gis?branchName=master)](https://dev.azure.com/Rijkswaterstaat/IVP-TB-Geo/_build/latest?definitionId=60&branchName=master) |  Ansible Inventory GIS Platform |
| {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis" text="c2platform.gis" htmlproofer_ignore="false" >}} [![pipeline status](https://gitlab.com/c2platform/rws/ansible-collection-gis/badges/master/pipeline.svg)](https://gitlab.com/c2platform/rws/ansible-collection-gis/-/commits/master) [![Latest Release](https://gitlab.com/c2platform/rws/ansible-collection-gis/-/badges/release.svg)](https://gitlab.com/c2platform/rws/ansible-collection-gis/-/releases)| {{< external-link url="https://dev.azure.com/Rijkswaterstaat/IVP-TB-Geo/_git/ansible-collection-gis" text="c2platform.gis" htmlproofer_ignore="true" >}} [![Build Status](https://dev.azure.com/Rijkswaterstaat/IVP-TB-Geo/_apis/build/status%2Fansible-collection-gis?branchName=master)](https://dev.azure.com/Rijkswaterstaat/IVP-TB-Geo/_build/latest?definitionId=57&branchName=master)  | Ansible Collection for GIS Platform |
| {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-wincore" text="c2platform.wincore" htmlproofer_ignore="false" >}} [![pipeline status](https://gitlab.com/c2platform/rws/ansible-collection-wincore/badges/master/pipeline.svg)](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/commits/master) [![Latest Release](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/badges/release.svg)](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/releases)  | {{< external-link url="https://dev.azure.com/Rijkswaterstaat/IVP-TB-Geo/_git/ansible-collection-wincore" text="c2platform.wincore" htmlproofer_ignore="true" >}} [![Build Status](https://dev.azure.com/Rijkswaterstaat/IVP-TB-Geo/_apis/build/status%2Fansible-collection-wincore?branchName=master)](https://dev.azure.com/Rijkswaterstaat/IVP-TB-Geo/_build/latest?definitionId=59&branchName=master) | Ansible Collection for MS Windows|
| {{< external-link url="https://gitlab.com/c2platform/rws/ansible-execution-environment" text="ansible-execution-environment" htmlproofer_ignore="false" >}} [![Pipeline Status](https://gitlab.com/c2platform/rws/ansible-execution-environment/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/rws/ansible-execution-environment/-/commits/master) [![Latest Release](https://gitlab.com/c2platform/rws/ansible-execution-environment/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/rws/ansible-execution-environment/-/releases) | | Ansible Execution Environment |
