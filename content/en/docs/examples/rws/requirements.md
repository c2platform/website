---
title: "Installing Collections with a Requirements File"
linkTitle: "Collections Requirements File"
translate: false
categories: ["Example"]
tags: [ansible, collection, aap, awx, requirements.yml, ansible-galaxy]
weight: 6
description: >
   An Ansible requirements file allows you to install multiple collections with a single command, and this file can also be used by AAP/AWX.
---

An [Ansible playbook project]({{< relref path="/docs/concepts/ansible/projects/inventory" >}}) such as [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) typically contains a requirements file `collections\requirements.yml`.  The example content for this file is shown below:

```yaml
---
collections:
  - name: ansible.windows
    version: 1.14.0
  - name: community.windows
    version: 1.13.0
  - name: https://gitlab.com/c2platform/rws/ansible-collection-gis.git
    type: git
    version: 1.0.1
  - name: https://gitlab.com/c2platform/rws/ansible-collection-wincore.git
    type: git
    version: master
```

Based on the given Ansible requirements file, Ansible will perform the following actions:
1. Install the collection `ansible.windows` with version `1.14.0`.
1. Install the collection `community.windows` with version `1.13.0`.
1. Clone the Git repository `https://gitlab.com/c2platform/rws/ansible-collection-gis.git` and install the collection with version `1.0.1`.
1. Clone the Git repository `https://gitlab.com/c2platform/rws/ansible-collection-wincore.git` and install the collection from the `master` branch.


Ansible will execute these actions when the command `ansible-galaxy collection install -r collections/requirements.yml -p .` is run. The `-r` flag specifies the requirements file to be used, and the `-p` flag specifies the path where the collections will be installed (in this case, the current directory).

```bash
ansible-galaxy collection install -r collections/requirements.yml -p .
```

Moreover, this file can also be used by [Ansible Automation Platform ( AAP ) or AWX]({{< relref path="/docs/concepts/ansible/aap" >}}). For more information see {{< external-link url="https://docs.ansible.com/ansible/latest/collections_guide/collections_installing.html#install-multiple-collections-with-a-requirements-file" text="Installing collections — Ansible Documentation" htmlproofer_ignore="false" >}}
