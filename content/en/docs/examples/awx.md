---
categories: ["Example"]
tags: [awx, tower, redhat, automation, platform, ansible, ri1]
title: "AWX"
linkTitle: "AWX"
translate: false
weight: 2
description: >
  AWX met Ansible
---

{{< under_construction >}}

  Red Hat Automation Platform

```plantuml
@startuml awx-example

!includeurl https://gitlab.com/dburet/journal/raw/master/plantuml.cfg

rectangle "c2platform.org"  << dmz >>  as lan {
    rectangle "<$reverse_proxy>\nReverse Proxy\nc2d-rproxy1" << serveur >> as ReverseProxy
    rectangle "<$web_server>\nAWX\nc2d-awx" as AWX
}

rectangle "<$users>Users"  as users

users --> ReverseProxy << https >> : https://awx.c2platform.org
ReverseProxy --> AWX << https >>
@enduml
```
