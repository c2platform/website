---
categories: ["How-to"]
tags: ["how-to", "gitlab"]
title: "GitLab"
linkTitle: "GitLab"
translate: false
weight: 2
description: >
  Create an GitLab instance with container registry and a GitLab Runner.
---
