---
categories: ["How-to"]
tags: [linux, vscode, laptop, ansible, git]
title: "Setting Up VS Code: Extensions and Tips"
linkTitle: "VS Code"
translate: false
weight: 4
description: >
  Learn how to setup VS Code including recommended VS Code extensions for Ansible engineering tasks.
---

## Extensions

Enhance your Ansible development experience in VS Code with these recommended
extensions:

### Linter

{{< external-link url="https://marketplace.visualstudio.com/items?itemName=fnando.linter" text="Linter" htmlproofer_ignore="false" >}}
by Nando Vieira. This extension supports linters for various languages,
including YAML.

### Trailing Spaces

{{< external-link url="https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces" text="Trailing Spaces" htmlproofer_ignore="false" >}}
by Shardul Mahadik. Visualizes trailing spaces by highlighting them in red. To
configure this extension to automatically remove extra spaces when saving:

1. Press <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>P</kbd> and select **Preferences: Open User Settings**.
2. Search for `trailing spaces`.
3. Enable **Trim on Save**.

### Markdown All in One

{{< external-link url="https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one" text="Markdown All in One" htmlproofer_ignore="false" >}}
by Yu Zhang. A comprehensive suite for writing Markdown, offering keyboard
shortcuts, table of contents generation, auto-preview, and more.

### Git Graph

{{< external-link url="https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph" text="Git Graph" htmlproofer_ignore="false" >}}
by mhutchie. View and manage your repository with a visual git graph. Easily
perform Git actions such as cherry-picking directly from the graph.

### Git Commit (No Verify)

{{< external-link url="https://marketplace.visualstudio.com/items?itemName=DevEscalus.git-commit-no-verify-action-button" text="Git commit(no verify)" htmlproofer_ignore="false" >}}
by DevEscalus. Skip the `pre-commit` hook and perform a "no-verify" commit. To
use this feature, enable it in settings after installation.

{{< alert title="Note:" >}}
You can also perform a "no-verify" commit using the Git command line:

```bash
git commit --no-verify
```

{{< /alert >}}

### Black Formatter

{{< external-link url="https://marketplace.visualstudio.com/items?itemName=ms-python.black-formatter" text="Black Formatter" htmlproofer_ignore="false" >}}
by Microsoft. Automatically formats Python code adhering to a standard style.
Enable Black Formatter by adding the following to **JSON User Settings**:

```json
    "[python]": {
        "editor.formatOnType": true,
        "editor.defaultFormatter": "ms-python.black-formatter",
        "editor.formatOnSave": true
    },
```

## Tips

### Multiple Cursor Selection

- <kbd>Alt</kbd> + <kbd>Click</kbd>: Create a new cursor at the click location.
- <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Up/Down</kbd>: Add a new cursor above or
  below the current cursor.
- <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>L</kbd>: Create cursors at all
  occurrences of the selected text.
- <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Shift</kbd> + <kbd>I</kbd>: Create
  cursors at the start of each line of the selected text.
- <kbd>Ctrl</kbd> + <kbd>D</kbd>: Select the next occurrence of selected text
  and create a cursor. Repeat to continue selecting further occurrences.
- <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>Click</kbd>: Add an additional cursor
  at the click location for simultaneous editing.

You can also use the command palette (<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>P</kbd>)
and type "Create cursor" to explore more options. For Mac users, substitute
<kbd>Ctrl</kbd> with <kbd>Cmd</kbd> in the shortcuts listed.

### Fold / Unfold

- <kbd>Ctrl</kbd> + <kbd>K</kbd> followed by <kbd>Ctrl</kbd> + <kbd>0</kbd>: Fold
- <kbd>Ctrl</kbd> + <kbd>K</kbd> followed by <kbd>Ctrl</kbd> + <kbd>J</kbd>:
  Unfold

### Font Size Adjustment

Adjust font size with <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>+</kbd> and
<kbd>Ctrl</kbd> + <kbd>-</kbd>.

### Comment / Uncomment

Toggle comments with <kbd>Ctrl</kbd> + <kbd>/</kbd>.

### Environment Variables

To run VS Code with specific local environment variables, create a `.env` file
with your desired settings. Instruct VS Code to use this file by adding the
following line in `.vscode/settings.json`:

```json
{
    "python.envFile": "${workspaceFolder}/.env"
}
```

Ensure that the `.env` file is ignored by Git by including it in your `.gitignore`:

```text
.env
```
