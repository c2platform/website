---
categories: ["How-to"]
tags: ["how-to"]
title: "Using Ansible without Vagrant"
linkTitle: "Ansible without Vagrant"
weight: 15
translate: false
description: >
    Vagrant is the default but you can also use Ansible directly if you prefer.
---

Projects: [`c2platform/ansible`]({{< relref
path="/docs/gitlab/c2platform/ansible" >}})

---
This how-to describes how you can use [Ansible]({{< relref
path="/docs/concepts/ansible" >}}) without [Vagrant]({{< relref
path="/docs/concepts/dev/vagrant" >}}) in this project. This only requires a
little bit of SSH configuration in `.ssh/config`. The added bonus is that you
can SSH into boxes without Vagrant. In this setup the `c2d-rproxy1` node starts
fulfilling a third role as a {{< external-link
url="https://en.wikipedia.org/wiki/Jump_server" text="bastion / jump server"
htmlproofer_ignore="false" >}}.

---

## Overview

There are several ways to utilize [Ansible]({{< relref
path="/docs/concepts/ansible" >}}) within the [`c2platform/ansible`]({{< relref
path="/docs/gitlab/c2platform/ansible" >}}) project. These different approaches
are illustrated in the PlantUML diagram below. The default and most efficient
method is to employ [Vagrant]({{< relref path="/docs/concepts/dev/vagrant" >}}),
represented by the black line. Vagrant acts as a driver for Ansible through its
{{< external-link
url="https://developer.hashicorp.com/vagrant/docs/provisioning/ansible"
text="Ansible Provisioner" htmlproofer_ignore="false" >}}.

Another option we are currently exploring, depicted by the red line, is to use
Ansible directly without Vagrant.

Additional options for utilizing Ansible include:

* Green Line: Utilizing the `c2d-xtop` node, which can be created (using
  Vagrant) by executing `vagrant up c2d-xtop`. Once created, you can connect to
  it using an RDP or SSH connection by executing `vagrant ssh c2d-xtop`.
* Blue Line: Employing [AWX]({{< relref path="/docs/concepts/ansible/aap.md"
  >}}). For more information, refer to the guide on setting up AWX: [Setup
  AWX]({{< relref path="/docs/howto/awx/awx" >}} "How-to: Setup AWX").

```plantuml
@startuml ansible-vagrant
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

AddRelTag("red", $lineColor="red")
AddRelTag("blue", $lineColor="blue")
AddRelTag("green", $lineColor="green")

Person(engineer, "")

Boundary(laptop, "laptop", $type="ubuntu-22") {
  Boundary(mgmt, "mgmt", $type="") {
    Container(ansible, "Ansible", "c2d-xtop")
    System(c2d_awx_k8s, "Kubernetes", "") {
        Container(c2d_awx1, "AWX", "c2d-awx1")
    }
  }
  Boundary(gis, "apps", $type="") {
    Container(rproxy2, "Reverse Proxy", "c2d-rproxy2")
    Container(rproxy, "Reverse Proxy", "c2d-rproxy1")
  }
  Container(vagrant, "Vagrant", "")
  Container(ansible_local, "Ansible", "")
}

Rel(ansible_local, rproxy, "Provision", "ssh", $tags="red")
Rel(ansible_local, rproxy2, "Provision", "winrm", $tags="red")
'Rel(ansible, ansible, "Provision", "ssh")
Rel(engineer, ansible, "Remote Desktop", "ssh\nrdp", $tags="green")
Rel(engineer, c2d_awx1, "AWX Webinterface", "https", $tags="blue")
Rel(engineer, vagrant, "Vagrant", "")
Rel_R(vagrant, ansible_local, "", "")
Rel(engineer, ansible_local, "Ansible", "", $tags="red")
'Rel_R(ansible_local, ansible, "Provision", "ssh", $tags="red")
@enduml
```

## SSH config

Edit `.ssh/config` and add entry shown below. This allows access to *all* nodes
using SSH hops via `c2d-rproxy1`.

```
Host c2d-*
  ProxyCommand ssh 1.1.4.205 -W %h:%p
  User vagrant
  IdentityFile ~/.vagrant.d/insecure_private_key
  StrictHostkeyChecking no
  UserKnownHostsFile /dev/null
  LogLevel INFO
  Compression yes
  ServerAliveInterval 10
  ServerAliveCountMax 10
```

## Verify

Start `c2d-rproxy1`, `c2d-rproxy2` and SSH into the node. Note: that we are
using `ssh` and not `vagrant ssh`, so we are bypassing Vagrant altogether.

```bash
vagrant up c2d-rproxy1 c2d-rproxy2
ssh c2d-rproxy1
ssh c2d-rproxy2
```

Now you should also be able to run Ansible directly, without Vagrant, for
example with command similar to below.

```bash
source ~/.virtualenv/uwd/bin/activate
export ANSIBLE_CONFIG=$PWD/ansible-dev.cfg
ansible-playbook plays/mw/reverse_proxy.yml -i hosts-dev.ini --limit c2d-rproxy2
```

## Example

See [Manage the RWS Ansible Execution Environment]({{< relref
path="/docs/howto/rws/exe-env" >}}) for a practical example for which this is
useful.
