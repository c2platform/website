---
categories: ["How-to"]
tags: [ansible, vault, ansible-vault]
title: "Ansible Vault"
linkTitle: "Ansible Vault"
translate: false
weight: 2
description: >
  Securely manage secrets using Ansible Vault.
---

Projects: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---
This guide will help you effectively utilize Ansible Vault to protect sensitive information and securely manage secrets within your Ansible projects.

---

## Development

In C2 Platform Ansible configuration/playbook projects, the default approach is to use Ansible Vault. In the *development* environment, which is based on [Vagrant]({{< relref path="/docs/concepts/dev/vagrant" >}}),using Ansible Vault is not strictly necessary since the configuration/playbook project for development is typically open source with no real secrets as part of the [ODM / OSS]({{< relref path="/docs/concepts/oss" >}}) approach. However, it is still recommended. Refer to the [Secrets]({{< relref path="/docs/guidelines/setup/secrets" >}} "Guideline: Secrets") for more information.

Examples can be found in the Ansible configuration/playbook projects: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}).

When using Vagrant, a default file named `vpass` is automatically created with the contents `secret` ( see the `Vagrantfile` for details). The `vpass` file is then used by Ansible. The configuration for this behavior is set in the `ansible.cfg`file with the following line:

```yaml
vault_password_file=vpass
```

To edit the vault, use the following command:

```bash
ansible-vault edit secret_vars/development/main.yml
```

## Other environments

In other environments than *development* the `vpass` file is of course not used. In these environments, the secrets and their configuration are managed using the [Red Hat Automation Platform (AAP) or AWX]({{< relref path="/docs/concepts/ansible/aap" >}}) web interface.

The command to edit the vault remains the same. From the root of the Ansible configuration/playbook, run the following command:

```bash
ansible-vault edit secret_vars/<environment>/main.yml
```

## Ansible Vault CLI

To create, decrypt, edit, or view vaults, you can access the Ansible Vault CLI help by running the command:

```bash
ansible-vault -h
```
