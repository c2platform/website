---
categories: ["How-to"]
tags: [linux, ubuntu, ssh]
title: "Remote SSH sessions"
linkTitle: "remote SSH sessions"
translate: false
draft: true
weight: 15
description: >
  TODO
---

For SSH access without continuous passphrase prompting during remote SSH sessions, use:

```bash
eval `ssh-agent -s`
ssh-add
```