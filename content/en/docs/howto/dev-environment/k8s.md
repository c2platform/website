---
categories: ["How-to"]
tags: [kubernetes, ide, kubectl, extension]
title: "Connecting to a Kubernetes Cluster"
linkTitle: "Connect to Kubernetes"
translate: false
weight: 5
description: >
    Learn how to connect to a Kubernetes cluster for efficient management and development.
---

Projects: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

---

## Prerequisites

Before you can connect to a cluster, ensure you have a Kubernetes cluster, such
as one running on `c2d-ks1`, `c2d-awx1`, or `c2d-galaxy1` in place. For detailed
instructions on setting up these clusters, refer to the following resources:

* [Setup the Automation Controller ( AWX ) using Ansible]({{< relref path="/docs/howto/awx/awx" >}})
* [Setup the Automation Hub ( Galaxy NG) using Ansible]({{< relref path="/docs/howto/awx/galaxy" >}})
* [Setup Kubernetes]({{< relref path="/docs/howto/kubernetes/kubernetes" >}})

## Create `~/.kube/config` File

To connect to a Kubernetes cluster, you need to create a `~/.kube/config` file. Here's an example of how to do this:

```bash
sudo snap install kubectl --classic
export BOX=c2d-awx1
mkdir --parents ~/.kube
vagrant ssh $BOX -c "microk8s config" > ~/.kube/config
```

## Verify the Connection

To ensure that you've successfully connected to the cluster, you can use any
`kubectl` command. For example, run the following command to verify your
connection:

```bash
kubectl get all --all-namespaces
```

## Visual Studio Code Integration

If you use Visual Studio Code, you can enhance your Kubernetes cluster management experience by installing the
{{< external-link
url="https://marketplace.visualstudio.com/items?itemName=ms-kubernetes-tools.vscode-kubernetes-tools"
text="Visual Studio Code Kubernetes Tools"
htmlproofer_ignore="false" >}}
. With this extension, you can seamlessly connect to and manage your Kubernetes cluster directly from Visual Studio Code.
