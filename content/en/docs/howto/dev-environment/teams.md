---
categories: ["How-to"]
tags: [teams, microsoft, ubuntu]
title: "MS Teams on Ubuntu 22"
linkTitle: "MS Teams"
translate: false
weight: 30
description: >
  Use MS Teams on Ubuntu 22 for collaborative work.
---

If you want to use MS Teams on Linux (specifically Ubuntu 22), please note that
Microsoft no longer officially supports MS Teams on Linux. However, there is a
workaround to access MS Teams by utilizing a supported browser such as Chromium.
Please be aware that Firefox is not a supported browser and certain
functionalities like screen sharing may not work properly.

To access MS Teams on Ubuntu 22:

* Install Chromium browser on your Ubuntu 22 system.
* Launch Chromium and navigate to the MS Teams web version (https://teams.microsoft.com).
* Log in to your MS Teams account and access the desired features and functionalities available in the web version.
