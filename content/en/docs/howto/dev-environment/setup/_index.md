---
categories: ["How-to"]
tags: [linux, ubuntu, laptop, lxd, vagrant, ansible, grub, virtualbox]
title: "Setup a Development Environment on Ubuntu 22"
linkTitle: "Setup"
translate: false
weight: 1
description: >
  Install Ansible, Vagrant, LXD, Virtualbox and clone the project directories.
---

---
This how-to provides instructions for configuring your development environment
on your laptop. It is designed for individuals using Ubuntu 22.04 as their host
operating system. To make it easier to navigate, the how-to pages are
thoughtfully ordered in descending order of importance. This allows you to focus
on the most crucial configurations first, ensuring a solid foundation for your
development environment.

---

{{< alert type="warning" title="Compatibility Issues between LXD, Docker, and CRC" >}}
If you install Docker or CRC or have it already installed LXD containers
will fail to obtain an IP address and the Ansible provisioning will fail.

<details>
  <summary>Read more...</summary>
</br>

Please be aware that there are known compatibility conflicts between LXD,
Docker, and CRC. If you have Docker or CRC installed, or if you plan to install
them, please note that LXD containers may encounter difficulties in acquiring an
IP address, leading to potential failures in Ansible provisioning.

To ensure a smooth operation, it is advised to take the following precautions:

1. If you already have Docker or CRC installed, consider temporarily disabling
   or uninstalling them before working with LXD containers and Ansible
   provisioning.
1. If you intend to install Docker or CRC alongside LXD, please be prepared for
   potential networking issues with LXD containers. Extra steps might be
   required to resolve these conflicts.
1. Consider using `podman` and `podman-compose` instead of `docker` and
   `docker-compose`. Podman does not conflict with LXD.

By carefully managing the installation and configuration of LXD, Docker, and
CRC, you can minimize the chances of IP address conflicts and guarantee a
successful provisioning process using Ansible.

Please proceed with caution and refer to the respective documentation of each
tool for further guidance on resolving compatibility issues.

</details>

{{< /alert >}}
