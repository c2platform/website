---
categories: ["How-to"]
tags: [gitlab, ssh]
title: "Create SSH Keys and Set Up a GitLab Account"
linkTitle: "GitLab Account & SSH Keys"
weight: 2
translate: false
description: >
    Learn how to generate SSH keys for securely accessing C2 Platform GitLab repositories and set up a GitLab account.
---

{{< alert title="Note:" >}}
If you are not planning to contribute changes as a C2 Platform engineer and don't require write access to C2 repositories, you can skip this step and use HTTPS to access repositories instead.
{{< /alert >}}

## Create SSH Keys

To securely access GitLab repositories, you need to generate a pair of SSH keys (public and private) using the command below. It's advisable to use a passphrase for added security.

```bash
ssh-keygen -t rsa -b 4096 -C "tony.clifton@dev.c2platform.org"
```

## Create a GitLab Account

1. Visit {{< external-link url="https://gitlab.com" text="gitlab.com" htmlproofer_ignore="true" >}} and register for an account.
2. Add your SSH public key: navigate to user **Settings** and go to {{< external-link url="https://gitlab.com/-/profile/keys" text="SSH Keys" htmlproofer_ignore="true" >}}.

## Verify SSH Setup

To ensure that your SSH setup is configured correctly, run the following commands:

```bash
cd /tmp
git clone git@gitlab.com:c2platform/ansible.git
```
