---
categories: ["How-to"]
tags: [ubuntu, grub, visudo, root, lxd]
title: "Ubuntu Configuration Guide"
linkTitle: "Ubuntu Guide"
translate: false
weight: 1
description: >
  Learn how to disable Secure Boot, configure Grub for LXD support, and enable root access without requiring a password on Ubuntu systems.
---

This guide provides instructions for configuring Grub to support [LXD]({{< relref path="../../../concepts/dev/lxd/" >}}) and enabling root access without requiring a password. While enabling root access without a password is not strictly necessary, it can be helpful for convenience, especially on a development laptop.

## Secure Boot

Ensure that **Secure Boot** is disabled on your system. This is often necessary for custom kernel parameters and modules.

## Grub

Grub is a bootloader that manages the boot process of the operating system. Configuring Grub with specific options is necessary for certain applications like LXD to function correctly.

{{< alert title="Note:" >}}
This step is required only for Ubuntu 22.04. It is not necessary for Ubuntu 18.04.
{{< /alert >}}

Edit the `/etc/default/grub` file using a text editor.

```bash
sudo nano /etc/default/grub
```

Locate the line starting with `GRUB_CMDLINE_LINUX` and update it to:

```bash
GRUB_CMDLINE_LINUX="cgroup_enable=memory swapaccount=1 systemd.unified_cgroup_hierarchy=0"
```

Save your changes and update Grub to apply them:

```bash
sudo update-grub
```

Reboot your system to ensure the changes take effect:

```bash
sudo reboot
```

Without these modifications, CentOS 7 boxes might not network correctly, and containers may fail to obtain an IP address. If you encounter an error stating:

> The container failed to acquire an IPv4 address within 30 seconds.

Then this Grub configuration change is necessary.

## Enabling Root Access without Password

Root access without password prompt can simplify tasks requiring administrative rights.

Add your user to the sudoers file with no password requirement:

```bash
echo "$USER ALL=(ALL) NOPASSWD:ALL" |  sudo tee -a /etc/sudoers.d/$USER > /dev/null
```

While this eases access for tasks needing root privileges, be cautious and understand the security implications of allowing passwordless sudo access.