---
categories: ["How-to"]
tags: [linux, virtualbox, kernel, gurumeditation, ubuntu, vagrant]
title: "How to Downgrade the Kernel on Ubuntu 22.04"
linkTitle: "Kernel Downgrade"
translate: false
weight: 2
description: >
  Learn how to downgrade the kernel on Ubuntu 22.04 to resolve compatibility issues with VirtualBox.
---

VirtualBox on Ubuntu can sometimes become incompatible with the latest kernel
after an update. When this happens, you can either upgrade Ubuntu, VirtualBox,
and Vagrant, or simply downgrade the kernel.

---

## Symptoms

### Vagrant

Vagrant operations on VirtualBox VMs fail with the following message:

{{< alert >}}
The guest machine entered an invalid state while waiting for it to boot. Valid
states are 'starting, running'. The machine is in the 'gurumeditation' state.
Please verify everything is configured properly and try again.
{{< /alert >}}

<details>
  <summary><kbd>Show Me</kbd></summary>

  ```bash
  :ansible-gis]└2 development(+200/-13) ± vagrant up $BOX
  Bringing machine 'gsd-agserver1' up with 'virtualbox' provider...
  ==> gsd-agserver1: Checking if box 'c2platform/win2022' version '0.1.0' is up to date...
  ==> gsd-agserver1: Clearing any previously set forwarded ports...
  ==> gsd-agserver1: Clearing any previously set network interfaces...
  ==> gsd-agserver1: Preparing network interfaces based on configuration...
      gsd-agserver1: Adapter 2: hostonly
      gsd-agserver1: Adapter 1: nat
  ==> gsd-agserver1: Forwarding ports...
      gsd-agserver1: 5985 (guest) => 55985 (host) (adapter 1)
      gsd-agserver1: 5986 (guest) => 55986 (host) (adapter 1)
      gsd-agserver1: 22 (guest) => 2222 (host) (adapter 1)
  ==> gsd-agserver1: Running 'pre-boot' VM customizations...
  ==> gsd-agserver1: Booting VM...
  ==> gsd-agserver1: Waiting for machine to boot. This may take a few minutes...
  The guest machine entered an invalid state while waiting for it
  to boot. Valid states are 'starting, running'. The machine is in the
  'gurumeditation' state. Please verify everything is configured
  properly and try again.

  If the provider you're using has a GUI that comes with it,
  it is often helpful to open that and watch the machine, since the
  GUI often has more helpful error messages than Vagrant can retrieve.
  For example, if you're using VirtualBox, run `vagrant up` while the
  VirtualBox GUI is open.

  The primary issue for this error is that the provider you're using
  is not properly configured. This is very rarely a Vagrant issue.
  ```
</details>

### VirtualBox

In the VirtualBox Manager, VMs won't start, and the logs provide little
information other than showing that the VM suddenly stopped working.

### Ubuntu

Ubuntu will start generating crash reports containing lines similar to:

{{< alert >}} virtualbox-dkms 6.1.48-dfsh-1~ubuntu1.22.02.1: virtualbox kernel
module failed to build {{< /alert >}}

## Rollback Kernel

If the problem arises after an automatic kernel upgrade, the old kernel is still
available. You can configure it as the default kernel using the `GRUB_DEFAULT`
option in `/etc/default/grub` with the following steps:

1. Run update Grub to list all available kernels:
   ```bash
   sudo update-grub
   ```
    <details>
    <summary><kbd>Show me</kbd></summary>

    ```bash
    [@mpc2:ansible-gis]└2 development(+200/-13) 1 ± sudo update-grub
    Sourcing file `/etc/default/grub'
    Sourcing file `/etc/default/grub.d/init-select.cfg'
    Generating grub configuration file ...
    Found linux image: /boot/vmlinuz-6.8.0-40-generic
    Found initrd image: /boot/initrd.img-6.8.0-40-generic
    Found linux image: /boot/vmlinuz-6.5.0-41-generic
    Found initrd image: /boot/initrd.img-6.5.0-41-generic
    Memtest86+ needs a 16-bit boot, that is not available on EFI, exiting
    Warning: os-prober will not be executed to detect other bootable partitions.
    Systems on them will not be added to the GRUB boot configuration.
    Check GRUB_DISABLE_OS_PROBER documentation entry.
    Adding boot menu entry for UEFI Firmware Settings ...
    done
    ```
    </details>

2. Run the following command to get the exact line to use in
   `/etc/default/grub`:
   ```bash
   grep 'menuentry \|submenu ' /boot/grub/grub.cfg | cut -f2 -d "'"
   ```
   For example, it might output:
   ```
   Ubuntu, with Linux 6.5.0-41-generic
   ```
    <details>
    <summary><kbd>Show me</kbd></summary>

    ```bash
    [@mpc2:ansible-gis]└2 development(+200/-13) ± grep 'menuentry \|submenu ' /boot/grub/grub.cfg | cut -f2 -d "'"
    Ubuntu
    Advanced options for Ubuntu
    Ubuntu, with Linux 6.8.0-40-generic
    Ubuntu, with Linux 6.8.0-40-generic (recovery mode)
    Ubuntu, with Linux 6.5.0-41-generic
    Ubuntu, with Linux 6.5.0-41-generic (recovery mode)
    UEFI Firmware Settings
    ```
    </details>

3. Change the `GRUB_DEFAULT` option in `/etc/default/grub` from:
   ```
   GRUB_DEFAULT=0
   ```
   to:
   ```
   GRUB_DEFAULT="Advanced options for Ubuntu>Ubuntu, with Linux 6.5.0-41-generic"
   ```
4. Update Grub again:

   ```bash
   sudo update-grub
   ```

5. Reboot the machine to start using the new kernel:
   ```bash
   reboot now
   ```

6. Verify the current kernel:
   ```bash
   uname -r
   ```

## Install Older Kernel

If the problem arises on a new Ubuntu installation and it has never worked
correctly, you can install an older kernel. For example, for Ubuntu 22.04, the
kernel "Ubuntu, with Linux 6.5.0-41-generic" works.

For instructions on how to download and install the kernel, refer to: {{<
external-link
url="https://askubuntu.com/questions/1404722/downgrade-kernel-for-ubuntu-22-04-lts"
text="Downgrade kernel for Ubuntu 22.04 LTS - Ask Ubuntu"
htmlproofer_ignore="false" >}}.
