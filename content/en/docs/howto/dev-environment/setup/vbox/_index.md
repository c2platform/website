---
categories: ["How-to"]
tags: [vagrant, hashicorp, virtualbox, clipboard, drag-and-drop]
title: "Install VirtualBox"
linkTitle: "VirtualBox"
translate: false
weight: 6
description: >
  Install VirtualBox, configure Host-Only Networking, and ensure Vagrant synced folders work flawlessly.
---

Projects: [`c2platform/gis/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---

## Install VirtualBox

VirtualBox is required for certain nodes in this project, especially for MS
Windows nodes where LXD is not an option. You can install it on Ubuntu with the
following commands:

```bash
sudo apt-get install virtualbox virtualbox-guest-utils virtualbox-guest-x11
```

## Allowing Any IP Addresses

To enable VMs to use network ranges other than `192.168.56.0/21`, create a file
called `/etc/vbox/networks.conf`.

```bash
sudo mkdir /etc/vbox
sudo nano /etc/vbox/networks.conf
```

Add the following line. For more details,
refer to the
{{< external-link url="https://www.virtualbox.org/manual/ch06.html#network_hostonly" text="6.7. Host-Only Networking" htmlproofer_ignore="false" >}}.


```
* 0.0.0.0/0 ::/0
```

<details>
  <summary><kbd>Show me</kbd></summary>

```
==> c2d-iis1: Clearing any previously set network interfaces...
The IP address configured for the host-only network is not within the
allowed ranges. Please update the address used to be within the allowed
ranges and run the command again.

  Address: 1.1.8.146
  Ranges: 192.168.56.0/21

Valid ranges can be modified in the /etc/vbox/networks.conf file. For
more information including valid format see:

  https://www.virtualbox.org/manual/ch06.html#network_hostonly
```

</details>

## Configuring Synced Folders

On Ubuntu 22.04, ensure that "synced folders" work correctly by adding the
following line to your `~/.bashrc` file:

```bash
export VAGRANT_DISABLE_VBOXSYMLINKCREATE=0
```

<details>
  <summary><kbd>Show me</kbd></summary>

  ```bash
Vagrant is currently configured to create VirtualBox synced folders with
the `SharedFoldersEnableSymlinksCreate` option enabled. If the Vagrant
guest is not trusted, you may want to disable this option. For more
information on this option, please refer to the VirtualBox manual:

  https://www.virtualbox.org/manual/ch04.html#sharedfolders

This option can be disabled globally with an environment variable:

  VAGRANT_DISABLE_VBOXSYMLINKCREATE=1

or on a per folder basis within the Vagrantfile:

  config.vm.synced_folder '/host/path', '/guest/path', SharedFoldersEnableSymlinksCreate: false
  ```
</details>

## Shared Clipboard

For better performance and usability, consider installing
{{< external-link url="https://www.virtualbox.org/manual/ch04.html" text="VirtualBox Guest Additions" htmlproofer_ignore="true" >}}
on
{{< external-link url="https://developer.hashicorp.com/vagrant/docs/boxes" text="Vagrant boxes" htmlproofer_ignore="true" >}}
based on VirtualBox images. If you're using a project like
[`c2platform/gis/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
 where the box
{{< external-link url="https://app.vagrantup.com/c2platform/boxes/win2016/versions/0.1.1" text="c2platform/win2016" htmlproofer_ignore="true" >}}
is used, VirtualBox Guest Additions are already installed.

If VirtualBox Guest Additions are installed, you can enable the shared clipboard
by installing the **VirtualBox Extension Pack** with the following commands:

```bash
export VBOX_VERSION=$(vboxmanage --version | cut -d'_' -f1)
cd /opt
sudo wget https://download.virtualbox.org/virtualbox/$VBOX_VERSION/Oracle_VM_VirtualBox_Extension_Pack-$VBOX_VERSION.vbox-extpack
sudo VBoxManage extpack install Oracle_VM_VirtualBox_Extension_Pack-$VBOX_VERSION.vbox-extpack
sudo systemctl restart virtualbox.service
```

To enable shared clipboard functionality, follow these steps:

1. Stop the VM(s) by running `vagrant halt`.
2. Open VirtualBox Manager and navigate to **File** → **Preferences** → **Extensions**.
3. If you have the **Oracle VM VirtualBox Extension Pack** installed, select it and uninstall it.
4. Select **Add New Package** and locate the extension file at `/opt/Oracle_VM_VirtualBox_Extension_Pack-$VBOX_VERSION.vbox-extpack`, then install it.
5. Start the VM(s).

{{< alert type="warning" title="Warning!" >}}
These steps need to be repeated
each time you create a new VM. For a newly created VM, drag-and-drop
functionality will work, but the shared clipboard won't. This behavior has been
observed with VirtualBox version 6.1.38 on Ubuntu 22.04.
{{< /alert >}}
