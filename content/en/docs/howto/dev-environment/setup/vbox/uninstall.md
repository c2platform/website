---
categories: [How-to]
tags: [linux, virtualbox, uninstall]
title: Uninstall VirtualBox
linkTitle: Uninstall VirtualBox
translate: false
weight: 1
description: >
  Learn how to completely remove VirtualBox from your system.
---

## Remove VirtualBox Packages

```bash
sudo apt-get install virtualbox virtualbox-guest-utils virtualbox-guest-x11
```

## Remove VirtualBox Extension Pack

```bash
vboxmanage list extpacks
sudo VBoxManage extpack uninstall "Oracle VM VirtualBox Extension Pack"
```
