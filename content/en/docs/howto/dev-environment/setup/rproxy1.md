---
categories: ["How-to"]
tags: [vagrant, reverse-proxy, forward-proxy, apache2]
title: "Create the Reverse Proxy"
linkTitle: "Reverse Proxy"
translate: false
weight: 7
description: >
  Create and provision the `c2d-rproxy1`,  which is an essential prerequisite for
  a functional development environment.
---

---
The c2d-rproxy1 node serves multiple essential functions in the setup of a
functional development environment. In a previous step
[LXD step]({{< relref path="./lxd" >}}),
we encountered an error while attempting to create the `c2d-rproxy1` node. In this
guide, we aim to successfully provision our first node, specifically
`c2d-rproxy1`, due to its various important functions.

---

## Creating `c2d-rproxy1`

To create and provision the node, use the following command:

```bash
c2  # activate c2d virtual environment
vagrant up c2d-rproxy1
```

## Verify

Setting up `c2d-rproxy1` with Apache2 is fairly straightforward, so if the
provisioning worked in the previous step, you are good to go. However, you can
verify the successful provisioning of the node by executing the following
commands:

```bash
vagrant ssh c2d-rproxy1
curl https://c2platform.org/is-alive
export https_proxy=http://1.1.4.205:1080
curl https://c2platform.org/is-alive
```

Running the `curl` commands should output the text: "Apache is alive." The first
`curl` verifies that the reverse proxy is working, and the second one verifies
that the forward proxy is working.

<details>
  <summary><kbd>Show me</kbd></summary>

```bash
vagrant@c2d-rproxy1:~$ curl https://c2platform.org/is-alive
Apache is alive
vagrant@c2d-rproxy1:~$ export https_proxy=http://1.1.4.205:1080
vagrant@c2d-rproxy1:~$ curl https://c2platform.org/is-alive
Apache is alive
```
</details>

## Additional Links

For more information about the various roles of `c2d-rproxy1`, refer to the following guides:

* [Managing Server Certificates as a Certificate Authority]({{< relref path="/docs/howto/c2/certs" >}} "How-to: Managing Server Certificates as a Certificate Authority")
* [Setup Reverse Proxy and CA server]({{< relref path="/docs/howto/c2/reverse-proxy" >}} "How-to: Setup Reverse Proxy and CA server")
* [Setup SOCKS proxy]({{< relref path="/docs/howto/c2/socks-proxy" >}} "How-to: Setup SOCKS proxy")
* [Setup DNS for Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}} "How-to: Setup DNS for Kubernetes")
