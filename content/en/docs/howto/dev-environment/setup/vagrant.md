---
categories: ["How-to"]
tags: [vagrant, hashicorp, lxd, sysprep, windows]
title: "Install Vagrant and Vagrant Plugins"
linkTitle: "Vagrant"
translate: false
weight: 4
description: >
  Install Vagrant, Vagrant plugin `vagrant-lxd`, `vagrant-windows-sysprep` and enable Vagrant autocomplete.
---

To install Vagrant, follow these steps:

1. **Install Vagrant:** begin by downloading and installing the appropriate Vagrant
   package for your operating system:

    ```bash
    sudo wget https://releases.hashicorp.com/vagrant/2.3.1/vagrant_2.3.1-1_amd64.deb
    sudo dpkg -i vagrant_2.3.1-1_amd64.deb
    ```
1.  **Install Plugins and Enable Autocomplete:** to extend Vagrant's
    functionality with the vagrant-lxd and vagrant-registration plugins, and to
    enable
    {{< external-link url="https://www.vagrantup.com/docs/cli" text="Vagrant command completion" htmlproofer_ignore="false" >}},
    execute the following:

    ```bash
    vagrant plugin install vagrant-lxd vagrant-registration
    vagrant autocomplete install --bash
    ```
1.  **Install the Vagrant Windows Sysprep Provisioner:** Additionally, install a custom version of the
      {{< external-link
      url="https://github.com/rgl/vagrant-windows-sysprep"
      text="Vagrant Windows Sysprep Provisioner"
      htmlproofer_ignore="false" >}}.
    This version is designed to work idempotently, enabling you to prepare
    Windows installations for reuse using
      {{< external-link
      url="https://learn.microsoft.com/en-us/windows-hardware/manufacture/desktop/sysprep--generalize--a-windows-installation"
      text="Sysprep"
      htmlproofer_ignore="false" >}}
. Follow these steps:

    ```bash
    wget https://c2platform.org/downloads/c2-iekeiTh7Fah5Orangooy/vagrant-windows-sysprep-0.0.11.gem
    vagrant plugin install vagrant-windows-sysprep-*.gem
    ```
1. **Optional: Automate Red Hat Linux VM Registration:** for those utilizing Red
   Hat Linux VMs, streamline the registration and unsubscription process by
   setting up `RHEL_DEV_ACCOUNT` and `RHEL_DEV_SECRET` environment variables. Refer
   to
   [Streamlining RHEL Registration and Subscription Automation]({{< relref path="/docs/guidelines/dev/rhel" >}}) for detailed guidance.
