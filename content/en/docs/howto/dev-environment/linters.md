---
categories: ["How-to"]
tags: [linux, ubuntu, laptop, linters, ansible, yaml, ansible-lint, yamllint]
title: "Linters"
linkTitle: "Linters"
translate: false
weight: 3
description: >
  Improve code quality, identify potential issues early on, and ensure adherence to coding standards.
---

If you are planning to contribute changes to an Ansible project, it is essential to integrate linters for Ansible and YAML into your project workflow. Linters play a crucial role in maintaining high code quality by automatically analyzing your codebase and identifying potential issues early on. By integrating linters, you can ensure adherence to coding standards and improve the overall reliability and readability of your Ansible code.


By following these steps and incorporating linters at various levels of your Ansible project, you can significantly improve code quality, identify potential issues early on, and ensure strict adherence to coding standards. This will result in more maintainable and robust Ansible code, facilitating collaboration and reducing the likelihood of errors in your deployments."

## VS Code

There are multiple levels at which you can apply linters within your Ansible project. Firstly, you can leverage the power of linters directly within popular integrated development environments (IDEs) such as Visual Studio Code. These IDEs offer plugins and extensions that provide real-time linting feedback, highlighting potential issues as you write code, enabling you to make immediate corrections and adhere to best practices.

### Linter

To integrate linters for Ansible and YAML into Visual Studio Code, you can install the {{< external-link url="https://marketplace.visualstudio.com/items?itemName=fnando.linter" text="Linter" htmlproofer_ignore="false" >}} extension developed by Nando Vieira. This extension supports linters for various languages, including YAML.

### Trailing Spaces

Another useful extension to consider for maintaining clean code in your Ansible project is {{< external-link url="https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces" text="Trailing Spaces" htmlproofer_ignore="false" >}} by Shardul Mahadik. This extension highlights trailing spaces as red squares, allowing you to easily spot and remove them.

To configure the Trailing Spaces extension to automatically remove trailing spaces whenever a file is saved, follow these additional steps:

1. Press <kbd>Cntrl</kbd>  <kbd>Shift</kbd> <kbd>p</kbd> to open the command palette.
2. Type "Preferences: Open User Settings" and select the corresponding option.
3. In the user settings, search for "trailing spaces".
4. Enable the "Trim on Save" option to automatically remove trailing spaces when saving files.

By installing and configuring the Linter and Trailing Spaces extensions in Visual Studio Code, you can ensure that your Ansible code adheres to coding standards, maintain clean and readable code, and catch potential issues early on.

## Manually

Secondly, you can choose to run linters manually in your development environment. By running linters on-demand, you can analyze your Ansible playbooks and YAML files for errors, style violations, and other coding issues. This manual approach allows you to review and fix any identified issues before committing your changes.

```bash
yamllint -c .yamllint .
ansible-lint -c .ansible-lint
```

## Pre-commit linters

To further enhance code quality and enforce linting as an integral part of your development workflow, you can utilize Git pre-commit hooks. By configuring pre-commit hooks, you can automatically run linters before each commit, preventing the introduction of faulty or non-compliant code into your repository. This ensures that all code modifications are thoroughly checked against defined standards, reducing the chances of introducing bugs or code smells. Create a file `.git/hooks/pre-commit`. Note use of environment variable `C2_LINTERS_VIRTUALENV` in the example below. This variable is for your Python virtual environment. You have to set this is for example `~/.bashrc` for the hook to work correctly in the terminal or VS Code.

```bash
#!/bin/bash
source $HOME/.virtualenv/$C2_LINTERS_VIRTUALENV/bin/activate
yamllint -c .yamllint .
ansible-lint -c .ansible-lint
```

It is recommended to create a clone script see [Clone script]({{< relref path="/docs/guidelines/setup/clone" >}} "Guideline: Clone script").

### Pre-commit linters using pre-commit ( expirimental )

An alternative way to run linters is to use [pre-commit](https://pre-commit.com/). This is a more advanced but also more complicated approach which might work or not work depending on your environment. Steps are as follows:

1. Create `.pre-commit-config.yaml` in root of repository.
2. Install pre-commit Python package using `pip3 install pre-commit`.
3. Create Git pre-commit hook using `pre-commit install`. This creates a file `.git/hooks/pre-commit`.

An example of a pre-commit config file `.pre-commit-config.yaml` is shown below

```yaml
---
default_language_version:
  python: python3.8
repos:
  - repo: https://github.com/ansible-community/ansible-lint.git
    rev: v6.7.0
    hooks:
      - id: ansible-lint
        args: ["-c", ".ansible-lint"]
  - repo: https://github.com/adrienverge/yamllint.git
    rev: v1.28.0
    hooks:
      - id: yamllint
        args: ["-c", ".yamllint"]
```

## CI/CD Pipeline

Lastly, integrating linters into your Continuous Integration/Continuous Deployment (CI/CD) pipeline provides an additional layer of code quality assurance. By incorporating linters into your automated build and deployment process, you can automatically scan your Ansible code for issues during the pipeline execution. This approach enables early detection of problems, ensures consistent code quality across your entire codebase, and helps maintain a reliable and stable infrastructure.

See [Continuous Integration and Deployment]({{< relref path="/docs/tutorials/git-workflow/6-cicd" >}}) in the [Mastering Git Workflow and Releases in Ansible]({{< relref path="/docs/tutorials/git-workflow" >}}) tutorial for more information.


## Links

* {{< external-link url="https://ansible.readthedocs.io/projects/lint/configuring/#ansible-lint-configuration" text="Configuration - Ansible Lint Documentation" htmlproofer_ignore="false" >}}.
