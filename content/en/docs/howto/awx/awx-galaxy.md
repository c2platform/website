---
categories: ["How-to"]
tags: [awx, galaxy, ansible]
title: Connecting the Automation Controller (AWX) to the Automation Hub (Galaxy NG) using Ansible
draft: false
linkTitle: Connect Controller to Hub
translate: false
weight: 3
description: >
  Learn how to reconfigure AWX to utilize the private Galaxy NG automation hub
  instead of the public Galaxy website.
---

Projects: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}),
[`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}),
[`c2platform.mgmt`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mgmt" >}}),
[`ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})

---

{{< under_construction >}}

This guide explains how to connect AWX, which can be accessed at
{{< external-link
url="https://awx.c2platform.org"
htmlproofer_ignore="true" >}},
to Galaxy NG, accessible at
{{< external-link
url="https://galaxy.c2platform.org"
htmlproofer_ignore="true" >}},
rather than relying on the public Galaxy website at
{{< external-link
url="https://galaxy.ansible.com"
htmlproofer_ignore="false" >}}.
This configuration enables you to have better control over the Ansible content
used in your environment.

By following the steps outlined below, you can ensure that AWX only uses curated
and approved roles from the public Galaxy site through the introduction of the
Automation Hub (Galaxy NG).

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
!include <logos/kubernetes>
!include <cloudogu/tools/ansible>

AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddRelTag("light", $textColor="#93c7fb")
AddRelTag("ansible", $sprite="ansible", $legendText="ansible provision")
UpdateRelStyle("#042a4f", "#042a4f")

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("xtop", $sprite="workstation", $legendText="virtual-desktop")
AddContainerTag("ansible_container", $sprite="ansible", $legendText="ansible")
AddSystemTag("ansible", $sprite="ansible", $legendText="ansible-awx", $bgColor="#999999")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
Person(engineer, "Engineer")

Person(engineer, "Engineer")

System_Boundary(local, "c2platform.org" ) {
    Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1,lxd", $tags="rproxy")
    Boundary(aap, "Ansible Automation Platform ( AAP )") {
      Container(c2d_galaxy_k8s, "Kubernetes", "c2d-galaxy1,lxd", $tags="k8s") {
          Container(galaxy, "Automation\nHub", "", $tags="ansible_container")
      }
      Container(c2d_awx_k8s, "Kubernetes", "c2d-awx1,lxd", $tags="k8s") {
          Container(awx, "Automation\nController", "", $tags="ansible_container")
      }
    }
}

System_Ext(galaxy_website, "galaxy.ansible.com", "Stores all open source Ansible automation \n( collections, roles )", $tags="ansible")

Rel(engineer, c2d_rproxy1, "Access\nAutomation Controller\nWeb Interface", "awx.c2platform.org\ngalaxy.c2platform.org,\nhttps,443")
Rel(c2d_rproxy1, awx, "", "")
Rel_R(awx, galaxy, "Download\ncurated / approved collections / roles", "https,port 443")
Rel_R(galaxy, galaxy_website, "Download collections / roles", "https,port 443")
@enduml
```


---


## Prerequisites

* [Setup the Automation Controller ( AWX ) using Ansible]({{< relref path="/docs/howto/awx/awx" >}})
* [Setup the Automation Hub ( Galaxy NG) using Ansible]({{< relref path="/docs/howto/awx/galaxy" >}})

## Connect using Vagrant

To configure the connection between AWX and Galaxy NG, follow these steps:
1. In the Ansible inventory project [`c2platform/ansible`]({{< relref
   path="/docs/gitlab/c2platform/ansible" >}}), locate the file
   `group_vars/awx/awx.yml`. This file contains the AWX configuration.
1. Edit the variable `c2_awx_cred_galaxy_selected`, changing its value from
   `Ansible Galaxy` to `c2-galaxy`:

```yaml
c2_awx_cred_galaxy_selected: c2-galaxy
```

Now provision using

```bash
TAGS=config vagrant provision c2d-awx1
```

> Note: using `TAGS=config` we only perform the Ansible tasks for AWX
> configuration. This is optional and is only used top save some provisioning
> time.


## Connect using AWX

