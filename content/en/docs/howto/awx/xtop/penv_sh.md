---
categories: ["Example"]
tags: [ansible, python, environment]
title: "Install Ansible Using Pyenv: A Bash Script Example"
linkTitle: "Example Script to Install Ansible"
translate: false
weight: 1
description: >
    Example of an Ansible Configuration File `ansible.cfg` that is used on
    `c2d-xtop` to connect to the private Ansible Automation Hub based on Galaxy NG
    running on `c2d-galaxy1`.
---

This example showcases a Bash script,
{{< external-link
url="https://gitlab.com/c2platform/ansible/-/blob/master/scripts/penv.sh?ref_type=heads"
text="penv.sh"
htmlproofer_ignore="false" >}}
, which leverages
{{< external-link
url="https://github.com/pyenv/pyenv#install-python-build-dependencies"
text="pyenv"
htmlproofer_ignore="false" >}}
to seamlessly install Python and Ansible. This script is employed within the
virtual desktop environment named `c2d-xtop`. For detailed usage instructions,
please refer to the guide on
[Using the Automation Hub (Galaxy NG) from the Virtual Desktop]({{< relref path="/docs/howto/awx/xtop" >}}).

The script `penv.sh` utilizes `pyenv` to simplify the installation process of
Python and Ansible. It created for use in the virtual desktop environment known
as `c2d-xtop`. For instructions on how to employ this script, please consult the
guide titled
[Using the Automation Hub (Galaxy NG) from the Virtual Desktop]({{< relref path="/docs/howto/awx/xtop" >}}).

The {{< external-link
url="https://github.com/pyenv/pyenv#install-python-build-dependencies"
text="pyenv"
htmlproofer_ignore="false" >}}
utility is also used to create the
[Ansible Execution Environment]({{< relref path="/docs/concepts/ansible/projects/execution-env" >}}) for
Rijkswaterstaat ( RWS) see
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})
 for more information.