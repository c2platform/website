---
categories: ["How-to"]
tags: [awx, aap, galaxy, ansible]
title: "Utilizing the Ansible Automation Platform (AAP) from the Virtual Desktop"
linkTitle: "Utilizing AAP on the Virtual Desktop"
translate: false
weight: 3
description: >
  Unlock the potential of AAP by creating a virtual desktop environment and
  configuring it as an Ansible Control Node. This configuration ensures seamless
  connectivity to the Ansible Automation Hub (Galaxy NG) for efficient automation
  workflows.
---

Projects: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

---

This guide provides an instruction on how to use virtual desktop
`c2d-xtop` as an
{{< external-link
url="https://docs.ansible.com/ansible/latest/network/getting_started/basic_concepts.html#control-node"
text="Ansible Control Node"
htmlproofer_ignore="false" >}}
that utilizes the private Automation Hub running on `c2d-galaxy1`. You can
connect to the GUI of this desktop using an RDP connection or you can simply SSH
into it using `vagrant ssh c2d-xtop`. From there you can use for example
`ansible-galaxy` command line to download / publish collections.


```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

'LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
'!include <cloudogu/tools/ansible>
!include <cloudogu/tools/ansible>
!include <logos/kubernetes>

AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddRelTag("light", $textColor="#93c7fb")
AddContainerTag("ansible_container", $sprite="ansible", $legendText="ansible")
AddRelTag("ansible", $sprite="ansible", $legendText="ansible provision")
UpdateRelStyle("#042a4f", "#042a4f")

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("xtop", $sprite="workstation", $legendText="virtual-desktop")
AddSystemTag("ansible", $sprite="ansible", $legendText="ansible-galaxy", $bgColor="#999999")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
Person(engineer, "Engineer")

System_Boundary(local, "c2platform.org") {
   Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1,lxd", $tags="rproxy")
   Container(c2d_xtop, "Virtual Desktop", "c2d-xtop,lxd", $tags="xtop")
   Boundary(aap, "Ansible Automation Platform ( AAP )") {
    Container(c2d_galaxy_k8s, "Kubernetes", "c2d-galaxy1,lxd", $tags="k8s") {
        Container(galaxy, "Automation\nHub", "", $tags="ansible_container")
    }
    Container(c2d_awx_k8s, "Kubernetes", "c2d-awx1,lxd", $tags="k8s") {
        Container(awx, "Automation Controller", "", $tags="ansible_container")
    }
   }
}

System_Ext(galaxy_website, "galaxy.ansible.com", "Stores all open source Ansible automation \n( collections, roles )", $tags="ansible")

Rel(engineer, c2d_xtop, "Connect to\n desktop", "xRDP,port 3389")
Rel(engineer, c2d_xtop, "Connect\nusing SSH", "vagrant ssh")
Rel_R(c2d_xtop, c2d_rproxy1, "Upload / download collections", "galaxy.c2platform.org,\nhttps,port 443")
Rel_R(c2d_xtop, c2d_rproxy1, "Start Jobs", "awx.c2platform.org\nhttps,port 443")


Rel(c2d_rproxy1, galaxy, "", "")
Rel_R(awx, galaxy, "Download\napproved / curated\ncollections / roles", "")
Rel_R(galaxy, galaxy_website, "Download collections / roles", "https,port 443")
Rel(c2d_rproxy1, awx, "", "")

SHOW_LEGEND(false)
@enduml
```
