---
categories: ["How-to"]
tags: [awx, aap, galaxy, ansible, kubernetes, operator, pulp]
title: "Setup the Automation Hub ( Galaxy NG) using Ansible"
linkTitle: "Automation Hub ( Galaxy NG )"
translate: false
weight: 2
description: >
  Discover how to create the Ansible Automation Hub (Galaxy NG) using Ansible, in
  conjunction with the Pulp Operator. Additionally, this guide demonstrates how to
  configure the remote community repository with collections, enhancing your
  automation capabilities.
---

Projects: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}})

---
This step-by-step guide demonstrates how to create an instance of [Ansible Galaxy]({{< relref path="/docs/concepts/ansible/aap" >}}) on the `c2d-galaxy1` node using the {{< external-link url="https://pulpproject.org/pulp-operator/" text="Pulp Operator" htmlproofer_ignore="false" >}}. The {{< external-link url="https://pulpproject.org/" text="Pulp" htmlproofer_ignore="false" >}} platform is a recently developed open-source software that facilitates the management and distribution of software packages, similar to {{< external-link url="https://www.sonatype.com/products/sonatype-nexus-repository" text="Sonatype Nexus Repository" htmlproofer_ignore="false" >}}. A Galaxy NG instance essentially refers to a Pulp instance with the {{< external-link url="https://github.com/ansible/galaxy_ng/" text="Galaxy NextGen" htmlproofer_ignore="false" >}} Pulp plugin installed.

---

{{< alert title="Note:" >}}The documentation for the Pulp Operator, especially regarding its integration with Galaxy NG, is currently inadequate. If you plan to utilize the Pulp Operator outside of this project, it is recommended to locally clone the {{< external-link url="https://github.com/pulp/pulp-operator" text="Pulp Operator project" htmlproofer_ignore="false" >}} repository. This will allow you to investigate and address configuration issues by examining both the code and documentation. {{< /alert >}}

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

'LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
'!include <cloudogu/tools/ansible>
!include <logos/kubernetes>
!include <cloudogu/tools/ansible>

AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddRelTag("light", $textColor="#93c7fb")
AddRelTag("ansible", $sprite="ansible", $legendText="ansible provision")
UpdateRelStyle("#042a4f", "#042a4f")

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("xtop", $sprite="workstation", $legendText="virtual-desktop")
AddContainerTag("ansible_container", $sprite="ansible", $legendText="ansible")
AddSystemTag("ansible", $sprite="ansible", $legendText="ansible-galaxy", $bgColor="#999999")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
Person(engineer, "Engineer")

Person(engineer, "Engineer")

System_Boundary(local, "c2platform.org" ) {
    Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1,lxd", $tags="rproxy")
    Boundary(aap, "Ansible Automation Platform ( AAP )") {
      Container(c2d_galaxy_k8s, "Kubernetes", "c2d-galaxy1,lxd", $tags="k8s") {
          Container(galaxy, "Automation\nHub", "", $tags="ansible_container")
      }
    }
}

System_Ext(galaxy_website, "galaxy.ansible.com", "Stores all open source Ansible automation \n( collections, roles )", $tags="ansible")

Rel(engineer, c2d_rproxy1, "Access Galaxy NG\nWeb interface", "galaxy.c2platform.org,\nhttps,443")
Rel(c2d_rproxy1, galaxy, "", "")
Rel_R(galaxy, galaxy_website, "Download collections / roles", "https,port 443")
@enduml
```


---

<!-- include-start: howto-prerequisites.md -->
## Prerequisites

Create the reverse and forward proxy `c2d-rproxy1`.

```bash
c2
unset PLAY  # ensure all plays run
vagrant up c2d-rproxy1
```

For more information about the various roles that `c2d-rproxy1` performs in this project:

* [Setup Reverse Proxy and CA server]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [Setup SOCKS proxy]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Managing Server Certificates as a Certificate Authority]({{< relref path="/docs/howto/c2/certs">}})
* [Setup DNS for Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}})
<!-- include-end -->

## Setup

To create the Kubernetes instance and deploy the Galaxy NG instance, execute the following command:

```bash
vagrant up c2d-galaxy1
```

## Verify

You can now verify your AWX instance using the Kubernetes Dashboard and the AWX
web interface via the forward proxy, as described in [Getting Started]({{<
relref path="/docs/howto/dev-environment/setup">}}).

### Kubernetes Dashboard

To access the Kubernetes Dashboard, navigate to
{{< external-link
url="https://dashboard-galaxy.c2platform.org/" htmlproofer_ignore="true" >}}
and log in using a token. Obtain the token by running the following command inside
the `c2d-galaxy1` node:

```bash
kubectl -n kube-system describe secret microk8s-dashboard-token
```

Using the dashboard, you can navigate to the
{{< external-link
url="https://dashboard-galaxy.c2platform.org/#/workloads?namespace=awx"
text="awx" htmlproofer_ignore="true" >}} namespace. There, you should see
running pods without any errors, such as `galaxy-web` and `galaxy-content`.

For more information about the token and dashboard setup, refer to
[Setup the Kubernetes Dashboard]({{< relref path="../kubernetes/dashboard" >}}).

### Galaxy NG

To access the Galaxy NG web interface, please visit
{{< external-link url="https://galaxy.c2platform.org" htmlproofer_ignore="true" >}}.
You can log in using the `admin` account with the password `secret`.

Thanks to Ansible's configuration of the remote `community` repository along
with its dependencies, you should be able to synchronize it. To do so, follow
these steps:

1. Navigate to **Collections** → **Repository Management** → **Remote**.
2. Click on the **Sync** button to initiate the synchronization process.

{{< image filename="/images/docs/galaxy-sync.png?width=1200px" >}}

After initiating the sync, head over to the **Task Management** section. You'll
notice a job in progress. Be patient and wait for it to complete. Once it's
done, navigate to the **Namespaces** section. Here, you'll find several
namespaces housing Ansible collections, including the `c2platform` namespace.

{{< image filename="/images/docs/galaxy-namespaces.png?width=1200px" >}}

## Review

For a comprehensive understanding of the Galaxy NG instance creation process
using Ansible, please consult the following components within the Ansible
playbook project
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}):

1. `Vagrantfile.yml`: This file configures the `c2d_galaxy1` node with the
   `mgmt/galaxy` playbook.
2. `plays/mgmt/awx_galaxy.yml`: This playbook sets up Ansible roles for the `galaxy`
   Ansible group.
3. `hosts-dev.ini`: The inventory file assigns the `c2d_galaxy1` node to the
   `galaxy` Ansible group.
4. `group_vars/galaxy/main.yml`: In this file, you'll find the primary
   Kubernetes configuration, including the activation of add-ons such as
   `dashboard` and the `metallb` load balancer.
5. `group_vars/galaxy/kubernetes.yml`: This file contains configurations for
   setting up the Kubernetes cluster and installing Galaxy. It encompasses three
   critical settings:  `ansible_api_hostname`, `ansible_content_hostname`,
   `content_origin`. These settings must be correctly configured to match the
   URL of the Galaxy NG instance at
   {{< external-link
   url="https://galaxy.c2platform.org"
   htmlproofer_ignore="true" >}}, as an incorrect configuration may hinder
   Galaxy NG's correct operation outside the cluster. This setup is crucial for
   enabling the successful execution of the `ansible-galaxy collection install`
   command, as demonstrated in [Utilizing the Ansible Automation Platform (AAP)
   from the Virtual Desktop]({{< relref path="./awx-galaxy" >}}).
6. `group_vars/galaxy/hub.yml`: This file contains configuration that is
    utilized by the
    {{< external-link
    url="https://github.com/ansible/galaxy_collection"
    text="galaxy.galaxy" htmlproofer_ignore="false" >}} to configure the
    remote `community` collection's "requirements". It's important to note that
    this collection is currently undergoing active development and is known to
    have a multitude of issues.

{{< alert type="warning" title="Warning!" >}}
At this juncture, it's advisable to exercise caution when using the
`galaxy.galaxy` collection. This collection has not yet been officially released
on the [Ansible Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}})
website and is presently in the midst of active development, marked by a
significant number of known issues.
{{< /alert >}}

Additionally, within the Ansible collection project
[`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}),
you can explore:

1. The `c2platform.mw.microk8s` Ansible role.
2. The `c2platform.mw.kubernetes` Ansible role.

To access various resources within the `c2d` environment via the forward Proxy
please use the following URLs:

* {{< external-link
  url="https://galaxy.c2platform.org"
  htmlproofer_ignore="true" >}}
  for the Automation Hub ( Galaxy NG ) web interface.
* {{< external-link
  url="https://dashboard-galaxy.c2platform.org/"
  htmlproofer_ignore="true" >}}
  for the Kubernetes Dashboard where Galaxy NG is deployed in the `galaxy` namespace.
* {{< external-link
  url="https://galaxy.c2platform.org/api/galaxy/"
  text="Django REST framework"
  htmlproofer_ignore="true" >}}
* {{< external-link
  url="https://galaxy.c2platform.org/api/galaxy/v3/swagger-ui/"
  text="Automation Hub API"
  htmlproofer_ignore="true" >}}

## Links

* For guidance on using the Automation Hub (Galaxy NG) from the Virtual Desktop, consult
  [Use the Automation Hub ( Galaxy NG) from the Virtual Desktop]({{< relref path="/docs/howto/awx/xtop" >}}).
* {{< external-link url="https://github.com/ansible/galaxy_ng/wiki/End-User-Installation" text="End User Installation · ansible/galaxy_ng Wiki" htmlproofer_ignore="false" >}}. This document offers detailed instructions on installing Pulp and Galaxy NG from PyPi packages, along with an Ansible playbook. Although this method of installing Galaxy was initially considered, it has been surpassed by the preference for deploying Galaxy on Kubernetes through the utilization of the Pulp Operator.
* {{< external-link url="https://pulpproject.org/about-pulp-3/" text="About Pulp 3 | software repository management" htmlproofer_ignore="false" >}}
* {{< external-link url="https://operatorhub.io/" text="OperatorHub.io | The registry for Kubernetes Operators" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://operatorhub.io/operator/pulp-operator" text="Pulp Project" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://operatorhub.io/operator/galaxy-operator" text="Ansible Galaxy" htmlproofer_ignore="false" >}}
* {{< external-link url="https://github.com/pulp/pulp-operator" text="pulp/pulp-operator: Kubernetes Operator for Pulp 3. Under active development." htmlproofer_ignore="false" >}}
* {{< external-link url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/" text="Galaxy NG" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/dev/vagrant/#6-create-the-installer-config-file" text="Create the installer config file" htmlproofer_ignore="true" >}}
  * {{< external-link url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/usage_guide/installation/" text="Installation Guide" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/usage_guide/collections/" text="Collections Guide" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/usage_guide/collections/" text="Execution Environment Guide" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/usage_guide/rbac/" text="RBAC and User Management Guide" htmlproofer_ignore="false" >}}
  * {{< external-link
    url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/community/api_v3/"
    text="Galaxy V3 API"
    htmlproofer_ignore="true" >}}
* {{< external-link url="https://docs.pulpproject.org/pulp_ansible/" text="Welcome to Pulp Ansible’s documentation!" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://docs.pulpproject.org/pulp_ansible/settings.html" text="Settings" htmlproofer_ignore="false" >}} various settings that can be passed to Galaxy Operator using `pulp_settings`.
* {{< external-link url="https://github.com/pulp/pulp-operator" text="pulp/pulp-operator: Kubernetes Operator for Pulp 3. Under active development." htmlproofer_ignore="false" >}}
* {{< external-link
  url="https://github.com/redhat-cop"
  text="Red Hat Communities of Practice"
  htmlproofer_ignore="false" >}}
  * {{< external-link
    url="https://github.com/redhat-cop/controller_configuration"
    text="redhat-cop/controller_configuration: A collection of roles to manage Ansible Controller and previously Ansible Tower"
    htmlproofer_ignore="false" >}}
