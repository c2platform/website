---
categories: ["How-to"]
tags: [ansible, kubernetes, dashboard, dns]
title: "Setup the Kubernetes Dashboard"
linkTitle: "Dashboard"
translate: false
weight: 3
description: >
    Setup the Kubernetes Dashboard and access the dashboard.
---

---
This how-to describes how
{{< external-link url="https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/"
text="Kubernetes Dashboard"
htmlproofer_ignore="false" >}}
can be enabled / used / deployed on `c2d-ks1`.

---

{{< alert title="Note:" >}}
In this [c2platform/ansible]({{< relref path="/docs/gitlab/c2platform/ansible" >}})
project the dashboard is default enabled by Ansible. So this how-to no longer
needs to be performed.
{{< /alert >}}


## Enable dashboard

**Important:**

```bash
vagrant ssh c2d-ks1
sudo su -
microk8s.enable dns dashboard
kubectl -n kube-system patch svc kubernetes-dashboard -p '{"spec":{"externalIPs":["1.1.4.155"]}}'
```

To verify the dashboard is run command below. This `curl` should output Kubernetes Dashboard HTML.

```bash
curl https://1.1.4.155/ --insecure  # verify
```

## Verify access through reverse proxy

Navigate to
{{< external-link url="https://k8s.c2platform.org" htmlproofer_ignore="true" >}}
and then login using bearer token. You can get the bear token using command:

```bash
kubectl -n kube-system describe secret microk8s-dashboard-token
```

## Restart

There is a known issue that on restart of `c2d-ks1` MicroK8s does not start and no longer works correctly:

```bash
root@c2d-ks1:~# microk8s start
missing profile snap.microk8s.microk8s.
Please make sure that the snapd.apparmor service is enabled and started
```

The workaround is execute:

```bash
apparmor_parser --add /var/lib/snapd/apparmor/profiles/snap.microk8s.*
microk8s restart
```