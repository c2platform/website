---
categories: ["How-to"]
tags: [TODO]
title: "GitLab using Operator"
linkTitle: "GitLab using Operator"
translate: false
weight: 6
description: >
    Create GitLab instance using operators
---

---
Install GitLab in Kubernetes / c2d-ks1 using [GitLab Operator](https://docs.gitlab.com/operator/).

---

## Links

* [GitLab Operator | GitLab](https://docs.gitlab.com/operator/)

## Prerequisites

* TODO How-to CodeReady Containers (CRC) ./howto-crc.md


## Installation

[GitLab Operator](https://docs.gitlab.com/operator/) → [Installation | GitLab](https://docs.gitlab.com/operator/installation.html).

> not yet suitable for production

### Ingress ( optional )

### cert-manager

Via **OperatorHub** install {{< external-link url="https://console-openshift-console.apps-crc.testing/operatorhub/ns/gitlab-system?keyword=cert-ma&details-item=cert-manager-community-operators-openshift-marketplace" text="cert-manager" htmlproofer_ignore="true" >}}[]()

1.11.0



https://docs.gitlab.com/operator/installation.html#cluster




Note: links to {{< external-link url="https://console-openshift-console.apps-crc.testing/operatorhub/ns/gitlab-system?keyword=gitlab&details-item=gitlab-operator-kubernetes-community-operators-openshift-marketplace" text="OperatorHub" htmlproofer_ignore="true" >}}

Custom GitLab Runner image

## IngressClass

See {{< external-link url="https://console-openshift-console.apps-crc.testing/operatorhub/ns/gitlab-system?keyword=gitlab&details-item=gitlab-operator-kubernetes-community-operators-openshift-marketplace" text="OperatorHub" htmlproofer_ignore="true" >}}

> Cluster-wide IngressClass should be created prior to Operator setup, as OLM does not currently support this object type:

```yaml
apiVersion: networking.k8s.io/v1
kind: IngressClass
metadata:
  # Ensure this value matches `spec.chart.values.global.ingress.class`
  # in the GitLab CR on the next step.
  name: gitlab-nginx
spec:
  controller: k8s.io/ingress-nginx
```