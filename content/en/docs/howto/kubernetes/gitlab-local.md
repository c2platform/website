---
categories: ["How-to"]
tags: [gitlab, ansible, kubernetes, pipeline]
title: "GitLab Pipelines in Kubernetes"
linkTitle: "GitLab Pipelines in Kubernetes"
translate: false
weight: 5
description: >
    Running GitLab pipelines in Kubernetes using local GitLab instance `c2d-gitlab`.
---

---
This how-to describes how we can run GitLab CI/CD pipelines on Kubernetes using a self-managed / self-hosted GitLab CE installation. It basically moves the example project [`examples/kubernetes/gitlab-docker-build`]({{< relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-docker-build" >}}) that uses {{< external-link url="https://gitlab.com" text="gitlab.com" htmlproofer_ignore="false" >}} to a local GitLab CE instance running on `c2d-gitlab`.

---

## Overview

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")
'Person(user, "User")

Boundary(local, "local", $type="high-end dev laptop") {
    'Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1")
  Boundary(c2d_ks, "Kubernetes", $type="c2d-ks1") {
      Boundary(c2d_k8s_nja, "gitlab-runner", $type="namespace") {
          Container(runner, "Gitlab Runner", "")
          ' Container(nj, "helloworld", "application")
      }
  }
  Boundary(gitlab, "gitlab.c2platform.org", $type="c2d-gitlab") {
      Boundary(njx, "c2platform/gitlab-docker-build", $type="") {
          Container(registry, "Registry", "docker")
          Container(pipeline, "Pipeline", ".gitlab-ci.yml")
          Container(repo, "Repository", "git")
      }
  }
}


Rel(engineer, repo, "Push code changes", "")
Rel(engineer, pipeline, "Create release \nor deploy", "")
Rel_Right(repo, pipeline, "Trigger", "")
Rel(pipeline, runner, "Docker  \nbuild", "")
Rel(runner, registry, "Push \nimage", "")
@enduml
```

<!-- include-start: howto-prerequisites.md -->
## Prerequisites

Create the reverse and forward proxy `c2d-rproxy1`.

```bash
c2
unset PLAY  # ensure all plays run
vagrant up c2d-rproxy1
```

For more information about the various roles that `c2d-rproxy1` performs in this project:

* [Setup Reverse Proxy and CA server]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [Setup SOCKS proxy]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Managing Server Certificates as a Certificate Authority]({{< relref path="/docs/howto/c2/certs">}})
* [Setup DNS for Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}})
<!-- include-end -->

* To make sure that Kubernetes can resolve `gitlab.c2platform.org` see [Setup DNS for Kubernetes]({{< relref path="./dns.md" >}}).
* GitLab CE running on `c2d-gitlab` see [Setup GitLab]({{< relref path="../gitlab/gitlab.md" >}}).
* Kubernetes running on `c2d-ks1` see [Setup Kubernetes]({{< relref path="../gitlab/gitlab.md" >}}).

For the first two prerequisites running command `vagrant up c2d-rproxy1 c2d-gitlab` should be enough.

## Custom docker-in-docker (dind) images

The project [`examples/kubernetes/gitlab-docker-build`]({{< relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-docker-build" >}}) that is imported in our local GitLab CE instance running on `c2d-ks1` uses custom Docker images see `.gitlab-ci.yml`. The related projects that create these two images are:

* [`c2platform/docker/c2d/dind`]({{< relref path="/docs/gitlab/c2platform/docker/c2d/dind" >}})
* [`c2platform/docker/c2d/docker`]({{< relref path="/docs/gitlab/c2platform/docker/c2d/docker" >}})

The only customization done in these projects is to import the c2d root ca bundle {{< external-link url="https://gitlab.com/c2platform/ansible/-/raw/master/.ca/c2/c2.crt" text="c2.crt" htmlproofer_ignore="false" >}} see {{< external-link url="https://gitlab.com/c2platform/docker/c2d/docker/-/blob/master/Dockerfile" text="Dockerfile" htmlproofer_ignore="false" >}}.

Without this customization Docker commands will fail with message

> x509: certificate signed by unknown authority

## Import GitLab project

Create public namespace {{< external-link url="https://gitlab.c2platform.org/c2platform" htmlproofer_ignore="true" >}} and then **Import project** → **Repository by URL**

|Property        |Value                                                                    |
|----------------|-------------------------------------------------------------------------|
|URL             |{{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-docker-build.git" htmlproofer_ignore="true" >}}|
|Visibility Level|Public                                                                   |

## GitLab Runner

### Get registration token

Navigate to {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-docker-build" htmlproofer_ignore="false" >}} and **Settings** → {{< external-link url="https://gitlab.c2platform.org/c2platform/gitlab-docker-build/-/settings/ci_cd" text="CI/CD" htmlproofer_ignore="true" >}} and then **Runners** and copy the **registration token**.

### Edit `values.yml`

Put the registration token in `values.yml`.

```bash
vagrant ssh c2d-ks1
nano /vagrant/doc/howto-kubernetes-gitlab-local/values.yml  # and update registration token
```

### Install runner

Run the script `gitlab-runner.sh`

```bash
source /vagrant/doc/howto-kubernetes-gitlab-local/gitlab-runner.sh
```

### Verify

Check the GitLab Runner pod log. It should show message

> Runner registered successfully

<details>
  <summary><kbd>Show me</kbd></summary>

```bash
vagrant@c2d-ks1:~/scripts/microk8s/gitlab/gitlab-runner$ kubectl logs -f gitlab-runner-c46457f8b-6dr7g
Registration attempt 1 of 30
Runtime platform                                    arch=amd64 os=linux pid=14 revision=456e3482 version=15.10.0
WARNING: Running in user-mode.
WARNING: The user-mode requires you to manually start builds processing:
WARNING: $ gitlab-runner run
WARNING: Use sudo for system-mode:
WARNING: $ sudo gitlab-runner...

WARNING: There might be a problem with your config
jsonschema: '/runners' does not validate with https://gitlab.com/gitlab-org/gitlab-runner/common/config#/$ref/properties/runners/type: expected array, but got null
Created missing unique system ID                    system_id=r_pjr46VlRtCXO
Merging configuration from template file "/configmaps/config.template.toml"
WARNING: Support for registration tokens and runner parameters in the 'register' command has been deprecated in GitLab Runner 15.6 and will be replaced with support for authentication tokens. For more information, see https://gitlab.com/gitlab-org/gitlab/-/issues/380872
Registering runner... succeeded                     runner=GR1348941TvUfy4ig
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!

Configuration (with the authentication token) was saved in "/home/gitlab-runner/.gitlab-runner/config.toml"
Runtime platform                                    arch=amd64 os=linux pid=7 revision=456e3482 version=15.10.0
Starting multi-runner from /home/gitlab-runner/.gitlab-runner/config.toml...  builds=0
WARNING: Running in user-mode.
WARNING: Use sudo for system-mode:
WARNING: $ sudo gitlab-runner...

WARNING: There might be a problem with your config
jsonschema: '/runners/0/kubernetes/node_tolerations' does not validate with https://gitlab.com/gitlab-org/gitlab-runner/common/config#/$ref/properties/runners/items/$ref/properties/kubernetes/$ref/properties/node_tolerations/type: expected object, but got null
Configuration loaded                                builds=0
listen_address not defined, metrics & debug endpoints disabled  builds=0
[session_server].listen_address not defined, session endpoints disabled  builds=0
Initializing executor providers                     builds=0
```
</details>



If you enter the pod take note of files `/home/gitlab-runner/.gitlab-runner/certs/gitlab.c2platform.org.crt` and `/home/gitlab-runner/.gitlab-runner/config.toml`

<details>
  <summary><kbd>Show me</kbd></summary>

```bash
vagrant@c2d-ks1:~$ kubectl exec -it gitlab-runner-c46457f8b-6dr7g -- sh
/ $ ls /home/gitlab-runner/.gitlab-runner/certs
gitlab.c2platform.org.crt
/ $ cat /home/gitlab-runner/.gitlab-runner/config.toml
concurrent = 10
check_interval = 30
log_level = "info"
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "gitlab-runner-c46457f8b-6dr7g"
  url = "https://gitlab.c2platform.org/"
  id = 1
  token = "yU1z6fGDjbr2pngVGh3A"
  token_obtained_at = 2023-03-28T05:57:34Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "kubernetes"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.kubernetes]
    host = ""
    bearer_token_overwrite_allowed = false
    image = "ubuntu:20.04"
    namespace = "gitlab-runner"
    namespace_overwrite_allowed = ""
    privileged = true
    node_selector_overwrite_allowed = ""
    pod_labels_overwrite_allowed = ""
    service_account_overwrite_allowed = ""
    pod_annotations_overwrite_allowed = ""
    [runners.kubernetes.pod_security_context]
    [runners.kubernetes.init_permissions_container_security_context]
    [runners.kubernetes.build_container_security_context]
    [runners.kubernetes.helper_container_security_context]
    [runners.kubernetes.service_container_security_context]
    [runners.kubernetes.volumes]

      [[runners.kubernetes.volumes.empty_dir]]
        name = "docker-certs"
        mount_path = "/certs/client"
        medium = "Memory"
    [runners.kubernetes.dns_config]
/ $
```
</details>

Of course you should also now the runner under **Runners** section via {{< external-link url="https://gitlab.c2platform.org/c2platform/gitlab-docker-build/-/settings/ci_cd" text="CI/CD settings" htmlproofer_ignore="true" >}}.

## Run pipeline

Now that there a project runner is available we can start pipeline via **CI/CD** → {{< external-link url="https://gitlab.c2platform.org/c2platform/gitlab-docker-build/-/pipelines" text="Pipelines" htmlproofer_ignore="true" >}}.

## Links

* {{< external-link url="https://docs.gitlab.com/runner/install/kubernetes.html#providing-a-custom-certificate-for-accessing-gitlab" text="GitLab Runner Helm Chart | GitLab" htmlproofer_ignore="false" >}}
* {{< external-link url="https://docs.gitlab.com/runner/configuration/tls-self-signed.html#trustin-tls-certificates-for-docker-and-kubernetes-executors" text="Self-signed certificates or custom Certification Authorities" htmlproofer_ignore="false" >}}
