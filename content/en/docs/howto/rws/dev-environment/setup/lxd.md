---
categories: ["How-to"]
tags: [ubuntu, lxd, vagrant, network]
title: "LXD Network and Profile for RWS"
linkTitle: "LXD"
translate: false
weight: 3
description: >
  Install, initialize and configure LXD.
---

---
This guide provides instructions for installing, initializing and configuring
[LXD]({{< relref path="/docs/concepts/dev/lxd" >}}). LXD is the default
lightweight hypervisor for creating and managing lightweight VM's in the form of
Linux (LXC) containers.

---

##


```bash
lxc network create lxdbrag1 ipv6.address=none ipv4.address=1.1.5.1/24 ipv4.nat=true
lxc profile copy default agd
```

Now edit the profile `agd` and configure `lxdbrag1` as the `parent` of `eth1`.

```bash
lxc profile edit agd  #
```

The profile `gsd` should be like below.

```yaml
config: {}
description: GSD LXD profile
devices:
  eth0:
    name: eth0
    network: lxdbr0
    type: nic
  eth1:
    nictype: bridged
    parent: lxdbrag1
    type: nic
  root:
    path: /
    pool: c2d
    type: disk
name: agd
used_by: []
```