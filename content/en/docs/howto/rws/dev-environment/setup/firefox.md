---
categories: ["How-to"]
tags: [reverse-proxy, forward-proxy, apache, firefox, profile, proxy, sandbox]
title: "Access the RWS Development Environment with Firefox"
linkTitle: "Firefox"
translate: false
weight: 6
description: >
    Learn how to access the RWS Development Environment using Firefox by
    configuring a dedicated Firefox profile, importing the C2 CA Root certificate, and
    setting up a forward proxy.
---

Projects: [`c2platform/rws/ansible`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

## Introduction

The Ansible inventory project
[`c2platform/rws/ansible`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})
simulates a setup with two environments: a development environment `gsd` and
a "test" environment `gst`. The `gst` environment allows you to experiment with a GitOps Pipeline. This environment is optional and is currently used for:
* [GitOps Pipeline for an Execution Environment (EE) with Ansible Collections]({{< relref path="/docs/howto/rws/aap/gitops" >}})

These environments are set up as a sandbox. The domain `c2platform.org` does
not resolve to services on the internet but to services accessed through the reverse proxy node `gsd-rproxy1` (or `gst-rproxy1`).

## Create a Firefox Profile

It is strongly recommended to create a dedicated Firefox profile to keep your
regular browsing separate from accessing the RWS Development Environment. Follow Mozilla's guide on
{{< external-link
url="https://support.mozilla.org/en-US/kb/profile-manager-create-remove-switch-firefox-profiles?redirectslug=profile-manager-create-and-remove-firefox-profiles&redirectlocale=en-US"
text="Profile Manager - Create, remove or switch Firefox profiles"
htmlproofer_ignore="false" >}}.

You can access the Firefox Profile Manager in two ways:
1. Run the command `firefox -no-remote -P`.
2. Type `about:profiles` in the Firefox address bar to create and manage your Firefox profiles.

Once you have your dedicated profile ready, proceed with the following settings.

## Configure Network Settings

In your dedicated profile, configure network settings for the `gsd` or `gst`
environment:

| Property           | GSD Environment | GST Environment (optional) |
|--------------------|-----------------|----------------------------|
| HTTP Proxy         | `1.1.5.205`     | `1.1.5.209`                |
| Port               | `1080`          | `1080`                     |
| Also use for HTTPS | ✔               | ✔                          |

## Import CA Bundle

To prevent continuous TLS/SSL certificate errors when accessing the services,
import the C2 root certificate located at
`~/git/gitlab/c2/ansible-gis/.ca/c2/c2.crt` through the Firefox Certificates
settings.

## Verify

1. Navigate to
   {{< external-link url="https://c2platform.org/is-alive" htmlproofer_ignore="true" >}}.
   You should see the text "Apache is alive." If you see this message, your
   browser is correctly configured to access services from the GIS platform.
2. Access the landing pages for the `gsd` and `gst` environments by navigating to
   {{< external-link url="https://gsd.c2platform.org/" htmlproofer_ignore="true" >}}
    and
   {{< external-link url="https://gst.c2platform.org/" htmlproofer_ignore="true" >}}.

   {{< image filename="/images/docs/gsd.png?width=500px" >}}.
3. Access the C2 Platform community website at
   {{< external-link
   url="https://c2platform.org/"
   htmlproofer_ignore="true" >}}.
   Note that this is a local deployment of the website inside `gsd-rproxy1` (and
   `gst-rproxy1`).

   {{< image filename="/images/docs/c2platform.org.png?width=500px" >}}.

