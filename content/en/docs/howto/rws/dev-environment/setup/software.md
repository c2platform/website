---
categories: ["How-to"]
tags: ["vagrant", "windows", "ansible"]
title: "Making RWS Software and License Files Available for Ansible"
linkTitle: "Software and Licenses"
translate: false
weight: 6
description: >
  Make necessary ( ArcGIS ) software and licenses for Ansible on Windows `gsd` nodes using Vagrant Sync Folders.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" lang="en" >}})

---

The [`c2platform/rws/ansible-gis`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-gis" lang="en" >}}) project
streamlines the process of making essential software and licenses accessible for
Ansible on Windows `gsd` nodes, such as `gsd-agserver1`, by leveraging [Vagrant Sync
Folders]({{< relref path="/docs/guidelines/dev/vagrant-sync-folders" >}}).

Follow these steps to make ArcGIS software and licenses available for Ansible:

## Prepare `.sync_folders.yml`

Create a file named `.sync_folders.yml` in the root of
[`c2platform/rws/ansible-gis`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-gis" lang="en" >}}) project and
populate it with content similar to the example below. Modify the `src` path to
correspond to your local folder containing the necessary software and license
files. This configuration file will be utilized by the `Vagrantfile` within the
[`c2platform/rws/ansible-gis`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-gis" lang="en" >}}) project to
establish a folder mount within the VM, specifically at
`C:\arcgis-software-repo`.

```yaml
---
- src: /software/projects/rws/
  target: /arcgis-software-repo
```

{{< alert title="Note:" >}}This `.sync_folders.yml` is ignored by Git. See `.gitignore`.{{< /alert >}}

## Provide Software and Licenses

As an example my local folder is shown below:

```bash
├── arcgis
│   └── 11.3
│       ├── ArcGISDataStore-11.3.zip
│       ├── ArcGISDataStore.zip
│       ├── ArcGIS_Enterprise_Portal_113_467329_20240712.json
│       ├── ArcGISGISServerAdvanced_ArcGISServer_1463509.ecp
│       ├── ArcGISServer-11.3.zip
│       ├── ArcGISWebStyles-11.3.zip
│       ├── ArcGISWebStyles.zip
│       ├── dotnet-hosting-6.0.16-win.exe
│       ├── dotnet-hosting-8.0.6-win.exe
│       ├── InstallWebAdaptor.ps1
│       ├── microsoft
│       │   ├── dotnet-hosting-8.0.6-win.exe
│       │   └── WebDeploy_amd64_en-US.msi
│       ├── PortalForArcGIS-11.3.zip
│       ├── WebAdaptorIIS-11.3.zip
│       ├── WebAdaptorIIS.zip
│       └── WebDeploy_amd64_en-US.msi
├── fme
│   ├── fme-flow-2023.1.1-b23631-win-x64.exe
│   ├── fme-flow-2023.1.1-b23631-win-x64.zip
│   ├── fme-flow-2023.2-b23764-win-x64.exe
│   ├── fme-flow-2023.2-b23764-win-x64.zip
│   ├── fme-flow-2024.0.3-b24220-win-x64.exe
│   ├── fme-flow-2024.0.3-b24220-win-x64.zip
│   └── fme-flow-2024.1.3-b24627-win-x64.exe
├── geoweb
│   ├── rws2024.txt
│   ├── VertiGIS-Studio-Reporting-5.24.1.msi
│   └── VertiGIS-Studio-Web-5.31.0.msi
├── microsoft
│   ├── dotnet-hosting-8.0.6-win.exe
│   └── WebDeploy_amd64_en-US.msi
└── oracle
    └── instantclient-basic-windows.x64-23.4.0.24.05.zip
```

This will make the ArcGIS Server software available in `gsd-agserver1` with path
`C:\arcgis-software-repo\ArcGISServer.zip`.


## Vagrant up or reload

After creating `.sync_folders.yml` Vagrant will create the mount for your local
software folder when you do a `vagrant up` or `vagrant reload` command.

## Verify

If you connect to the VM using VirtualBox Manager you should be able to see the
mount `C:\arcgis-software-repo`.

{{< image filename="/images/docs/vagrant-sync-folder.png" >}}
