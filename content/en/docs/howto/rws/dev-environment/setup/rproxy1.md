---
categories: ["How-to"]
tags: [vagrant, reverse-proxy]
title: "Create the Reverse Proxy and Web Proxy"
linkTitle: "Reverse and Web Proxy"
translate: false
weight: 5
description: >
    Create and provision the `gsd-rproxy1` node, which is an essential prerequisite for a functional development environment.
---

---
The `gsd-rproxy1` node plays a similar role as the `c2d-rproxy1`. See
[Create the Reverse Proxy and Web Proxy]({{< relref path="/docs/howto/dev-environment/setup/rproxy1" >}})
for more information.

---

## Creating `gsd-rproxy1`

To create and provision the node, use the following command:

```bash
vagrant up gsd-rproxy1
```
