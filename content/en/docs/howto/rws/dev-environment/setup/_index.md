---
categories: ["How-to"]
tags: [linux, ubuntu, laptop, lxd, vagrant, ansible, virtualbox]
title: "Setup the RWS Development Environment on Ubuntu 22"
linkTitle: "Setup"
translate: false
weight: 2
description: >
  Install Ansible, Vagrant, LXD, Virtualbox and clone the project directory.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---

In this section, you will find step-by-step instructions on how to set up and
utilize the RWS development environment.
