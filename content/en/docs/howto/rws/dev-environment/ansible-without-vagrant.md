---
categories: ["How-to"]
tags: ["how-to", "ssh", "vagrant", "ansible"]
title: "Using Ansible without Vagrant"
linkTitle: "Ansible without Vagrant"
translate: false
weight: 15
description: >
    Vagrant is the default but you can also use Ansible directly if you prefer.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---
To be able to use Ansible directly without Vagrant with other words in order to
be able to run for example `ansible-playbook` you have to change SSH config on
your machine.

## SSH config

Edit `.ssh/config` and add entry shown below. This allows access to *all* nodes
using SSH hops via `gsd-rproxy1`.

```
Host gsd-*
  ProxyCommand ssh 1.1.4.205 -W %h:%p
  User vagrant
  IdentityFile ~/.vagrant.d/insecure_private_key
  StrictHostkeyChecking no
  UserKnownHostsFile /dev/null
  LogLevel INFO
  Compression yes
  ServerAliveInterval 10
  ServerAliveCountMax 10
```

## Verify

Start `gsd-rproxy1` and SSH into the node. Note: that we are using `ssh` and not
`vagrant ssh`, so we are bypassing Vagrant altogether.

```bash
vagrant up gsd-rproxy1
ssh gsd-rproxy1
```

Now you should also be able to run Ansible directly, without Vagrant, for
example with command similar to below.

```bash
source ~/.virtualenv/uwd/bin/activate
export ANSIBLE_CONFIG=$PWD/ansible-dev.cfg
ansible-playbook plays/mw/reverse_proxy.yml -i hosts-dev.ini --limit gsd-rproxy1
```

