---
categories: ["How-to"]
tags: [argcis, windows]
title: "Setup RWS GIS Platform using Ansible"
linkTitle: "Setup GIS Platform"
translate: false
weight: 2
description: >
  Setup ArcGIS Server, ArcGIS Data Store, ArcGIS Portal, FME and Geoweb using Ansible.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

