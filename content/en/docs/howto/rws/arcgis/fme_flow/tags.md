---
categories: [How-to]
tags: [tags, ansible, fme, windows]
title: Accelerating Ansible Provisioning with FME Flow using Ansible Tags
linkTitle: FME Flow and Ansible Tags
translate: false
weight: 2
toc_hide: false
hide_summary: true
description: >
  Enhance the efficiency of your Ansible provisioning by leveraging the power of tags.
---

Projects:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

This guide demonstrates how to expedite Ansible provisioning using tags. Tasks
within Ansible roles, which are part of the
[C2 Platform Ansible Collections]({{< relref path="/docs/concepts/ansible/projects/collections">}}),
adhere to the tagging scheme outlined in the guideline
{{< rellink path="/docs/guidelines/coding/tags" desc=false >}}.

## Prerequisites

Ensure you have completed the following prerequisite:

* {{< rellink path="/docs/howto/rws/arcgis/fme_flow" desc="true" >}}

## Provision

Provision the node, assuming it's already up-to-date, by executing the following
command:

```bash
ansible-playbook plays/gis/fme.yml -i hosts.ini --limit gsd-fme-core
```

The play might take several minutes to complete, approximately over 4.5 minutes
depending on your hardware, as it checks for the desired state of a previously
provisioned node. The run might conclude with a play recap as shown below,
indicating that 250 tasks were executed, with 106 tasks skipped.

```log
PLAY RECAP *********************************************************************
gsd-fme-core               : ok=240  changed=0    unreachable=0    failed=0    skipped=106  rescued=0    ignored=0
```

## List Tasks

With the `gsd-fme-core` provisioned and operational, you can use the
`--list-tasks` option to review all tasks and their tags.

```bash
ansible-playbook plays/gis/fme.yml -i hosts.ini --limit gsd-fme-core --list-tasks
```

In the Ansible GIS inventory project
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
this command outputs the results for four plays. We are primarily interested in
the final play as we are using `--limit gsd-fme-core`.

```log
  play #4 (fme): FME	TAGS: []
    tasks:
      c2platform.core.secrets : Set fact for common_secrets_dirs	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Deprecated variable	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Include secrets	TAGS: [always, secrets, vault]
      c2platform.wincore.ad : Include ad_resources	TAGS: [v0]
      c2platform.wincore.ad : Include ad_resources_types	TAGS: [v0]
      c2platform.core.vagrant_hosts : Set fact vagrant_hosts_content	TAGS: [config, development, system, v0, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage /etc/hosts of Vagrant Host	TAGS: [config, development, system, v0, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Linux guest	TAGS: [config, development, system, v0, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [config, development, system, v0, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [config, development, system, v0, vagrant, vagrant_hosts]
      c2platform.core.secrets : Set fact for common_secrets_dirs	TAGS: [always, secrets, v0, vault]
      c2platform.core.secrets : Deprecated variable	TAGS: [always, secrets, v0, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, secrets, v0, vault]
      c2platform.core.secrets : Include secrets	TAGS: [always, secrets, v0, vault]
      c2platform.wincore.win : Include win_resources_types	TAGS: [install, system, v0, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip, windows]
      c2platform.wincore.win : Include win_resources	TAGS: [install, system, v0, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip, windows]
      c2platform.gis.java : Stat java_home	TAGS: [v0]
      c2platform.gis.java : Install	TAGS: [application, install, java, v0]
      c2platform.core.cacerts2 : Set fact cacerts2_certificates	TAGS: [always, cacerts2-facts, v0]
      c2platform.core.cacerts2 : cacerts2_certificates	TAGS: [always, cacerts2-facts, v0]
      c2platform.core.cacerts2 : Set various certificate facts	TAGS: [always, cacerts2-facts, v0]
      c2platform.core.cacerts2 : Set fact cacerts2_certificates	TAGS: [always, cacerts2-facts, v0]
      include_tasks	TAGS: [application, cacerts2, install, v0]
      include_tasks	TAGS: [application, cacerts2, install, v0]
      include_tasks	TAGS: [application, cacerts2, install, v0]
      c2platform.gis.fme_flow : Stat license	TAGS: [application, fme_flow, install, v1]
      c2platform.gis.fme_flow : Create folders	TAGS: [application, fme_flow, install, v1]
      c2platform.gis.fme_flow : Include debug tasks	TAGS: [v1]
      c2platform.gis.tomcat : Stat Tomcat license	TAGS: [application, install, tomcat, v1]
      c2platform.gis.tomcat : Install	TAGS: [application, install, tomcat, v1]
      c2platform.core.vagrant_hosts : Set fact vagrant_hosts_content	TAGS: [application, config, development, system, tomcat, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage /etc/hosts of Vagrant Host	TAGS: [application, config, development, system, tomcat, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Linux guest	TAGS: [application, config, development, system, tomcat, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, system, tomcat, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, system, tomcat, v1, vagrant, vagrant_hosts]
      c2platform.core.secrets : Set fact for common_secrets_dirs	TAGS: [always, application, config, secrets, tomcat, v1, vault]
      c2platform.core.secrets : Deprecated variable	TAGS: [always, application, config, secrets, tomcat, v1, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, application, config, secrets, tomcat, v1, vault]
      c2platform.core.secrets : Include secrets	TAGS: [always, application, config, secrets, tomcat, v1, vault]
      c2platform.wincore.win : Include win_resources_types	TAGS: [application, config, tomcat, v1, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip]
      c2platform.wincore.win : Include win_resources	TAGS: [application, config, tomcat, v1, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip]
      c2platform.gis.tomcat : Include certificates tasks	TAGS: [application, install, tomcat, v1]
      c2platform.gis.tomcat : Include download cleanup tasks	TAGS: [v1]
      c2platform.gis.tomcat : Manage service	TAGS: [application, install, tomcat, v1, win_service]
      c2platform.gis.tomcat : Test Tomcat service	TAGS: [application, install, tomcat, v1, win_service, win_uri]
      c2platform.gis.fme_flow : Include install tasks	TAGS: [application, fme_flow, install, v1]
      c2platform.core.vagrant_hosts : Set fact vagrant_hosts_content	TAGS: [application, config, development, fme_flow, system, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage /etc/hosts of Vagrant Host	TAGS: [application, config, development, fme_flow, system, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Linux guest	TAGS: [application, config, development, fme_flow, system, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, fme_flow, system, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, fme_flow, system, v1, vagrant, vagrant_hosts]
      c2platform.core.secrets : Set fact for common_secrets_dirs	TAGS: [always, application, config, fme_flow, secrets, v1, vault]
      c2platform.core.secrets : Deprecated variable	TAGS: [always, application, config, fme_flow, secrets, v1, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, application, config, fme_flow, secrets, v1, vault]
      c2platform.core.secrets : Include secrets	TAGS: [always, application, config, fme_flow, secrets, v1, vault]
      c2platform.wincore.win : Include win_resources_types	TAGS: [application, config, fme_flow, install, system, v1, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip, windows]
      c2platform.wincore.win : Include win_resources	TAGS: [application, config, fme_flow, install, system, v1, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip, windows]
      c2platform.gis.fme_flow : Include database tasks	TAGS: [v1]
      c2platform.core.cacerts2 : Set fact cacerts2_certificates	TAGS: [always, application, cacerts2-facts, fme_flow, install, v1]
      c2platform.core.cacerts2 : cacerts2_certificates	TAGS: [always, application, cacerts2-facts, fme_flow, install, v1]
      c2platform.core.cacerts2 : Set various certificate facts	TAGS: [always, application, cacerts2-facts, fme_flow, install, v1]
      c2platform.core.cacerts2 : Set fact cacerts2_certificates	TAGS: [always, application, cacerts2-facts, fme_flow, install, v1]
      include_tasks	TAGS: [application, cacerts2, fme_flow, install, v1]
      include_tasks	TAGS: [application, cacerts2, fme_flow, install, v1]
      include_tasks	TAGS: [application, cacerts2, fme_flow, install, v1]
      c2platform.gis.fme_flow : FME Service	TAGS: [application, fme_flow, install, v1]
      c2platform.gis.fme_flow : Stat {{ fme_flow_api_config['token']['path'] }}	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Create token	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Return status_code	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Display the status code	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Save token value	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Read token	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Extract the token value	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Construct JSON body	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Debug JSON body	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : If LDAP connection to FME exists, then update	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Return status_code	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Create new connection to LDAP	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Fail task if status code is unexpected	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Display message if status code is 409	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Continue with other tasks	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Set proxy voor fme flow	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Licenseer de FME flow applicatie	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Configure email-settings	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Vind admin wachtwoord	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Copy content	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Fetch content	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Include the fetched YAML file	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Extract ID from the YAML structure	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Pas Admin wachtwoord aan	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Delete admin_id.yml	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Delete fme_flow_uri_return.yml	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Delete fme_flow_LDAP.yml	TAGS: [application, config, config_api, fme_flow, v1]
      Include role download	TAGS: [application, fme_flow, install, v1]
      c2platform.gis.fme_flow : Stat license	TAGS: [application, fme_flow, install, v2]
      c2platform.gis.fme_flow : Create folders	TAGS: [application, fme_flow, install, v2]
      c2platform.gis.fme_flow : Include debug tasks	TAGS: [v2]
      c2platform.gis.tomcat : Stat Tomcat license	TAGS: [application, install, tomcat, v2]
      c2platform.gis.tomcat : Install	TAGS: [application, install, tomcat, v2]
      c2platform.core.vagrant_hosts : Set fact vagrant_hosts_content	TAGS: [application, config, development, system, tomcat, v2, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage /etc/hosts of Vagrant Host	TAGS: [application, config, development, system, tomcat, v2, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Linux guest	TAGS: [application, config, development, system, tomcat, v2, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, system, tomcat, v2, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, system, tomcat, v2, vagrant, vagrant_hosts]
      c2platform.core.secrets : Set fact for common_secrets_dirs	TAGS: [always, application, config, secrets, tomcat, v2, vault]
      c2platform.core.secrets : Deprecated variable	TAGS: [always, application, config, secrets, tomcat, v2, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, application, config, secrets, tomcat, v2, vault]
      c2platform.core.secrets : Include secrets	TAGS: [always, application, config, secrets, tomcat, v2, vault]
      c2platform.wincore.win : Include win_resources_types	TAGS: [application, config, tomcat, v2, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip]
      c2platform.wincore.win : Include win_resources	TAGS: [application, config, tomcat, v2, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip]
      c2platform.gis.tomcat : Include certificates tasks	TAGS: [application, install, tomcat, v2]
      c2platform.gis.tomcat : Include download cleanup tasks	TAGS: [v2]
      c2platform.gis.tomcat : Manage service	TAGS: [application, install, tomcat, v2, win_service]
      c2platform.gis.tomcat : Test Tomcat service	TAGS: [application, install, tomcat, v2, win_service, win_uri]
      c2platform.gis.fme_flow : Include install tasks	TAGS: [application, fme_flow, install, v2]
      c2platform.core.vagrant_hosts : Set fact vagrant_hosts_content	TAGS: [application, config, development, fme_flow, system, v2, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage /etc/hosts of Vagrant Host	TAGS: [application, config, development, fme_flow, system, v2, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Linux guest	TAGS: [application, config, development, fme_flow, system, v2, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, fme_flow, system, v2, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, fme_flow, system, v2, vagrant, vagrant_hosts]
      c2platform.core.secrets : Set fact for common_secrets_dirs	TAGS: [always, application, config, fme_flow, secrets, v2, vault]
      c2platform.core.secrets : Deprecated variable	TAGS: [always, application, config, fme_flow, secrets, v2, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, application, config, fme_flow, secrets, v2, vault]
      c2platform.core.secrets : Include secrets	TAGS: [always, application, config, fme_flow, secrets, v2, vault]
      c2platform.wincore.win : Include win_resources_types	TAGS: [application, config, fme_flow, install, system, v2, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip, windows]
      c2platform.wincore.win : Include win_resources	TAGS: [application, config, fme_flow, install, system, v2, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip, windows]
      c2platform.gis.fme_flow : Include database tasks	TAGS: [v2]
      c2platform.core.cacerts2 : Set fact cacerts2_certificates	TAGS: [always, application, cacerts2-facts, fme_flow, install, v2]
      c2platform.core.cacerts2 : cacerts2_certificates	TAGS: [always, application, cacerts2-facts, fme_flow, install, v2]
      c2platform.core.cacerts2 : Set various certificate facts	TAGS: [always, application, cacerts2-facts, fme_flow, install, v2]
      c2platform.core.cacerts2 : Set fact cacerts2_certificates	TAGS: [always, application, cacerts2-facts, fme_flow, install, v2]
      include_tasks	TAGS: [application, cacerts2, fme_flow, install, v2]
      include_tasks	TAGS: [application, cacerts2, fme_flow, install, v2]
      include_tasks	TAGS: [application, cacerts2, fme_flow, install, v2]
      c2platform.gis.fme_flow : FME Service	TAGS: [application, fme_flow, install, v2]
      c2platform.gis.fme_flow : Stat {{ fme_flow_api_config['token']['path'] }}	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Create token	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Return status_code	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Display the status code	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Save token value	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Read token	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Extract the token value	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Construct JSON body	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Debug JSON body	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : If LDAP connection to FME exists, then update	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Return status_code	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Create new connection to LDAP	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Fail task if status code is unexpected	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Display message if status code is 409	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Continue with other tasks	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Set proxy voor fme flow	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Licenseer de FME flow applicatie	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Configure email-settings	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Vind admin wachtwoord	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Copy content	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Fetch content	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Include the fetched YAML file	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Extract ID from the YAML structure	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Pas Admin wachtwoord aan	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Delete admin_id.yml	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Delete fme_flow_uri_return.yml	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Delete fme_flow_LDAP.yml	TAGS: [application, config, config_api, fme_flow, v2]
      Include role download	TAGS: [application, fme_flow, install, v2]

```

## Provision with Tags

To accelerate the provisioning process and focus on specific tasks like
configuration, you can skip tasks tagged with `install` or `system` by using:

```bash
ansible-playbook plays/gis/fme.yml -i hosts.ini --limit gsd-fme-core --skip-tags install,system
```

Below are examples of task filtering using tags:

| Tags                                   | Tasks | Duration | Description                                                                                                |
|----------------------------------------|-------|----------|------------------------------------------------------------------------------------------------------------|
| ``                                     | 240   | ≈ 4:40   | Run all provisioning tasks.                                                                                |
| `--skip-tags install,system`           | 27    | ≈ 0:30   | Exclude tasks tagged as `install` or `system`, focusing on tasks tagged as `config` and `application`.[^1] |
| `--skip-tags install,system,tomcat`    | 17    | ≈ 0:20   | Same as above with the additional exclusion of Tomcat tasks.                                               |
| `--tags fme_flow`                      | 45    | ≈ 0:30   | Run tasks specifically tagged with `fme_flow`.                                                             |
| `--tags fme_flow --skip-tags install`  | 11    | ≈ 0:12   | Run FME Flow application configuration tasks only.                                                         |
| `--tags fme_flow --skip-tags cacerts2` | 27    | ≈ 0:20   | Run FME Flow application installation and configuration tasks, skipping certificate tasks.                 |

{{< alert title="Note:" >}}
When using `--tags` or `--skip-tags` with multiple tags, an implicit "or"
condition applies. For instance, `--tags config,fme_flow` results in tasks with
either `config` or `fme_flow` tags being executed. To specifically target
`config` tasks within `fme_flow`, use `--tags fme_flow --skip_tags install`.
{{< /alert >}}

## Additional Information

* {{< rellink path="/docs/howto/rws/arcgis/fme_flow/tags" desc=true >}}
* {{< external-link url="https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_tags.html" text="Tags — Ansible Community Documentation" htmlproofer_ignore="false" >}}
* {{< rellink path="/docs/guidelines/coding/tags/linux" desc=true >}}

## Footnotes

[^1]: As per the C2 Platform tagging guidelines outlined in
    {{< rellink path="/docs/guidelines/coding/tags" desc=false >}},
    tasks are assigned tags based on `install` or `config`, and `system` or
    `application`, so if you "skip" `install` or `system` this implies you
    "include" `config` "and" `application`.
