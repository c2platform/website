---
categories: ["How-to"]
tags: [gis, fme, windows, sysprep]
title: "Setting Up FME Flow with Ansible"
linkTitle: "FME Flow"
translate: false
weight: 5
provisionTime: 21 minutes provision
description: >
  Set up the FME Flow Core, FME Flow Database, and FME Flow System Share on MS Windows using Ansible.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-gis" >}}), [`c2platform.wincore`]({{<
relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

This guide provides an overview of setting up **FME Flow Core**, **FME Flow
Database**, and **FME Flow System Share** using Vagrant and Ansible. For more
details about these components, refer to {{< external-link
url="https://docs.safe.com/fme/2023.0/html/FME-Flow/ReferenceManual/architecture.htm"
text="FME Flow Architecture" htmlproofer_ignore="false" >}}.

1. Vagrant creates two VirtualBox Windows VMs: `gsd-fme-core`, `gsd-ad`, and an
   LXD container `gsd-db1`.
2. Vagrant uses the
   {{< external-link
   url="https://github.com/rgl/vagrant-windows-sysprep"
   text="Vagrant Windows Sysprep Provisioner"
   htmlproofer_ignore="false" >}} on `gsd-fme-core` and
   `gsd-ad`[^1].
4. Vagrant runs the Ansible provisioner in the following order on nodes:
   1. On `gsd-ad`, the `c2platform.wincore.ad` collection configures the AD
      domain controller for the domain `ad.c2platform.org`.
   2. PostgreSQL 14 is installed on `gsd-db1`, along with a database and user
      with necessary privileges.
   3. On `gsd-fme-core`, Ansible performs the following steps:
      1. Joins the node to the Windows domain `ad.c2platform.org`.
      2. Installs Java using the `c2platform.gis.java` role.
      3. Installs Tomcat using the `c2platform.gis.tomcat` role.
      4. Installs **FME Flow Core** using the `c2platform.gis.fme` role.

The diagram below illustrates the setup achieved with Vagrant, excluding the
reverse proxy `gsd-rproxy1`.

| Node           | OS                  | Provider   | Purpose                                       |
|----------------|---------------------|------------|-----------------------------------------------|
| `gsd-fme-core` | Windows 2022 Server | VirtualBox | FME Core Web Server                           |
| `gsd-ad`       | Windows 2022 Server | VirtualBox | AD domain controller and hosts FME File Share |
| `gsd-db1`      | Ubuntu 22.04 LTS    | LXD        | PostgreSQL database server                    |

## Prerequisites

Before proceeding, ensure you have completed the steps described in {{< rellink path="/docs/howto/rws/dev-environment/setup" desc=false >}}.

## Setup

Use the following commands to run the FME play and create the `gsd-ad`,
`gsd-db1`, and `gsd-core-fme` nodes. Running the `vagrant up` command will take
approximately 25 minutes to complete.

```bash
export BOX="gsd-ad gsd-db1 gsd-core-fme"
export PLAY="plays/gis/fme.yml"
vagrant up $BOX | tee provision.log
```

## Verify

### `gsd-ad`

1. Log in to `gsd-ad` and execute `systeminfo | Select-String "Domain"`. This
   should return `ad.c2platform.org`.
    <details>
      <summary><kbd>Show me</kbd></summary>

    ```bash
    PS C:\Users\vagrant> systeminfo | Select-String "Domain"

    OS Configuration:          Primary Domain Controller
    Domain:                    ad.c2platform.org


    PS C:\Users\vagrant> nslookup ad.c2platform.org
    Server:  ip6-localhost
    Address:  ::1

    Name:    ad.c2platform.org
    Addresses:  1.1.8.108
              10.0.2.15

    PS C:\Users\vagrant>
    ```

    </details>
2. Open the **DNS Manager** and check the properties of the DNS server
   **GSD-AD**. Only `1.1.8.108` should be enabled as a listening IP address.
    <details>
    <summary><kbd>Show me</kbd></summary>
    {{< image filename="/images/docs/dns-manager.png?width=1200px" >}}
    </details>
3. On your Ubuntu laptop, run `dig @1.1.8.108 ad.c2platform.org`. This should
   resolve `ad.c2platform.org` to `1.1.8.108`.
    <details>
      <summary><kbd>Show me</kbd></summary>

    ```bash
    onknows@io3:~$ dig @1.1.8.108 ad.c2platform.org

    ; <<>> DiG 9.18.12-0ubuntu0.22.04.3-Ubuntu <<>> @1.1.8.108 ad.c2platform.org
    ; (1 server found)
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 27806
    ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 4000
    ;; QUESTION SECTION:
    ;ad.c2platform.org.		IN	A

    ;; ANSWER SECTION:
    ad.c2platform.org.	600	IN	A	1.1.8.108
    ad.c2platform.org.	600	IN	A	10.0.2.15

    ;; Query time: 0 msec
    ;; SERVER: 1.1.8.108#53(1.1.8.108) (UDP)
    ;; WHEN: Wed Oct 25 09:40:07 CEST 2023
    ;; MSG SIZE  rcvd: 78
    ```

    </details>

### `gsd-fme-core`

Log in to `gsd-fme-core`.

1. Verify that the computer is part of the domain `ad.c2platform.org`:
    ```bash
    vagrant ssh gsd-fme-core
    powershell
    systeminfo.exe | Select-String "Domain"
    ```
    <details>
      <summary><kbd>Show me</kbd></summary>

    ```bash
    vagrant@GSD-FME-CORE C:\Users\vagrant>powershell
    Windows PowerShell
    Copyright (C) Microsoft Corporation. All rights reserved.

    Install the latest PowerShell for new features and improvements! https://aka.ms/PSWindows

    PS C:\Users\vagrant> systeminfo.exe | Select-String "Domain"

    Domain:                    ad.c2platform.org


    PS C:\Users\vagrant>

    ```

    </details>

### Login `gis-backup-operator`

Log in as `gis-backup-operator` using remote desktop to `gsd-fme-core` to
confirm that the user has been created with the correct password.

### Login FME

After successfully completing these commands, you should be able to access the
FME Flow interface by visiting {{< external-link
url="https://gsd-fme-core.internal.c2platform.org/fmeserver/"
htmlproofer_ignore="false" >}}. Log in as `admin` with the password `admin`.

### Database

On `gsd-fme-core`, connect using remote desktop with the Vagrant user, start
`pgAdmin`, and import the settings in the file
`C:\Users\Public\Desktop\pgadmin.json`.

Connect to the server using the password `secret`. Drill down to the tables of
the **fmeserver** database; you should see the database tables of FME Flow like
`fme_action` etc. This verifies that the database has been created.

## Review

In the RWS Ansible Inventory project [`c2platform/rws/ansible-gis`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-gis" >}}), review specific plays and
configurations.

### FME Play

Review the play `plays/gis/fme.yml`. Pay attention to the use of the `when`
condition, ensuring that the installation of Java and Tomcat is restricted to
`gsd-fme-core`.

This play utilizes various roles. Notable ones are:

* The `fme_flow` and `tomcat` roles in the [`c2platform.gis`]({{< relref
  path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}) collection.
* `win` from the [`c2platform.wincore`]({{< relref
  path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}) collection,
  which is integrated into both `tomcat` and `fme_flow` roles. This integration
  facilitates the management of Windows resources through
  `fme_flow_win_resources` and `tomcat_win_resources`.

### Tomcat `context.xml`

The variable `tomcat_win_resources` is used to manage the `context.xml` file of
Tomcat. In `group_vars/fme_core/tomcat.yml`, take note of the following item:

```yaml
  - path: "{{ tomcat_home }}/conf/context.xml"
    xpath: /Context
    fragment: >-
      <Valve className="org.apache.catalina.authenticator.SSLAuthenticator"
      disableProxyCaching="false" />
    notify: Restart Tomcat service
```

This item adds a `Valve` element to the file `context.xml`:

```xml
<Valve className="org.apache.catalina.authenticator.SSLAuthenticator" disableProxyCaching="false" />
```

### Tomcat `web.xml`

Similarly, using `tomcat_win_resources`, a `security-constraint` element is
added to `web.xml`. Take note of the `xpath` expression. This XPath expression
`/*[local-name()='web-app']` selects the root element only if its local name
(i.e., the tag name without the namespace prefix) is 'web-app'. This is a
workaround and is generally less precise than using proper namespace handling,
but it's often used in tools that don't support XML namespaces directly.

```yaml
  - path: "{{ tomcat_home }}/conf/web.xml"
    backup: true
    xpath: /*[local-name()='web-app']
    fragment: >-
      <security-constraint>
        <web-resource-collection>
          <web-resource-name>HTTPSOnly</web-resource-name>
          <url-pattern>/*</url-pattern>
        </web-resource-collection>
        <user-data-constraint>
          <transport-guarantee>CONFIDENTIAL</transport-guarantee>
        </user-data-constraint>
      </security-constraint>
    notify: Restart Tomcat service
```

### FME `fmeServerConfig.txt`

Take note of the file `fmeServerConfig.txt` in the directory
`D:\Apps\FME\Flow\Server`. It has been changed by Ansible and contains the line:

```
FME_SERVER_WEB_URL=https://GSD-FME-CORE.ad.c2platform.org:443
```

This line is managed by Ansible using configuration in
`group_vars/fme_core/main.yml`. This file contains the variable
`fme_flow_win_resources` as shown below:

```yaml
fme_flow_win_resources:
  core:
    - name: FME_SERVER_WEB_URL
      module: win_lineinfile
      path: "{{ fme_flow_home }}/Server/fmeServerConfig.txt"
      regex: '^FME_SERVER_WEB_URL='
      line: FME_SERVER_WEB_URL=https://{{ ansible_fqdn }}:{{ gs_tomcat_https_port }}
      notify: Restart FME Flow services
```

### WebSocket

Ansible configures FME WebSockets to be secure / use the `wss` scheme. See the
configuration in `group_vars/fme_core/websocket.yml`. When Ansible provisioning
completes without errors you can be reasonably sure WebSockets are working
because provisioning will trigger a handler to verify the WebSocket port 7078 is
open and accepting connections.

A number of files are changed / need to be changed:

In FME Flow home `D:\Apps\FME\Flow`:

1. `Server/fmeWebSocketConfig.txt`
2. `Server/config/publishers/websocket.properties`
3. `Server/config/subscribers/websocket.properties`

On FME Share `\\gsd-fme-core.ad.c2platform.org\fme\localization`:

1. `publishers\websocket\publisherProperties.xml`
2. `subscribers\websocket\subscriberProperties.xml`

The situation where part of the configuration is on the node and part of the configuration is on a share is interesting espcially when more that one FME Core node is targetted. When targetting hsots in the group `fme_core`, while with default Ansible behaviour the nodes in the group are changed in parallel creates a race conditino of some sort. Will one node succeed in changing the file and other nodes won't have to change it because it has already been changed? Or will they look at the file at the same time, and write to the file the same change? Depending on the situation, if all are "changed" Ansible will correctly notify restart of FME service. When only one node "changes" the file, the FME service of only one node will be restarted, which is not correct. And maybe Ansible proivision might fail because some nodes try to update and the file are locked because they are writen to by another node.

For this reason an additional file is created in the FME Flow home directory with the name `Server/ansible_shared_websocket_config.sha256`. It contains a sha256 hash value of the configuration in
`publisherProperties.xml` and `subscriberProperties.xml` and this file is then used to notify the two handlers, one to restart FME Service and one to verify that the WebSocket port is opened.

To verify WebSocket configuration, see
[Test FME WebSocket using OpenSSL Library and / or WSCAT]({{< relref
path="/docs/howto/rws/arcgis/fme_flow/websocket-test" >}}).

## Troubleshooting

### WebSocket

For troubleshooting, testing, and verifying the WebSocket configuration, see
[Test FME WebSocket using OpenSSL Library and / or WSCAT]({{< relref
path="/docs/howto/rws/arcgis/fme_flow/websocket-test" >}}).

### Install Task

Set the variable `fme_flow_debug: true` to create the file
`install_extract_command.txt` in `D:\Apps\FME\logs`.

## Default Install

To perform a default installation of FME Flow, which includes deploying a
PostgreSQL database without a separate Tomcat instance, follow these steps:

1. Remove or disable the `fme_flow_install_command`.
2. Recreate the environment by executing the following commands:

```bash
vagrant destroy gsd-fme-core -f
vagrant up gsd-fme-core
```

After successfully completing these commands, you should be able to access the
FME Flow interface by visiting {{< external-link url="http://localhost"
htmlproofer_ignore="false" >}} , which will redirect you to {{< external-link
url="http://localhost/fmeserver/" htmlproofer_ignore="false" >}}. Log in as
`admin` with the password `admin`.

<!--
TODO
## Troubleshooting

If the service does not start as expected for `gis-backup-operator`, you can try the following command:

```
D:\apps\FME\Flow\Server\WindowsService\FMEProcessMonitorCore.exe -s "FME Server Core"
```
-->

## Additional Information

* {{< external-link
  url="https://support.safe.com/hc/en-us/articles/25407558172557-WebSockets-and-FME-Server"
  text="WebSockets and FME Server – FME Support Center"
  htmlproofer_ignore="false" >}}


## Footnotes

[^1]: Vagrant uses the
      {{< external-link
      url="https://github.com/rgl/vagrant-windows-sysprep"
      text="Vagrant Windows Sysprep Provisioner"
      htmlproofer_ignore="false" >}}
      on all nodes in the Ansible inventory project
      [`c2platform/rws/ansible-gis`]({{< relref
      path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) but in this case
      running Sysprep is explicitly mentioned because this Ansible play for FME
      won't work without Sysprep because the play also creates an AD domain.