---
categories: ["How-to"]
tags: [argcis, windows]
title: "Setup ArcGIS Server and Date Store using Ansible"
linkTitle: "ArcGIS Server"
translate: false
provisionTime: 23 minute provision
weight: 1
description: >
  Install ArcGIS Server and ArcGIS Data Store on `gsd-agserver1` using Ansible.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

This how-to describes how to create the ArcGIS Server and Data Store instance on
node `gsd-agserver1` using the `arcgis_server` role Ansible role that is part of
the `c2platform.gis` Ansible collection.

| Node            | OS                  | Provider   | Purpose                      |
|-----------------|---------------------|------------|------------------------------|
| `gsd-agserver1` | Windows 2022 Server | VirtualBox | ArcGIS Server and Data Store |


---

<!-- include-start: howto-prerequisites-rws.md -->
## Prerequisites

Create the reverse and forward proxy `gsd-rproxy1`.

```bash
vagrant up gsd-rproxy1
```

For more information about the various roles that `gsd-rproxy1` performs in this project:

* [Setup Reverse Proxy and CA server]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [Setup SOCKS proxy]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Managing Server Certificates as a Certificate Authority]({{< relref path="/docs/howto/c2/certs">}})
* [ArcGIS Server software and licenses]({{< relref path="/docs/howto/rws/dev-environment/setup/software" >}}).
<!-- include-end -->

## Setup

```bash
vagrant up gsd-agserver1
```

## Verify

1. Using the
   [RWS FireFox profile]({{< relref path="/docs/howto/rws/dev-environment/setup/firefox" >}})
   navigate to
   {{< external-link url="https://gsd.c2platform.org/" htmlproofer_ignore="true" >}}.
  This should show the landing page.
2. Using credentials below go to
   {{< external-link url="https://gsd-agserver1.internal.c2platform.org:6443/arcgis/admin/" text="ArcGIS Server Administrator Directory" htmlproofer_ignore="true" >}} and navigate to
   {{< external-link url="https://gsd-agserver1.internal.c2platform.org:6443/arcgis/manager/" text="ArcGIS Server Manager" htmlproofer_ignore="true" >}}
   and login.
   | Username    | Password       |
   |-------------|----------------|
   | `siteadmin` | `siteadmin123` |
3. Navigate to
  {{< external-link url="https://localhost:2443/arcgis/datastore/" htmlproofer_ignore="true" >}}
  you should see the ArcGIS Data Store web interface with stores listed:
  **Relational** and **Tile Cache**.
  Select **Configure Additional Data Stores** and then login to the ArcGIS Server
  instance using values below:

   | GIS Server                              | Username    | Password       |
   |-----------------------------------------|-------------|----------------|
   | `gsd-agserver1.internal.c2platform.org` | `siteadmin` | `siteadmin123` |

## Review

{{< under_construction >}}

{{< download path="/download/rws/arcgis_server_initialized.yml" text="`arcgis_server_initialized.yml`" >}}
{{< download path="/download/rws/arcgis_server_not_initialized.yml" text="`arcgis_server_not_initialized.yml`" >}}

## Links

* {{< external-link url="https://enterprise.arcgis.com/en/server/latest/install/windows/welcome-to-the-arcgis-for-server-install-guide.htm" text="Welcome to the ArcGIS Server (Windows) installation guide" htmlproofer_ignore="false" >}}
* {{< external-link url="https://enterprise.arcgis.com/en/server/latest/get-started/windows/what-is-arcgis-for-server-.htm" text="What is ArcGIS Server?" htmlproofer_ignore="false" >}}
r_ignore="false" >}}
