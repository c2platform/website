---
categories: ["How-to"]
tags: [gis, geoweb, windows, sysprep, vertigis, studio, iis, SSL/TLS]
title: "Setting Up Geoweb"
linkTitle: "Geoweb"
translate: false
weight: 5
provisionTime: 7 minutes
description: >
  Guide to installing VertiGIS Studio Web and Studio Reporting.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

## Overview

This guide provides a step-by-step process to set up Geoweb, which includes
**VertiGIS Studio Web** and **Reporting**.

1. Vagrant creates a VirtualBox VM named `gsd-geoweb` and performs [Sysprep]({{<
   relref path="/docs/howto/dev-environment/setup/vagrant/" >}}) on it.
2. Vagrant runs the Ansible provisioner to prepare the VM for manual
   installation of the MSI:

   1. Open firewall ports for Web and Reporting.
   2. Create an SSL/TLS certificate.
   3. Install IIS.
   4. Create a service account `sa_geoweb`.
   5. Configure IIS: create websites, bindings, application pools, etc.
   6. Download the MSI and license file to the node `gsd-geoweb`.
3. Connect to `gsd-geoweb` via RDP and manually run the installers[^1].
4. Run the provisioner again to update the IIS configuration created by the
   VertiGIS installers to use the service account `sa_geoweb`.

| Node         | OS                  | Provider   | Purpose           |
|--------------|---------------------|------------|-------------------|
| `gsd-geoweb` | Windows 2022 Server | VirtualBox | Web and Reporting |

## Prerequisites

* Complete the steps to [Set Up the RWS Development Environment on Ubuntu
  22]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}).
* Ensure VertiGIS Studio software and license files are available. See [Making
  RWS Software and License Files Available for Ansible]({{< relref
  path="/docs/howto/rws/dev-environment/setup/software/" >}}).
* Ensure ArcGIS Server, DataStore, Portal, and Web Adaptor are running on
  `gsd-agserver1` and `gsd-agportal1`. See [Setup ArcGIS Server and Data Store
  using Ansible]({{< relref path="/docs/howto/rws/arcgis/server" >}}) and [Setup
  ArcGIS Portal and Web Adaptors using Ansible]({{< relref
  path="/docs/howto/rws/arcgis/portal" >}}).

## Setup

Run the following command:

```bash
vagrant up gsd-geoweb
```

## Create App in ArcGIS Portal

1. Navigate to {{< external-link
   url="https://gsd-agportal1.internal.c2platform.org/portal/home"
   htmlproofer_ignore="true" >}} and log in as `portaladmin`[^2].

   | Username      | Password         |
   |---------------|------------------|
   | `portaladmin` | `portaladmin123` |
2. Choose **Content** → **New Item** → **Application**
   | Property         | Value                                          | Comment                   |
   |------------------|------------------------------------------------|---------------------------|
   | Application Type | **Other Application**                          |                           |
   | Title            | `Geoweb Modules registratie`                   |                           |
   | Folder           |                                                | Keep default `portaldmin` |
   | Tags             | `Geoweb` `Vertigis Studio` `Modules`           |                           |
   | Summary          | `App for VertiGIS Studio modules registration` |                           |
3. Via **Settings** → **Application** → **Update**:
   1. add the **URL**
      {{< external-link
      url="https://geoweb.c2platform.org/"
      htmlproofer_ignore="true" >}};
   2. add the **Redirect URI**
      - `https://geoweb.c2platform.org/`
      - `https://geoweb.c2platform.org/ModuleViewer`
      - `https://geoweb.c2platform.org/ModuleReporting/designer`
   3. Click **Save**.
4. Copy the **App ID** / **Client ID** to your clipboard (e.g.,
   `3a01FOjRkQJD2dCv`).

## Install Studio Web

1. Connect to `gsd-geoweb` via RDP.
2. Right-click on the **Software** shortcut on the Desktop and select **Run as
   Administrator**.
3. Run `VertiGIS-Studio-Web-5.31.0.msi`.
   | Module                    | Destination Folder           |
   |---------------------------|------------------------------|
   | VertiGIS Studio Web       | `D:\Apps\VertiGIS\Web`       |
5. Start post-installation configuration:
   | Property          | Value                                                           |
   |-------------------|-----------------------------------------------------------------|
   | IIS Website       | **ModuleViewer**                                                |
   | Virtual Directory | `/ModuleViewer/`                                                |
   | Portal Type       | **Portal for ArcGIS**                                           |
   | Portal URL        | `https://gsd-agportal1.internal.c2platform.org/portal/home`[^2] |
   | App ID            | ( value from previous step )                                    |
   | Redirect URL      | `https` `geoweb.c2platform.org`                                 |

## Install Studio Reporting

1. Run as Administrator `VertiGIS-Studio-Reporting-5.22.0.msi`.
   | Module                    | Destination Folder           |
   |---------------------------|------------------------------|
   | VertiGIS Studio Reporting | `D:\Apps\VertiGIS\Reporting` |
2. Post-installation configuration:
   | Property          | Value                                                           |
   |-------------------|-----------------------------------------------------------------|
   | Data Directory    | `D:\ProgramData\Geocortex\Reporting`                            |
   | IIS Website       | **ModuleReporting**                                             |
   | Virtual Directory | `/ModuleReporting/`                                             |
   | Portal Type       | **Portal for ArcGIS**                                           |
   | Portal URL        | `https://gsd-agportal1.internal.c2platform.org/portal/home`[^2] |
   | App ID            | ( value from previous step )                                    |
   | Redirect URL      | `https` `geoweb.c2platform.org` `/ModuleReporting/designer/`    |

## Verify

1. Connect to `gsd-geoweb` as user `vagrant`, start **IIS Manager**, and review
   site settings:
   | Site name       | Application Pool | Physical path                | Connect as[^3]              |
   |-----------------|------------------|------------------------------|-----------------------------|
   | ModuleViewer    | DefaultAppPool   | `D:\inetpub\StudioWeb`       | Pass-through authentication |
   | ModuleReporting | DefaultAppPool   | `D:\inetpub\ModuleReporting` | Pass-through authentication |

   Review site bindings:

   | Site name       | Type  | IP address     | Port | Host name | SSL certificate                    |
   |-----------------|-------|----------------|------|-----------|------------------------------------|
   | ModuleViewer    | http  | All Unassigned | 8080 | (empty)   |                                    |
   | ModuleViewer    | https | All Unassigned | 4443 | (empty)   | gsd-geoweb.internal.c2platform.org |
   | ModuleReporting | http  | All Unassigned | 8081 | (empty)   |                                    |

2. Review applications:
   | Application     | Application Pool        | Physical Path              | Connect as |
   |-----------------|-------------------------|----------------------------|------------|
   | ModuleViewer    | DefaultAppPool          | D:\inetpub\ModuleReporting | sa_geoweb  |
   | ModuleReporting | VertiGISStudioReporting | D:\inetpub\ModuleReporting | sa_geoweb  |

3. Review application pools:
   | Application Pool        | Identity  |
   |-------------------------|-----------|
   | DefaultAppPool          | sa_geoweb |
   | VertiGISStudioReporting | sa_geoweb |


4. Navigate to
   {{< external-link
   url="https://geoweb.c2platform.org/ModuleViewer/"
   htmlproofer_ignore="true" >}}
   and
   {{< external-link
   url="https://geoweb.c2platform.org/ModuleReporting/"
   htmlproofer_ignore="true" >}}
   and login as `portaladmin`.

## Review

In the inventory project
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}):

* The Geoweb play is located at `plays/gis/geoweb`.
* The Geoweb configuration is in `group_vars/geoweb`.

The `cplatform.gis.vertigis_studio` Ansible role currently has minimal code.
Downloads of MSI installers and the license file are handled by the
`c2platform.wincore.download` role, while most configuration work is done by the
`c2platform.wincore.win` role.

### Certificates

The `c2platform.gis.vertigis_studio` Ansible role, part of the Ansible GIS
Collection [`c2platform.gis`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}), uses the Ansible
role `c2platform.core.cacerts2` to create and deploy certificates trusted on all
nodes (including `gsd-geoweb`). On `gsd-geoweb`, the deployed certificate is
used to create IIS HTTPS bindings.

1. The trust relationship for certificates created with the `cacerts2` role is
   configured in the file `group_vars/windows/main.yml`, applying to all Windows
   hosts:
   ```yaml
   win_resources:
      0-bootstrap:
         - name: Download c2.crt
           type: win_get_url
           url: https://gitlab.com/c2platform/rws/ansible-gis/-/raw/master/.ca/c2/c2.crt?ref_type=heads
           dest: C:\tmp\c2.crt
         - name: C2 CA Server Trust Relationship
           type: win_certificate_store
           path: C:\tmp\c2.crt
           store_name: Root
           store_location: LocalMachine
   ```
3. The file `group_vars/geoweb/certs.yml` contains the configuration for the
   certificate as part of the `vertigis_studio_cacerts2_certificates` variable,
   deploying it to `C:\ProgramData\Certs\geoweb-GSD-GEOWEB.p12`.
4. The file `group_vars/geoweb/win.yml` contains the configuration to import the
   certificate into the Windows certificate store using the variable
   `vertigis_studio_win_resources`, which includes:
   ```yaml
    - name: Import Geoweb Certificate
      type: win_certificate_store
      path: "{{ vertigis_studio_cacerts2_certificates[0]['deploy']['p12']['dest'] }}"
      key_exportable: true
      store_name: My
      key_storage: machine
      store_location: LocalMachine
      state: present
   ```
5. Finally, the variable `vertigis_studio_win_resources` is used to manage IIS
   HTTPS bindings with an item similar to:
   ```yaml
    - name: Add IIS HTTPS bindings
      type: win_iis_webbinding
      defaults:
        ssl_flags: 0  # sni disabled
        protocol: https
        certificate_friendly_name: "{{ gs_geoweb_certificate_friendly_name }}"
        state: present
      resources:
        - name: ModuleViewer
          port: "{{ gs_vertigis_studio_web_port_ssl }}"
        - name: ModuleReporting
          port: "{{ gs_vertigis_studio_reporting_port_ssl }}"
   ```

## Additional Information

* {{< external-link
  url="https://docs.vertigisstudio.com/webviewer/latest/admin-help/on-premises-installation.html"
  text="VertiGIS Studio Web - On-Premises Installation"
  htmlproofer_ignore="false" >}}
* [Tomcat SSL/TLS and Java Keystore and TrustStore Configuration for Linux and Windows Hosts]({{< relref path="/docs/howto/rws/certs/" >}})
* [Create a Simple CA Server using Ansible]({{< relref path="/docs/howto/rws/ca/" >}})

## Footnotes

[^1]: Automating the MSI might be possible using a script similar to {{<
    external-link
    url="https://gitlab.com/c2platform/rws/ansible-gis/-/blob/master/scripts/geoweb/install-vsw.ps1?ref_type=heads"
    text="install-vsw.ps1" htmlproofer_ignore="false" >}}, but it does not seem
    supported to use a different virtual directory. The parameter `VirtualPath`
    appears to be ignored.

[^2]: The reverse proxy URL `https://age.c2platform.org/portal/home/` is
    currently not working. Instead, use
    `https://gsd-agportal1.internal.c2platform.org/portal/home`.

[^3]: The `win_iis_website` module does not allow configuring a different
    "connect as" user. If this needs to be changed, it must be done manually.
