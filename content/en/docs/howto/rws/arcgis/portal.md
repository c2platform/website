---
categories: ["How-to"]
tags: [argcis, windows, portal]
title: "Setup ArcGIS Portal and Web Adaptors using Ansible"
linkTitle: "ArcGIS Portal"
translate: false
provisionTime: 26 minute provision
weight: 2
description: >
  Install ArcGIS Portal and ArcGIS Web Adaptors on `gsd-agportal1` using Ansible.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

This how-to describes how to create the
{{< external-link url="https://enterprise.arcgis.com/en/portal/latest/administer/windows/what-is-portal-for-arcgis-.htm" ext="ArcGIS Portal" htmlproofer_ignore="false" >}}
instance `gsd-agportal1` using the `arcgis_portal` role Ansible role that is
part of the
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})
Ansible collection. Provisioning of this node also creates two
{{< external-link url="https://enterprise.arcgis.com/en/server/latest/install/windows/about-the-arcgis-web-adaptor.htm" text="ArcGIS Web Adaptor" htmlproofer_ignore="false" >}} instances, one for ArcGIS Portal and one for ArcGIS Server running on `gsd-agserver1`. See [Setup ArcGIS Server using Ansible]({{< relref path="./server" >}} "How-to: Setup ArcGIS Server using Ansible").

| Node            | OS                  | Provider   | Purpose                        |
|-----------------|---------------------|------------|--------------------------------|
| `gsd-agportal1` | Windows 2022 Server | VirtualBox | ArcGIS Portal and Web Adaptors |

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Boundary(gis, "gis.c2platform.org", $type="") {
  System(agpro1, "ArcGIS Portal & Web Adaptors", "gsd-agportal1", "") {
      Container(wa1, "Web Adaptor", "")
      Container(wa2, "Web Adaptor ", "")
      Container(portal1, "ArcGIS Portal", "")
  }
  System_Ext(server_datastore, "ArcGIS Server & Datastore", "gsd-agserver1") {
    Container_Ext(agserver1, "ArcGIS Server", "")
    Container_Ext(datastore, "ArcGIS Datastore", "")
  }
}

Rel(wa1, portal1, "", "")
Rel(wa2, agserver1, "", "")
Rel_D(portal1, agserver1, "", "")
Rel_D(agserver1, datastore, "", "")
'Lay_D(agserver1, datastore, "", "")
@enduml
```

## Prerequisites

* [Setup ArcGIS Server and Date Store using Ansible]({{< relref path="/docs/howto/rws/arcgis/server" >}})

{{< alert >}}
If you don't have ArcGIS Server / `gsd-agserver1` running Ansible task
**Configure ArcGIS Web Adaptor for Portal for ArcGIS** will fail.
{{< /alert >}}

## Setup

To set up your environment, follow these steps using Vagrant:

1. Run the following command to execute two plays: `plays/gis/portal.yml` and
   `plays/gis/web_adaptor.yml` on the gsd-portal1 virtual machine.

```bash
vagrant up gsd-portal1
```

2. Alternatively, if you prefer to run these plays separately, you can use the
   following commands:

```bash
export PLAY=plays/gis/portal.yml
vagrant up gsd-portal1 --provision
```

```bash
export PLAY=plays/gis/web_adaptor.yml
vagrant up gsd-portal1 --provision
```

Choose the option that best suits your needs to configure the gsd-portal1
virtual machine.

## Verify

Select **Show** in the GUI in VirtualBox Manager for `gsd-portal1` and then
Login and then start FireFox.

### ArcGIS Portal

Navigate to
{{< external-link url="https://gsd-agportal1:7443/arcgis/" htmlproofer_ignore="true" >}}
and then select **Sign In** and login using
credentials below. Navigate to
{{< external-link url="https://gsd-agportal1:7443/arcgis/portaladmin" htmlproofer_ignore="true" >}}
and then select **Sign In** and login using credentials below.

| Username      | Password         |
|---------------|------------------|
| `portaladmin` | `portaladmin123` |

### ArcGIS Web Adapter

1. Navigate to {{< external-link url="https://gsd-agportal1/portal/webadaptor" htmlproofer_ignore="true" >}}. You should see the ArcGIS Web Adapter
   configuration wizard. Note: the web adaptor configuration URL has to be
   accessed from the machine hosting the web adaptor.
    <details>
      <summary><kbd>Show me</kbd></summary>
    {{< image filename="/images/docs/age-web-adaptor.png?width=600px" >}}
    </details>
    </br>
2. Navigate to **IIS Manager**  can check the configuration for example the HTTPS
   binding on port 443 with certificate `gsd-agportal1`.
    <details>
      <summary><kbd>Show me</kbd></summary>
    {{< image filename="/images/docs/age-web-adaptor-iis.png?width=600px" >}}
    </details>

### ArcGIS Server ( via Web Adaptor )

Using credentials below navigate to {{< external-link url="https://gsd-agportal1.internal.c2platform.org/server/admin/" text="ArcGIS Server Administrator Directory" htmlproofer_ignore="true" >}} and navigate to {{< external-link url="https://gsd-agportal1.internal.c2platform.org/server/manager/" text="ArcGIS Server Manager" htmlproofer_ignore="true" >}} and login.


|Property  |Value                |
|----------|---------------------|
|Username  |`siteadmin`          |
|Password  |`siteadmin123`       |


### ArcGIS Portal ( via Web Adaptor )

{{< external-link url="https://gsd-agportal1.internal.c2platform.org/portal/home/" htmlproofer_ignore="true" >}}

## Review

To gain a better understanding of how the ArcGIS Portal is created using Ansible,
you can review the following:

In Ansible playbook project [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}):

### ArcGIS Portal Play


1. `Vagrantfile.yml`: This file configures the `gsd-agportal1` node with the
   `gis/portal` playbook. This file is read in the `Vagrantfile`.
1. `hosts-dev.ini`: The inventory file assigns the `gsd-agportal1` node to the
   `gs_portal` Ansible group.

### ArcGIS Web Adaptor Play

1.


