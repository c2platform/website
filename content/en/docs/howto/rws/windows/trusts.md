---
categories: ["How-to","Example"]
tags: [ansible, certificates, pki, windows, ssl/tls]
title: Managing SSL/TLS Trust with Ansible on Windows Hosts
linkTitle: Manage SSL/TLS Trust
translate: false
weight: 5
description: >
    Discover how to seamlessly manage SSL/TLS trust on Windows hosts using the
    `win_resources` variable of the `c2platform.wincore.win` Ansible role.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

---
In the setup described in
[Create a Simple Software Repository for Ansible]({{< relref path="/docs/howto/rws/software" >}}),
the download nodes `gsd-ansible-download1`, `gsd-ansible-download2`,
`gsd-ansible-download3` establish a SSL/TLS trust with the software server
`gsd-ansible-repo`, ensuring error-free software downloads.

The `gsd-rproxy1` node within the ansible-gis project doubles as a CA Server, detailed in
[Managing Server Certificates as a Certificate Authority]({{< relref path="/docs/howto/c2/certs" >}}).
Here, the download nodes use the root CA certificate from this Ansible-based CA
server to establish trust via the `c2platform.wincore.win` role.

Let's review how Ansible is used to reach this result.

## Ansible Inventory and Playbook

The Ansible inventory project
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})
includes important files as follows:

| File                                   | Description                                     |
|----------------------------------------|-------------------------------------------------|
| `plays/mgmt/ansible_repo.yml`          | Contains a playbook section for download nodes. |
| `group_vars/ansible_download/main.yml` | Configures the download nodes.                  |

### Download Node Playbook

The playbook `ansible_repo.yml` targets download nodes within the
`ansible_download` group. It employs the `c2platform.wincore.win` role for
establishing trust and the `c2platform.wincore.download` role for secure binary
downloads of Tomcat, relying on this trust setup. Using
`c2platform.wincore.download`, downloads proceed without SSL/TLS certificate
errors.

```yaml
- name: Tomcat on MS Windows
  hosts: ansible_download
  tags:
    - v2

  roles:
    - { role: c2platform.core.secrets, tags: ["common", "vault", "download"] }
    - { role: c2platform.wincore.win, tags: ["windows"] }
    - { role: c2platform.wincore.download, tags: ["download"] }
```
### Configuration for Downloads

Configuration specifics for download nodes are found in
`group_vars/ansible_download/main.yml`. It outlines the `win_resources`
variable, including an item for managing the certificate store through the
`c2platform.wincore.win` role, which leverages the
`ansible.windows.win_certificate_store` module for such operations.

```yaml
win_resources:
  ansible_download:
    - name: C2 Trust Relationship
      type: win_certificate_store
      path: C:\vagrant\.ca\c2\c2.crt
      store_name: Root
      store_location: LocalMachine
```

The downloads that need to be performed by the `c2platform.wincore.download`
role are configured using the `download_files` dictionary.
