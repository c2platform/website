---
categories: ["Example"]
tags: [windows, ansible, proxy, filter, jinja, cartesian]
title: "Configuring a Web Proxy Server Example for MS Windows Hosts"
linkTitle: "Web Proxy Server Example"
translate: false
weight: 3
description: >
    Learn how to configure and provision a web proxy server using the
    `c2platform.wincore.win` Ansible role. This example demonstrates the setup
    process for Windows hosts.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

---

The RWS Ansible Inventory project
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})
contains the necessary configuration for setting up the web proxy server, which is
hosted at
{{< external-link
url="http://webproxy.c2platform.org:1080"
htmlproofer_ignore="false" >}}.
This serves as an illustrative example of configuring a web proxy using the
`c2platform.wincore.win` role.

## Project Variable `gs_proxy_regedit`

Refer to the `group_vars/windows/proxy.yml` file where a "project variable"
`gs_proxy_regedit` is defined. For more information on defining project
variables, please see our
[guideline]({{< relref path="/docs/guidelines/coding/var-prefix" >}}) on it.

This variable is defined as a Cartesian product of two other project variables:
`gs_proxy_regedit_paths` and `gs_proxy_regedit_properties`, achieved through the
`c2platform.core.product` filter, an integral component of the `c2platform.core`
collection. This approach streamlines configuration and prevents redundancy.

```yaml
---
gs_proxy_regedit_paths:
  - path: HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
  - path: HKLM:\Software\Microsoft\Windows\CurrentVersion\Internet Settings

gs_proxy_regedit_properties:
  - name: ProxyServer
    data: http://webproxy.c2platform.org:1080
    type: string
  - name: ProxyOverride
    data: "*.c2platform.org;localhost;127.0.0.1;{{ ansible_hostname }}"
    type: string
  - name: ProxyEnable
    data: 1  # 1 enable, 0 disable
    type: dword

gs_proxy_regedit: >-
  {{ gs_proxy_regedit_paths
  | c2platform.core.product(gs_proxy_regedit_properties) }}
```

Of course, you can opt to bypass the use of the `gs_proxy_regedit_paths` and
`gs_proxy_regedit_properties` variables and manually specify the Cartesian
product.

<details>
  <summary><kbd>Show me</kbd></summary>

```yaml
---
gs_proxy_regedit:
  - name: ProxyServer
    data: http://webproxy.c2platform.org:1080
    type: string
    path: HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
  - name: ProxyOverride
    data: "*.c2platform.org;localhost;127.0.0.1;{{ ansible_hostname }}"
    type: string
    path: HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
  - name: ProxyEnable
    data: 1  # 1 enable, 0 disable
    type: dword
    path: HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
  - name: ProxyServer
    data: http://webproxy.c2platform.org:1080
    type: string
    path: HKLM:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
  - name: ProxyOverride
    data: "*.c2platform.org;localhost;127.0.0.1;{{ ansible_hostname }}"
    type: string
    path: HKLM:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
  - name: ProxyEnable
    data: 1  # 1 enable, 0 disable
    type: dword
    path: HKLM:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
```

</details>


## Configuration `win_resources`

In the file `group_vars/windows/main.yml` a variable `win_resources` is defined
which contains a group/key `1-windows`. This key is a list that includes various
items, one of which is named **Configure proxy server**.

```yaml
win_resources:
  1-windows:
    - name: Configure proxy server
      type: win_regedit
      resources: "{{ gs_proxy_regedit }}"
```

The `win_resources` variable plays a pivotal role in the
`c2platform.wincore.win` role. For detailed information, please refer to the
role's README.

## Policy Codification

With this configuration, the policy to configure the web proxy is effectively
codified. No specific manual actions are required to set up the web proxy on a
server. Thanks to Ansible, this process is automated seamlessly for any server
within the `windows` group.
