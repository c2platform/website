---
categories: ["Example"]
tags: [windows, ansible, download]
title: "Managing Downloads Example for MS Windows Hosts"
linkTitle: "Managing Downloads Example"
translate: false
draft: true
weight: 4
description: >
    Learn how to efficiently manage downloads using the
    `c2platform.wincore.download` Ansible role. This example demonstrates how to
    download ArcGIS software using this role.
---

{{< under_construction >}}

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

In the RWS Ansible Inventory project
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
the `c2platform.gis.arcgis_portal` role is employed to install ArcGIS Portal.

This role leverages the `c2platform.win.download` role to efficiently download
and extract a zip file containing ArcGIS Portal.

When reviewing the Ansible role `roles/arcgis_portal` in the
[`c2platform.gis`](<{{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}>)
Ansible collection, take note of the following:

1. `defaults/main.yml`: Pay attention to the `arcgis_portal_versions`
   dictionary, which defines the versions that can be installed by the role.
2. The `arcgis_portal_versions` dictionary is also used by the download role. In
   the file `tasks/download.yml`, you can observe the inclusion of the
   `c2platform.wincore.download` role and the passing of the
   `arcgis_portal_versions` dictionary as a variable. Additionally, the
   `arcgis_portal_temp_dir` is passed to the download role.
3. In `tasks/main.yml`, the role is included again, this time to "cleanup"
   downloaded files and/or created download directories.

By utilizing the `c2platform.wincore.download` role, you can efficiently
delegate the management of downloads, specifying what to download, where to
download, whether to use a proxy, and how to extract files. This eliminates the
need to duplicate this type of code in each product role.

For a more comprehensive understanding of the download role's functionality,
refer to the `download` role in the
[`c2platform.wincore`](<{{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}>)
 Ansible collection.

For an example of how to integrate the download role, please see the Ansible
role `roles/arcgis_portal` in the
[`c2platform.gis`](<{{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}>)
Ansible collection.

TODO