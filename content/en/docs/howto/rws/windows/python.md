---
categories: ["How-To"]
tags: [windows, ansible, python, pip, chocolatey]
title: "Manage Python and Python Library Dependencies"
linkTitle: "Python (Libraries)"
translate: false
weight: 4
provisionTime: 5 minute
description: >
    Learn how to manage Python and Python library dependencies on Windows hosts
    using Ansible.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

---

## Overview

This how-to utilizes Vagrant and Ansible to automate the following:

1. **Virtual Machine Setup:** Vagrant, with the VirtualBox Provider, initializes a
   VM named `gsd-python`.
2. **Python Installation:** Through Ansible, Vagrant installs **Python 3.11.0** from
   the Chocolatey repository and the `requests` library using `pip`. It's important
   to note that the PIP installation process will be configured to use a web
   proxy available at
   {{< external-link
   url="http://webproxy.c2platform.org:1080"
   htmlproofer_ignore="false" >}},
   specifically through the `gsd-rproxy1` node.


| Node         | OS                  | Provider   | Purpose                       |
|--------------|---------------------|------------|-------------------------------|
| `gsd-python` | Windows 2022 Server | VirtualBox | Setting up a Python environment on Windows |

## Prerequisites

* Ensure your RWS Development Environment is set up on Ubuntu 22, as detailed
  [here]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}).
* Ensure the web proxy is running by executing `vagrant up gsd-rproxy1`, as detailed
  [here]({{< relref path="/docs/howto/rws/dev-environment/setup/rproxy1" >}}).

## Setup

To set up the Windows node with Python and its libraries, execute the following
command, which typically completes in about 5 minutes:

```bash
vagrant up gsd-python
```

## Verification

1. A successful playbook execution confirms that Python and the `requests`
   package have been installed, as indicated by the final log entries of
   gsd-python, showing tasks **Manage Chocolatey packages** and **Manage Python
   library dependencies**.

   <p><details>
   <summary><kbd>Show me</kbd></summary><p>

   ```bash
    TASK [c2platform.wincore.win : Manage Chocolatey packages] *********************
    changed: [gsd-python] => (item=python present)

    TASK [c2platform.wincore.win : Manage Python library dependencies] *************
    changed: [gsd-python] => (item=requests 2.31.0 → present)
   ```

   </p></details></p>

2. To further verify the installation of Python and the requests package,
   execute the following commands:
   ```bash
   vagrant ssh gsd-python
   python version
   pip list
   ```
   This should display Python 3.11.0 and the installed packages, including
   `requests`.
   <p><details>
   <summary><kbd>Show me</kbd></summary><p>

   ```bash
    Microsoft Windows [Version 10.0.20348.707]
    (c) Microsoft Corporation. All rights reserved.

    vagrant@GSD-PYTHON C:\Users\vagrant>python --version
    Python 3.11.0

    vagrant@GSD-PYTHON C:\Users\vagrant>pip list | grep requests
    'grep' is not recognized as an internal or external command,
    operable program or batch file.

    vagrant@GSD-PYTHON C:\Users\vagrant>pip list
    Package            Version
    ------------------ --------
    certifi            2024.2.2
    charset-normalizer 3.3.2
    idna               3.6
    pip                22.3
    requests           2.31.0
    setuptools         65.5.0
    urllib3            2.2.1

    [notice] A new release of pip available: 22.3 -> 24.0
    [notice] To update, run: python.exe -m pip install --upgrade pip

    vagrant@GSD-PYTHON C:\Users\vagrant>
   ```
   </p></details></p>

## Review

For insights into the VM setup and configuration, review the Ansible Inventory,
Vagrant project files, and Ansible collections.

### Ansible Inventory

Review the following files in the Ansible inventory / Vagrant project:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}):

| File(s)                             | Description                                              |
|-------------------------------------|----------------------------------------------------------|
| `Vagrantfile` and `Vagrantfile.yml` | Defines the VM creation and networking for `gsd-python`. |
| `plays/core/python.yml`             | The main Ansible playbook.                               |
| `group_vars/python/main.yml`        | Contains the Python configuration settings.              |

The win_resources variable in `group_vars/python/main.yml` outlines the
resources required for the Python setup, including Chocolatey packages and PIP
installations:

```yaml
---
win_resources:
  python:
    - name: python
      type: win_chocolatey
      version: 3.11.0
    - name: requests
      type: win_pip
      version: 2.31.0
      state: present
      environment:
        Path: 'C:\Python311\;C:\Python311\Scripts;{{ ansible_env.Path }}'
```

The environment variable ensures that the `Path` variable is updated with the
necessary Python paths, which might not be immediately available to Ansible.


### Ansible Collections / Roles

| Collection           | Description                                                                                           |
|----------------------|-------------------------------------------------------------------------------------------------------|
| `c2platform.wincore` | Includes the `c2platform.wincore.win` role for streamlined installations of Python and its libraries. |


## Streamlining Development

The `gsd-python` node encompasses several tasks as it belongs to the `windows`
group. For a more focused setup, specifically targeting Chocolatey and PIP
installations, you can leverage Ansible tags like so:

```bash
TAGS=win_chocolatey,win_pip vagrant provision gsd-python
```

This approach allows you to pinpoint and expedite the provisioning process for
specific tasks.
