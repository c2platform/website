---
categories: ["How-to"]
tags: [windows, firewall]
title: "Manage Windows Firewall with Ansible"
linkTitle: "Windows Firewall Management"
translate: false
weight: 2
description: >
    Effective Windows Firewall management using Ansible with the
    `c2platform.wincore` collection. Configure inbound and outbound rules, manage
    advanced settings, and streamline firewall management tasks.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

---

The [`c2platform.wincore`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}) Ansible
collection offers powerful tools for efficiently managing Windows Firewall. This
guide will walk you through utilizing two key lists within the collection:
`win_firewalls` and `win_firewall_rules`. The reference / example implementation
for the GIS platform in project [`c2platform/rws/ansible-gis`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) offers an example on how
these lists are used to:

1. Enable Windows Firewall on all MS Windows nodes.
2. Open port `6443` and `6006` on all ArcGIS Server nodes.

## `gs_server` Ansible group

The file `group_vars/gs_server/firewall.yml` in [`c2platform/rws/ansible-gis`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) contains the following code:

```bash
gs_win_firewall_rules:
  - name: ArcGIS server https port for Apache Tomcat
    localport: 6443
  - name: ArcGIS Server for internal processes
    localport: 6006

gs_win_firewall_rules_defaults:
  action: allow
  direction: in
  protocol: tcp
  profiles:
    - private
    - public
    - domain
  state: present
  enabled: true

win_firewall_rules: "{{ gs_win_firewall_rules | c2platform.core.add_attributes(gs_win_firewall_rules_defaults) }}"
```

This example introduces two [project variables]({{< relref
path="/docs/guidelines/coding/var-prefix" >}}) `gs_win_firewall_rules` and
`gs_win_firewall_rules_defaults`. These variables only exists in
[`c2platform/rws/ansible-gis`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) project and their purpose
is to circumvent the necessity of duplicating code structures.

This way we can configure the firewall for servers in the Ansible group
`gs_server` with a simple list `gs_win_firewall_rules` with only two attributes
`name` and `port`.

Using the {{< external-link
url="https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_filters.html"
text="filter" htmlproofer_ignore="true" >}} `c2platform.core.add_attributes` the
defaults attributes defined with `gs_win_firewall_rules_defaults` are add to
this list and used to define `win_firewall_rules`.

For your convenience, here's an equivalent configuration without using `gs_`
project variables ( and the `c2platform.core.add_attributes` filter):

```bash
win_firewall_rules:
  - name: ArcGIS server https port for Apache Tomcat
    localport: 6443
    action: allow
    direction: in
    protocol: tcp
    profiles:
        - private
        - public
        - domain
  state: present
  enabled: true
  - name: ArcGIS Server for internal processes
    localport: 6006
    action: allow
    direction: in
    protocol: tcp
    profiles:
        - private
        - public
        - domain
    state: present
    enabled: true
```

## `windows` Ansible group

The file `group_vars/windows/firewall.yml` in [`c2platform/rws/ansible-gis`]({{<
relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) contains the
following code:

```yaml
win_firewalls:
  - state: enabled
    profiles:
      - private
      - public
      - domain
```

This configuration will ensure that all MS Windows nodes ( that are in `windows`
ansible group ) as defined in the `hosts.ini` in
[`c2platform/rws/ansible-gis`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) project, the firewall will
be enabled.
