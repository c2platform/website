---
categories: ["How-to", "Example"]
tags: [ansible, environment, podman, ansible-builder, ansible-navigator, docker]
title: "Manage the RWS Ansible Execution Environment"
linkTitle: "Ansible Execution Environment"
translate: false
weight: 5
description: >
  This guide provides step-by-step instructions on how to manage the RWS Ansible
  Execution Environment, ensuring compatibility with the latest Python and Ansible
  versions, along with the required Ansible collections.
---


Projects:
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}}),
[`c2platform/c2/docker/gitlab-runner`]({{< relref path="/docs/gitlab/c2platform/c2/docker/gitlab-runner" >}})

---

The RWS Ansible Execution Environment is constructed via `ansible-builder`
within the `c2platform/rws/ansible-execution-environment` project, utilizing a
GitLab CI/CD pipeline that operates in Docker containers. This process results
in a Docker image that serves as the EE. The custom GitLab Runner image required
for this process is derived from the `c2platform/c2/docker/gitlab-runner`
project. For additional details on this approach, please refer to the
[Docker-in-docker ( dind )]({{< relref path="/docs/howto/c2/dind" >}})
how-to guide.

---

## Prerequisites

* Ensure your RWS Development Environment is set up on Ubuntu 22, as detailed
  [here]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}).
* Install {{< external-link
  url="https://podman.io/"
  text="Podman"
  htmlproofer_ignore="false" >}},
  a container management tool.

## Inspecting the Execution Environment

To explore the contents of the EE, start by setting environment variables for
convenience:

```bash
export IMAGE_NAME="registry.gitlab.com/c2platform/rws/ansible-execution-environment"
export IMAGE_VERSION="0.1.7"
export IMAGE="$IMAGE_NAME:$IMAGE_VERSION"
```

Then, execute the following commands to list all included Ansible collections
and Python/PIP packages:

```bash
podman run $IMAGE ansible-galaxy collection list
podman run $IMAGE pip3 list installed
```

The output will display the available collections and installed packages.

<p><details>
  <summary><kbd>Show me</kbd></summary><p>

```bash
θ85° [:ansible-gis]└2 master(+39/-10)* ± podman run $IMAGE ansible-galaxy collection list

# /usr/share/ansible/collections/ansible_collections
Collection            Version
--------------------- -------
ansible.posix         1.5.4
ansible.windows       1.14.0
awx.awx               22.4.0
c2platform.core       1.0.8
c2platform.gis        1.0.4
c2platform.mgmt       1.0.2
c2platform.wincore    1.0.5
checkmk.general       3.0.0
chocolatey.chocolatey 1.5.1
community.crypto      2.17.1
community.general     8.3.0
community.postgresql  2.4.3
community.windows     2.0.0
```

```bash
θ65° [:ansible-execution-environment]└2 master(+18/-2)* ± podman run $IMAGE pip3 list installed | grep ansible
ansible-compat            4.1.11
ansible-core              2.15.0
ansible-lint              6.17.2
ansible-runner            2.3.4

[notice] A new release of pip available: 22.3.1 -> 24.0
[notice] To update, run: pip install --upgrade pip
```

</p></details></p>


## Updating the Execution Environment

To update the EE, you can either clone the
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})
project or edit it directly using the GitLab Web IDE. Consider modifying the
following files:

* Update the Ansible version, Python, and PIP packages in `execution-environment.yml`.
* Refresh Ansible collections in `requirements.yml`.
* Amend OS system dependencies in `indep.txt`.

Submitting and pushing your modifications will trigger the GitLab CI/CD pipeline
to release a new `latest` version of the EE.

## Local Build, Inspection, and Testing

For local development, you can use `ansible-builder`, `ansible-navigator`, and
`podman` to construct and validate the EE. This is a recommended practice before
committing changes:

```bash
rws  # Activate the RWS virtual environment
cd ../ansible-execution-environment/
ansible-builder build --tag $IMAGE
```

<p><details>
  <summary><kbd>Show me</kbd></summary><p>

```bash
θ62° [:ansible-execution-environment]└2 master(+18/-2)* ± ansible-builder build --tag $IMAGE
Running command:
  podman build -f context/Containerfile -t registry.gitlab.com/c2platform/rws/ansible-execution-environment:0.1.7 context
Complete! The build context can be found at: /home/ostraaten/git/gitlab/c2/ansible-execution-environment/context
```
</p></details></p>

Inspect and test the EE similarly to the initial inspection step.

```bash
podman run $IMAGE ansible-galaxy collection list
podman run $IMAGE pip3 list installed
```

To test the EE, use:

```bash
ansible-navigator run test_localhost.yml --eei $IMAGE --mode stdout
```

This command will execute a test playbook and display the output.

<p><details>
  <summary><kbd>Show me</kbd></summary><p>

```bash
θ65° [:ansible-execution-environment]└2 master ± ansible-navigator run test_localhost.yml --eei $IMAGE --mode stdout
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that
the implicit localhost does not match 'all'

PLAY [Gather and print local Ansible / Python facts] ***************************

TASK [Gathering Facts] *********************************************************
ok: [localhost]

TASK [Print Ansible version] ***************************************************
ok: [localhost] => {
    "ansible_version": {
        "full": "2.15.0",
        "major": 2,
        "minor": 15,
        "revision": 0,
        "string": "2.15.0"
    }
}

TASK [Print Ansible Python] ****************************************************
ok: [localhost] => {
    "ansible_playbook_python": "/usr/bin/python3"
}

TASK [Print Ansible Python version] ********************************************
ok: [localhost] => {
    "ansible_python_version": "3.11.7"
}

PLAY RECAP *********************************************************************
localhost                  : ok=4    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

</p></details></p>


## Releasing a New Version

To release an updated version of the EE:

1. Amend the `CHANGELOG.md` to reflect the latest changes.
2. Commit and push these changes. This action will prompt the CI/CD pipeline to
   publish a new latest version of the image.
3. After the pipeline concludes, manually initiate the **Create release** step
   via the GitLab web interface.

## Additional Resources

* {{< external-link
  url="https://ansible.readthedocs.io/en/latest/getting_started_ee/run_community_ee_image.html"
  text="Running Ansible with the community EE image - Ansible ecosystem documentation"
  htmlproofer_ignore="false" >}}
* {{< external-link
  url="https://ansible.readthedocs.io/en/latest/getting_started_ee/setup_environment.html"
  text="Setting up your environment - Ansible ecosystem documentation"
  htmlproofer_ignore="false" >}}
* {{< external-link
  url="https://ansible.readthedocs.io/en/latest/getting_started_ee/build_execution_environment.html"
  text="Building your first Execution Environment - Ansible ecosystem documentation"
  htmlproofer_ignore="false" >}}
* {{< external-link
  url="https://ansible.readthedocs.io/projects/builder/en/stable/definition/"
  text="Execution Environment Definition — Ansible Builder Documentation"
  htmlproofer_ignore="false" >}}
* {{< external-link
  url="https://ansible.readthedocs.io/projects/runner/en/stable/intro/#inputdir"
  text="Introduction to Ansible Runner — ansible-runner documentation"
  htmlproofer_ignore="false" >}}
