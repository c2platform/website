---
categories: ["How-to"]
tags: [ansible, software, nexus, ca, ssl/tls, windows, trust]
title: "Create a Simple Software Repository for Ansible"
linkTitle: "Software Repository"
translate: false
provisionTime: 25 minute
weight: 11
time: true
description: >
   This guide demonstrates how to establish a straightforward software repository
   with Ansible, which will serve as a centralized source for software
   distribution.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}})

---
## Overview

This guide outlines the process adopted by RWS for managing software downloads
using Ansible. It follows the strategy described in
[Designing a Flexible Software Repository for Ansible]({{< relref path="/docs/projects/rws/software" >}})
and employs the `c2platform.wincore.download` role for download operations. The
setup involves Vagrant and Ansible executing the following tasks:

1. Vagrant employs the VirtualBox Provider to instantiate five VMs as specified
   in the table below.
2. Using the Ansible Provisioner, Vagrant orchestrates an Ansible playbook
   across these VMs to:
   1. Establish a Windows File Share on `gsd-ansible-file-share1`, populating it with a Tomcat tarball and a zip file.
   2. Set up a software service, `gsd-ansible-repo`, which mounts the file share and hosts its contents via Apache2.
   3. Initiate concurrent downloads on three nodes (`gsd-ansible-download1`,
      `gsd-ansible-download2`, `gsd-ansible-download3`), using HTTPS. Shared
      storage acts as a cache, facilitated by `gsd-ansible-file-share1`. The
      `c2platform.wincore.download` role ensures that downloads are
      synchronized, with only one node downloading a file at any given time,
      determined by which node acquires a lock file first. There are two
      downloads, potentially handled by different nodes.

| Node                      | OS                  | Provider   | Purpose                                            |
|---------------------------|---------------------|------------|----------------------------------------------------|
| `gsd-ansible-file-share1` | Windows 2022 Server | VirtualBox | Hosts file share for storing and caching downloads |
| `gsd-ansible-repo`        | Red Hat 9           | VirtualBox | Simple web-based software server using Apache2     |
| `gsd-ansible-download1`   | Windows 2022 Server | VirtualBox | Executes test downloads                            |
| `gsd-ansible-download2`   | Windows 2022 Server | VirtualBox | Executes test downloads                            |
| `gsd-ansible-download3`   | Windows 2022 Server | VirtualBox | Executes test downloads                            |

## Prerequisites

* Ensure your RWS Development Environment is set up on Ubuntu 22, as detailed
  [here]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}).


## Setup

To prepare the complete environment, execute vagrant up for the specified VMs. On a
[high-performance development workstation]({{< relref path="/docs/concepts/dev/laptop" >}}),
this setup should take approximately 25 minutes.

```bash
export BOX="gsd-ansible-repo gsd-ansible-file-share1 gsd-ansible-download1 \
gsd-ansible-download2 gsd-ansible-download3"
vagrant up $BOX

```

## Verification

1. Successful execution of the playbook indicates that the download nodes
   (`gsd-ansible-download1`, `gsd-ansible-download2`, `gsd-ansible-download3`)
   managed to retrieve the Tomcat binaries. Review the final console output from
   Ansible, highlighting tasks like tasks **Create download lock**, **Download
   install file**,  **Extract zip** and **Remove lock file**, to determine which
   node successfully acquired the lock and completed the download.

   <p><details>
   <summary><kbd>Show me</kbd></summary><p>

   ```bash
   TASK [c2platform.wincore.download : Create download lock] **********************
   changed: [gsd-ansible-download1] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.tar.gz)
   ok: [gsd-ansible-download2] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.tar.gz)
   ok: [gsd-ansible-download3] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.tar.gz)
   changed: [gsd-ansible-download1] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.zip)
   ok: [gsd-ansible-download2] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.zip)
   ok: [gsd-ansible-download3] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.zip)

   TASK [c2platform.wincore.download : Download install file] *********************
   changed: [gsd-ansible-download1] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.tar.gz)
   changed: [gsd-ansible-download1] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.zip)

   TASK [c2platform.wincore.download : Extract zip] *******************************
   changed: [gsd-ansible-download1] => (item=apache-tomcat-10.1.19.zip → //gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19)

   TASK [c2platform.wincore.download : Remove lock file] **************************
   changed: [gsd-ansible-download1] => (item=\\gsd-ansible-file-share1.internal.c2platform.org\ansible-repo\cache\e9e4e25175d60ab7bf720609971f81206c644ce950deed17bf6de78613cca091.lock → absent)
   changed: [gsd-ansible-download1] => (item=\\gsd-ansible-file-share1.internal.c2platform.org\ansible-repo\cache\4c6082b852ad235cb10bc50992259ddc1145664c6347b0ea97100cada66c5153.lock → absent)
   ```

   </p></details></p>

2. Using the
   [RWS FireFox profile]({{< relref path="/docs/howto/rws/dev-environment/setup/firefox" >}})
   navigate to
   {{< external-link url="https://gsd-ansible-repo.internal.c2platform.org/"
   htmlproofer_ignore="true" >}}.
   When prompted, log in with the username `ansible` and the password
   `Supersecret!`. You should be able to view and download the Tomcat binaries,
   such as `apache-tomcat-10.1.19.tar.gz`.

3. Access `gsd-ansible-download1` via **Remmina** or **VirtualBox Manager** as
   the `vagrant` user (password: `vagrant`). Desktop shortcuts provide links to
   the file share and downloads. The `vagrant` user has full control over and
   access to the share.

## Review

To understand the VM setup and configuration, review the Ansible Inventory and
Vagrant project files, and the Ansible collections.

### Ansible Inventory

Review the following files in the Ansible inventory / Vagrant project
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}):

| File(s)                             | Description                                                               |
|-------------------------------------|---------------------------------------------------------------------------|
| `Vagrantfile` and `Vagrantfile.yml` | Used by Vagrant for VM creation, network setup, etc.                      |
| `plays/mgmt/ansible_repo.yml`       | The primary Ansible playbook                                              |
| `group_vars/ansible_file_share/*`   | Configuration for the file share node  `gsd-ansible-file-share1`          |
| `group_vars/ansible_repo/*`         | Configuration for the web server node `gsd-ansible-repo`                  |
| `group_vars/ansible_download/*`     | Configuration for the download nodes `gsd-ansible-download1`, `2` and `3` |

### Ansible Collections / Roles

| Collection           | Description                                                                         |
|----------------------|-------------------------------------------------------------------------------------|
| `c2platform.wincore` | Includes essential roles for Windows hosts, such as  `c2platform.wincore.download`. |
| `c2platform.core`    | Provides roles for Linux targets, e.g., `c2platform.core.linux`.                    |
| `c2platform.mw`      | Contains the `apache` role for web server setup.                                    |


## Next Steps

For development and experimentation, consider a stepwise approach using Ansible
tags and Vagrant snapshots to manage and iterate on the environment more
efficiently.

<p><details>
  <summary><kbd>Show me</kbd></summary><p>


| Tag  | Step              |Command| Description                                                                           |
|------|-------------------|---|---------------------------------------------------------------------------------------|
| `v0` | Create File Share |`TAGS=v0 vagrant up $BOX`| Creates all nodes but only runs the Ansible provisioner on `gsd-ansible-file-share1`. |
| `v1` | Create Web Server |`TAGS=v1 vagrant provision $BOX`| Run the provisioner on `gsd-ansible-repo`. This creates the Apache2 based web server. |
| `v2` | Download software |`TAGS=v2 vagrant provision $BOX`| Runs Ansible on `gsd-ansible-download[1:3]`                                           |

To provision a specific tag you just prefix the command with `TAGS=` for example to provision step `v1` run the command:

```bash
TAGS=v1 vagrant provision $BOX
```

After each step you can create snapshots using Vagrant. Especially a snapshot
after `v0` because node creation takes some time. This also includes
registration of the RHEL 9 based `gsd-ansible-repo` and Sysprep of all Windows hosts.

There are other tags so for example if you only want to perform only the download and only from `gsd-ansible-download1`

```bash
TAGS=common,download vagrant up $BOX --provision | tee provision.log
```

</p></details></p>

## Additional Information

For additional insights and guidance:

* Explore the design and advantages of this setup in
  [Designing a Flexible Software Repository for Ansible]({{< relref path="/docs/projects/rws/software" >}}).
* Learn about Vagrant's capabilities in handling    Sysprep for Windows and
  automated registration for RHEL 9 in the
  [Vagrant setup guide]({{< relref path="/docs/howto/dev-environment/setup/vagrant" >}}).
* For an overview of the types of projects in Ansible, refer to
  [Ansible Projects]({{< relref path="/docs/concepts/ansible/projects" >}}).
* To facilitate downloads using HTTPS without SSL/TLS errors a trust
  relationship is created by Ansible. Refer to
  [Managing SSL/TLS Trust with Ansible on Windows Hosts]({{< relref path="docs/howto/rws/windows/trusts" >}}) for more information.
