---
categories: [Reference]
title: How to Manage Multistage Environments with Ansible | DigitalOcean
linkTitle: DigitalOcean
translate: false
draft: true
weight: 1
description: >
  In this tutorial four inventory strategies are explored by author.
---

In {{< external-link
url="https://www.digitalocean.com/community/tutorials/how-to-manage-multistage-environments-with-ansible#ansible-recommended-strategy-using-groups-and-multiple-inventories"
text="How to Manage Multistage Environments with Ansible | DigitalOcean"
htmlproofer_ignore="false" >}}
by by Justin Ellingwood four strategies are explored. These are listed below
with the challenges each of these approaches pose. The is followed by an
analysis commentary of these strategies and the conclusions of the article.

## Strategies

### 1. Relying Solely on Group Variables

- Group intersection can cause conflicts.
- No explicit way to specify variable precedence.
- Alphabetical evaluation of groups can lead to unpredictable behavior.

### 2. Using Group Children to Establish a Hierarchy

- Does not solve the problem of group intersection.
- Unintuitive and muddles the distinction between true children and
  hierarchy-establishing children.
- Compromises Ansible's goal of clear and easy-to-follow configuration.

### 3. Using Ansible Constructs that Allow Explicit Loading Order

- Requires placing variables in a different location, adding complexity.
- Every playbook needs a section to explicitly load the correct variable files.
- Ad-hoc tasks relying on variables become almost impossible.

### 4. Ansible Recommended Strategy: Using Groups and Multiple Inventories

- Duplication in the directory tree.
- Inability to select all hosts by function across environments.
- Variable sharing across environments is not possible without additional steps.

## Analysis

With regards to the inventory strategy, the author takes the position that a
key concern of the Ansible community is "group intersection". When a node is
part of multiple groups, and the configuration for those groups have the same
variable, the results unpredictable.

### Group Intersection Example

An example is shown below where two Ansible groups `fme` and `gs_server` define
a variable `checkmk_folder_path`. The two definitions have the same precedence
value / priority and then the results is determined by alphabetical order of the
groups in which case the value of the `gs_server` will override the value of the
`fme` group.

1. `group_vars/fme/main.yml`
  ```yaml
  checkmk_folder_path: "lcm/fme"
  ```
2. `group_vars/gs_server/main.yml`
  ```yaml
  checkmk_folder_path: "lcm/age/server"
  ```

This is an actual real-life occurrence of the group intersection problem but
this occurred only once in the project. And this was the result of an illogical
setup of the groups. In this case an node **FME** node that was part of the FME
group / role `fme` was also placed in the **ArcGIS Server** group `gs_server`
because of the reason that the FME node needed to use the
{{< external-link
url="https://developers.arcgis.com/python/"
text="ArcGIS API for Python"
htmlproofer_ignore="false" >}}
and this is made available by installing the ArcGIS software and disabling the
Windows service. The more logical approach to achieve this not by adding a FME
node to **ArcGIS Server** group but by change the FME play to install the
software. This is the more logical approach because the nature / the role of the
node of being an FME Engine does not change when the FME requires an extra
Python library to communicate with and **ArcGIS Server**.

Now we could imagine a scenario where, for example where we wanted to create
a small environment with a limited numbers of nodes, where we would want a node
to fulfill multiple roles for example to continue with the above example we want
the node to be an **FME Engine** and a **ArcGIS Server** server at the same
time.

In that case, the group intersection causes "unpredictable" result based on
alphabetical order to set the variable with `checkmk_folder_path`
`lcm/age/server`.

The purpose of the variable `checkmk_folder_path` is to register the node in
CheckMK monitoring under a path that matches the role of the node, but with this
scenario, this won't work, because the node has two roles.

The interesting point to make at this point regarding the approach for fixing
group intersection problems by using multiple inventories, one for each
environment, does not fix this problem!

And actually splitting inventory based on environment does not prevent group
intersection at all because nodes are always part 1 environment, not more, not
less. The key question there is:

{{< alert >}}
Given that a node is always part of only one environment, how are we preventing
variable conflicts by separating the configuration of different environments?
What specific group intersections are we preventing by this approach?
{{< /alert >}}

The answer is that we are not preventing any group intersection problems by
separating the inventory based on environment.

So the fourth strategy, the Ansible recommended strategy does not fix "any"
group intersection problems.

The actual problem we want to manage environment differences. This is not group
intersection / precedence problem

if you have `development`, `staging`, `production` and a group `fme`.
`fme_flow_max_memory: 8gb`  Memory setting or some kind of other that is
different for each environment? You put the setting in the environment group.

### Precedence Rules

The
{{< external-link
url="https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_variables.html#understanding-variable-precedence"
text="Ansible documentation"
htmlproofer_ignore="false" >}}
lists 22 places where configuration can be stored, variables can be defined. The
table below is a simplified list of precedence order.

|    |                  |                                                                                                                                                       |
|----|------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| 2  | Role defaults    | ✔️                                                                                                                                                    |
| 4  | `group_vars/all` | ✔️                                                                                                                                                    |
| 6  | `group_vars/*`   | ✔️                                                                                                                                                    |
| 9  | `host_vars/*`    | ❌, unless                                                                                                                                             |
| 12 | Plays `vars`     | ❌, unless                                                                                                                                             |
| 15 | Role `vars`      | ❌ → [StackOverflow](https://stackoverflow.com/questions/29127560/whats-the-difference-between-defaults-and-vars-in-an-ansible-role/58078985#58078985) |
| 18 | `include_vars`   | ✔️ → [Ansible Vault](https://c2platform.org/docs/guidelines/setup/secrets/)                                                                                |
