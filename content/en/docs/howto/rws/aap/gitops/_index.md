---
categories: ["How-to"]
tags: [awx, ansible, dtap, promotion model, Execution Environment, requirements.yml]
title: "GitOps Pipeline for an Execution Environment (EE) with Ansible Collections"
linkTitle: "GitOps Pipeline"
translate: false
weight: 2
description: >
    Learn how to realize a GitOps Pipeline when using an EE that includes Ansible Collections.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform/ansible-collection-core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}),
[`c2platform/ansible-collection-mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}),
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}}),
[`c2platform/ansible-collection-mgmt`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mgmt" >}})

---

## Introduction

When using the Ansible Automation Platform (AAP), there are three strategies for
managing Ansible collections:

1. **Include collections in the EE**.
2. **Pull collections using `requirements.yml`**[^1].
3. **A combination of both**.

Each approach has its pros and cons. A significant downside of using an EE is
that the EE and its version are defined externally in AAP, preventing a pure
GitOps approach. This how-to guide describes how to use an AAP Workflow and an
Ansible Job Template to configure AAP to use a specific EE version before other
Job Templates run. The Job Template utilizes the Ansible role
`c2platform.mgmt.awx` to manage the EE version. Red Hat recommends using only
the EE for performance reasons[^2]

## Collection Strategies

### Only use the `requirements.yml` file

This is easy and flexible, suitable at the start of a project when many updates
are made to collections. The downside is longer job times as collections are
fetched each time.

```yaml
---
collections:
   - name: c2platform.core
      version: 1.0.21
   - name: https://gitlab.com/c2platform/rws/ansible-collection-wincore.git
      type: git
      version: master
```

<p><details>
<summary><kbd>Show Diagram</kbd></summary><p>

```plantuml
@startuml rws-gis-automation-requirements-yml
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads

'title [Container] GIS Platform Automation for Operations

AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

Person(rws_operator, "Ansible Operator", "RWS GIS Platform engineer (basic Ansible knowledge)")

System_Ext(galaxy, "Galaxy\nansible.galaxy.com", "Hosts Ansible content")
System(gis, "GIS_LABEL", "Provides GEO information and processing services")
System_Ext(azure_devops, "Azure DevOps\ndev.azure.com/Rijkswaterstaat", "Hosts RWS projects") {
ContainerDb(gis_inventory, "GIS_INVENTORY_LABEL", "GIS_INVENTORY_TECH", "GIS_INVENTORY_DESC")
}
System_Ext(gitlab, "GitLab\ngitlab.com/c2platform/rws", "Hosts the GIS Platform open source projects") {
ContainerDb(gis_ee, "GIS_EE_LABEL", "GIS_EE_TECH", "GIS Platform Execution Environment (EE) with Pythoon, Ansible")
ContainerDb(gis_collections, "Ansible Collections", "Ansible,Git", "GIS Platform Ansible Collections e.g. GIS Collection and Wincore Collection")
}

System_Ext(aap, "Ansible\nAutomation Platform", "Manages nodes and deploys Ansible playbooks across environments")

Lay_R(gis_ee, gis_collections)
Rel(aap, gis_inventory, "1) Source Control Update and \n3) Inventory Sync", "https,git,vault")
Rel(aap, galaxy, "2) Fetch collections", "https")
Rel(aap, gis_ee, "4) Download\nRWS GIS Ansible EE", "Docker pull")
Rel_R(aap, gis, "5) Deploy, update, upgrade services of")
Rel(rws_operator,gis_inventory,"Performs LCM and TAM tasks by making changes to", "Browser, Web Edit/IDE, Git Push and Pull")
Rel_R(rws_operator, aap, "Execute jobs\nview logs etc.", "browser, https")
Rel_R(gis_collections, galaxy, "Release collections", "GitLab CI/CD Pipeline")

LAYOUT_WITH_LEGEND()
@enduml
```

</p></details></p>

### Only use the EE

Jobs run faster on AWX. The downside is the need to release new versions of the
collection and EE and manually configure the new EE version in AWX[^3].

<p><details>
   <summary><kbd>Show Diagram</kbd></summary><p>

```plantuml
@startuml rws-gis-automation-ee
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads

'title [Container] GIS Platform Automation for Operations

AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

Person(rws_operator, "Ansible Operator", "RWS GIS Platform engineer (basic Ansible knowledge)")

System_Ext(galaxy, "Galaxy\nansible.galaxy.com", "Hosts Ansible content")
System(gis, "GIS_LABEL", "Provides GEO information and processing services")
System_Ext(azure_devops, "Azure DevOps\ndev.azure.com/Rijkswaterstaat", "Hosts RWS projects") {
ContainerDb(gis_inventory, "GIS_INVENTORY_LABEL", "GIS_INVENTORY_TECH", "GIS_INVENTORY_DESC")
}
System_Ext(gitlab, "GitLab\ngitlab.com/c2platform/rws", "Hosts the GIS Platform open source projects") {
ContainerDb(gis_ee, "GIS_EE_LABEL", "GIS_EE_TECH", "GIS_EE_DESC")
ContainerDb(gis_collections, "Ansible Collections", "Ansible,Git", "GIS Platform Ansible Collections e.g. GIS Collection and Wincore Collection")
}

System_Ext(aap, "Ansible\nAutomation Platform", "Manages nodes and deploys Ansible playbooks across environments")

Rel(aap, gis_inventory, "1) Source Control Update and \n2) Inventory Sync", "https,git,vault")
Rel(aap, gis_ee, "3) Download\nRWS GIS Ansible EE", "Docker pull")
Rel_R(aap, gis, "4) Deploy, update, upgrade services of")
Rel(rws_operator,gis_inventory,"Performs LCM and TAM tasks by making changes to", "Browser, Web Edit/IDE, Git Push and Pull")
Rel_R(rws_operator, aap, "Execute jobs\nview logs etc.", "browser, https")
Rel(gis_ee, galaxy, "Fetch collections", "GitLab CI/CD Pipeline")
Rel(gis_collections, galaxy, "Release collections", "GitLab CI/CD Pipeline")

LAYOUT_WITH_LEGEND()
@enduml
```

</p></details></p>

### Use both the EE and the `requirements.yml`**

Add community collections to the EE and use `requirements.yml` for frequently
updated collections.

A downside to adding collections to the EE is that it complicates a DTAP
promotion model setup in Git. This can be overcome by setting up a workflow in
AWX that configures the correct EE version.

## Prerequisites

* Ensure `gsd-awx1` is up and running. Refer to [Setup the Automation Controller
  (AWX) using Ansible]({{< relref path="/docs/howto/rws/aap/awx" >}}).
* Configure SSH connections if running Ansible plays without Vagrant. Refer to
  [Using Ansible without Vagrant]({{< relref
  path="/docs/howto/rws/dev-environment/ansible-without-vagrant" >}}).

## Change the EE

This section demonstrates how to change the EE in AAP in a workflow job so that
the new EE becomes available to jobs that run after that job.

This section demonstrates how to change the EE in AAP in a workflow job so that the new EE becomes available to jobs that run after that job.

1. In the `ansible-gis` project, switch to the `development` branch and edit
   `group_vars/awx/awx.yml`. Define `gs_ee_images` as follows:
    ```yaml
    gs_ee_images:
      default: registry.gitlab.com/c2platform/rws/ansible-execution-environment:0.1.18
      development: quay.io/ansible/awx-ee:24.4.0
    ```
    Remove or disable the line with `development`, commit, and push back to the
    development branch.
2. Navigate to {{< external-link
   url="https://awx.c2platform.org/#/execution_environments"
   htmlproofer_ignore="true" >}}. Verify that `quay.io/ansible/awx-ee:24.4.0` is
   the EE.
3. Navigate to {{< external-link url="https://awx.c2platform.org/#/templates"
   htmlproofer_ignore="true" >}} and click on the Visualizer link of
   `gsd-awx-collections-workflow`. This shows a workflow with five nodes.
4. Launch the workflow.
5. When the workflow completes, verify that `gsd-collections1` shows
   `quay.io/ansible/awx-ee:24.4.0` and `gsd-collections2` shows
   `registry.gitlab.com/c2platform/rws/ansible-execution-environment:0.1.18`.

This demonstrates that we can include the EE in our DTAP promotion model
similarly to how we use the `requirements.yml` file by setting up a workflow
that includes a job like `gsd-awx-ee` to manage the EE for the environment.

## Promote the EE

This section shows how we can promote changes, including the EE, from `development` to `test`.

1. Create the configuration in AWX for the `test` environment using node
   `gst-awx1`[^4]:
   ```bash
   export PLAY=plays/mgmt/awx_config.yml
   ansible-playbook $PLAY -i hosts.ini --limit=gst-awx1
   ```
   {{< alert title="Note:" >}}Running Ansible commands directly will only work
   if you set up the necessary SSH configuration for it. Refer to [Using Ansible
   without Vagrant]({{< relref
   path="/docs/howto/rws/dev-environment/ansible-without-vagrant" >}}) for more
   information.{{< /alert >}}
2. Using the AWX web interface, launch `gst-awx-collections-workflow`.
3. After completion, verify no difference between collections in
   `gst-collections1` and `gst-collections2`.
4. In the `development` branch, change the default image to `0.1.19`. Commit and
   push.
    ```yaml
    gs_ee_images:
      default: registry.gitlab.com/c2platform/rws/ansible-execution-environment:0.1.19
      development: quay.io/ansible/awx-ee:24.4.0
    ```
5. Merge changes to the `test` branch and launch `gst-awx-collections-workflow`.
6. After completion, verify differences between outputs of `gst-collections1`
   and `gst-collections2`.

## Review

In the Ansible Inventory project [`c2platform/rws/ansible-gis`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-gis" >}}):

1. The play `plays/mgmt/awx.yml`, which shows Ansible roles utilized to create
   the `gsd-awx1` node.
2. Files inside `group_vars/awx`. These files contain configuration.
3. The file `Vagrantfile.yml`, which defines the Vagrant LXD machine.

The Ansible role `c2platform.mgmt.awx` in the Ansible Management Collection
[`c2platform/ansible-collection-mgmt`]({{< relref
path="/docs/gitlab/c2platform/ansible-collection-mgmt" >}}). This role uses
modules from the `awx.awx` collection to configure AWX (job templates,
workflows).

The Ansible roles `c2platform.mw.microk8s` and `c2platform.mw.kubernetes` in the
Ansible Middleware collection [`c2platform/ansible-collection-mw`]({{< relref
path="/docs/gitlab/c2platform/ansible-collection-mw" >}}). The first role
creates a Kubernetes instance; the second installs AWX on the cluster.


## Tips

If you are experimenting or developing with AWX configuration, you can use tags
to speed up provisioning:

```bash
export PLAY=plays/mgmt/awx_config.yml
ansible-playbook $PLAY -i hosts.ini --limit=gsd-awx1 --tags secrets,awx_workflow_job_template,awx_workflow_job_template_node
```

You can also use tags when using Vagrant:

```bash
export PLAY=plays/mgmt/awx_config.yml
TAGS=secrets,awx_workflow_job_template,awx_workflow_job_template_node vagrant provision gsd-awx1
```

## Notes

[^1]: The requirements file should be stored in the folder `collections`, as is
    the case in the `ansible-gis` project.

[^2]: There is no official source for this position. This is communicated by a
    Red Hat consultant to RWS after issues were raised about the time it takes
    to fetch Ansible collections from Galaxy and Ansible Automation Hub at the
    RWS site.

[^3]: At least this is the current setup of the project. It is possible to
    change the GitLab CI/CD pipeline for the EE to release a `latest` version
    based not on released Galaxy collections but by directly pulling collections
    from the Git repository master branch. Using webhooks, changes to
    collections would trigger the pipeline to produce a new `latest` version. In
    AWX, this latest version can then be configured for use.

[^4]: The node `gst-awx1` is not a normal Vagrant machine; it is only an alias
    for `gsd-awx1`. In `hosts.ini`, we define it as a separate node inside the
    `test` environment so that we can create AWX configuration for this
    environment. For this reason, we also cannot manage `gst-awx1` using
    Vagrant; it doesn't exist for Vagrant but only for Ansible.

<!--
## Improvement Suggestions

- Consider adding diagrams or flowcharts to visually represent different approaches and workflows.
- Include more detailed examples or case studies demonstrating each approach's pros and cons.
- Provide troubleshooting tips or common issues encountered during implementation.
- Add links to official documentation or resources for further reading on specific topics like AWX configuration or Ansible collections.
- Consider breaking down complex sections into smaller sub-sections for better readability.
-->
