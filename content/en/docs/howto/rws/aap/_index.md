---
categories: ["How-to"]
tags: [aap, awx]
title: "Setup Ansible Automation Platform (AAP)"
linkTitle: "AAP"
translate: false
weight: 3
description: >
  This section provides comprehensive instructions for creating, managing, and
  harnessing the power of the Ansible Automation Platform (AAP). AAP comprises two
  key components: the Automation Controller (AWX) and the Ansible Automation Hub
  (Galaxy NG).
---


