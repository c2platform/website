---
categories: ["How-to"]
tags: [ansible, monitoring, checkmk]
title: Setup CheckMK Monitoring using Ansible
linkTitle: CheckMK
translate: false
provisionTime: 5 minute
weight: 12
description: >
    This guide walks you through the setup of CheckMK monitoring for the RWS GIS
    Platform using Ansible, ensuring efficient monitoring configuration.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

{{< under_construction >}}

## Overview

This tutorial explains how to use Vagrant and Ansible with the `checkmk.general`
Ansible collection to deploy a CheckMK server on an LXD node running Ubuntu
22.04.1.

| Node                 | OS                 | Provider | Purpose        |
|----------------------|--------------------|----------|----------------|
| `gsd-checkmk-server` | Ubuntu 22.04.1 LTS | LXD      | CheckMK Server |


## Prerequisites

* Ensure your RWS Development Environment is set up on Ubuntu 22, as detailed
  [here]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}).
* Ensure the proxy node `gsd-rproxy1` is active for Windows node internet access:
  ```bash
  vagrant up gsd-rproxy1
  ```

## Setup

Run vagrant up to create the CheckMK server. This setup should take
approximately 5 minutes.

1. Initiate the CheckMK server creation with Vagrant. The process takes about 5
   minutes.
    ```bash
    vagrant up gsd-checkmk-server
    ```
2. After deployment, modify the IP address for the `gs` site from the default
   `127.0.0.1` to `1.1.5.208`:

    ```bash
    vagrant ssh gsd-checkmk-server
    omd config gs show APACHE_TCP_ADDR
    omd stop gs
    omd config gs set APACHE_TCP_ADDR 1.1.5.208
    omd update-apache-config gs
    omd start gs
    omd status gs
    ```

## Verification

To verify the installation:

1. Open the [RWS FireFox profile]({{< relref
   path="/docs/howto/rws/dev-environment/setup/firefox" >}}) and navigate to {{<
   external-link url="http://1.1.5.208:5000/gs" htmlproofer_ignore="true" >}}.
   Log in as `cmkadmin` with the password `secret`.
2. Navigate to {{< external-link url="https://checkmk.c2platform.org/gs/"
   htmlproofer_ignore="true" >}} and again, log in as `cmkadmin` with the
   password `secret`.


## Additional Information

For additional insights and guidance:

* {{< external-link
  url="https://docs.checkmk.com/latest/en/intro_setup.html"
  text="Setting up Checkmk"
  htmlproofer_ignore="false" >}}
* {{< external-link
  url="https://galaxy.ansible.com/ui/repo/published/checkmk/general/"
  text="Checkmk Ansible Collection"
  htmlproofer_ignore="false" >}}


<!--
## TODO

```bash
vagrant@gsd-checkmk-server:~$ nc -zv gsd-ca-server-client 6556
Connection to gsd-ca-server-client (1.1.8.117) 6556 port [tcp/*] succeeded!
vagrant@gsd-checkmk-server:~$
```

```bash
vagrant@gsd-checkmk-server:~$ nc -zv gsd-ca-server-client 161
f
nc: connect to gsd-ca-server-client (1.1.8.117) port 161 (tcp) failed: Connection timed out
```

echo -n -e '\x30\x26\x02\x01\x01\x04\x06\x70\x75\x62\x6C\x69\x63\xa5\x19\x02\x04\x71\x77\x68\x77\x02\x01\x00\x02\x01\x7F\x30\x0d\x30\x0b\x06\x07\x2B\x06\x01\x02\x01\x01\x05\x00' | nc -u -w5 gsd-ca-server-client 161

-->