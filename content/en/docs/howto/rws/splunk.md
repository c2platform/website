---
categories: ["How-to"]
tags: [ansible, monitoring, splunk]
title: Setting up Splunk Enterprise in RWS Development Environment
linkTitle: Splunk Setup
translate: false
provisionTime: 2 minutes
weight: 13
description: >
  This guide aids the GIS Platform team in integrating and experimenting with
  Splunk Enterprise monitoring within the RWS development environment.
---

Projects:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.mgmt`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mgmt" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

## Overview

This guide walks you through deploying a Splunk Enterprise server using Vagrant
and Ansible, specifically employing the `c2platform.mgmt.splunk` Ansible role,
on an LXD node running Ubuntu 22.04.1.

| Node         | OS                 | Provider | Purpose                        |
|--------------|--------------------|----------|--------------------------------|
| `gsd-splunk` | Ubuntu 22.04.1 LTS | LXD      | Splunk Enterprise (standalone) |

## Prerequisites

* Ensure your RWS Development Environment is set up on Ubuntu 22, as outlined
  [here]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}).
* Ensure the reverse/web proxy node `gsd-rproxy1` is active:

  ```bash
  vagrant up gsd-rproxy1
  ```

  Refer to
  {{< rellink path="/docs/howto/rws/dev-environment/setup/rproxy1" >}}
  for more details. This node serves as the web and reverse proxy. It's required
  for internet access to download the Splunk software and install packages. As
  a reverse proxy, it allows you to access the Splunk web interface using
  TLS/HTTPS without certificate errors.

## Setup Splunk Enterprise

Start the Splunk Enterprise server setup using Vagrant. This process should take
approximately 1.5 minutes.

```bash
vagrant up gsd-splunk
```

## Login to Splunk Enterprise

To confirm the installation:

Open the
[RWS Firefox profile]({{< relref path="/docs/howto/rws/dev-environment/setup/firefox" >}})
and go to
{{< external-link url="https://splunk.c2platform.org" htmlproofer_ignore="true" >}}.
Log in as `admin` with the password `Supersecret!`. You should log in successfully
and see the Splunk Enterprise interface, as depicted below:

{{< image filename="/images/docs/splunk.png?width=600px" >}}

## Receive Data

To test whether data can be sent to your new Splunk Enterprise node, provision
the Universal Forwarder on the Reverse/Web Proxy node `gsd-rproxy1`.

```bash
export PLAY="plays/mgmt/splunk_uf.yml"
vagrant provision gsd-rproxy1
```

Use Splunk Web to configure Splunk to receive data on port `9997`. Navigate to
**Settings** → **Forwarding and receiving** → **Receive data** → **Add new** and
configure port `9997`.

{{< image filename="/images/docs/splunk-9997.png?width=600px" >}}

Log in to the reverse proxy `gsd-rproxy1` and set up the Universal Forwarder to
start monitoring `/var/log`.

```bash
vagrant ssh gsd-rproxy1
```

```bash
sudo su -
```

```bash
/opt/splunkforwarder/bin/splunk add forward-server gsd-splunk:9997
/opt/splunkforwarder/bin/splunk add monitor /var/log
/opt/splunkforwarder/bin/splunk restart
```

<p><details>
  <summary><kbd>Show me</kbd></summary><p>

```bash
vagrant@gsd-rproxy1:~$ sudo su -
root@gsd-rproxy1:~# /opt/splunkforwarder/bin/splunk add forward-server gsd-splunk:9997
Warning: Attempting to revert the SPLUNK_HOME ownership
Warning: Executing "chown -R root:root /opt/splunkforwarder"
Added forwarding to: gsd-splunk:9997.
root@gsd-rproxy1:~# /opt/splunkforwarder/bin/splunk add monitor /var/log
Warning: Attempting to revert the SPLUNK_HOME ownership
Warning: Executing "chown -R root:root /opt/splunkforwarder"
Added monitor of '/var/log'.
root@gsd-rproxy1:~# /opt/splunkforwarder/bin/splunk restart
Warning: Attempting to revert the SPLUNK_HOME ownership
Warning: Executing "chown -R root:root /opt/splunkforwarder"
Stopping splunkd...
Shutting down.  Please wait, as this may take a few minutes.

Stopping splunk helpers...

Done.
splunkd.pid doesn't exist...

Splunk> Be an IT superhero. Go home early.

Checking prerequisites...
        Checking mgmt port [8089]: open
        Checking conf files for problems...
        Done
        Checking default conf files for edits...
        Validating installed files against hashes from '/opt/splunkforwarder/splunkforwarder-9.3.2-d8bb32809498-linux-2.6-x86_64-manifest'
        All installed files intact.
        Done
All preliminary checks passed.

Starting splunk server daemon (splunkd)...
Done
```

</p></details></p>

In Splunk Web, navigate to **Search & Reporting**, and click on **Data Summary**.
You should see under hosts that Splunk is receiving data from `gsd-rproxy1`.

{{< image filename="/images/docs/splunk-data.png?width=600px" >}}

## Review Ansible Play

The Ansible Play facilitates the setup of a Splunk Enterprise node with minimal
commands by integrating changes in the Ansible inventory project, which is also
a {{< rellink path="/docs/concepts/dev/vagrant" >}} project. This integration
simplifies the addition of services like Splunk Enterprise into the RWS
development environment. For detailed information, you can review the commit
[34282de8](https://gitlab.com/c2platform/rws/ansible-gis/-/commit/34282de8e2471fee36b571c841de6511c916018f)
of the Ansible inventory project
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}).
The commit included the following changes:

| Component         | File                                 | Comment                                                                                                                         |
|-------------------|--------------------------------------|---------------------------------------------------------------------------------------------------------------------------------|
| Vagrant           | `Vagrantfile.yml`                    | Added `splunk.c2platform.org` as an "alias" for `gsd-rproxy1`.                                                                  |
| Web/Forward Proxy | `group_vars/reverse_proxy/files.yml` | Added ports `8000` and `8089` to `proxy.conf`.                                                                                  |
| Reverse Proxy     | `group_vars/reverse_proxy/files.yml` | Created new `splunk.conf` Apache configuration for `splunk.c2platform.org`.                                                     |
| Splunk            | `hosts.ini`                          | Introduced 10 new groups for Splunk, e.g., `splunk`, `splunk_clustermanager`.                                                   |
| Splunk            | `group_vars/splunk*/main.yml`        | Configured those 10 new Splunk groups.                                                                                          |
| Splunk            | `group_vars/splunk_search/main.yml`  | Used variable `splunk_resources` to modify `web.conf` and added `tools.proxy.on = True` to enable access via a reverse proxy. |

## Review Ansible Role

The Ansible role
{{< ansible-role-link role_name="c2platform.mgmt.splunk" >}}
extends the `mason_splunk.ansible_role_for_splunk.splunk`. It imports this role
alongside the `c2platform.core.linux` Ansible role, offering enhanced
flexibility and power for managing multiple tasks. This includes configuration
in `web.conf` needed for accessing Splunk Enterprise via the reverse proxy. It
was created to leverage the flexible and powerful `c2platform.core.linux` role,
which provides access to 115 Ansible modules through a single resource variable
`splunk_resources`. This facilitates tasks such as modifying `web.conf` for
reverse proxy setup, allowing comprehensive management of Splunk Enterprise
settings.

## Additional Information

* {{< rellink path="/docs/concepts/dev/vagrant" desc=true >}}