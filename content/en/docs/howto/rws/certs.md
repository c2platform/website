---
categories: ["How-to", "Example"]
tags: [ansible, certificates, pki, java, keystore, tomcat, SSL/TLS]
title: Tomcat SSL/TLS and Java Keystore and TrustStore Configuration for Linux and Windows Hosts
linkTitle: Tomcat SSL/TLS
translate: false
provisionTime: 11 minute
weight: 4
description: >
    Learn how to efficiently manage and set up SSL/TLS certificates, Java KeyStores,
    and Java Truststores using Ansible. This guide includes steps for incorporating
    both automatically generated and externally obtained certificates.
---

Projects:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}),
[`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

## Overview

This tutorial demonstrates the setup of two Tomcat nodes – one on Ubuntu 22 and
another on Windows Server 2022 – for SSL and Java TrustStore configuration. This
how-to basically can be used to validate the RWS approach for a simple Ansible
based CA server approach as documented in
[Implementing PKI for RWS GIS with Ansible]({{< relref path="/docs/projects/rws/pki" >}})


Using Vagrant and Ansible the following will be performed:

1. Vagrant creates two nodes: an LXD box running Ubuntu 22 and a VirtualBox VM
   with Windows Server 2022.
2. Vagrant initiates the Ansible provisioner.
3. Ansible installs Tomcat on both nodes using different roles.
   1. For Linux, the role `c2platform.mw.tomcat` is used to download and extract
      {{< external-link
      text="apache-tomcat-9.0.87.tar.gz"
      url="https://dlcdn.apache.org/tomcat/tomcat-9/v9.0.87/bin/apache-tomcat-9.0.87.tar.gz"
      htmlproofer_ignore="false" >}}
      and then configure Tomcat.
   2. For Windows `c2platform.gis.tomcat` is used which downloads
      {{< external-link
      text="apache-tomcat-9.0.78.exe"
      url="https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.78/bin/apache-tomcat-9.0.78.exe"
      htmlproofer_ignore="false" >}} and uses that to install Tomcat on MS Windows.
   3. Both roles include the `c2platform.core.cacerts` role for SSL/TLS
      certificate generation, Java KeyStore creation, and Java TrustStore
      updates with the CA Root CA certificate.

| Node          | OS      | Distribution        | Provider   |
|---------------|---------|---------------------|------------|
| `gsd-tomcat1` | Linux   | Ubuntu 22           | LXD        |
| `gsd-tomcat2` | Windows | Windows Server 2022 | VirtualBox |

## Prerequisites

Before proceeding, make sure you have completed the steps to
[Setup the RWS Development Environment on Ubuntu 22]({{< relref path="/docs/howto/rws/dev-environment/setup" >}})
and have the reverse proxy running

```bash
vagrant up gsd-rproxy1
```

In the
[RWS Development Environment]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}),
the Reverse Proxy `gsd-rproxy1` also serves a secondary purpose by functioning
as the CA Server. This setup plays a crucial role in managing and
distributing certificates and Java Keystores / TrustStores.

## Setup

To initialize the Tomcat nodes `gsd-tomcat1` and `gsd-tomcat2`, execute the
command below. The provisioning process is expected to take approximately 11
minutes to complete.


```bash
vagrant up gsd-tomcat1 gsd-tomcat2
```

## Verify

If Vagrant was able to succesfully provision the two nodes, without errors, you
can verify if everything is working correclty with the following steps.

### SSL/TLS Configuration

Start FireFox using your FireFox profile ( see
[Configure a FireFox Profile]({{< relref path="/docs/howto/dev-environment/setup/firefox" >}}) )
and you should be able to navigate to the links below and see the "Hello World"
application. You should not see any SSL/TLS certificate error messages.

1. {{< external-link
   url="https://gsd-tomcat1.internal.c2platform.org:8443/helloworld/"
   htmlproofer_ignore="true" >}}
2. {{< external-link
   url="https://gsd-tomcat2.internal.c2platform.org:8443/helloworld/"
   htmlproofer_ignore="true" >}}

With this, you have verified that Tomcat SSL/TLS configuration was performed by
Ansible on `gsd-tomcat1` and `gsd-tomcat2` and is correctly working.

Navigate to the links below. You should see the same messages.

3. {{< external-link
   url="https://helloworld.c2platform.org/"
   htmlproofer_ignore="true" >}}
4. {{< external-link
   url="https://helloworld2.c2platform.org/"
   htmlproofer_ignore="true" >}}

This confirms that you are also able to access the Tomcat services running on
`gsd-tomcat1` and `gsd-tomcat2` via the reverse proxy server `gsd-rproxy1`.

### TrustStore

Ansible also updates the Java TrustStore on `gsd-tomcat1` and `gsd-tomcat2` with
CA root certificate. Wich you can verify is working correctly by using a custom
Java test class `HttpsConnectionTest`. Run

Execute the commands below to verify the Java TrustStore on `gsd-tomcat1`:

1. Enter the node using SSH:
   ```bash
   vagrant ssh gds-tomcat1
   ```
2. From directory `/vagrant/scripts/gsd-tomcat` run the `HttpsConnectionTest`:
    ```bash
    cd /vagrant/scripts/gsd-tomcat
    bash test-https.sh
    ```

    <details>
    <summary><kbd>Show me</kbd></summary>

    ```
    vagrant@gsd-tomcat1:/vagrant/scripts/gsd-tomcat$ bash test-https.sh
    google, Dec 14, 2023, trustedCertEntry,
    Certificate fingerprint (SHA-256): 98:2D:C0:52:3D:84:7D:B5:89:AE:22:3E:DD:D8:37:86:72:79:D7:32:40:D6:03:55:D8:A6:F1:70:53:DF:48:95
    c2-ca, Dec 14, 2023, trustedCertEntry,
    Certificate fingerprint (SHA-256): 64:1B:BC:79:EE:0A:37:EA:3D:AC:B9:E3:E5:7F:02:71:3E:7D:C8:82:89:62:C5:E8:11:75:45:37:58:18:71:6F
    Response Code: 200
    Response Message: OK
    Response Code: 200
    Response Message: OK
    Response Code: 200
    Response Message: null
    vagrant@gsd-tomcat1:/vagrant/scripts/gsd-tomcat$
    ```

    </details>

To verify the same on `gsd-tomcat2` run the following commands:

1. Enter the node using SSH:
   ```bash
   vagrant ssh gds-tomcat2
   ```
2. From directory `C:/vagrant/scripts/gsd-tomcat` run the `HttpsConnectionTest`:
    ```cmd
    cd /vagrant/scripts/gsd-tomcat
    test-https.bat
    ```
    <details>
    <summary><kbd>Show me</kbd></summary>

    ```cmd
    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>test-https.bat

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>set JAVA_HOME=D:\Apps\jdk-17.0.5\jdk-17.0.5+8

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>set PATH=C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Windows\System32\OpenSSH\;C:\ProgramData\chocolatey\bin;C:\Program Files\PowerShell\7\;C:\Program Files\OpenSSH;C:\Program Files\Git\cmd;C:\Users\vagrant\AppData\Local\Microsoft\WindowsApps;;D:\Apps\jdk-17.0.5\jdk-17.0.5+8\bin;D:\Apps\jdk-17.0.5\jdk-17.0.5+8\bin

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>keytool -list -cacerts -storepass changeit -alias google
    google, Mar 20, 2024, trustedCertEntry,
    Certificate fingerprint (SHA-256): 47:19:35:3D:CE:E0:3D:17:BD:60:C2:28:71:84:03:CD:2F:9F:81:EE:06:C8:A6:63:23:4E:56:95:15:8C:19:C6

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>keytool -list -cacerts -storepass changeit -alias c2-ca
    c2-ca, Dec 14, 2023, trustedCertEntry,
    Certificate fingerprint (SHA-256): 64:1B:BC:79:EE:0A:37:EA:3D:AC:B9:E3:E5:7F:02:71:3E:7D:C8:82:89:62:C5:E8:11:75:45:37:58:18:71:6F

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>javac HttpsConnectionTest.java

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>java HttpsConnectionTest https://www.google.nl
    Response Code: 200
    Response Message: OK

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>java HttpsConnectionTest https://c2platform.org
    Response Code: 200
    Response Message: OK

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>java HttpsConnectionTest https://gsd-tomcat2.internal.c2platform.org:8443/helloworld/
    Response Code: 200
    Response Message: null

    ```

    </details>

Alternatively, for `gsd-tomcat2`, as this is a Windows node, you can also use an
RDP connection:

1. Start RDP connection to `1.1.8.207` login as `vagrant` with password
   `vagrant`
2. Use the WIndow Commant Prompt shortcut on the desktop named **Scripts** and
   run the script `test-https.bat`. This will compile a simple Java test class
   that is then used to verify that Java TrustStore was updated correctly with
   the CA certificate. Running this script should not show any error messages
   and only a response code of 200 indicating succes.
    <details>
    <summary><kbd>Show me</kbd></summary>

    ```
    c:\vagrant\scripts\gsd-tomcat>test-https.bat

    c:\vagrant\scripts\gsd-tomcat>set JAVA_HOME=D:\Apps\jdk-17.0.5\jdk-17.0.5+8

    c:\vagrant\scripts\gsd-tomcat>set PATH=C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Windows\System32\OpenSSH\;C:\ProgramData\chocolatey\bin;C:\Program Files\PowerShell\7\;C:\Program Files\OpenSSH;C:\Program Files\Git\cmd;%USERPROFILE%\AppData\Local\Microsoft\WindowsApps;%JAVA_HOME%\bin;D:\Apps\jdk-17.0.5\jdk-17.0.5+8\bin;D:\Apps\jdk-17.0.5\jdk-17.0.5+8\bin;D:\Apps\jdk-17.0.5\jdk-17.0.5+8\bin

    c:\vagrant\scripts\gsd-tomcat>keytool -list -cacerts -storepass changeit -alias google
    google, Dec 14, 2023, trustedCertEntry,
    Certificate fingerprint (SHA-256): 98:2D:C0:52:3D:84:7D:B5:89:AE:22:3E:DD:D8:37:86:72:79:D7:32:40:D6:03:55:D8:A6:F1:70:53:DF:48:95

    c:\vagrant\scripts\gsd-tomcat>keytool -list -cacerts -storepass changeit -alias c2-ca
    c2-ca, Dec 14, 2023, trustedCertEntry,
    Certificate fingerprint (SHA-256): 64:1B:BC:79:EE:0A:37:EA:3D:AC:B9:E3:E5:7F:02:71:3E:7D:C8:82:89:62:C5:E8:11:75:45:37:58:18:71:6F

    c:\vagrant\scripts\gsd-tomcat>javac HttpsConnectionTest.java

    c:\vagrant\scripts\gsd-tomcat>java HttpsConnectionTest https://www.google.nl
    Response Code: 200
    Response Message: OK

    c:\vagrant\scripts\gsd-tomcat>java HttpsConnectionTest https://c2platform.org
    Response Code: 200
    Response Message: OK

    c:\vagrant\scripts\gsd-tomcat>java HttpsConnectionTest https://gsd-tomcat2.internal.c2platform.org:8443/helloworld/
    Response Code: 200
    Response Message: null

    c:\vagrant\scripts\gsd-tomcat>
    ```

    </details>


## Review

With regards to the Tomcat Nodes `gsd-tomcat1`,`gsd-tomcat2` and the CA Server `gsd-rproxy1` take note of the following files.

|Node         |Project                                                         |Play                 |File                     |
|-------------|---------------------------------------------------------------------------------------|---------------------|-------------------------|
|`gsd-rproxy1`|[GIS Platform]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})|`plays/mgmt/cacerts_server.yml`|`group_vars/all/certs.yml`|
|`gsd-tomcat1`|[GIS Platform]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})|`plays/mw/tomcat.yml`|`group_vars/tomcat_linux/certs.yml`|
|`gsd-tomcat2`|[GIS Platform]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})|`plays/mw/tomcat.yml`|`group_vars/tomcat_win/certs.yml`  |

### Node `gsd-tomcat1`

For node `gsd-tomcat1` the configuration is `group_vars/tomcat_linux/certs.yml`

```yaml
---
tomcat_cacerts2_certificates:
  - common_name: "{{ inventory_hostname }}"  # alias
    country_name: NL
    locality_name: Den Haag
    organization_name: C2
    organizational_unit_name: C2 GIS Platform Team
    state_or_province_name: Zuid-Holland
    subject_alt_name:
      - "DNS:{{ inventory_hostname }}"
      - "DNS:{{ inventory_hostname }}.internal.c2platform.org"
      - "IP:{{ ansible_eth1.ipv4.address }}"
    ansible_group: tomcat
    deploy:
      keystore:
        dir: "{{ tomcat_home_version }}/conf"
        password: secret
        owner: "{{ tomcat_user }}"
        group: "{{ tomcat_group }}"
        notify: restart tomcat instance
      truststore:
        dest: "{{ java_version | c2platform.core.java_home }}/lib/security/cacerts"
        sites:
          - name: www.google.com
            port: 443
            alias: google
```

This configuration results in the creation of a file named
`gsd-tomcat1-gsd-tomcat1.keystore` on the CA Server `gsd-rproxy1`.

```bash
vagrant@gsd-rproxy1:~$ ls /vagrant/.ca/c2/tomcat/ | grep tomcat1
gsd-tomcat1-gsd-tomcat1.crt
gsd-tomcat1-gsd-tomcat1.csr
gsd-tomcat1-gsd-tomcat1.key
gsd-tomcat1-gsd-tomcat1.keystore
gsd-tomcat1-gsd-tomcat1.p12
gsd-tomcat1-gsd-tomcat1.pem
gsd-tomcat1-gsd-tomcat1.truststore
```

This file is subsequently distributed to the `gsd-tomcat1 node`, as shown in the
following directory listing:


```bash
root@gsd-tomcat1:~# ls /opt/tomcat/tomcat/conf/
Catalina             gsd-tomcat1-gsd-tomcat1.keystore  jmxremote.password  tomcat-users.xsd
catalina.policy      jaspic-providers.xml              logging.properties  web.xml
catalina.properties  jaspic-providers.xsd              server.xml
context.xml          jmxremote.access                  tomcat-users.xml
root@gsd-tomcat1:~#
```

This configuration also updates / manages the Java TrustStore. Using `keytool` you can verify that
the certificate of
{{< external-link
url="ttps://www.google.com:443"
htmlproofer_ignore="false" >}}
and the C2 Root CA Certificate were imported into the Java Truststore.

```bash
keytool -list -cacerts -storepass changeit -alias google
keytool -list -cacerts -storepass changeit -alias c2-ca
```

<details>
  <summary><kbd>Show me</kbd></summary>

```bash
root@gsd-tomcat1:~# keytool -list -cacerts -storepass changeit -alias google
google, Dec 14, 2023, trustedCertEntry,
Certificate fingerprint (SHA-256): 98:2D:C0:52:3D:84:7D:B5:89:AE:22:3E:DD:D8:37:86:72:79:D7:32:40:D6:03:55:D8:A6:F1:70:53:DF:48:95
root@gsd-tomcat1:~# keytool -list -cacerts -storepass changeit -alias c2-ca
c2-ca, Dec 14, 2023, trustedCertEntry,
Certificate fingerprint (SHA-256): 64:1B:BC:79:EE:0A:37:EA:3D:AC:B9:E3:E5:7F:02:71:3E:7D:C8:82:89:62:C5:E8:11:75:45:37:58:18:71:6F
root@gsd-tomcat1:~#

```

</details>


### Node `gsd-tomcat2`

For node `gsd-tomcat2` the configuration is `group_vars/tomcat_win/certs.yml`

```yaml
tomcat_cacerts2_certificates:
  - common_name: "{{ inventory_hostname }}"  # alias
    country_name: NL
    locality_name: Den Haag
    organization_name: C2
    organizational_unit_name: C2 GIS Platform Team
    state_or_province_name: Zuid-Holland
    subject_alt_name:
      - "DNS:{{ inventory_hostname }}"
      - "DNS:{{ inventory_hostname }}.internal.c2platform.org"
      - "DNS:{{ inventory_hostname }}.ad.c2platform.org"
      - "IP:{{ ansible_host }}"
    ansible_group: tomcat
    deploy:
      keystore:
        dir: "{{ tomcat_home }}/conf"
        password: secret
        notify: Restart Tomcat service
      truststore:
        dest: "{{ java_install_dir }}/jdk-17.0.5+8/lib/security/cacerts"
        sites:
          - name: www.google.com
            port: 443
            alias: google
```

This configuration results in the creation of a file named
`gsd-tomcat2-GSD-TOMCAT2.keystore` on the CA Server `gsd-rproxy1`.

<details>
  <summary><kbd>Show me</kbd></summary>

```bash
root@gsd-rproxy1:~# ls /vagrant/.ca/c2/tomcat/ | grep tomcat2
gsd-tomcat2-GSD-TOMCAT2.crt
gsd-tomcat2-GSD-TOMCAT2.csr
gsd-tomcat2-GSD-TOMCAT2.key
gsd-tomcat2-GSD-TOMCAT2.keystore
gsd-tomcat2-GSD-TOMCAT2.p12
gsd-tomcat2-GSD-TOMCAT2.pem
gsd-tomcat2-GSD-TOMCAT2.truststore
root@gsd-rproxy1:~#
```

</details>

Which is deployed to directory `d:\Apps\tomcat\conf` on `gsd-tomcat2`.


<details>
  <summary><kbd>Show me</kbd></summary>

```
Microsoft Windows [Version 10.0.20348.707]
(c) Microsoft Corporation. All rights reserved.

vagrant@GSD-TOMCAT2 C:\Users\vagrant>dir d:\Apps\tomcat\conf
 Volume in drive D is Data
 Volume Serial Number is DCAD-2354

 Directory of d:\Apps\tomcat\conf

12/15/2023  03:15 AM    <DIR>          .
12/15/2023  03:14 AM    <DIR>          ..
12/15/2023  03:15 AM    <DIR>          Catalina
07/04/2023  01:15 PM            13,216 catalina.policy
07/04/2023  01:15 PM             7,849 catalina.properties
07/04/2023  01:15 PM             1,431 context.xml
12/15/2023  03:15 AM             4,530 gsd-tomcat2-GSD-TOMCAT2.keystore
07/04/2023  01:15 PM             1,172 jaspic-providers.xml
07/04/2023  01:15 PM             2,365 jaspic-providers.xsd
07/04/2023  01:15 PM             4,223 logging.properties
12/15/2023  03:15 AM             2,508 server.xml
12/15/2023  03:14 AM             8,034 server.xml.3200.20231215-031501.bak
12/15/2023  03:14 AM             2,813 tomcat-users.xml
07/04/2023  01:15 PM             2,617 tomcat-users.xsd
07/04/2023  01:15 PM           177,316 web.xml
              12 File(s)        228,074 bytes
               3 Dir(s)  321,481,920,512 bytes free

vagrant@GSD-TOMCAT2 C:\Users\vagrant>

```

</details>


## Deploy RWS Certificate

In this section we are going to deploy simulate deployment of an official / external RWS certificate.

1. Lookup and save the **thumbprint** of the current certificate:
    ```bash
    vagrant ssh gsd-tomcat1
    ```
    ```bash
    echo -n | openssl s_client -connect gsd-tomcat1.internal.c2platform.org:8443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/gsd-tomcat1.pem
    openssl x509 -noout -fingerprint -sha256 -inform pem -in /tmp/gsd-tomcat1.pem
    ```

    <details>
    <summary><kbd>Show me</kbd></summary>

    ```bash
    vagrant@gsd-tomcat1:~$ echo -n | openssl s_client -connect gsd-tomcat1.internal.c2platform.org:8443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/gsd-tomcat1.pem
    depth=1 CN = c2
    verify return:1
    depth=0 C = NL, ST = Zuid-Holland, L = Den Haag, O = C2, OU = C2 GIS Platform Team, CN = gsd-tomcat1
    verify return:1
    DONE
    vagrant@gsd-tomcat1:~$ openssl x509 -noout -fingerprint -sha256 -inform pem -in /tmp/gsd-tomcat1.pem
    sha256 Fingerprint=7B:63:B1:A0:F8:DC:CA:6F:14:92:A8:F5:A1:43:92:DD:1D:44:8F:90:BE:77:E4:96:08:D4:A7:84:F9:3D:23:05
    vagrant@gsd-tomcat1:~$
    ```

    </details>
2.  We are going the remove the self-signed certificate
    `gsd-tomcat1-gsd-tomcat1.crt` by renaming it to
    `gsd-tomcat1-gsd-tomcat1.crt.saved`. In subsequent tests we are going to
    treat as if it is a certificate from an external CSP. On the host in the
    root of the project run:

    ```bash
    sudo chown $USER:$USER .ca/c2/tomcat/*
    sudo mv .ca/c2/tomcat/gsd-tomcat1-gsd-tomcat1.crt .ca/c2/tomcat/gsd-tomcat1-gsd-tomcat1.crt.saved
    ```

    Provision `gsd-tomcat1`


    ```bash
    vagrant provision gsd-tomcat1 | tee provision.log
    ```

    Check the `provision.log` file where you should see that a new SSL / TLS
    certificate, a new Java KeyStore were created and deployed and that these
    changes triggered a restart of the Tomcat services. If you run the commands
    to check the thumbprint you should see a different value now. Indicating
    that the Tomcat service is indeed using a different certificate.

    <details>
      <summary><kbd>Show me</kbd></summary>

    ```bash
    ==> gsd-tomcat1: Running provisioner: shell...
        gsd-tomcat1: Running: inline script
    ==> gsd-tomcat1: Running provisioner: ansible...
        gsd-tomcat1: Running ansible-playbook...
    [DEPRECATION WARNING]: "include" is deprecated, use include_tasks/import_tasks
    instead. See https://docs.ansible.com/ansible-
    core/2.15/user_guide/playbooks_reuse_includes.html for details. This feature
    will be removed in version 2.16. Deprecation warnings can be disabled by
    setting deprecation_warnings=False in ansible.cfg.

    PLAY [Tomcat] ******************************************************************

    TASK [Gathering Facts] *********************************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.server_update : include_tasks] ***************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/server_update/tasks/update_cache.yml for gsd-tomcat1

    TASK [c2platform.core.server_update : Apt update cache] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : include_tasks] *******************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=python3-pip)
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=upgrade pip and setuptools)
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=['setuptools', 'pyOpenSSL==22.0.0', 'psycopg2-binary', 'lxml', 'cryptography==38.0.4'])
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=['nano', 'wget', 'tree', 'unzip', 'zip', 'jq', 'build-essential', 'python3-dev', 'python3-wheel', 'libsasl2-dev', 'libldap2-dev', 'libssl-dev', 'git', 'nfs-common', 'net-tools', 'telnet', 'curl', 'dnsutils', 'python2'])

    TASK [c2platform.core.bootstrap : OS package] **********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : Custom command upgrade pip and setuptools] ***
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : PIP package] *********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : OS package] **********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.os_trusts : CA distribute ( Debian )] ********************
    ok: [gsd-tomcat1] => (item=https://letsencrypt.org/certs/isrgrootx1.pem)
    ok: [gsd-tomcat1] => (item=file:///vagrant/.ca/c2/c2.crt)

    TASK [c2platform.core.secrets : Stat secret dir] *******************************
    ok: [gsd-tomcat1 -> localhost] => (item=/home/ostraaten/git/gitlab/c2/ansible-gis/secret_vars/development)
    ok: [gsd-tomcat1 -> localhost] => (item=/runner/project/secret_vars/development)

    TASK [c2platform.core.secrets : Include secrets] *******************************
    ok: [gsd-tomcat1] => (item=/home/ostraaten/git/gitlab/c2/ansible-gis/secret_vars/development)

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files.yml for gsd-tomcat1 => (item=marker)

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files_src.yml for gsd-tomcat1

    TASK [c2platform.core.files : Link] ********************************************
    ok: [gsd-tomcat1] => (item=/usr/bin/python → /usr/bin/python2)

    TASK [c2platform.core.files : Lineinfile] **************************************
    ok: [gsd-tomcat1] => (item=/home/vagrant/.bashrc)
    ok: [gsd-tomcat1] => (item=/root/.bashrc)

    TASK [c2platform.core.lcm : Ansible roles names without prefix] ****************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lcm : lcm_roles_node] ************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lvm : Get disks] *****************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lvm : Get usable devices] ********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lvm : Get nodes lvm roles] *******************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.hosts : Set up /etc/hostname.] ***************************
    ok: [gsd-tomcat1] => (item=/home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/hosts/templates/../templates/hostname.j2)

    TASK [c2platform.core.hosts : Set up /etc/hosts.] ******************************
    ok: [gsd-tomcat1] => (item=/home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/hosts/templates/../templates/hosts.j2)

    TASK [c2platform.core.alias : Set fact node roles] *****************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.common : Remove custom prompt] ***************************
    ok: [gsd-tomcat1]

    TASK [Include role(s)] *********************************************************

    TASK [c2platform.core.java : Set additional java facts] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.java : Install Java] *************************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/java/tasks/install.yml for gsd-tomcat1 => (item=jdk17)

    TASK [c2platform.core.java : Check installed] **********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.java : Create etc profile] *******************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : test if tomcat_versions is set correctly] *********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_home is set correctly] *************
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_version is set correctly] **********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_user is set correctly] *************
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_group is set correctly] ************
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_catalina_opts is set correctly] ****
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_non_ssl_connector_port is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_ssl_connector_port is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_shutdown_port is set correctly] ****
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_ajp_port is set correctly] *********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if port collisions occur is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_jre_home is set correctly] *********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_service_state is set correctly] ****
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_service_enabled is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_address is set correctly] **********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_enabled is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_directory is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_prefix is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_suffix is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_pattern is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : Group] ********************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : User] *********************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Home directory] ***********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Create home link] *********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Stat bin dir] *************************************
    ok: [gsd-tomcat1]

    TASK [Include certs tasks] *****************************************************

    TASK [c2platform.core.cacerts2 : Set fact cacerts2_certificates] ***************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : cacerts2_certificates] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : Set various certificate facts] ****************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : Set fact cacerts2_certificates] ***************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : include_tasks] ********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/certs_group.yml for gsd-tomcat1

    TASK [c2platform.core.cacerts2 : include_tasks] ********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/cert.yml for gsd-tomcat1 => (item=gsd-tomcat1)

    TASK [c2platform.core.cacerts2 : Stat key] *************************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Stat crt] *************************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Stat dir] *************************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate an OpenSSL private key] **************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate an OpenSSL Certificate Signing Request] ***
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate an OpenSSL certificate] **************
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate pkcs12 file] *************************
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Include Java KeyStore tasks] ******************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/java_keystore.yml for gsd-tomcat1

    TASK [c2platform.core.cacerts2 : Stat KeyStore] ********************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Manage Java KeyStore] *************************
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Create PEM file] ******************************
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : include_tasks] ********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/cert_deploy.yml for gsd-tomcat1 => (item=gsd-tomcat1)

    TASK [c2platform.core.cacerts2 : Copy to control node ( fetch )] ***************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)] => (item=key)
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)] => (item=crt)
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)] => (item=keystore)

    TASK [c2platform.core.cacerts2 : Stat parent dir] ******************************
    ok: [gsd-tomcat1] => (item=key)
    ok: [gsd-tomcat1] => (item=crt)
    ok: [gsd-tomcat1] => (item=keystore)

    TASK [c2platform.core.cacerts2 : Copy file] ************************************
    ok: [gsd-tomcat1] => (item=key)
    changed: [gsd-tomcat1] => (item=crt)
    changed: [gsd-tomcat1] => (item=keystore)

    TASK [c2platform.mw.tomcat : Configure conf/server.xml] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Configure conf/tomcat-users.xml] ******************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Configure bin/setenv.sh] **************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Stat manager] *************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Configure webapps/manager/META-INF/context.xml] ***
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Properties folder] ********************************
    ok: [gsd-tomcat1] => (item=/opt/tomcat/tomcat/conf)

    TASK [c2platform.mw.tomcat : Deploy WAR] ***************************************
    ok: [gsd-tomcat1] => (item=https://github.com/aeimer/java-example-helloworld-war/raw/master/dist/helloworld.war → helloworld.war)

    TASK [Files] *******************************************************************

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files.yml for gsd-tomcat1 => (item=jmx)

    TASK [c2platform.core.files : Stat no update files] ****************************
    ok: [gsd-tomcat1] => (item=/opt/tomcat/tomcat/conf/jmxremote.password)

    TASK [c2platform.core.files : Content file] ************************************
    ok: [gsd-tomcat1] => (item=/opt/tomcat/tomcat/conf/jmxremote.access)

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files_src.yml for gsd-tomcat1

    TASK [Service] *****************************************************************

    TASK [c2platform.core.service : Fact _services] ********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.service : Configure systemd service] *********************
    ok: [gsd-tomcat1] => (item=tomcat)

    TASK [c2platform.mw.tomcat : Manage service] ***********************************
    ok: [gsd-tomcat1]

    TASK [copy] ********************************************************************
    ok: [gsd-tomcat1]

    RUNNING HANDLER [c2platform.mw.tomcat : restart tomcat instance] ***************
    changed: [gsd-tomcat1]

    PLAY RECAP *********************************************************************
    gsd-tomcat1                : ok=93   changed=7    unreachable=0    failed=0    skipped=86   rescued=0    ignored=0
    ```

    </details>

3. Deploy the external certificate

   Simulate the manual download
   ```bash
   sudo mv .ca/c2/tomcat/gsd-tomcat1-gsd-tomcat1.crt.saved .ca/c2/tomcat/gsd-tomcat1-gsd-tomcat1.rws-csp.crt
   ```

   {{< alert title="Note:" >}}The string **`rws-csp`** is a special tag that indicates this is a certificate from an external / manual CSP{{< /alert >}}

    ```bash
    vagrant provision gsd-tomcat1 | tee provision.log
    ```

    Note the changes in the file `provision.log`

    1. Generate pkcs12 file
    2. Manage Java KeyStore
    3. Copy to control node ( fetch )


    <details>
    <summary><kbd>Show me</kbd></summary>

    ```bash
    ==> gsd-tomcat1: Running provisioner: shell...
        gsd-tomcat1: Running: inline script
    ==> gsd-tomcat1: Running provisioner: ansible...
        gsd-tomcat1: Running ansible-playbook...
    [DEPRECATION WARNING]: "include" is deprecated, use include_tasks/import_tasks
    instead. See https://docs.ansible.com/ansible-
    core/2.15/user_guide/playbooks_reuse_includes.html for details. This feature
    will be removed in version 2.16. Deprecation warnings can be disabled by
    setting deprecation_warnings=False in ansible.cfg.

    PLAY [Tomcat] ******************************************************************

    TASK [Gathering Facts] *********************************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.server_update : include_tasks] ***************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/server_update/tasks/update_cache.yml for gsd-tomcat1

    TASK [c2platform.core.server_update : Apt update cache] ************************
    changed: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : include_tasks] *******************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=python3-pip)
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=upgrade pip and setuptools)
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=['setuptools', 'pyOpenSSL==22.0.0', 'psycopg2-binary', 'lxml', 'cryptography==38.0.4'])
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=['nano', 'wget', 'tree', 'unzip', 'zip', 'jq', 'build-essential', 'python3-dev', 'python3-wheel', 'libsasl2-dev', 'libldap2-dev', 'libssl-dev', 'git', 'nfs-common', 'net-tools', 'telnet', 'curl', 'dnsutils', 'python2'])

    TASK [c2platform.core.bootstrap : OS package] **********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : Custom command upgrade pip and setuptools] ***
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : PIP package] *********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : OS package] **********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.os_trusts : CA distribute ( Debian )] ********************
    ok: [gsd-tomcat1] => (item=https://letsencrypt.org/certs/isrgrootx1.pem)
    ok: [gsd-tomcat1] => (item=file:///vagrant/.ca/c2/c2.crt)

    TASK [c2platform.core.secrets : Stat secret dir] *******************************
    ok: [gsd-tomcat1 -> localhost] => (item=/home/ostraaten/git/gitlab/c2/ansible-gis/secret_vars/development)
    ok: [gsd-tomcat1 -> localhost] => (item=/runner/project/secret_vars/development)

    TASK [c2platform.core.secrets : Include secrets] *******************************
    ok: [gsd-tomcat1] => (item=/home/ostraaten/git/gitlab/c2/ansible-gis/secret_vars/development)

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files.yml for gsd-tomcat1 => (item=marker)

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files_src.yml for gsd-tomcat1

    TASK [c2platform.core.files : Link] ********************************************
    ok: [gsd-tomcat1] => (item=/usr/bin/python → /usr/bin/python2)

    TASK [c2platform.core.files : Lineinfile] **************************************
    ok: [gsd-tomcat1] => (item=/home/vagrant/.bashrc)
    ok: [gsd-tomcat1] => (item=/root/.bashrc)

    TASK [c2platform.core.lcm : Ansible roles names without prefix] ****************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lcm : lcm_roles_node] ************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lvm : Get disks] *****************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lvm : Get usable devices] ********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lvm : Get nodes lvm roles] *******************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.hosts : Set up /etc/hostname.] ***************************
    ok: [gsd-tomcat1] => (item=/home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/hosts/templates/../templates/hostname.j2)

    TASK [c2platform.core.hosts : Set up /etc/hosts.] ******************************
    ok: [gsd-tomcat1] => (item=/home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/hosts/templates/../templates/hosts.j2)

    TASK [c2platform.core.alias : Set fact node roles] *****************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.common : Remove custom prompt] ***************************
    ok: [gsd-tomcat1]

    TASK [Include role(s)] *********************************************************

    TASK [c2platform.core.java : Set additional java facts] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.java : Install Java] *************************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/java/tasks/install.yml for gsd-tomcat1 => (item=jdk17)

    TASK [c2platform.core.java : Check installed] **********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.java : Create etc profile] *******************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : test if tomcat_versions is set correctly] *********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_home is set correctly] *************
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_version is set correctly] **********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_user is set correctly] *************
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_group is set correctly] ************
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_catalina_opts is set correctly] ****
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_non_ssl_connector_port is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_ssl_connector_port is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_shutdown_port is set correctly] ****
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_ajp_port is set correctly] *********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if port collisions occur is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_jre_home is set correctly] *********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_service_state is set correctly] ****
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_service_enabled is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_address is set correctly] **********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_enabled is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_directory is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_prefix is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_suffix is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_pattern is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : Group] ********************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : User] *********************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Home directory] ***********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Create home link] *********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Stat bin dir] *************************************
    ok: [gsd-tomcat1]

    TASK [Include certs tasks] *****************************************************

    TASK [c2platform.core.cacerts2 : Set fact cacerts2_certificates] ***************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : cacerts2_certificates] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : Set various certificate facts] ****************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : Set fact cacerts2_certificates] ***************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : include_tasks] ********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/certs_group.yml for gsd-tomcat1

    TASK [c2platform.core.cacerts2 : include_tasks] ********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/cert.yml for gsd-tomcat1 => (item=gsd-tomcat1)

    TASK [c2platform.core.cacerts2 : Stat key] *************************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Stat crt] *************************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Stat dir] *************************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate an OpenSSL private key] **************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate an OpenSSL Certificate Signing Request] ***
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate an OpenSSL certificate] **************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate pkcs12 file] *************************
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Include Java KeyStore tasks] ******************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/java_keystore.yml for gsd-tomcat1

    TASK [c2platform.core.cacerts2 : Stat KeyStore] ********************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Manage Java KeyStore] *************************
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Create PEM file] ******************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : include_tasks] ********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/cert_deploy.yml for gsd-tomcat1 => (item=gsd-tomcat1)

    TASK [c2platform.core.cacerts2 : Copy to control node ( fetch )] ***************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)] => (item=key)
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)] => (item=crt)
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)] => (item=keystore)

    TASK [c2platform.core.cacerts2 : Stat parent dir] ******************************
    ok: [gsd-tomcat1] => (item=key)
    ok: [gsd-tomcat1] => (item=crt)
    ok: [gsd-tomcat1] => (item=keystore)

    TASK [c2platform.core.cacerts2 : Copy file] ************************************
    ok: [gsd-tomcat1] => (item=key)
    changed: [gsd-tomcat1] => (item=crt)
    changed: [gsd-tomcat1] => (item=keystore)

    TASK [c2platform.mw.tomcat : Configure conf/server.xml] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Configure conf/tomcat-users.xml] ******************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Configure bin/setenv.sh] **************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Stat manager] *************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Configure webapps/manager/META-INF/context.xml] ***
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Properties folder] ********************************
    ok: [gsd-tomcat1] => (item=/opt/tomcat/tomcat/conf)

    TASK [c2platform.mw.tomcat : Deploy WAR] ***************************************
    ok: [gsd-tomcat1] => (item=https://github.com/aeimer/java-example-helloworld-war/raw/master/dist/helloworld.war → helloworld.war)

    TASK [Files] *******************************************************************

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files.yml for gsd-tomcat1 => (item=jmx)

    TASK [c2platform.core.files : Stat no update files] ****************************
    ok: [gsd-tomcat1] => (item=/opt/tomcat/tomcat/conf/jmxremote.password)

    TASK [c2platform.core.files : Content file] ************************************
    ok: [gsd-tomcat1] => (item=/opt/tomcat/tomcat/conf/jmxremote.access)

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files_src.yml for gsd-tomcat1

    TASK [Service] *****************************************************************

    TASK [c2platform.core.service : Fact _services] ********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.service : Configure systemd service] *********************
    ok: [gsd-tomcat1] => (item=tomcat)

    TASK [c2platform.mw.tomcat : Manage service] ***********************************
    ok: [gsd-tomcat1]

    TASK [copy] ********************************************************************
    changed: [gsd-tomcat1]

    RUNNING HANDLER [c2platform.mw.tomcat : restart tomcat instance] ***************
    changed: [gsd-tomcat1]

    PLAY RECAP *********************************************************************
    gsd-tomcat1                : ok=93   changed=7    unreachable=0    failed=0    skipped=86   rescued=0    ignored=0
        ```

    </details>
4. Verify that the deployed certificate is the correct / new certificate.

    <details>
    <summary><kbd>Show me</kbd></summary>

    ```bash
    vagrant@gsd-tomcat1:~$     echo -n | openssl s_client -connect gsd-tomcat1.internal.c2platform.org:8443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/gsd-tomcat1.pem
        openssl x509 -noout -fingerprint -sha256 -inform pem -in /tmp/gsd-tomcat1.pem
    depth=1 CN = c2
    verify return:1
    depth=0 C = NL, ST = Zuid-Holland, L = Den Haag, O = C2, OU = C2 GIS Platform Team, CN = gsd-tomcat1
    verify return:1
    DONE
    sha256 Fingerprint=B8:65:69:8B:8B:DF:94:5F:37:5C:CA:0F:04:EE:32:D2:2E:67:4D:04:D0:DF:38:1F:4F:DB:B6:54:02:78:63:B3
    vagrant@gsd-tomcat1:~$ openssl x509 -noout -fingerprint -sha256 -inform pem -in /vagrant/.ca/c2/tomcat/gsd-tomcat1-gsd-tomcat1.
    gsd-tomcat1-gsd-tomcat1.crt          gsd-tomcat1-gsd-tomcat1.keystore     gsd-tomcat1-gsd-tomcat1.rws-csp.crt
    gsd-tomcat1-gsd-tomcat1.csr          gsd-tomcat1-gsd-tomcat1.p12
    gsd-tomcat1-gsd-tomcat1.key          gsd-tomcat1-gsd-tomcat1.pem
    vagrant@gsd-tomcat1:~$ openssl x509 -noout -fingerprint -sha256 -inform pem -in /vagrant/.ca/c2/tomcat/gsd-tomcat1-gsd-tomcat1.rws-csp.crt
    sha256 Fingerprint=B8:65:69:8B:8B:DF:94:5F:37:5C:CA:0F:04:EE:32:D2:2E:67:4D:04:D0:DF:38:1F:4F:DB:B6:54:02:78:63:B3
    vagrant@gsd-tomcat1:~$
    ```

    </details>

## Additional Information

For additional insights and guidance:

* Explore the design and advantages of this setup in
  [Implementing PKI for RWS GIS with Ansible]({{< relref path="/docs/projects/rws/pki" >}}).
* At RWS the CA Server uses a CIFS share for storing certificates which creates
  some additional manual steps because this not is supported in Ansible Core
  Utility modules see
  [Create a Simple CA Server using Ansible]({{< relref path="/docs/howto/rws/ca" >}}).
