---
categories: ["How-to"]
tags: [ansible,Behavior,ansible.cfg,merge,dictionary]
title: "Review Dictionary Merging in Ansible Projects"
linkTitle: "Review Dictionary Merging"
translate: false
weight: 10
description: >
  Explore dictionary merging in Ansible inventory projects, focusing on the
  `hash_behaviour = merge` setting in C2 Ansible inventory projects."
---


Projects:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

---

## Configuring `hash_behaviour`

Ensure the `hash_behaviour` in `ansible.cfg` is set to `merge` to enable
dictionary merging in your
[Ansible inventory project]({{< relref path="/docs/concepts/ansible/projects/inventory" >}}).

## Review variables

Variables in C2 Platform Ansible Roles like `win_resources` and
`linux_resources` can be complex dictionaries. Use this command to locate
win_resources in your project:

```bash
grep -HnA1 '^win_resources:' $(find group_vars -name "*.yml")
```

Example Output:

```bash
 ~/git/azure/rws/ansible-gis$ grep -HnA1 '^win_resources:' $(find group_vars -name "*.yml")
group_vars/fme/win.yml:2:win_resources:
group_vars/fme/win.yml-3-  2-fme:
--
group_vars/windows/main.yml:20:win_resources:
group_vars/windows/main.yml-21-  0-bootstrap:
--
group_vars/fme_engine/main.yml:12:win_resources:
group_vars/fme_engine/main.yml-13-  fme_arcpy_path:
rws →
 ~/git/azure/rws/ansible-gis$
```

It shows three occurrences in three Ansible groups:

| Group        | Key              |
|--------------|------------------|
| `fme`        | `2-fme`          |
| `windows`    | `0-bootstrap`    |
| `fme_engine` | `fme_arcpy_path` |

If a node is part of all three groups, Ansible will merge these lists, for
example to a dictionary similar to:

```yaml
win_resources:
  2-fme:
    - <list item>
    - <list item>
  0-bootstrap:
    - <list item>
    - <list item>
  fme_arcpy_path:
    - <list item>
```

C2 Ansible Roles will then typically first process these dictionaries using the
filter `c2platform.core.groups2items` for simplified handling. This filter will
flatten and also sort alphabetically the list on the group name. The Ansible role
`c2platform.wincore.win` also uses this filter. The result is shown below:

```yaml
win_resources:
  - group: 0-bootstrap
    <list item>
  - group: 0-bootstrap
    <list item>
  - group: 2-fme
    <list item>
  - group: fme_arcpy_path
    <list item>
```

{{< alert title="Tip:" >}}
Prefixing group names with numbers (e.g., `0-bootstrap`) organizes the execution
order in Ansible, prioritizing dependencies effectively.
{{< /alert >}}

## Review other "merging" dictionaries

Review all other variables that are dictionaries and that are or can be merged.
For example, C2 Platform product roles can include the `c2platform.wincore.win` role and define their own "resources" variables. Search for `_win_resources`. The result might look like below.

```yaml
ostraaten@localhost:~/git/azure/ansible-gis$ grep -HnA1 '_win_resources:' $(find group_vars -name "*.yml")
group_vars/fme_core/main.yml:11:fme_flow_win_resources:
group_vars/fme_core/main.yml-12-  core:
--
group_vars/fme_core/tomcat.yml:26:tomcat_win_resources:
group_vars/fme_core/tomcat.yml-27-  tomcat:
ostraaten@localhost:~/git/azure/ansible-gis$
```

From this we can derive that the `tomcat` role and `fme_flow` include the
`c2platform.wincore.win` role. Respectively the roles utilize this role though
the `fme_flow_win_resources` and `tomcat_win_resources` variables.

## Additional Information

* See the guideline
  [Managing Dictionary Merging in C2 Platform Ansible Projects]({{< relref path="/docs/guidelines/coding/merge" >}}).






