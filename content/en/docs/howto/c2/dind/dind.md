---
categories: ["Guideline", "Example"]
tags: [dind, docker, docker-in-docker, gitlab, pipeline, ci]
title: "What is Docker-in-docker ( DinD )?"
linkTitle: "DinD?"
translate: false
weight: 1
description: >
  Docker-in-Docker (DinD) is a common technique used in GitLab CI/CD pipelines on
  Kubernetes to run Docker commands within Docker containers.
---

Docker-in-Docker (DinD) is a technique used in GitLab CI/CD pipelines on
Kubernetes to run Docker commands within Docker containers. It allows for
building, testing, and running Docker images and tests in an isolated
environment. The value of using DinD is that it provides a consistent and
reproducible environment for building and testing Docker images, ensuring that
the pipeline runs in a controlled and predictable environment. The purpose of
DinD is to simplify the setup and management of Docker images and containers in
a CI/CD pipeline on Kubernetes, making it easier to build and test Docker images
as part of the continuous integration and delivery process. The challenge of
using DinD is that it can introduce security concerns, as it requires running
Docker within a Docker container, which may potentially expose the host system
to security risks. Proper security measures, such as using appropriate Docker
images, ensuring container isolation, and managing permissions, should be
implemented to mitigate these risks. Additionally, DinD may require additional
resources and configuration in terms of storage, networking, and Docker
configurations to ensure smooth execution within the Kubernetes environment.

An example of this technique can be found in the Ansible project
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})
which is used to manage the
[RWS Ansible Execution Environment]({{< relref path="/docs/howto/rws/exe-env" >}})
