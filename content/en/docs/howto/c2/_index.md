---
categories: ["How-to"]
tags: []
title: "How to Use the C2 Platform"
linkTitle: "C2"
translate: false
weight: 19
description: >
  Step-by-step instructions related to the C2 Platform (C2) reference implementation.
---

> The content in this section is specifically related to the C2 Platform (C2) Reference Implementation, which is contained within the [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) project.
