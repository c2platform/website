---
categories: ["How-to"]
tags: []
title: "C2 Platform Community Website Instructions"
linkTitle: "Website"
translate: false
draft: true
weight: 20
description: >
    This section provides step-by-step instructions for creating, maintaining, and
    updating the C2 Platform Community Website.
---

Projects: [`c2platform/website`]({{< relref path="/docs/gitlab/c2platform/website" >}})

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

'LAYOUT_WITH_LEGEND()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <logos/kubernetes>

AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddRelTag("light", $textColor="#93c7fb")
AddRelTag("ansible", $sprite="img:https://s3.amazonaws.com/media-p.slid.es/uploads/ignasifoschalonso/images/482990/Ansible_logo.png{scale=0.04}", $legendSprite="img:https://s3.amazonaws.com/media-p.slid.es/uploads/ignasifoschalonso/images/482990/Ansible_logo.png{scale=0.01}", $legendText="ansible provision")
UpdateRelStyle("#042a4f", "#042a4f")

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
' $bgColor="#1168bd"
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
Person(engineer, "Engineer")

System_Boundary(local, "c2platform.org") {
   Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1,lxd", $tags="rproxy")
   Container(c2d_galaxy_k8s, "Kubernetes", "c2d-galaxy1,lxd", $tags="k8s") {
      Container(galaxy, "Galaxy NG", "", $tags="")
      Container(debug_cli, "Debug CLI", "", $tags="")
      Container(debug_gui, "Debug Desktop", "", $tags="")
   }
}

Rel(engineer, c2d_rproxy1, "galaxy.c2platform.org", "https,port 443")
Rel(c2d_rproxy1, galaxy, "", "")
Rel(engineer, debug_gui, "RDP", "")
note bottom on link
   hihi
end note
Rel(engineer, debug_cli, "Via Kubernetes\nDashboard", "")
Rel(debug_gui, galaxy, "Debug using\nFireFox\n(and CLI utilities)", "", $tags="ansible")
Rel(debug_cli, galaxy, "Debug using\nCLI utilities", "", $tags="light")

note right of galaxy
   the <U+0025>autonumber<U+0025> works everywhere.
   Here, its value is ** %autonumber% **
end note

SHOW_LEGEND(false)

@enduml
```
