---
categories: ["Handleiding"]
tags: [linux, ubuntu, laptop, linters, ansible, yaml, ansible-lint, yamllint]
title: "Linters"
linkTitle: "Linters"
weight: 3
description: >
  Verbeter de codekwaliteit, identificeer potentiële problemen vroegtijdig en zorg voor naleving van codestandaarden.
---

Als je van plan bent om wijzigingen aan te brengen in een Ansible-project, is
het essentieel om linters voor Ansible en YAML in je projectworkflow te
integreren. Linters spelen een cruciale rol bij het handhaven van hoge
codekwaliteit door je codebasis automatisch te analyseren en potentiële
problemen vroegtijdig te identificeren. Door linters te integreren, kun je
ervoor zorgen dat codestandaarden worden nageleefd en de algehele
betrouwbaarheid en leesbaarheid van je Ansible-code wordt verbeterd.

Door het volgen van deze stappen en door linters op verschillende niveaus van je
Ansible-project toe te passen, kun je de codekwaliteit aanzienlijk verbeteren,
potentiële problemen vroegtijdig identificeren en zorgen voor strikte naleving
van codestandaarden. Dit resulteert in meer onderhoudbare en robuuste
Ansible-code, die samenwerking vergemakkelijkt en de kans op fouten in je
implementaties vermindert.

## VS Code

Er zijn meerdere niveaus waarop je linters kunt toepassen binnen je
Ansible-project. Ten eerste kun je de kracht van linters direct benutten binnen
populaire geïntegreerde ontwikkelomgevingen (IDEs) zoals Visual Studio Code.
Deze IDEs bieden plugins en extensies die real-time feedback geven, door
mogelijke problemen te markeren terwijl je code schrijft. Hierdoor kun je direct
correcties aanbrengen en best practices volgen.

### Linter

Om linters voor Ansible en YAML in Visual Studio Code te integreren, kun je de
{{< external-link url="https://marketplace.visualstudio.com/items?itemName=fnando.linter" text="Linter" htmlproofer_ignore="false" >}} extensie van Nando Vieira
installeren. Deze extensie ondersteunt linters voor verschillende talen,
waaronder YAML.

### Trailing Spaces

Een andere nuttige extensie om te overwegen voor het handhaven van schone code
in je Ansible-project is {{< external-link url="https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces" text="Trailing Spaces" htmlproofer_ignore="false" >}} van Shardul Mahadik. Deze
extensie markeert overbodige spaties aan het einde van regels als rode
vierkanten, zodat je deze eenvoudig kunt opmerken en verwijderen.

Om de Trailing Spaces-extensie zo in te stellen dat overbodige spaties
automatisch worden verwijderd wanneer een bestand wordt opgeslagen, volg je deze
extra stappen:

1. Druk op <kbd>Ctrl</kbd> <kbd>Shift</kbd> <kbd>p</kbd> om de opdrachtpalet te
   openen.
2. Typ "Voorkeuren: Open Gebruikersinstellingen" en selecteer de bijbehorende
   optie.
3. Zoek in de gebruikersinstellingen naar "trailing spaces".
4. Schakel de optie "Trimmen bij opslaan" in om overbodige spaties automatisch
   te verwijderen bij het opslaan van bestanden.

Door de Linter en Trailing Spaces-extensies in Visual Studio Code te installeren
en te configureren, kun je ervoor zorgen dat je Ansible-code voldoet aan
codestandaarden, schone en leesbare code behouden, en vroegtijdig potentiële
problemen ontdekken.

## Handmatig

Ten tweede kun je ervoor kiezen om linters handmatig in je ontwikkelomgeving te
draaien. Door linters op aanvraag uit te voeren, kun je je Ansible playbooks en
YAML-bestanden analyseren op fouten, stijlmisstanden en andere codeproblemen.
Deze handmatige benadering stelt je in staat om eventuele geïdentificeerde
problemen te bekijken en op te lossen voordat je je wijzigingen commit.

```bash
yamllint -c .yamllint .
ansible-lint -c .ansible-lint
```

## Pre-commit linters

Om de codekwaliteit verder te verbeteren en linting als een integraal onderdeel
van je ontwikkelworkflow te handhaven, kun je gebruik maken van Git pre-commit
hooks. Door pre-commit hooks te configureren, kun je automatisch linters
uitvoeren voor elke commit, waardoor de introductie van foutieve of
niet-conforme code in je repository wordt voorkomen. Dit zorgt ervoor dat alle
codewijzigingen grondig worden gecontroleerd aan de hand van gedefinieerde
standaarden, waardoor de kans op bugs of codegeuren wordt verkleind. Maak een
bestand `.git/hooks/pre-commit`. Let op het gebruik van de omgevingsvariabele
`C2_LINTERS_VIRTUALENV` in het onderstaande voorbeeld. Deze variabele is voor je
Python-virtuele omgeving. Je moet dit instellen, bijvoorbeeld in `~/.bashrc`,
zodat de hook correct werkt in de terminal of VS Code.

```bash
#!/bin/bash
source $HOME/.virtualenv/$C2_LINTERS_VIRTUALENV/bin/activate
yamllint -c .yamllint .
ansible-lint -c .ansible-lint
```

Het wordt aanbevolen om een kloonscript te maken, zie [Kloonscript]({{< relref path="/docs/guidelines/setup/clone" >}} "Richtlijn: Kloonscript").

### Pre-commit linters met behulp van pre-commit (experimenteel)

Een alternatieve manier om linters uit te voeren is met behulp van
[pre-commit](https://pre-commit.com/). Dit is een meer geavanceerde maar ook
complexere aanpak die al dan niet werkt, afhankelijk van je omgeving. De stappen
zijn als volgt:

1. Maak `.pre-commit-config.yaml` aan in de root van de repository.
2. Installeer het pre-commit Python-pakket met `pip3 install pre-commit`.
3. Maak een Git pre-commit hook aan met `pre-commit install`. Dit creëert een
   bestand `.git/hooks/pre-commit`.

Een voorbeeld van een pre-commit configuratiebestand `.pre-commit-config.yaml`
wordt hieronder getoond:

```yaml
---
default_language_version:
  python: python3.8
repos:
  - repo: https://github.com/ansible-community/ansible-lint.git
    rev: v6.7.0
    hooks:
      - id: ansible-lint
        args: ["-c", ".ansible-lint"]
  - repo: https://github.com/adrienverge/yamllint.git
    rev: v1.28.0
    hooks:
      - id: yamllint
        args: ["-c", ".yamllint"]
```

## CI/CD Pipeline

Tot slot biedt het integreren van linters in je Continuous
Integration/Continuous Deployment (CI/CD) pipeline een extra laag van
codekwaliteitsborging. Door linters op te nemen in je geautomatiseerde build- en
deploymentproces, kun je je Ansible-code automatisch scannen op problemen
tijdens de uitvoering van de pijplijn. Deze aanpak maakt vroege detectie van
problemen mogelijk, zorgt voor consistente codekwaliteit in je gehele codebasis
en helpt een betrouwbare en stabiele infrastructuur te behouden.

Zie [Continuous Integration and Deployment]({{< relref path="/docs/tutorials/git-workflow/6-cicd" >}}) in de [Mastering Git Workflow
and Releases in Ansible]({{< relref path="/docs/tutorials/git-workflow" >}})
tutorial voor meer informatie.

## Links

* {{< external-link url="https://ansible.readthedocs.io/projects/lint/configuring/#ansible-lint-configuration" text="Configuratie - Ansible Lint Documentatie" htmlproofer_ignore="false" >}}.