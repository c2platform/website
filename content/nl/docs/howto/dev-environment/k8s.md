---
categories: ["Handleiding"]
tags: [kubernetes, ide, kubectl, extension]
title: "Verbinding maken met een Kubernetes-cluster"
linkTitle: "Verbind met Kubernetes"
weight: 5
description: >
    Leer hoe je verbinding kunt maken met een Kubernetes-cluster voor efficiënt beheer en ontwikkeling.
---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

---

## Vereisten

Voordat je verbinding kunt maken met een cluster, zorg ervoor dat je een
Kubernetes-cluster beschikbaar hebt, zoals een draaiend op `c2d-ks1`, `c2d-awx1`
of `c2d-galaxy1`. Voor gedetailleerde instructies over het opzetten van deze
clusters, raadpleeg de volgende bronnen:

* [Installeer de Automation Controller (AWX) met Ansible]({{< relref path="/docs/howto/awx/awx" >}})
* [Installeer de Automation Hub (Galaxy NG) met Ansible]({{< relref path="/docs/howto/awx/galaxy" >}})
* [Installeer Kubernetes]({{< relref path="/docs/howto/kubernetes/kubernetes" >}})

## Creëer `~/.kube/config` bestand

Om verbinding te maken met een Kubernetes-cluster moet je een `~/.kube/config`
bestand aanmaken. Hier is een voorbeeld hoe je dit kunt doen:

```bash
sudo snap install kubectl --classic
export BOX=c2d-awx1
mkdir --parents ~/.kube
vagrant ssh $BOX -c "microk8s config" > ~/.kube/config
```

## Verifieer de verbinding

Om ervoor te zorgen dat je succesvol verbinding hebt gemaakt met het cluster,
kun je een willekeurig `kubectl`-commando gebruiken. Voer bijvoorbeeld het
volgende commando uit om je verbinding te verifiëren:

```bash
kubectl get all --all-namespaces
```

## Integratie met Visual Studio Code

Als je Visual Studio Code gebruikt, kun je je Kubernetes-clusterbeheerervaring
verbeteren door de {{< external-link url="https://marketplace.visualstudio.com/items?itemName=ms-kubernetes-tools.vscode-kubernetes-tools" text="Visual Studio Code Kubernetes Tools" htmlproofer_ignore="false" >}} te
installeren. Met deze extensie kun je naadloos verbinding maken met en je
Kubernetes-cluster beheren, rechtstreeks vanuit Visual Studio Code.