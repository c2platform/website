---
categories: ["Handleiding"]
tags: [vagrant, reverse-proxy, forward-proxy, apache2]
title: "Creëer de Reverse Proxy"
linkTitle: "Reverse Proxy"
weight: 7
description: >
  Creëer en voorzie de `c2d-rproxy1`, wat een essentiële voorwaarde is voor
  een functionele ontwikkelomgeving.
---

De `c2d-rproxy1` node vervult meerdere essentiële functies bij het opzetten van
een functionele ontwikkelomgeving. In een eerdere stap [LXD stap]({{< relref path="./lxd" >}}), liepen we tegen een fout aan bij het proberen te creëren van
de `c2d-rproxy1` node. In deze gids willen we onze eerste node succesvol
provisioneren, in het bijzonder `c2d-rproxy1`, vanwege de verschillende
belangrijke functies die het vervult.

## Creëren van `c2d-rproxy1`

Om de node te creëren en te provisioneren, gebruik het volgende commando:

```bash
c2  # activeer de c2d virtuele omgeving
vagrant up c2d-rproxy1
```

## Verificatie

Het opzetten van `c2d-rproxy1` met Apache2 is redelijk eenvoudig, dus als het
provisioneren in de vorige stap gelukt is, zit je goed. Om de succesvolle
provisioning van de node te verifiëren, voer de volgende commando's uit:

```bash
vagrant ssh c2d-rproxy1
curl https://c2platform.org/is-alive
export https_proxy=http://1.1.4.205:1080
curl https://c2platform.org/is-alive
```

Het uitvoeren van de `curl` commando's zou de tekst moeten opleveren: "Apache is
alive." De eerste `curl` verifieert dat de reverse proxy werkt, en de tweede
verifieert dat de forward proxy werkt.

<details>
  <summary><kbd>Toon mij</kbd></summary>

```bash
vagrant@c2d-rproxy1:~$ curl https://c2platform.org/is-alive
Apache is alive
vagrant@c2d-rproxy1:~$ export https_proxy=http://1.1.4.205:1080
vagrant@c2d-rproxy1:~$ curl https://c2platform.org/is-alive
Apache is alive
```
</details>

## Aanvullende Links

Voor meer informatie over de verschillende rollen van `c2d-rproxy1`, raadpleeg
de volgende handleidingen:

* [Servercertificaten beheren als een Certificaatautoriteit]({{< relref path="/docs/howto/c2/certs" >}} "Handleiding: Servercertificaten beheren als
  een Certificaatautoriteit")
* [Reverse Proxy and CA-server instellen]({{< relref path="/docs/howto/c2/reverse-proxy" >}} "Handleiding: Reverse Proxy en
  CA-server instellen")
* [SOCKS proxy instellen]({{< relref path="/docs/howto/c2/socks-proxy" >}}
  "Handleiding: SOCKS proxy instellen")
* [DNS instellen voor Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}} "Handleiding: DNS instellen voor Kubernetes")