---
categories: ["Handleiding"]
tags: [ubuntu, lxd, pre-commit, git]
title: "Instellen van Projectmappen en Ansible Installeren"
linkTitle: "Project en Ansible"
weight: 3
description: >
  Clone de Git-repositories en installeer Ansible en Ansible Galaxy Rollen en Collecties.
---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

---

## Pas `~/.bashrc` aan

Verbeter je `~/.bashrc` bestand met de volgende toevoegingen:

```bash
export GIT_USER='tclifton'
export GIT_MAIL='tony.clifton@dev.c2platform.org'
alias python='python3'
alias pip='pip3'
alias c2-env='source ~/.virtualenv/c2d/bin/activate'
alias c2-home='cd ~/git/gitlab/c2/ansible-dev'
alias c2='c2-home && c2-env'
alias c2-roles='c2 && ansible-galaxy install -r roles/requirements.yml --force --no-deps -p roles/external'
alias c2-collections='c2 && ansible-galaxy collection install -r collections/requirements.yml -p .'
export C2_HTTPS_SSH=https  # ssh
```

Als je SSH wilt gebruiken in plaats van HTTPS om toegang te krijgen tot de
GitLab Git-repositories, moet je de regel voor `C2_HTTPS_SSH` aanpassen voor
SSH-toegang:

```bash
export C2_HTTPS_SSH=ssh
```

Als je van plan bent om wijzigingen bij te dragen, moet je een regel toevoegen
die de `ansible-lint` en `yamllint` linters inschakelt door een `pre-commit`
hook te creëren:

```bash
export C2_LINTERS=Y
```

Voor aanvullende informatie over linters raadpleeg:

* [Linters]({{< relref path="/docs/howto/dev-environment/linters" >}})
* [Clone script]({{< relref path="/docs/guidelines/setup/clone" >}})

## Clone Projecten

Gebruik het `clone.sh` script om projectmappen in te stellen:

```bash
sudo apt install git curl -y
curl -s -L https://gitlab.com/c2platform/ansible/-/raw/master/clone.sh | bash
```

{{< alert title="Opmerking:" >}} Het clone script is idempotent, waardoor je het
opnieuw kunt uitvoeren na het bijwerken van instellingen zoals `C2_LINTERS` en
het protocol via `C2_HTTPS_SSH`.

{{< /alert >}}

## Stel Python Virtuele Omgeving in

Creëer een Python 3 virtuele omgeving voor [Ansible]({{< relref path="/docs/concepts/ansible" >}}) voor stabiliteit en flexibiliteit:

```bash
sudo apt update
sudo apt install virtualenv -y
mkdir ~/.virtualenv
DEB_PYTHON_INSTALL_LAYOUT='deb' virtualenv ~/.virtualenv/c2d -p python3
```

## Installeer Ansible

Installeer Ansible in de virtuele omgeving `c2d`:

```bash
source ~/.bashrc
c2
pip install -r requirements.txt
```

## Installeer Ansible Collecties / Rollen

Gebruik de `c2-roles` en `c2-collections` aliassen om Ansible Collecties en
Rollen te installeren:

```bash
c2-roles
c2-collections
```

## Aanvullende Informatie

* {{< external-link url="https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-and-upgrading-ansible" text="Ansible Installeren — Ansible Documentatie" htmlproofer_ignore="false" >}}