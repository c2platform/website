---

categories: [Handleiding]
tags: [linux, virtualbox, verwijderen]
title: VirtualBox Verwijderen
linkTitle: VirtualBox Verwijderen
weight: 1
description: >
  Leer hoe je VirtualBox volledig van je systeem kunt verwijderen.

---

## Verwijder VirtualBox Pakketten

```bash
sudo apt-get purge virtualbox virtualbox-guest-utils virtualbox-guest-x11
```

## Verwijder VirtualBox Extensiepakket

```bash
vboxmanage list extpacks
sudo VBoxManage extpack uninstall "Oracle VM VirtualBox Extension Pack"
```