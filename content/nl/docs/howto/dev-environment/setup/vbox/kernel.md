---
categories: ["Handleiding"]
tags: [linux, virtualbox, kernel, gurumeditatie, ubuntu, vagrant]
title: "Hoe de Kernel te Downgraden in Ubuntu 22.04"
linkTitle: "Kernel Downgrade"
weight: 2
description: >
  Leer hoe je de kernel in Ubuntu 22.04 kunt downgraden om compatibiliteitsproblemen met VirtualBox op te lossen.
---

VirtualBox op Ubuntu kan soms incompatibel worden met de nieuwste kernel na een
update. Wanneer dit gebeurt, kun je ervoor kiezen om Ubuntu, VirtualBox en
Vagrant te updaten, of simpelweg de kernel te downgraden.

---

## Symptomen

### Vagrant

Vagrant-operaties op VirtualBox-VM's mislukken met het volgende bericht:

{{< alert >}}
De gastmachine kwam in een ongeldige toestand tijdens het wachten op het opstarten. Geldige toestanden zijn 'starting, running'. De machine bevindt zich in de 'gurumeditatie' toestand. Controleer of alles correct is geconfigureerd en probeer het opnieuw.
{{< /alert >}}

<details>
  <summary><kbd>Toon Mij</kbd></summary>

  ```bash
  :ansible-gis]└2 development(+200/-13) ± vagrant up $BOX
  Machine 'gsd-agserver1' wordt opgestart met de 'virtualbox' provider...
  ==> gsd-agserver1: Controleren of box 'c2platform/win2022' versie '0.1.0' up-to-date is...
  ==> gsd-agserver1: Alle eerder ingestelde doorgestuurde poorten wissen...
  ==> gsd-agserver1: Alle eerder ingestelde netwerkinterfaces wissen...
  ==> gsd-agserver1: Netwerkinterfaces voorbereiden op basis van configuratie...
      gsd-agserver1: Adapter 2: hostonly
      gsd-agserver1: Adapter 1: nat
  ==> gsd-agserver1: Poorten doorsturen...
      gsd-agserver1: 5985 (gast) => 55985 (host) (adapter 1)
      gsd-agserver1: 5986 (gast) => 55986 (host) (adapter 1)
      gsd-agserver1: 22 (gast) => 2222 (host) (adapter 1)
  ==> gsd-agserver1: 'Pre-boot' VM-aanpassingen uitvoeren...
  ==> gsd-agserver1: VM opstarten...
  ==> gsd-agserver1: Wachten tot de machine is opgestart. Dit kan enkele minuten duren...
  De gastmachine kwam in een ongeldige toestand tijdens het wachten op
  opstarten. Geldige toestanden zijn 'starting, running'. De machine bevindt zich in de
  'gurumeditatie' toestand. Controleer of alles correct is
  geconfigureerd en probeer het opnieuw.

  Als de provider die je gebruikt een GUI heeft, kan het vaak nuttig zijn om deze te
  openen en de machine te bekijken, aangezien de GUI vaak meer behulpzame foutmeldingen kan geven dan Vagrant kan ophalen. Als je bijvoorbeeld
  VirtualBox gebruikt, voer dan `vagrant up` uit terwijl de VirtualBox
  GUI open is.

  Het primaire probleem voor deze fout is dat de provider die je gebruikt
  niet correct is geconfigureerd. Dit is zelden een Vagrant-probleem.
  ```
</details>

### VirtualBox

In de VirtualBox Manager starten VM's niet en bieden de logbestanden weinig
informatie, anders dan dat de VM plotseling stopte met werken.

### Ubuntu

Ubuntu genereert crashrapporten met regels zoals:

{{< alert >}} virtualbox-dkms 6.1.48-dfsh-1~ubuntu1.22.02.1: virtualbox kernel
module kon niet worden gebouwd {{< /alert >}}

## Kernel Terugdraaien

Als het probleem optreedt na een automatische kernel-upgrade, is de oude kernel
nog steeds beschikbaar. Je kunt deze als standaardkernel instellen met de
`GRUB_DEFAULT` optie in `/etc/default/grub` met de volgende stappen:

1. Voer update Grub uit om alle beschikbare kernels te tonen:
   ```bash
   sudo update-grub
   ```
   <details>
   <summary><kbd>Toon Mij</kbd></summary>

   ```bash
   [@mpc2:ansible-gis]└2 development(+200/-13) 1 ± sudo update-grub
   Bestand `/etc/default/grub` wordt ingeladen
   Bestand `/etc/default/grub.d/init-select.cfg` wordt ingeladen
   Grub configuratiebestand genereren...
   Linux afbeelding gevonden: /boot/vmlinuz-6.8.0-40-generic
   Initrd afbeelding gevonden: /boot/initrd.img-6.8.0-40-generic
   Linux afbeelding gevonden: /boot/vmlinuz-6.5.0-41-generic
   Initrd afbeelding gevonden: /boot/initrd.img-6.5.0-41-generic
   Memtest86+ vereist een 16-bits boot, dat niet beschikbaar is op EFI, afsluiten.
   Waarschuwing: os-prober zal niet worden uitgevoerd om andere opstartbare partities te detecteren. Systemen hierop zullen niet aan de GRUB opstartconfiguratie worden toegevoegd. Controleer de GRUB_DISABLE_OS_PROBER documentatie invoer.
   Opstartmenu-item voor UEFI Firmware-instellingen toevoegen...
   klaar
   ```
   </details>

2. Voer de volgende opdracht uit om de exacte regel te krijgen die in
   `/etc/default/grub` moet worden gebruikt:
   ```bash
   grep 'menuentry \|submenu ' /boot/grub/grub.cfg | cut -f2 -d "'"
   ```
   Bijvoorbeeld, het kan het volgende uitvoeren:
   ```
   Ubuntu, met Linux 6.5.0-41-generic
   ```
   <details>
   <summary><kbd>Toon Mij</kbd></summary>

   ```bash
   [@mpc2:ansible-gis]└2 development(+200/-13) ± grep 'menuentry \|submenu ' /boot/grub/grub.cfg | cut -f2 -d "'"
   Ubuntu
   Geavanceerde opties voor Ubuntu
   Ubuntu, met Linux 6.8.0-40-generic
   Ubuntu, met Linux 6.8.0-40-generic (herstelmodus)
   Ubuntu, met Linux 6.5.0-41-generic
   Ubuntu, met Linux 6.5.0-41-generic (herstelmodus)
   UEFI Firmware-instellingen
   ```
   </details>

3. Wijzig de `GRUB_DEFAULT` optie in `/etc/default/grub` van:
   ```
   GRUB_DEFAULT=0
   ```
   naar:
   ```
   GRUB_DEFAULT="Geavanceerde opties voor Ubuntu>Ubuntu, met Linux 6.5.0-41-generic"
   ```
4. Werk Grub opnieuw bij:

   ```bash
   sudo update-grub
   ```

5. Herstart de machine om de nieuwe kernel te gebruiken:
   ```bash
   reboot now
   ```

6. Controleer de huidige kernel:
   ```bash
   uname -r
   ```

## Oudere Kernel Installeren

Als het probleem optreedt op een nieuwe Ubuntu-installatie en het heeft nooit
correct gewerkt, kun je een oudere kernel installeren. Bijvoorbeeld, voor Ubuntu
22.04 werkt de kernel "Ubuntu, met Linux 6.5.0-41-generic".

Voor instructies over hoe de kernel te downloaden en te installeren, raadpleeg: {{< external-link url="https://askubuntu.com/questions/1404722/downgrade-kernel-for-ubuntu-22-04-lts" text="Kernel downgraden voor Ubuntu 22.04 LTS - Ask Ubuntu" htmlproofer_ignore="false" >}}.