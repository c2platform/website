---
categories: ["Handleiding"]
tags: [vagrant, hashicorp, virtualbox, klembord, slepen-en-neerzetten]
title: "VirtualBox Installeren"
linkTitle: "VirtualBox"
weight: 6
description: >
  Installeer VirtualBox, configureer Host-Only Networking en zorg ervoor dat de gesynchroniseerde mappen van Vagrant vlekkeloos werken.
---

Projecten: [`c2platform/gis/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---

## VirtualBox Installeren

VirtualBox is vereist voor bepaalde nodes in dit project, vooral voor MS Windows
nodes waar LXD geen optie is. Je kunt het op Ubuntu installeren met de volgende
commando's:

```bash
sudo apt-get install virtualbox virtualbox-guest-utils virtualbox-guest-x11
```

## Toestaan van Elk IP-Adres

Om VM's netwerkranges anders dan `192.168.56.0/21` te laten gebruiken, maak je
een bestand genaamd `/etc/vbox/networks.conf`.

```bash
sudo mkdir /etc/vbox
sudo nano /etc/vbox/networks.conf
```

Voeg de volgende regel toe. Voor meer details, raadpleeg de
{{< external-link url="https://www.virtualbox.org/manual/ch06.html#network_hostonly" text="6.7. Host-Only Networking" htmlproofer_ignore="false" >}}.

```
* 0.0.0.0/0 ::/0
```

<details>
  <summary><kbd>Toon mij</kbd></summary>

```
==> c2d-iis1: Alle eerder ingestelde netwerkinterfaces wissen...
Het IP-adres dat is geconfigureerd voor het host-only netwerk ligt niet binnen de toegestane reeksen. Werk het gebruikte adres bij om binnen de toegestane reeksen te liggen en voer de opdracht opnieuw uit.

  Adres: 1.1.8.146
  Reeksen: 192.168.56.0/21

Geldige reeksen kunnen worden aangepast in het /etc/vbox/networks.conf bestand. Voor meer informatie, inclusief geldig formaat, zie:

  https://www.virtualbox.org/manual/ch06.html#network_hostonly
```

</details>

## Gesynchroniseerde Mappen Configureren

Op Ubuntu 22.04, zorg ervoor dat "gesynchroniseerde mappen" correct werken door
de volgende regel toe te voegen aan je `~/.bashrc` bestand:

```bash
export VAGRANT_DISABLE_VBOXSYMLINKCREATE=0
```

<details>
  <summary><kbd>Toon mij</kbd></summary>

  ```bash
Vagrant is momenteel geconfigureerd om VirtualBox gesynchroniseerde mappen te maken met de `SharedFoldersEnableSymlinksCreate` optie ingeschakeld. Als de Vagrant gast niet vertrouwd is, wil je deze optie wellicht uitschakelen. Voor meer informatie over deze optie, raadpleeg de VirtualBox handleiding:

  https://www.virtualbox.org/manual/ch04.html#sharedfolders

Deze optie kan wereldwijd worden uitgeschakeld met een omgevingsvariabele:

  VAGRANT_DISABLE_VBOXSYMLINKCREATE=1

of op basis van een specifieke map binnen het Vagrantfile:

  config.vm.synced_folder '/host/path', '/guest/path', SharedFoldersEnableSymlinksCreate: false
  ```
</details>

## Gedeeld Klembord

Voor betere prestaties en bruikbaarheid, overweeg om
{{< external-link url="https://www.virtualbox.org/manual/ch04.html" text="VirtualBox Guest Additions" htmlproofer_ignore="true" >}}
te installeren op
{{< external-link url="https://developer.hashicorp.com/vagrant/docs/boxes" text="Vagrant boxes" htmlproofer_ignore="true" >}}
gebaseerd op VirtualBox afbeeldingen. Als je een project gebruikt zoals
[`c2platform/gis/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})
, waar de box
{{< external-link url="https://app.vagrantup.com/c2platform/boxes/win2016/versions/0.1.1" text="c2platform/win2016" htmlproofer_ignore="true" >}}
wordt gebruikt, zijn de VirtualBox Guest Additions al geïnstalleerd.

Als de VirtualBox Guest Additions zijn geïnstalleerd, kun je het gedeelde
klembord inschakelen door het **VirtualBox Extension Pack** te installeren met
de volgende commando's:

```bash
export VBOX_VERSION=$(vboxmanage --version | cut -d'_' -f1)
cd /opt
sudo wget https://download.virtualbox.org/virtualbox/$VBOX_VERSION/Oracle_VM_VirtualBox_Extension_Pack-$VBOX_VERSION.vbox-extpack
sudo VBoxManage extpack install Oracle_VM_VirtualBox_Extension_Pack-$VBOX_VERSION.vbox-extpack
sudo systemctl restart virtualbox.service
```

Om de functionaliteit van het gedeelde klembord in te schakelen, volg je deze
stappen:

1. Stop de VM(s) door `vagrant halt` uit te voeren.
2. Open VirtualBox Manager en ga naar **Bestand** → **Voorkeuren** →
   **Extensies**.
3. Als je het **Oracle VM VirtualBox Extension Pack** hebt geïnstalleerd,
   selecteer het en verwijder het.
4. Selecteer **Nieuwe Pakket Toevoegen** en zoek naar het extensiebestand op
   `/opt/Oracle_VM_VirtualBox_Extension_Pack-$VBOX_VERSION.vbox-extpack`, en
   installeer het.
5. Start de VM(s).

{{< alert type="warning" title="Waarschuwing!" >}} Deze stappen moeten elke keer
herhaald worden wanneer je een nieuwe VM aanmaakt. Voor een nieuw aangemaakte VM
zal de sleep-en-neerzet functionaliteit werken, maar het gedeelde klembord niet.
Dit gedrag is waargenomen met VirtualBox versie 6.1.38 op Ubuntu 22.04. {{<
/alert >}}