---
categories: ["Handleiding"]
tags: [gitlab, ssh]
title: "SSH-sleutels Aanmaken en een GitLab-account Instellen"
linkTitle: "GitLab Account & SSH-Sleutels"
weight: 2
description: >
    Leer hoe u SSH-sleutels kunt genereren voor het veilig benaderen van C2 Platform GitLab repositories en stel een GitLab-account in.
---

{{< alert title="Opmerking:" >}} Als u geen wijzigingen als een C2 Platform
engineer wilt aanbrengen en geen schrijfrechten voor C2-repositories nodig
heeft, kunt u deze stap overslaan en in plaats daarvan HTTPS gebruiken om
toegang tot repositories te krijgen. {{< /alert >}}

## SSH-sleutels Aanmaken

Om veilig toegang te krijgen tot GitLab-repositories, moet u een paar
SSH-sleutels (publiek en privé) genereren met het onderstaande commando. Het is
raadzaam om een wachtwoordzin te gebruiken voor extra veiligheid.

```bash
ssh-keygen -t rsa -b 4096 -C "tony.clifton@dev.c2platform.org"
```

## Een GitLab-account Aanmaken

1. Bezoek {{< external-link url="https://gitlab.com" text="gitlab.com" htmlproofer_ignore="true" >}} en registreer u voor een account.
2. Voeg uw SSH-publieke sleutel toe: navigeer naar **Instellingen** en ga naar
   {{< external-link url="https://gitlab.com/-/profile/keys" text="SSH-sleutels" htmlproofer_ignore="true" >}}.

## SSH-configuratie Verifiëren

Om te controleren of uw SSH-configuratie correct is ingesteld, voert u de
volgende commando's uit:

```bash
cd /tmp
git clone git@gitlab.com:c2platform/ansible.git
```