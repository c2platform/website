---
categories: ["Handleiding"]
tags: [linux, ubuntu, laptop, lxd, vagrant, ansible, grub, virtualbox]
title: "Een Ontwikkelomgeving Opzetten op Ubuntu 22"
linkTitle: "Installeren"
weight: 1
description: >
  Installeer Ansible, Vagrant, LXD, Virtualbox en kloon de projectmappen.
---

Deze handleiding biedt instructies voor het configureren van je
ontwikkelomgeving op je laptop. Het is ontworpen voor mensen die Ubuntu 22.04
als hun host-besturingssysteem gebruiken. Om de navigatie te vergemakkelijken,
zijn de handleidingpagina's zorgvuldig gesorteerd in aflopende volgorde van
belangrijkheid. Dit stelt je in staat om je eerst te concentreren op de meest
cruciale configuraties, wat zorgt voor een solide basis voor je
ontwikkelomgeving.

{{< alert type="warning" title="Compatibiliteitsproblemen tussen LXD, Docker en CRC" >}} Als je Docker of CRC installeert of al geïnstalleerd hebt, zullen
LXD-containers geen IP-adres kunnen verkrijgen en zal de provisioning met
Ansible mislukken.

<details>
  <summary>Lees meer...</summary>
</br>

Houd er rekening mee dat er bekende compatibiliteitsconflicten zijn tussen LXD,
Docker en CRC. Als je Docker of CRC hebt geïnstalleerd, of als je van plan bent
om deze te installeren, moet je weten dat LXD-containers moeilijkheden kunnen
ondervinden bij het verkrijgen van een IP-adres, wat kan leiden tot mogelijke
mislukkingen in de provisioning met Ansible.

Om een soepele werking te garanderen, wordt geadviseerd om de volgende
voorzorgsmaatregelen te nemen:

1. Als je Docker of CRC al hebt geïnstalleerd, overweeg dan om deze tijdelijk
   uit te schakelen of te verwijderen voordat je werkt met LXD-containers en
   Ansible-provisioning.
2. Als je van plan bent Docker of CRC naast LXD te installeren, wees dan
   voorbereid op mogelijke netwerkproblemen met LXD-containers. Extra stappen
   zijn wellicht nodig om deze conflicten op te lossen.
3. Overweeg om `podman` en `podman-compose` te gebruiken in plaats van `docker`
   en `docker-compose`. Podman conflicteert niet met LXD.

Door zorgvuldig om te gaan met de installatie en configuratie van LXD, Docker en
CRC, kun je de kans op IP-adresconflicten minimaliseren en een succesvol
provisioning-proces met Ansible garanderen.

Ga voorzichtig te werk en raadpleeg de respectieve documentatie van elk
hulpmiddel voor verdere begeleiding bij het oplossen van
compatibiliteitsproblemen.

</details>

{{< /alert >}}