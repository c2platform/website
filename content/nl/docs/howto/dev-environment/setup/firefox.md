---
categories: ["Handleiding"]
tags: [proxy, firefox, profiel, ssh]
title: "Configureer een Firefox-profiel"
linkTitle: "Firefox"
weight: 8
description: >
  Stel een Firefox-profiel in voor gemakkelijke toegang tot de omgeving via een browser.
---

Deze handleiding legt uit hoe je toegang kunt krijgen tot je ontwikkelomgeving
via de forward proxy die draait op `c2d-rproxy1`. Het is sterk aanbevolen om
voor dit doel een apart Firefox-profiel aan te maken. Deze setup maakt gebruik
van de forward proxy die in de vorige stap is gecreëerd [Maak de Reverse Proxy
en Web Proxy]({{< relref path="/docs/howto/dev-environment/setup/rproxy1" >}}).

## Maak een Profiel

Om je ontwikkelomgeving gescheiden te houden van andere profielen en je
standaard browsing, is het aan te raden een Firefox-profiel te gebruiken. Op
Ubuntu kun je de **Firefox Profile Manager** starten met de volgende opdracht:

```bash
firefox -no-remote -P
```

Alternatief kun je in de adresbalk `about:profiles` invoeren om toegang te
krijgen tot de Firefox Profile Manager.

## Configureer Netwerkinstellingen

Zodra je een apart Firefox-profiel hebt aangemaakt, open je Firefox met dat
profiel en configureer je de **Netwerkinstellingen** zoals hieronder
weergegeven:

| Eigenschap                    | Waarde     |
|-------------------------------|------------|
| Handmatige proxyconfiguratie  | ✔          |
| HTTP Proxy                    | `1.1.4.205`|
| Poort                         | `1080`     |
| Gebruik deze proxy ook voor HTTPS | ✔       |

## Importeer het Root CA-Certificaat

In de sectie **Bekijk Certificaten**, selecteer **Importeer** en importeer het
root CA-certificaat `.ca/c2.crt`. Als je het eerder vermelde clone-script hebt
gebruikt in [Stel een Ontwikkelomgeving in op Ubuntu 22]({{< relref "/docs/howto/dev-environment/setup" >}} "Handleiding: Stel een Ontwikkelomgeving
in op Ubuntu 22"), zou je het certificaat moeten vinden in de map
`~/git/gitlab/c2/ansible-dev/.ca/c2`.

## Maak een Starter

Voor je gemak kun je een aparte starter maken zodat je Firefox niet vanaf een
terminalvenster hoeft te starten. De volgende opdracht maakt een aparte starter
genaamd `firefox-profile-manager.desktop` voor Firefox, met een extra menuoptie,
**Profielbeheer**, die de opdracht `firefox -no-remote -P` uitvoert.

```bash
curl https://gitlab.com/c2platform/ansible/-/raw/master/doc/howto-socks-proxy/firefox-profile-manager.desktop  --output /tmp/firefox-profile-manager.desktop && sudo desktop-file-install /tmp/firefox-profile-manager.desktop
```

## Verifieer

Aangenomen dat je `c2d-rproxy` hebt gecreëerd volgens de vorige stappen
beschreven in [Maak de Reverse Proxy en Web Proxy]({{< relref "/docs/howto/dev-environment/setup/rproxy1" >}}), zou je in staat moeten zijn te
navigeren naar {{< external-link url="https://c2platform.org/is-alive" htmlproofer_ignore="true" >}}. Dit zou het volgende bericht moeten weergeven, en
je zou geen beveiligings- of certificaatwaarschuwingen moeten tegenkomen
aangezien het certificaat vertrouwd zou moeten zijn:

> Apache is alive