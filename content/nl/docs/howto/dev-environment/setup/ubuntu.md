---
categories: ["Handleiding"]
tags: [ubuntu, grub, visudo, root, lxd]
title: "Ubuntu Configuratie Handleiding"
linkTitle: "Ubuntu Handleiding"
weight: 1
description: >
  Leer hoe je Secure Boot uitschakelt, Grub configureert voor LXD-ondersteuning, en roottoegang zonder wachtwoord inschakelt op Ubuntu-systemen.
---

Deze handleiding biedt instructies voor het configureren van Grub om [LXD]({{<
relref path="../../../concepts/dev/lxd/" >}}) te ondersteunen en roottoegang
zonder een wachtwoord mogelijk te maken. Hoewel het inschakelen van roottoegang
zonder een wachtwoord niet strikt noodzakelijk is, kan het handig zijn voor
gemak, vooral op een ontwikkellaaptop.

## Secure Boot

Zorg ervoor dat **Secure Boot** is uitgeschakeld op je systeem. Dit is vaak
nodig voor aangepaste kernelparameters en modules.

## Grub

Grub is een bootloader die het opstartproces van het besturingssysteem beheert.
Het configureren van Grub met specifieke opties is noodzakelijk voor bepaalde
applicaties zoals LXD om correct te functioneren.

{{< alert title="Let op:" >}} Deze stap is alleen nodig voor Ubuntu 22.04. Het
is niet noodzakelijk voor Ubuntu 18.04. {{< /alert >}}

Open het bestand `/etc/default/grub` met een teksteditor.

```bash
sudo nano /etc/default/grub
```

Zoek de regel die begint met `GRUB_CMDLINE_LINUX` en pas deze aan naar:

```bash
GRUB_CMDLINE_LINUX="cgroup_enable=memory swapaccount=1 systemd.unified_cgroup_hierarchy=0"
```

Sla de wijzigingen op en werk Grub bij om ze toe te passen:

```bash
sudo update-grub
```

Herstart je systeem om er zeker van te zijn dat de wijzigingen van kracht
worden:

```bash
sudo reboot
```

Zonder deze aanpassingen werken CentOS 7-machines mogelijk niet correct in het
netwerk en kunnen containers geen IP-adres verkrijgen. Als je een foutmelding
tegenkomt die zegt:

> De container kon binnen 30 seconden geen IPv4-adres verkrijgen.

Dan is deze Grub-configuratie wijziging noodzakelijk.

## Roottoegang zonder Wachtwoord Inschakelen

Roottoegang zonder wachtwoordprompt kan taken die administratieve rechten
vereisen vereenvoudigen.

Voeg je gebruiker toe aan het sudoers bestand zonder wachtwoordvereiste:

```bash
echo "$USER ALL=(ALL) NOPASSWD:ALL" | sudo tee -a /etc/sudoers.d/$USER > /dev/null
```

Hoewel dit de toegang voor taken die rootprivileges nodig hebben vereenvoudigt,
wees voorzichtig en begrijp de veiligheidsimplicaties van het toestaan van
wachtwoordloze sudo toegang.