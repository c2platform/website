---
categories: ["Handleiding"]
tags: [ubuntu, lxd, vagrant]
title: "LXD Initialisatie en Configuratie"
linkTitle: "LXD"
weight: 5
description: >
  Installeer, initialiseer en configureer LXD.
---

---

Deze handleiding biedt instructies voor het installeren, initialiseren en
configureren van [LXD]({{< relref path="../../../concepts/dev/lxd/" >}}). LXD is
de standaard lichtgewicht hypervisor voor het creëren en beheren van lichte VM's
in de vorm van Linux (LXC) containers.

---

## Installeer LXD

De meeste nodes in dit project komen in de vorm van
[LXD](https://linuxcontainers.org/lxd/introduction/) Linux-containers. Hiervoor
moeten we het LXD-profiel `microk8s` en de netwerkbrug `lxdbr1` creëren. Het
`microk8s`-profiel is noodzakelijk voor het installeren van
[MicroK8s](https://microk8s.io/) op **c2d-ks1**, **c2d-ks2**, **c2d-ks3**.

LXD is standaard geïnstalleerd op Ubuntu 22.04, maar we moeten het initialiseren
met `lxd init`. Accepteer alle standaardinstellingen door op de return-toets te
drukken. Let op: de `init` zal een brug `lxdbr0` aanmaken.

```bash
sudo apt install curl -y
sudo snap install lxd
curl -s -L https://gitlab.com/c2platform/ansible/-/raw/master/doc/howto-development/lxd-init-preseed.yml | lxd init --preseed
```

## Maak microk8s-profiel

Maak het `microk8s`-profiel aan.

```bash
lxc profile create microk8s
curl -s -L https://gitlab.com/c2platform/ansible/-/raw/master/doc/howto-development/microk8s.profile.txt | lxc profile edit microk8s
```

## Maak lxdbr1-brug

Maak de brug `lxdbr1` aan en voeg deze toe aan het `default`-profiel.

```bash
lxc network create lxdbr1 ipv6.address=none ipv4.address=1.1.4.1/24 ipv4.nat=true
lxc network attach-profile lxdbr1 default eth1
```

## Stel https_address in

Maak LXD beschikbaar voor de Vagrant LXD-provider door `httpd_address` in te
stellen en vertrouwen te configureren. Zonder deze stap zie je een bericht bij
`vagrant up` vergelijkbaar met:

> De LXD-provider kon niet authenticeren bij de daemon op
> https://127.0.0.1:8443.

```bash
lxc config set core.https_address [::]:8443
```

## Vagrant gesynchroniseerde mappen

Sta ondersteuning toe voor LXD gesynchroniseerde mappen door `/etc/subuid` en
`/etc/subgid` te wijzigen. Zonder deze stap zal je een bericht zien bij `vagrant
up`:

> De hostmachine ondersteunt geen LXD gesynchroniseerde mappen

```bash
echo root:1000:1 | sudo tee -a /etc/subuid
echo root:1000:1 | sudo tee -a /etc/subgid
```

## Dir opslagpool

Op Ubuntu 22 gebruikt de `default` opslagpool de `zfs` driver, wat goed is voor
productieachtige omgevingen maar omslachtig voor ontwikkelingsdoeleinden. Om
deze reden gebruikt dit project een aangepaste opslagpool `c2d` die de `dir`
driver gebruikt.

```bash
lxc storage create c2d dir
lxc profile edit default  # en verander de pool naar c2d
```

<details>
  <summary><kbd>Toon mij</kbd></summary>

```bash
onknows@io3:~/git/gitlab/vagrant-lxd$ lxc storage create c2d dir
Opslagpool c2d aangemaakt
onknows@io3:~$ lxc storage ls
+---------+--------+--------------------------------------------+-------------+---------+---------+
|  NAAM   | DRIVER |                   SOURCE                   | BESCHRIJVING | GEBRUIKT_DOOR |  STATUS  |
+---------+--------+--------------------------------------------+-------------+---------+---------+
| c2d     | dir    | /var/snap/lxd/common/lxd/storage-pools/c2d |             | 0       | AANGEMAAKT |
+---------+--------+--------------------------------------------+-------------+---------+---------+
| default | zfs    | /var/snap/lxd/common/lxd/disks/default.img |             | 0       | AANGEMAAKT |
+---------+--------+--------------------------------------------+-------------+---------+---------+
onknows@io3:~$ lxc profile edit default  # verander de pool naar c2d
onknows@io3:~$ lxc profile show default
config: {}
beschrijving: Standaard LXD-profiel
apparaten:
  eth0:
    naam: eth0
    netwerk: lxdbr0
    type: nic
  eth1:
    nictype: gekoppeld
    parent: lxdbr1
    type: nic
  root:
    pad: /
    pool: c2d
    type: disk
naam: standaard
gebruikt_door: []
```

</details>

## LXC vertrouwen

Start de eerste node op met Vagrant

```bash
c2
vagrant up c2d-rproxy1
```

Dit zal de foutmelding opleveren:

> De LXD-provider kon niet authenticeren bij de daemon op
> https://127.0.0.1:8443.

Dit kun je oplossen met:

```bash
lxc config trust add ~/.vagrant.d/data/lxd/client.crt
```