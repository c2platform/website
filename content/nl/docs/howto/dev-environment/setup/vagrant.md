---
categories: ["Handleiding"]
tags: [vagrant, hashicorp, lxd, sysprep, windows]
title: "Installeer Vagrant en Vagrant Plugins"
linkTitle: "Vagrant"
weight: 4
description: >
  Installeer Vagrant, Vagrant plugin `vagrant-lxd`, `vagrant-windows-sysprep` en activeer Vagrant-autocompletion.
---

Volg deze stappen om Vagrant te installeren:

1. **Installeer Vagrant:** begin met het downloaden en installeren van het
   juiste Vagrant-pakket voor je besturingssysteem:

    ```bash
    sudo wget https://releases.hashicorp.com/vagrant/2.3.1/vagrant_2.3.1-1_amd64.deb
    sudo dpkg -i vagrant_2.3.1-1_amd64.deb
    ```
2. **Installeer Plugins en Activeer Autocompletion:** om de functionaliteit van
   Vagrant uit te breiden met de vagrant-lxd en vagrant-registration plugins, en
   om {{< external-link url="https://www.vagrantup.com/docs/cli" text="Vagrant command completion" htmlproofer_ignore="false" >}} te activeren, voer het
   volgende uit:

    ```bash
    vagrant plugin install vagrant-lxd vagrant-registration
    vagrant autocomplete install --bash
    ```
3. **Installeer de Vagrant Windows Sysprep Provisioner:** Installeer daarnaast
   een aangepaste versie van de {{< external-link url="https://github.com/rgl/vagrant-windows-sysprep" text="Vagrant Windows Sysprep Provisioner" htmlproofer_ignore="false" >}}. Deze versie is ontworpen
   om op een idempotente manier te werken, waardoor je Windows-installaties kunt
   voorbereiden voor hergebruik met behulp van {{< external-link url="https://learn.microsoft.com/en-us/windows-hardware/manufacture/desktop/sysprep--generalize--a-windows-installation" text="Sysprep" htmlproofer_ignore="false" >}}. Volg deze stappen:

    ```bash
    wget https://c2platform.org/downloads/c2-iekeiTh7Fah5Orangooy/vagrant-windows-sysprep-0.0.11.gem
    vagrant plugin install vagrant-windows-sysprep-*.gem
    ```
4. **Optioneel: Automatiseer Red Hat Linux VM Registratie:** voor diegenen die
   gebruik maken van Red Hat Linux VM's, vereenvoudig het registratie- en
   afmeldproces door de omgevingsvariabelen `RHEL_DEV_ACCOUNT` en
   `RHEL_DEV_SECRET` in te stellen. Raadpleeg [Stroomlijnen van RHEL Registratie
   en Abonnementsautomatisering]({{< relref path="/docs/guidelines/dev/rhel" >}}) voor gedetailleerde richtlijnen.