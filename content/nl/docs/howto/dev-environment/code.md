---
categories: ["Handleiding"]
tags: [linux, vscode, laptop, ansible, git]
title: "VS Code-configuratie: Extensies en Tips"
linkTitle: "VS Code"
weight: 4
description: >
  Leer hoe je VS Code instelt, inclusief aanbevolen VS Code-extensies voor Ansible-ontwikkelingstaken.
---

## Extensies

Verbeter je Ansible-ontwikkelervaring in VS Code met deze aanbevolen extensies:

### Linter

{{< external-link url="https://marketplace.visualstudio.com/items?itemName=fnando.linter" text="Linter" htmlproofer_ignore="false" >}}
van Nando Vieira. Deze extensie ondersteunt linters voor verschillende talen, waaronder YAML.

### Trailing Spaces

{{< external-link url="https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces" text="Trailing Spaces" htmlproofer_ignore="false" >}}
van Shardul Mahadik. Visualiseert "trailing spaces" door ze rood te maken. Om deze extensie zo te configureren dat extra spaties automatisch worden verwijderd bij het opslaan:

1. Druk op <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>P</kbd> en selecteer **Voorkeuren: Open Gebruikersinstellingen**.
2. Zoek naar `trailing spaces`.
3. Schakel **Trim bij Opslaan** in.

### Markdown All in One

{{< external-link url="https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one" text="Markdown All in One" htmlproofer_ignore="false" >}}
van Yu Zhang. Een uitgebreide suite voor het schrijven van Markdown, met sneltoetsen, inhoudsopgave, automatische voorvertoning en meer.

### Git Graph

{{< external-link url="https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph" text="Git Graph" htmlproofer_ignore="false" >}}
van mhutchie. Beheer je repository met een visuele git grafiek. Voer eenvoudig Git-acties zoals "cherry picking" direct uit vanuit de grafiek.

### Git Commit (No Verify)

{{< external-link url="https://marketplace.visualstudio.com/items?itemName=DevEscalus.git-commit-no-verify-action-button" text="Git commit(no verify)" htmlproofer_ignore="false" >}}
van DevEscalus. Hiermee kun je de `pre-commit` hook omzeilen en een "no-verify" commit uitvoeren. Om deze functie te gebruiken, activeer je deze in de instellingen na installatie.

{{< alert title="Opmerking:" >}}
Je kunt natuurlijk ook altijd een "no-verify" commit uitvoeren met de Git CLI:

```bash
git commit --no-verify
```

{{< /alert >}}

### Black Formatter

{{< external-link url="https://marketplace.visualstudio.com/items?itemName=ms-python.black-formatter" text="Black Formatter" htmlproofer_ignore="false" >}}
van Microsoft. Vormt automatisch Python-code volgens een standaardstijl. Schakel Black Formatter in door het volgende toe te voegen aan **JSON Gebruikersinstellingen**:

```json
    "[python]": {
        "editor.formatOnType": true,
        "editor.defaultFormatter": "ms-python.black-formatter",
        "editor.formatOnSave": true
    },
```

## Tips

### Meerdere Cursor Selecties

- <kbd>Alt</kbd> + <kbd>Klik</kbd>: Maak een nieuwe cursor op de locatie van je klik.
- <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Omhoog/Omlaag</kbd>: Voeg een nieuwe cursor toe boven of onder de huidige cursor.
- <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>L</kbd>: Maak cursors bij alle voorvallen van de geselecteerde tekst.
- <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Shift</kbd> + <kbd>I</kbd>: Maak cursors aan het begin van elke regel van de geselecteerde tekst.
- <kbd>Ctrl</kbd> + <kbd>D</kbd>: Selecteer de volgende voorval van de geselecteerde tekst en maak een cursor. Herhaal om verder te selecteren.
- <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>Klik</kbd>: Voeg een extra cursor toe op de locatie van je klik voor gelijktijdige bewerkingen.

Je kunt ook het opdrachtenpalet (<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>P</kbd>) gebruiken en "Cursor maken" typen om alle beschikbare opties te zien. Voor Mac-gebruikers, vervang <kbd>Ctrl</kbd> door <kbd>Cmd</kbd> in de genoemde sneltoetsen.

### Vouwen / Uitvouwen

- <kbd>Ctrl</kbd> + <kbd>K</kbd> gevolgd door <kbd>Ctrl</kbd> + <kbd>0</kbd>: Vouwen
- <kbd>Ctrl</kbd> + <kbd>K</kbd> gevolgd door <kbd>Ctrl</kbd> + <kbd>J</kbd>: Uitvouwen

### Aanpassen Lettergrootte

Pas de lettergrootte aan met <kbd>Cntrl</kbd> + <kbd>Shift</kbd> + <kbd>+</kbd> en <kbd>Cntrl</kbd> + <kbd>-</kbd>.

### Commentaar geven / verwijderen

Schakel commentaar in/uit met <kbd>Cntrl</kbd> + <kbd>/</kbd>.

### Omgevingsvariabelen

Om VS Code met specifieke lokale omgevingsvariabelen uit te voeren, maak je een `.env` bestand met je gewenste instellingen. Geef VS Code de opdracht dit bestand te gebruiken door de volgende regel toe te voegen in `.vscode/settings.json`:

```json
{
    "python.envFile": "${workspaceFolder}/.env"
}
```

Zorg ervoor dat het `.env` bestand door Git wordt genegeerd door het toe te voegen aan je `.gitignore`:

```text
.env
```
