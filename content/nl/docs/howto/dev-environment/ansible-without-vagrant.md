---
categories: ["Handleiding"]
tags: ["handleiding"]
title: "Ansible gebruiken zonder Vagrant"
linkTitle: "Ansible zonder Vagrant"
weight: 15
description: >
    Vagrant is standaard, maar je kunt Ansible ook direct gebruiken als je dat liever hebt.
---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

---

Deze handleiding beschrijft hoe je [Ansible]({{< relref path="/docs/concepts/ansible" >}}) zonder [Vagrant]({{< relref path="/docs/concepts/dev/vagrant" >}}) kunt gebruiken binnen dit project. Dit
vereist slechts een kleine SSH-configuratie in `.ssh/config`. Het extra voordeel
is dat je kunt SSH'en naar de machines zonder Vagrant. In deze opzet begint de
`c2d-rproxy1`-node een extra rol te vervullen als {{< external-link url="https://en.wikipedia.org/wiki/Jump_server" text="bastion / jump server" htmlproofer_ignore="false" >}}.

---

## Overzicht

Er zijn verschillende manieren om [Ansible]({{< relref path="/docs/concepts/ansible" >}}) te benutten binnen het
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})
project. Deze verschillende benaderingen worden geïllustreerd in het
PlantUML-diagram hieronder. De standaard en meest efficiënte methode is het
gebruik van [Vagrant]({{< relref path="/docs/concepts/dev/vagrant" >}}),
weergegeven door de zwarte lijn. Vagrant fungeert als een driver voor Ansible
via zijn {{< external-link url="https://developer.hashicorp.com/vagrant/docs/provisioning/ansible" text="Ansible Provisioner" htmlproofer_ignore="false" >}}.

Een andere optie die we momenteel verkennen, weergegeven door de rode lijn, is
het direct gebruiken van Ansible zonder Vagrant.

Aanvullende opties voor het gebruik van Ansible zijn:

* Groene Lijn: Gebruik van de `c2d-xtop` node, die kan worden aangemaakt (met
  Vagrant) door `vagrant up c2d-xtop` uit te voeren. Eenmaal aangemaakt, kun je
  verbinden via een RDP- of SSH-verbinding door `vagrant ssh c2d-xtop` uit te
  voeren.
* Blauwe Lijn: Gebruik maken van [AWX]({{< relref path="/docs/concepts/ansible/aap.md" >}}). Raadpleeg de handleiding voor het
  instellen van AWX: [Setup AWX]({{< relref path="/docs/howto/awx/awx" >}}
  "Handleiding: Setup AWX").

```plantuml
@startuml ansible-vagrant
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

AddRelTag("red", $lineColor="red")
AddRelTag("blue", $lineColor="blue")
AddRelTag("green", $lineColor="green")

Person(engineer, "")

Boundary(laptop, "laptop", $type="ubuntu-22") {
  Boundary(mgmt, "mgmt", $type="") {
    Container(ansible, "Ansible", "c2d-xtop")
    System(c2d_awx_k8s, "Kubernetes", "") {
        Container(c2d_awx1, "AWX", "c2d-awx1")
    }
  }
  Boundary(gis, "apps", $type="") {
    Container(rproxy2, "Reverse Proxy", "c2d-rproxy2")
    Container(rproxy, "Reverse Proxy", "c2d-rproxy1")
  }
  Container(vagrant, "Vagrant", "")
  Container(ansible_local, "Ansible", "")
}

Rel(ansible_local, rproxy, "Voorziening", "ssh", $tags="red")
Rel(ansible_local, rproxy2, "Voorziening", "winrm", $tags="red")
Rel(engineer, ansible, "Remote Desktop", "ssh\nrdp", $tags="green")
Rel(engineer, c2d_awx1, "AWX Webinterface", "https", $tags="blue")
Rel(engineer, vagrant, "Vagrant", "")
Rel_R(vagrant, ansible_local, "", "")
Rel(engineer, ansible_local, "Ansible", "", $tags="red")
@enduml
```

## SSH-configuratie

Bewerk `.ssh/config` en voeg de volgende invoer toe. Dit maakt toegang tot
*alle* nodes mogelijk via SSH-hops via `c2d-rproxy1`.

```
Host c2d-*
  ProxyCommand ssh 1.1.4.205 -W %h:%p
  User vagrant
  IdentityFile ~/.vagrant.d/insecure_private_key
  StrictHostkeyChecking no
  UserKnownHostsFile /dev/null
  LogLevel INFO
  Compression yes
  ServerAliveInterval 10
  ServerAliveCountMax 10
```

## Verificatie

Start `c2d-rproxy1`, `c2d-rproxy2` en maak verbinding met de nodes via SSH. Let
op: we gebruiken `ssh` en niet `vagrant ssh`, zodat we Vagrant volledig
omzeilen.

```bash
vagrant up c2d-rproxy1 c2d-rproxy2
ssh c2d-rproxy1
ssh c2d-rproxy2
```

Nu zou je ook Ansible direct moeten kunnen draaien, zonder Vagrant, bijvoorbeeld
met het volgende commando.

```bash
source ~/.virtualenv/uwd/bin/activate
export ANSIBLE_CONFIG=$PWD/ansible-dev.cfg
ansible-playbook plays/mw/reverse_proxy.yml -i hosts-dev.ini --limit c2d-rproxy2
```

## Voorbeeld

Zie [Beheer de RWS Ansible Execution Environment]({{< relref path="/docs/howto/rws/exe-env" >}}) voor een praktisch voorbeeld waarvoor dit
nuttig is.