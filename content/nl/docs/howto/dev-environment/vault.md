---
categories: ["Handleiding"]
tags: [ansible, vault, ansible-vault]
title: "Ansible Vault"
linkTitle: "Ansible Vault"
weight: 2
description: >
  Beveiligd beheer van geheimen met Ansible Vault.
---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform/rws/ansible-gis`]({{<
relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---
Deze handleiding helpt je effectief gebruik te maken van Ansible Vault om
gevoelige informatie te beschermen en geheimen veilig te beheren binnen je
Ansible-projecten.

---

## Ontwikkeling

In C2 Platform Ansible configuratie-/playbook-projecten is de
standaardbenadering het gebruik van Ansible Vault. In de
*ontwikkelings*omgeving, gebaseerd op [Vagrant]({{< relref path="/docs/concepts/dev/vagrant" >}}), is het gebruik van Ansible Vault niet
strikt noodzakelijk, aangezien het configuratie-/playbook-project voor
ontwikkeling doorgaans open source is zonder echte geheimen als onderdeel van de
[ODM / OSS]({{< relref path="/docs/concepts/oss" >}}) aanpak. Het wordt echter
nog steeds aanbevolen. Raadpleeg voor meer informatie de [Geheimen]({{< relref path="/docs/guidelines/setup/secrets" >}} "Richtlijn: Geheimen").

Voorbeelden zijn te vinden in de Ansible configuratie-/playbook-projecten:
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}),
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}).

Bij gebruik van Vagrant wordt er automatisch een standaardbestand genaamd
`vpass` aangemaakt met de inhoud `secret` (zie de `Vagrantfile` voor details).
Het `vpass` bestand wordt vervolgens door Ansible gebruikt. De configuratie voor
dit gedrag is ingesteld in het bestand `ansible.cfg` met de volgende regel:

```yaml
vault_password_file=vpass
```

Om de vault te bewerken, gebruik je de volgende opdracht:

```bash
ansible-vault edit secret_vars/development/main.yml
```

## Andere omgevingen

In andere omgevingen dan *ontwikkeling* wordt het `vpass` bestand natuurlijk
niet gebruikt. In deze omgevingen worden de geheimen en hun configuratie beheerd
met behulp van de [Red Hat Automation Platform (AAP) of AWX]({{< relref path="/docs/concepts/ansible/aap" >}}) webinterface.

Het commando om de vault te bewerken blijft hetzelfde. Voer het volgende
commando uit vanuit de root van de Ansible configuratie-/playbook:

```bash
ansible-vault edit secret_vars/<environment>/main.yml
```

## Ansible Vault CLI

Om vaults te creëren, ontsleutelen, bewerken of bekijken, kun je de Ansible
Vault CLI-hulp openen door het commando uit te voeren:

```bash
ansible-vault -h
```