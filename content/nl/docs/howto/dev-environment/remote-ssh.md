---
categories: ["Handleiding"]
tags: [linux, ubuntu, ssh]
title: "Externe SSH-sessies"
linkTitle: "externe SSH-sessies"
draft: true
weight: 15
description: >
  Toegang tot een externe server zonder voortdurende wachtwoordherinneringen.
---

Voor SSH-toegang zonder continu wachtwoordherinneringen tijdens externe
SSH-sessies, gebruik:

```bash
eval `ssh-agent -s`
ssh-add
```

Door deze opdrachten uit te voeren, start je de SSH-agent en voeg je je
SSH-sleutels toe, waardoor je niet steeds een wachtwoord hoeft in te voeren voor
elke verbinding.