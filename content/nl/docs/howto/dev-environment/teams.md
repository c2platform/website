---
categories: ["Handleiding"]
tags: [teams, microsoft, ubuntu]
title: "MS Teams op Ubuntu 22"
linkTitle: "MS Teams"
weight: 30
description: >
  Gebruik MS Teams op Ubuntu 22 voor samenwerkingswerk.
---

Als je MS Teams wilt gebruiken op Linux (specifiek Ubuntu 22), is het belangrijk
om te weten dat Microsoft MS Teams niet langer officieel ondersteunt op Linux.
Er is echter een manier om toegang te krijgen tot MS Teams door gebruik te maken
van een ondersteunde browser zoals Chromium. Houd er rekening mee dat Firefox
geen ondersteunde browser is en dat bepaalde functionaliteiten zoals het delen
van schermen mogelijk niet goed werken.

Om toegang te krijgen tot MS Teams op Ubuntu 22:

* Installeer de Chromium-browser op je Ubuntu 22-systeem.
* Start Chromium en ga naar de webversie van MS Teams
  (https://teams.microsoft.com).
* Log in op je MS Teams-account en gebruik de gewenste functies en
  functionaliteiten die beschikbaar zijn in de webversie.