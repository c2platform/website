---  
categories: ["Handleiding"]  
tags: [aap, awx]  
title: "Installeren van Ansible Automation Platform (AAP)"  
linkTitle: "AAP"  
weight: 3  
description: >  
  Deze sectie biedt uitgebreide instructies voor het maken, beheren en benutten van de kracht van het Ansible Automation Platform (AAP). AAP bestaat uit twee  
  belangrijke componenten: de Automation Controller (AWX) en de Ansible Automation Hub (Galaxy NG).  
---