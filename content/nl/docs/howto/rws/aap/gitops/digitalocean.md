---
categories: [Bron]
title: Hoe Multistage Omgevingen te Beheren met Ansible | DigitalOcean
linkTitle: DigitalOcean
draft: true
weight: 1
description: >
  In deze tutorial worden vier inventory-strategieën verkend door de auteur.
---

In {{< external-link url="https://www.digitalocean.com/community/tutorials/how-to-manage-multistage-environments-with-ansible#ansible-recommended-strategy-using-groups-and-multiple-inventories" text="Hoe Multistage Omgevingen te Beheren met Ansible | DigitalOcean" htmlproofer_ignore="false" >}}
door Justin Ellingwood worden vier strategieën verkend. Deze worden hieronder opgesomd
met de uitdagingen die elke van deze benaderingen met zich meebrengen. Dit wordt gevolgd door een
analyse van deze strategieën en de conclusies van het artikel.

## Strategieën

### 1. Alleen Vertrouwen op Groepsvariabelen

- Groepsintersectie kan conflicten veroorzaken.
- Geen expliciete manier om variabele prioriteit te specificeren.
- Alfabetische evaluatie van groepen kan leiden tot onvoorspelbaar gedrag.

### 2. Gebruik van Groepskinderen om een Hiërarchie te Vestigen

- Lost het probleem van groepsintersectie niet op.
- Niet intuïtief en verward de onderscheid tussen echte kinderen en kinderen die de hiërarchie vaststellen.
- Kompromitteert Ansible's doel van duidelijke en makkelijk te volgen configuratie.

### 3. Gebruik van Ansible Constructen die Expliciete Laadvolgorde Toestaan

- Vereist het plaatsen van variabelen op een andere locatie, wat complexiteit toevoegt.
- Elke playbook heeft een sectie nodig om expliciet de juiste variabelebestanden te laden.
- Ad-hoctaken die afhankelijk zijn van variabelen worden bijna onmogelijk.

### 4. Ansible Aanbevolen Strategie: Gebruik van Groepen en Meerdere Inventories

- Duplicatie in de directorystructuur.
- Onvermogen om alle hosts op functie te selecteren over omgevingen heen.
- Variabele uitwisseling tussen omgevingen is niet mogelijk zonder extra stappen.

## Analyse

Met betrekking tot de inventory-strategie neemt de auteur het standpunt in dat een
belangrijk punt van zorg in de Ansible-gemeenschap "groepsintersectie" is. Wanneer een node
deel uitmaakt van meerdere groepen, en de configuratie voor die groepen dezelfde
variabele heeft, zijn de resultaten onvoorspelbaar.

### Groepsintersectie Voorbeeld

Een voorbeeld wordt hieronder weergegeven waar twee Ansible-groepen `fme` en `gs_server` een
variabele `checkmk_folder_path` definiëren. De twee definities hebben dezelfde voorrangwaarde en dan wordt het resultaat bepaald door de alfabetische volgorde van de
groepen waarbij de waarde van de `gs_server` de waarde van de
`fme` groep zal overschrijven.

1. `group_vars/fme/main.yml`
  ```yaml
  checkmk_folder_path: "lcm/fme"
  ```
2. `group_vars/gs_server/main.yml`
  ```yaml
  checkmk_folder_path: "lcm/age/server"
  ```

Dit is een daadwerkelijk voorkomend voorbeeld van het groepsintersectieprobleem, maar
dit kwam slechts één keer voor in het project. En dit was het resultaat van een onlogische
opzet van de groepen. In dit geval werd een FME-node die deel uitmaakte van de FME
groep/rol `fme` ook geplaatst in de **ArcGIS Server** groep `gs_server`
omdat de FME-node gebruik moest maken van de
{{< external-link url="https://developers.arcgis.com/python/" text="ArcGIS API for Python" htmlproofer_ignore="false" >}}
en dit is beschikbaar gemaakt door de ArcGIS-software te installeren en de Windows-service uit te schakelen. De meer logische aanpak om dit te bereiken is niet door een FME-node toe te voegen aan de **ArcGIS Server** groep, maar door de FME-play te veranderen om de
software te installeren. Dit is een meer logische benadering omdat de aard/de rol van de
node als FME Engine niet verandert wanneer de FME een extra
Python-bibliotheek nodig heeft om te communiceren met een **ArcGIS Server**.

We kunnen ons nu een scenario voorstellen waarin we, bijvoorbeeld, een
kleine omgeving willen creëren met een beperkt aantal nodes, waarbij we een node willen die
meerdere rollen vervult. Om door te gaan met het bovenstaande voorbeeld willen we dat de node een **FME Engine** en een **ArcGIS Server** tegelijkertijd is.

In dat geval veroorzaakt de groepsintersectie "onvoorspelbare" resultaten op basis van
alfabetische volgorde om de variabele in te stellen met `checkmk_folder_path`
`lcm/age/server`.

Het doel van de variabele `checkmk_folder_path` is om de node te registreren in
CheckMK-monitoring onder een pad dat overeenkomt met de rol van de node, maar met dit
scenario werkt dit niet, omdat de node twee rollen heeft.

Het interessante punt om op dit moment te maken met betrekking tot de aanpak voor het oplossen
van groepsintersectieproblemen door gebruik te maken van meerdere inventories, één voor elke
omgeving, lost dit probleem niet op!

En eigenlijk voorkomt het splitsen van de inventory op basis van omgeving
helemaal geen groepintersectie, omdat nodes altijd deel uitmaken van 1 omgeving, niet meer,
niet minder. De cruciale vraag hier is:

{{< alert >}}
Gezien dat een node altijd deel uitmaakt van slechts één omgeving, hoe voorkomen
we variabele conflicten door de configuratie van verschillende omgevingen te scheiden?
Welke specifieke groepsintersecties voorkomen we met deze aanpak?
{{< /alert >}}

Het antwoord is dat we geen enkele groepsintersectieproblemen voorkomen
door de inventory op basis van omgeving te splitsen.

Dus de vierde strategie, de door Ansible aanbevolen strategie, lost "geen"
groepsintersectieproblemen op.

Het daadwerkelijke probleem dat we willen beheren zijn omgevingsverschillen. Dit is geen groepintersectie-/voorrangsprobleem

Als je `development`, `staging`, `production` en een groep `fme` hebt.
`fme_flow_max_memory: 8gb` Geheugeninstelling of een ander soort dat
verschilt per omgeving? Je plaatst de instelling in de omgevingsgroep.

### Voorangevolgorde Regels

De
{{< external-link url="https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_variables.html#understanding-variable-precedence" text="Ansible documentatie" htmlproofer_ignore="false" >}}
somt 22 plaatsen op waar configuratie kan worden opgeslagen, variabelen kunnen worden gedefinieerd. De
tabel hieronder is een vereenvoudigde lijst van voorrangvolgorde.

|    |                  |                                                                                                                                                       |
|----|------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| 2  | Rolstandaarden   | ✔️                                                                                                                                                    |
| 4  | `group_vars/all` | ✔️                                                                                                                                                    |
| 6  | `group_vars/*`   | ✔️                                                                                                                                                    |
| 9  | `host_vars/*`    | ❌, tenzij                                                                                                                                             |
| 12 | Plays `vars`     | ❌, tenzij                                                                                                                                             |
| 15 | Rol `vars`      | ❌ → [StackOverflow](https://stackoverflow.com/questions/29127560/whats-the-difference-between-defaults-and-vars-in-an-ansible-role/58078985#58078985) |
| 18 | `include_vars`   | ✔️ → [Ansible Vault](https://c2platform.org/docs/guidelines/setup/secrets/)                                                                                |