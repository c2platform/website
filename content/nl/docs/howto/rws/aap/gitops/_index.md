---
categories: ["Handleiding"]
tags: [awx, ansible, dtap, promotie model, Execution Environment, requirements.yml]
title: "GitOps Pipeline voor een Execution Environment (EE) met Ansible Collections"
linkTitle: "GitOps Pipeline"
weight: 2
description: >
    Leer hoe je een GitOps Pipeline kunt realiseren met een EE die Ansible Collections bevat.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform/ansible-collection-core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}),
[`c2platform/ansible-collection-mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}),
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}}),
[`c2platform/ansible-collection-mgmt`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mgmt" >}})

---

## Introductie

Bij gebruik van het Ansible Automation Platform (AAP) zijn er drie strategieën voor het beheren van Ansible-collecties:

1. **Verwerk collecties in de EE**.
2. **Haal collecties op met behulp van `requirements.yml`**[^1].
3. **Een combinatie van beide**.

Elke aanpak heeft zijn voor- en nadelen. Een belangrijk nadeel van het gebruik van een EE is dat de EE en zijn versie extern zijn gedefinieerd in AAP, waardoor een pure GitOps-benadering wordt voorkomen. Deze handleiding beschrijft hoe je een AAP Workflow en een Ansible Job Template gebruikt om AAP te configureren zodat een specifieke EE-versie wordt gebruikt voordat andere Job Templates worden uitgevoerd. Het Job Template maakt gebruik van de Ansible-rol `c2platform.mgmt.awx` om de EE-versie te beheren. Red Hat raadt aan om alleen de EE te gebruiken om prestatieredenen[^2].

## Collectie Strategieën

### Gebruik alleen het bestand `requirements.yml`

Dit is eenvoudig en flexibel, geschikt aan het begin van een project wanneer veel updates aan collecties worden gedaan. Het nadeel is langere jobtijden omdat collecties telkens weer worden opgehaald.

```yaml
---
collections:
   - name: c2platform.core
      version: 1.0.21
   - name: https://gitlab.com/c2platform/rws/ansible-collection-wincore.git
      type: git
      version: master
```

<p><details>
<summary><kbd>Toon Diagram</kbd></summary><p>

```plantuml
@startuml rws-gis-automation-requirements-yml
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads

'title [Container] GIS Platform Automation voor Operaties

AddRelTag("optioneel", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

Person(rws_operator, "Ansible Operator", "RWS GIS Platform engineer (basiskennis van Ansible)")

System_Ext(galaxy, "Galaxy\nansible.galaxy.com", "Host Ansible content")
System(gis, "GIS_LABEL", "Biedt GEO-informatie en verwerkingsdiensten")
System_Ext(azure_devops, "Azure DevOps\ndev.azure.com/Rijkswaterstaat", "Host RWS projecten") {
ContainerDb(gis_inventory, "GIS_INVENTORY_LABEL", "GIS_INVENTORY_TECH", "GIS_INVENTORY_DESC")
}
System_Ext(gitlab, "GitLab\ngitlab.com/c2platform/rws", "Host de GIS Platform open source projecten") {
ContainerDb(gis_ee, "GIS_EE_LABEL", "GIS_EE_TECH", "GIS Platform Execution Environment (EE) met Pythoon, Ansible")
ContainerDb(gis_collections, "Ansible Collecties", "Ansible,Git", "GIS Platform Ansible Collecties bijv. GIS Collectie en Wincore Collectie")
}

System_Ext(aap, "Ansible\nAutomation Platform", "Beheert nodes en implementeert Ansible Playbooks over omgevingen heen")

Lay_R(gis_ee, gis_collections)
Rel(aap, gis_inventory, "1) Source Control Update en \n3) Inventory Sync", "https,git,vault")
Rel(aap, galaxy, "2) Haal collecties op", "https")
Rel(aap, gis_ee, "4) Download\nRWS GIS Ansible EE", "Docker pull")
Rel_R(aap, gis, "5) Implementeer, update, upgrade diensten van")
Rel(rws_operator,gis_inventory,"Voert LCM en TAM taken uit door wijzigingen aan te brengen in", "Browser, Web Edit/IDE, Git Push en Pull")
Rel_R(rws_operator, aap, "Voer jobs uit\nbekijk logs etc.", "browser, https")
Rel_R(gis_collections, galaxy, "Release collecties", "GitLab CI/CD Pipeline")

LAYOUT_WITH_LEGEND()
@enduml
```

</p></details></p>

### Gebruik alleen de EE

Jobs worden sneller uitgevoerd op AWX. Het nadeel is de behoefte om nieuwe versies van de collectie en EE vrij te geven en handmatig de nieuwe EE-versie in AWX te configureren[^3].

<p><details>
   <summary><kbd>Toon Diagram</kbd></summary><p>

```plantuml
@startuml rws-gis-automation-ee
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads

'title [Container] GIS Platform Automation voor Operaties

AddRelTag("optioneel", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

Person(rws_operator, "Ansible Operator", "RWS GIS Platform engineer (basiskennis van Ansible)")

System_Ext(galaxy, "Galaxy\nansible.galaxy.com", "Host Ansible content")
System(gis, "GIS_LABEL", "Biedt GEO-informatie en verwerkingsdiensten")
System_Ext(azure_devops, "Azure DevOps\ndev.azure.com/Rijkswaterstaat", "Host RWS projecten") {
ContainerDb(gis_inventory, "GIS_INVENTORY_LABEL", "GIS_INVENTORY_TECH", "GIS_INVENTORY_DESC")
}
System_Ext(gitlab, "GitLab\ngitlab.com/c2platform/rws", "Host de GIS Platform open source projecten") {
ContainerDb(gis_ee, "GIS_EE_LABEL", "GIS_EE_TECH", "GIS_EE_DESC")
ContainerDb(gis_collections, "Ansible Collecties", "Ansible,Git", "GIS Platform Ansible Collecties bijv. GIS Collectie en Wincore Collectie")
}

System_Ext(aap, "Ansible\nAutomation Platform", "Beheert nodes en implementeert Ansible Playbooks over omgevingen heen")

Rel(aap, gis_inventory, "1) Source Control Update en \n2) Inventory Sync", "https,git,vault")
Rel(aap, gis_ee, "3) Download\nRWS GIS Ansible EE", "Docker pull")
Rel_R(aap, gis, "4) Implementeer, update, upgrade diensten van")
Rel(rws_operator,gis_inventory,"Voert LCM en TAM taken uit door wijzigingen aan te brengen in", "Browser, Web Edit/IDE, Git Push en Pull")
Rel_R(rws_operator, aap, "Voer jobs uit\nbekijk logs etc.", "browser, https")
Rel(gis_ee, galaxy, "Haal collecties op", "GitLab CI/CD Pipeline")
Rel(gis_collections, galaxy, "Release collecties", "GitLab CI/CD Pipeline")

LAYOUT_WITH_LEGEND()
@enduml
```

</p></details></p>

### Gebruik zowel de EE als de `requirements.yml`

Voeg community-collecties toe aan de EE en gebruik `requirements.yml` voor regelmatig bijgewerkte collecties.

Een nadeel van het toevoegen van collecties aan de EE is dat het een DTAP-promotiemodel in Git ingewikkelder maakt. Dit kan worden opgelost door een workflow in AWX in te richten die de juiste EE-versie configureert.

## Vereisten

* Zorg ervoor dat `gsd-awx1` actief is. Raadpleeg [Stel de Automation Controller (AWX) in met behulp van
  Ansible]({{< relref path="/docs/howto/rws/aap/awx" >}}).
* Stel SSH-verbindingen in als je Ansible-plays uitvoert zonder Vagrant. Raadpleeg [Ansible gebruiken zonder Vagrant]({{< relref path="/docs/howto/rws/dev-environment/ansible-without-vagrant" >}}).

## Wijzig de EE

Dit gedeelte laat zien hoe je de EE in AAP kunt wijzigen in een workflow job, zodat de nieuwe EE beschikbaar wordt voor jobs die daarna worden uitgevoerd.

1. In het project `ansible-gis`, schakel over naar de `development` branch en bewerk `group_vars/awx/awx.yml`. Definieer `gs_ee_images` als volgt:
   ```yaml
   gs_ee_images:
     default: registry.gitlab.com/c2platform/rws/ansible-execution-environment:0.1.18
     development: quay.io/ansible/awx-ee:24.4.0
   ```
   Verwijder of schakel de regel met `development` uit, commit en push terug naar de development branch.
2. Navigeer naar {{< external-link url="https://awx.c2platform.org/#/execution_environments" htmlproofer_ignore="true" >}}. Controleer of `quay.io/ansible/awx-ee:24.4.0` de EE is.
3. Navigeer naar {{< external-link url="https://awx.c2platform.org/#/templates" htmlproofer_ignore="true" >}} en klik op de Visualizer link van
   `gsd-awx-collections-workflow`. Dit toont een workflow met vijf nodes.
4. Start de workflow.
5. Wanneer de workflow is voltooid, controleer of `gsd-collections1`
   `quay.io/ansible/awx-ee:24.4.0` toont en `gsd-collections2`
   `registry.gitlab.com/c2platform/rws/ansible-execution-environment:0.1.18`.

Dit toont aan dat we de EE in ons DTAP-promotiemodel kunnen opnemen op een vergelijkbare manier als we het bestand `requirements.yml` gebruiken door een workflow in te richten die een taak zoals `gsd-awx-ee` omvat om de EE voor de omgeving te beheren.

## Promoveer de EE

Dit deel laat zien hoe we wijzigingen, inclusief de EE, kunnen promoten van `development` naar `test`.

1. Maak de configuratie in AWX voor de `test` omgeving met behulp van node
   `gst-awx1`[^4]:
   ```bash
   export PLAY=plays/mgmt/awx_config.yml
   ansible-playbook $PLAY -i hosts.ini --limit=gst-awx1
   ```
   {{< alert title="Let op:" >}}Ansible commando's rechtstreeks uitvoeren werkt alleen als je de nodige SSH configuratie voor hebt ingesteld. Raadpleeg [Ansible gebruiken zonder Vagrant]({{< relref path="/docs/howto/rws/dev-environment/ansible-without-vagrant" >}}) voor meer informatie.{{< /alert >}}
2. Gebruik de AWX webinterface en start `gst-awx-collections-workflow`.
3. Controleer na voltooiing of er geen verschil is tussen collecties in
   `gst-collections1` en `gst-collections2`.
4. In de `development` branch, wijzig je het standaardbeeld naar `0.1.19`. Commit en push.
   ```yaml
   gs_ee_images:
     default: registry.gitlab.com/c2platform/rws/ansible-execution-environment:0.1.19
     development: quay.io/ansible/awx-ee:24.4.0
   ```
5. Voeg wijzigingen samen naar de `test` branch en start `gst-awx-collections-workflow`.
6. Controleer na voltooiing de verschillen tussen de outputs van `gst-collections1`
   en `gst-collections2`.

## Review

In het Ansible Inventory project [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}):

1. De play `plays/mgmt/awx.yml`, die Ansible rollen toont die gebruikt worden om de `gsd-awx1` node te creëren.
2. Bestanden binnen `group_vars/awx`. Deze bestanden bevatten configuratie.
3. Het bestand `Vagrantfile.yml`, dat de Vagrant LXD machine definieert.

De Ansible rol `c2platform.mgmt.awx` in de Ansible Management Collectie
[`c2platform/ansible-collection-mgmt`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mgmt" >}}). Deze rol gebruikt
modules uit de `awx.awx` collectie om AWX te configureren (job templates,
workflows).

De Ansible rollen `c2platform.mw.microk8s` en `c2platform.mw.kubernetes` in de
Ansible Middleware collectie [`c2platform/ansible-collection-mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}). De eerste rol
creëert een Kubernetes instantie; de tweede installeert AWX op het cluster.


## Tips

Als je experimenteert of ontwikkelt met AWX configuratie, kun je tags gebruiken
om de provisioning te versnellen:

```bash
export PLAY=plays/mgmt/awx_config.yml
ansible-playbook $PLAY -i hosts.ini --limit=gsd-awx1 --tags secrets,awx_workflow_job_template,awx_workflow_job_template_node
```

Je kunt ook tags gebruiken bij gebruik van Vagrant:

```bash
export PLAY=plays/mgmt/awx_config.yml
TAGS=secrets,awx_workflow_job_template,awx_workflow_job_template_node vagrant provision gsd-awx1
```

## Notities

[^1]: Het requirements bestand moet worden opgeslagen in de map `collections`, zoals het geval is in het `ansible-gis` project.

[^2]: Er is geen officiële bron voor deze positie. Dit is gecommuniceerd door een
    Red Hat consultant aan RWS nadat er problemen waren gemeld over de tijd die
    het kost om Ansible-collecties op te halen van Galaxy en Ansible Automation Hub op de locatie van RWS.

[^3]: Althans, dit is de huidige opzet van het project. Het is mogelijk om de GitLab CI/CD-pijplijn voor de EE te wijzigen om een `latest` versie te publiceren, niet gebaseerd op vrijgegeven Galaxy-collecties, maar door direct collecties uit de Git-repository master branch te halen. Met behulp van webhooks zouden wijzigingen in
    collecties de pijplijn triggeren om een nieuwe `latest` versie te produceren. In
    AWX kan deze laatste versie dan worden geconfigureerd voor gebruik.

[^4]: De node `gst-awx1` is geen normale Vagrant machine; het is slechts een alias
    voor `gsd-awx1`. In `hosts.ini` definiëren we het als een aparte node binnen de
    `test` omgeving zodat we AWX configuratie voor deze
    omgeving kunnen creëren. Om deze reden kunnen we ook `gst-awx1` niet beheren met
    Vagrant; het bestaat niet voor Vagrant maar alleen voor Ansible.