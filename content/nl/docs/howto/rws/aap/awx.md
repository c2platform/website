---
categories: ["Handleiding"]
tags: [awx, ansible, operator, kubernetes]
title: "Automatiseringscontroller ( AWX ) instellen met Ansible"
linkTitle: "Automatiseringscontroller ( AWX )"
provisionTime: 9 minuten
weight: 1
description: >
   Leer hoe u de Ansible Automatiseringscontroller (AWX) kunt maken met
   gebruik van Ansible, door de AWX Operator te gebruiken. Deze sectie leidt je ook door
   het configureren van de controller met Ansible, inclusief het instellen van een organisatie,
   inloggegevens, een Ansible-executieomgeving en taaksjablonen, die
   het inrichten vergemakkelijken.
---

Projecten: [`c2platform/gis/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}),
[`c2platform.mgmt`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mgmt" >}}),
[`ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})

---

## Overzicht

Deze handleiding geeft stapsgewijze instructies over hoe je een [AWX]({{< relref path="/docs/concepts/ansible/aap" >}}) instance creëert op de `gsd-awx1` node met behulp van de {{< external-link url="https://github.com/ansible/awx-operator/blob/devel/.helm/starter/README.md" text="AWX Operator Helm Chart" htmlproofer_ignore="false" >}} en [Ansible]({{< relref path="/docs/concepts/ansible" >}}). Deze Ansible play is in wezen hetzelfde als [Automatiseringscontroller ( AWX ) instellen met Ansible]({{< relref path="/docs/howto/awx/awx" >}}), raadpleeg die handleiding voor meer informatie.

| Node       | OS        | Provider | Doel                         |
|------------|-----------|----------|------------------------------|
| `gsd-awx1` | Ubuntu 22 | LXD      | Kubernetes cluster met AWX   |


<!-- include-start: howto-prerequisites-rws.md -->
## Vereisten

Maak de reverse en forward proxy `gsd-rproxy1`.

```bash
vagrant up gsd-rproxy1
```

Voor meer informatie over de verschillende rollen die `gsd-rproxy1` vervult in dit project:

* [Reverse Proxy en CA-server instellen]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [SOCKS-proxy opzetten]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Servercertificaten beheren als een Certificaatautoriteit]({{< relref path="/docs/howto/c2/certs">}})
* [ArcGIS Server software en licenties]({{< relref path="/docs/howto/rws/dev-environment/setup/software" >}}).
<!-- include-end -->

## Setup

Om de Kubernetes instance te maken en de AWX instance te implementeren, voer je het volgende commando uit:

```bash
vagrant up gsd-awx1
```

{{< alert type="warning" title="Waarschuwing!" >}} AWX kan enige tijd nodig hebben om beschikbaar te komen, wat mogelijk kan leiden tot een fout in Ansible tijdens stap 4, **AWX Aanpassing:**. Als je een fout tegenkomt, zorg er dan voor dat je toegang hebt tot de AWX webinterface. Zodra je het inlogscherm ziet, kun je het inrichtingproces opnieuw uitvoeren met het volgende commando:

```bash
vagrant provision gsd-awx1
```
{{< /alert >}}

## Verifiëren

Volg deze stappen om je AWX instance te verifiëren:

### Verbinden

Als je `kubectl` op je host hebt geïnstalleerd, kun je ook toegang tot de cluster krijgen door de volgende commando's uit te voeren:

```bash
export BOX=gsd-awx1
sudo snap install kubectl --classic
vagrant ssh $BOX -c "microk8s config" > ~/.kube/config
kubectl get all --all-namespaces
```

<p><details>
  <summary><kbd>Toon mij</kbd></summary><p>

```bash
NAMESPACE            NAME                                                  READY   STATUS      RESTARTS   AGE
kube-system          pod/hostpath-provisioner-766849dd9d-jdd5b             1/1     Running     0          17m
kube-system          pod/calico-node-vf72q                                 1/1     Running     0          17m
kube-system          pod/coredns-d489fb88-8xpwb                            1/1     Running     0          17m
kube-system          pod/calico-kube-controllers-d8b9b6478-2dj4s           1/1     Running     0          17m
container-registry   pod/registry-6674bf676f-twgpz                         1/1     Running     0          17m
kube-system          pod/dashboard-metrics-scraper-64bcc67c9c-5hbvz        1/1     Running     0          15m
kube-system          pod/kubernetes-dashboard-74b66d7f9c-8hqgf             1/1     Running     0          15m
metallb-system       pod/controller-56c4696b5-w2xbs                        1/1     Running     0          15m
metallb-system       pod/speaker-pkkpk                                     1/1     Running     0          15m
cert-manager         pod/cert-manager-cainjector-7985fb445b-jdkk6          1/1     Running     0          15m
cert-manager         pod/cert-manager-655bf9748f-6jxbg                     1/1     Running     0          15m
kube-system          pod/metrics-server-6b6844c455-7564v                   1/1     Running     0          15m
cert-manager         pod/cert-manager-webhook-6dc9656f89-br48w             1/1     Running     0          15m
awx                  pod/awx-operator-controller-manager-699d64766-ltpxh   2/2     Running     0          15m
awx                  pod/awx-postgres-15-0                                 1/1     Running     0          14m
awx                  pod/awx-web-6cb68b86cc-dp8tr                          3/3     Running     0          14m
awx                  pod/awx-migration-24.4.0-cljp4                        0/1     Completed   0          13m
awx                  pod/awx-task-5c96c47f86-r7vn7                         4/4     Running     0          14m

NAMESPACE            NAME                                                      TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                  AGE
default              service/kubernetes                                        ClusterIP      10.152.183.1     <none>        443/TCP                  17m
container-registry   service/registry                                          NodePort       10.152.183.38    <none>        5000:32000/TCP           17m
kube-system          service/kube-dns                                          ClusterIP      10.152.183.10    <none>        53/UDP,53/TCP,9153/TCP   17m
kube-system          service/metrics-server                                    ClusterIP      10.152.183.147   <none>        443/TCP                  16m
kube-system          service/kubernetes-dashboard                              ClusterIP      10.152.183.175   <none>        443/TCP                  16m
kube-system          service/dashboard-metrics-scraper                         ClusterIP      10.152.183.61    <none>        8000/TCP                 16m
metallb-system       service/webhook-service                                   ClusterIP      10.152.183.124   <none>        443/TCP                  16m
cert-manager         service/cert-manager                                      ClusterIP      10.152.183.51    <none>        9402/TCP                 15m
cert-manager         service/cert-manager-webhook                              ClusterIP      10.152.183.177   <none>        443/TCP                  15m
kube-system          service/dashboard-lb                                      LoadBalancer   10.152.183.133   1.1.4.16      443:32189/TCP            15m
awx                  service/awx-operator-controller-manager-metrics-service   ClusterIP      10.152.183.195   <none>        8443/TCP                 15m
awx                  service/awx-postgres-15                                   ClusterIP      None             <none>        5432/TCP                 14m
awx                  service/awx-service                                       ClusterIP      10.152.183.88    <none>        80/TCP                   14m
awx                  service/awx-service-lb                                    LoadBalancer   10.152.183.29    1.1.4.17      8052:32477/TCP           11m

NAMESPACE        NAME                         DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kube-system      daemonset.apps/calico-node   1         1         1       1            1           kubernetes.io/os=linux   17m
metallb-system   daemonset.apps/speaker       1         1         1       1            1           kubernetes.io/os=linux   16m

NAMESPACE            NAME                                              READY   UP-TO-DATE   AVAILABLE   AGE
kube-system          deployment.apps/calico-kube-controllers           1/1     1            1           17m
kube-system          deployment.apps/hostpath-provisioner              1/1     1            1           17m
kube-system          deployment.apps/coredns                           1/1     1            1           17m
container-registry   deployment.apps/registry                          1/1     1            1           17m
kube-system          deployment.apps/dashboard-metrics-scraper         1/1     1            1           16m
kube-system          deployment.apps/kubernetes-dashboard              1/1     1            1           16m
metallb-system       deployment.apps/controller                        1/1     1            1           16m
cert-manager         deployment.apps/cert-manager-cainjector           1/1     1            1           15m
cert-manager         deployment.apps/cert-manager                      1/1     1            1           15m
kube-system          deployment.apps/metrics-server                    1/1     1            1           16m
cert-manager         deployment.apps/cert-manager-webhook              1/1     1            1           15m
awx                  deployment.apps/awx-operator-controller-manager   1/1     1            1           15m
awx                  deployment.apps/awx-web                           1/1     1            1           14m
awx                  deployment.apps/awx-task                          1/1     1            1           14m

NAMESPACE            NAME                                                        DESIRED   CURRENT   READY   AGE
kube-system          replicaset.apps/calico-kube-controllers-d8b9b6478           1         1         1       17m
kube-system          replicaset.apps/hostpath-provisioner-766849dd9d             1         1         1       17m
kube-system          replicaset.apps/coredns-d489fb88                            1         1         1       17m
container-registry   replicaset.apps/registry-6674bf676f                         1         1         1       17m
kube-system          replicaset.apps/dashboard-metrics-scraper-64bcc67c9c        1         1         1       15m
kube-system          replicaset.apps/kubernetes-dashboard-74b66d7f9c             1         1         1       15m
metallb-system       replicaset.apps/controller-56c4696b5                        1         1         1       15m
cert-manager         replicaset.apps/cert-manager-cainjector-7985fb445b          1         1         1       15m
cert-manager         replicaset.apps/cert-manager-655bf9748f                     1         1         1       15m
kube-system          replicaset.apps/metrics-server-6b6844c455                   1         1         1       15m
cert-manager         replicaset.apps/cert-manager-webhook-6dc9656f89             1         1         1       15m
awx                  replicaset.apps/awx-operator-controller-manager-699d64766   1         1         1       15m
awx                  replicaset.apps/awx-web-6cb68b86cc                          1         1         1       14m
awx                  replicaset.apps/awx-task-5c96c47f86                         1         1         1       14m

NAMESPACE   NAME                               READY   AGE
awx         statefulset.apps/awx-postgres-15   1/1     14m

NAMESPACE   NAME                             COMPLETIONS   DURATION   AGE
awx         job.batch/awx-migration-24.4.0   1/1           102s       13m
```

</p></details></p>

Zie
[Verbinden met een Kubernetes Cluster]({{< relref path="/docs/howto/dev-environment/k8s" >}})
voor meer informatie.

### Kubernetes Dashboard

1. Toegang tot het Kubernetes Dashboard door te gaan naar {{< external-link url="https://dashboard-awx.c2platform.org/" htmlproofer_ignore="true" >}} en in te loggen met een token. Verkrijg het token door het `kubectl` commando uit te voeren zoals hieronder weergegeven binnen de `gsd-awx1` node.
2. Eenmaal ingelogd, navigeer naar de {{< external-link url="https://dashboard-awx.c2platform.org/#/workloads?namespace=awx" text="awx" htmlproofer_ignore="true" >}} namespace. Controleer of de `awx-web` en `awx-task` pods zonder fouten draaien.

```bash
kubectl -n kube-system describe secret microk8s-dashboard-token
```

Voor meer informatie over het verkrijgen van het token en het instellen van het Kubernetes Dashboard, raadpleeg de
[Het Kubernetes Dashboard instellen]({{< relref path="/docs/howto/kubernetes/dashboard" >}})
gids.

### AWX

1. Om toegang te krijgen tot de AWX webinterface, ga naar {{< external-link url="https://awx.c2platform.org" htmlproofer_ignore="true" >}}. Gebruik de `admin` account met het wachtwoord `secret` om in te loggen.
2. Eenmaal ingelogd, navigeer naar de {{< external-link url="https://awx.c2platform.org/#/templates" text="Sjablonen" htmlproofer_ignore="true" >}} sectie.
3. Van daaruit kun je verschillende plays starten, zoals de reverse proxy play `c2-reverse-proxy` of een van de AWX plays `c2-awx` of `c2-awx-config`.

## Beoordeling

Zie
[Automatiseringscontroller ( AWX ) instellen met Ansible]({{< relref path="/docs/howto/awx/awx" >}})