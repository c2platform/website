---
categories: ["Handleiding"]
tags: [ansible, desktop, xrdp, ide, rhel, rdp, remmina, kerberos]
title: "Instellen van Ansible Control Node"
linkTitle: "Ansible Control Node"
provisionTime: 15 minuten implementatie
weight: 11
description: >
  Stel een Ansible Control Node in op basis van Red Hat 8 op `gsd-ansible`.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

---
In deze tutorial worden stap-voor-stap instructies gegeven voor het opzetten van een {{<
external-link
url="https://docs.ansible.com/ansible/latest/network/getting_started/basic_concepts.html#control-node"
text="Ansible Control Node" htmlproofer_ignore="true" >}} op Red Hat 8,
specifiek op de `gsd-ansible` node. Hoewel het voornamelijk is bedoeld voor **Ansible
Ad-Hoc** taken, kan deze control node ook dienen als een grafische gebruikersinterface
(GUI) voor lichtgewicht ontwikkelingswerk, toegankelijk via RDP (Remote Desktop
Protocol). Door deze richtlijnen te volgen, kun je een veelzijdige control node
omgeving creëren die zowel snelle automatiseringstaken als handige ontwikkelingsmogelijkheden ondersteunt.

---

## Overzicht

Deze handleiding richt zich op het opzetten van een Ansible Control Node / lichtgewicht ontwikkelomgeving. Deze installatie omvat Vagrant en Ansible voor de uitvoering van de volgende taken:

1. Vagrant gebruikt de VirtualBox Provider om een Red Hat 8 VM te creëren, inclusief het registreren van de node bij Red Hat met behulp van een Developer licentie-abonnement. Zie
   [Het stroomlijnen van RHEL registratie en abonnement automatisering]({{< relref path="/docs/guidelines/dev/rhel" >}})
   voor meer informatie.
2. Vagrant gebruikt de Ansible-beheerder om een playbook uit te voeren dat essentiële pakketten zoals pip, setuptools, psycopg2-binary, curl, git en tree installeert of upgradet. Daarnaast wordt Xrdp geconfigureerd om bij het opstarten te starten en wordt er een Red Hat Desktopomgeving ingesteld.

| Node          | OS        | Provider   | Doel                                               |
|---------------|-----------|------------|----------------------------------------------------|
| `gsd-ansible` | Red Hat 8 | VirtualBox | Ansible Controle Node en lichtgewicht ontwikkelomgeving |

Het onderstaande diagram illustreert het standaard implementatieproces in de ontwikkelomgeving met behulp van Vagrant en Ansible. De volgende VMs worden gemaakt: `gsd-ansible`, `gsd-agserver1` en `gsd-rproxy1`.

<p>

```plantuml
@startuml ansible-vagrant
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "")

Boundary(laptop, "laptop", $type="ubuntu-22") {
  Boundary(mgmt, "mgmt", $type="") {
    Container(ansible, "Ansible", "gsd-ansible 8.105")
  }
  Boundary(gis, "gis.c2platform.org", $type="") {
    Container(agserver1, "ArcGIS Server", "gsd-server1 8.100")
    Container(rproxy, "Reverse Proxy", "gsd-rproxy1 5.205")
  }
  Container(vagrant, "Vagrant", "")
}

Rel(engineer, vagrant, "", "")
Rel(vagrant, ansible, "Voorzien", "ssh")
Rel(vagrant, rproxy, "Voorzien", "ssh")
Rel(vagrant, agserver1, "Voorzien", "winrm")
@enduml
```

</p>

De Ansible Control Node, `gsd-ansible`, kan afzonderlijk worden gebruikt om de nodes te voorzien en te beheren met Ansible zonder afhankelijk te zijn van Vagrant.

<p>

```plantuml
@startuml ansible-control-node
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "")

Boundary(laptop, "laptop", $type="ubuntu-22") {
  Boundary(mgmt, "mgmt", $type="") {
    Container(ansible, "Ansible", "gsd-ansible 8.105")
  }
  Boundary(gis, "gis.c2platform.org", $type="") {
    Container(agserver1, "ArcGIS Server", "gsd-server1 8.100")
    Container(rproxy, "Reverse Proxy", "gsd-rproxy1 5.205")
  }
  Container(vagrant, "Vagrant", "")
}

Rel(ansible, rproxy, "Voorzien", "ssh")
Rel(ansible, agserver1, "Voorzien", "winrm")
Rel(ansible, ansible, "Voorzien", "ssh")
Rel(engineer, ansible, "", "ssh\nrdp")
@enduml
```

</p>

Deze diagrammen tonen de twee provisioning benaderingen: één met behulp van Vagrant en Ansible samen, en de andere met Ansible onafhankelijk met de Ansible Control Node.

## Vereisten

* Red Hat vereist natuurlijk een abonnement. In de RWS ontwikkelomgeving kunnen we een {{< external-link url="https://developers.redhat.com/articles/getting-red-hat-developer-subscription-what-rhel-users-need-know" text="ontwikkelaarslicentie" htmlproofer_ignore="true" >}} gebruiken.
* [RWS Ontwikkelomgeving]({{< relref path="./dev-environment" >}}).

## Installatie

Om de node in te stellen, voer je het volgende commando uit:

```bash
vagrant up gsd-ansible
```

## Verifiëren

Om te verifiëren dat de node correct is aangemaakt/geïmplementeerd, wordt in deze sectie beschreven hoe je verbinding maakt met de node, een Ansible-omgeving creëert, Ansible installeert en vervolgens node `gsd-rproxy1` voorziet met behulp van deze omgeving. Deze stappen bootsen de stappen na die elke engineer bij RWS moet volgen om hun eigen gepersonaliseerde Ansible-werkruimte in te richten.

### RDP Verbinding

Je kunt nu een RDP-verbinding maken met deze node met behulp van bijvoorbeeld {{< external-link url="https://remmina.org/" text="Remmina" htmlproofer_ignore="false" >}}.
Je zou moeten kunnen verbinden met gebruiker `vagrant` met wachtwoord `vagrant`.

{{< alert title="Opmerking:" >}}
De twee onderstaande secties gaan ervan uit dat je binnen de `gsd-ansible` VM bent via een RDP
verbinding of SSH.
{{< /alert >}}

### Ansible-omgeving

Installeer {{< external-link url="https://github.com/pyenv/pyenv" text="pyenv" htmlproofer_ignore="true" >}}:

```bash
curl https://pyenv.run | bash
```

Voeg de volgende regels toe aan `~/.bashrc`:

```bash
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
```

Installeer Python 3.10.6

```bash
source ~/.bashrc
pyenv install 3.10.6
pyenv global 3.10.6
```

Maak Python virtuele omgeving aan met `penv virtualenv`:

```bash
pyenv virtualenv rws
pyenv virtualenvs  # dit zal alle beschikbare virtuele omgevingen inclusief "rws" tonen
pyenv virtualenv-init rws
pyenv activate rws
```

Voeg in `~/.bashrc` toe

```bash
eval "$(pyenv virtualenv-init -)"
pyenv activate rws
```

### Installeer Ansible

Installeer Ansible, inclusief PIP-pakketten voor Kerberos en WinRM.

```bash
pip3 install -r requirements.txt  # ansible maakt dit bestand aan in de home directory van vagrant
```

Als alternatief kun je de volgende opdrachten uitvoeren:

```bash
pip3 install --upgrade pip
pip3 install ansible-core==2.11.12
pip3 install setuptools_rust
pip3 install yamllint==1.28.0 ansible-lint==6.8.6 pre-commit==2.20.0
pip3 install pywinrm==0.4.3
pip3 install requests-kerberos
pip3 install pywinrm[kerberos] requests-kerberos pykerberos
```

### Monteer Dev Collecties

Zodra je je Ansible-omgeving hebt ingesteld, kun je verdergaan met verschillende
taken zoals het klonen van het [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) project, het downloaden van Ansible
collecties, en het voorzien van nodes met Ansible.

Om ervoor te zorgen dat je omgeving functioneel is, is de makkelijkste manier echter om
gebruik te maken van de `/vagrant` mount. Deze mount bevat het
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) project van je host. Het enige
missende onderdeel is de `ansible-dev-collections` map.

Om deze map te incorporeren in `gsd-ansible`, kun je een speciaal lokaal en verborgen bestandsbestand genaamd `.sync_folders.yml` maken met de volgende inhoud. Dit bestand staat in de git ignore lijst:

```yaml
---
- src: ../ansible-dev-collections/
  target: /ansible-dev-collections
```

### Provisioneren met Ansible

Zodra je opnieuw opstart, zou je de `gsd` omgeving moeten kunnen provisioneren zoals volgt:

```bash
vagrant reload gsd-ansible  # maakt /ansible-dev-collections mount aan
vagrant ssh gsd-ansible
cd /vagrant
ansible-playbook plays/mw/reverse-proxy.yml -i hosts.ini
```

Houd er rekening mee dat de bovenstaande provision ervan uitgaat dat je de `gsd-agserver1`
al hebt draaien.

Met de Ansible Control Node, `gsd-ansible`, kun je nu de nodes beheren met
uitsluitend Ansible. Vagrant is niet langer betrokken bij het proces.

Zie [Vagrant Sync mappen]({{< relref path="/docs/guidelines/dev/vagrant-sync-folders" >}} "Richtlijn: Vagrant Sync
Mappen") voor meer informatie over het `.sync_folders.yml` bestand.

## Evaluatie

Het provisioning proces voor deze node wordt bepaald door het bestand in de map
`group_vars/ansible` bestand, dat gebruik maakt van de `bootstrap_packages`
lijst, evenals de `plays/mgmt/ansible.yml` play. Je kunt de bestanden vinden in het
Ansible project [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}).