---
categories: ["Handleiding"]
tags: [windows, firewall]
title: "Beheer Windows Firewall met Ansible"
linkTitle: "Windows Firewall Beheer"
weight: 2
description: >
    Effectief Windows Firewall beheer met Ansible met behulp van de
    `c2platform.wincore` collectie. Stel inkomende en uitgaande regels in, beheer
    geavanceerde instellingen, en stroomlijn firewall beheer taken.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

---

De [`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}) Ansible
collectie biedt krachtige tools voor het efficiënt beheren van de Windows Firewall. Deze
handleiding begeleidt je bij het gebruik van twee belangrijke lijsten binnen de collectie:
`win_firewalls` en `win_firewall_rules`. De referentie / voorbeeld implementatie
voor het GIS platform in het project [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) biedt een voorbeeld van hoe
deze lijsten worden gebruikt voor:

1. Inschakelen van de Windows Firewall op alle MS Windows nodes.
2. Openen van poort `6443` en `6006` op alle ArcGIS Server nodes.

## `gs_server` Ansible groep

Het bestand `group_vars/gs_server/firewall.yml` in [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) bevat de volgende code:

```bash
gs_win_firewall_rules:
  - name: ArcGIS server https poort voor Apache Tomcat
    localport: 6443
  - name: ArcGIS Server voor interne processen
    localport: 6006

gs_win_firewall_rules_defaults:
  action: allow
  direction: in
  protocol: tcp
  profiles:
    - private
    - public
    - domain
  state: present
  enabled: true

win_firewall_rules: "{{ gs_win_firewall_rules | c2platform.core.add_attributes(gs_win_firewall_rules_defaults) }}"
```

Dit voorbeeld introduceert twee [projectvariabelen]({{< relref path="/docs/guidelines/coding/var-prefix" >}}) `gs_win_firewall_rules` en
`gs_win_firewall_rules_defaults`. Deze variabelen bestaan alleen in het
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) project en hun doel is
om te voorkomen dat code-structuren gedupliceerd moeten worden.

Op deze manier kunnen we de firewall configureren voor servers in de Ansible groep
`gs_server` met een eenvoudige lijst `gs_win_firewall_rules` met slechts twee eigenschappen:
`name` en `port`.

Door gebruik te maken van de {{< external-link url="https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_filters.html" text="filter" htmlproofer_ignore="true" >}} `c2platform.core.add_attributes` worden
de standaardattributen gedefinieerd met `gs_win_firewall_rules_defaults` toegevoegd aan
deze lijst en gebruikt om `win_firewall_rules` te definiëren.

Voor jouw gemak, hier is een gelijkwaardige configuratie zonder gebruik van `gs_`
projectvariabelen (en de `c2platform.core.add_attributes` filter):

```bash
win_firewall_rules:
  - name: ArcGIS server https poort voor Apache Tomcat
    localport: 6443
    action: allow
    direction: in
    protocol: tcp
    profiles:
        - private
        - public
        - domain
  state: present
  enabled: true
  - name: ArcGIS Server voor interne processen
    localport: 6006
    action: allow
    direction: in
    protocol: tcp
    profiles:
        - private
        - public
        - domain
    state: present
    enabled: true
```

## `windows` Ansible groep

Het bestand `group_vars/windows/firewall.yml` in [`c2platform/rws/ansible-gis`]({{<
relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) bevat de
volgende code:

```yaml
win_firewalls:
  - state: enabled
    profiles:
      - private
      - public
      - domain
```

Deze configuratie zorgt ervoor dat op alle MS Windows nodes ( die in de `windows`
ansible groep zitten) zoals gedefinieerd in de `hosts.ini` in
het [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) project, de firewall wordt
ingeschakeld.