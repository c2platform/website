--- 
categories: ["Handleiding","Voorbeeld"]
tags: [ansible, certificaten, pki, windows, ssl/tls]
title: Beheer van SSL/TLS Vertrouwen met Ansible op Windows Hosts
linkTitle: Beheer SSL/TLS Vertrouwen
weight: 5
description: >
    Ontdek hoe u probleemloos SSL/TLS-vertrouwen kunt beheren op Windows-hosts met behulp van
    de `win_resources` variabele van de `c2platform.wincore.win` Ansible role.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

---
In de setup beschreven in
[Aanmaken van een eenvoudige software repository voor Ansible]({{< relref path="/docs/howto/rws/software" >}}),
leggen de download nodes `gsd-ansible-download1`, `gsd-ansible-download2`,
`gsd-ansible-download3` een SSL/TLS-vertrouwen vast met de software server
`gsd-ansible-repo`, waardoor foutloze software downloads worden gegarandeerd.

De `gsd-rproxy1` node binnen het ansible-gis project fungeert ook als een CA Server, zoals beschreven in
[Beheer van servercertificaten als certificaatautoriteit]({{< relref path="/docs/howto/c2/certs" >}}).
Hier gebruiken de download nodes het root CA-certificaat van deze Ansible-gebaseerde CA-server
om vertrouwen op te bouwen via de `c2platform.wincore.win`-rol.

Laten we bekijken hoe Ansible wordt gebruikt om dit resultaat te bereiken.

## Ansible Inventaris en Playbook

Het Ansible inventarisproject
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})
bevat belangrijke bestanden zoals volgt:

| Bestand                                 | Beschrijving                                     |
|----------------------------------------|-------------------------------------------------|
| `plays/mgmt/ansible_repo.yml`          | Bevat een playbook sectie voor download nodes.  |
| `group_vars/ansible_download/main.yml` | Configureert de download nodes.                 |

### Download Node Playbook

Het playbook `ansible_repo.yml` richt zich op download nodes binnen de
`ansible_download` groep. Het maakt gebruik van de `c2platform.wincore.win` rol voor
het tot stand brengen van vertrouwen en de `c2platform.wincore.download` rol voor veilige
binaire downloads van Tomcat, gebaseerd op deze vertrouwensrelatie. Met behulp van
`c2platform.wincore.download` verlopen downloads zonder SSL/TLS certificaatfouten.

```yaml
- naam: Tomcat op MS Windows
  hosts: ansible_download
  tags:
    - v2

  rollen:
    - { rol: c2platform.core.secrets, tags: ["common", "vault", "download"] }
    - { rol: c2platform.wincore.win, tags: ["windows"] }
    - { rol: c2platform.wincore.download, tags: ["download"] }
```
### Configuratie voor Downloads

Configuratie specificaties voor download nodes zijn te vinden in
`group_vars/ansible_download/main.yml`. Hierin wordt de `win_resources`
variabele uiteengezet, inclusief een item voor het beheren van de certificaatopslag via de
`c2platform.wincore.win` rol, die gebruikmaakt van de
`ansible.windows.win_certificate_store` module voor dergelijke operaties.

```yaml
win_resources:
  ansible_download:
    - naam: C2 Vertrouwensrelatie
      type: win_certificate_store
      path: C:\vagrant\.ca\c2\c2.crt
      store_name: Root
      store_location: LocalMachine
```

De downloads die moeten worden uitgevoerd door de `c2platform.wincore.download`
rol worden geconfigureerd met behulp van de `download_files` woordenlijst.