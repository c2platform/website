---
categories: ["Handleiding"]
tags: [windows]
title: "Beheer Windows-systemen"
linkTitle: "Windows"
weight: 15
description: >
  Beheer Microsoft Windows-systemen met behulp van de C2 Platform `win_core` verzameling.
---