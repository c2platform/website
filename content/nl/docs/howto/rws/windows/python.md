---
categories: ["Handleiding"]
tags: [windows, ansible, python, pip, chocolatey]
title: "Beheer Python en Python Bibliotheekafhankelijkheden"
linkTitle: "Python (Bibliotheken)"
weight: 4
provisionTime: 5 minuten
description: >
    Leer hoe je Python en Python bibliotheekafhankelijkheden op Windows hosts
    beheert met behulp van Ansible.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

---

## Overzicht

Deze handleiding maakt gebruik van Vagrant en Ansible om het volgende te automatiseren:

1. **Virtuele Machine Setup:** Vagrant, met de VirtualBox Provider, initialiseert een
   VM genaamd `gsd-python`.
2. **Python Installatie:** Via Ansible installeert Vagrant **Python 3.11.0** vanuit
   de Chocolatey repository en de `requests` bibliotheek met behulp van `pip`. Het is belangrijk
   op te merken dat het PIP installatiescript is geconfigureerd om een web proxy te gebruiken die beschikbaar is op
   {{< external-link url="http://webproxy.c2platform.org:1080" htmlproofer_ignore="false" >}},
   specifiek via de `gsd-rproxy1` node.


| Node         | OS                  | Provider   | Doelstelling                         |
|--------------|---------------------|------------|--------------------------------------|
| `gsd-python` | Windows 2022 Server | VirtualBox | Opzetten van een Python-omgeving op Windows |

## Voorwaarden

* Zorg dat je RWS-ontwikkelomgeving is opgezet op Ubuntu 22, zoals beschreven
  [hier]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}).
* Zorg dat de webproxy draait door `vagrant up gsd-rproxy1` uit te voeren, zoals beschreven
  [hier]({{< relref path="/docs/howto/rws/dev-environment/setup/rproxy1" >}}).

## Setup

Om de Windows node met Python en zijn bibliotheken op te zetten, voer je het volgende
commando uit, wat meestal binnen ongeveer 5 minuten voltooid is:

```bash
vagrant up gsd-python
```

## Verificatie

1. Een succesvolle uitvoering van het playbook bevestigt dat Python en het `requests`
   pakket zijn geïnstalleerd, zoals aangegeven door de laatste logvermeldingen van
   gsd-python, die de taken **Beheer Chocolatey pakketten** en **Beheer Python
   bibliotheekafhankelijkheden** laten zien.

   <p><details>
   <summary><kbd>Toon me</kbd></summary><p>

   ```bash
    TASK [c2platform.wincore.win : Beheer Chocolatey pakketten] *********************
    veranderd: [gsd-python] => (item=python aanwezig)

    TASK [c2platform.wincore.win : Beheer Python bibliotheekafhankelijkheden] *************
    veranderd: [gsd-python] => (item=requests 2.31.0 → aanwezig)
   ```

   </p></details></p>

2. Om de installatie van Python en het requests pakket verder te verifiëren,
   voer je de volgende commando's uit:
   ```bash
   vagrant ssh gsd-python
   python versie
   pip lijst
   ```
   Dit zou Python 3.11.0 en de geïnstalleerde pakketten, inclusief `requests`, moeten tonen.
   <p><details>
   <summary><kbd>Toon me</kbd></summary><p>

   ```bash
    Microsoft Windows [Version 10.0.20348.707]
    (c) Microsoft Corporation. Alle rechten voorbehouden.

    vagrant@GSD-PYTHON C:\Users\vagrant>python --version
    Python 3.11.0

    vagrant@GSD-PYTHON C:\Users\vagrant>pip list | grep requests
    'grep' wordt niet erkend als een interne of externe opdracht,
    uitvoerbaar programma of batchbestand.

    vagrant@GSD-PYTHON C:\Users\vagrant>pip lijst
    Pakket            Versie
    ------------------ --------
    certifi            2024.2.2
    charset-normalizer 3.3.2
    idna               3.6
    pip                22.3
    requests           2.31.0
    setuptools         65.5.0
    urllib3            2.2.1

    [mededeling] Een nieuwe release van pip beschikbaar: 22.3 -> 24.0
    [mededeling] Om te updaten, voer uit: python.exe -m pip install --upgrade pip

    vagrant@GSD-PYTHON C:\Users\vagrant>
   ```
   </p></details></p>

## Evaluatie

Voor inzicht in de VM-configuratie en -instelling, bekijk de Ansible Inventory,
Vagrant projectbestanden en Ansible collections.

### Ansible Inventory

Bekijk de volgende bestanden in de Ansible-inventory/Vagrant-project:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}):

| Bestand(en)                           | Beschrijving                                                |
|---------------------------------------|-------------------------------------------------------------|
| `Vagrantfile` en `Vagrantfile.yml`    | Definieert de VM-creatie en netwerkinstellingen voor `gsd-python`. |
| `plays/core/python.yml`               | Het hoofde Ansible-playbook.                                 |
| `group_vars/python/main.yml`          | Bevat de Python-configuratie-instellingen.                   |

De win_resources variabele in `group_vars/python/main.yml` schetst de
resources die nodig zijn voor de Python-installatie, inclusief Chocolatey pakketten en PIP
installaties:

```yaml
---
win_resources:
  python:
    - naam: python
      type: win_chocolatey
      versie: 3.11.0
    - naam: requests
      type: win_pip
      versie: 2.31.0
      status: aanwezig
      omgeving:
        Path: 'C:\Python311\;C:\Python311\Scripts;{{ ansible_env.Path }}'
```

De omgevingsvariabele zorgt ervoor dat de `Path` variabele wordt bijgewerkt met de
noodzakelijke Python-paden, die mogelijk niet direct beschikbaar zijn voor Ansible.


### Ansible Collections / Rollen

| Collectie             | Beschrijving                                                                                             |
|-----------------------|----------------------------------------------------------------------------------------------------------|
| `c2platform.wincore`  | Bevat de `c2platform.wincore.win` rol voor gestroomlijnde installaties van Python en zijn bibliotheken. |


## Het stroomlijnen van ontwikkeling

De `gsd-python` node omvat verschillende taken omdat deze onderdeel is van de `windows`
groep. Voor een meer gerichte opzet, specifiek gericht op Chocolatey en PIP
installaties, kun je gebruik maken van Ansible-tags als volgt:

```bash
TAGS=win_chocolatey,win_pip vagrant provision gsd-python
```

Deze aanpak stelt je in staat om het provisioningproces voor
specifieke taken te identificeren en te versnellen.