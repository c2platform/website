---
categories: ["Voorbeeld"]
tags: [windows, ansible, download]
title: "Voorbeeld voor Beheer van Downloads op MS Windows Hosts"
linkTitle: "Voorbeeld voor Beheer van Downloads"
draft: true
weight: 4
description: >
    Leer hoe u efficiënt downloads kunt beheren met de
    `c2platform.wincore.download` Ansible rol. Dit voorbeeld demonstreert hoe de
    ArcGIS software kan worden gedownload met behulp van deze rol.
---

{{< under_construction >}}

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

In het RWS Ansible Inventory project
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
wordt de `c2platform.gis.arcgis_portal` rol gebruikt voor de installatie van ArcGIS Portal.

Deze rol maakt gebruik van de `c2platform.win.download` rol om efficiënt een zipbestand te downloaden en te extraheren dat ArcGIS Portal bevat.

Bij het doornemen van de Ansible rol `roles/arcgis_portal` in de
[`c2platform.gis`](<{{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}>)
Ansible collectie, let op het volgende:

1. `defaults/main.yml`: Let op de `arcgis_portal_versions`
   woordenlijst, die de versies definieert die door de rol kunnen worden geïnstalleerd.
2. De `arcgis_portal_versions` woordenlijst wordt ook gebruikt door de downloadrol. In het bestand `tasks/download.yml`, kunt u de opname van de
   `c2platform.wincore.download` rol en het doorgeven van de
   `arcgis_portal_versions` woordenlijst als een variabele observeren. Bovendien wordt de
   `arcgis_portal_temp_dir` doorgegeven aan de downloadrol.
3. In `tasks/main.yml`, wordt de rol opnieuw opgenomen, dit keer om "op te ruimen"
   gedownloade bestanden en/of aangemaakte downloadmappen.

Door gebruik te maken van de `c2platform.wincore.download` rol, kunt u efficiënt het beheer van downloads delegeren, specificeren wat te downloaden, waar te downloaden, of een proxy te gebruiken en hoe bestanden te extraheren. Dit elimineert de noodzaak om dit type code in elke productrol te dupliceren.

Voor een beter begrip van de functionaliteit van de downloadrol,
raadpleeg de `download` rol in de
[`c2platform.wincore`](<{{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}>)
Ansible collectie.

Voor een voorbeeld van hoe de downloadrol te integreren, zie de Ansible
rol `roles/arcgis_portal` in de
[`c2platform.gis`](<{{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}>)
Ansible collectie.

TODO