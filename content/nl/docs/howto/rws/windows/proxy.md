---
categories: ["Voorbeeld"]
tags: [windows, ansible, proxy, filter, jinja, cartesiaans]
title: "Configuratie van een webproxyserver Voorbeeld voor MS Windows Hosts"
linkTitle: "Voorbeeld Webproxyserver"
weight: 3
description: >
    Leer hoe je een webproxyserver kunt configureren en voorzien met de 
    `c2platform.wincore.win` Ansible rol. Dit voorbeeld demonstreert het 
    installatieproces voor Windows-hosts.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

---

Het RWS Ansible Inventory-project
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})
bevat de benodigde configuratie voor het opzetten van de webproxyserver, die gehost wordt op
{{< external-link url="http://webproxy.c2platform.org:1080" htmlproofer_ignore="false" >}}.
Dit dient als een illustratief voorbeeld voor het configureren van een webproxy met de
`c2platform.wincore.win` rol.

## Projectvariabele `gs_proxy_regedit`

Verwijs naar het `group_vars/windows/proxy.yml`-bestand waar een "projectvariabele"
`gs_proxy_regedit` is gedefinieerd. Voor meer informatie over het definiëren van projectvariabelen, zie onze
[richtlijn]({{< relref path="/docs/guidelines/coding/var-prefix" >}}) hierover.

Deze variabele is gedefinieerd als een cartesiaans product van twee andere projectvariabelen:
`gs_proxy_regedit_paths` en `gs_proxy_regedit_properties`, bereikt door de
`c2platform.core.product` filter, een integraal onderdeel van de `c2platform.core`
collectie. Deze aanpak stroomlijnt de configuratie en voorkomt redundantie.

```yaml
---
gs_proxy_regedit_paths:
  - path: HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
  - path: HKLM:\Software\Microsoft\Windows\CurrentVersion\Internet Settings

gs_proxy_regedit_properties:
  - name: ProxyServer
    data: http://webproxy.c2platform.org:1080
    type: string
  - name: ProxyOverride
    data: "*.c2platform.org;localhost;127.0.0.1;{{ ansible_hostname }}"
    type: string
  - name: ProxyEnable
    data: 1  # 1 inschakelen, 0 uitschakelen
    type: dword

gs_proxy_regedit: >-
  {{ gs_proxy_regedit_paths
  | c2platform.core.product(gs_proxy_regedit_properties) }}
```

Natuurlijk kun je ervoor kiezen om het gebruik van de `gs_proxy_regedit_paths` en
`gs_proxy_regedit_properties` variabelen te omzeilen en het cartesisch product
handmatig te specificeren.

<details>
  <summary><kbd>Laat me zien</kbd></summary>

```yaml
---
gs_proxy_regedit:
  - name: ProxyServer
    data: http://webproxy.c2platform.org:1080
    type: string
    path: HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
  - name: ProxyOverride
    data: "*.c2platform.org;localhost;127.0.0.1;{{ ansible_hostname }}"
    type: string
    path: HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
  - name: ProxyEnable
    data: 1  # 1 inschakelen, 0 uitschakelen
    type: dword
    path: HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
  - name: ProxyServer
    data: http://webproxy.c2platform.org:1080
    type: string
    path: HKLM:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
  - name: ProxyOverride
    data: "*.c2platform.org;localhost;127.0.0.1;{{ ansible_hostname }}"
    type: string
    path: HKLM:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
  - name: ProxyEnable
    data: 1  # 1 inschakelen, 0 uitschakelen
    type: dword
    path: HKLM:\Software\Microsoft\Windows\CurrentVersion\Internet Settings
```

</details>

## Configuratie `win_resources`

In het bestand `group_vars/windows/main.yml` is een variabele `win_resources` gedefinieerd
die een groep/sleutel `1-windows` bevat. Deze sleutel is een lijst die verschillende
items omvat, waarvan er één **Proxyserver configureren** heet.

```yaml
win_resources:
  1-windows:
    - name: Proxyserver configureren
      type: win_regedit
      resources: "{{ gs_proxy_regedit }}"
```

De `win_resources` variabele speelt een cruciale rol in de
`c2platform.wincore.win` rol. Voor gedetailleerde informatie, raadpleeg de
README van de rol.

## Beleidscodificatie

Met deze configuratie wordt het beleid om de webproxy te configureren effectief
gecodificeerd. Er zijn geen specifieke handmatige handelingen vereist om de
webproxy op een server in te stellen. Dankzij Ansible wordt dit proces naadloos
geautomatiseerd voor elke server binnen de `windows` groep.