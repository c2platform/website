---
categories: ["Voorbeeld"]
tags: [windows, nuget, win_powershell, chocolatey, ansible]
title: "NuGet en Andere Pakketleveranciers op Windows Installeren met Ansible"
linkTitle: "Nuget Pakketleverancier"
weight: 1
description: >
    Leer hoe u de NuGet Pakketleverancier kunt installeren met Ansible voor
    Windows-systemen. Dit is essentieel voor Ansible-projecten die zich richten
    op Microsoft Windows-hosts, zoals die in het RWS GIS Platform.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

---

## Inleiding

Bij het werken met Ansible-projecten die gericht zijn op Windows-hosts, moet u
vaak de **NuGet Pakketleverancier** installeren. Zonder deze kan het volgende
bericht verschijnen:

> PowerShellGet vereist NuGet-provider versie '2.8.5.201' of nieuwer om te
> communiceren met NuGet-gebaseerde repositories

Ansible biedt momenteel geen speciale modules voor deze taak, maar u kunt de
NuGet Pakketleverancier handmatig installeren met het volgende PowerShell
commando:

```powershell
Find-PackageProvider -name Nuget -ForceBootstrap -IncludeDependencies -Force
```

Als alternatief kunt u de `win_powershell` Ansible-module gebruiken om deze taak
uit te voeren.

## Gebruik van de `c2platform.wincore.win` Rol

Een nog handigere aanpak is het gebruik van de
[`c2platform.wincore.win`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})
Ansible rol. Deze rol biedt een sjabloon `install_package_provider.ps.j2` voor
het gecontroleerd installeren van pakketleveranciers zoals NuGet en Chocolatey.

Het voordeel van het gebruik van de `c2platform.wincore.win` rol is dat het
weinig Ansible-configuratie vereist; u hoeft het alleen maar te configureren.
Het RWS-referentie-implementatieproject,
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})
geeft een voorbeeld van hoe u deze rol configureert in het bestand
`group_vars/windows/main.yml`. Hier is een fragment van dat bestand:

```yaml
win_resources:
  1-windows:
    - name: Chocolatey Package Provider
      type: win_powershell
      script: "{{ lookup('ansible.builtin.template', 'install_package_provider.ps.j2', template_vars={'win_package_provider': 'Chocolatey'}) }}"
      # debug_path: C:\vagrant\logs\chocolately-package-provider.yml
    - name: NuGet Package Provider
      type: win_powershell
      script: "{{ lookup('ansible.builtin.template', 'install_package_provider.ps.j2', template_vars={'win_package_provider': 'Nuget'}) }}"
```

## Sjabloon voor Installeren van Pakketleveranciers

Het `install_package_provider.ps.j2` sjabloon, onderdeel van de
[`c2platform.wincore.win`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})
Ansible rol, is hieronder voor uw gemak opgenomen:

```powershell
$Ansible.Failed = $false
$Ansible.Changed = $false
{% if win_proxy_url is defined %}
$proxy = '{{ win_proxy_url }}'
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
[system.net.webrequest]::defaultwebproxy = new-object system.net.webproxy($proxy)
[system.net.webrequest]::defaultwebproxy.BypassProxyOnLocal = $true
{% endif %}

Write-Output "Install {{ win_package_provider }} Package Provider (if not installed already)"

try {
    $packageProvider = Get-PackageProvider -Name {{ win_package_provider }}

    if (-not $packageProvider) {
        Write-Output "Installing {{ win_package_provider }} Package Provider"
        $result = Install-PackageProvider -Name {{ win_package_provider }} -Force -Confirm:$false

        if ($result -eq $null) {
            $Ansible.Failed = $true
            Write-Error "Failed to install {{ win_package_provider }} Package Provider"
        } else {
            $Ansible.Changed = $true
        }
    }

    if ($Ansible.Changed) {
        Write-Output "{{ win_package_provider }} Package Provider has been installed or updated."
    } elseif ($Ansible.Failed) {
        Write-Output "{{ win_package_provider }} Package Provider failed to install."
    }
    else {
        Write-Output "{{ win_package_provider }} Package Provider is installed, and the installation was not changed"
    }
}
catch {
    $Ansible.Failed = $true
    Write-Error "An error occurred: $_"
}
```