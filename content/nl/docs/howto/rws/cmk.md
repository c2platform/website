---
categories: ["Handleiding"]
tags: [ansible, monitoring, checkmk]
title: CheckMK Monitoring Instellen met Ansible
linktitle: CheckMK
provisionTime: 5 minuten
weight: 12
description: >
    Deze handleiding begeleidt je bij het instellen van CheckMK monitoring voor
    het RWS GIS Platform met behulp van Ansible, wat zorgt voor een efficiënte
    monitoring configuratie.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

{{< under_construction >}}

## Overzicht

Deze tutorial legt uit hoe je Vagrant en Ansible kunt gebruiken met de `checkmk.general` Ansible collectie om een CheckMK server te implementeren op een LXD node met Ubuntu 22.04.1.

| Node                 | OS                 | Provider | Doel           |
|----------------------|--------------------|----------|----------------|
| `gsd-checkmk-server` | Ubuntu 22.04.1 LTS | LXD      | CheckMK Server |

## Vereisten

* Zorg ervoor dat je RWS Ontwikkelomgeving is ingesteld op Ubuntu 22, zoals gedetailleerd
  [hier]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}).
* Zorg ervoor dat de proxy node `gsd-rproxy1` actief is voor internettoegang van Windows node:
  ```bash
  vagrant up gsd-rproxy1
  ```

## Installatie

Voer vagrant up uit om de CheckMK server te creëren. Deze installatie zou ongeveer 5 minuten moeten duren.

1. Start de creatie van de CheckMK server met Vagrant. Het proces duurt ongeveer 5 minuten.
    ```bash
    vagrant up gsd-checkmk-server
    ```
2. Na de implementatie, wijzig het IP adres voor de `gs` site van de standaard
   `127.0.0.1` naar `1.1.5.208`:

    ```bash
    vagrant ssh gsd-checkmk-server
    omd config gs show APACHE_TCP_ADDR
    omd stop gs
    omd config gs set APACHE_TCP_ADDR 1.1.5.208
    omd update-apache-config gs
    omd start gs
    omd status gs
    ```

## Verificatie

Om de installatie te verifiëren:

1. Open het [RWS FireFox profiel]({{< relref path="/docs/howto/rws/dev-environment/setup/firefox" >}}) en navigeer naar
   {{< external-link url="http://1.1.5.208:5000/gs" htmlproofer_ignore="true" >}}.
   Log in als `cmkadmin` met het wachtwoord `secret`.
2. Navigeer naar {{< external-link url="https://checkmk.c2platform.org/gs/" htmlproofer_ignore="true" >}} en log opnieuw in als `cmkadmin` met het
   wachtwoord `secret`.

## Aanvullende Informatie

Voor extra inzichten en begeleiding:

* {{< external-link url="https://docs.checkmk.com/latest/en/intro_setup.html" text="Instellen van Checkmk" htmlproofer_ignore="false" >}}
* {{< external-link url="https://galaxy.ansible.com/ui/repo/published/checkmk/general/" text="Checkmk Ansible Collectie" htmlproofer_ignore="false" >}}