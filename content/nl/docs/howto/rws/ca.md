---
categories: ["Handleiding"]
tags: [ansible, certificaten, pki, java, keystore, tomcat, selinux, rhel, cifs]
title: Creëer een Simpele CA Server met Ansible
linktitle: CA Server
provisionTime: 25 minuten
weight: 3
description: >
  Leer hoe je met Ansible een eenvoudige Certificate Authority (CA) server kunt opzetten,
  ter ondersteuning van het beheer van certificaten en Java Keystore/Truststore.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),

---

## Overzicht

Dit document beschrijft de RWS-aanpak voor het beheren van certificaten en Java
Keystores/Truststores via Ansible, gebruikmakend van de rol `c2platform.core.cacerts2`.
Deze opzet, met Vagrant en Ansible, bereikt het volgende:

1. Vagrant stelt drie VM's in via VirtualBox, elk voorbereid voor zijn rol: een
   bestandsdeling, een CA-server en een Tomcat-server.
2. Ansible automatiseert de configuratie van deze VM's, het opzetten van een
   bestandsdeling, het initialiseren van de CA-server en het implementeren van een Tomcat-server geconfigureerd met door de CA uitgegeven certificaten.

| Node                      | OS                  | Provider   | Doel                                        |
|---------------------------|---------------------|------------|---------------------------------------------|
| `gsd-ansible-file-share1` | Windows 2022 Server | VirtualBox | Bestandsdeling voor certificaten            |
| `gsd-ca-server`           | Red Hat 9           | VirtualBox | Eenvoudige CA-server op basis van Ansible   |
| `gsd-ca-server-client`    | Windows 2022 Server | VirtualBox | Tomcat-server met HTTPS / Java Keystores    |

## Vereisten

* Zorg dat je RWS-ontwikkelomgeving is opgezet op Ubuntu 22, zoals beschreven
  [hier]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}).
* Zorg dat de proxy-node `gsd-rproxy1` actief is voor internettoegang van Windows-nodes:
  ```bash
  vagrant up gsd-rproxy1
  ```

## Installatie

Om de volledige omgeving voor te bereiden, voer `vagrant up` uit voor de opgegeven VM's. Op een
[hoogwaardige ontwikkelwerkstation]({{< relref path="/docs/concepts/dev/laptop" >}}),
zou deze opzet ongeveer 25 minuten moeten duren.

```bash
vagrant up gsd-ansible-file-share1 gsd-ca-server gsd-ca-server-client
```

## Verificatie

1. SSH naar de CA-server en inspecteer de certificaatdirectory:

   ```bash
   vagrant ssh gsd-ca-server
   ```
   ```bash
   tree /mnt/ansible-certificates/
   ```
   Verwacht de root-certificaat van de CA (`c2.crt`), sleutel (`c2.key`), en
   het certificaat en Java KeyStore van de Tomcat-server te zien.
    <p><details>
    <summary><kbd>Laat zien</kbd></summary><p>

    ```bash
    [vagrant@gsd-ca-server ~]$ tree /mnt/ansible-certificates/
    /mnt/ansible-certificates/
    └── c2
        ├── c2.crt
        ├── c2.csr
        ├── c2.key
        ├── downloads
        │   └── c2-ca.crt
        └── tomcat
            ├── gsd-ca-server-client-gsd-ca-server-client.crt
            ├── gsd-ca-server-client-gsd-ca-server-client.csr
            ├── gsd-ca-server-client-gsd-ca-server-client.key
            ├── gsd-ca-server-client-gsd-ca-server-client.keystore
            ├── gsd-ca-server-client-gsd-ca-server-client.p12
            ├── gsd-ca-server-client-gsd-ca-server-client.pem
            └── gsd-ca-server-client-gsd-ca-server-client.truststore

    3 directories, 11 bestanden
    [vagrant@gsd-ca-server ~]$

    ```
    </p></details></p>
2. Toegang tot `gsd-ca-server-client` via **Remmina** of **VirtualBox Manager** als
   de gebruiker `vagrant` (wachtwoord: `vagrant`).

   Op het bureaublad is er een snelkoppeling naar
   {{< external-link url="https://gsd-ca-server-client:8443/helloworld/" htmlproofer_ignore="true" >}}

   Dit toont een waarschuwing over een mogelijke beveiligingsrisico omdat het
   certificaat niet door FireFox wordt vertrouwd. Op dit punt kan je het root
   certificaat `\\gsd-ansible-file-share1\ansible-certs\c2\c2.crt` importeren om
   een vertrouwensrelatie op te bouwen en deze waarschuwing wordt niet getoond.
3. Controleer of `server.xml` in `D:\Apps\tomcat\conf` de juiste SSL/TLS configuratie
   bevat, wat het succes van de installatie van de Java Keystore door Ansible
   aangeeft.

    ```xml
    <Connector port="8443" URIEncoding="ISO-8859-1" protocol="HTTP/1.1" SSLEnabled="true" maxThreads="150" scheme="https"
        secure="True" clientAuth="False" sslProtocol="TLSv1.2" SSLCipherSuite="ECDHE-RSA-AES128-GCM-SHA256"
        keystoreFile="conf/gsd-ca-server-client-gsd-ca-server-client.keystore"
        keystorePass="secret"
        keyAlias="gsd-ca-server-client"
        SSLHonorCipherOrder="true"
        server="server"  />
    ```

    De Java Keystore `gsd-ca-server-client-gsd-ca-server-client.keystore` werd
    op de CA-server door Ansible aangemaakt en vervolgens geïnstalleerd door Ansible
    met de rol `c2platform.cacerts2`.

## Certificaatbeheer op CIFS Share

{{< alert type="waarschuwing" title="Waarschuwing!" >}}
De rol `c2platform.core.cacerts2` ondersteunt geen directe certificaatupdates
op CIFS-shares vanwege beperkingen van Ansible met bestandsbeheer op zulke shares.
{{< /alert >}}

Om dit probleem te reproduceren:

1. Navigeer naar `group_vars/cacerts_server_client/certs.yml`, wijzig een
   certificaat eigenschap (bijv. `subject_alt_name`).
2. Provision `gsd-ca-server-client` opnieuw:
    ```bash
    vagrant provision gsd-ca-server-client
    ```

Dit zal mislukken met de onderstaande boodschap, vanwege de incompatibiliteit van Ansible
met bestandsattribuutbeheer op CIFS-shares.

> Fout bij het instellen van attributen: Gebruik: /bin/chattr [-pRVf] [-+=aAcCdDeijPsStTuFx]

<p><details>
<summary><kbd>Laat zien</kbd></summary><p>

```
TASK [c2platform.core.cacerts2 : Genereer een OpenSSL-certificaat] **************
Een uitzondering vond plaats tijdens de uitvoering van de taak. Om de volledige traceback te zien, gebruik -vvv. De fout was: Exception: Fout bij het instellen van attributen: Gebruik: /bin/chattr [-pRVf] [-+=aAcCdDeijPsStTuFx] [-v versie] bestanden...
fatal: [rws-ipvw-fme201 → rws-iavl-gjb201(rws-iavl-gjb201.workload.rws.local)]: MISLUKT! => {"changed": false, "details": "Fout bij het instellen van attributen: Gebruik: /bin/chattr [-pRVf] [-+=aAcCdDeijPsStTuFx] [-v versie] bestanden...\\n", "gid": 0, "group": "root", "mode": "0755", "msg": "chattr mislukt", "owner": "root", "path": "/mnt/tbgeo/Certificates/gs/tomcat/rws-ipvw-fme201-rws-ipvw-fme201.crt.1781900.2024-03-19@13:50:05~", "secontext": "system_u:object_r:cifs_t:s0", "size": 2037, "state": "file", "uid": 0}
```
</p></details></p>

Dit wordt veroorzaakt door het gebrek aan MS Windows-ondersteuning in Ansible kernhulpprogrammamodules. Specifiek ondersteunen deze modules geen bestandsbeheer op CIFS-shares. In dit geval, omdat we certificaten updaten, probeert Ansible de methode `backup_local` in
{{< external-link url="https://github.com/ansible/ansible/blob/devel/lib/ansible/module_utils/basic.py" text="lib/ansible/module_utils/basic.py" htmlproofer_ignore="false" >}}
te gebruiken om een kopie van het certificaat te maken voordat het wordt overschreven. Dit mislukt, het wordt niet ondersteund.

Om dit op te lossen en het certificaat en de keystore bij te werken:

1. Toegang tot `gsd-ca-server-client` via **Remmina** of **VirtualBox Manager** als de gebruiker `vagrant` (wachtwoord: `vagrant`).
2. Open Windows Verkenner en ga naar
   `\\gsd-ansible-file-share1\ansible-certs\c2\tomcat`
3. Verwijder alle bestanden in die map behalve de privésleutel.
4. Provision `gsd-ca-server-client` opnieuw:
    ```bash
    vagrant provision gsd-ca-server-client
    ```

Let op dat het nieuwe certificaat en de Java Keystore worden aangemaakt, geïmplementeerd en dat Tomcat wordt herstart om de wijziging door te voeren.

## Integreren van Externe Certificaten

De rol `c2platform.core.cacerts2` ondersteunt het vervangen van certificaten door officiële certificaten uitgegeven door een toegewezen eenheid, zoals het geval is voor RWS, zoals beschreven in
[Implementatie van PKI voor RWS GIS met Ansible]({{< relref path="/docs/projects/rws/pki" >}})
Dit kunnen we simuleren met de volgende stappen:

1. Toegang tot `gsd-ca-server-client` via **Remmina** of **VirtualBox Manager** als de gebruiker `vagrant` (wachtwoord: `vagrant`).
2. Open Windows Verkenner en ga naar
   `\\gsd-ansible-file-share1\ansible-certs\c2\tomcat`.
3. Kopieer `gsd-ca-server-client-gsd-ca-server-client.crt` en
   `gsd-ca-server-client-gsd-ca-server-client.csr` naar het bureaublad. Deze CSR en certificaat simuleren respectievelijk de CSR die we naar de eenheid sturen en het certificaat dat we als antwoord ontvangen. Hernoem het certificaat naar
   `gsd-ca-server-client-gsd-ca-server-client.rws-csp.crt`. Dit is nu voor de test het officiële RWS-certificaat.
4. Verwijder de bestanden met de extensies `crt`, `keystore`, `p12`, `pem`, `truststore`.
5. Provision `gsd-ca-server-client` opnieuw:
    ```bash
    vagrant provision gsd-ca-server-client
    ```
   We hebben nu een nieuw `crt`-bestand dat anders is dan
   `gsd-ca-server-client-gsd-ca-server-client.rws-csp.crt`.

Dus we hebben nu het punt bereikt waar we de Ansible-rol
`c2platform.core.cacerts2` gaan gebruiken om onze tijdelijke certificaten te vervangen door RWS-certificaten.
1. Ga naar de map `\\gsd-ansible-file-share1\ansible-certs\c2\tomcat`
2. Verwijder de bestanden met de extensies `keystore`, `p12`, `pem`.
3. Kopieer het certificaat `gsd-ca-server-client-gsd-ca-server-client.rws-csp.crt`
   van het bureaublad naar de map
   `\\gsd-ansible-file-share1\ansible-certs\c2\tomcat`
4. Provision `gsd-ca-server-client` opnieuw:
    ```bash
    vagrant provision gsd-ca-server-client
    ```
5. Gebruik de snelkoppeling op het bureaublad voor KeyStore Explorer om te controleren aan de hand van de thumbprint dat het certificaat in de KeyStore is
   `gsd-ca-server-client-gsd-ca-server-client.rws-csp.crt`.

## Overzicht

Om de opzet en configuratie van de VM's te begrijpen, bekijk de Ansible Inventory en 
Vagrant-projectbestanden en de Ansible-collecties.

### Ansible Inventory

Bekijk de volgende bestanden in het Ansible-inventory / Vagrant-project
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}):

| Bestand(en)                           | Beschrijving                                                    |
|---------------------------------------|-----------------------------------------------------------------|
| `plays/mgmt/cacerts_server_rhel.yml`  | Het voornaamste Ansible playbook voor het provisionen van de drie VM's |
| `group_vars/ansible_file_share/*`     | Configuratie voor de fileshare-node  `gsd-ansible-file-share1`  |
| `group_vars/cacerts_server_client/*`  | Configuratie voor de Tomcat-server  `gsd-ca-server-client`      |

Zie
[Ansible Inventory Project]({{< relref path="/docs/concepts/ansible/projects/inventory" >}})
voor meer informatie over Ansible inventaris projecten.

### Ansible Collecties / Rollen

| Collectie             | Beschrijving                                                         |
|-----------------------|----------------------------------------------------------------------|
| `c2platform.wincore`  | Bevat essentiële rollen voor Windows-hosts, zoals `win`              |
| `c2platform.core`     | Biedt rollen voor Linux-doelen `linux` en `cacerts2`.                |
| `c2platform.gis`      | Collectie met rollen voor GIS-platform zoals `tomcat` en `java`.     |

Zie
[Ansible Collection Project]({{< relref path="/docs/concepts/ansible/projects/collections" >}})
voor meer informatie over Ansible collectie projecten.

## Aanvullende Informatie

Voor extra inzichten en richtlijnen:

* Verken het ontwerp en de voordelen van deze opzet in
  [Implementatie van PKI voor RWS GIS met Ansible]({{< relref path="/docs/projects/rws/pki" >}}).
* Leer over de mogelijkheden van Vagrant in het omgaan met Sysprep voor Windows en
  geautomatiseerde registratie voor RHEL 9 in de
  [Vagrant installatierichtlijn]({{< relref path="/docs/howto/dev-environment/setup/vagrant" >}}).
* [Tomcat SSL/TLS en Java Keystore en TrustStore Configuratie voor Linux en
  Windows Hosts]({{< relref path="/docs/howto/rws/certs" >}}) is vergelijkbaar met deze
  handleiding maar maakt geen gebruik van een CIFS-share en richt zich op certificaatbeheer op
  Linux- en Windows-hosts. Deze instructie behandelt alleen een Windows-target.