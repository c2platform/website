---
categories: ["Handleiding"]
tags: [ansible, monitoring, splunk]
title: Splunk Enterprise instellen in RWS Ontwikkelomgeving
linkTitle: Splunk Installatie
provisionTime: 2 minuten
weight: 13
description: >
  Deze handleiding helpt het GIS Platform team bij de integratie en het
  experimenteren met Splunk Enterprise monitoring binnen de RWS
  ontwikkelomgeving.
---

Projecten:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.mgmt`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mgmt" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

## Overzicht

Deze handleiding leidt je door het uitrollen van een Splunk Enterprise server
met behulp van Vagrant en Ansible, waarbij specifiek de Ansible rol
`c2platform.mgmt.splunk` wordt gebruikt, op een LXD-node die Ubuntu 22.04.1
draaide.

| Node         | Besturingssysteem  | Provider | Doel                             |
|--------------|--------------------|----------|----------------------------------|
| `gsd-splunk` | Ubuntu 22.04.1 LTS | LXD      | Splunk Enterprise (standalone)   |

## Randvoorwaarden

* Zorg ervoor dat je RWS Ontwikkelomgeving is ingericht op Ubuntu 22, zoals
  beschreven [hier]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}).
* Zorg ervoor dat de reverse/web proxy node `gsd-rproxy1` actief is:

  ```bash
  vagrant up gsd-rproxy1
  ```

  Raadpleeg
  {{< rellink path="/docs/howto/rws/dev-environment/setup/rproxy1" >}}
  voor meer details. Deze node fungeert als de web- en reverse proxy. Het is
  nodig voor internettoegang om de Splunk software te downloaden en pakketten te
  installeren. Als reverse proxy stelt het je in staat om de Splunk webinterface
  te benaderen via TLS/HTTPS zonder certificaatfouten.

## Uitrol Splunk Enterprise

Start het installatieproces van de Splunk Enterprise-server met Vagrant. Dit
proces zou ongeveer 1,5 minuut moeten duren.

```bash
vagrant up gsd-splunk
```

## Inloggen op Splunk Enterprise

Om de installatie te bevestigen:

Open het
[RWS Firefox profiel]({{< relref path="/docs/howto/rws/dev-environment/setup/firefox" >}})
en ga naar
{{< external-link url="https://splunk.c2platform.org" htmlproofer_ignore="true" >}}.
Log in als `admin` met het wachtwoord `Supersecret!`. Je zou succesvol moeten
inloggen en de Splunk Enterprise-interface moeten zien, zoals hieronder
afgebeeld:

{{< image filename="/images/docs/splunk.png?width=600px" >}}

## Gegevens Ontvangen

Om te testen of gegevens naar je nieuwe Splunk Enterprise-node kunnen worden
gestuurd, voorzie de Universal Forwarder op de Reverse/Web Proxy node
`gsd-rproxy1`.

```bash
export PLAY="plays/mgmt/splunk_uf.yml"
vagrant provision gsd-rproxy1
```

Gebruik Splunk Web om Splunk te configureren voor het ontvangen van gegevens op
poort `9997`. Ga naar **Instellingen** → **Forwarding and receiving** →
**Receive data** → **Add new** en configureer poort `9997`.

{{< image filename="/images/docs/splunk-9997.png?width=600px" >}}

Log in op de reverse proxy `gsd-rproxy1` en stel de Universal Forwarder in om
`/var/log` te monitoren.

```bash
vagrant ssh gsd-rproxy1
```

```bash
sudo su -
```

```bash
/opt/splunkforwarder/bin/splunk add forward-server gsd-splunk:9997
/opt/splunkforwarder/bin/splunk add monitor /var/log
/opt/splunkforwarder/bin/splunk restart
```

<p><details>
  <summary><kbd>Toon mij</kbd></summary><p>

```bash
vagrant@gsd-rproxy1:~$ sudo su -
root@gsd-rproxy1:~# /opt/splunkforwarder/bin/splunk add forward-server gsd-splunk:9997
Warning: Attempting to revert the SPLUNK_HOME ownership
Warning: Executing "chown -R root:root /opt/splunkforwarder"
Added forwarding to: gsd-splunk:9997.
root@gsd-rproxy1:~# /opt/splunkforwarder/bin/splunk add monitor /var/log
Warning: Attempting to revert the SPLUNK_HOME ownership
Warning: Executing "chown -R root:root /opt/splunkforwarder"
Added monitor of '/var/log'.
root@gsd-rproxy1:~# /opt/splunkforwarder/bin/splunk restart
Warning: Attempting to revert the SPLUNK_HOME ownership
Warning: Executing "chown -R root:root /opt/splunkforwarder"
Stopping splunkd...
Shutting down.  Please wait, as this may take a few minutes.

Stopping splunk helpers...

Done.
splunkd.pid doesn't exist...

Splunk> Be an IT superhero. Go home early.

Checking prerequisites...
        Checking mgmt port [8089]: open
        Checking conf files for problems...
        Done
        Checking default conf files for edits...
        Validating installed files against hashes from '/opt/splunkforwarder/splunkforwarder-9.3.2-d8bb32809498-linux-2.6-x86_64-manifest'
        All installed files intact.
        Done
All preliminary checks passed.

Starting splunk server daemon (splunkd)...
Done
```

</p></details></p>

In Splunk Web, navigeer naar **Search & Reporting**, en klik op **Data Summary**.
Je zou onder hosts moeten zien dat Splunk gegevens ontvangt van `gsd-rproxy1`.

{{< image filename="/images/docs/splunk-data.png?width=600px" >}}

## Review Ansible Play

De Ansible Play faciliteert het opzetten van een Splunk Enterprise-node met
minimale commando's door integratie van wijzigingen in het Ansible inventory
project, wat ook een {{< rellink path="/docs/concepts/dev/vagrant" >}} project
is. Deze integratie vereenvoudigt de toevoeging van diensten zoals Splunk
Enterprise in de RWS ontwikkelomgeving. Voor gedetailleerde informatie, kun je
de commit [34282de8](https://gitlab.com/c2platform/rws/ansible-gis/-/commit/34282de8e2471fee36b571c841de6511c916018f)
in het Ansible inventory project
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})
bekijken. De commit bevatte de volgende wijzigingen:

| Component         | Bestandsnaam                         | Opmerking                                                                                                                         |
|-------------------|--------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|
| Vagrant           | `Vagrantfile.yml`                    | Toegevoegd `splunk.c2platform.org` als een "alias" voor `gsd-rproxy1`.                                                                 |
| Web/Forward Proxy | `group_vars/reverse_proxy/files.yml` | Poorten `8000` en `8089` toegevoegd aan `proxy.conf`.                                                                                |
| Reverse Proxy     | `group_vars/reverse_proxy/files.yml` | Nieuwe `splunk.conf` Apache configuratie gemaakt voor `splunk.c2platform.org`.                                                     |
| Splunk            | `hosts.ini`                          | Tien nieuwe groepen geïntroduceerd voor Splunk, bijv. `splunk`, `splunk_clustermanager`.                                           |
| Splunk            | `group_vars/splunk*/main.yml`        | Geconfigureerd voor die 10 nieuwe Splunk groepen.                                                                                  |
| Splunk            | `group_vars/splunk_search/main.yml`  | Variabele `splunk_resources` gebruikt om `web.conf` te wijzigen en `tools.proxy.on = True` toegevoegd voor toegang via een reverse proxy. |

## Review Ansible Rol

De Ansible rol
{{< ansible-role-link role_name="c2platform.mgmt.splunk" >}}
breidt de `mason_splunk.ansible_role_for_splunk.splunk` uit. Het importeert deze
rol naast de `c2platform.core.linux` Ansible rol, wat verbeterde flexibiliteit
en kracht biedt voor het beheren van meerdere taken. Dit omvat configuratie in
`web.conf` nodig voor toegang tot Splunk Enterprise via de reverse proxy. Het
werd gecreëerd om de flexibele en krachtige `c2platform.core.linux` rol te
benutten, die toegang biedt tot 115 Ansible modules via een enkele
resource-variabele `splunk_resources`. Dit vereenvoudigt taken zoals het
wijzigen van `web.conf` voor reverse proxy-instellingen, waardoor uitgebreide
beheer van Splunk Enterprise-instellingen mogelijk is.

## Aanvullende Informatie

* {{< rellink path="/docs/concepts/dev/vagrant" desc=true >}}