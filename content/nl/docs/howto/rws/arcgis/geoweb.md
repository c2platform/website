---
categories: ["Handleiding"]
tags: [gis, geoweb, windows, sysprep, vertigis, studio, iis, SSL/TLS]
title: "Geoweb Instellen"
linkTitle: "Geoweb"
weight: 5
provisionTime: 7 minuten
description: >
  Gids voor het installeren van VertiGIS Studio Web en Studio Reporting.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

## Overzicht

Deze gids biedt een stapsgewijs proces om Geoweb in te stellen, inclusief
**VertiGIS Studio Web** en **Reporting**.

1. Vagrant maakt een VirtualBox VM genaamd `gsd-geoweb` en voert [Sysprep]({{<
   relref path="/docs/howto/dev-environment/setup/vagrant/" >}}) daarop uit.
2. Vagrant voert de Ansible provisioner uit om de VM voor te bereiden voor
   handmatige installatie van de MSI:

   1. Open firewall-poorten voor Web en Reporting.
   2. Maak een SSL/TLS-certificaat aan.
   3. Installeer IIS.
   4. Creëer een service account `sa_geoweb`.
   5. Configureer IIS: maak websites, bindings, applicatiepools, enz.
   6. Download de MSI en licentie bestand naar de node `gsd-geoweb`.
3. Maak verbinding met `gsd-geoweb` via RDP en voer de installateurs handmatig
   uit[^1].
4. Voer de provisioner opnieuw uit om de IIS-configuratie gecreëerd door de
   VertiGIS-installateurs bij te werken om het service account `sa_geoweb` te
   gebruiken.

| Node         | OS                  | Provider   | Doel              |
|--------------|---------------------|------------|-------------------|
| `gsd-geoweb` | Windows 2022 Server | VirtualBox | Web en Reporting  |

## Vereisten

* Voltooi de stappen om [de RWS Ontwikkelomgeving op Ubuntu 22 in te stellen]({{<
  relref path="/docs/howto/rws/dev-environment/setup" >}}).
* Zorg ervoor dat VertiGIS Studio software en licentiebestanden beschikbaar
  zijn. Zie [Software en licentiebestanden beschikbaar maken voor Ansible]({{<
  relref path="/docs/howto/rws/dev-environment/setup/software/" >}}).
* Zorg ervoor dat ArcGIS Server, DataStore, Portal en Web Adaptor draaien op
  `gsd-agserver1` en `gsd-agportal1`. Zie [ArcGIS Server en Data Store
  instellen met behulp van Ansible]({{< relref path="/docs/howto/rws/arcgis/server" >}})
  en [ArcGIS Portal en Web Adaptors instellen met behulp van Ansible]({{< relref path="/docs/howto/rws/arcgis/portal" >}}).

## Installatie

Voer het volgende commando uit:

```bash
vagrant up gsd-geoweb
```

## App maken in ArcGIS Portal

1. Navigeer naar {{< external-link url="https://gsd-agportal1.internal.c2platform.org/portal/home" htmlproofer_ignore="true" >}} en log in als `portaladmin`[^2].

   | Gebruikersnaam  | Wachtwoord      |
   |-----------------|------------------|
   | `portaladmin`   | `portaladmin123` |
2. Kies **Content** → **Nieuw item** → **Applicatie**
   | Eigenschap      | Waarde                                            | Opmerking                |
   |-----------------|--------------------------------------------------|--------------------------|
   | Applicatietype  | **Andere applicatie**                            |                          |
   | Titel           | `Geoweb Modules registratie`                     |                          |
   | Map             |                                                   | Houd standaard `portaldmin` |
   | Tags            | `Geoweb` `Vertigis Studio` `Modules`             |                          |
   | Samenvatting    | `App voor VertiGIS Studio modules registratie`   |                          |
3. Via **Instellingen** → **Applicatie** → **Bijwerken**:
   1. voeg de **URL** toe
      {{< external-link url="https://geoweb.c2platform.org/" htmlproofer_ignore="true" >}};
   2. voeg de **Redirect URI** toe
      - `https://geoweb.c2platform.org/`
      - `https://geoweb.c2platform.org/ModuleViewer`
      - `https://geoweb.c2platform.org/ModuleReporting/designer`
   3. Klik **Opslaan**.
4. Kopieer de **App ID** / **Client ID** naar je klembord (bv.,
   `3a01FOjRkQJD2dCv`).

## Installeren van Studio Web

1. Verbind met `gsd-geoweb` via RDP.
2. Rechtsklik op de **Software** snelkoppeling op het Bureaublad en selecteer
   **Uitvoeren als Administrator**.
3. Voer `VertiGIS-Studio-Web-5.31.0.msi` uit.
   | Module                    | Bestemmingsmap               |
   |---------------------------|------------------------------|
   | VertiGIS Studio Web       | `D:\Apps\VertiGIS\Web`       |
5. Start post-installatie configuratie:
   | Eigenschap        | Waarde                                                         |
   |-------------------|---------------------------------------------------------------|
   | IIS Website       | **ModuleViewer**                                              |
   | Virtuele Map      | `/ModuleViewer/`                                              |
   | Portal Type       | **Portal for ArcGIS**                                         |
   | Portal URL        | `https://gsd-agportal1.internal.c2platform.org/portal/home`[^2] |
   | App ID            | ( waarde van vorige stap )                                    |
   | Redirect URL      | `https` `geoweb.c2platform.org`                               |

## Installeren van Studio Reporting

1. Voer als Administrator `VertiGIS-Studio-Reporting-5.22.0.msi` uit.
   | Module                    | Bestemmingsmap               |
   |---------------------------|------------------------------|
   | VertiGIS Studio Reporting | `D:\Apps\VertiGIS\Reporting` |
2. Post-installatie configuratie:
   | Eigenschap        | Waarde                                                         |
   |-------------------|---------------------------------------------------------------|
   | Gegevensmap       | `D:\ProgramData\Geocortex\Reporting`                          |
   | IIS Website       | **ModuleReporting**                                           |
   | Virtuele Map      | `/ModuleReporting/`                                           |
   | Portal Type       | **Portal for ArcGIS**                                         |
   | Portal URL        | `https://gsd-agportal1.internal.c2platform.org/portal/home`[^2] |
   | App ID            | ( waarde van vorige stap )                                    |
   | Redirect URL      | `https` `geoweb.c2platform.org` `/ModuleReporting/designer/`  |

## Verifiëren

1. Verbind met `gsd-geoweb` als gebruiker `vagrant`, start **IIS Manager**, en
   bekijk sitesinstellingen:
   | Site naam        | Applicatiepool  | Fysiek pad                 | Verbinden als[^3]           |
   |------------------|-----------------|-----------------------------|-----------------------------|
   | ModuleViewer     | DefaultAppPool  | `D:\inetpub\StudioWeb`      | Pass-through-verificatie    |
   | ModuleReporting  | DefaultAppPool  | `D:\inetpub\ModuleReporting`| Pass-through-verificatie    |

   Bekijk sitesbindings:

   | Site naam       | Type  | IP-adres        | Poort | Hostnaam   | SSL certificaat                    |
   |-----------------|-------|-----------------|-------|------------|------------------------------------|
   | ModuleViewer    | http  | Alles Niet-Toegewezen | 8080 | (leeg)     |                                    |
   | ModuleViewer    | https | Alles Niet-Toegewezen | 4443 | (leeg)     | gsd-geoweb.internal.c2platform.org |
   | ModuleReporting | http  | Alles Niet-Toegewezen | 8081 | (leeg)     |                                    |

2. Bekijk applicaties:
   | Applicatie      | Applicatiepool         | Fysiek pad                | Verbinden als |
   |-----------------|-------------------------|---------------------------|---------------|
   | ModuleViewer    | DefaultAppPool          | D:\inetpub\ModuleReporting| sa_geoweb     |
   | ModuleReporting | VertiGISStudioReporting | D:\inetpub\ModuleReporting| sa_geoweb     |

3. Bekijk applicatiepools:
   | Applicatiepool         | Identiteit |
   |------------------------|------------|
   | DefaultAppPool         | sa_geoweb  |
   | VertiGISStudioReporting| sa_geoweb  |

4. Navigeer naar
   {{< external-link url="https://geoweb.c2platform.org/ModuleViewer/" htmlproofer_ignore="true" >}}
   en
   {{< external-link url="https://geoweb.c2platform.org/ModuleReporting/" htmlproofer_ignore="true" >}}
   en log in als `portaladmin`.

## Herzien

In het inventarisatieproject
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}):

* De Geoweb play bevindt zich op `plays/gis/geoweb`.
* De Geoweb configuratie is in `group_vars/geoweb`.

De Ansible rol `c2platform.gis.vertigis_studio` heeft momenteel minimale code.
Downloads van MSI-installatieprogramma's en het licentiebestand worden
afgehandeld door de rol `c2platform.wincore.download`, terwijl het meeste
configuratiewerk wordt gedaan door de rol `c2platform.wincore.win`.

### Certificaten

De Ansible rol `c2platform.gis.vertigis_studio`, onderdeel van de Ansible GIS
Collectie [`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}), gebruikt de
Ansible rol `c2platform.core.cacerts2` om certificaten te maken en te implementeren
die vertrouwd zijn op alle knooppunten (inclusief `gsd-geoweb`). Op
`gsd-geoweb` wordt het geïmplementeerde certificaat gebruikt om IIS HTTPS
bindings aan te maken.

1. De vertrouwensrelatie voor certificaten gemaakt met de `cacerts2` rol is
   geconfigureerd in het bestand `group_vars/windows/main.yml`, van toepassing
   op alle Windows hosts:
   ```yaml
   win_resources:
      0-bootstrap:
         - name: Download c2.crt
           type: win_get_url
           url: https://gitlab.com/c2platform/rws/ansible-gis/-/raw/master/.ca/c2/c2.crt?ref_type=heads
           dest: C:\tmp\c2.crt
         - name: C2 CA Server Trust Relationship
           type: win_certificate_store
           path: C:\tmp\c2.crt
           store_name: Root
           store_location: LocalMachine
   ```
3. Het bestand `group_vars/geoweb/certs.yml` bevat de configuratie voor het
   certificaat als onderdeel van de variabele
   `vertigis_studio_cacerts2_certificates`, het wordt geïmplementeerd naar
   `C:\ProgramData\Certs\geoweb-GSD-GEOWEB.p12`.
4. Het bestand `group_vars/geoweb/win.yml` bevat de configuratie om het
   certificaat te importeren in de Windows certificaatwinkel met behulp van de
   variabele `vertigis_studio_win_resources`, waaronder:
   ```yaml
    - name: Import Geoweb Certificaat
      type: win_certificate_store
      path: "{{ vertigis_studio_cacerts2_certificates[0]['deploy']['p12']['dest'] }}"
      key_exportable: true
      store_name: My
      key_storage: machine
      store_location: LocalMachine
      state: present
   ```
5. Ten slotte, de variabele `vertigis_studio_win_resources` wordt gebruikt voor
   het beheren van IIS HTTPS bindings met een item vergelijkbaar met:
   ```yaml
    - name: Voeg IIS HTTPS bindings toe
      type: win_iis_webbinding
      defaults:
        ssl_flags: 0  # sni uitgeschakeld
        protocol: https
        certificate_friendly_name: "{{ gs_geoweb_certificate_friendly_name }}"
        state: present
      resources:
        - name: ModuleViewer
          port: "{{ gs_vertigis_studio_web_port_ssl }}"
        - name: ModuleReporting
          port: "{{ gs_vertigis_studio_reporting_port_ssl }}"
   ```

## Extra Informatie

* {{< external-link url="https://docs.vertigisstudio.com/webviewer/latest/admin-help/on-premises-installation.html" text="VertiGIS Studio Web - On-Premises Installatie" htmlproofer_ignore="false" >}}
* [Tomcat SSL/TLS en Java Keystore en TrustStore Configuratie voor Linux en Windows Hosts]({{< relref path="/docs/howto/rws/certs/" >}})
* [Creëer een Eenvoudige CA Server met Ansible]({{< relref path="/docs/howto/rws/ca/" >}})

## Voetnoten

[^1]: Het automatiseren van de MSI is mogelijk met een script vergelijkbaar met
    {{< external-link url="https://gitlab.com/c2platform/rws/ansible-gis/-/blob/master/scripts/geoweb/install-vsw.ps1?ref_type=heads" text="install-vsw.ps1" htmlproofer_ignore="false" >}}, maar het lijkt niet 
    ondersteund te zijn om een andere virtuele map te gebruiken. De parameter
    `VirtualPath` lijkt genegeerd te worden.

[^2]: De reverse proxy-URL `https://age.c2platform.org/portal/home/` werkt
    momenteel niet. Gebruik in plaats daarvan
    `https://gsd-agportal1.internal.c2platform.org/portal/home`.

[^3]: De `win_iis_website` module staat niet toe dat je een andere "verbinden als"
    gebruiker instelt. Als dit moet worden gewijzigd, moet dit handmatig worden
    gedaan.