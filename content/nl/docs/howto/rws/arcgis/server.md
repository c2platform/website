---
categories: ["Handleiding"]
tags: [argcis, windows]
title: "Installeer ArcGIS Server en Data Store met Ansible"
linkTitle: "ArcGIS Server"
provisionTime: 23 minuten installatie
weight: 1
description: >
  Installeer ArcGIS Server en ArcGIS Data Store op `gsd-agserver1` met Ansible.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

Deze handleiding beschrijft hoe je de ArcGIS Server en Data Store instantie kunt maken op
node `gsd-agserver1` door gebruik te maken van de `arcgis_server` rol die deel uitmaakt van
de `c2platform.gis` Ansible collectie.

| Node              | OS                    | Provider    | Doel                             |
|-------------------|-----------------------|-------------|----------------------------------|
| `gsd-agserver1`   | Windows 2022 Server   | VirtualBox  | ArcGIS Server en Data Store     |

---

<!-- include-start: howto-prerequisites-rws.md -->
## Voorwaarden

Maak de reverse en forward proxy `gsd-rproxy1`.

```bash
vagrant up gsd-rproxy1
```

Voor meer informatie over de verschillende rollen die `gsd-rproxy1` vervult in dit project:

* [Setup Reverse Proxy en CA server]({{< relref path="/docs/howto/c2/reverse-proxy" >}})
* [Setup SOCKS proxy]({{< relref path="/docs/howto/c2/socks-proxy" >}})
* [Beheer van Server Certificaten als Certificeringsinstantie]({{< relref path="/docs/howto/c2/certs" >}})
* [ArcGIS Server software en licenties]({{< relref path="/docs/howto/rws/dev-environment/setup/software" >}}).
<!-- include-end -->

## Installatie

```bash
vagrant up gsd-agserver1
```

## Verifiëren

1. Gebruik het
   [RWS FireFox profiel]({{< relref path="/docs/howto/rws/dev-environment/setup/firefox" >}})
   en navigeer naar
   {{< external-link url="https://gsd.c2platform.org/" htmlproofer_ignore="true" >}}.
  Dit zou de startpagina moeten tonen.
2. Ga met de onderstaande inloggegevens naar
   {{< external-link url="https://gsd-agserver1.internal.c2platform.org:6443/arcgis/admin/" text="ArcGIS Server Administrator Directory" htmlproofer_ignore="true" >}} en navigeer naar
   {{< external-link url="https://gsd-agserver1.internal.c2platform.org:6443/arcgis/manager/" text="ArcGIS Server Manager" htmlproofer_ignore="true" >}}en log in.
   | Gebruikersnaam | Wachtwoord       |
   |----------------|------------------|
   | `siteadmin`    | `siteadmin123`   |
3. Navigeer naar
  {{< external-link url="https://localhost:2443/arcgis/datastore/" htmlproofer_ignore="true" >}}
  je zou de ArcGIS Data Store webinterface moeten zien met de opslagen:
  **Relationeel** en **Tegelcache**.
  Selecteer **Configureer Extra Data Stores** en log dan in op de ArcGIS Server
  instantie met de onderstaande waarden:

   | GIS Server                              | Gebruikersnaam | Wachtwoord      |
   |-----------------------------------------|----------------|-----------------|
   | `gsd-agserver1.internal.c2platform.org` | `siteadmin`    | `siteadmin123`  |

## Beoordeling

{{< under_construction >}}

{{< download path="/download/rws/arcgis_server_initialized.yml" text="`arcgis_server_initialized.yml`" >}}
{{< download path="/download/rws/arcgis_server_not_initialized.yml" text="`arcgis_server_not_initialized.yml`" >}}

## Links

* {{< external-link url="https://enterprise.arcgis.com/en/server/latest/install/windows/welcome-to-the-arcgis-for-server-install-guide.htm" text="Welkom bij de ArcGIS Server (Windows) installatiehandleiding" htmlproofer_ignore="false" >}}
* {{< external-link url="https://enterprise.arcgis.com/en/server/latest/get-started/windows/what-is-arcgis-for-server-.htm" text="Wat is ArcGIS Server?" htmlproofer_ignore="false" >}}