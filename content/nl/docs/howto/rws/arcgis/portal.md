---
categories: ["Handleiding"]
tags: [argcis, windows, portal]
title: "Instellen van ArcGIS Portal en Web Adapters met Ansible"
linkTitle: "ArcGIS Portal"
provisionTime: 26 minuten voorzien
weight: 2
description: >
  Installeer ArcGIS Portal en ArcGIS Web Adapters op `gsd-agportal1` met behulp van Ansible.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

Deze handleiding beschrijft hoe je de {{< external-link url="https://enterprise.arcgis.com/en/portal/latest/administer/windows/what-is-portal-for-arcgis-.htm" text="ArcGIS Portal" htmlproofer_ignore="false" >}} instantie `gsd-agportal1` kunt creëren met behulp van de `arcgis_portal` rol, die deel uitmaakt van de [`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}) Ansible collectie. Het inrichten van dit knooppunt creëert ook twee {{< external-link url="https://enterprise.arcgis.com/en/server/latest/install/windows/about-the-arcgis-web-adaptor.htm" text="ArcGIS Web Adaptor" htmlproofer_ignore="false" >}} instanties, één voor ArcGIS Portal en één voor ArcGIS Server die draait op `gsd-agserver1`. Zie [Instellen van ArcGIS Server met Ansible]({{< relref path="./server" >}} "Handleiding: Instellen van ArcGIS Server met Ansible").

| Knooppunt       | OS                  | Provider   | Doel                              |
|-----------------|---------------------|------------|-----------------------------------|
| `gsd-agportal1` | Windows 2022 Server | VirtualBox | ArcGIS Portal en Web Adapters     |

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Boundary(gis, "gis.c2platform.org", $type="") {
  System(agpro1, "ArcGIS Portal & Web Adapters", "gsd-agportal1", "") {
      Container(wa1, "Web Adapter", "")
      Container(wa2, "Web Adapter ", "")
      Container(portal1, "ArcGIS Portal", "")
  }
  System_Ext(server_datastore, "ArcGIS Server & Datastore", "gsd-agserver1") {
    Container_Ext(agserver1, "ArcGIS Server", "")
    Container_Ext(datastore, "ArcGIS Datastore", "")
  }
}

Rel(wa1, portal1, "", "")
Rel(wa2, agserver1, "", "")
Rel_D(portal1, agserver1, "", "")
Rel_D(agserver1, datastore, "", "")
'Lay_D(agserver1, datastore, "", "")
@enduml
```

## Voorwaarden

* [Instellen van ArcGIS Server en Date Store met behulp van Ansible]({{< relref path="/docs/howto/rws/arcgis/server" >}})

{{< alert >}}
Indien je geen ArcGIS Server / `gsd-agserver1` hebt draaien, zal de Ansible taak **Configure ArcGIS Web Adapter for Portal for ArcGIS** mislukken.
{{< /alert >}}

## Installatie

Om je omgeving in te stellen, volg je deze stappen met behulp van Vagrant:

1. Voer de volgende opdracht uit om twee playbooks uit te voeren: `plays/gis/portal.yml` en `plays/gis/web_adaptor.yml` op de gsd-portal1 virtuele machine.

```bash
vagrant up gsd-portal1
```

2. Als je liever deze playbooks afzonderlijk wilt uitvoeren, gebruik dan de volgende commando's:

```bash
export PLAY=plays/gis/portal.yml
vagrant up gsd-portal1 --provision
```

```bash
export PLAY=plays/gis/web_adaptor.yml
vagrant up gsd-portal1 --provision
```

Kies de optie die het best bij jouw behoefte past om de gsd-portal1 virtuele machine te configureren.

## Verificatie

Selecteer **Toon** in de GUI in VirtualBox Manager voor `gsd-portal1` en vervolgens inloggen en start FireFox.

### ArcGIS Portal

Navigeer naar {{< external-link url="https://gsd-agportal1:7443/arcgis/" htmlproofer_ignore="true" >}} en selecteer vervolgens **Aanmelden** en log in met de onderstaande inloggegevens. Navigeer naar {{< external-link url="https://gsd-agportal1:7443/arcgis/portaladmin" htmlproofer_ignore="true" >}} en selecteer vervolgens **Aanmelden** en log in met de onderstaande inloggegevens.

| Gebruikersnaam  | Wachtwoord       |
|-----------------|------------------|
| `portaladmin`   | `portaladmin123` |

### ArcGIS Web Adapter

1. Navigeer naar {{< external-link url="https://gsd-agportal1/portal/webadaptor" htmlproofer_ignore="true" >}}. Je zou de ArcGIS Web Adapter configuratiewizard moeten zien. Let op: de configuratie-URL van de webadapter moet worden benaderd vanaf de machine die de webadapter host.
    <details>
      <summary><kbd>Toon mij</kbd></summary>
    {{< image filename="/images/docs/age-web-adaptor.png?width=600px" >}}
    </details>
    </br>
2. Navigeer naar **IIS Manager** om de configuratie te controleren, bijvoorbeeld de HTTPS-binding op poort 443 met certificaat `gsd-agportal1`.
    <details>
      <summary><kbd>Toon mij</kbd></summary>
    {{< image filename="/images/docs/age-web-adaptor-iis.png?width=600px" >}}
    </details>

### ArcGIS Server (via Web Adapter)

Gebruik onderstaande inloggegevens om te navigeren naar {{< external-link url="https://gsd-agportal1.internal.c2platform.org/server/admin/" text="ArcGIS Server Beheerdersdirectory" htmlproofer_ignore="true" >}} en navigeer naar {{< external-link url="https://gsd-agportal1.internal.c2platform.org/server/manager/" text="ArcGIS Server Beheer" htmlproofer_ignore="true" >}} en log in.

| Eigenschap | Waarde             |
|------------|--------------------|
| Gebruikersnaam | `siteadmin`     |
| Wachtwoord | `siteadmin123`       |

### ArcGIS Portal (via Web Adapter)

{{< external-link url="https://gsd-agportal1.internal.c2platform.org/portal/home/" htmlproofer_ignore="true" >}}

## Beoordeling

Om een beter begrip te krijgen van hoe de ArcGIS Portal met Ansible wordt gemaakt, kun je het volgende bekijken:

In het Ansible playbook project [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}):

### ArcGIS Portal Play

1. `Vagrantfile.yml`: Dit bestand configureert de `gsd-agportal1` node met het `gis/portal` playbook. Dit bestand wordt gelezen in de `Vagrantfile`.
1. `hosts-dev.ini`: Het inventarisbestand wijst de `gsd-agportal1` node toe aan de `gs_portal` Ansible groep.

### ArcGIS Web Adapter Play

1.