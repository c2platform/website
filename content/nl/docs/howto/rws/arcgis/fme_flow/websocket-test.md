---
categories: ["Handleiding"]
tags: [fme, openssl, websocket, ubuntu, linux]
title: "FME WebSocket testen met OpenSSL en WSCAT"
linkTitle: "Test WebSocket"
weight: 1
description: >
  Leer hoe je kunt controleren of FME WebSockets correct zijn geconfigureerd en operationeel zijn.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}), [`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}), [`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

Wanneer FME Flow is geïnstalleerd, worden WebSockets standaard geconfigureerd met behulp van het `ws` protocol. In het Ansible-inventarisproject [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) worden WebSockets geconfigureerd om veilig te zijn en het `wss` protocol te gebruiken. Verkeerde configuraties kunnen de WebSockets-functionaliteit breken. Deze handleiding biedt drie methoden om de functionaliteit van WebSockets te verifiëren:

1. Gebruik van de OpenSSL-bibliotheek.
2. Gebruik van de WSCAT-hulpprogramma.
3. Gebruik van een aangepast PowerShell-script.

## Vereisten

* Volg [FME Flow instellen met Ansible]({{< relref path="/docs/howto/rws/arcgis/fme_flow" >}}) om de FME-omgeving in te stellen, die bestaat uit `gsd-fme-core`, `gsd-ad`, `gsd-db1` en `gsd-rproxy1`. Deze knooppunten moeten actief zijn en werken.

## SSH naar `gsd-rproxy1`

Om de testen met OpenSSL en WSCAT uit te voeren, verbind je met `gsd-rproxy1`:

```bash
vagrant ssh gsd-rproxy1
```

## WebSocket testen met OpenSSL

Het `gsd-rproxy1` knooppunt heeft standaard de OpenSSL-bibliotheek geïnstalleerd.

1. Leg een veilige SSL/TLS-verbinding met de server:

   ```bash
   openssl s_client -connect gsd-fme-core:7078
   ```

   <p><details>
   <summary><kbd>Toon mij</kbd></summary><p>

   ```bash
   root@gsd-rproxy1:~# openssl s_client -connect gsd-fme-core:7078
   VERBONDEN(00000003)
   Kan SSL_get_servername niet gebruiken
   depth=1 CN = c2
   verify return:1
   depth=0 C = NL, ST = Zuid-Holland, L = Den Haag, O = C2, OU = C2 GIS Platform Team, CN = gsd-fme-core
   verify return:1
   ---
   Certificaatketen
   0 s:C = NL, ST = Zuid-Holland, L = Den Haag, O = C2, OU = C2 GIS Platform Team, CN = gsd-fme-core
      i:CN = c2
      a:PKEY: rsaEncryption, 4096 (bit); sigalg: RSA-SHA256
      v:NotBefore: Sep  2 07:34:13 2024 GMT; NotAfter: Aug 31 07:34:13 2034 GMT
   ---
   Servercertificaat
   -----BEGIN CERTIFICATE-----
   MIIFyjCCA7KgAwIBAgIUFMVOmvJuY7/68kemIj6HCLuiTy4wDQYJKoZIhvcNAQEL
   BQAwDTELMAkGA1UEAwwCYzIwHhcNMjQwOTAyMDczNDEzWhcNMzQwODMxMDczNDEz
   WjB6MQswCQYDVQQGEwJOTDEVMBMGA1UECAwMWnVpZC1Ib2xsYW5kMREwDwYDVQQH
   (regels verwijderd)
   bocbN6ZKuDSO4087FzwjMOJ5yGxMAQymcGmiyZrM4D5iIrf3ozr8SAQ3lXDAnfZ9
   0RG2T5QteaD9ThQOsseGBq6HURphzOFtzL/FGR1nNr6jCGA5001+FQ/77dP84A==
   -----END CERTIFICATE-----
   subject=C = NL, ST = Zuid-Holland, L = Den Haag, O = C2, OU = C2 GIS Platform Team, CN = gsd-fme-core
   issuer=CN = c2
   ---
   Geen CA-namen voor cliëntcertificaat verzonden
   Digitale ondertekening van de peer: SHA256
   Type handtekening van de peer: RSA-PSS
   Server Tijdelijke Sleutel: X25519, 253 bits
   ---
   SSL-handdruk heeft 2274 bytes gelezen en 373 bytes geschreven
   Verificatie: OK
   ---
   Nieuw, TLSv1.3, Cijfer is TLS_AES_256_GCM_SHA384
   Server openbare sleutel is 4096 bit
   Veilige heronderhandeling wordt NIET ondersteund
   Compressie: GEEN
   Expansie: GEEN
   Geen ALPN onderhandeld
   Vroegtijdige gegevens werden niet verzonden
   Retourcode verifiëren: 0 (ok)
   ---
   ---
   Nieuw sessieticket na de handdruk aangekomen:
   SSL-Sessie:
      Protocol  : TLSv1.3
      Cijfer    : TLS_AES_256_GCM_SHA384
      Sessie-ID: 8AC31E962569F35F4A9C6FD9DE5FF5308D819A012DB754FFF88ED573E19C5DF1
      Sessie-ID-ctx:
      Hervatten PSK: 727A2C1C5D2B4EC513E941C9C759998493743FADED7E74E92AADC4C951EDF6A43E59775010FB465920C07B57DE42F96A
      PSK-identiteit: Geen
      PSK-identiteit hint: Geen
      SRP-gebruikersnaam: Geen
      Levensduurhint van TLS-sessieticket: 86400 (seconden)
      TLS-sessieticket:
      0000 - 7a b5 a3 f2 b0 91 51 91-6a 90 1c 40 3f 8f 79 f4   z.....Q.j..@?.y.
      0010 - fe 9f 42 86 fe 69 b1 20-45 2c 04 ff 0a 6f cd 6a   ..B..i. E,...o.j
      0020 - 29 8c 99 c2 d8 79 aa 17-11 53 56 ab c8 2a fc 22   )....y...SV..*."
   (regels verwijderd)
      06a0 - 43 d6 b3 c1 7f 62 55 50-7d a3 53 a4 3d 2c c3 30   C....bUP}.S.=,.0
      06b0 - 56 5d a9 ef 21 e0 8e 1e-27 0c f9 cf 61 ed         V]..!...'...a.

      Start Tijd: 1725356133
      Timeout   : 7200 (sec)
      Retourcode verifiëren: 0 (ok)
      Uitgebreid hoofdgeheim: nee
      Max Vroege Gegevens: 0
   ```

   </p></details></p>

2. Start de WebSocket-handdruk door handmatig de volgende regels in te voeren nadat de beveiligde verbinding tot stand is gebracht:

   ```
   GET /websocket HTTP/1.1
   Host: gsd-fme-core:7078
   Upgrade: websocket
   Connection: Upgrade
   Sec-WebSocket-Key: h3aOngl22xTlrMuoCGC4+w==
   Sec-WebSocket-Version: 13
   ```

   Opmerking: `Sec-WebSocket-Key` is een Base64-gecodeerde willekeurige waarde[^1].

3. Om je invoer in te dienen, druk je twee keer op Enter om een dubbele nieuwe regel te verzenden.

4. De reactie zou iets moeten zijn als:

   ```
   HTTP/1.1 101 Switching Protocols
   Upgrade: websocket
   Connection: Upgrade
   Sec-WebSocket-Accept: AEwKHncza7ZXcfBb5yM/aluzmpE=
   ```

   Deze reactie geeft aan dat de WebSocket-handdruk succesvol is, wat bevestigt dat de HTTP-verbinding nu is geüpgraded naar een WebSocket-verbinding.

Voor een uitgebreidere test die ook het `wss`-schema valideert, heb je een WebSocket-cliëntbibliotheek zoals WSCAT[^2] nodig.

## WSCAT

Dit gedeelte beschrijft hoe je FME WebSockets kunt valideren met {{< external-link url="https://github.com/websockets/wscat" text="WSCAT" htmlproofer_ignore="false" >}}.

### NodeJS, NPM en WSCAT installeren

Om WSCAT te installeren, voer je de volgende commando's uit op het knooppunt `gsd-proxy1`:

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # Dit laadt nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # Dit laadt nvm bash_completion
nvm install 20
node -v
npm -v
npm install -g wscat
```

<p><details>
  <summary><kbd>Toon mij</kbd></summary><p>

```bash
vagrant@gsd-rproxy1:~$ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # Dit laadt nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # Dit laadt nvm bash_completion
nvm install 20
node -v
npm -v
npm install -g wscat
  % Totaal    % Ontvangen % Xferd  Gemiddelde Snelheid   Tijd   Tijd    Tijd  Huidig
                                    Dload  Upload   Totaal   Besteed   Over   Snelheid
100 15916  100 15916    0     0  68354      0 --:--:-- --:--:-- --:--:-- 68603
=> Nvm downloaden van git naar '/home/vagrant/.nvm'
=> Kloon naar '/home/vagrant/.nvm'...
remote: Enumerating objects: 376, done.
remote: Counting objects: 100% (376/376), done.
remote: Compressing objects: 100% (324/324), done.
remote: Totaal 376 (delta 43), reused 168 (delta 25), pack-reused 0 (from 0)
Ontvangen objects: 100% (376/376), 374.67 KiB | 6.46 MiB/s, klaar.
Resolving deltas: 100% (43/43), klaar.
* (HEAD detached at FETCH_HEAD)
  master
=> Git-repository comprimeren en opruimen

=> Nvm bronstring aan .bashrc toevoegen van /home/vagrant
=> Nvm bash_completion bronstring aan .bashrc toevoegen van /home/vagrant
=> Sluit en open uw terminal opnieuw om nvm te gebruiken of voer het volgende uit om het nu te gebruiken:

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # Dit laadt nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # Dit laadt nvm bash_completion
Node v20.17.0 downloaden en installeren...
Downloading https://nodejs.org/dist/v20.17.0/node-v20.17.0-linux-x64.tar.xz...
#################################################################################################### 100.0%
Checksum berekenen met sha256sum
Checksums gematcht!
Nu met node v20.17.0 (npm v10.8.2)
Standaard alias maken: default -> 20 (-> v20.17.0)
v20.17.0
10.8.2

9 pakketten toegevoegd in 1s
npm notice
npm notice Nieuwe patchversie van npm beschikbaar! 10.8.2 -> 10.8.3
npm notice Changelog: https://github.com/npm/cli/releases/tag/v10.8.3
npm notice Om te updaten voer uit: npm install -g npm@10.8.3
npm notice
vagrant@gsd-rproxy1:~$ wscat --help
Gebruik: wscat [opties] (--listen <poort> | --connect <url>)

Opties:
  -V, --versie                       output de versienummer
  --auth < gebruiker: wachtwoord>    toevoeg basale HTTP authenticatie header (--connect only)
  --ca <ca>                          specificeer een Certificate Authority (--connect only)
  --cert <certificaat>               specificeer een Client SSL-certificaat (--connect only)
  --host <host>                      optionele host
  --sleutel <sleutel>                specificeer een Client SSL-certificaat's sleutel (--connect only)
  --max-omleidingen [nummer]         maximaal aantal toegestane omleidingen (--connect only) (standaard: 10)
  --geen-kleur                       zonder kleur uitvoeren
  --passphrase [passphrase]          specificeer een Client SSL-certificaat Sleutel's passphrase (--connect only).
                                     Indien geen waarde verstrekt, wordt er om gevraagd
  --proxy <[protocol://] host[:poort]>  verbonden via een proxy. Proxy moet de CONNECT-methode ondersteunen
  --slash                            slash-opdrachten voor controleframes activeren (/ping [data], /pong [data], /close [code [, reden]])
  -c, --connect <url>                verbinden met een WebSocket-server
  -H, --header <header:waarde>       stel een HTTP-header in. Herhaal om meerdere in te stellen (--connect only)
                                     (standaard: [])
  -L, --locatie                      omleidingen volgen (--connect only)
  -l, --luister <poort>              luister op poort
  -n, --geen-controle                controleer niet voor ongeautoriseerde certificaten
  -o, --origine <origine>            optionele origine
  -p, --protocol <versie>            optionele protocolversie
  -P, --toon-ping-pong               een melding tonen wanneer een ping of pong wordt ontvangen
  -s, --subprotocol <protocol>       optioneel subprotocol (standaard: [])
  -w, --wacht <seconden>             wacht gegeven seconden na uitvoerende commando
  -x, --uitvoeren <commando>         commando uitvoeren na verbinden
  -h, --help                         help voor commando weergeven
vagrant@gsd-rproxy1:~$

```

</p></details></p>

### WebSocket testen met WSCAT

Om de WebSocket te testen met WSCAT, voer het volgende commando uit op `gsd-rproxy1`:

```bash
wscat -c wss://gsd-fme-core:7078/websocket --ca /vagrant/.ca/c2/c2.crt
```

Dit commando zou moeten reageren met "Verbonden (druk op CTRL+C om te stoppen)". Merk op dat je de Certificeringsautoriteit (CA) bundel/certificaat `c2.crt` moet opgeven. Alternatief kun je `-n` of `--no-check` opgeven om de certificaatverificatie over te slaan. Zie `wscat --help` voor meer opties.

<p><details>
  <summary><kbd>Toon mij</kbd></summary><p>

```bash
vagrant@gsd-rproxy1:~$ wscat -c wss://gsd-fme-core:7078/websocket --ca /vagrant/.ca/c2/c2.crt
Verbonden (druk op CTRL+C om te stoppen)
> vagrant@gsd-rproxy1:~$ wscat -c wss://gsd-fme-core:7078/websocket -n
Verbonden (druk op CTRL+C om te stoppen)
> vagrant@gsd-rproxy1:~$ wscat -c wss://gsd-fme-core:7078/websocket
fout: niet in staat om het eerste certificaat te verifiëren
> vagrant@gsd-rproxy1:~$

```
</p></details></p>

## WebSocket testen met PowerShell

Je kunt ook een eenvoudig PowerShell-script gebruiken om de verbinding te testen. Verbind eerst met de FME Flow-node `gsd-fme-core`:

```bash
vagrant ssh gds-fme-core
```

Start PowerShell:

```cmd
powershell
```

Voer het script `websocket-test.ps1` uit:

```cmd
cd c:\vagrant\scripts\gsd-fme-core
.\websocket-test.ps1
```

<p><details>
  <summary><kbd>Toon mij</kbd></summary><p>

```bash
Microsoft Windows [Versie 10.0.20348.707]
(c) Microsoft Corporation. Alle rechten voorbehouden.

vagrant@GSD-FME-CORE C:\Users\vagrant>cd c:\vagrant\scripts\gsd-fme-core

vagrant@GSD-FME-CORE c:\vagrant\scripts\gsd-fme-core>powershell
Windows PowerShell
Copyright (C) Microsoft Corporation. Alle rechten voorbehouden.

Installeer de nieuwste PowerShell voor nieuwe functies en verbeteringen! https://aka.ms/PSWindows

PS C:\vagrant\scripts\gsd-fme-core> .\websocket-test.ps1
WebSocket-verbinding succesvol beveiligd
PS C:\vagrant\scripts\gsd-fme-core>
```

</p></details></p>

## Voetnoten

[^1]: De sleutel `Sec-WebSocket-Key` is een Base64-gecodeerde willekeurige waarde gegenereerd door de cliënt. Deze waarde wordt door de server gebruikt om een antwoordsleutel te genereren voor instemming met de WebSocket-verbinding. Je kunt je eigen `Sec-WebSocket-Key` genereren met het commando:

      ```bash
      openssl rand -base64 16
      ```

[^2]: De test met de OpenSSL-bibliotheekopdracht opent een TLS/SSL-verbinding naar de opgegeven server. Dit zorgt ervoor dat het onderliggende transport (van cliënt naar server) wordt versleuteld met TLS/SSL. Echter, het WebSocket-protocol dat via die verbinding wordt onderhandeld is apart van de beveiliging die de transportlaag biedt. Om `wss` specifiek te testen gebruik je een WebSocket client-bibliotheek (zoals WSCAT of een browser met WebSocket-ondersteuning), waarbij je het `wss`-schema specificeert.