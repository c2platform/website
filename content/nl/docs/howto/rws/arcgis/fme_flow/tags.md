---
categories: [Handleiding]
tags: [tags, ansible, fme, windows]
title: Versnellen van Ansible Provisioning met FME Flow via Ansible Tags
linkTitle: FME Flow en Ansible Tags
weight: 2
toc_hide: false
hide_summary: true
description: >
  Verbeter de efficiëntie van je Ansible provisioning door gebruik te maken van de
  kracht van tags.
---

Projecten:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

Deze handleiding toont hoe je het provisioning-proces van Ansible kunt
versnellen door tags te gebruiken. Taken binnen Ansible-rollen, die deel
uitmaken van de
[C2 Platform Ansible Collections]({{< relref path="/docs/concepts/ansible/projects/collections">}}),
volgen het tagging-schema zoals beschreven in de richtlijn
{{< rellink path="/docs/guidelines/coding/tags" desc=false >}}.

## Randvoorwaarden

Zorg ervoor dat je de volgende voorwaarde hebt voltooid:

* {{< rellink path="/docs/howto/rws/arcgis/fme_flow" desc="true" >}}

## Uitrol

Voorzie de node, ervan uitgaande dat deze al up-to-date is, door de volgende
command uit te voeren:

```bash
ansible-playbook plays/gis/fme.yml -i hosts.ini --limit gsd-fme-core
```

Het uitvoeren van de play kan enkele minuten duren, ongeveer meer dan 4.5
minuten afhankelijk van je hardware, omdat het de gewenste staat van een eerder
geprovisioneerde node controleert. De uitvoering kan worden afgesloten met een
samenvatting zoals hieronder, wat aangeeft dat 250 taken zijn uitgevoerd,
waarbij 106 taken zijn overgeslagen.

```log
PLAY RECAP *********************************************************************
gsd-fme-core               : ok=240  changed=0    unreachable=0    failed=0    skipped=106  rescued=0    ignored=0
```

## Taken Lijst

Met de 'gsd-fme-core' geprovisioneerd en operationeel, kun je de optie
`--list-tasks` gebruiken om alle taken en hun tags te bekijken.

```bash
ansible-playbook plays/gis/fme.yml -i hosts.ini --limit gsd-fme-core --list-tasks
```

In het Ansible GIS-inventarisproject
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
geeft dit commando de resultaten voor vier plays weer. We zijn vooral
geïnteresseerd in de laatste play omdat we `--limit gsd-fme-core` gebruiken.

```log
  play #4 (fme): FME	TAGS: []
    tasks:
      c2platform.core.secrets : Set fact for common_secrets_dirs	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Deprecated variable	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Include secrets	TAGS: [always, secrets, vault]
      c2platform.wincore.ad : Include ad_resources	TAGS: [v0]
      c2platform.wincore.ad : Include ad_resources_types	TAGS: [v0]
      c2platform.core.vagrant_hosts : Set fact vagrant_hosts_content	TAGS: [config, development, system, v0, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage /etc/hosts of Vagrant Host	TAGS: [config, development, system, v0, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Linux guest	TAGS: [config, development, system, v0, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [config, development, system, v0, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [config, development, system, v0, vagrant, vagrant_hosts]
      c2platform.core.secrets : Set fact for common_secrets_dirs	TAGS: [always, secrets, v0, vault]
      c2platform.core.secrets : Deprecated variable	TAGS: [always, secrets, v0, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, secrets, v0, vault]
      c2platform.core.secrets : Include secrets	TAGS: [always, secrets, v0, vault]
      c2platform.wincore.win : Include win_resources_types	TAGS: [install, system, v0, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip, windows]
      c2platform.wincore.win : Include win_resources	TAGS: [install, system, v0, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip, windows]
      c2platform.gis.java : Stat java_home	TAGS: [v0]
      c2platform.gis.java : Install	TAGS: [application, install, java, v0]
      c2platform.core.cacerts2 : Set fact cacerts2_certificates	TAGS: [always, cacerts2-facts, v0]
      c2platform.core.cacerts2 : cacerts2_certificates	TAGS: [always, cacerts2-facts, v0]
      c2platform.core.cacerts2 : Set various certificate facts	TAGS: [always, cacerts2-facts, v0]
      c2platform.core.cacerts2 : Set fact cacerts2_certificates	TAGS: [always, cacerts2-facts, v0]
      include_tasks	TAGS: [application, cacerts2, install, v0]
      include_tasks	TAGS: [application, cacerts2, install, v0]
      include_tasks	TAGS: [application, cacerts2, install, v0]
      c2platform.gis.fme_flow : Stat license	TAGS: [application, fme_flow, install, v1]
      c2platform.gis.fme_flow : Create folders	TAGS: [application, fme_flow, install, v1]
      c2platform.gis.fme_flow : Include debug tasks	TAGS: [v1]
      c2platform.gis.tomcat : Stat Tomcat license	TAGS: [application, install, tomcat, v1]
      c2platform.gis.tomcat : Install	TAGS: [application, install, tomcat, v1]
      c2platform.core.vagrant_hosts : Set fact vagrant_hosts_content	TAGS: [application, config, development, system, tomcat, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage /etc/hosts of Vagrant Host	TAGS: [application, config, development, system, tomcat, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Linux guest	TAGS: [application, config, development, system, tomcat, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, system, tomcat, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, system, tomcat, v1, vagrant, vagrant_hosts]
      c2platform.core.secrets : Set fact for common_secrets_dirs	TAGS: [always, application, config, secrets, tomcat, v1, vault]
      c2platform.core.secrets : Deprecated variable	TAGS: [always, application, config, secrets, tomcat, v1, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, application, config, secrets, tomcat, v1, vault]
      c2platform.core.secrets : Include secrets	TAGS: [always, application, config, secrets, tomcat, v1, vault]
      c2platform.wincore.win : Include win_resources_types	TAGS: [application, config, tomcat, v1, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip]
      c2platform.wincore.win : Include win_resources	TAGS: [application, config, tomcat, v1, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip]
      c2platform.gis.tomcat : Include certificates tasks	TAGS: [application, install, tomcat, v1]
      c2platform.gis.tomcat : Include download cleanup tasks	TAGS: [v1]
      c2platform.gis.tomcat : Manage service	TAGS: [application, install, tomcat, v1, win_service]
      c2platform.gis.tomcat : Test Tomcat service	TAGS: [application, install, tomcat, v1, win_service, win_uri]
      c2platform.gis.fme_flow : Include install tasks	TAGS: [application, fme_flow, install, v1]
      c2platform.core.vagrant_hosts : Set fact vagrant_hosts_content	TAGS: [application, config, development, fme_flow, system, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage /etc/hosts of Vagrant Host	TAGS: [application, config, development, fme_flow, system, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Linux guest	TAGS: [application, config, development, fme_flow, system, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, fme_flow, system, v1, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, fme_flow, system, v1, vagrant, vagrant_hosts]
      c2platform.core.secrets : Set fact for common_secrets_dirs	TAGS: [always, application, config, fme_flow, secrets, v1, vault]
      c2platform.core.secrets : Deprecated variable	TAGS: [always, application, config, fme_flow, secrets, v1, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, application, config, fme_flow, secrets, v1, vault]
      c2platform.core.secrets : Include secrets	TAGS: [always, application, config, fme_flow, secrets, v1, vault]
      c2platform.wincore.win : Include win_resources_types	TAGS: [application, config, fme_flow, install, system, v1, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip, windows]
      c2platform.wincore.win : Include win_resources	TAGS: [application, config, fme_flow, install, system, v1, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip, windows]
      c2platform.gis.fme_flow : Include database tasks	TAGS: [v1]
      c2platform.core.cacerts2 : Set fact cacerts2_certificates	TAGS: [always, application, cacerts2-facts, fme_flow, install, v1]
      c2platform.core.cacerts2 : cacerts2_certificates	TAGS: [always, application, cacerts2-facts, fme_flow, install, v1]
      c2platform.core.cacerts2 : Set various certificate facts	TAGS: [always, application, cacerts2-facts, fme_flow, install, v1]
      c2platform.core.cacerts2 : Set fact cacerts2_certificates	TAGS: [always, application, cacerts2-facts, fme_flow, install, v1]
      include_tasks	TAGS: [application, cacerts2, fme_flow, install, v1]
      include_tasks	TAGS: [application, cacerts2, fme_flow, install, v1]
      include_tasks	TAGS: [application, cacerts2, fme_flow, install, v1]
      c2platform.gis.fme_flow : FME Service	TAGS: [application, fme_flow, install, v1]
      c2platform.gis.fme_flow : Stat {{ fme_flow_api_config['token']['path'] }}	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Create token	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Return status_code	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Display the status code	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Save token value	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Read token	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Extract the token value	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Construct JSON body	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Debug JSON body	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : If LDAP connection to FME exists, then update	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Return status_code	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Create new connection to LDAP	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Fail task if status code is unexpected	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Display message if status code is 409	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Continue with other tasks	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Set proxy voor fme flow	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Licenseer de FME flow applicatie	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Configure email-settings	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Vind admin wachtwoord	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Copy content	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Fetch content	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Include the fetched YAML file	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Extract ID from the YAML structure	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Pas Admin wachtwoord aan	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Delete admin_id.yml	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Delete fme_flow_uri_return.yml	TAGS: [application, config, config_api, fme_flow, v1]
      c2platform.gis.fme_flow : Delete fme_flow_LDAP.yml	TAGS: [application, config, config_api, fme_flow, v1]
      Include role download	TAGS: [application, fme_flow, install, v1]
      c2platform.gis.fme_flow : Stat license	TAGS: [application, fme_flow, install, v2]
      c2platform.gis.fme_flow : Create folders	TAGS: [application, fme_flow, install, v2]
      c2platform.gis.fme_flow : Include debug tasks	TAGS: [v2]
      c2platform.gis.tomcat : Stat Tomcat license	TAGS: [application, install, tomcat, v2]
      c2platform.gis.tomcat : Install	TAGS: [application, install, tomcat, v2]
      c2platform.core.vagrant_hosts : Set fact vagrant_hosts_content	TAGS: [application, config, development, system, tomcat, v2, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage /etc/hosts of Vagrant Host	TAGS: [application, config, development, system, tomcat, v2, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Linux guest	TAGS: [application, config, development, system, tomcat, v2, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, system, tomcat, v2, vagrant, vagrant_hosts]
      c2platform.core.vagrant_hosts : Manage hosts file of Vagrant Windows guest	TAGS: [application, config, development, system, tomcat, v2, vagrant, vagrant_hosts]
      c2platform.core.secrets : Set fact for common_secrets_dirs	TAGS: [always, application, config, secrets, tomcat, v2, vault]
      c2platform.core.secrets : Deprecated variable	TAGS: [always, application, config, secrets, tomcat, v2, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, application, config, secrets, tomcat, v2, vault]
      c2platform.core.secrets : Include secrets	TAGS: [always, application, config, secrets, tomcat, v2, vault]
      c2platform.wincore.win : Include win_resources_types	TAGS: [application, config, tomcat, v2, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip]
      c2platform.wincore.win : Include win_resources	TAGS: [application, config, tomcat, v2, win_acl, win_acl_inheritance, win_audit_policy_system, win_audit_rule, win_auto_logon, win_certificate, win_certificate_info, win_certificate_store, win_chocolatey, win_chocolatey_config, win_chocolatey_feature, win_chocolatey_source, win_command, win_computer_description, win_copy, win_credential, win_data_deduplication, win_defrag, win_dhcp_lease, win_disk_facts, win_disk_image, win_dns_client, win_dns_record, win_dns_zone, win_dotnet_ngen, win_dsc, win_environment, win_eventlog, win_eventlog_entry, win_fail, win_feature, win_feature_info, win_file, win_file_compression, win_file_version, win_firewall, win_firewall_rule, win_format, win_get_url, win_group, win_group_membership, win_hostname, win_hosts, win_hotfix, win_http_proxy, win_iis_virtualdirectory, win_iis_webapplication, win_iis_webapppool, win_iis_webbinding, win_iis_website, win_include_role, win_inet_proxy, win_initialize_disk, win_lineinfile, win_listen_ports_facts, win_mapped_drive, win_msg, win_net_adapter_feature, win_netbios, win_nssm, win_optional_feature, win_owner, win_package, win_pagefile, win_partition, win_path, win_pester, win_ping, win_pip, win_power_plan, win_powershell, win_psexec, win_psmodule, win_psmodule_info, win_psrepository, win_psrepository_copy, win_psrepository_info, win_psscript, win_psscript_info, win_pssession_configuration, win_rds_cap, win_rds_rap, win_rds_settings, win_reboot, win_reg_stat, win_regedit, win_region, win_regmerge, win_robocopy, win_route, win_say, win_scheduled_task, win_scheduled_task_stat, win_scoop, win_scoop_bucket, win_security_policy, win_service, win_service_info, win_share, win_shell, win_shortcut, win_slurp, win_snmp, win_tempfile, win_template, win_timezone, win_toast, win_unzip, win_updates, win_uri, win_user, win_user_profile, win_user_right, win_wait_for, win_wait_for_process, win_wakeonlan, win_webpicmd, win_whoami, win_xml, win_zip]
      c2platform.gis.fme_flow : Include database tasks	TAGS: [v2]
      c2platform.core.cacerts2 : Set fact cacerts2_certificates	TAGS: [always, application, cacerts2-facts, fme_flow, install, v2]
      c2platform.core.cacerts2 : cacerts2_certificates	TAGS: [always, application, cacerts2-facts, fme_flow, install, v2]
      c2platform.core.cacerts2 : Set various certificate facts	TAGS: [always, application, cacerts2-facts, fme_flow, install, v2]
      c2platform.core.cacerts2 : Set fact cacerts2_certificates	TAGS: [always, application, cacerts2-facts, fme_flow, install, v2]
      include_tasks	TAGS: [application, cacerts2, fme_flow, install, v2]
      include_tasks	TAGS: [application, cacerts2, fme_flow, install, v2]
      include_tasks	TAGS: [application, cacerts2, fme_flow, install, v2]
      c2platform.gis.fme_flow : FME Service	TAGS: [application, fme_flow, install, v2]
      c2platform.gis.fme_flow : Stat {{ fme_flow_api_config['token']['path'] }}	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Create token	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Return status_code	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Display the status code	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Save token value	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Read token	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Extract the token value	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Construct JSON body	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Debug JSON body	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : If LDAP connection to FME exists, then update	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Return status_code	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Create new connection to LDAP	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Fail task if status code is unexpected	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Display message if status code is 409	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Continue with other tasks	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Set proxy voor fme flow	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Licenseer de FME flow applicatie	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Configure email-settings	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Vind admin wachtwoord	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Copy content	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Fetch content	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Include the fetched YAML file	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Extract ID from the YAML structure	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Pas Admin wachtwoord aan	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Delete admin_id.yml	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Delete fme_flow_uri_return.yml	TAGS: [application, config, config_api, fme_flow, v2]
      c2platform.gis.fme_flow : Delete fme_flow_LDAP.yml	TAGS: [application, config, config_api, fme_flow, v2]
      Include role download	TAGS: [application, fme_flow, install, v2]

```

## Provisioneren met Tags

Om het provisioneringsproces te versnellen en je te richten op specifieke taken
zoals configuratie, kun je taken met de tags `install` of `system` overslaan
via:

```bash
ansible-playbook plays/gis/fme.yml -i hosts.ini --limit gsd-fme-core --skip-tags install,system
```

Hieronder zijn voorbeelden van takenfiltering met behulp van tags:

| Tags                                   | Taken | Tijd   | Beschrijving                                                                                                  |
|----------------------------------------|-------|--------|---------------------------------------------------------------------------------------------------------------|
| ``                                     | 240   | ≈ 4:40 | Voer alle provisioneringstaken uit.                                                                           |
| `--skip-tags install,system`           | 27    | ≈ 0:30 | Sluit taken uit die de tags `install` of `system` bevatten, focus op taken met `config` en `application`.[^1] |
| `--skip-tags install,system,tomcat`    | 17    | ≈ 0:20 | Zoals hierboven met daarnaast ook uitsluiting van Tomcat-taken.                                               |
| `--tags fme_flow`                      | 45    | ≈ 0:30 | Voer taken uit die specifiek de tag `fme_flow` hebben.                                                        |
| `--tags fme_flow --skip-tags install`  | 11    | ≈ 0:12 | Voer alleen configuratietaken van de FME Flow applicatie uit.                                                 |
| `--tags fme_flow --skip-tags cacerts2` | 27    | ≈ 0:20 | Voer installatie- en configuratietaken van de FME Flow applicatie uit, zonder certificatentaken.              |

{{< alert title="Let op:" >}}
Bij gebruik van `--tags` of `--skip-tags` met meerdere tags geldt een impliciete
"of" conditie. Bijvoorbeeld, `--tags config,fme_flow` resulteert in taken met
ofwel `config` of `fme_flow` tags die uitgevoerd worden. Om specifiek `config`
taken binnen `fme_flow` te targeten, gebruik `--tags fme_flow --skip_tags
install`.
{{< /alert >}}

## Extra Informatie

* {{< rellink path="/docs/howto/rws/arcgis/fme_flow/tags" desc=true >}}
* {{< external-link url="https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_tags.html" text="Tags — Ansible Community Documentation" htmlproofer_ignore="false" >}}
* {{< rellink path="/docs/guidelines/coding/tags/linux" desc=true >}}

## Voetnoten

[^1]: Volgens de C2 Platform tagging richtlijnen beschreven in
    {{< rellink path="/docs/guidelines/coding/tags" desc=false >}},
    worden taken getagd op basis van `install` of `config`, en `system` of
    `application`, dus als je `install` of `system` "overslaat", betekent dat je
    `config` "en" `application` "inclusief" bent.
