---
categories: ["Handleiding"]
tags: [gis, fme, windows, sysprep]
title: "FME Flow Instellen met Ansible"
linkTitle: "FME Flow"
weight: 5
provisionTime: 21 minuten voorzien
description: >
  Stel de FME Flow Core, FME Flow Database en FME Flow System Share in op MS Windows met behulp van Ansible.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}), [`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}), [`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

Deze handleiding geeft een overzicht van het instellen van **FME Flow Core**, **FME Flow Database**, en **FME Flow System Share** met Vagrant en Ansible. Voor meer details over deze componenten, verwijzen wij naar {{< external-link url="https://docs.safe.com/fme/2023.0/html/FME-Flow/ReferenceManual/architecture.htm" text="FME Flow Architectuur" htmlproofer_ignore="false" >}}.

1. Vagrant creëert twee VirtualBox Windows VM's: `gsd-fme-core`, `gsd-ad`, en een LXD-container `gsd-db1`.
2. Vagrant maakt gebruik van de {{< external-link url="https://github.com/rgl/vagrant-windows-sysprep" text="Vagrant Windows Sysprep Provisioner" htmlproofer_ignore="false" >}} op zowel `gsd-fme-core` als `gsd-ad`[^1].
4. Vagrant voert de Ansible provisioner in de volgende volgorde uit op de nodes:
   1. Op `gsd-ad` configureert de `c2platform.wincore.ad` collectie de AD domeincontroller voor het domein `ad.c2platform.org`.
   2. PostgreSQL 14 wordt geïnstalleerd op `gsd-db1`, inclusief een database en gebruiker met de benodigde rechten.
   3. Op `gsd-fme-core` voert Ansible de volgende stappen uit:
      1. Maakt de node lid van het Windows-domein `ad.c2platform.org`.
      2. Installeert Java met de `c2platform.gis.java` rol.
      3. Installeert Tomcat met de `c2platform.gis.tomcat` rol.
      4. Installeert **FME Flow Core** met de `c2platform.gis.fme` rol.

Het onderstaande diagram illustreert de setup die met Vagrant is bereikt, met uitzondering van de reverse proxy `gsd-rproxy1`.

| Node           | OS                  | Provider   | Doelstelling                                  |
|----------------|---------------------|------------|-----------------------------------------------|
| `gsd-fme-core` | Windows 2022 Server | VirtualBox | FME Core Web Server                           |
| `gsd-ad`       | Windows 2022 Server | VirtualBox | AD domeincontroller en host van FME File Share|
| `gsd-db1`      | Ubuntu 22.04 LTS    | LXD        | PostgreSQL database server                    |

## Vereisten

Voordat u verder gaat, zorg ervoor dat u de stappen hebt voltooid die zijn beschreven in {{< rellink path="/docs/howto/rws/dev-environment/setup" desc=false >}}.

## Installatie

Gebruik de volgende opdrachten om de FME play uit te voeren en de `gsd-ad`, `gsd-db1` en `gsd-core-fme` nodes te creëren. Het uitvoeren van het `vagrant up` commando zal ongeveer 25 minuten duren.

```bash
export BOX="gsd-ad gsd-db1 gsd-core-fme"
export PLAY="plays/gis/fme.yml"
vagrant up $BOX | tee provision.log
```

## Verifiëren

### `gsd-ad`

1. Log in op `gsd-ad` en voer `systeminfo | Select-String "Domain"` uit. Dit zou `ad.c2platform.org` moeten retourneren.
    <details>
      <summary><kbd>Toon mij</kbd></summary>

    ```bash
    PS C:\Users\vagrant> systeminfo | Select-String "Domain"

    OS Configuratie:          Primair Domeincontroller
    Domein:                    ad.c2platform.org


    PS C:\Users\vagrant> nslookup ad.c2platform.org
    Server:  ip6-localhost
    Adres:  ::1

    Naam:    ad.c2platform.org
    Adressen:  1.1.8.108
              10.0.2.15

    PS C:\Users\vagrant>
    ```

    </details>
2. Open de **DNS Manager** en controleer de eigenschappen van de DNS-server **GSD-AD**. Alleen `1.1.8.108` moet zijn ingeschakeld als een luisterend IP-adres.
    <details>
    <summary><kbd>Toon mij</kbd></summary>
    {{< image filename="/images/docs/dns-manager.png?width=1200px" >}}
    </details>
3. Voer op je Ubuntu-laptop `dig @1.1.8.108 ad.c2platform.org` uit. Dit zou `ad.c2platform.org` moeten vertalen naar `1.1.8.108`.
    <details>
      <summary><kbd>Toon mij</kbd></summary>

    ```bash
    onknows@io3:~$ dig @1.1.8.108 ad.c2platform.org

    ; <<>> DiG 9.18.12-0ubuntu0.22.04.3-Ubuntu <<>> @1.1.8.108 ad.c2platform.org
    ; (1 server gevonden)
    ;; globale opties: +cmd
    ;; Antwoord ontvangen:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 27806
    ;; vlaggen: qr aa rd ra; QUERY: 1, ANTWOORD: 2, AUTHORITY: 0, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: versie: 0, vlaggen:; udp: 4000
    ;; VRAAGSECTIE:
    ;ad.c2platform.org.		IN	A

    ;; ANTWOORDSECTIE:
    ad.c2platform.org.	600	IN	A	1.1.8.108
    ad.c2platform.org.	600	IN	A	10.0.2.15

    ;; Vraagduur: 0 msec
    ;; SERVER: 1.1.8.108#53(1.1.8.108) (UDP)
    ;; WANNEER: Wo Okt 25 09:40:07 CEST 2023
    ;; BERICHTGROOTTE rcvd: 78
    ```

    </details>

### `gsd-fme-core`

Log in op `gsd-fme-core`.

1. Verifieer dat de computer deel uitmaakt van het domein `ad.c2platform.org`:
    ```bash
    vagrant ssh gsd-fme-core
    powershell
    systeminfo.exe | Select-String "Domain"
    ```
    <details>
      <summary><kbd>Toon mij</kbd></summary>

    ```bash
    vagrant@GSD-FME-CORE C:\Users\vagrant>powershell
    Windows PowerShell
    Copyright (C) Microsoft Corporation. Alle rechten voorbehouden.

    Installeer de nieuwste PowerShell voor nieuwe functies en verbeteringen! https://aka.ms/PSWindows

    PS C:\Users\vagrant> systeminfo.exe | Select-String "Domain"

    Domein:                    ad.c2platform.org


    PS C:\Users\vagrant>

    ```

    </details>

### Login `gis-backup-operator`

Log in als `gis-backup-operator` gebruikmakend van remote desktop op `gsd-fme-core` om te bevestigen dat de gebruiker is aangemaakt met het juiste wachtwoord.

### Login FME

Na het succesvol voltooien van deze opdrachten, moet u toegang kunnen krijgen tot de FME Flow-interface door te bezoeken {{< external-link url="https://gsd-fme-core.internal.c2platform.org/fmeserver/" htmlproofer_ignore="false" >}}. Log in als `admin` met het wachtwoord `admin`.

### Database

Op `gsd-fme-core`, maak verbinding gebruikmakend van remote desktop met de Vagrant-gebruiker, start `pgAdmin`, en importeer de instellingen in het bestand `C:\Users\Public\Desktop\pgadmin.json`.

Maak verbinding met de server gebruikmakend van het wachtwoord `secret`. Doorzoek de tabellen van de **fmeserver** database; je zou de databasetabellen van FME Flow moeten zien zoals `fme_action` enz. Dit bevestigt dat de database is aangemaakt.

## Beoordelen

In het RWS Ansible Inventory-project [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}), bekijk specifieke plays en configuraties.

### FME Play

Bekijk de play `plays/gis/fme.yml`. Besteed aandacht aan het gebruik van de `when` voorwaarde, zodat de installatie van Java en Tomcat beperkt blijft tot `gsd-fme-core`.

Deze play maakt gebruik van verschillende rollen. Opvallende zijn:

* De `fme_flow` en `tomcat` rollen in de [`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}) collectie.
* `win` van de [`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}) collectie, die geïntegreerd is in zowel `tomcat` als `fme_flow` rollen. Deze integratie faciliteert het beheer van Windows resources via `fme_flow_win_resources` en `tomcat_win_resources`.

### Tomcat `context.xml`

De variabele `tomcat_win_resources` wordt gebruikt om het `context.xml` bestand van Tomcat te beheren. In `group_vars/fme_core/tomcat.yml`, let op het volgende item:

```yaml
  - path: "{{ tomcat_home }}/conf/context.xml"
    xpath: /Context
    fragment: >-
      <Valve className="org.apache.catalina.authenticator.SSLAuthenticator"
      disableProxyCaching="false" />
    notify: Restart Tomcat service
```

Dit onderdeel voegt een `Valve` element toe aan het bestand `context.xml`:

```xml
<Valve className="org.apache.catalina.authenticator.SSLAuthenticator" disableProxyCaching="false" />
```

### Tomcat `web.xml`

Evenzo wordt met `tomcat_win_resources` een `security-constraint` element toegevoegd aan `web.xml`. Let op de `xpath` expressie. Deze XPath expressie `/*[local-name()='web-app']` selecteert alleen het root element als de lokale naam (d.w.z. de tagnaam zonder de namespace prefix) 'web-app' is. Dit is een oplossing die over het algemeen minder precies is dan het gebruik van de juiste namespace-afhandeling, maar het wordt vaak gebruikt in tools die XML namespaces niet direct ondersteunen.

```yaml
  - path: "{{ tomcat_home }}/conf/web.xml"
    backup: true
    xpath: /*[local-name()='web-app']
    fragment: >-
      <security-constraint>
        <web-resource-collection>
          <web-resource-name>HTTPSOnly</web-resource-name>
          <url-pattern>/*</url-pattern>
        </web-resource-collection>
        <user-data-constraint>
          <transport-guarantee>CONFIDENTIAL</transport-guarantee>
        </user-data-constraint>
      </security-constraint>
    notify: Restart Tomcat service
```

### FME `fmeServerConfig.txt`

Let op het bestand `fmeServerConfig.txt` in de map `D:\Apps\FME\Flow\Server`. Het is door Ansible gewijzigd en bevat de regel:

```
FME_SERVER_WEB_URL=https://GSD-FME-CORE.ad.c2platform.org:443
```

Deze regel wordt beheerd door Ansible met behulp van de configuratie in `group_vars/fme_core/main.yml`. Dit bestand bevat de variabele `fme_flow_win_resources` zoals hieronder weergegeven:

```yaml
fme_flow_win_resources:
  core:
    - name: FME_SERVER_WEB_URL
      module: win_lineinfile
      path: "{{ fme_flow_home }}/Server/fmeServerConfig.txt"
      regex: '^FME_SERVER_WEB_URL='
      line: FME_SERVER_WEB_URL=https://{{ ansible_fqdn }}:{{ gs_tomcat_https_port }}
      notify: Restart FME Flow services
```

### WebSocket

Ansible configureert FME WebSockets om veilig te zijn / gebruik te maken van het `wss` schema. Zie de configuratie in `group_vars/fme_core/websocket.yml`. Wanneer Ansible provisieert zonder fouten, kun je redelijk zeker zijn dat WebSockets werken, omdat provisioning een handler zal activeren om te verifiëren dat de WebSocket poort 7078 open is en verbindingen accepteert.

Een aantal bestanden wordt gewijzigd / moet gewijzigd worden:

In FME Flow map `D:\Apps\FME\Flow`:

1. `Server/fmeWebSocketConfig.txt`
2. `Server/config/publishers/websocket.properties`
3. `Server/config/subscribers/websocket.properties`

Op FME Share `\\gsd-fme-core.ad.c2platform.org\fme\localization`:

1. `publishers\websocket\publisherProperties.xml`
2. `subscribers\websocket\subscriberProperties.xml`

De situatie waarbij een deel van de configuratie op de node staat en een deel van de configuratie op een share, is interessant, vooral wanneer meer dan één FME Core node wordt getarget. Wanneer er op hosts in de groep `fme_core` wordt gericht, is er met de standaard Ansible gedrag waarbij de nodes in de groep parallel worden gewijzigd een bepaalde race-conditie. Zal één node erin slagen het bestand te wijzigen en zullen andere nodes het niet hoeven te wijzigen omdat het al is gewijzigd? Of zullen ze het bestand tegelijk bekijken en dezelfde wijziging in het bestand schrijven? Afhankelijk van de situatie, als allen "gewijzigd" zijn, zal Ansible de herstart van de FME-service correct melden. Bij een situatie waarbij slechts één node het bestand "wijzigt", zal de FME-service van slechts één node worden herstart, wat niet correct is. En misschien kan de Ansible provisioning mislukken omdat sommige nodes proberen te updaten terwijl de bestanden zijn vergrendeld omdat ze door een andere node worden geschreven.

Om deze reden wordt er een extra bestand gemaakt in de FME Flow hoofddirectory met de naam `Server/ansible_shared_websocket_config.sha256`. Het bevat een sha256-hashwaarde van de configuratie in `publisherProperties.xml` en `subscriberProperties.xml` en dit bestand wordt vervolgens gebruikt om de twee handlers aan te roepen, één om de FME Service opnieuw op te starten en één om te verifiëren dat de WebSocket-poort geopend is.

Om de WebSocket-configuratie te verifiëren, zie [Test FME WebSocket met behulp van OpenSSL-library en / of WSCAT]({{< relref path="/docs/howto/rws/arcgis/fme_flow/websocket-test" >}}).

## Problemen oplossen

### WebSocket

Voor probleemoplossing, testen en verifiëren van de WebSocket-configuratie, zie [Test FME WebSocket met behulp van OpenSSL-library en / of WSCAT]({{< relref path="/docs/howto/rws/arcgis/fme_flow/websocket-test" >}}).

### Installatie Taak

Stel de variabele `fme_flow_debug: true` in om het bestand `install_extract_command.txt` te creëren in `D:\Apps\FME\logs`.

## Standaard Installatie

Om een standaardinstallatie van FME Flow uit te voeren, wat het implementeren van een PostgreSQL-database zonder een aparte Tomcat-instantie omvat, volg je deze stappen:

1. Verwijder of schakel de `fme_flow_install_command` uit.
2. Herstel de omgeving door de volgende opdrachten uit te voeren:

```bash
vagrant destroy gsd-fme-core -f
vagrant up gsd-fme-core
```

Na het succesvol voltooien van deze opdrachten, moet u de FME Flow-interface kunnen openen door te bezoeken {{< external-link url="http://localhost" htmlproofer_ignore="false" >}}, wat u zal omleiden naar {{< external-link url="http://localhost/fmeserver/" htmlproofer_ignore="false" >}}. Log in als `admin` met het wachtwoord `admin`.

## Aanvullende Informatie

* {{< external-link url="https://support.safe.com/hc/en-us/articles/25407558172557-WebSockets-and-FME-Server" text="WebSockets en FME Server – FME Support Center" htmlproofer_ignore="false" >}}


## Voetnoten

[^1]: Vagrant gebruikt de {{< external-link url="https://github.com/rgl/vagrant-windows-sysprep" text="Vagrant Windows Sysprep Provisioner" htmlproofer_ignore="false" >}} op alle nodes in het Ansible inventory project [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) maar in dit geval wordt het uitvoeren van Sysprep expliciet vermeld omdat deze Ansible play voor FME niet zal werken zonder Sysprep, omdat de play ook een AD domein creëert.