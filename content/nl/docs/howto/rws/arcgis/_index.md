---
categories: ["Handleiding"]
tags: [arcgis, windows]
title: "Instellen RWS GIS Platform met Ansible"
linkTitle: "GIS Platform Instellen"
weight: 2
description: >
  Installeer ArcGIS Server, ArcGIS Data Store, ArcGIS Portal, FME en Geoweb met behulp van Ansible.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})