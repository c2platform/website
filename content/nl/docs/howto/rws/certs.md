---
categories: ["Handleiding", "Voorbeeld"]
tags: [ansible, certificaten, pki, java, keystore, tomcat, SSL/TLS]
title: Tomcat SSL/TLS en Java Keystore en TrustStore Configuratie voor Linux en Windows Hosts
linkTitle: Tomcat SSL/TLS
provisionTime: 11 minuten
weight: 4
description: >
    Leer hoe u efficiënt SSL/TLS-certificaten, Java KeyStores en Java Truststores beheert en instelt met behulp van Ansible. Deze handleiding bevat stappen voor het integreren van zowel automatisch gegenereerde als extern verkregen certificaten.
---

Projecten:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}),
[`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

## Overzicht

Deze tutorial demonstreert het opzetten van twee Tomcat-nodes – een op Ubuntu 22 en een op Windows Server 2022 – voor SSL en Java TrustStore configuratie. Deze handleiding kan in principe worden gebruikt om de RWS-aanpak te valideren voor een eenvoudige Ansible-gebaseerde CA-serverbenadering zoals gedocumenteerd in [Implementatie van PKI voor RWS GIS met Ansible]({{< relref path="/docs/projects/rws/pki" >}}).

Met behulp van Vagrant en Ansible zullen de volgende dingen worden uitgevoerd:

1. Vagrant creëert twee nodes: een LXD-box die Ubuntu 22 draait en een VirtualBox VM met Windows Server 2022.
2. Vagrant initieert de Ansible provisioner.
3. Ansible installeert Tomcat op beide nodes met behulp van verschillende rollen.
   1. Voor Linux wordt de rol `c2platform.mw.tomcat` gebruikt om {{< external-link text="apache-tomcat-9.0.87.tar.gz" url="https://dlcdn.apache.org/tomcat/tomcat-9/v9.0.87/bin/apache-tomcat-9.0.87.tar.gz" htmlproofer_ignore="false" >}} te downloaden en uit te pakken en configureert vervolgens Tomcat.
   2. Voor Windows wordt `c2platform.gis.tomcat` gebruikt, die {{< external-link text="apache-tomcat-9.0.78.exe" url="https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.78/bin/apache-tomcat-9.0.78.exe" htmlproofer_ignore="false" >}} downloadt en gebruikt voor de installatie van Tomcat op MS Windows.
   3. Beide rollen omvatten de `c2platform.core.cacerts` rol voor de generatie van SSL/TLS-certificaten, het maken van een Java KeyStore en het bijwerken van Java TrustStore met het CA rootcertificaat.

| Node          | OS      | Distributie        | Provider   |
|---------------|---------|-------------------|------------|
| `gsd-tomcat1` | Linux   | Ubuntu 22         | LXD        |
| `gsd-tomcat2` | Windows | Windows Server 2022 | VirtualBox |

## Vereisten

Voordat u verder gaat, zorg ervoor dat u de stappen hebt voltooid voor het [Instellen van de RWS Ontwikkelomgeving op Ubuntu 22]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}) en dat de reverse proxy actief is

```bash
vagrant up gsd-rproxy1
```

In de [RWS Ontwikkelomgeving]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}) heeft de Reverse Proxy `gsd-rproxy1` ook een secundaire functie als de CA-server. Deze setup speelt een cruciale rol bij het beheren en distribueren van certificaten en Java Keystores / TrustStores.

## Installatie

Om de Tomcat-nodes `gsd-tomcat1` en `gsd-tomcat2` te initialiseren, voert u het onderstaande commando uit. Het provisioning-proces duurt naar verwachting ongeveer 11 minuten om te voltooien.

```bash
vagrant up gsd-tomcat1 gsd-tomcat2
```

## Verificatie

Als Vagrant de twee nodes succesvol heeft kunnen voorzien, zonder fouten, kan je controleren of alles correct werkt met de volgende stappen.

### SSL/TLS Configuratie

Start FireFox met je FireFox-profiel (zie [Configureer een FireFox-profiel]({{< relref path="/docs/howto/dev-environment/setup/firefox" >}})) en je zou naar de onderstaande links moeten kunnen navigeren en de "Hello World"-applicatie moeten zien. Er zouden geen SSL/TLS-certificaat foutmeldingen moeten verschijnen.

1. {{< external-link url="https://gsd-tomcat1.internal.c2platform.org:8443/helloworld/" htmlproofer_ignore="true" >}}
2. {{< external-link url="https://gsd-tomcat2.internal.c2platform.org:8443/helloworld/" htmlproofer_ignore="true" >}}

Hiermee heeft u geverifieerd dat de Tomcat SSL/TLS-configuratie door Ansible op `gsd-tomcat1` en `gsd-tomcat2` correct is uitgevoerd.

Navigeer naar de onderstaande links. Je zou dezelfde berichten moeten zien.

3. {{< external-link url="https://helloworld.c2platform.org/" htmlproofer_ignore="true" >}}
4. {{< external-link url="https://helloworld2.c2platform.org/" htmlproofer_ignore="true" >}}

Dit bevestigt dat je ook toegang hebt tot de Tomcat-diensten die draaien op `gsd-tomcat1` en `gsd-tomcat2` via de reverse proxy server `gsd-rproxy1`.

### TrustStore

Ansible werkt ook de Java TrustStore op `gsd-tomcat1` en `gsd-tomcat2` bij met het CA rootcertificaat. Wat je kunt verifiëren, werkt correct door gebruik te maken van een aangepaste Java-testklasse `HttpsConnectionTest`. Voer de volgende opdrachten uit om de Java TrustStore op `gsd-tomcat1` te verifiëren:

1. Ga met SSH de node binnen:
   ```bash
   vagrant ssh gds-tomcat1
   ```
2. Vanuit de directory `/vagrant/scripts/gsd-tomcat` voer je de `HttpsConnectionTest` uit:
    ```bash
    cd /vagrant/scripts/gsd-tomcat
    bash test-https.sh
    ```

    <details>
    <summary><kbd>Toon me</kbd></summary>

    ```
    vagrant@gsd-tomcat1:/vagrant/scripts/gsd-tomcat$ bash test-https.sh
    google, Dec 14, 2023, trustedCertEntry,
    Certificate fingerprint (SHA-256): 98:2D:C0:52:3D:84:7D:B5:89:AE:22:3E:DD:D8:37:86:72:79:D7:32:40:D6:03:55:D8:A6:F1:70:53:DF:48:95
    c2-ca, Dec 14, 2023, trustedCertEntry,
    Certificate fingerprint (SHA-256): 64:1B:BC:79:EE:0A:37:EA:3D:AC:B9:E3:E5:7F:02:71:3E:7D:C8:82:89:62:C5:E8:11:75:45:37:58:18:71:6F
    Response Code: 200
    Response Message: OK
    Response Code: 200
    Response Message: OK
    Response Code: 200
    Response Message: null
    vagrant@gsd-tomcat1:/vagrant/scripts/gsd-tomcat$
    ```

    </details>

Om hetzelfde op `gsd-tomcat2` te verifiëren, voer je de volgende opdrachten uit:

1. Ga met SSH de node binnen:
   ```bash
   vagrant ssh gds-tomcat2
   ```
2. Vanuit de directory `C:/vagrant/scripts/gsd-tomcat` voer je de `HttpsConnectionTest` uit:
    ```cmd
    cd /vagrant/scripts/gsd-tomcat
    test-https.bat
    ```
    <details>
    <summary><kbd>Toon me</kbd></summary>

    ```cmd
    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>test-https.bat

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>set JAVA_HOME=D:\Apps\jdk-17.0.5\jdk-17.0.5+8

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>set PATH=C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Windows\System32\OpenSSH\;C:\ProgramData\chocolatey\bin;C:\Program Files\PowerShell\7\;C:\Program Files\OpenSSH;C:\Program Files\Git\cmd;C:\Users\vagrant\AppData\Local\Microsoft\WindowsApps;;D:\Apps\jdk-17.0.5\jdk-17.0.5+8\bin;D:\Apps\jdk-17.0.5\jdk-17.0.5+8\bin

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>keytool -list -cacerts -storepass changeit -alias google
    google, Mar 20, 2024, trustedCertEntry,
    Certificate fingerprint (SHA-256): 47:19:35:3D:CE:E0:3D:17:BD:60:C2:28:71:84:03:CD:2F:9F:81:EE:06:C8:A6:63:23:4E:56:95:15:8C:19:C6

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>keytool -list -cacerts -storepass changeit -alias c2-ca
    c2-ca, Dec 14, 2023, trustedCertEntry,
    Certificate fingerprint (SHA-256): 64:1B:BC:79:EE:0A:37:EA:3D:AC:B9:E3:E5:7F:02:71:3E:7D:C8:82:89:62:C5:E8:11:75:45:37:58:18:71:6F

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>javac HttpsConnectionTest.java

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>java HttpsConnectionTest https://www.google.nl
    Response Code: 200
    Response Message: OK

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>java HttpsConnectionTest https://c2platform.org
    Response Code: 200
    Response Message: OK

    vagrant@GSD-TOMCAT2 C:\vagrant\scripts\gsd-tomcat>java HttpsConnectionTest https://gsd-tomcat2.internal.c2platform.org:8443/helloworld/
    Response Code: 200
    Response Message: null

    ```

    </details>

Alternatief, voor `gsd-tomcat2`, aangezien dit een Windows-node is, kunt u ook een RDP-verbinding gebruiken:

1. Start RDP-verbinding naar `1.1.8.207` login als `vagrant` met wachtwoord `vagrant`
2. Gebruik de Windows Command Prompt shortcut op het bureaublad genaamd **Scripts** en voer het script `test-https.bat` uit. Hiermee wordt een eenvoudige Java-testklasse gecompileerd die vervolgens wordt gebruikt om te controleren of de Java TrustStore correct is bijgewerkt met het CA-certificaat. Als je dit script uitvoert, mogen er geen foutmeldingen verschijnen en alleen een response code van 200 die succes aangeeft.
    <details>
    <summary><kbd>Toon me</kbd></summary>

    ```
    c:\vagrant\scripts\gsd-tomcat>test-https.bat

    c:\vagrant\scripts\gsd-tomcat>set JAVA_HOME=D:\Apps\jdk-17.0.5\jdk-17.0.5+8

    c:\vagrant\scripts\gsd-tomcat>set PATH=C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Windows\System32\OpenSSH\;C:\ProgramData\chocolatey\bin;C:\Program Files\PowerShell\7\;C:\Program Files\OpenSSH;C:\Program Files\Git\cmd;%USERPROFILE%\AppData\Local\Microsoft\WindowsApps;%JAVA_HOME%\bin;D:\Apps\jdk-17.0.5\jdk-17.0.5+8\bin;D:\Apps\jdk-17.0.5\jdk-17.0.5+8\bin;D:\Apps\jdk-17.0.5\jdk-17.0.5+8\bin

    c:\vagrant\scripts\gsd-tomcat>keytool -list -cacerts -storepass changeit -alias google
    google, Dec 14, 2023, trustedCertEntry,
    Certificate fingerprint (SHA-256): 98:2D:C0:52:3D:84:7D:B5:89:AE:22:3E:DD:D8:37:86:72:79:D7:32:40:D6:03:55:D8:A6:F1:70:53:DF:48:95

    c:\vagrant\scripts\gsd-tomcat>keytool -list -cacerts -storepass changeit -alias c2-ca
    c2-ca, Dec 14, 2023, trustedCertEntry,
    Certificate fingerprint (SHA-256): 64:1B:BC:79:EE:0A:37:EA:3D:AC:B9:E3:E5:7F:02:71:3E:7D:C8:82:89:62:C5:E8:11:75:45:37:58:18:71:6F

    c:\vagrant\scripts\gsd-tomcat>javac HttpsConnectionTest.java

    c:\vagrant\scripts\gsd-tomcat>java HttpsConnectionTest https://www.google.nl
    Response Code: 200
    Response Message: OK

    c:\vagrant\scripts\gsd-tomcat>java HttpsConnectionTest https://c2platform.org
    Response Code: 200
    Response Message: OK

    c:\vagrant\scripts\gsd-tomcat>java HttpsConnectionTest https://gsd-tomcat2.internal.c2platform.org:8443/helloworld/
    Response Code: 200
    Response Message: null

    c:\vagrant\scripts\gsd-tomcat>
    ```

    </details>

## Herzien

Met betrekking tot de Tomcat-nodes `gsd-tomcat1`, `gsd-tomcat2` en de CA-server `gsd-rproxy1` let op de volgende bestanden.

|Node         |Project                                                         |Speel                |Bestand                 |
|-------------|---------------------------------------------------------------------------------------|---------------------|-------------------------|
|`gsd-rproxy1`|[GIS Platform]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})|`plays/mgmt/cacerts_server.yml`|`group_vars/all/certs.yml`|
|`gsd-tomcat1`|[GIS Platform]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})|`plays/mw/tomcat.yml`|`group_vars/tomcat_linux/certs.yml`|
|`gsd-tomcat2`|[GIS Platform]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})|`plays/mw/tomcat.yml`|`group_vars/tomcat_win/certs.yml`  |

### Node `gsd-tomcat1`

Voor node `gsd-tomcat1` is de configuratie `group_vars/tomcat_linux/certs.yml`

```yaml
---
tomcat_cacerts2_certificates:
  - common_name: "{{ inventory_hostname }}"  # alias
    country_name: NL
    locality_name: Den Haag
    organization_name: C2
    organizational_unit_name: C2 GIS Platform Team
    state_or_province_name: Zuid-Holland
    subject_alt_name:
      - "DNS:{{ inventory_hostname }}"
      - "DNS:{{ inventory_hostname }}.internal.c2platform.org"
      - "IP:{{ ansible_eth1.ipv4.address }}"
    ansible_group: tomcat
    deploy:
      keystore:
        dir: "{{ tomcat_home_version }}/conf"
        password: geheim
        owner: "{{ tomcat_user }}"
        group: "{{ tomcat_group }}"
        notify: herstart tomcat instance
      truststore:
        dest: "{{ java_version | c2platform.core.java_home }}/lib/security/cacerts"
        sites:
          - name: www.google.com
            port: 443
            alias: google
```

Deze configuratie resulteert in de creatie van een bestand met de naam `gsd-tomcat1-gsd-tomcat1.keystore` op de CA-server `gsd-rproxy1`.

```bash
vagrant@gsd-rproxy1:~$ ls /vagrant/.ca/c2/tomcat/ | grep tomcat1
gsd-tomcat1-gsd-tomcat1.crt
gsd-tomcat1-gsd-tomcat1.csr
gsd-tomcat1-gsd-tomcat1.key
gsd-tomcat1-gsd-tomcat1.keystore
gsd-tomcat1-gsd-tomcat1.p12
gsd-tomcat1-gsd-tomcat1.pem
gsd-tomcat1-gsd-tomcat1.truststore
```

Dit bestand wordt vervolgens gedistribueerd naar de `gsd-tomcat1 node`, zoals blijkt uit de volgende directorylijst:

```bash
root@gsd-tomcat1:~# ls /opt/tomcat/tomcat/conf/
Catalina             gsd-tomcat1-gsd-tomcat1.keystore  jmxremote.password  tomcat-users.xsd
catalina.policy      jaspic-providers.xml              logging.properties  web.xml
catalina.properties  jaspic-providers.xsd              server.xml
context.xml          jmxremote.access                  tomcat-users.xml
root@gsd-tomcat1:~#
```

Deze configuratie werkt ook de Java TrustStore bij / beheert deze. Met behulp van `keytool` kunt u verifiëren dat het certificaat van {{< external-link url="ttps://www.google.com:443" htmlproofer_ignore="false" >}} en het C2 Root CA-certificaat is geïmporteerd in de Java TrustStore.

```bash
keytool -list -cacerts -storepass changeit -alias google
keytool -list -cacerts -storepass changeit -alias c2-ca
```

<details>
  <summary><kbd>Toon me</kbd></summary>

```bash
root@gsd-tomcat1:~# keytool -list -cacerts -storepass changeit -alias google
google, Dec 14, 2023, trustedCertEntry,
Certificate fingerprint (SHA-256): 98:2D:C0:52:3D:84:7D:B5:89:AE:22:3E:DD:D8:37:86:72:79:D7:32:40:D6:03:55:D8:A6:F1:70:53:DF:48:95
root@gsd-tomcat1:~# keytool -list -cacerts -storepass changeit -alias c2-ca
c2-ca, Dec 14, 2023, trustedCertEntry,
Certificate fingerprint (SHA-256): 64:1B:BC:79:EE:0A:37:EA:3D:AC:B9:E3:E5:7F:02:71:3E:7D:C8:82:89:62:C5:E8:11:75:45:37:58:18:71:6F
root@gsd-tomcat1:~#

```

</details>

### Node `gsd-tomcat2`

Voor node `gsd-tomcat2` is de configuratie `group_vars/tomcat_win/certs.yml`

```yaml
tomcat_cacerts2_certificates:
  - common_name: "{{ inventory_hostname }}"  # alias
    country_name: NL
    locality_name: Den Haag
    organization_name: C2
    organizational_unit_name: C2 GIS Platform Team
    state_or_province_name: Zuid-Holland
    subject_alt_name:
      - "DNS:{{ inventory_hostname }}"
      - "DNS:{{ inventory_hostname }}.internal.c2platform.org"
      - "DNS:{{ inventory_hostname }}.ad.c2platform.org"
      - "IP:{{ ansible_host }}"
    ansible_group: tomcat
    deploy:
      keystore:
        dir: "{{ tomcat_home }}/conf"
        password: geheim
        notify: Herstart Tomcat-service
      truststore:
        dest: "{{ java_install_dir }}/jdk-17.0.5+8/lib/security/cacerts"
        sites:
          - name: www.google.com
            port: 443
            alias: google
```

Deze configuratie resulteert in de creatie van een bestand met de naam `gsd-tomcat2-GSD-TOMCAT2.keystore` op de CA-server `gsd-rproxy1`.

<details>
  <summary><kbd>Toon me</kbd></summary>

```bash
root@gsd-rproxy1:~# ls /vagrant/.ca/c2/tomcat/ | grep tomcat2
gsd-tomcat2-GSD-TOMCAT2.crt
gsd-tomcat2-GSD-TOMCAT2.csr
gsd-tomcat2-GSD-TOMCAT2.key
gsd-tomcat2-GSD-TOMCAT2.keystore
gsd-tomcat2-GSD-TOMCAT2.p12
gsd-tomcat2-GSD-TOMCAT2.pem
gsd-tomcat2-GSD-TOMCAT2.truststore
root@gsd-rproxy1:~#
```

</details>

Dit wordt geïmplementeerd naar de directory `d:\Apps\tomcat\conf` op `gsd-tomcat2`.

<details>
  <summary><kbd>Toon me</kbd></summary>

```
Microsoft Windows [Version 10.0.20348.707]
(c) Microsoft Corporation. Alle rechten voorbehouden.

vagrant@GSD-TOMCAT2 C:\Users\vagrant>dir d:\Apps\tomcat\conf
 Volume in drive D is Data
 Volume Serial Number is DCAD-2354

 Directory of d:\Apps\tomcat\conf

12/15/2023  03:15 AM    <DIR>          .
12/15/2023  03:14 AM    <DIR>          ..
12/15/2023  03:15 AM    <DIR>          Catalina
07/04/2023  01:15 PM            13,216 catalina.policy
07/04/2023  01:15 PM             7,849 catalina.properties
07/04/2023  01:15 PM             1,431 context.xml
12/15/2023  03:15 AM             4,530 gsd-tomcat2-GSD-TOMCAT2.keystore
07/04/2023  01:15 PM             1,172 jaspic-providers.xml
07/04/2023  01:15 PM             2,365 jaspic-providers.xsd
07/04/2023  01:15 PM             4,223 logging.properties
12/15/2023  03:15 AM             2,508 server.xml
12/15/2023  03:14 AM             8,034 server.xml.3200.20231215-031501.bak
12/15/2023  03:14 AM             2,813 tomcat-users.xml
07/04/2023  01:15 PM             2,617 tomcat-users.xsd
07/04/2023  01:15 PM           177,316 web.xml
              12 File(s)        228,074 bytes
               3 Dir(s)  321,481,920,512 bytes free

vagrant@GSD-TOMCAT2 C:\Users\vagrant>

```

</details>

## RWS Certificaat Implementeren

In deze sectie gaan we de implementatie van een officieel / extern RWS-certificaat simuleren.

1. Zoek en sla de **vingerafdruk** van het huidige certificaat op:
    ```bash
    vagrant ssh gsd-tomcat1
    ```
    ```bash
    echo -n | openssl s_client -connect gsd-tomcat1.internal.c2platform.org:8443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/gsd-tomcat1.pem
    openssl x509 -noout -fingerprint -sha256 -inform pem -in /tmp/gsd-tomcat1.pem
    ```

    <details>
    <summary><kbd>Toon me</kbd></summary>

    ```bash
    vagrant@gsd-tomcat1:~$ echo -n | openssl s_client -connect gsd-tomcat1.internal.c2platform.org:8443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/gsd-tomcat1.pem
    depth=1 CN = c2
    verify return:1
    depth=0 C = NL, ST = Zuid-Holland, L = Den Haag, O = C2, OU = C2 GIS Platform Team, CN = gsd-tomcat1
    verify return:1
    DONE
    vagrant@gsd-tomcat1:~$ openssl x509 -noout -fingerprint -sha256 -inform pem -in /tmp/gsd-tomcat1.pem
    sha256 Fingerprint=7B:63:B1:A0:F8:DC:CA:6F:14:92:A8:F5:A1:43:92:DD:1D:44:8F:90:BE:77:E4:96:08:D4:A7:84:F9:3D:23:05
    vagrant@gsd-tomcat1:~$
    ```

    </details>
2.  We gaan het zelfondertekende certificaat `gsd-tomcat1-gsd-tomcat1.crt` verwijderen door het te hernoemen naar `gsd-tomcat1-gsd-tomcat1.crt.saved`. In latere tests behandelen we het als een certificaat van een externe CSP. Voer op de host in de root van het project uit:

    ```bash
    sudo chown $USER:$USER .ca/c2/tomcat/*
    sudo mv .ca/c2/tomcat/gsd-tomcat1-gsd-tomcat1.crt .ca/c2/tomcat/gsd-tomcat1-gsd-tomcat1.crt.saved
    ```

    Provision `gsd-tomcat1`

    ```bash
    vagrant provision gsd-tomcat1 | tee provision.log
    ```

    Controleer het `provision.log`-bestand waar u zou moeten zien dat een nieuw SSL/TLS-certificaat, een nieuwe Java KeyStore zijn gemaakt en gedistribueerd, en dat deze wijzigingen een herstart van de Tomcat-services triggerden. Als je de opdrachten uitvoert om de vingerafdruk te controleren, zou je nu een andere waarde moeten zien. Dit geeft aan dat de Tomcat-service inderdaad een ander certificaat gebruikt.

    <details>
      <summary><kbd>Toon me</kbd></summary>

    ```bash
    ==> gsd-tomcat1: Running provisioner: shell...
        gsd-tomcat1: Running: inline script
    ==> gsd-tomcat1: Running provisioner: ansible...
        gsd-tomcat1: Running ansible-playbook...
    [DEPRECATION WARNING]: "include" is deprecated, use include_tasks/import_tasks
    instead. See https://docs.ansible.com/ansible-
    core/2.15/user_guide/playbooks_reuse_includes.html for details. This feature
    will be removed in version 2.16. Deprecation warnings can be disabled by
    setting deprecation_warnings=False in ansible.cfg.

    PLAY [Tomcat] ******************************************************************

    TASK [Gathering Facts] *********************************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.server_update : include_tasks] ***************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/server_update/tasks/update_cache.yml for gsd-tomcat1

    TASK [c2platform.core.server_update : Apt update cache] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : include_tasks] *******************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=python3-pip)
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=upgrade pip and setuptools)
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=['setuptools', 'pyOpenSSL==22.0.0', 'psycopg2-binary', 'lxml', 'cryptography==38.0.4'])
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=['nano', 'wget', 'tree', 'unzip', 'zip', 'jq', 'build-essential', 'python3-dev', 'python3-wheel', 'libsasl2-dev', 'libldap2-dev', 'libssl-dev', 'git', 'nfs-common', 'net-tools', 'telnet', 'curl', 'dnsutils', 'python2'])

    TASK [c2platform.core.bootstrap : OS package] **********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : Custom command upgrade pip and setuptools] ***
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : PIP package] *********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : OS package] **********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.os_trusts : CA distribute ( Debian )] ********************
    ok: [gsd-tomcat1] => (item=https://letsencrypt.org/certs/isrgrootx1.pem)
    ok: [gsd-tomcat1] => (item=file:///vagrant/.ca/c2/c2.crt)

    TASK [c2platform.core.secrets : Stat secret dir] *******************************
    ok: [gsd-tomcat1 -> localhost] => (item=/home/ostraaten/git/gitlab/c2/ansible-gis/secret_vars/development)
    ok: [gsd-tomcat1 -> localhost] => (item=/runner/project/secret_vars/development)

    TASK [c2platform.core.secrets : Include secrets] *******************************
    ok: [gsd-tomcat1] => (item=/home/ostraaten/git/gitlab/c2/ansible-gis/secret_vars/development)

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files.yml for gsd-tomcat1 => (item=marker)

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files_src.yml for gsd-tomcat1

    TASK [c2platform.core.files : Link] ********************************************
    ok: [gsd-tomcat1] => (item=/usr/bin/python → /usr/bin/python2)

    TASK [c2platform.core.files : Lineinfile] **************************************
    ok: [gsd-tomcat1] => (item=/home/vagrant/.bashrc)
    ok: [gsd-tomcat1] => (item=/root/.bashrc)

    TASK [c2platform.core.lcm : Ansible roles names without prefix] ****************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lcm : lcm_roles_node] ************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lvm : Get disks] *****************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lvm : Get usable devices] ********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lvm : Get nodes lvm roles] *******************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.hosts : Set up /etc/hostname.] ***************************
    ok: [gsd-tomcat1] => (item=/home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/hosts/templates/../templates/hostname.j2)

    TASK [c2platform.core.hosts : Set up /etc/hosts.] ******************************
    ok: [gsd-tomcat1] => (item=/home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/hosts/templates/../templates/hosts.j2)

    TASK [c2platform.core.alias : Set fact node roles] *****************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.common : Remove custom prompt] ***************************
    ok: [gsd-tomcat1]

    TASK [Include role(s)] *********************************************************

    TASK [c2platform.core.java : Set additional java facts] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.java : Install Java] *************************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/java/tasks/install.yml for gsd-tomcat1 => (item=jdk17)

    TASK [c2platform.core.java : Check installed] **********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.java : Create etc profile] *******************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : test if tomcat_versions is set correctly] *********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_home is set correctly] *************
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_version is set correctly] **********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_user is set correctly] *************
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_group is set correctly] ************
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_catalina_opts is set correctly] ****
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_non_ssl_connector_port is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_ssl_connector_port is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_shutdown_port is set correctly] ****
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_ajp_port is set correctly] *********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if port collisions occur is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_jre_home is set correctly] *********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_service_state is set correctly] ****
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_service_enabled is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_address is set correctly] **********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_enabled is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_directory is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_prefix is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_suffix is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_pattern is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : Group] ********************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : User] *********************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Home directory] ***********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Create home link] *********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Stat bin dir] *************************************
    ok: [gsd-tomcat1]

    TASK [Include certs tasks] *****************************************************

    TASK [c2platform.core.cacerts2 : Set fact cacerts2_certificates] ***************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : cacerts2_certificates] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : Set various certificate facts] ****************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : Set fact cacerts2_certificates] ***************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : include_tasks] ********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/certs_group.yml for gsd-tomcat1

    TASK [c2platform.core.cacerts2 : include_tasks] ********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/cert.yml for gsd-tomcat1 => (item=gsd-tomcat1)

    TASK [c2platform.core.cacerts2 : Stat key] *************************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Stat crt] *************************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Stat dir] *************************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate an OpenSSL private key] **************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate an OpenSSL Certificate Signing Request] ***
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate an OpenSSL certificate] **************
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate pkcs12 file] *************************
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Include Java KeyStore tasks] ******************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/java_keystore.yml for gsd-tomcat1

    TASK [c2platform.core.cacerts2 : Stat KeyStore] ********************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Manage Java KeyStore] *************************
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Create PEM file] ******************************
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : include_tasks] ********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/cert_deploy.yml for gsd-tomcat1 => (item=gsd-tomcat1)

    TASK [c2platform.core.cacerts2 : Copy to control node ( fetch )] ***************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)] => (item=key)
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)] => (item=crt)
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)] => (item=keystore)

    TASK [c2platform.core.cacerts2 : Stat parent dir] ******************************
    ok: [gsd-tomcat1] => (item=key)
    ok: [gsd-tomcat1] => (item=crt)
    ok: [gsd-tomcat1] => (item=keystore)

    TASK [c2platform.core.cacerts2 : Copy file] ************************************
    ok: [gsd-tomcat1] => (item=key)
    changed: [gsd-tomcat1] => (item=crt)
    changed: [gsd-tomcat1] => (item=keystore)

    TASK [c2platform.mw.tomcat : Configure conf/server.xml] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Configure conf/tomcat-users.xml] ******************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Configure bin/setenv.sh] **************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Stat manager] *************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Configure webapps/manager/META-INF/context.xml] ***
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Properties folder] ********************************
    ok: [gsd-tomcat1] => (item=/opt/tomcat/tomcat/conf)

    TASK [c2platform.mw.tomcat : Deploy WAR] ***************************************
    ok: [gsd-tomcat1] => (item=https://github.com/aeimer/java-example-helloworld-war/raw/master/dist/helloworld.war → helloworld.war)

    TASK [Files] *******************************************************************

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files.yml for gsd-tomcat1 => (item=jmx)

    TASK [c2platform.core.files : Stat no update files] ****************************
    ok: [gsd-tomcat1] => (item=/opt/tomcat/tomcat/conf/jmxremote.password)

    TASK [c2platform.core.files : Content file] ************************************
    ok: [gsd-tomcat1] => (item=/opt/tomcat/tomcat/conf/jmxremote.access)

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files_src.yml for gsd-tomcat1

    TASK [Service] *****************************************************************

    TASK [c2platform.core.service : Fact _services] ********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.service : Configure systemd service] *********************
    ok: [gsd-tomcat1] => (item=tomcat)

    TASK [c2platform.mw.tomcat : Manage service] ***********************************
    ok: [gsd-tomcat1]

    TASK [copy] ********************************************************************
    ok: [gsd-tomcat1]

    RUNNING HANDLER [c2platform.mw.tomcat : restart tomcat instance] ***************
    changed: [gsd-tomcat1]

    PLAY RECAP *********************************************************************
    gsd-tomcat1                : ok=93   changed=7    unreachable=0    failed=0    skipped=86   rescued=0    ignored=0
    ```

    </details>

3. Implementeer het externe certificaat

   Simuleer de handmatige download
   ```bash
   sudo mv .ca/c2/tomcat/gsd-tomcat1-gsd-tomcat1.crt.saved .ca/c2/tomcat/gsd-tomcat1-gsd-tomcat1.rws-csp.crt
   ```

   {{< alert title="Let op:" >}}De string **`rws-csp`** is een speciale tag die aangeeft dat dit een certificaat van een externe / handmatige CSP is{{< /alert >}}

    ```bash
    vagrant provision gsd-tomcat1 | tee provision.log
    ```

    Let op de veranderingen in het bestand `provision.log`

    1. Genereer pkcs12-bestand
    2. Beheer Java KeyStore
    3. Kopieer naar controlenode (fetch)

    <details>
    <summary><kbd>Toon me</kbd></summary>

    ```bash
    ==> gsd-tomcat1: Running provisioner: shell...
        gsd-tomcat1: Running: inline script
    ==> gsd-tomcat1: Running provisioner: ansible...
        gsd-tomcat1: Running ansible-playbook...
    [DEPRECATION WARNING]: "include" is deprecated, use include_tasks/import_tasks
    instead. See https://docs.ansible.com/ansible-
    core/2.15/user_guide/playbooks_reuse_includes.html for details. This feature
    will be removed in version 2.16. Deprecation warnings can be disabled by
    setting deprecation_warnings=False in ansible.cfg.

    PLAY [Tomcat] ******************************************************************

    TASK [Gathering Facts] *********************************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.server_update : include_tasks] ***************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/server_update/tasks/update_cache.yml for gsd-tomcat1

    TASK [c2platform.core.server_update : Apt update cache] ************************
    changed: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : include_tasks] *******************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=python3-pip)
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=upgrade pip and setuptools)
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=['setuptools', 'pyOpenSSL==22.0.0', 'psycopg2-binary', 'lxml', 'cryptography==38.0.4'])
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/bootstrap/tasks/package.yml for gsd-tomcat1 => (item=['nano', 'wget', 'tree', 'unzip', 'zip', 'jq', 'build-essential', 'python3-dev', 'python3-wheel', 'libsasl2-dev', 'libldap2-dev', 'libssl-dev', 'git', 'nfs-common', 'net-tools', 'telnet', 'curl', 'dnsutils', 'python2'])

    TASK [c2platform.core.bootstrap : OS package] **********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : Custom command upgrade pip and setuptools] ***
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : PIP package] *********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.bootstrap : OS package] **********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.os_trusts : CA distribute ( Debian )] ********************
    ok: [gsd-tomcat1] => (item=https://letsencrypt.org/certs/isrgrootx1.pem)
    ok: [gsd-tomcat1] => (item=file:///vagrant/.ca/c2/c2.crt)

    TASK [c2platform.core.secrets : Stat secret dir] *******************************
    ok: [gsd-tomcat1 -> localhost] => (item=/home/ostraaten/git/gitlab/c2/ansible-gis/secret_vars/development)
    ok: [gsd-tomcat1 -> localhost] => (item=/runner/project/secret_vars/development)

    TASK [c2platform.core.secrets : Include secrets] *******************************
    ok: [gsd-tomcat1] => (item=/home/ostraaten/git/gitlab/c2/ansible-gis/secret_vars/development)

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files.yml for gsd-tomcat1 => (item=marker)

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files_src.yml for gsd-tomcat1

    TASK [c2platform.core.files : Link] ********************************************
    ok: [gsd-tomcat1] => (item=/usr/bin/python → /usr/bin/python2)

    TASK [c2platform.core.files : Lineinfile] **************************************
    ok: [gsd-tomcat1] => (item=/home/vagrant/.bashrc)
    ok: [gsd-tomcat1] => (item=/root/.bashrc)

    TASK [c2platform.core.lcm : Ansible roles names without prefix] ****************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lcm : lcm_roles_node] ************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lvm : Get disks] *****************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lvm : Get usable devices] ********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.lvm : Get nodes lvm roles] *******************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.hosts : Set up /etc/hostname.] ***************************
    ok: [gsd-tomcat1] => (item=/home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/hosts/templates/../templates/hostname.j2)

    TASK [c2platform.core.hosts : Set up /etc/hosts.] ******************************
    ok: [gsd-tomcat1] => (item=/home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/hosts/templates/../templates/hosts.j2)

    TASK [c2platform.core.alias : Set fact node roles] *****************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.common : Remove custom prompt] ***************************
    ok: [gsd-tomcat1]

    TASK [Include role(s)] *********************************************************

    TASK [c2platform.core.java : Set additional java facts] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.java : Install Java] *************************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/java/tasks/install.yml for gsd-tomcat1 => (item=jdk17)

    TASK [c2platform.core.java : Check installed] **********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.java : Create etc profile] *******************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : test if tomcat_versions is set correctly] *********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_home is set correctly] *************
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_version is set correctly] **********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_user is set correctly] *************
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_group is set correctly] ************
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_catalina_opts is set correctly] ****
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_non_ssl_connector_port is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_ssl_connector_port is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_shutdown_port is set correctly] ****
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_ajp_port is set correctly] *********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if port collisions occur is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_jre_home is set correctly] *********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_service_state is set correctly] ****
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_service_enabled is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_address is set correctly] **********
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_enabled is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_directory is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_prefix is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_suffix is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : test if tomcat_access_log_pattern is set correctly] ***
    ok: [gsd-tomcat1 -> localhost]

    TASK [c2platform.mw.tomcat : Group] ********************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : User] *********************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Home directory] ***********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Create home link] *********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Stat bin dir] *************************************
    ok: [gsd-tomcat1]

    TASK [Include certs tasks] *****************************************************

    TASK [c2platform.core.cacerts2 : Set fact cacerts2_certificates] ***************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : cacerts2_certificates] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : Set various certificate facts] ****************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : Set fact cacerts2_certificates] ***************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.cacerts2 : include_tasks] ********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/certs_group.yml for gsd-tomcat1

    TASK [c2platform.core.cacerts2 : include_tasks] ********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/cert.yml for gsd-tomcat1 => (item=gsd-tomcat1)

    TASK [c2platform.core.cacerts2 : Stat key] *************************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Stat crt] *************************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Stat dir] *************************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate an OpenSSL private key] **************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate an OpenSSL Certificate Signing Request] ***
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate an OpenSSL certificate] **************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Generate pkcs12 file] *************************
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Include Java KeyStore tasks] ******************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/java_keystore.yml for gsd-tomcat1

    TASK [c2platform.core.cacerts2 : Stat KeyStore] ********************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Manage Java KeyStore] *************************
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : Create PEM file] ******************************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)]

    TASK [c2platform.core.cacerts2 : include_tasks] ********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/cacerts2/tasks/certs/cert_deploy.yml for gsd-tomcat1 => (item=gsd-tomcat1)

    TASK [c2platform.core.cacerts2 : Copy to control node ( fetch )] ***************
    ok: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)] => (item=key)
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)] => (item=crt)
    changed: [gsd-tomcat1 -> gsd-rproxy1(1.1.5.205)] => (item=keystore)

    TASK [c2platform.core.cacerts2 : Stat parent dir] ******************************
    ok: [gsd-tomcat1] => (item=key)
    ok: [gsd-tomcat1] => (item=crt)
    ok: [gsd-tomcat1] => (item=keystore)

    TASK [c2platform.core.cacerts2 : Copy file] ************************************
    ok: [gsd-tomcat1] => (item=key)
    changed: [gsd-tomcat1] => (item=crt)
    changed: [gsd-tomcat1] => (item=keystore)

    TASK [c2platform.mw.tomcat : Configure conf/server.xml] ************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Configure conf/tomcat-users.xml] ******************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Configure bin/setenv.sh] **************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Stat manager] *************************************
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Configure webapps/manager/META-INF/context.xml] ***
    ok: [gsd-tomcat1]

    TASK [c2platform.mw.tomcat : Properties folder] ********************************
    ok: [gsd-tomcat1] => (item=/opt/tomcat/tomcat/conf)

    TASK [c2platform.mw.tomcat : Deploy WAR] ***************************************
    ok: [gsd-tomcat1] => (item=https://github.com/aeimer/java-example-helloworld-war/raw/master/dist/helloworld.war → helloworld.war)

    TASK [Files] *******************************************************************

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files.yml for gsd-tomcat1 => (item=jmx)

    TASK [c2platform.core.files : Stat no update files] ****************************
    ok: [gsd-tomcat1] => (item=/opt/tomcat/tomcat/conf/jmxremote.password)

    TASK [c2platform.core.files : Content file] ************************************
    ok: [gsd-tomcat1] => (item=/opt/tomcat/tomcat/conf/jmxremote.access)

    TASK [c2platform.core.files : include_tasks] ***********************************
    included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/files/tasks/files_src.yml for gsd-tomcat1

    TASK [Service] *****************************************************************

    TASK [c2platform.core.service : Fact _services] ********************************
    ok: [gsd-tomcat1]

    TASK [c2platform.core.service : Configure systemd service] *********************
    ok: [gsd-tomcat1] => (item=tomcat)

    TASK [c2platform.mw.tomcat : Manage service] ***********************************
    ok: [gsd-tomcat1]

    TASK [copy] ********************************************************************
    changed: [gsd-tomcat1]

    RUNNING HANDLER [c2platform.mw.tomcat : restart tomcat instance] ***************
    changed: [gsd-tomcat1]

    PLAY RECAP *********************************************************************
    gsd-tomcat1                : ok=93   changed=7    unreachable=0    failed=0    skipped=86   rescued=0    ignored=0
    ```

    </details>

4. Verifieer dat het geïmplementeerde certificaat het juiste / nieuwe certificaat is.

    <details>
    <summary><kbd>Toon me</kbd></summary>

    ```bash
    vagrant@gsd-tomcat1:~$     echo -n | openssl s_client -connect gsd-tomcat1.internal.c2platform.org:8443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/gsd-tomcat1.pem
        openssl x509 -noout -fingerprint -sha256 -inform pem -in /tmp/gsd-tomcat1.pem
    depth=1 CN = c2
    verify return:1
    depth=0 C = NL, ST = Zuid-Holland, L = Den Haag, O = C2, OU = C2 GIS Platform Team, CN = gsd-tomcat1
    verify return:1
    DONE
    sha256 Fingerprint=B8:65:69:8B:8B:DF:94:5F:37:5C:CA:0F:04:EE:32:D2:2E:67:4D:04:D0:DF:38:1F:4F:DB:B6:54:02:78:63:B3
    vagrant@gsd-tomcat1:~$ openssl x509 -noout -fingerprint -sha256 -inform pem -in /vagrant/.ca/c2/tomcat/gsd-tomcat1-gsd-tomcat1.
    gsd-tomcat1-gsd-tomcat1.crt          gsd-tomcat1-gsd-tomcat1.keystore     gsd-tomcat1-gsd-tomcat1.rws-csp.crt
    gsd-tomcat1-gsd-tomcat1.csr          gsd-tomcat1-gsd-tomcat1.p12
    gsd-tomcat1-gsd-tomcat1.key          gsd-tomcat1-gsd-tomcat1.pem
    vagrant@gsd-tomcat1:~$ openssl x509 -noout -fingerprint -sha256 -inform pem -in /vagrant/.ca/c2/tomcat/gsd-tomcat1-gsd-tomcat1.rws-csp.crt
    sha256 Fingerprint=B8:65:69:8B:8B:DF:94:5F:37:5C:CA:0F:04:EE:32:D2:2E:67:4D:04:D0:DF:38:1F:4F:DB:B6:54:02:78:63:B3
    vagrant@gsd-tomcat1:~$
    ```

    </details>

## Aanvullende Informatie

Voor aanvullende inzichten en richtlijnen:

* Verken het ontwerp en de voordelen van deze setup in [Implementatie van PKI voor RWS GIS met Ansible]({{< relref path="/docs/projects/rws/pki" >}}).
* Bij RWS maakt de CA-server gebruik van een CIFS-share voor het opslaan van certificaten, wat enkele aanvullende handmatige stappen creëert omdat dit niet wordt ondersteund in Ansible Core Utility-modules, zie [Maak een eenvoudige CA-server met Ansible]({< relref path="/docs/howto/rws/ca" >}}).