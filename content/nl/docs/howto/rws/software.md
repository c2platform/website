---
categories: ["Handleiding"]
tags: [ansible, software, nexus, ca, ssl/tls, windows, trust]
title: "Maak een Eenvoudige Software Repository voor Ansible"
linkTitle: "Software Repository"
provisionTime: 25 minuten
weight: 11
time: true
description: >
   Deze gids demonstreert hoe je een eenvoudige software repository kunt opzetten
   met Ansible, die zal dienen als een gecentraliseerde bron voor software
   distributie.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}})

---
## Overzicht

Deze handleiding schetst het proces dat door RWS is aangenomen voor het beheren van softwaredownloads met Ansible. Het volgt de strategie beschreven in [Ontwerp van een Flexibele Software Repository voor Ansible]({{< relref path="/docs/projects/rws/software" >}}) en maakt gebruik van de `c2platform.wincore.download` rol voor downloadbewerkingen. De installatie omvat Vagrant en Ansible die de volgende taken uitvoeren:

1. Vagrant gebruikt de VirtualBox Provider om vijf VM's te creëren zoals gespecificeerd in de onderstaande tabel.
2. Vagrant organiseert met behulp van de Ansible Provisioner een Ansible playbook over deze VM's om:
   1. Een Windows Bestand Share op `gsd-ansible-file-share1` op te zetten, gevuld met een Tomcat tarball en een zip-bestand.
   2. Een softwaredienst, `gsd-ansible-repo`, opzetten die de file share aankoppelt en de inhoud ervan host via Apache2.
   3. Gelijktijdige downloads starten op drie nodes (`gsd-ansible-download1`, `gsd-ansible-download2`, `gsd-ansible-download3`), met gebruik van HTTPS. Gedeelde opslag fungeert als cache, mogelijk gemaakt door `gsd-ansible-file-share1`. De `c2platform.wincore.download` rol zorgt ervoor dat downloads gesynchroniseerd zijn, waarbij slechts één node een bestand op een bepaald moment downloadt, bepaald door welke node het eerst een lock bestand verkrijgt. Er zijn twee downloads, die mogelijk door verschillende nodes worden afgehandeld.

| Node                      | OS                  | Provider   | Doel                                               |
|---------------------------|---------------------|------------|----------------------------------------------------|
| `gsd-ansible-file-share1` | Windows 2022 Server | VirtualBox | Hostt file share voor opslag en caching downloads  |
| `gsd-ansible-repo`        | Red Hat 9           | VirtualBox | Eenvoudige webgebaseerde software server met Apache2 |
| `gsd-ansible-download1`   | Windows 2022 Server | VirtualBox | Voert test downloads uit                            |
| `gsd-ansible-download2`   | Windows 2022 Server | VirtualBox | Voert test downloads uit                            |
| `gsd-ansible-download3`   | Windows 2022 Server | VirtualBox | Voert test downloads uit                            |

## Vereisten

* Zorg ervoor dat je RWS Ontwikkelingsomgeving is ingesteld op Ubuntu 22, zoals beschreven [hier]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}).

## Setup

Om de volledige omgeving voor te bereiden, voer je vagrant up uit voor de opgegeven VM's. Op een [high-performance ontwikkelwerkstation]({{< relref path="/docs/concepts/dev/laptop" >}}), zou deze setup ongeveer 25 minuten moeten duren.

```bash
export BOX="gsd-ansible-repo gsd-ansible-file-share1 gsd-ansible-download1 \
gsd-ansible-download2 gsd-ansible-download3"
vagrant up $BOX

```

## Verificatie

1. Succesvolle uitvoering van het playbook geeft aan dat de download nodes (`gsd-ansible-download1`, `gsd-ansible-download2`, `gsd-ansible-download3`) erin zijn geslaagd om de Tomcat binaries te verkrijgen. Bekijk de laatste console-uitvoer van Ansible, met taken zoals **Create download lock**, **Download install file**, **Extract zip** en **Remove lock file**, om te bepalen welke node met succes de lock heeft verkregen en de download heeft voltooid.

   <p><details>
   <summary><kbd>Toon mij</kbd></summary><p>

   ```bash
   TASK [c2platform.wincore.download : Create download lock] **********************
   changed: [gsd-ansible-download1] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.tar.gz)
   ok: [gsd-ansible-download2] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.tar.gz)
   ok: [gsd-ansible-download3] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.tar.gz)
   changed: [gsd-ansible-download1] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.zip)
   ok: [gsd-ansible-download2] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.zip)
   ok: [gsd-ansible-download3] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.zip)

   TASK [c2platform.wincore.download : Download install file] *********************
   changed: [gsd-ansible-download1] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.tar.gz)
   changed: [gsd-ansible-download1] => (item=//gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19.zip)

   TASK [c2platform.wincore.download : Extract zip] *******************************
   changed: [gsd-ansible-download1] => (item=apache-tomcat-10.1.19.zip → //gsd-ansible-file-share1.internal.c2platform.org/ansible-repo/cache\apache-tomcat-10.1.19)

   TASK [c2platform.wincore.download : Remove lock file] **************************
   changed: [gsd-ansible-download1] => (item=\\gsd-ansible-file-share1.internal.c2platform.org\ansible-repo\cache\e9e4e25175d60ab7bf720609971f81206c644ce950deed17bf6de78613cca091.lock → absent)
   changed: [gsd-ansible-download1] => (item=\\gsd-ansible-file-share1.internal.c2platform.org\ansible-repo\cache\4c6082b852ad235cb10bc50992259ddc1145664c6347b0ea97100cada66c5153.lock → absent)
   ```

   </p></details></p>

2. Gebruik het [RWS FireFox profiel]({{< relref path="/docs/howto/rws/dev-environment/setup/firefox" >}}) om naar {{< external-link url="https://gsd-ansible-repo.internal.c2platform.org/" htmlproofer_ignore="true" >}} te navigeren. Log in wanneer daarom wordt gevraagd met de gebruikersnaam `ansible` en het wachtwoord `Supersecret!`. Je zou in staat moeten zijn om de Tomcat binaries te bekijken en te downloaden, zoals `apache-tomcat-10.1.19.tar.gz`.

3. Toegang `gsd-ansible-download1` via **Remmina** of **VirtualBox Manager** als de `vagrant` gebruiker (wachtwoord: `vagrant`). Desktop snelkoppelingen bieden links naar de file share en downloads. De `vagrant` gebruiker heeft volledige controle over en toegang tot de share.

## Review

Om de VM installatie en configuratie te begrijpen, bekijk de Ansible Inventory en Vagrant projectbestanden en de Ansible collecties.

### Ansible Inventory

Bekijk de volgende bestanden in de Ansible inventory / Vagrant project [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}):

| Bestandsnaam                     | Beschrijving                                                           |
|----------------------------------|-------------------------------------------------------------------------|
| `Vagrantfile` en `Vagrantfile.yml` | Gebruikt door Vagrant voor VM creatie, netwerkinstelling, enzovoort.  |
| `plays/mgmt/ansible_repo.yml`    | Het primaire Ansible playbook                                            |
| `group_vars/ansible_file_share/*` | Configuratie voor de file share node `gsd-ansible-file-share1`          |
| `group_vars/ansible_repo/*`      | Configuratie voor de webserver node `gsd-ansible-repo`                   |
| `group_vars/ansible_download/*`  | Configuratie voor de download nodes `gsd-ansible-download1`, `2` en `3` |

### Ansible Collecties / Rollen

| Collectie             | Beschrijving                                                                         |
|-----------------------|---------------------------------------------------------------------------------------|
| `c2platform.wincore`  | Bevat essentiële rollen voor Windows hosts, zoals `c2platform.wincore.download`.      |
| `c2platform.core`     | Biedt rollen voor Linux targets, zoals `c2platform.core.linux`.                       |
| `c2platform.mw`       | Bevat de `apache` rol voor het opzetten van een webserver.                            |

## Volgende stappen

Voor ontwikkeling en experimenten, overweeg een stapsgewijze aanpak met behulp van Ansible tags en Vagrant snapshots om de omgeving efficiënter te beheren en te itereren.

<p><details>
  <summary><kbd>Toon mij</kbd></summary><p>

| Tag  | Stap                 | Opdracht                                            | Beschrijving                                                                           |
|------|----------------------|-----------------------------------------------------|---------------------------------------------------------------------------------------|
| `v0` | Bestand Share aanmaken | `TAGS=v0 vagrant up $BOX`                            | Maakt alle nodes maar voert alleen de Ansible provisioner uit op `gsd-ansible-file-share1`. |
| `v1` | Webserver aanmaken   | `TAGS=v1 vagrant provision $BOX`                     | Voert de provisioner uit op `gsd-ansible-repo`. Dit creëert de Apache2-gebaseerde webserver. |
| `v2` | Software downloaden  | `TAGS=v2 vagrant provision $BOX`                     | Voert Ansible uit op `gsd-ansible-download[1:3]`.                                       |

Om een specifieke tag te voorzien, voorzie je de opdracht met `TAGS=`, bijvoorbeeld om stap `v1` te voorzien, voer de opdracht uit:

```bash
TAGS=v1 vagrant provision $BOX
```

Na elke stap kun je snapshots maken met behulp van Vagrant, vooral een snapshot na `v0` omdat de creatie van de node enige tijd in beslag neemt. Dit omvat ook de registratie van de RHEL 9-gebaseerde `gsd-ansible-repo` en Sysprep van alle Windows hosts.

Er zijn andere tags, dus bijvoorbeeld als je alleen de download wilt uitvoeren en alleen van `gsd-ansible-download1`

```bash
TAGS=common,download vagrant up $BOX --provision | tee provision.log
```

</p></details></p>

## Aanvullende Informatie

Voor extra inzichten en richtlijnen:

* Ontdek het ontwerp en de voordelen van deze setup in [Ontwerp van een Flexibele Software Repository voor Ansible]({{< relref path="/docs/projects/rws/software" >}}).
* Leer meer over de mogelijkheden van Vagrant bij het omgaan met Sysprep voor Windows en geautomatiseerde registratie voor RHEL 9 in de [Vagrant setup handleiding]({{< relref path="/docs/howto/dev-environment/setup/vagrant" >}}).
* Voor een overzicht van de soorten projecten in Ansible, raadpleeg [Ansible Projecten]({{< relref path="/docs/concepts/ansible/projects" >}}).
* Om downloads met HTTPS zonder SSL/TLS fouten mogelijk te maken, wordt een vertrouwensrelatie gecreëerd door Ansible. Raadpleeg [Beheer van SSL/TLS Vertrouwen met Ansible op Windows Hosts]({{< relref path="docs/howto/rws/windows/trusts" >}}) voor meer informatie.