---
categories: ["Voorbeeld"]
tags: [vagrant, windows, ansible]
title: "Overzicht RWS Ontwikkelomgeving"
linkTitle: "Overzicht"
draft: true
weight: 1
description: >
  TODO
---

```plantuml
@startuml overzicht
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")

Boundary(laptop, "", $type="") {
  Boundary(mgmt, "mgmt", $type="") {
    Container(ansible, "Ansible", "gsd-ansible 8.105")
  }
  Boundary(gis, "gis.c2platform.org", $type="") {
    Container(agwa1, "ArcGIS Web Adaptor", "gsd-agwa1 8.102")
    Container(agpro1, "ArcGIS Portal", "gsd-agportal1 8.103")
    Container(agserver1, "ArcGIS Server", "gsd-server1 8.100")
    Container(rproxy, "Reverse Proxy", "gsd-rproxy1 5.205")
  }
}

Rel(engineer, rproxy, "", "https\n443")
Rel(engineer, ansible, "", "ssh\nrdp")
Rel(rproxy, agwa1, "", "https\n6443")
Rel(agwa1, agpro1, "", "https\n7443")
Rel(agwa1, agserver1, "", "https\n6443")
@enduml
```