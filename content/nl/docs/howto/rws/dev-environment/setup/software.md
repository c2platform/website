---
categories: ["Handleiding"]
tags: ["vagrant", "windows", "ansible"]
title: "RWS Software en Licentie Bestanden Beschikbaar Maken voor Ansible"
linkTitle: "Software en Licenties"
weight: 6
description: >
  Maak de benodigde (ArcGIS) software en licenties beschikbaar voor Ansible op Windows `gsd` knooppunten met behulp van Vagrant Sync Folders.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---

Het project [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) vereenvoudigt het proces om essentiële software en licenties beschikbaar te maken voor Ansible op Windows `gsd` knooppunten, zoals `gsd-agserver1`, door gebruik te maken van [Vagrant Sync Folders]({{< relref path="/docs/guidelines/dev/vagrant-sync-folders" >}}).

Volg deze stappen om ArcGIS software en licenties beschikbaar te maken voor Ansible:

## Voorbereiden van `.sync_folders.yml`

Maak een bestand genaamd `.sync_folders.yml` in de root van het [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) project en vul het met inhoud die lijkt op het onderstaande voorbeeld. Pas het `src` pad aan om overeen te komen met je lokale map die de benodigde software en licentiebestanden bevat. Dit configuratiebestand zal door het `Vagrantfile` binnen het [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) project worden gebruikt om een mapmount in de VM tot stand te brengen, specifiek op `C:\arcgis-software-repo`.

```yaml
---
- src: /software/projects/rws/
  target: /arcgis-software-repo
```

{{< alert title="Opmerking:" >}}Deze `.sync_folders.yml` wordt genegeerd door Git. Zie `.gitignore`.{{< /alert >}}

## Software en Licenties Beschikbaar Stellen

Als voorbeeld wordt mijn lokale map hieronder getoond:

```bash
├── arcgis
│   └── 11.3
│       ├── ArcGISDataStore-11.3.zip
│       ├── ArcGISDataStore.zip
│       ├── ArcGIS_Enterprise_Portal_113_467329_20240712.json
│       ├── ArcGISGISServerAdvanced_ArcGISServer_1463509.ecp
│       ├── ArcGISServer-11.3.zip
│       ├── ArcGISWebStyles-11.3.zip
│       ├── ArcGISWebStyles.zip
│       ├── dotnet-hosting-6.0.16-win.exe
│       ├── dotnet-hosting-8.0.6-win.exe
│       ├── InstallWebAdaptor.ps1
│       ├── microsoft
│       │   ├── dotnet-hosting-8.0.6-win.exe
│       │   └── WebDeploy_amd64_en-US.msi
│       ├── PortalForArcGIS-11.3.zip
│       ├── WebAdaptorIIS-11.3.zip
│       ├── WebAdaptorIIS.zip
│       └── WebDeploy_amd64_en-US.msi
├── fme
│   ├── fme-flow-2023.1.1-b23631-win-x64.exe
│   ├── fme-flow-2023.1.1-b23631-win-x64.zip
│   ├── fme-flow-2023.2-b23764-win-x64.exe
│   ├── fme-flow-2023.2-b23764-win-x64.zip
│   ├── fme-flow-2024.0.3-b24220-win-x64.exe
│   ├── fme-flow-2024.0.3-b24220-win-x64.zip
│   └── fme-flow-2024.1.3-b24627-win-x64.exe
├── geoweb
│   ├── rws2024.txt
│   ├── VertiGIS-Studio-Reporting-5.24.1.msi
│   └── VertiGIS-Studio-Web-5.31.0.msi
├── microsoft
│   ├── dotnet-hosting-8.0.6-win.exe
│   └── WebDeploy_amd64_en-US.msi
└── oracle
    └── instantclient-basic-windows.x64-23.4.0.24.05.zip
```

Dit maakt de ArcGIS Server software beschikbaar in `gsd-agserver1` met het pad `C:\arcgis-software-repo\ArcGISServer.zip`.

## Vagrant opstarten of herladen

Na het aanmaken van `.sync_folders.yml` zal Vagrant de mount voor je lokale softwaremap maken wanneer je een `vagrant up` of `vagrant reload` commando uitvoert.

## Verifiëren

Als je verbinding maakt met de VM via VirtualBox Manager, zou je de mount `C:\arcgis-software-repo` moeten kunnen zien.

{{< image filename="/images/docs/vagrant-sync-folder.png" >}}