---
categories: ["Handleiding"]
tags: [linux, ubuntu, laptop, lxd, vagrant, ansible, virtualbox]
title: "De RWS Ontwikkelomgeving instellen op Ubuntu 22"
linkTitle: "Instellen"
weight: 2
description: >
  Installeer Ansible, Vagrant, LXD, Virtualbox en kloon de projectdirectory.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---

In dit gedeelte vind je stapsgewijze instructies over hoe je de RWS
ontwikkelomgeving kunt instellen en gebruiken.