---  
categories: ["Handleiding"]  
tags: [ubuntu, lxd, vagrant, netwerk]  
title: "LXD Netwerk en Profiel voor RWS"  
linkTitle: "LXD"  
weight: 3  
description: >  
  Installeer, initialiseer en configureer LXD.  
---

Deze handleiding biedt instructies voor het installeren, initialiseren en configureren van [LXD]({{< relref path="/docs/concepts/dev/lxd" >}}). LXD is de standaard lichte hypervisor voor het creëren en beheren van lichte virtuele machines in de vorm van Linux (LXC) containers.

---

## Netwerk en Profiel Configuratie

```bash  
lxc network create lxdbrag1 ipv6.address=none ipv4.address=1.1.5.1/24 ipv4.nat=true  
lxc profile copy default agd  
```

Bewerk nu het profiel `agd` en configureer `lxdbrag1` als de `ouder` van `eth1`.

```bash  
lxc profile edit agd  
```

Het profiel `agd` zou als volgt moeten zijn:

```yaml  
config: {}  
description: GSD LXD profiel  
devices:  
  eth0:  
    name: eth0  
    network: lxdbr0  
    type: nic  
  eth1:  
    nictype: bridged  
    parent: lxdbrag1  
    type: nic  
  root:  
    path: /  
    pool: c2d  
    type: disk  
name: agd  
used_by: []  
```