---
categories: ["Handleiding"]
tags: [git]
title: "Projectdirectories instellen voor RWS"
linkTitle: "Projectdirectory Instellen"
weight: 2
description: >
  Leer hoe je RWS Git-repositories kunt klonen met behulp van het kloonscript en Ansible
  Galaxy Rollen en Collecties kunt installeren om je ontwikkelomgeving efficiënt op te zetten.
---

## Voer het Kloonscript uit

```bash
sudo apt install git -y
export GIT_USER= # je gitlab gebruiker
export GIT_MAIL= # je gitlab email
eval `ssh-agent -s`
ssh-add
curl -s -L https://gitlab.com/c2platform/rws/ansible-gis/-/raw/master/clone.sh | bash
```

## Maak `rws` Alias

Als onderdeel van het instellen van de C2 ontwikkelomgeving, zie
[Setup Project Directory and Install Ansible]({{< relref path="/docs/howto/dev-environment/setup/project" >}}),
hebben we twee aliassen `c2` en `c2-home` gemaakt.

```
alias c2-home='cd ~/git/gitlab/c2/ansible-dev'
alias c2='c2-home && source ~/.virtualenv/c2d/bin/activate'
```

Voor de ontwikkeling van RWS maken we de alias `rws` zoals hieronder weergegeven.

```
alias rws='c2 && cd ~/git/gitlab/c2/ansible-gis'
```

## Installeer Ansible Collecties en Rollen

Navigeer naar de root van het Ansible-project en activeer de Ansible virtuele
omgeving met behulp van de `rws` alias. Installeer vervolgens de rollen en collecties als volgt:

```bash
source ~/.bashrc
rws # activeer de RWS Ansible Virtuele Omgeving
ansible-galaxy install -r roles/requirements.yml --force --no-deps -p roles/external
ansible-galaxy collection install -r collections/requirements.yml -p .
```
Door deze stappen te volgen, heb je je projectdirectories ingesteld en ben je
klaar voor de RWS-ontwikkeling.