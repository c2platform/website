---
categories: ["Handleiding"]
tags: [vagrant, reverse-proxy]
title: "Creëer de Reverse Proxy en Web Proxy"
linkTitle: "Reverse en Web Proxy"
weight: 5
description: >
    Maak en voorzie de `gsd-rproxy1` node, wat een essentiële voorwaarde is voor een functionele ontwikkelomgeving.
---

De `gsd-rproxy1` node speelt een vergelijkbare rol als de `c2d-rproxy1`. Zie
[Creëer de Reverse Proxy en Web Proxy]({{< relref path="/docs/howto/dev-environment/setup/rproxy1" >}})
voor meer informatie.

## Het creëren van `gsd-rproxy1`

Om de node te maken en van voorzieningen te voorzien, gebruik het volgende commando:

```bash
vagrant up gsd-rproxy1
```