---
categories: ["Handleiding"]
tags: [reverse-proxy, forward-proxy, apache, firefox, profiel, proxy, sandbox]
title: "Toegang tot de RWS Ontwikkelomgeving met Firefox"
linkTitle: "Firefox"
weight: 6
description: >
    Leer hoe u toegang krijgt tot de RWS Ontwikkelomgeving met behulp van Firefox door 
    een speciaal Firefox-profiel in te stellen, het C2 CA Root-certificaat te importeren, en 
    een forward proxy te configureren.
---

Projecten: [`c2platform/rws/ansible`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

## Inleiding

Het Ansible inventory project
[`c2platform/rws/ansible`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})
simuleert een opstelling met twee omgevingen: een ontwikkelomgeving `gsd` en een "test" omgeving `gst`. De `gst` omgeving geeft je de mogelijkheid om te experimenteren met een GitOps pijplijn. Deze omgeving is optioneel en wordt momenteel gebruikt voor:
* [GitOps Pipeline voor een Execution Environment (EE) met Ansible Collections]({{< relref path="/docs/howto/rws/aap/gitops" >}})

Deze omgevingen zijn opgezet als een sandbox. Het domein `c2platform.org` verwijst niet naar diensten op het internet, maar naar diensten die toegankelijk zijn via de reverse proxy node `gsd-rproxy1` (of `gst-rproxy1`).

## Een Firefox-profiel maken

Het wordt sterk aanbevolen om een speciaal Firefox-profiel te maken om je reguliere browsen gescheiden te houden van de toegang tot de RWS Ontwikkelomgeving. Volg de gids van Mozilla over
{{< external-link url="https://support.mozilla.org/en-US/kb/profile-manager-create-remove-switch-firefox-profiles?redirectslug=profile-manager-create-and-remove-firefox-profiles&redirectlocale=en-US" text="Profielbeheerder - Maak, verwijder of verwissel Firefox-profielen" htmlproofer_ignore="false" >}}.

Je kunt de Firefox Profile Manager op twee manieren openen:
1. Voer het commando `firefox -no-remote -P` uit.
2. Typ `about:profiles` in de adresbalk van Firefox om je Firefox-profielen te maken en beheren.

Zodra je je speciale profiel klaar hebt, ga dan verder met de volgende instellingen.

## Netwerkinstellingen configureren

In je speciale profiel, configureer de netwerkinstellingen voor de `gsd` of `gst` omgeving:

| Eigenschap         | GSD-omgeving    | GST-omgeving (optioneel)   |
|--------------------|-----------------|----------------------------|
| HTTP Proxy         | `1.1.5.205`     | `1.1.5.209`                |
| Poort              | `1080`          | `1080`                     |
| Ook gebruiken voor HTTPS | ✔         | ✔                          |

## CA Bundel importeren

Om continue TLS/SSL-certificaatfouten bij het benaderen van de diensten te voorkomen, importeer het C2 root-certificaat op locatie
`~/git/gitlab/c2/ansible-gis/.ca/c2/c2.crt` via de Firefox Certificaat instellingen.

## Verifiëren

1. Navigeer naar
   {{< external-link url="https://c2platform.org/is-alive" htmlproofer_ignore="true" >}}.
   Je zou de tekst "Apache is alive." moeten zien. Als je dit bericht ziet, is je browser correct geconfigureerd om diensten van het GIS-platform te benaderen.
2. Bezoek de startpagina's voor de `gsd` en `gst` omgevingen door te navigeren naar
   {{< external-link url="https://gsd.c2platform.org/" htmlproofer_ignore="true" >}}
    en
   {{< external-link url="https://gst.c2platform.org/" htmlproofer_ignore="true" >}}.

   {{< image filename="/images/docs/gsd.png?width=500px" >}}.
3. Bezoek de C2 Platform community website op
   {{< external-link url="https://c2platform.org/" htmlproofer_ignore="true" >}}.
   Merk op dat dit een lokale implementatie van de website is binnen `gsd-rproxy1` (en `gst-rproxy1`).

   {{< image filename="/images/docs/c2platform.org.png?width=500px" >}}.