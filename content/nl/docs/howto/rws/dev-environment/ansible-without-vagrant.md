---
categories: ["Handleiding"]
tags: ["handleiding", "ssh", "vagrant", "ansible"]
title: "Ansible gebruiken zonder Vagrant"
linkTitle: "Ansible zonder Vagrant"
weight: 15
description: >
    Vagrant is de standaard, maar je kunt ook direct Ansible gebruiken als je dat liever hebt.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---
Om Ansible direct te kunnen gebruiken zonder Vagrant, met andere woorden om bijvoorbeeld `ansible-playbook` te kunnen draaien, moet je de SSH-configuratie op je machine aanpassen.

## SSH-configuratie

Bewerk `.ssh/config` en voeg de onderstaande regel toe. Dit maakt toegang tot *alle* knooppunten mogelijk door gebruik te maken van SSH-hops via `gsd-rproxy1`.

```
Host gsd-*
  ProxyCommand ssh 1.1.4.205 -W %h:%p
  User vagrant
  IdentityFile ~/.vagrant.d/insecure_private_key
  StrictHostkeyChecking no
  UserKnownHostsFile /dev/null
  LogLevel INFO
  Compression yes
  ServerAliveInterval 10
  ServerAliveCountMax 10
```

## Verifiëren

Start `gsd-rproxy1` en maak verbinding met de node via SSH. Let op: we gebruiken `ssh` en niet `vagrant ssh`, dus we omzeilen Vagrant volledig.

```bash
vagrant up gsd-rproxy1
ssh gsd-rproxy1
```

Nu zou je Ansible direct moeten kunnen gebruiken, zonder Vagrant, bijvoorbeeld met een opdracht zoals hieronder.

```bash
source ~/.virtualenv/uwd/bin/activate
export ANSIBLE_CONFIG=$PWD/ansible-dev.cfg
ansible-playbook plays/mw/reverse_proxy.yml -i hosts-dev.ini --limit gsd-rproxy1
```