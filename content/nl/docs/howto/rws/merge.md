---
categories: ["Handleiding"]
tags: [ansible,Gedrag,ansible.cfg,samenvoegen,woordenboek]
title: "Woordenboek Samenvoegen in Ansible Projecten Beoordelen"
linkTitle: "Woordenboek Samenvoegen Beoordelen"
weight: 10
description: >
  Onderzoek het samenvoegen van woordenboeken in Ansible inventarisprojecten,
  met een focus op de `hash_behaviour = merge` instelling in C2 Ansible
  inventarisprojecten.
---

Projecten:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

---

## Configureren van `hash_behaviour`

Zorg ervoor dat de `hash_behaviour` in `ansible.cfg` is ingesteld op `merge` om
het samenvoegen van woordenboeken in uw
[Ansible inventarisproject]({{< relref path="/docs/concepts/ansible/projects/inventory" >}})
mogelijk te maken.

## Beoordeel variabelen

Variabelen in C2 Platform Ansible Rollen zoals `win_resources` en
`linux_resources` kunnen complexe woordenboeken zijn. Gebruik dit commando om
win_resources in uw project te vinden:

```bash
grep -HnA1 '^win_resources:' $(find group_vars -name "*.yml")
```

Voorbeeld Uitvoer:

```bash
 ~/git/azure/rws/ansible-gis$ grep -HnA1 '^win_resources:' $(find group_vars -name "*.yml")
group_vars/fme/win.yml:2:win_resources:
group_vars/fme/win.yml-3-  2-fme:
--
group_vars/windows/main.yml:20:win_resources:
group_vars/windows/main.yml-21-  0-bootstrap:
--
group_vars/fme_engine/main.yml:12:win_resources:
group_vars/fme_engine/main.yml-13-  fme_arcpy_path:
rws →
 ~/git/azure/rws/ansible-gis$
```

Het toont drie voorkomen in drie Ansible groepen:

| Groep        | Sleutel          |
|--------------|------------------|
| `fme`        | `2-fme`          |
| `windows`    | `0-bootstrap`    |
| `fme_engine` | `fme_arcpy_path` |

Als een node deel uitmaakt van alle drie de groepen, zal Ansible deze lijsten
samensmelten tot bijvoorbeeld een woordenboek zoals:

```yaml
win_resources:
  2-fme:
    - <lijstitem>
    - <lijstitem>
  0-bootstrap:
    - <lijstitem>
    - <lijstitem>
  fme_arcpy_path:
    - <lijstitem>
```

C2 Ansible Rollen zullen deze woordenboeken meestal eerst verwerken met de filter
`c2platform.core.groups2items` voor vereenvoudigde afhandeling. Deze filter zal
de lijst afvlakken en ook alfabetisch sorteren op groepsnaam. De Ansible rol
`c2platform.wincore.win` gebruikt ook deze filter. Het resultaat is hieronder te
zien:

```yaml
win_resources:
  - group: 0-bootstrap
    <lijstitem>
  - group: 0-bootstrap
    <lijstitem>
  - group: 2-fme
    <lijstitem>
  - group: fme_arcpy_path
    <lijstitem>
```

{{< alert title="Tip:" >}}
Het voorvoegen van groepsnamen met nummers (bijv. `0-bootstrap`) organiseert de
uitvoeringsvolgorde in Ansible, waardoor afhankelijkheden effectief
geprioriteerd worden.
{{< /alert >}}

## Beoordeel andere "samenvoegende" woordenboeken

Bekijk alle andere variabelen die woordenboeken zijn en die samengevoegd worden
of kunnen worden. Bijvoorbeeld, C2 Platform productrollen kunnen de
`c2platform.wincore.win` rol omvatten en hun eigen "resources" variabelen
definiëren. Zoek naar `_win_resources`. Het resultaat kan er als volgt uitzien.

```yaml
ostraaten@localhost:~/git/azure/ansible-gis$ grep -HnA1 '_win_resources:' $(find group_vars -name "*.yml")
group_vars/fme_core/main.yml:11:fme_flow_win_resources:
group_vars/fme_core/main.yml-12-  core:
--
group_vars/fme_core/tomcat.yml:26:tomcat_win_resources:
group_vars/fme_core/tomcat.yml-27-  tomcat:
ostraaten@localhost:~/git/azure/ansible-gis$
```

Hieruit kunnen we afleiden dat de `tomcat` rol en `fme_flow` de
`c2platform.wincore.win` rol bevatten. Respectievelijk maken deze rollen gebruik
van deze rol via de variabelen `fme_flow_win_resources` en
`tomcat_win_resources`.

## Aanvullende Informatie

* Zie de richtlijn
  [Woordenboek Samenvoegen Beheren in C2 Platform Ansible Projecten]({{< relref path="/docs/guidelines/coding/merge" >}}).