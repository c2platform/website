---
categories: ["Handleiding", "Voorbeeld"]
tags: [ansible, omgeving, podman, ansible-builder, ansible-navigator, docker]
title: "Beheer van de RWS Ansible Uitvoeringsomgeving"
linkTitle: "Ansible Uitvoeringsomgeving"
weight: 5
description: >
  Deze handleiding biedt stapsgewijze instructies over hoe de RWS Ansible
  Uitvoeringsomgeving te beheren, waarbij compatibiliteit met de nieuwste
  versies van Python en Ansible wordt gewaarborgd, samen met de vereiste
  Ansible-collecties.
---

Projecten:
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}}),
[`c2platform/c2/docker/gitlab-runner`]({{< relref path="/docs/gitlab/c2platform/c2/docker/gitlab-runner" >}})

---

De RWS Ansible Uitvoeringsomgeving wordt geconstrueerd via `ansible-builder` binnen het `c2platform/rws/ansible-execution-environment` project, met gebruik van een GitLab CI/CD-pipeline die opereert in Docker containers. Dit proces resulteert in een Docker image dat als de EE dient. Het aangepaste GitLab Runner image dat voor dit proces nodig is, is afgeleid van het `c2platform/c2/docker/gitlab-runner` project. Voor meer details over deze aanpak, zie de [Docker-in-docker (dind)]({{< relref path="/docs/howto/c2/dind" >}}) handleiding.

---

## Vereisten

* Zorg ervoor dat je RWS Ontwikkelomgeving is opgezet op Ubuntu 22, zoals gedetailleerd
  [hier]({{< relref path="/docs/howto/rws/dev-environment/setup" >}}).
* Installeer {{< external-link url="https://podman.io/" text="Podman" htmlproofer_ignore="false" >}},
  een beheerhulpmiddel voor containers.

## De Uitvoeringsomgeving Inspecteren

Om de inhoud van de EE te verkennen, begin met het instellen van omgevingsvariabelen voor gemak:

```bash
export IMAGE_NAME="registry.gitlab.com/c2platform/rws/ansible-execution-environment"
export IMAGE_VERSION="0.1.7"
export IMAGE="$IMAGE_NAME:$IMAGE_VERSION"
```

Voer vervolgens de volgende commando's uit om alle opgenomen Ansible-collecties en Python/PIP-pakketten te lijst:

```bash
podman run $IMAGE ansible-galaxy collection list
podman run $IMAGE pip3 list installed
```

De uitvoer zal de beschikbare collecties en geïnstalleerde pakketten tonen.

<p><details>
  <summary><kbd>Toon mij</kbd></summary><p>

```bash
θ85° [:ansible-gis]└2 master(+39/-10)* ± podman run $IMAGE ansible-galaxy collection list

# /usr/share/ansible/collections/ansible_collections
Collection            Versie
--------------------- -------
ansible.posix         1.5.4
ansible.windows       1.14.0
awx.awx               22.4.0
c2platform.core       1.0.8
c2platform.gis        1.0.4
c2platform.mgmt       1.0.2
c2platform.wincore    1.0.5
checkmk.general       3.0.0
chocolatey.chocolatey 1.5.1
community.crypto      2.17.1
community.general     8.3.0
community.postgresql  2.4.3
community.windows     2.0.0
```

```bash
θ65° [:ansible-execution-environment]└2 master(+18/-2)* ± podman run $IMAGE pip3 list installed | grep ansible
ansible-compat            4.1.11
ansible-core              2.15.0
ansible-lint              6.17.2
ansible-runner            2.3.4

[notice] Een nieuwe versie van pip beschikbaar: 22.3.1 -> 24.0
[notice] Om te updaten, voer uit: pip install --upgrade pip
```

</p></details></p>

## Het Updaten van de Uitvoeringsomgeving

Om de EE bij te werken, kun je het
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})
project klonen of het direct bewerken met de GitLab Web IDE. Overweeg het aanpassen van de
volgende bestanden:

* Update de Ansible-versie, Python, en PIP-pakketten in `execution-environment.yml`.
* Vernieuw Ansible-collecties in `requirements.yml`.
* Pas OS systeemafhankelijkheden aan in `indep.txt`.

Het indienen en pushen van je wijzigingen zal de GitLab CI/CD-pipeline activeren om een nieuwe `latest` versie van de EE vrij te geven.

## Lokale Bouw, Inspectie en Testen

Voor lokale ontwikkeling kun je `ansible-builder`, `ansible-navigator` en `podman` gebruiken om de EE te construeren en valideren. Dit is een aanbevolen praktijk voordat wijzigingen worden doorgevoerd:

```bash
rws  # Activeer de RWS virtuele omgeving
cd ../ansible-execution-environment/
ansible-builder build --tag $IMAGE
```

<p><details>
  <summary><kbd>Toon mij</kbd></summary><p>

```bash
θ62° [:ansible-execution-environment]└2 master(+18/-2)* ± ansible-builder build --tag $IMAGE
Voer commando uit:
  podman build -f context/Containerfile -t registry.gitlab.com/c2platform/rws/ansible-execution-environment:0.1.7 context
Voltooid! De bouwcontext is te vinden op: /home/ostraaten/git/gitlab/c2/ansible-execution-environment/context
```
</p></details></p>

Inspecteer en test de EE op een vergelijkbare manier als in de initiële inspectiestap.

```bash
podman run $IMAGE ansible-galaxy collection list
podman run $IMAGE pip3 list installed
```

Gebruik voor het testen van de EE:

```bash
ansible-navigator run test_localhost.yml --eei $IMAGE --mode stdout
```

Dit commando zal een test playbook uitvoeren en de uitvoer tonen.

<p><details>
  <summary><kbd>Toon mij</kbd></summary><p>

```bash
θ65° [:ansible-execution-environment]└2 master ± ansible-navigator run test_localhost.yml --eei $IMAGE --mode stdout
[WAARSCHUWING]: Er is geen inventaris geparsed, alleen impliciete localhost is beschikbaar
[WAARSCHUWING]: De opgegeven hosts-lijst is leeg, alleen localhost is beschikbaar. Let op dat
de impliciete localhost niet overeenkomt met 'all'

PLAY [Verzamel en print lokale Ansible / Python feiten] ***************************

OPDRACHT [Verzamelen van feiten] **************************************************
ok: [localhost]

OPDRACHT [Print Ansible versie] ***************************************************
ok: [localhost] => {
    "ansible_version": {
        "full": "2.15.0",
        "major": 2,
        "minor": 15,
        "revision": 0,
        "string": "2.15.0"
    }
}

OPDRACHT [Print Ansible Python] ***************************************************
ok: [localhost] => {
    "ansible_playbook_python": "/usr/bin/python3"
}

OPDRACHT [Print Ansible Python versie] ********************************************
ok: [localhost] => {
    "ansible_python_version": "3.11.7"
}

SPELSAMENVATTING *******************************************************************
localhost                  : ok=4    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

</p></details></p>

## Een Nieuwe Versie Vrijgeven

Om een bijgewerkte versie van de EE vrij te geven:

1. Wijzig de `CHANGELOG.md` om de laatste wijzigingen weer te geven.
2. Commit en push deze wijzigingen. Deze actie zal de CI/CD-pipeline activeren
   om een nieuwe laatste versie van de image te publiceren.
3. Nadat de pipeline is voltooid, initieer handmatig de **Release maken** stap
   via de GitLab webinterface.

## Aanvullende Bronnen

* {{< external-link url="https://ansible.readthedocs.io/en/latest/getting_started_ee/run_community_ee_image.html" text="Ansible uitvoeren met de community EE image - Ansible ecosysteem documentatie" htmlproofer_ignore="false" >}}
* {{< external-link url="https://ansible.readthedocs.io/en/latest/getting_started_ee/setup_environment.html" text="Je omgeving instellen - Ansible ecosysteem documentatie" htmlproofer_ignore="false" >}}
* {{< external-link url="https://ansible.readthedocs.io/en/latest/getting_started_ee/build_execution_environment.html" text="Het bouwen van je eerste Uitvoeringsomgeving - Ansible ecosysteem documentatie" htmlproofer_ignore="false" >}}
* {{< external-link url="https://ansible.readthedocs.io/projects/builder/en/stable/definition/" text="Uitvoeringsomgeving Definitie — Ansible Builder Documentatie" htmlproofer_ignore="false" >}}
* {{< external-link url="https://ansible.readthedocs.io/projects/runner/en/stable/intro/#inputdir" text="Inleiding tot Ansible Runner — ansible-runner documentatie" htmlproofer_ignore="false" >}}