---
categories: ["Handleiding"]
tags: [TODO]
title: "Gitlab Agent Installeren"
linkTitle: "Gitlab Agent"
weight: 20
description: >
    Installeer Gitlab Agent op Kubernetes
---

Deze handleiding beschrijft de installatie van de **Gitlab Agent** op een
MicroK8s-node genaamd `c2d-ks1`. Het bijbehorende voorbeeldproject is
[c2platform/examples/nodejs-kubernetes](https://gitlab.com/c2platform/examples/nodejs-kubernetes).
Deze handleiding is gebaseerd op [De agent voor Kubernetes installeren |
GitLab](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html).

## Maak een Kubernetes-cluster

Maak een lokaal Kubernetes-cluster op `c2d-ks`.

* [Kubernetes opzetten]({{< relref path="./kubernetes.md" >}})
* [Het Kubernetes-dashboard opzetten]({{< relref path="../kubernetes/dashboard.md" >}}) (optioneel, maar aanbevolen)

## Registreer de GitLab-agent

De eerste stap om een agent te creëren is het maken van een agentbestand. Dit is
reeds gedaan, zie `.gitlab/agents/c2d-mk8s/config.yaml` in {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-gitops" text="c2platform/examples/kubernetes/gitlab-gitops" htmlproofer_ignore="false" >}}. Met `config.yaml` aangemaakt en gepusht naar de repository, kan de agent
worden geregistreerd. Navigeer in Gitlab.com naar **Infrastructuur** → {{<
external-link
url="https://gitlab.com/c2platform/examples/nodejs-kubernetes/-/clusters"
text="Kubernetes-cluster" htmlproofer_ignore="true" >}} en selecteer **Verbind
een cluster**. Selecteer de agent (`c2d-mk8s` in ons voorbeeld) en registreer
deze. Dit resulteert in enkele `helm`-opdrachten die we moeten uitvoeren om de
agent in OpenShift te installeren.

Zie voor meer informatie {{< external-link url="https://docs.gitlab.com/ee/user/clusters/agent/install/index.html" text="De agent voor Kubernetes installeren | GitLab" htmlproofer_ignore="false" >}}.

## Installeer de GitLab-agent

De agent wordt vervolgens geïnstalleerd door opdrachten uit te voeren die
vergelijkbaar zijn met hieronder. Let op: het exacte commando en de token worden
weergegeven wanneer u de agent registreert. Let op: voor MicroK8s is het
`helm`-commando `microk8s helm`, maar in `c2d-ks` is een alias gemaakt voor
`helm`, zodat je `helm` kunt gebruiken.

```bash
helm repo add gitlab https://charts.gitlab.io
helm repo update
export GITLAB_AGENT_TOKEN=<token>
export GITLAB_AGENT_FOLDER=c2d-mk8s
kubectl config set-context --current --namespace=gitlab-agent-$GITLAB_AGENT_FOLDER
helm upgrade --install $GITLAB_AGENT_FOLDER gitlab/gitlab-agent \
    --namespace gitlab-agent-$GITLAB_AGENT_FOLDER \
    --create-namespace \
    --set image.tag=v15.9.0 \
    --set config.token=$GITLAB_AGENT_TOKEN \
    --set config.kasAddress=wss://kas.gitlab.com
```

<details>
  <summary><kbd>Toon mij</kbd></summary>

```bash
vagrant@c2d-ks1:/tmp$ helm repo add gitlab https://charts.gitlab.io
helm repo update
"gitlab" is toegevoegd aan je repositories
Even geduld terwijl we de laatste updates van je chart-repositories binnenhalen...
...Succesvol een update gekregen van de "gitlab" chart-repository
Update compleet. ⎈Gelukkig Helmen!⎈
vagrant@c2d-ks1:/tmp$ export GITLAB_AGENT_TOKEN=jWsjSqySt_jpjzC9xecqxxQD3js7hRcRftz6YuQniNs-F8JM7w
vagrant@c2d-ks1:/tmp$ export GITLAB_AGENT_FOLDER=c2d-mk8s
vagrant@c2d-ks1:/tmp$ kubectl config set-context --current --namespace=gitlab-agent-$GITLAB_AGENT_FOLDER
Context "microk8s" aangepast.
vagrant@c2d-ks1:/tmp$ helm upgrade --install $GITLAB_AGENT_FOLDER gitlab/gitlab-agent \
    --namespace gitlab-agent-$GITLAB_AGENT_FOLDER \
    --create-namespace \
    --set image.tag=v15.9.0 \
    --set config.token=$GITLAB_AGENT_TOKEN \
    --set config.kasAddress=wss://kas.gitlab.com
Release "c2d-mk8s" bestaat niet. Installeert het nu.
NAAM: c2d-mk8s
LAATST GEDEPLOYD: Mon Mar  6 07:50:17 2023
NAMESPACE: gitlab-agent-c2d-mk8s
STATUS: gedeployed
REVISION: 1
TEST SUITE: Geen

```

## Verifieer

De `helm upgrade` creëert een namespace `gitlab-agent-c2d-mk8s`.

```bash
vagrant@c2d-ks1:/tmp$ kubectl get all --all-namespaces
NAMESPACE               NAME                                            READY   STATUS    RESTARTS       AGE
kube-system             pod/calico-kube-controllers-586dd5cf66-k6cpr    1/1     Running   1 (167m geleden)   4d
kube-system             pod/kubernetes-dashboard-dc96f9fc-zgldh         1/1     Running   1 (167m geleden)   4d
kube-system             pod/coredns-6f5f9b5d74-n8sq8                    1/1     Running   1 (167m geleden)   4d
kube-system             pod/dashboard-metrics-scraper-7bc864c59-bx2n6   1/1     Running   1 (167m geleden)   4d
kube-system             pod/calico-node-bmvgb                           1/1     Running   1 (167m geleden)   4d
kube-system             pod/metrics-server-6f754f88d-gttst              1/1     Running   1 (167m geleden)   4d
gitlab-agent-c2d-mk8s   pod/c2d-mk8s-gitlab-agent-56b9f5bc97-sqq49      1/1     Running   0              4m
nja                     pod/nj-7ff9d46d57-z8z5q                         1/1     Running   0              3m54s

NAMESPACE     NAME                                TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                  AGE
default       service/kubernetes                  ClusterIP   10.152.183.1     <geen>        443/TCP                  4d
kube-system   service/metrics-server              ClusterIP   10.152.183.34    <geen>        443/TCP                  4d
kube-system   service/dashboard-metrics-scraper   ClusterIP   10.152.183.60    <geen>        8000/TCP                 4d
kube-system   service/kube-dns                    ClusterIP   10.152.183.10    <geen>        53/UDP,53/TCP,9153/TCP   4d
kube-system   service/kubernetes-dashboard        ClusterIP   10.152.183.213   1.1.4.155     443/TCP                  4d
nja           service/frontend-service            ClusterIP   10.152.183.251   <geen>        3000/TCP                 3m54s

NAMESPACE     NAME                         DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kube-system   daemonset.apps/calico-node   1         1         1       1            1           kubernetes.io/os=linux   4d

NAMESPACE               NAME                                        READY   UP-TO-DATE   AVAILABLE   AGE
kube-system             deployment.apps/calico-kube-controllers     1/1     1            1           4d
kube-system             deployment.apps/coredns                     1/1     1            1           4d
kube-system             deployment.apps/kubernetes-dashboard        1/1     1            1           4d
kube-system             deployment.apps/dashboard-metrics-scraper   1/1     1            1           4d
kube-system             deployment.apps/metrics-server              1/1     1            1           4d
gitlab-agent-c2d-mk8s   deployment.apps/c2d-mk8s-gitlab-agent       1/1     1            1           4m
nja                     deployment.apps/nj                          1/1     1            1           3m54s

NAMESPACE               NAME                                                  DESIRED   CURRENT   READY   AGE
kube-system             replicaset.apps/calico-kube-controllers-79568db7f8    0         0         0       4d
kube-system             replicaset.apps/calico-kube-controllers-586dd5cf66    1         1         1       4d
kube-system             replicaset.apps/coredns-6f5f9b5d74                    1         1         1       4d
kube-system             replicaset.apps/kubernetes-dashboard-dc96f9fc         1         1         1       4d
kube-system             replicaset.apps/dashboard-metrics-scraper-7bc864c59   1         1         1       4d
kube-system             replicaset.apps/metrics-server-6f754f88d              1         1         1       4d
gitlab-agent-c2d-mk8s   replicaset.apps/c2d-mk8s-gitlab-agent-56b9f5bc97      1         1         1       4m
nja                     replicaset.apps/nj-7ff9d46d57                         1         1         1       3m54s
```

</details>

Als je het dashboard hebt ingeschakeld, zie [Het Kubernetes-dashboard
opzetten]({{< relref path="dashboard.md" >}}), kun je deze gebruiken om de
agentlog te controleren.