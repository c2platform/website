---
categories: ["Handleiding"]
tags: [rancher, kubernetes, ansible, helm]
title: "Rancher implementeren op Kubernetes"
linkTitle: "Rancher"
weight: 20
description: >
    Voer Rancher uit op MicroK8s
---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}})

---
Deze handleiding biedt stapsgewijze instructies voor het implementeren van
Rancher op MicroK8s Kubernetes op de node `gsd-rancher1`. Op deze node zal
Ansible eerst MicroK8s installeren en vervolgens Rancher erop implementeren. Het
Kubernetes Dashboard wordt daarna geactiveerd. Na de installatie is Rancher
beschikbaar via {{< external-link url="https://rancher.c2platform.org" htmlproofer_ignore="true" >}} en is het dashboard beschikbaar via {{<
external-link url="https://dashboard-rancher.c2platform.org"
htmlproofer_ignore="true" >}}.

---

## Overzicht

<p>

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "")

Boundary(laptop, "laptop", $type="ubuntu-22") {

  Boundary(gis, "rancher.c2platform.org", $type="") {
    Container(rancher, "MicroK8s", "c2d-rancher 4.163") {
        Container(rancher_deploy, "Rancher", "")
        Container(dashboard, "Dashboard", "")
    }
    Container(rproxy, "Reverse Proxy", "c2d-rproxy1 4.205")
  }
}

Rel(engineer, rproxy, "", "")
Rel(rproxy, rancher, "", "https")
@enduml
```

</p>

## Vereisten

Maak de reverse en forward proxy `c2d-rproxy1`.

```bash
c2
unset PLAY  # zorg ervoor dat alle plays worden uitgevoerd
vagrant up c2d-rproxy1
```

Voor meer informatie over de verschillende rollen die `c2d-rproxy1` in dit
project uitvoert:

* [Reverse Proxy en CA-server instellen]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [SOCKS-proxy instellen]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Beheer van servercertificaten als Certificate Authority]({{< relref path="/docs/howto/c2/certs">}})
* [DNS instellen voor Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}})

## Vagrant

Maak en voorzie de node van de volgende opdracht:

```bash
vagrant up c2d-rancher1
```

## Verifiëren

Met de reverse proxy `c2d-rproxy` geïnstalleerd en succesvol draaiend, zie [Aan
de slag]({{< relref path="/docs/howto/dev-environment/setup">}}), zou u moeten
kunnen navigeren naar {{< external-link url="https://rancher.c2platform.org" htmlproofer_ignore="true" >}} of {{< external-link url="https://dashboard-rancher.c2platform.org" htmlproofer_ignore="true" >}} en
inloggen.

## Links

* [Kubernetes instellen]({{< relref path="./kubernetes.md" >}})
* [Het Kubernetes Dashboard instellen]({{< relref path="./dashboard.md" >}})