---
categories: ["Handleiding"]
tags: [TODO]
title: "Geheim voor SSL/TLS Certificaat"
linkTitle: "Geheim voor SSL/TLS Certificaat"
weight: 21
description: >
    Maak een Kubernetes Secret aan voor SSL/TLS Certificaat
---

## Geheim aanmaken

Om een dergelijk Kubernetes geheim aan te maken, verkrijg eerst de
base64-gecodeerde waarde van het certificaat.

```bash
cat .ca/c2/c2.crt | base64 -w 0 > /tmp/secret.txt
```

Maak een YAML-bestand aan, bijvoorbeeld
{{< download path="/download/c2/cert.yml" text="`cert.yml`" >}}

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: c2-root-cert
data:
  c2.crt: <base64-gecodeerde certificaattekst>
```

Maak het geheim aan in de namespace `kube-system`, zodat het door alle pods in
de cluster kan worden gebruikt.

```bash
vagrant ssh c2d-ks1
kubectl config set-context --current --namespace=kube-system
kubectl apply -f /vagrant/doc/howto-kubernetes-cert/c2-cert.yml
```

<details>
  <summary><kbd>Toon me</kbd></summary>

```yaml
vagrant@c2d-ks1:~/scripts/microk8s/gitlab/gitlab-runner$ kubectl get secrets
NAME                              TYPE                                  DATA   AGE
kubernetes-dashboard-certs        Opaque                                0      4d21h
microk8s-dashboard-token          kubernetes.io/service-account-token   3      4d21h
kubernetes-dashboard-csrf         Opaque                                1      4d21h
kubernetes-dashboard-key-holder   Opaque                                2      4d21h
c2-root-cert                      Opaque                                1      6s
```

</details>