---
categories: ["Handleiding"]
tags: [ansible, kubernetes]
title: "Gitlab Runner op Kubernetes"
linkTitle: "Gitlab Runner op Kubernetes"
weight: 5
description: >
    Installeer Gitlab Runner op Kubernetes
---

---

Deze handleiding beschrijft de installatie van **Gitlab Runner** in een
MicroK8s-cluster dat draait op `c2d-ks1`. Er zijn twee voorbeeldprojecten die
bij deze handleiding horen: [Voer Robot-tests uit op Kubernetes met GitLab]({{<
relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-robot" >}}) en
[Bouw Docker-image op Kubernetes met GitLab]({{< relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-docker-build" >}}).

---

## Maak een Kubernetes-cluster

Maak een lokaal Kubernetes-cluster op `c2d-ks1`.

* [Installeer Kubernetes]({{< relref path="./kubernetes.md" >}})

## Registratietoken

Navigeer naar het GitLab-project
[c2platform/examples/kubernetes/gitlab-robot](https://gitlab.com/c2platform/examples/kubernetes/gitlab-robot)
en klik vervolgens op **Instellingen** → **CI/CD** → **Runners** en kopieer het
"registratietoken". Herhaal dit voor alle projecten.

## Bewerk `values.yaml`

Een belangrijke stap om een **GitLab Runner** te registreren is het aanmaken van
een `values.yaml`-bestand. Zie het online voorbeeld / sjabloon
[values.yaml](https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml).
Zie ook [Docker-in-Docker met TLS ingeschakeld in
Kubernetes](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-in-docker-with-tls-enabled-in-kubernetes)
en `values.yaml` in dit project. Bewerk vanuit `c2d-ks1` het
`values.yaml`-bestand.

```bash
nano /vagrant/doc/howto-kubernetes-gitlab-runner/values.yaml
```

<details>
  <summary>values.yaml voorbeeld</summary>

```yml
gitlabUrl: https://gitlab.com/
runnerRegistrationToken: <registratietoken>
tags: "c2d,kubernetes,microk8s,c2d-ks1,io3,docker"
runners:
  config: |
    [[runners]]
      url = "https://gitlab.com/"
      token = "<registratietoken>"
      executor = "docker"
      [runners.kubernetes]
        image = "ubuntu:20.04"
        privileged = true
      [[runners.kubernetes.volumes.empty_dir]]
        name = "docker-certs"
        mount_path = "/certs/client"
        medium = "Memory"
```

</details>

## Registreer GitLab Runner

Voer vanuit `c2d-ks1` het volgende uit:

```bash
cd /vagrant/doc/howto-kubernetes-gitlab-runner/
helm3 install --namespace nja gitlab-runner -f values.yaml gitlab/gitlab-runner
```

## Verifieer

Controleer of de pod actief is met behulp van `kubectl` of het Kubernetes
Dashboard. Zie [Installeer het Kubernetes Dashboard]({{< relref path="dashboard.md" >}}) om te verifiëren dat de pod succesvol is geregistreerd
en draait.

```bash
kubectl config set-context --current --namespace=nja
kubectl get pods
kubectl logs -f gitlab-runner-...
```

<details>
  <summary><kbd>Toon mij</kbd></summary>

```bash
vagrant@c2d-ks1:~$ kubectl config set-context --current --namespace=nja
Context "microk8s" aangepast.
vagrant@c2d-ks1:~$ kubectl get pods
NAME                             READY   STATUS    RESTARTS   AGE
nj-7ff9d46d57-bp5fd              1/1     Running   0          64m
gitlab-runner-7b8b5b76c7-7zm2k   1/1     Running   0          63m
vagrant@c2d-ks1:~$ kubectl logs -f gitlab-runner-7b8b5b76c7-7zm2k
Registratiepoging 1 van 30
Runtime platform                                    arch=amd64 os=linux pid=14 revision=d540b510 version=15.9.1
WAARSCHUWING: Werken in gebruikersmodus.
WAARSCHUWING: De gebruikersmodus vereist dat je handmatig de bouwverwerking start:
WAARSCHUWING: $ gitlab-runner run
WAARSCHUWING: Gebruik sudo voor systeemmodus:
WAARSCHUWING: $ sudo gitlab-runner...

Unieke systeem-ID aangemaakt                        system_id=r_QVvM6mNZxsMT
Configuratie uit sjabloonbestand "/configmaps/config.template.toml" samengevoegd
WAARSCHUWING: Ondersteuning voor registratie tokens en runner parameters in de 'register' opdracht is verouderd in GitLab Runner 15.6 en zal worden vervangen door ondersteuning voor authenticatietokens. Zie voor meer informatie https://gitlab.com/gitlab-org/gitlab/-/issues/380872
Runner registreren... gelukt                        runner=GR1348941H6iYssSA
Runner succesvol geregistreerd. Voel je vrij om het te starten, maar als het al draait, zou de configuratie automatisch opnieuw geladen moeten worden!
Configuratie (met het authenticatietoken) is opgeslagen in "/home/gitlab-runner/.gitlab-runner/config.toml"
Runtime platform                                    arch=amd64 os=linux pid=7 revision=d540b510 version=15.9.1
Multi-runner starten vanuit /home/gitlab-runner/.gitlab-runner/config.toml...  builds=0
WAARSCHUWING: Werken in gebruikersmodus.
WAARSCHUWING: Gebruik sudo voor systeemmodus:
WAARSCHUWING: $ sudo gitlab-runner...

Configuratie geladen                                builds=0
listen_address niet gedefinieerd, metrics & debug endpoints uitgeschakeld  builds=0
[session_server].listen_address niet gedefinieerd, sessie-eindpunten uitgeschakeld  builds=0
Creëren van executor providers                      builds=0
Controleren op jobs... ontvangen                    job=3894427599 repo_url=https://gitlab.com/c2platform/examples/nodejs-kubernetes.git runner=nq46x6Yh
Trace bijwerken naar coördinator...ok               code=202 job=3894427599 job-log=0-760 job-status=running runner=nq46x6Yh sent-log=0-759 status=202 Geaccepteerd update-interval=1m0s
WAARSCHUWING: Job mislukt: opdracht beëindigd met exitcode 2
  duration_s=49.327169521 job=3894427599 project=44071435 runner=nq46x6Yh
Trace bijwerken naar coördinator...ok               code=202 job=3894427599 job-log=0-10413 job-status=running runner=nq46x6Yh sent-log=760-10412 status=202 Geaccepteerd update-interval=3s
Job bijwerken...                                    bytesize=10413 checksum=crc32:2d0dbfb7 job=3894427599 runner=nq46x6Yh
Job indienen bij coördinator...geaccepteerd, maar nog niet voltooid  bytesize=10413 checksum=crc32:2d0dbfb7 code=202 job=3894427599 job-status= runner=nq46x6Yh update-interval=1s
Job bijwerken...                                    bytesize=10413 checksum=crc32:2d0dbfb7 job=3894427599 runner=nq46x6Yh
Job indienen bij coördinator...ok                   bytesize=10413 checksum=crc32:2d0dbfb7 code=200 job=3894427599 job-status= runner=nq46x6Yh update-interval=0s
WAARSCHUWING: Runner verwerken mislukt               builds=0 fout=opdracht beëindigd met exitcode 2 executor=kubernetes runner=nq46x6Yh
```

</details>

## Voeg tags toe

De `values.yaml` heeft tags, maar om de een of andere reden verschijnen ze niet
in GitLab. Ga naar
[c2platform/examples/nodejs-kubernetes](https://gitlab.com/c2platform/examples/nodejs-kubernetes)
en klik vervolgens op **Instellingen** → **CI/CD** → **Runners** en voeg de tags
toe:

```
c2d,kubernetes,microk8s,c2d-ks1,io3,docker
```

## Voer de pijplijn uit

In het project
[c2platform/examples/nodejs-kubernetes](https://gitlab.com/c2platform/examples/nodejs-kubernetes)
zou je nu de pijplijn moeten kunnen starten.