---
categories: ["Handleiding"]
tags: [TODO]
title: "GitLab gebruiken met Operator"
linkTitle: "GitLab gebruiken met Operator"
weight: 6
description: >
    Maak een GitLab-instantie aan met behulp van operators
---

---
Installeer GitLab in Kubernetes / c2d-ks1 met behulp van de [GitLab
Operator](https://docs.gitlab.com/operator/).

---

## Links

* [GitLab Operator | GitLab](https://docs.gitlab.com/operator/)

## Vereisten

* TODO Handleiding voor CodeReady Containers (CRC) ./howto-crc.md

## Installatie

[GitLab Operator](https://docs.gitlab.com/operator/) → [Installatie |
GitLab](https://docs.gitlab.com/operator/installation.html).

> nog niet geschikt voor productie

### Ingress (optioneel)

### cert-manager

Installeer via **OperatorHub** {{< external-link url="https://console-openshift-console.apps-crc.testing/operatorhub/ns/gitlab-system?keyword=cert-ma&details-item=cert-manager-community-operators-openshift-marketplace" text="cert-manager" htmlproofer_ignore="true" >}}[]()

1.11.0

https://docs.gitlab.com/operator/installation.html#cluster

Opmerking: links naar {{< external-link url="https://console-openshift-console.apps-crc.testing/operatorhub/ns/gitlab-system?keyword=gitlab&details-item=gitlab-operator-kubernetes-community-operators-openshift-marketplace" text="OperatorHub" htmlproofer_ignore="true" >}}

Aangepast GitLab Runner-image

## IngressClass

Zie {{< external-link url="https://console-openshift-console.apps-crc.testing/operatorhub/ns/gitlab-system?keyword=gitlab&details-item=gitlab-operator-kubernetes-community-operators-openshift-marketplace" text="OperatorHub" htmlproofer_ignore="true" >}}

> Een clusterbrede IngressClass moet worden gemaakt vóór het instellen van de
> Operator, aangezien OLM dit objecttype momenteel niet ondersteunt:

```yaml
apiVersion: networking.k8s.io/v1
kind: IngressClass
metadata:
  # Zorg ervoor dat deze waarde overeenkomt met `spec.chart.values.global.ingress.class`
  # in de GitLab CR in de volgende stap.
  name: gitlab-nginx
spec:
  controller: k8s.io/ingress-nginx
```