---
categories: ["Handleiding"]
tags: [gitlab, gitops, kubernetes, ansible, agent]
title: "Stel een GitLab GitOps-werkstroom voor Kubernetes in"
linkTitle: "GitLab GitOps voor Kubernetes"
weight: 3
description: >
    Maak een Kubernetes-cluster op node `c2d-ks1` en beheer het met behulp van de GitLab-agent.
---

Deze handleiding laat zien hoe we een [MicroK8s](https://microk8s.io/)
Kubernetes-cluster draaiend op `c2d-ks` kunnen beheren met behulp van een
**GitLab Agent** die in het cluster draait. Deze GitLab Agent-configuratie en
Kubernetes-manifestbestanden staan in een apart project
[`c2platform/examples/kubernetes/gitlab-gitops`]({{< relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-gitops" >}}). Dit is
gescheiden van het Ansible-project [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) dat wordt gebruikt om het cluster
aanvankelijk te creëren met behulp van Ansible.

## Overzicht

Dit voorbeeldproject gebruikt een **GitOps-werkstroom** om OpenShift-resources
in twee namespaces `nja` en `njp` te beheren. De eerste namespace is voor de
"acceptatie"-omgeving, de tweede voor "productie". Zie `manifests/staging.yml`
en `manifests/production.yml`. Het onderstaande diagram toont alleen `njp`. De
configuratie voor `nja` en `njp` is identiek, met uitzondering van het feit dat
`njp` is geconfigureerd om alleen afbeeldingen met het label `production` te
trekken. Dit project gebruikt het Docker-image van project
[`c2platform/examples/kubernetes/gitlab-docker-build`]({{< relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-docker-build" >}}).

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")

Boundary(kubernetes, "Kubernetes", $type="c2d-ks1") {
    Boundary(nja, "nja", $type="namespace") {
      Container(nj, "helloworld", "applicatie")
    }
    Boundary(gitlab_agent, "gitlab_agent", $type="namespace") {
        Container(agent, "Gitlab Agent", "openshift")
    }
}
Boundary(gitlab, "gitlab.com/c2platform", $type="") {
    Boundary(gitlab_gitops, "examples/kubernetes/gitlab-gitops", $type="") {
        Container(gitlab_gitops_repo, "Repository", "git")
    }
    Boundary(docker, "examples/kubernetes/gitlab-docker-build", $type="") {
        Container(registry, "Registry", "registry")
    }
}

Rel(engineer, gitlab_gitops_repo, "Push manifest veranderingen", "")
Rel(agent, gitlab_gitops_repo, "Pull manifest veranderingen", "")
Rel(agent, nja, "Beheer project / namespace", "")
Rel(nj, registry, "Pull productie image", "")
@enduml
```

## Vereisten

* [Stel Kubernetes in]({{< relref path="./kubernetes.md" >}})

## GitLab Agent

Om een **GitLab Agent** te registreren moet je een "toegangstoken" voor Ansible
configureren. In dit "ontwikkelings" project worden dit soort lokale geheimen
opgeslagen met var `c2_gitlab_agent_access_token` in het bestand
`group_vars/all/local_stuff.yml`. Zie [Lokale Zaken]({{< relref path="/docs/guidelines/dev/local-stuff" >}}).

Creëer een toegangstoken zie [Installeer GitLab Agent]({{< relref path="./gitlab-agent.md" >}}) voor meer informatie. Deze handleiding beschrijft
de handmatige installatie van een GitLab Agent.

Bijvoorbeeld, in project [`c2platform/examples/kubernetes/gitlab-gitops`]({{<
relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-gitops" >}})
navigeer naar **Infrastructuur** → **Kubernetes clusters**, klik op **c2d-mk8s**
en selecteer vervolgens het tabblad {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-gitops/-/cluster_agents/c2d-mk8s?tab=tokens" text="Toegangstokens" htmlproofer_ignore="true" >}} en klik op **Token
creëren**.

Maak (of update) het bestand `group_vars/all/local_stuff.yml` en voeg toe

```yaml
c2_gitlab_agent_access_token: <access-token-van-gitlab-project>
```

## Maak c2d-ks1

Voer de volgende stappen uit om de Kubernetes-node `c2d-ks1` te maken:

1. [Stel Kubernetes in]({{< relref path="./kubernetes.md" >}})
2. [Stel het Kubernetes Dashboard in]({{< relref path="./dashboard.md" >}})

## GitLab Agent

Zie [Installeer GitLab Agent]({{< relref path="./gitlab-agent.md" >}})

## Verifieer

Als de **GitLab Agent** voor het {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-gitops/" text="gitlab-gitops" >}} project succesvol is gemaakt, wordt een eenvoudige
applicatie geïmplementeerd die toegankelijk zou moeten zijn via {{<
external-link url="http://1.1.4.12:3000/" htmlproofer_ignore="true" >}} en {{<
external-link url="http://1.1.4.13:3000/" htmlproofer_ignore="true" >}}. Zie
[`c2platform/examples/kubernetes/gitlab-gitops`]({{< relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-gitops" >}}) voor meer
informatie.

Met `c2d-rproxy1` draaiend en geprovisioneerd zou je naar {{< external-link url="https://frontend-nja.k8s.c2platform.org/" text="https://frontend-nja.k8s.c2platform.org/" htmlproofer_ignore="true" >}} en
{{< external-link url="https://frontend-njp.k8s.c2platform.org/" text="https://frontend-njp.k8s.c2platform.org/" htmlproofer_ignore="true" >}}
kunnen gaan en het bericht zien

> Hallo Wereld!

Als je de Kubernetes Dashboard add-on hebt ingeschakeld, zou je naar {{<
external-link url="https://k8s.c2platform.org" text="Kubernetes Dashboard"
htmlproofer_ignore="true" >}} moeten kunnen navigeren en bijvoorbeeld de service
`frontend-service` in de namespace `nja` met externe endpoint {{< external-link url="http://1.1.4.12:3000/" htmlproofer_ignore="true" >}} moeten zien.

Met je browser kun je navigeren naar {{< external-link url="http://1.1.4.12:3000/" htmlproofer_ignore="true" >}} en de tekst zien. Of
gebruik `curl`

```bash
vagrant@c2d-ks1:~$ curl http://1.1.4.12:3000/
Hallo Wereld! Versie: 0.1.5vagrant@c2d-ks1:~$
```