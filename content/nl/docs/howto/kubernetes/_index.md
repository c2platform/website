---
categories: ["Handleiding"]
tags: ["handleiding", "kubernetes"]
title: "Kubernetes"
linkTitle: "Kubernetes"
weight: 3
description: >
    Instructies voor het opzetten van een lokale Kubernetes-instantie en het beheren ervan met {{< external-link url="https://www.ansible.com/" text="Ansible" htmlproofer_ignore="false" >}} en / of een {{< external-link url="https://about.gitlab.com/topics/gitops/" text="GitOps" htmlproofer_ignore="false" >}} workflow. Standaard is {{< external-link url="https://microk8s.io/" text="Microk8s" htmlproofer_ignore="false" >}}, maar {{< external-link url="https://www.rancher.com/" text="Rancher" htmlproofer_ignore="false" >}} wordt ook ondersteund.
---