---
categories: ["Handleiding"]
tags: [gitlab, ansible, kubernetes, pipeline]
title: "GitLab Pipelines in Kubernetes"
linkTitle: "GitLab Pipelines in Kubernetes"
weight: 5
description: >
    GitLab-pijplijnen uitvoeren in Kubernetes met behulp van een lokale GitLab-instantie `c2d-gitlab`.
---

Deze handleiding beschrijft hoe we GitLab CI/CD-pijplijnen kunnen draaien op
Kubernetes met behulp van een zelf beheerde / zelf gehoste GitLab
CE-installatie. Het verplaatst in feite het voorbeeldproject
[`examples/kubernetes/gitlab-docker-build`]({{< relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-docker-build" >}}) dat
gebruikmaakt van {{< external-link url="https://gitlab.com" text="gitlab.com" htmlproofer_ignore="false" >}} naar een lokale GitLab CE-instantie die draait op
`c2d-gitlab`.

## Overzicht

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")

Boundary(local, "lokaal", $type="advanced dev laptop") {
  Boundary(c2d_ks, "Kubernetes", $type="c2d-ks1") {
      Boundary(c2d_k8s_nja, "gitlab-runner", $type="namespace") {
          Container(runner, "Gitlab Runner", "")
      }
  }
  Boundary(gitlab, "gitlab.c2platform.org", $type="c2d-gitlab") {
      Boundary(njx, "c2platform/gitlab-docker-build", $type="") {
          Container(registry, "Registratie", "docker")
          Container(pipeline, "Pipeline", ".gitlab-ci.yml")
          Container(repo, "Repository", "git")
      }
  }
}

Rel(engineer, repo, "Codewijzigingen pushen", "")
Rel(engineer, pipeline, "Release maken \nof implementeren", "")
Rel_Right(repo, pipeline, "Triggeren", "")
Rel(pipeline, runner, "Docker \nbuild", "")
Rel(runner, registry, "Push \nimage", "")
@enduml
```

## Vereisten

Maak de reverse en forward proxy `c2d-rproxy1`.

```bash
c2
unset PLAY  # zorg dat alle plays draaien
vagrant up c2d-rproxy1
```

Voor meer informatie over de verschillende rollen die `c2d-rproxy1` vervult in
dit project:

* [Reverse Proxy en CA-server instellen]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [SOCKS-proxy instellen]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Beheer van servercertificaten als Certificeringsautoriteit]({{< relref path="/docs/howto/c2/certs">}})
* [DNS instellen voor Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}})

* Om ervoor te zorgen dat Kubernetes `gitlab.c2platform.org` kan oplossen, zie
  [DNS instellen voor Kubernetes]({{< relref path="./dns.md" >}}).
* GitLab CE draait op `c2d-gitlab`, zie [GitLab instellen]({{< relref path="../gitlab/gitlab.md" >}}).
* Kubernetes draait op `c2d-ks1`, zie [Kubernetes instellen]({{< relref path="../gitlab/gitlab.md" >}}).

Voor de eerste twee vereisten zou het uitvoeren van het commando `vagrant up
c2d-rproxy1 c2d-gitlab` voldoende moeten zijn.

## Aangepaste docker-in-docker (dind) afbeeldingen

Het project [`examples/kubernetes/gitlab-docker-build`]({{< relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-docker-build" >}}) dat
is geïmporteerd in onze lokale GitLab CE-instantie die draait op `c2d-ks1`
gebruikt aangepaste Docker-afbeeldingen, zie `.gitlab-ci.yml`. De gerelateerde
projecten die deze twee afbeeldingen maken zijn:

* [`c2platform/docker/c2d/dind`]({{< relref path="/docs/gitlab/c2platform/docker/c2d/dind" >}})
* [`c2platform/docker/c2d/docker`]({{< relref path="/docs/gitlab/c2platform/docker/c2d/docker" >}})

De enige aanpassing die in deze projecten is gedaan, is het importeren van de
c2d root ca bundle {{< external-link url="https://gitlab.com/c2platform/ansible/-/raw/master/.ca/c2/c2.crt" text="c2.crt" htmlproofer_ignore="false" >}}, zie {{< external-link url="https://gitlab.com/c2platform/docker/c2d/docker/-/blob/master/Dockerfile" text="Dockerfile" htmlproofer_ignore="false" >}}.

Zonder deze aanpassing zullen Docker-opdrachten mislukken met het bericht:

> x509: certificaat ondertekend door onbekende autoriteit

## GitLab project importeren

Maak een openbare namespace {{< external-link url="https://gitlab.c2platform.org/c2platform" htmlproofer_ignore="true" >}} en
vervolgens **Project importeren** → **Repository via URL**

|Eigenschap        |Waarde                                                                    |
|-----------------|-------------------------------------------------------------------------|
|URL              |{{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-docker-build.git" htmlproofer_ignore="true" >}}|
|Zichtbaarheidsniveau|Openbaar                                                                   |

## GitLab Runner

### Haal registratietoken op

Ga naar {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-docker-build" htmlproofer_ignore="false" >}} en **Instellingen** → {{< external-link url="https://gitlab.c2platform.org/c2platform/gitlab-docker-build/-/settings/ci_cd" text="CI/CD" htmlproofer_ignore="true" >}} en dan **Runners** en kopieer het
**registratietoken**.

### Bewerk `values.yml`

Plaats het registratietoken in `values.yml`.

```bash
vagrant ssh c2d-ks1
nano /vagrant/doc/howto-kubernetes-gitlab-local/values.yml  # en update registratietoken
```

### Runner installeren

Voer het script `gitlab-runner.sh` uit

```bash
source /vagrant/doc/howto-kubernetes-gitlab-local/gitlab-runner.sh
```

### Verifiëren

Controleer de log van de GitLab Runner pod. Het zou het volgende bericht moeten
tonen:

> Runner succesvol geregistreerd

<details>
  <summary><kbd>Toon mij</kbd></summary>

```bash
vagrant@c2d-ks1:~/scripts/microk8s/gitlab/gitlab-runner$ kubectl logs -f gitlab-runner-c46457f8b-6dr7g
Registratiepoging 1 van 30
Runtime platform                                    arch=amd64 os=linux pid=14 revision=456e3482 version=15.10.0
WAARSCHUWING: Draaien in gebruikersmodus.
WAARSCHUWING: De gebruikersmodus vereist dat u handmatig buildsverwerking start:
WAARSCHUWING: $ gitlab-runner run
WAARSCHUWING: Gebruik sudo voor systeemmodus:
WAARSCHUWING: $ sudo gitlab-runner...

WAARSCHUWING: Er lijkt een probleem te zijn met uw configuratie
jsonschema: '/runners' valideert niet met https://gitlab.com/gitlab-org/gitlab-runner/common/config#/$ref/properties/runners/type: verwachte array, maar kreeg null
Unieke systeem-ID gerecreëerd                    system_id=r_pjr46VlRtCXO
Samenvoegen van configuratie uit sjabloonbestand "/configmaps/config.template.toml"
WAARSCHUWING: Ondersteuning voor registratietokens en runnerparameters in het 'register'-commando is afgeschaft in GitLab Runner 15.6 en zal worden vervangen door ondersteuning voor authenticatietokens. Voor meer informatie, zie https://gitlab.com/gitlab-org/gitlab/-/issues/380872
Runner registreren... gelukt                     runner=GR1348941TvUfy4ig
Runner is succesvol geregistreerd. Voel je vrij om deze te starten, maar als hij al draait, zou de configuratie automatisch moeten worden herladen!

Configuratie (met het authenticatietoken) is opgeslagen in "/home/gitlab-runner/.gitlab-runner/config.toml"
Runtime platform                                    arch=amd64 os=linux pid=7 revision=456e3482 version=15.10.0
Meervoudige runners starten vanaf /home/gitlab-runner/.gitlab-runner/config.toml...  builds=0
WAARSCHUWING: Draaien in gebruikersmodus.
WAARSCHUWING: Gebruik sudo voor systeemmodus:
WAARSCHUWING: $ sudo gitlab-runner...

WAARSCHUWING: Er lijkt een probleem te zijn met uw configuratie
jsonschema: '/runners/0/kubernetes/node_tolerations' valideert niet met https://gitlab.com/gitlab-org/gitlab-runner/common/config#/$ref/properties/runners/items/$ref/properties/kubernetes/$ref/properties/node_tolerations/type: verwachte object, maar kreeg null
Configuratie geladen                                builds=0
listen_address niet gedefinieerd, metrics & debug endpoints uitgeschakeld  builds=0
[session_server].listen_address niet gedefinieerd, sessie-endpoints uitgeschakeld  builds=0
Initialiseren uitvoerderproviders                     builds=0
```
</details>

Als je de pod binnengaat, let dan op de bestanden
`/home/gitlab-runner/.gitlab-runner/certs/gitlab.c2platform.org.crt` en
`/home/gitlab-runner/.gitlab-runner/config.toml`

<details>
  <summary><kbd>Toon mij</kbd></summary>

```bash
vagrant@c2d-ks1:~$ kubectl exec -it gitlab-runner-c46457f8b-6dr7g -- sh
/ $ ls /home/gitlab-runner/.gitlab-runner/certs
gitlab.c2platform.org.crt
/ $ cat /home/gitlab-runner/.gitlab-runner/config.toml
concurrent = 10
check_interval = 30
log_level = "info"
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "gitlab-runner-c46457f8b-6dr7g"
  url = "https://gitlab.c2platform.org/"
  id = 1
  token = "yU1z6fGDjbr2pngVGh3A"
  token_obtained_at = 2023-03-28T05:57:34Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "kubernetes"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.kubernetes]
    host = ""
    bearer_token_overwrite_allowed = false
    image = "ubuntu:20.04"
    namespace = "gitlab-runner"
    namespace_overwrite_allowed = ""
    privileged = true
    node_selector_overwrite_allowed = ""
    pod_labels_overwrite_allowed = ""
    service_account_overwrite_allowed = ""
    pod_annotations_overwrite_allowed = ""
    [runners.kubernetes.pod_security_context]
    [runners.kubernetes.init_permissions_container_security_context]
    [runners.kubernetes.build_container_security_context]
    [runners.kubernetes.helper_container_security_context]
    [runners.kubernetes.service_container_security_context]
    [runners.kubernetes.volumes]

      [[runners.kubernetes.volumes.empty_dir]]
        name = "docker-certs"
        mount_path = "/certs/client"
        medium = "Memory"
    [runners.kubernetes.dns_config]
/ $
```
</details>

Natuurlijk zou je ook de runner onder de **Runners** sectie moeten zien via {{<
external-link
url="https://gitlab.c2platform.org/c2platform/gitlab-docker-build/-/settings/ci_cd"
text="CI/CD-instellingen" htmlproofer_ignore="true" >}}.

## Pipeline draaien

Nu er een projectrunner beschikbaar is, kunnen we de pipeline starten via
**CI/CD** → {{< external-link url="https://gitlab.c2platform.org/c2platform/gitlab-docker-build/-/pipelines" text="Pipelines" htmlproofer_ignore="true" >}}.

## Links

* {{< external-link url="https://docs.gitlab.com/runner/install/kubernetes.html#providing-a-custom-certificate-for-accessing-gitlab" text="GitLab Runner Helm Chart | GitLab" htmlproofer_ignore="false" >}}
* {{< external-link url="https://docs.gitlab.com/runner/configuration/tls-self-signed.html#trustin-tls-certificates-for-docker-and-kubernetes-executors" text="Zelfondertekende certificaten of aangepaste Certificeringsautoriteiten" htmlproofer_ignore="false" >}}