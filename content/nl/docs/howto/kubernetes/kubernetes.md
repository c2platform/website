---
categories: ["Handleiding"]
tags: [kubernetes, ansible, kubectl, vagrant, helm]
title: "Kubernetes Installeren"
linkTitle: "Kubernetes"
weight: 1
description: >
    Creëer Kubernetes cluster / instantie op `c2d-ks1`.
---

---
Deze handleiding beschrijft de creatie en het beheer van Kubernetes cluster /
instantie c2d-ks1 gebaseerd op {{< external-link url="https://microk8s.io/" text="MicroK8s" htmlproofer_ignore="false" >}}. Zie het GitLab-project
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})
voor meer informatie.

---

## Overzicht

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")

Boundary(local, "lokaal", $type="high-end dev laptop") {
    Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1")
    Container(c2d_ks1, "Kubernetes", "c2d_ks1")
}

Rel(engineer, c2d_rproxy1, "https://k8s.c2platform.org", "")
Rel(c2d_rproxy1, c2d_ks1,"","")
@enduml
```

<!-- include-start: howto-prerequisites.md -->
## Vereisten

Creëer de reverse en forward proxy `c2d-rproxy1`.

```bash
c2
unset PLAY  # zorg ervoor dat alle plays worden uitgevoerd
vagrant up c2d-rproxy1
```

Voor meer informatie over de verschillende rollen die `c2d-rproxy1` vervult in
dit project:

* [Reverse Proxy en CA-server instellen]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [SOCKS proxy instellen]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Servercertificaten beheren als een Certificaat Autoriteit]({{< relref path="/docs/howto/c2/certs">}})
* [DNS instellen voor Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}})
<!-- include-end -->

## Ansible Play

De Ansible play is `plays/mw/mk8s.yml`. Deze play bevat twee rollen gerelateerd
aan Kubernetes:
1. `c2platform.mw.microk8s` wordt gebruikt om MicroK8s te installeren.
1. `c2platform.mw.kubernetes` wordt gebruikt om het MicroK8s cluster te beheren.

Deze Ansible configuratie is in twee bestanden:
1. `group_vars/mk8s/main.yml`
1. `group_vars/mk8s/kubernetes.yml`

## Provision

Om de eerste Kubernetes node `c2d-ks1` te creëren:

```bash
vagrant up c2d-ks1
```

Met betrekking tot Kubernetes zal de `mk8s.yml` play de volgende stappen
uitvoeren:

1. Installeer MicroK8s en voeg `vagrant` toe aan de `microk8s` groep.
1. Schakel de `dns`, `registry`, `dashboard` en `metallb` add-on in.
1. De plugin `metallb` is geconfigureerd met IP-range `1.1.4.10-1.1.4.100`.
1. Configureer de `dns` add-on met `1.1.4.203:5353` als de enige DNS-server.

## Verifiëren

### Clusterstatus

Om de status van het cluster te controleren:

```bash
vagrant ssh c2d-ks1
microk8s status --wait-ready
```

Dit zou statusinformatie moeten outputten die laat zien dat het cluster
operationeel is.

<details>
  <summary><kbd>Laat zien</kbd></summary>

```bash
[:ansible-dev]└2 master(+3/-3,1)* 1 ± vagrant ssh c2d-ks1
Welcome to Ubuntu 22.04.1 LTS (GNU/Linux 5.19.0-32-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage
Last login: Tue Feb 28 06:40:07 2023 from 10.84.107.1
vagrant@c2d-ks1:~$ microk8s status --wait-ready
microk8s is running
high-availability: no
  datastore master nodes: 127.0.0.1:19001
  datastore standby nodes: none
addons:
  enabled:
    ha-cluster           # (core) Configure high availability on the current node
    helm                 # (core) Helm - the package manager for Kubernetes
    helm3                # (core) Helm 3 - the package manager for Kubernetes
  disabled:
    cert-manager         # (core) Cloud native certificate management
    community            # (core) The community addons repository
    dashboard            # (core) The Kubernetes dashboard
    dns                  # (core) CoreDNS
    gpu                  # (core) Automatic enablement of Nvidia CUDA
    host-access          # (core) Allow Pods connecting to Host services smoothly
    hostpath-storage     # (core) Storage class; allocates storage from host directory
    ingress              # (core) Ingress controller for external access
    kube-ovn             # (core) An advanced network fabric for Kubernetes
    mayastor             # (core) OpenEBS MayaStor
    metallb              # (core) Loadbalancer for your Kubernetes cluster
    metrics-server       # (core) K8s Metrics Server for API access to service metrics
    minio                # (core) MinIO object storage
    observability        # (core) A lightweight observability stack for logs, traces and metrics
    prometheus           # (core) Prometheus operator for monitoring and logging
    rbac                 # (core) Role-Based Access Control for authorisation
    registry             # (core) Private image registry exposed on localhost:32000
    storage              # (core) Alias to hostpath-storage add-on, deprecated
vagrant@c2d-ks1:~$
```

</details>

### Dashboard

Controleer of het dashboard functioneel is met `curl`, zoals hieronder getoond.
Dit zou HTML van het dashboard moeten opleveren.

```bash
curl https://1.1.4.155/ --insecure
```

Met het **Kubernetes Dashboard** operationeel, zou je naar {{< external-link url="https://k8s.c2platform.org/" htmlproofer_ignore="true" >}} moeten kunnen
navigeren en inloggen met een token. De waarde van het token kun je verkrijgen
met de onderstaande commando's.

```bash
vagrant ssh c2d-ks1
kubectl -n kube-system describe secret microk8s-dashboard-token
```

Zie [Het Kubernetes Dashboard instellen]({{< relref path="./dashboard.md" >}})
voor meer informatie.

### DNS

**CoreDNS** is geconfigureerd om een aangepaste DNS-server te gebruiken die
draait op `c2d-rproxy`. Je kunt met `dig` verifiëren dat `gitlab.c2platform.org`
oplosbaar is. Zie [DNS instellen voor Kubernetes]({{< relref path="./dns.md" >}}) voor meer informatie.

## Volgende stappen

### GitLab GitOps

Implementeer een eenvoudige applicatie met een GitOps workflow, zie [Een GitLab
GitOps workflow instellen voor Kubernetes]({{< relref path="./kubernetes-gitlab-gitops.md" >}}).

### Helm, kubectl

Op `c2d-ks1` zijn `kubectl`, `helm` en `helm3` beschikbaar en geconfigureerd om
met het cluster te werken.

<details>
  <summary><kbd>Laat zien</kbd></summary>

```bash
vagrant@c2d-ks1:~$ helm version
version.BuildInfo{Version:"v3.9.1+unreleased", GitCommit:"0b977ed36f2db4f947a7a107fc3f5298401c4a96", GitTreeState:"clean", GoVersion:"go1.19.5"}
vagrant@c2d-ks1:~$ helm3 version
version.BuildInfo{Version:"v3.9.1+unreleased", GitCommit:"0b977ed36f2db4f947a7a107fc3f5298401c4a96", GitTreeState:"clean", GoVersion:"go1.19.5"}
vagrant@c2d-ks1:~$
```

```bash
vagrant@c2d-ks1:~$ kubectl get namespaces
NAME                 STATUS   AGE
kube-system          Active   3h35m
kube-public          Active   3h35m
kube-node-lease      Active   3h35m
default              Active   3h35m
container-registry   Active   3h34m
metallb-system       Active   3h34m
gitlab-agent-c2d     Active   3h30m
gitlab-runners       Active   3h30m
nja                  Active   3h30m
njp                  Active   3h30m
vagrant@c2d-ks1:~$
```

</details>

## Bekende problemen

### ontbrekend profiel snap.microk8s.microk8s.

MicroK8s node `c2d-ks1` slaagt er soms niet in om een werkende Kubernetes te
produceren. Het commando `microk8s status --wait-ready` zal het bericht
weergeven

> ontbrekend profiel snap.microk8s.microk8s. Zorg ervoor dat de snapd.apparmor
> service is ingeschakeld en gestart

<details>
  <summary><kbd>Laat zien</kbd></summary>

```bash
vagrant@c2d-ks1:~$ microk8s status --wait-ready
ontbrekend profiel snap.microk8s.microk8s.
Zorg ervoor dat de snapd.apparmor service is ingeschakeld en gestart
vagrant@c2d-ks1:~$ sudo systemctl status apparmor.service
● apparmor.service - Load AppArmor profiles
     Loaded: loaded (/lib/systemd/system/apparmor.service; enabled; vendor preset: enabled)
     Active: active (exited) since Mon 2023-03-06 04:43:26 UTC; 20min ago
       Docs: man:apparmor(7)
             https://gitlab.com/apparmor/apparmor/wikis/home/
   Main PID: 99 (code=exited, status=0/SUCCESS)

Mar 06 04:43:26 c2d-ks1 apparmor.systemd[99]: Not starting AppArmor in container
```

</details>

Om dit op te lossen, voer de volgende commando’s uit

```bash
sudo apparmor_parser --add /var/lib/snapd/apparmor/profiles/snap.microk8s.*
sudo reboot now
```