---
categories: ["Handleiding"]
tags: [ansible, kubernetes, dns, dnsmasq, kubectl, dig]
title: "DNS instellen voor Kubernetes"
linkTitle: "DNS"
weight: 5
description: >
    Maak een DNS-server op `c2d-rproxy` zodat we bijvoorbeeld `c2platform.org` kunnen oplossen.
---

[Dnsmasq](https://dnsmasq.org/) wordt gebruikt om een DNS-server op `c2d-rproxy`
te maken. Zie de nodeconfiguratie in `Vagrantfile` die de play
`plays/mw/dnsmasq.yml` bevat. DNS is een secundaire functie van `c2d-rproxy`,
zie [SOCKS proxy instellen]({{< relref path="../c2/socks-proxy.md" >}}) voor de
twee andere functies. Typisch gebruikt dit project het bestand `/etc/hosts` om
"domeinnamen" op te lossen, zie `group_vars/all/hosts.yml`, behalve voor
Kubernetes. Voor Kubernetes wordt deze op Dnsmasq gebaseerde DNS-server
gebruikt. Zie het GitLab-project [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) voor meer informatie.

---

{{< under_construction >}}

## Vereisten

* [Stel omgekeerde proxy en CA-server in]({{< relref path="../c2/reverse-proxy.md" >}})
* [Kubernetes instellen]({{< relref path="./kubernetes.md" >}})

## Installatie

```bash
export PLAY=plays/mw/dnsmasq.yml  # optioneel
vagrant provision c2d-rproxy1
```

## Dnsmasq verifiëren

[Dnsmasq](https://dnsmasq.org/) is ingesteld om DNS-service te bieden voor het
domein `c2platform.org`. Deze configuratie is te vinden in het
`group_vars/dnsmasq/main.yml` bestand. Specifiek is er een enkel wildcard
DNS-record voor `c2platform.org` dat naar het IP-adres `1.1.4.205` wijst, wat in
feite betekent dat elke subdomein onder `c2platform.org` naar hetzelfde IP-adres
zal worden opgelost.

Je kunt dat bijvoorbeeld verifiëren met:

```bash
dig @1.1.4.205 -p 5353 c2platform.org
dig @1.1.4.205 -p 5353 whatever.c2platform.org
```

<details>
  <summary><kbd>Laat me zien</kbd></summary>

```bash
root@c2d-rproxy1:~# dig @1.1.4.205 -p 5353 c2platform.org

; <<>> DiG 9.11.3-1ubuntu1.18-Ubuntu <<>> @1.1.4.205 -p 5353 c2platform.org
; (1 server gevonden)
;; globale opties: +cmd
;; Antwoord gekregen:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 54953
;; vlaggen: qr aa rd ra; QUERY: 1, ANTWOORD: 1, AUTHORITY: 0, EXTRA: 1

;; OPT PSEUDOSECTION:
; EDNS: versie: 0, vlaggen:; udp: 4096
;; VRAAGSECTIE:
;c2platform.org.                        IN      A

;; ANTWOORDSECTIE:
c2platform.org.         0       IN      A       1.1.4.205

;; Query tijd: 0 msec
;; SERVER: 1.1.4.205#5353(1.1.4.205)
;; WANNEER: do mrt 23 05:59:52 UTC 2023
;; MSG GROOTTE ontvangen: 59

root@c2d-rproxy1:~#
```

```bash
root@c2d-rproxy1:~# dig @1.1.4.205 -p 5353 whatever.c2platform.org

; <<>> DiG 9.11.3-1ubuntu1.18-Ubuntu <<>> @1.1.4.205 -p 5353 whatever.c2platform.org
; (1 server gevonden)
;; globale opties: +cmd
;; Antwoord gekregen:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 27200
;; vlaggen: qr aa rd ra ad; QUERY: 1, ANTWOORD: 1, AUTHORITY: 0, EXTRA: 1

;; OPT PSEUDOSECTION:
; EDNS: versie: 0, vlaggen:; udp: 4096
;; VRAAGSECTIE:
;whatever.c2platform.org.       IN      A

;; ANTWOORDSECTIE:
whatever.c2platform.org. 0      IN      A       1.1.4.205

;; Query tijd: 0 msec
;; SERVER: 1.1.4.205#5353(1.1.4.205)
;; WANNEER: do mrt 23 06:04:21 UTC 2023
;; MSG GROOTTE ontvangen: 68

root@c2d-rproxy1:~#
```

</details>

## CoreDNS configureren

Met onze op Dnsmasq gebaseerde DNS-server actief, kunnen we nu de CoreDNS pod in
Kubernetes wijzigen om deze te gebruiken. Bewerk de `coredns` `configmap` en
configureer `1.1.4.205:5353` als onze enige DNS-server (verwijder `8.8.8.8`
etc.).

```bash
kubectl edit configmap coredns -n kube-system  # configureer 1.1.4.205:5353
microk8s stop && microk8s start
microk8s status --wait-ready
```

## CoreDNS verifiëren

Eerst moeten we het IP-adres van de CoreDNS pod vinden en vervolgens kunnen we
de `dig` tests opnieuw uitvoeren:

```bash
export COREDNS_IP=$(kubectl get pods -n kube-system | grep coredns- | awk '{print $1}' | xargs -I % sh -c 'kubectl get pod % -n kube-system -o jsonpath="{.status.podIP}"')
echo $COREDNS_IP
```

```bash
dig @$COREDNS_IP c2platform.org
dig @$COREDNS_IP whatever.c2platform.org
dig @$COREDNS_IP google.nl
```

<details>
  <summary><kbd>Laat me zien</kbd></summary>

```bash
vagrant@c2d-ks1:~$ dig @$COREDNS_IP c2platform.org

; <<>> DiG 9.18.1-1ubuntu1.3-Ubuntu <<>> @10.1.211.147 c2platform.org
; (1 server gevonden)
;; globale opties: +cmd
;; Antwoord gekregen:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 47079
;; vlaggen: qr aa rd ra; QUERY: 1, ANTWOORD: 1, AUTHORITY: 0, EXTRA: 1

;; OPT PSEUDOSECTION:
; EDNS: versie: 0, vlaggen:; udp: 1232
; COOKIE: a1154c86acbeba68 (weergegeven)
;; VRAAGSECTIE:
;c2platform.org.                        IN      A

;; ANTWOORDSECTIE:
c2platform.org.         5       IN      A       1.1.4.205

;; Query tijd: 0 msec
;; SERVER: 10.1.211.147#53(10.1.211.147) (UDP)
;; WANNEER: do mrt 23 10:44:35 UTC 2023
;; MSG GROOTTE ontvangen: 85
```

```bash
vagrant@c2d-ks1:~$ dig @$COREDNS_IP whatever.c2platform.org

; <<>> DiG 9.18.1-1ubuntu1.3-Ubuntu <<>> @10.1.211.147 whatever.c2platform.org
; (1 server gevonden)
;; globale opties: +cmd
;; Antwoord gekregen:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 56450
;; vlaggen: qr aa rd ra; QUERY: 1, ANTWOORD: 1, AUTHORITY: 0, EXTRA: 1

;; OPT PSEUDOSECTION:
; EDNS: versie: 0, vlaggen:; udp: 1232
; COOKIE: 1b2532719222bf31 (weergegeven)
;; VRAAGSECTIE:
;whatever.c2platform.org.       IN      A

;; ANTWOORDSECTIE:
whatever.c2platform.org. 5      IN      A       1.1.4.205

;; Query tijd: 0 msec
;; SERVER: 10.1.211.147#53(10.1.211.147) (UDP)
;; WANNEER: do mrt 23 10:45:04 UTC 2023
;; MSG GROOTTE ontvangen: 103

```

```bash
vagrant@c2d-ks1:~$ dig @$COREDNS_IP google.nl

; <<>> DiG 9.18.1-1ubuntu1.3-Ubuntu <<>> @10.1.211.147 google.nl
; (1 server gevonden)
;; globale opties: +cmd
;; Antwoord gekregen:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 62105
;; vlaggen: qr rd ra; QUERY: 1, ANTWOORD: 1, AUTHORITY: 0, EXTRA: 1

;; OPT PSEUDOSECTION:
; EDNS: versie: 0, vlaggen:; udp: 1232
; COOKIE: ea0bc03c335aa625 (weergegeven)
;; VRAAGSECTIE:
;google.nl.                     IN      A

;; ANTWOORDSECTIE:
google.nl.              30      IN      A       142.250.179.163

;; Query tijd: 0 msec
;; SERVER: 10.1.211.147#53(10.1.211.147) (UDP)
;; WANNEER: do mrt 23 10:45:23 UTC 2023
;; MSG GROOTTE ontvangen: 75
```

</details>