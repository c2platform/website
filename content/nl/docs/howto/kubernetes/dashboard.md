---
categories: ["Handleiding"]
tags: [ansible, kubernetes, dashboard, dns]
title: "Instellen van het Kubernetes Dashboard"
linkTitle: "Dashboard"
weight: 3
description: >
    Stel het Kubernetes Dashboard in en krijg toegang tot het dashboard.

---

---
Deze handleiding beschrijft hoe de {{< external-link url="https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/" text="Kubernetes Dashboard" htmlproofer_ignore="false" >}} kan worden
ingeschakeld / gebruikt / geïmplementeerd op `c2d-ks1`.

---

{{< alert title="Let op:" >}} In dit [c2platform/ansible]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) project is het dashboard standaard
ingeschakeld door Ansible. Dus deze handleiding hoeft niet meer uitgevoerd te
worden. {{< /alert >}}

## Dashboard inschakelen

**Belangrijk:**

```bash
vagrant ssh c2d-ks1
sudo su -
microk8s.enable dns dashboard
kubectl -n kube-system patch svc kubernetes-dashboard -p '{"spec":{"externalIPs":["1.1.4.155"]}}'
```

Om te verifiëren dat het dashboard werkt, voer de onderstaande opdracht uit.
Deze `curl` zou de HTML van het Kubernetes Dashboard moeten weergeven.

```bash
curl https://1.1.4.155/ --insecure  # verifieren
```

## Toegang via reverse proxy verifiëren

Navigeer naar {{< external-link url="https://k8s.c2platform.org" htmlproofer_ignore="true" >}} en log vervolgens in met behulp van een bearer
token. Je kunt het bearer token verkrijgen met de volgende opdracht:

```bash
kubectl -n kube-system describe secret microk8s-dashboard-token
```

## Herstarten

Er is een bekend probleem dat bij herstart van `c2d-ks1` MicroK8s niet start en
niet meer correct werkt:

```bash
root@c2d-ks1:~# microk8s start
missing profile snap.microk8s.microk8s.
Please make sure that the snapd.apparmor service is enabled and started
```

De oplossing is het uitvoeren van:

```bash
apparmor_parser --add /var/lib/snapd/apparmor/profiles/snap.microk8s.*
microk8s restart
```