---
categories: ["Handleiding"]
tags: [apache2, reverse-proxy, ca, ansible, c2, cd2, ri1, dns]
title: "Opzetten van Reverse Proxy en CA-server"
linkTitle: "Reverse Proxy en CA-server"
weight: 2
description: >
  Stel de reverse proxy en Certificate Authority (CA) server `c2d-rproxy1` in. Deze node is een vereiste voor een functionele ontwikkelomgeving omdat hij verschillende rollen vervult.
---

Deze handleiding beschrijft hoe de reverse proxy en Certificate Authority (CA) server `c2d-rproxy1` wordt gecreëerd. Deze node vervult verschillende rollen in dit project. Het is een [reverse proxy](https://en.wikipedia.org/wiki/Reverse_proxy) en vervult ook een cruciale secundaire rol als onze [eigen kleine CA](https://docs.ansible.com/ansible/latest/collections/community/crypto/docsite/guide_ownca.html), zodat we privésleutels, certificaataanvraagverzoeken, en certificaten kunnen maken voor veilige communicatie tussen nodes/componenten.

## Snelle setup

Je kunt **c2d-rproxy1** creëren met `vagrant up c2d-rproxy1`. Als je dat commando uitvoert, zal Vagrant de node maken met de **LXD provider** en dan zal de **Ansible provisioner** drie plays uitvoeren.

1. [core/cacerts_server]({{< siteBaseURL "ri1" >}}/plays/core/cacerts_server.yml)
2. [mw/reverse_proxy]({{< siteBaseURL "ri1" >}}/plays/mw/reverse_proxy.yml)
3. [mw/dnsmasq]({{< siteBaseURL "ri1" >}}/plays/mw/dnsmasq.yml)

Om een eenvoudige setup uit te voeren en deze drie plays uit te voeren:

```bash
unset PLAY  # zorg ervoor dat alle plays worden uitgevoerd
vagrant up c2d-rproxy1
```

De plays die voor elke node moeten worden uitgevoerd, zijn geconfigureerd in [Vagrantfile.yml]({{< siteBaseURL "ri1" >}}/Vagrantfile). Elke node heeft een `plays`-variabele die een lijst van uit te voeren plays bevat. Dit kan ook in afzonderlijke stappen worden gedaan met behulp van de `PLAY`-variabele.

Als je deze node in afzonderlijke stappen wilt voorzien, creëer dan eerst de node zonder provisioning:

```bash
vagrant up c2d-rproxy1 --no-provision
```

## Setup met setup play

```bash
PLAY=setup vagrant up c2d-rproxy1
```

De [setup]({{< siteBaseURL "ri1" >}}/plays/setup.yml) play bevat alle plays, net als de [site]({{< siteBaseURL "ri1" >}}/plays/site.yml). In de context van deze handleiding zullen beide dezelfde taken/stappen uitvoeren. Dit komt omdat deze automatisering eenvoudig is. Bij meer gecompliceerde producten zoals bijvoorbeeld de [ForgeRock]({{< siteBaseURL "ansible-collection-forgerock" >}}/README.md) tools zal een *setup* play fundamenteel anders zijn dan een *site* play.

## Setup

### CA-server

Let op de inhoud van de `.ca` map. Deze map bevat de CA-bestanden om onze certificaten te ondertekenen.

```bash
.ca/
└── c2
    ├── c2.crt
    ├── c2.csr
    └── c2.key
```

Opmerking: je kunt deze `.ca` map verwijderen, deze wordt opnieuw aangemaakt zodra je de node voorziet. De reden waarom we deze map in Git bewaren, is dat we het CA-certificaat niet telkens opnieuw willen moeten importeren wanneer we deze node opnieuw maken.

Nu kunnen we de CA-server voorzien.

```bash
export PLAY=plays/core/cacerts_server.yml
vagrant provision c2d-rproxy1
```

De [log]({{< siteBaseURL "ri1" >}}/howto-reverse-proxy/ca-server.txt) toont aan dat taken in verschillende rollen zoals [bootstrap]({{< siteBaseURL "ansible-collection-core" >}}/roles/bootstrap/README.md), [os_trusts]({{< siteBaseURL "ansible-collection-core" >}}/roles/os_trusts/README.md) worden uitgevoerd, maar voor deze handleiding is alleen de rol [cacerts2]({{< siteBaseURL "ansible-collection-core" >}}/roles/cacerts2/README.md) belangrijk. Het toont drie taken aan om de drie bestanden in de `.ca` directory te maken. Simpel genoeg.

De configuratie voor de CA-server staat in [group_vars/all/smallca.yml]({{< siteBaseURL "ri1" >}}/group_vars/all/smallca.yml)

```yaml
cacerts2_ca_server: "{{ groups['cacerts_server'][0] }}"
c2_cacerts2_ca_dir:
  default: /etc/ownca
  development: /vagrant/.ca
cacerts2_ca_dir: "{{ c2_cacerts2_ca_dir[c2_env]|default(c2_cacerts2_ca_dir['default']) }}"
cacerts2_ca_domain:
  common_name: c2
  cipher: auto
  passphrase: "{{ c2_cacerts2_ca_domain_passphrase }}"  # geheim zie kluis
  create: ['key','csr', 'crt', 'p12', 'pem']
```

De variabele `c2_env` is gedefinieerd in [group_vars/development.yml]({{< siteBaseURL "ri1" >}}/group_vars/development.yml). Variabelen met de prefix `c2_` zijn projectvariabelen en geen rolvariabelen. Zie [Variabelen naamgeving]({{< siteBaseURL "ri1" >}}/doc/guideline-vars.md). Let op nog een projectvariabele `c2_cacerts2_ca_dir`. Dit wordt gebruikt om `/vagrant/.ca` te definiëren als `cacerts2_ca_dir` voor "ontwikkeling". Dit is het standaard aankoppelpunt voor Vagrant in elke node, zie de [Vagrantfile]({{< siteBaseURL "ri1" >}}/Vagrantfile) die de volgende regel bevat.

```ruby
config.vm.synced_folder '.', '/vagrant'
```

Dus in deze Vagrant-gebaseerde "ontwikkel"-omgeving eindigen de CA-bestanden die op node **c2d-rproxy1** zijn gemaakt uiteindelijk op de host via `/vagrant` mount, zodat ze in Git kunnen worden opgeslagen.

### Reverse proxy

Nu voorzien we de reverse proxy. Let op: we gebruiken nu `vagrant provision` in plaats van `vagrant up`, omdat de node al is aangemaakt. Zie `vagrant help`.

```bash
export PLAY=mw/reverse_proxy
vagrant provision c2d-rproxy1
```

De [log]({{< siteBaseURL "ri1" >}}/howto-reverse-proxy/reverse-proxy.txt) toont nieuwe taken van [cacerts2]({{< siteBaseURL "ansible-collection-core" >}}/roles/cacerts2/README.md) die worden uitgevoerd, bijvoorbeeld

```
TASK [c2platform.core.cacerts2 : Create dir for key, crt etc] ******************
changed: [c2d-rproxy1 -> c2d-rproxy1]
```

Deze taak is "gedelegeerd" aan de CA-server, die in dit project dezelfde node is, zodat hij verschijnt als **c2d-rproxy1 -> c2d-rproxy1**. De `.ca` map heeft nu de bestanden en structuur zoals hieronder weergegeven. Sleutel en certificaten worden aangemaakt op de CA-server zodat ze hergebruikt kunnen worden.

```
.ca
└── c2
    ├── apache
    │   ├── c2-c2d-rproxy1.crt
    │   ├── c2-c2d-rproxy1.csr
    │   ├── c2-c2d-rproxy1.key
    │   ├── c2-c2d-rproxy1.p12
    │   └── c2-c2d-rproxy1.pem
    ├── c2.crt
    ├── c2.csr
    └── c2.key
```

Het aanmaken van certificaten wordt gestuurd door de configuratie in [group_vars/reverse_proxy/certs.yml]({{< siteBaseURL "ri1" >}}/group_vars/reverse_proxy/certs.yml).

```yaml
apache_cacerts2_certificates:
  - common_name: c2
    subject_alt_name:
    - "DNS:{{ c2_domain_name }}"
    - "DNS:{{ c2_env }}.{{ c2_domain_name }}"
    - "DNS:www.{{ c2_domain_name }}"
    - "DNS:www.{{ c2_env }}.{{ c2_domain_name }}"
    - "DNS:{{ c2_domain_name_helloworld }}"
    - "DNS:{{ c2_env }}.{{ c2_domain_name_helloworld }}"
    - "DNS:www.{{ c2_domain_name_helloworld }}"
    - "DNS:www.{{ c2_env }}.{{ c2_domain_name_helloworld }}"
    - "DNS:{{ ansible_hostname }}"
    - "DNS:{{ ansible_fqdn }}"
    - "IP:{{ ansible_eth1.ipv4.address }}"
    ansible_group: reverse_proxy
    deploy:
      key:
        dir: /etc/ssl/private
        owner: "{{ apache_owner }}"
        group: "{{ apache_group }}"
        mode: '640'
      crt:
        dir: /etc/ssl/certs
        owner: "{{ apache_owner }}"
        group: "{{ apache_group }}"
        mode: '644'
```

De `deploy`-variabele wordt gebruikt om te configureren waar sleutel en certificaten geplaatst zullen worden. Als je het commando uitvoert, kun je zien dat de sleutel en het certificaat daar zijn.

```bash
vagrant ssh c2d-rproxy1 -c 'sudo ls /etc/ssl/private /etc/ssl/certs | grep rproxy1'
```

<details>
  <summary>Voorbeeld output</summary>

```bash
[:ansible-dev]└3 master(+73/-150)* ± vagrant ssh c2d-rproxy1 -c 'sudo ls /etc/ssl/private /etc/ssl/certs | grep rproxy1'
c2-c2d-rproxy1.crt
c2-c2d-rproxy1.key
Connection to 10.176.104.153 closed.
```

</details>

Het certificaat en de sleutel worden gebruikt om een Apache `VirtualHost` te configureren. Dit staat in [group_vars/reverse_proxy/files.ymls]({{< siteBaseURL "ri1" >}}/group_vars/reverse_proxy/files.yml). Merk op dat de volgende regels in dat bestand staan.

```
SSLCertificateKeyFile {{ apache_cacerts2_certificates[0]['deploy']['key']['dest'] }}
SSLCertificateFile {{ apache_cacerts2_certificates[0]['deploy']['crt']['dest'] }}
```

De `dest` key is aangemaakt door de [cacerts2]({{< siteBaseURL "ansible-collection-core" >}}/roles/cacerts2/README.md) rol met behulp van de [c2platform.core.set_certificate_facts]({{< siteBaseURL "ansible-collection-core" >}}/plugins/modules/set_certificate_facts.py) module. Als je niet tevreden bent met het pad dat gegenereerd wordt door deze module, kun je je eigen pad opgeven.

```yaml
    deploy:
      key:
        dest: /etc/apache2/my.key
        dir: /etc/apache2/
```

<details>
  <summary>Volledig voorbeeld</summary>

```yaml
apache_cacerts2_certificates:
  - common_name: c2
    subject_alt_name:
    - "DNS:{{ c2_domain_name }}"
    - "DNS:{{ c2_env }}.{{ c2_domain_name }}"
    - "DNS:www.{{ c2_domain_name }}"
    - "DNS:www.{{ c2_env }}.{{ c2_domain_name }}"
    - "DNS:{{ c2_domain_name_helloworld }}"
    - "DNS:{{ c2_env }}.{{ c2_domain_name_helloworld }}"
    - "DNS:www.{{ c2_domain_name_helloworld }}"
    - "DNS:www.{{ c2_env }}.{{ c2_domain_name_helloworld }}"
    - "DNS:{{ ansible_hostname }}"
    - "DNS:{{ ansible_fqdn }}"
    - "IP:{{ ansible_eth1.ipv4.address }}"
    ansible_group: reverse_proxy
    deploy:
      key:
        dest: /etc/apache2/my.key
        dir: /etc/apache2/
        owner: "{{ apache_owner }}"
        group: "{{ apache_group }}"
        mode: '640'
      crt:
        dir: /etc/ssl/certs
        owner: "{{ apache_owner }}"
        group: "{{ apache_group }}"
        mode: '644'
```
</details>

### DNS instellen

```bash
export PLAY=plays/mw/dnsmasq.yml
vagrant provision c2d-rproxy1
```

Voor meer informatie zie [Handleiding DNS]({{< siteBaseURL "ri1" >}}/howto-dns.md)

## Verifiëren

Voer het commando `vagrant ssh c2d-rproxy1 -c 'curl https://c2platform.org/is-alive --insecure'` uit om te verifiëren dat Apache2 werkt.

```
[:ansible-dev]└3 master(+1/-1) 60 ± vagrant ssh c2d-rproxy1 -c 'curl https://c2platform.org/is-alive --insecure'
Apache is aliveConnection to 10.176.104.153 closed.
```