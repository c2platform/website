---
categories: ["Handleiding"]
tags: [socks, proxy, firefox, profiel, ssh]
title: "Configuratie van SOCKS Proxy"
linkTitle: "SOCKS Proxy"
weight: 3
description: >
  Stel een SOCKS-proxy en een Firefox-profiel in voor gemakkelijke toegang tot
  de ontwikkelomgeving.
---

Deze handleiding legt uit hoe je toegang krijgt tot de ontwikkelomgeving via een 
{{< external-link url="https://en.wikipedia.org/wiki/SOCKS" text="SOCKS proxy" >}},
die een alternatieve methode biedt voor toegang tot de omgeving via een webbrowser.
Dit is een alternatief voor het gebruik van de standaard eenvoudige HTTP(s) forward
proxy, zoals beschreven in
[Creëer de Reverse Proxy en Web Proxy]({{< relref path="/docs/howto/dev-environment/setup/rproxy1" >}})
en
[Configureer een FireFox Profiel]({{< relref path="/docs/howto/dev-environment/setup/firefox" >}}).
De SOCKS-proxy wordt opgezet via een SSH-verbinding met `c2d-rproxy1` en biedt
ondersteuning voor meer geavanceerde protocollen dan alleen HTTP en HTTPS.

## Vereisten

Creëer de reverse en forward proxy `c2d-rproxy1`.

```bash
c2
unset PLAY  # zorg ervoor dat alle plays worden uitgevoerd
vagrant up c2d-rproxy1
```

Voor meer informatie over de verschillende rollen die `c2d-rproxy1` vervult in dit project:

* [Setup Reverse Proxy en CA server]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [Setup SOCKS proxy]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Beheer van Servercertificaten als Certificaat Autoriteit]({{< relref path="/docs/howto/c2/certs">}})
* [Setup DNS voor Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}})

## Overzicht

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Ingenieur")

Boundary(local, "lokaal", $type="high-end ontwikkellaptop") {
    Container(ssh, "SSH", "")
    Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1")
    Container(c2d_gitlab, "GitLab", "c2d-gitlab")
    Container(c2d_awx, "AWX", "c2d-awx")
    Container(firefox, "FireFox", "")
}

Rel(ssh, c2d_rproxy1, "SOCKS proxy \nover SSH", "")
Rel(engineer, firefox, "https://gitlab.c2platform.org\nhttps://awx.c2platform.org\netc.", "")
Rel(engineer, ssh, "Maak proxy", "")
Rel_Left(firefox, ssh, "", "")
Rel(c2d_rproxy1, c2d_gitlab,"","")
Rel(c2d_rproxy1, c2d_awx,"","")
@enduml
```

## SSH-configuratie

Om de SOCKS-proxy op te zetten, voeg je de volgende sectie toe aan je `.ssh/config` bestand:

```
Host c2d_socks
  User vagrant
  IdentityFile ~/.vagrant.d/insecure_private_key
  StrictHostkeyChecking no
  UserKnownHostsFile /dev/null
  Hostname 1.1.4.205
  LogLevel INFO
  Compression yes
  DynamicForward 9903
  ServerAliveInterval 10
  ServerAliveCountMax 10
```

## De SOCKS Proxy starten

Nu kun je de SOCKS-proxy starten met het volgende commando:

```bash
ssh c2d_socks
```

## Een Firefox Profiel creëren

Vergelijkbaar met 
[Configureer een FireFox Profiel]({{< relref path="/docs/howto/dev-environment/setup/firefox" >}})
, maak je een Firefox-profiel aan. Configureer echter de **Netwerkinstellingen** zoals hieronder getoond:

|Eigenschap                      |Waarde                                                                          |
|--------------------------------|--------------------------------------------------------------------------------|
|Automatische proxyconfiguratie URL|https://gitlab.com/c2platform/ansible/-/raw/master/doc/howto-socks-proxy/c2d.pac|
|Proxy DNS bij gebruik van SOCKS v5 |✔                                                                               |
|Schakel DNS via HTTPS in       |✔                                                                               |

## Verificatie

Navigeer naar {{< external-link url="https://c2platform.org/is-alive" htmlproofer_ignore="true" >}}.
Als alles correct is ingesteld, zou je het volgende bericht moeten zien, en er
moeten geen beveiligings- of certificaatwaarschuwingen zijn, aangezien het certificaat
vertrouwd zou moeten worden:

> Apache is alive