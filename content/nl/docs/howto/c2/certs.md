---
categories: ["Handleiding"]
tags: [vagrant, ansible, certificaten, ca, eigenca]
title: "Beheren van Servercertificaten als Certificeringsinstantie"
linkTitle: "Beheer Certificaten als CA"
weight: 10
description: >
  Leer hoe je je eigen Certificeringsinstantie (CA) wordt en certificaten beheert voor verschillende diensten met behulp van de `cacerts2` Ansible-rol.
---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

{{< under_construction >}}

---
Deze gids legt het proces uit voor het opzetten van een eenvoudige Certificeringsinstantie (CA) en het effectief beheren van certificaten voor verschillende diensten. Het maakt gebruik van de `cacerts2` Ansible-rol uit de [`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}) collectie.

---

## Configuratie

Één node krijgt de rol van **CA Server** toegewezen waar certificaten worden gemaakt en opgeslagen. In dit project wordt **c2d-rproxy1** voor deze rol toegewezen. Dit is gedaan in `hosts-dev.ini` in [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}).

```yaml
[cacerts_server]
c2d-rproxy1
```

De configuratie voor de CA server bevindt zich in `group_vars/all/smallca.yml`.

```yaml
cacerts2_ca_server: "{{ groups['cacerts_server'][0] }}"
c2_cacerts2_ca_dir:
  default: /etc/ownca
  development: /vagrant/.ca
cacerts2_ca_dir: "{{ c2_cacerts2_ca_dir[c2_env]|default(c2_cacerts2_ca_dir['default']) }}"
cacerts2_ca_domain:
  common_name: c2
  cipher: auto
  passphrase: "{{ c2_cacerts2_ca_domain_passphrase }}"  # geheim zie vault
  create: ['key','csr', 'crt', 'p12', 'pem']
```

De variabele `c2_env` is gedefinieerd in `group_vars/development.yml`. Variabelen met de prefix `c2_` zijn projectvariabelen en geen rolvariabelen. Zie [Variabele prefix]({{< relref path="/docs/guidelines/coding/var-prefix.md" >}} "Richtlijn: Variabele Prefix").


## CA Server aanmaken

In `Vagrantfile.yml` is de CA server `c2d-rproxy1` als volgt gedefinieerd:

```yaml
  - name: rproxy1
    short_description: Reverse Proxy
    description: Op Apache gebaseerde reverse proxy
    box: ubuntu18-lxd
    ip-address: 1.1.4.205
    plays:
      - core/cacerts_server
      - mw/reverse_proxy
```

Let op dat deze node twee `plays` heeft waarvan één `core/cacerts_server` is.

```bash
vagrant up c2d-rproxy1
```

Opmerking: dit project bevat een `.ca` map met de CA-bestanden zoals hieronder weergegeven. De (CA) sleutels en certificaten worden opgeslagen in de root van het project in de map `.ca`. Op deze manier kan de CA-sleutel en -certificaat opnieuw worden gebruikt als je nodes vernietigt en aanmaakt.

```
.ca/
└── c2
    ├── c2.crt
    ├── c2.csr
    └── c2.key
```

Let op: als je deze `.ca` map verwijdert, zullen die CA-bestanden opnieuw worden aangemaakt door de `plays/core/cacerts_server` play. Dit is iets wat je misschien niet wilt, bijvoorbeeld als je `.ca/c2/c2.crt` in je internetbrowser hebt geïmporteerd.

## Reverse Proxy

Omdat `c2d-rproxy1` twee plays geconfigureerd heeft, hebben we nu ook een reverse proxy server gecreëerd met de `plays/mw/reverse_proxy.yml` play. Het uitvoeren van deze play heeft certificaten gecreëerd die door onze kleine CA zijn ondertekend. Deze zouden nu ook in de `.ca` map aanwezig moeten zijn zoals hieronder weergegeven. Opmerking: je kunt die certificaten verwijderen, de certificaten in de `apache` map op elk moment omdat ze opnieuw worden aangemaakt wanneer de `plays/mw/reverse_proxy.yml` play wordt uitgevoerd. De creatie van de reverse proxy servers wordt meer in detail uitgelegd in [Setup Reverse Proxy en CA server]({{< relref path="/docs/howto/c2/reverse-proxy.md" >}} "Handleiding: Setup Reverse Proxy en CA server").

```
.ca/
└── c2
    ├── apache
    │   ├── c2-c2d-rproxy1.crt
    │   ├── c2-c2d-rproxy1.csr
    │   ├── c2-c2d-rproxy1.key
    │   ├── c2-c2d-rproxy1.p12
    │   └── c2-c2d-rproxy1.pem
    ├── c2.crt
    ├── c2.csr
    └── c2.key
```