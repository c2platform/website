---
categories: ["Handleiding"]
tags: [awx, vagrant, ansible, virtualbox, iis, windows]
title: "Automatiseren van Stop-Start Procedures met Ansible"
linkTitle: "Stop-Start Automatisering"
weight: 10
description: >
  Leer hoe je stop-start routines efficiënt kunt automatiseren met een veelzijdige en aanpasbare Ansible-rol.
---

Projecten:
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

---
Deze handleiding legt uit hoe je de stop-, start- en herstartprocedures voor het "WinApp" systeem binnen de C2 referentie-implementatie en Ansible inventarisproject kunt automatiseren:
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}).
Het inventarisproject maakt gebruik van een veelzijdige `restart` rol uit de
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})
Ansible collectie. Deze rol stelt ons in staat om stop-start routines te implementeren door een lijst genaamd `restart_config` te configureren.

In het inventarisproject wordt deze lijst gebruikt om vijf verschillende routines te definiëren: `stop`, `start`, `restart`, `maintenance-start`, en `maintenance-stop`. Je kunt de configuratie vinden in `group_vars/winapp_system/restart_config.yml` binnen het
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible">}})
project. Het cruciale punt hier is dat het gebruik van een flexibele en generieke rol zoals `restart` het mogelijk maakt om stop-start routines te implementeren zonder dat er aparte Ansible rollen of collecties voor elke applicatie gemaakt of beheerd hoeven te worden. Eén goed ontworpen rol kan worden gebruikt voor honderden of zelfs duizenden applicaties, waardoor het geschikt is voor grotere organisaties met diverse en niet-gestandaardiseerde applicatieportfolio's.

{{< alert >}}
Bekijk een presentatie en YouTube-video gebaseerd op deze handleiding bij [Stop-start Automatisering](https://www.onknows.com/slides/uwv/start-stop-automation/).
{{< /alert >}}

---

## Overzicht

### WinApp Systeem

Het WinApp Systeem bestaat uit vier nodes:

1. Drie LXD containers: `c2d-rproxy1`, `c2d-tomcat1`, `c2d-db1`. Deze containers draaien respectievelijk een reverse proxy gebaseerd op Apache, een applicatieserver gebaseerd op Tomcat, en een PostgreSQL database.
2. Eén MS Windows VirtualBox VM die IIS draait.

```plantuml
@startuml winapp-system
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
!include <logos/kubernetes>
!include <cloudogu/tools/ansible>
!include <logos/gitlab>
!include <cloudinsight/iis>
!include <cloudinsight/tomcat>
!include <cloudinsight/postgresql>

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("xtop", $sprite="workstation", $legendText="virtual-desktop")
AddContainerTag("ansible_container", $sprite="ansible", $legendText="ansible")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
AddContainerTag("iis", $sprite="iis", $legendText="iis")
AddContainerTag("tomcat", $sprite="tomcat", $legendText="tomcat")
AddContainerTag("postgresql", $sprite="postgresql", $legendText="postgresql")

Person(user, "Gebruikers")

System_Boundary(winapp, "winapp" ) {
  Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1,lxd,linux", $tags="rproxy")
  Container(helloworld, "WinAppService1", "c2d-tomcat1,lxd,linux", $tags="tomcat")
  Container(winappservice, "WinAppService2", "c2d-winapp1,virtualbox,windows", $tags="iis")
  ContainerDb(winappdb, "WinAppDatabase", "c2d-db1,lxd,linux", $tags="postgresql")
}

Rel(user, c2d_rproxy1, "Toegang tot de WinApp applicatie", "*.c2platform.org\nhttps,443")
Rel(c2d_rproxy1, helloworld, "", "")
Rel(c2d_rproxy1, winappservice, "", "")
Lay_D(winappservice, winappdb)
@enduml
```

### Beheer van WinApp

Het onderstaande diagram illustreert de setup voor het beheren van de WinApp-applicatie. Met behulp van de Automation Controller van
[Ansible Automation Platform (AAP)]({{< relref path="/docs/howto/awx" >}}),
kan een engineer de WinApp applicatie stoppen en starten.

Wanneer de engineer het "stop" script uitvoert, doet de Automation Controller (AWX) het volgende:

1. Voert een **Source Update** uit, waarbij de nieuwste versie van de inhoud wordt gedownload van het Ansible inventaris project
   [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})
   gehost op
   {{< external-link url="https://gitlab.com/c2platform/ansible" text="GitLab.com" htmlproofer_ignore="false" >}}
2. Downloadt Ansible rollen en collecties van
   [Galaxy](https://galaxy.ansible.com/ui/search/?keywords=c2platform).
3. Voert een inventarisupdate uit op basis van `hosts-dev.ini`, waarbij informatie over servers en hosts wordt bijgewerkt terwijl configuraties van `group_vars` worden gelezen, inclusief de restart-configuratie in
   `group_vars/winapp_system/restart.yml`.
4. Voert een van de beschikbare Ansible scripts uit om het systeem te beheren, wat `c2d-rproxy1`, `c2d-tomcat1`, `c2d-winapp1`, en `c2d-db1` omvat. Vijf scripts komen overeen met elke routine die gedefinieerd is in
   `group_vars/winapp_system/restart.yml`.


| Routine             | Job Template                   | Script                      | Beschrijving                                                                               |
|---------------------|--------------------------------|-----------------------------|--------------------------------------------------------------------------------------------|
| Stop                | `c2d-winapp-stop`              | `stop.yml`                  | Toont een onderhoudsbericht en stopt WinApp diensten (IIS, Tomcat, en PostgreSQL).         |
| Start               | `c2d-winapp-start`             | `start.yml`                 | Verwijdert het onderhoudsbericht en start WinApp diensten.                                 |
| Herstel             | `c2d-winapp-restart`           | `restart.yml`               | Combineert taken van de stop- en start routine.                                            |
| Onderhouds Start    | `c2d-winapp-maintenance-start` | `maintenance-start.yml`     | Toont een onderhoudsbericht, blokkeert toegang voor eindgebruikers. Geen WinApp-diensten worden gestopt. |
| Onderhouds Stop     | `c2d-winapp-maintenance-stop`  | `maintenance-stop.yml`      | Verwijdert het onderhoudsbericht en maakt de applicatie weer beschikbaar voor eindgebruikers. |


```plantuml
@startuml stop-start-automation
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
!include <logos/kubernetes>
!include <cloudogu/tools/ansible>
!include <logos/gitlab>
!include <cloudinsight/iis>
!include <cloudinsight/tomcat>
!include <cloudinsight/postgresql>

'AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
'AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")
'AddRelTag("light", $textColor="#93c7fb")
'AddRelTag("ansible", $sprite="ansible", $legendText="ansible provisie")
'UpdateRelStyle("#042a4f", "#042a4f")

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("xtop", $sprite="workstation", $legendText="virtual-desktop")
AddContainerTag("ansible_container", $sprite="ansible", $legendText="ansible")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
AddContainerTag("iis", $sprite="iis", $legendText="iis")
AddContainerTag("tomcat", $sprite="tomcat", $legendText="tomcat")
AddContainerTag("postgresql", $sprite="postgresql", $legendText="postgresql")

AddExternalSystemTag("gitlab", $sprite="gitlab", $legendText="gitlab")
AddExternalSystemTag("ansible", $sprite="ansible", $legendText="ansible-awx")

Person(engineer, "TAM Engineer")

System_Ext(galaxy_website, "galaxy.ansible.com", "Opslaat alle open source Ansible automatisering \n( collecties, rollen )", $tags="ansible")
System_Ext(gitlab, "gitlab.com", "Opslaat alle C2 Platform Git repositories en projecten", $tags="gitlab")

System_Boundary(local, "c2platform.org" ) {
    Boundary(app, "winapp", $type="") {
        System(winapp_system, "WinApp", "c2d-rproxy1,c2d-tomcat1, c2d-winapp1, c2d-db1") {
        }
    }
    Boundary(mngt, "mngt", $type="") {
        'Container(vagrant, "Vagrant", "")
        Boundary(aap, "Ansible Automation Platform ( AAP )") {
            Container(c2d_awx_k8s, "Kubernetes", "c2d-awx1,lxd", $tags="k8s") {
                Container(awx, "Automation\nController", "", $tags="ansible_container")
            }
            Container(c2d_galaxy_k8s, "Kubernetes", "c2d-galaxy1,lxd", $tags="k8s") {
                Container(galaxy, "Automation\nHub", "", $tags="ansible_container")
            }
        }
    }
}

Rel(awx, gitlab, "Download C2 Ansible Inventarisproject")
Rel(awx, galaxy_website, "Download collecties / rollen")
Rel_L(gitlab, galaxy_website, "Publiceer C2 Ansible collectie\ngebruikmakend van CI/CD pijplijn")
'Lay_D(gitlab,galaxy_website)

Boundary(local, "local", $type="high-end dev laptop") {

}

Rel_R(awx, winapp_system,"Stop, start, herstart","ssh, winrm")
Rel_Down(engineer, awx, "Stop, start de WinApp applicatie", "awx.c2platform.org")
@enduml
```


<!-- include-start: howto-prerequisites.md -->
## Vereisten

Maak de reverse en forward proxy `c2d-rproxy1`.

```bash
c2
unset PLAY  # zorg ervoor dat alle scripts worden uitgevoerd
vagrant up c2d-rproxy1
```

Voor meer informatie over de verschillende rollen die `c2d-rproxy1` vervult in dit project:

* [Instellen Reverse Proxy en CA server]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [Instellen SOCKS-proxy]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Beheer van Server Certificaten als een Certificeringsautoriteit]({{< relref path="/docs/howto/c2/certs">}})
* [Instellen DNS voor Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}})
<!-- include-end -->

## Inrichten van WinApp

Voer de volgende opdrachten uit om de noodzakelijke nodes van het WinApp Systeem te maken en starten:

```bash
c2
export BOX="c2d-winapp1 c2d-db1 c2d-tomcat1"
vagrant up $BOX
```

## Inrichten van AWX

Om de Automation Hub (AWX) node `c2d-awx1` te maken, voer je de volgende opdracht uit:

```bash
c2
vagrant up c2d-awx1
```

Deze opdracht creëert een Kubernetes cluster en implementeert AWX erop met behulp van de AWX-operator.

Deze opdracht creëert een Kubernetes cluster en implementeert AWX erop met behulp van de AWX operator. Voor gedetailleerde instructies over het maken van deze node, raadpleeg
[De Automation Controller (AWX) instellen met Ansible]({{< relref path="/docs/howto/awx/awx" >}})

## Verifiëren van WinApp en AWX

Met de `c2d-rproxy1` node actief zou je toegang moeten hebben tot de volgende links:

1. {{< external-link url="https://winapp-iis.c2platform.org/is-alive" htmlproofer_ignore="true" >}}:
   Deze link zou "Apache is alive" moeten weergeven, wat aangeeft dat de reverse proxy `c2d-rproxy1` operationeel is.
2. {{< external-link url="https://winapp-tomcat.c2platform.org/" htmlproofer_ignore="true" >}}:
    Toegang tot deze link zal de Tomcat standaard pagina weergeven, wat bevestigt dat `c2d-tomcat1` draait.
3. {{< external-link url="https://winapp-iis.c2platform.org/" htmlproofer_ignore="true" >}}:
    Toegang tot deze link zou de standaard IIS homepagina moeten tonen, wat bevestigt dat IIS draait op `c2d-winapp1`.

Om de AWX-installatie te verifiëren:

1. Toegang de AWX web UI op
   {{< external-link url="https://awx.c2platform.org/" htmlproofer_ignore="true" >}}.
   Log in met de standaard inloggegevens: gebruikersnaam `admin` en wachtwoord `secret`.
2. Bevestig dat het AWX-dashboard zonder fouten laadt.

## Uitvoeren van Stop-Start Routines

Om de routines met AWX uit te voeren:

1. Open de AWX web UI op
   {{< external-link url="https://awx.c2platform.org/" htmlproofer_ignore="true" >}}
   en log in als `admin` met het wachtwoord `secret`.
2. Navigeer naar **Templates** en selecteer de juiste Job Template op basis van de routine die je wilt uitvoeren. Selecteer bijvoorbeeld `c2d-winapp-stop` voor de "Stop" routine.
3. Klik op **Launch** om de routine uit te voeren. Je kunt de voortgang van de job in AWX volgen.

### Stop WinApp

Om WinApp te stoppen start je de `c2d-winapp-stop` job. Na voltooiing ga je naar
{{< external-link url="https://winapp-tomcat.c2platform.org/" htmlproofer_ignore="true" >}} en
{{< external-link url="https://winapp-iis.c2platform.org/" htmlproofer_ignore="true" >}}
Nu is er een bericht:

> We komen snel terug!

Om de AWX job output te zien klik je op

<details>
  <summary><kbd>Toon output</kbd></summary>

```bash
==> c2d-rproxy1: Provisioner uitvoeren: shell...
    c2d-rproxy1: Uitvoeren: inline script
==> c2d-rproxy1: Provisioner uitvoeren: ansible...
    c2d-rproxy1: Ansible-playbook aan het uitvoeren...

PLAY [Stop] ********************************************************************

TASK [Verzamelen van Feiten] ***************************************************
ok: [c2d-rproxy1]

TASK [Verzamelen van feiten → restart_facts_hosts] *****************************

TASK [c2platform.core.facts : Feiten instellen] ********************************
ok: [c2d-rproxy1]

TASK [c2platform.core.facts : Feiten verzamelen] *******************************
ok: [c2d-rproxy1] => (item=c2d-rproxy1)
ok: [c2d-rproxy1 -> c2d-db1(1.1.4.207)] => (item=c2d-db1)
ok: [c2d-rproxy1 -> c2d-winapp1(1.1.8.146)] => (item=c2d-winapp1)
ok: [c2d-rproxy1 -> c2d-tomcat1(1.1.4.151)] => (item=c2d-tomcat1)

TASK [c2platform.core.facts : Feiten instellen] ********************************
skipping: [c2d-rproxy1]

TASK [c2platform.core.facts : Feiten instellen] ********************************
ok: [c2d-rproxy1]

TASK [c2platform.core.restart : Feit restart_hosts_unreachable instellen] *****
ok: [c2d-rproxy1]

TASK [c2platform.core.restart : Onbereikbare hosts tonen] **********************
skipping: [c2d-rproxy1]

TASK [c2platform.core.restart : Falen bij onbereikbare hosts] ******************
skipping: [c2d-rproxy1]

TASK [c2platform.core.restart : Feit restart_hosts_reachable instellen] ********
ok: [c2d-rproxy1]

TASK [c2platform.core.restart : include_tasks] *********************************
skipping: [c2d-rproxy1] => (item=Controleer Tomcat onderhoudsbericht → c2d-rproxy1)
skipping: [c2d-rproxy1] => (item=Controleer IIS onderhoudsbericht → c2d-rproxy1)
skipping: [c2d-rproxy1] => (item=Start PostgreSQL → c2d-db1)
skipping: [c2d-rproxy1] => (item=Wacht op PostgreSQL → c2d-db1)
skipping: [c2d-rproxy1] => (item=Start Tomcat → c2d-tomcat1)
skipping: [c2d-rproxy1] => (item=Start IIS → c2d-winapp1)
skipping: [c2d-rproxy1] => (item=Controleer IIS-poort gestart → c2d-winapp1)
skipping: [c2d-rproxy1] => (item=Wacht op IIS → c2d-winapp1)
skipping: [c2d-rproxy1] => (item=Stop onderhoud → c2d-rproxy1)
skipping: [c2d-rproxy1] => (item=Herlaad reverse proxy → c2d-rproxy1)
skipping: [c2d-rproxy1] => (item=Controleer Tomcat online → c2d-rproxy1)
skipping: [c2d-rproxy1] => (item=Controleer IIS onderhoudsbericht → c2d-rproxy1)
included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/restart/tasks/copy.yml voor c2d-rproxy1 => (item=Start onderhoud → c2d-rproxy1)
included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/restart/tasks/service.yml voor c2d-rproxy1 => (item=Herlaad reverse proxy → c2d-rproxy1)
included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/restart/tasks/win_service.yml voor c2d-rproxy1 => (item=Stop IIS → c2d-winapp1)
included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/restart/tasks/win_wait_for.yml voor c2d-rproxy1 => (item=Controleer IIS poort geleegd → c2d-winapp1)
included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/restart/tasks/service.yml voor c2d-rproxy1 => (item=Stop Tomcat → c2d-tomcat1)
included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/restart/tasks/service.yml voor c2d-rproxy1 => (item=Stop PostgreSQL → c2d-db1)

TASK [c2platform.core.restart : Start onderhoud] ********************************
changed: [c2d-rproxy1]

TASK [c2platform.core.restart : Herlaad reverse proxy] **************************
changed: [c2d-rproxy1]

TASK [c2platform.core.restart : Stop IIS] ***************************************
changed: [c2d-rproxy1 -> c2d-winapp1(1.1.8.146)]

TASK [c2platform.core.restart : Controleer IIS poort geleegd] *******************
ok: [c2d-rproxy1 -> c2d-winapp1(1.1.8.146)]

TASK [c2platform.core.restart : Stop Tomcat] ************************************
changed: [c2d-rproxy1 -> c2d-tomcat1(1.1.4.151)]

TASK [c2platform.core.restart : Stop PostgreSQL] ********************************
changed: [c2d-rproxy1 -> c2d-db1(1.1.4.207)]

PLAY RECAP *********************************************************************
c2d-rproxy1                : ok=18   changed=5    unreachable=0    failed=0    skipped=3    rescued=0    ignored=0
```

</details>

</br>

Wanneer een engineer de "stop" routine voor de WinApp applicatie uitvoert, worden de volgende stappen ondernomen:

1. **Start onderhoud**: Deze taak wordt uitgevoerd op de `c2d-rproxy1` server. Het omvat het wijzigen van de Apache2 reverse proxy configuratie om een onderhoudsbericht weer te geven aan eindgebruikers die proberen toegang te krijgen tot de applicatie. Het onderhoudsbericht luidt: "We’ll be back soon."
2. **Herlaad reverse proxy**: Ook uitgevoerd op `c2d-rproxy1`, herlaadt deze taak de Apache2 service om het onderhoudsbericht te activeren. Deze actie blokkeert effectief de toegang van eindgebruikers tot de applicatie tijdens het onderhoud.
3. **Stop IIS**: Deze taak wordt uitgevoerd op `c2d-winapp1`, de server die de IIS (Internet Information Services) service voor de WinApp applicatie host. De IIS service wordt gestopt om te voorkomen dat het inkomende verzoeken verwerkt.
4. **Controleer IIS poort geleegd**: Ook op `c2d-winapp1`, monitort Ansible de IIS-poort, wachtend op leegloop. Deze stap zorgt ervoor dat er geen actieve verbindingen meer zijn op de IIS-service vóór het doorgaan met verdere onderhoudstaken.
5. **Stop Tomcat**: Op `c2d-tomcat1`, de server die de Tomcat-service voor de WinApp applicatie host, wordt de Tomcat-service gestopt. Deze actie stopt de verwerking van Java-gebaseerde componenten van de applicatie.
6. **Stop PostgreSQL**: Op `c2d-db1`, de databaseserver voor de WinApp applicatie, wordt de PostgreSQL-service gestopt. Deze actie voorkomt database-transacties of updates terwijl het onderhoud gaande is.

```plantuml
@startuml winapp-stop-sequence

!define ArrowColor #000000
!define BoxColor #A9DCDF
!define ActorColor #FFA500
!define ActorFontSize 14
!define BoxFontSize 14

participant "c2d-rproxy1 (delegator)" as Orchestrator
participant "c2d-rproxy" as TargetRProxy
participant "c2d-winapp1" as TargetWinApp
participant "c2d-tomcat1" as TargetTomcat
participant "c2d-db1" as TargetDB

'autonumber
Orchestrator -> TargetRProxy : 1. Start Onderhoud
TargetRProxy -> Orchestrator : Onderhoud Compleet
Orchestrator -> TargetRProxy : 2. Herlaad Reverse Proxy
TargetRProxy -> Orchestrator : Herladen Compleet
Orchestrator -> TargetWinApp : 3. Stop IIS
TargetWinApp -> Orchestrator : IIS Gestopt
Orchestrator -> TargetWinApp : 4. Controleer IIS Poort Geleegd
TargetWinApp -> Orchestrator : IIS-poort Geleegd
Orchestrator -> TargetTomcat : 5. Stop Tomcat
TargetTomcat -> Orchestrator : Tomcat Gestopt
Orchestrator -> TargetDB : 6. Stop PostgreSQL
TargetDB -> Orchestrator : PostgreSQL Gestopt
@enduml
```

</br></br>


### Start, Herstel etc.

De functionaliteit en werking van de `start`, `restart`, `maintenance-start`, en
`maintenance-stop` routines zijn eenvoudig en intuïtief, zoals gedefinieerd in het
`restart_config`. Daarom gaan we niet gedetailleerd in op de stappen voor elk van
deze routines afzonderlijk.

Voel je vrij om deze routines te verkennen en te experimenteren op basis van jouw specifieke behoeften en configuraties. Je kunt deze routines eenvoudig starten en testen om hun gedrag te observeren en hun functionaliteit te verifiëren. Deze hands-on benadering geeft je een praktische kennis van hoe ze werken binnen jouw omgeving.

Door gebruik te maken van de flexibele `restart` rol en de configuratieopties die beschikbaar zijn
in `restart_config`, heb je de mogelijkheid om deze routines aan te passen en te verfijnen aan de unieke behoeften van jouw organisatie. Deze veelzijdigheid maakt het
gemakkelijk om een breed scala aan applicaties efficiënt te beheren.

## Beheer met Vagrant

Je hebt ook de mogelijkheid om deze scripts direct vanuit de opdrachtregel
met behulp van Vagrant uit te voeren. Deze benadering biedt een handige manier om WinApp te beheren.

```bash
c2
PLAY=plays/mgmt/restart/stop.yml \
vagrant provision c2d-rproxy1 | tee provision.log
```

## Beheer van WinApp met behulp van Ansible CLI

Als je dat verkiest, kun je ervoor kiezen om deze routines uit te voeren met behulp van de Ansible CLI zonder gebruik te maken van Vagrant.

```bash
c2
ansible-playbook plays/mgmt/restart/restart.yml -i hosts-dev.ini
```

Zorg ervoor dat je de juiste SSH-configuratie in je `~/.ssh/config` bestand hebt, wat essentieel is voor een soepele uitvoering. Hier is een voorbeeldconfiguratie:


```
Host c2d-*
  ProxyCommand ssh 1.1.4.205 -W %h:%p
  User vagrant
  IdentityFile ~/.vagrant.d/insecure_private_key
  StrictHostkeyChecking no
  UserKnownHostsFile /dev/null
  LogLevel INFO
  Compression yes
  ServerAliveInterval 10
  ServerAliveCountMax 10
```

Voor meer details kun je de documentatie raadplegen over
[Ansible gebruiken zonder Vagrant]({{< relref path="/docs/howto/dev-environment/ansible-without-vagrant" >}}).

## Beoordeling

In het C2 Inventaris project
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}).

1. Het bestand `group_vars/winapp_system/restart_config.yml` dient als het
   configuratiecentrum voor het definiëren van de vijf "stop-start" routines met behulp van de
   `restart_config` lijst. Deze routines worden geïmplementeerd via verschillende Ansible
   modules, inclusief `copy`, `service`, `uri`, `wait_for`, `win_powershell`,
   `win_service` en `win_wait_for`.
2. In de `plays/mgmt/restart` map is er een apart script gemaakt voor elke
   routine. Deze scripts bevatten uitsluitend de restart rol, een integraal
   onderdeel van de Ansible collectie
   [`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}).

Binnen het C2 Ansible Collectie project
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}):

1. De `restart` Ansible rol speelt een centrale rol in het orkestreren van deze
   routines. Deze rol maakt gebruik van een reeks Ansible modules om de nodige
   taken efficiënt en effectief uit te voeren.

Deze goed gestructureerde configuraties en rollen stroomlijnen het beheer van het
WinApp systeem, wat de automatisering en controle verbetert.