---
categories: ["Richtlijn", "Voorbeeld"]
tags: [dind, docker, docker-in-docker, gitlab, pipeline, ci]
title: "Wat is Docker-in-Docker (DinD)?"
linkTitle: "DinD?"
weight: 1
description: >
  Docker-in-Docker (DinD) is een veelgebruikte techniek in GitLab CI/CD pipelines op
  Kubernetes om Docker-opdrachten binnen Docker-containers uit te voeren.
---

Docker-in-Docker (DinD) is een techniek die wordt gebruikt in GitLab CI/CD pipelines op Kubernetes om Docker-opdrachten binnen Docker-containers uit te voeren. Hiermee kan men Docker-images bouwen, testen en uitvoeren in een geïsoleerde omgeving. Het voordeel van het gebruik van DinD is dat het een consistente en reproduceerbare omgeving biedt voor het bouwen en testen van Docker-images, en ervoor zorgt dat de pipeline draait in een gecontroleerde en voorspelbare omgeving. Het doel van DinD is om het instellen en beheren van Docker-images en containers in een CI/CD-pipeline op Kubernetes te vereenvoudigen, waardoor het gemakkelijker wordt om Docker-images te bouwen en te testen als onderdeel van het continue integratie- en leveringsproces. De uitdaging van het gebruik van DinD is dat het veiligheidsrisico's met zich mee kan brengen, omdat het vereist dat Docker binnen een Docker-container draait, wat mogelijk het host-systeem aan risico's kan blootstellen. Juiste veiligheidsmaatregelen, zoals het gebruik van geschikte Docker-images, het verzekeren van containerisolatie en het beheren van rechten, moeten worden geïmplementeerd om deze risico's te beperken. Daarnaast kan DinD aanvullende middelen en configuratie vereisen op het gebied van opslag, netwerken en Docker-configuraties om een soepele uitvoering binnen de Kubernetes-omgeving te garanderen.

Een voorbeeld van deze techniek is te vinden in het Ansible-project
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})
dat wordt gebruikt om de
[RWS Ansible Execution Environment]({{< relref path="/docs/howto/rws/exe-env" >}})
te beheren.