---
categories: ["Handleiding"]
tags: [dind, docker, docker-in-docker, gitlab, pipeline, ci]
title: "Docker-in-docker ( dind )"
linkTitle: "Docker-in-docker ( dind )"
weight: 15
description: >
  Voer DinD lokaal uit, bijvoorbeeld om een GitLab CI/CD-pijplijn te ontwikkelen die deze techniek gebruikt.
---

{{< under_construction >}}
<!-- TODO update n.a.v. o.a. RWS-216 -->

{{< alert title="Opmerking:" >}}
Deze handleiding maakt momenteel gebruik van de `c2d` en specifiek de `c2d-xtop`
omgeving, niet de RWS `gsd` ontwikkelomgeving, om de procedure voor het creëren en onderhouden
van deze Ansible uitvoering omgeving te beschrijven.
Echter, je kunt de stappen aanpassen voor elke machine met Docker, Python, en
Ansible geïnstalleerd.
{{< /alert >}}
Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

Als je DinD wilt gebruiken om bijvoorbeeld een uitvoering omgeving te bouwen, kan het
moeilijk zijn om de pijplijn te ontwikkelen door middel van trial-and-error. Commit en push en zie
de resultaten.

Het is zinvol om de code lokaal te ontwikkelen en te testen.

Deze handleiding laat zien hoe je lokaal kunt simuleren wat er in de pijplijn gebeurt
door een container `dind-service` te starten die een Docker-service aanbiedt en waarmee je
Docker kunt gebruiken binnen een container.

---

Je kunt `c2d-xtop` gebruiken om te experimenteren met DinD. Voer de onderstaande command uit
om dit te maken. Zie het playbook `plays/dev/xtop.yml` en de configuratie
`group_vars/xtop/main.yml`.

```bash
vagrant up c2d-xtop
```

Het playbook start twee containers, eentje genaamd `dind-service`, en eentje genaamd `docker`.

```bash
vagrant@c2d-xtop:~$ docker ps
CONTAINER ID   IMAGE                                                   COMMAND                  CREATED          STATUS          PORTS           NAMES
5a8783ee37f4   docker:20.10.16                                         "docker-entrypoint.s…"   27 minutes ago   Up 27 minutes                   docker
8316dd08990c   registry.gitlab.com/c2platform/docker/c2d/dind:latest   "dockerd-entrypoint.…"   38 minutes ago   Up 38 minutes   2375-2376/tcp   dind-service
vagrant@c2d-xtop:~$
```

Binnen de `docker` container kun je Docker-commando's uitvoeren.

```bash
vagrant@c2d-xtop:~$ docker exec -it docker sh
/ # docker pull ubuntu
Using default tag: latest
latest: Pulling from library/ubuntu
2ab09b027e7f: Pull complete
Digest: sha256:67211c14fa74f070d27cc59d69a7fa9aeff8e28ea118ef3babc295a0428a6d21
Status: Downloaded newer image for ubuntu:latest
docker.io/library/ubuntu:latest
/ #
```

Als je de `c2d-gitlab` node hebt gemaakt, kun je die ook gebruiken

```bash
/ # docker pull registry.c2platform.org/c2platform/gitlab-docker-build:latest
latest: Pulling from c2platform/gitlab-docker-build
Digest: sha256:9a1289a3ae53088de59893efc9b998de1d6404bb99c7218119ea8d6bc22d9367
Status: Image is up to date for registry.c2platform.org/c2platform/gitlab-docker-build:latest
registry.c2platform.org/c2platform/gitlab-docker-build:latest
/ #
```

[Setup Project Directories and Install Ansible]({{< relref path="/docs/howto/dev-environment/setup/project" >}})

## Configureer Vagrant Sync Folder

[Vagrant Sync Folders]({{< relref path="/docs/guidelines/dev/vagrant-sync-folders" >}})

```bash
---
- src: ../c2/docker
  target: /home/vagrant/images
```

```bash
export IMAGE_NAME="gitlab-runner"
export IMAGE_VERSION="0.0.1"
export IMAGE="$IMAGE_NAME:$IMAGE_VERSION"
docker build -t $IMAGE . | tee build.log
docker run -it --rm $IMAGE /bin/bash
```

```bash
(c2) vagrant@c2d-xtop:~/images/gitlab-runner$ docker run -it --rm $IMAGE
root@2f3b45e29a2e:/# ansible --version
ansible [core 2.15.0]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /root/.virtualenv/c2d/lib/python3.10/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /root/.virtualenv/c2d/bin/ansible
  python version = 3.10.12 (main, Nov 20 2023, 15:14:05) [GCC 11.4.0] (/root/.virtualenv/c2d/bin/python)
  jinja version = 3.1.3
  libyaml = True
root@2f3b45e29a2e:/# ansible-builder --version
3.0.0
root@2f3b45e29a2e:/# git --version
git version 2.34.1
```

Test met RWS

```bash
vagrant provision c2d-xtop
vagrant ssh c2d-xtop
docker ps
source ~/.virtualenv/c2d/bin/activate
git clone https://gitlab.com/c2platform/rws/ansible-execution-environment.git
cd ansible-execution-environment
rm -rf context
ansible-builder create
ansible-builder build
```

https://docs.docker.com/go/buildx/

```yaml
---
collections:
  - name: ansible.windows
    version: 1.14.0
  - name: community.windows
    version: 2.0.0
  - name: awx.awx
    version: 22.4.0
  - name: checkmk.general
    version: 3.0.0
  - name: c2platform.core
    version: 1.0.8
  # - name: https://gitlab.com/c2platform/ansible-collection-core.git
  #  type: git
  #  version: master
  - name: c2platform.mgmt
    version: 1.0.2
  #- name: https://gitlab.com/c2platform/ansible-collection-mgmt.git
  #  type: git
  #  version: master
  - name: c2platform.gis
    version: 1.0.4
  #- name: https://gitlab.com/c2platform/rws/ansible-collection-gis.git
  #  type: git
 #   version: master
  - name: c2platform.wincore
    version: 1.0.5
  #- name: https://gitlab.com/c2platform/rws/ansible-collection-wincore.git
  #  type: git
  #  version: master
  - name: community.postgresql
    version: 2.4.3
```

(c2d) root@8ad3b66959cf:/ansible-execution-environment# rm -rf context
(c2d) root@8ad3b66959cf:/ansible-execution-environment# docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
(c2d) root@8ad3b66959cf:/ansible-execution-environment# ansible-builder create
Complete! De buildcontext is te vinden op: /ansible-execution-environment/context
(c2d) root@8ad3b66959cf:/ansible-execution-environment# ansible-builder build
Running command:
  docker build -f context/Dockerfile -t ansible-execution-env:latest context
Complete! De buildcontext is te vinden op: /ansible-execution-environment/context
(c2d) root@8ad3b66959cf:/ansible-execution-environment# docker images
REPOSITORY                                       TAG        IMAGE ID       CREATED         SIZE
ansible-execution-env                            latest     c26229e6f4d7   2 minutes ago   632MB
<none>                                           <none>     d772f9d37f78   3 minutes ago   638MB
<none>                                           <none>     3b2870c07300   5 minutes ago   286MB
ghcr.io/ansible-community/community-ee-minimal   2.15.4-2   649292486077   5 months ago    270MB
(c2d) root@8ad3b66959cf:/ansible-execution-environment#

## Stel een Python-omgeving in

Binnen `c2d-xtop`, voer het script `/vagrant/scripts/penv.sh` uit

```bash
source /vagrant/scripts/penv.sh
```

Installeer Ansible Builder en Navigator:

```bash
pip install ansible-builder
pip install ansible-navigator
```

## Clone Project

```bash
cd /tmp
git clone git@gitlab.com:c2platform/rws/ansible-execution-environment.git
cd ansible-execution-environment
```

## Bouw

```bash
rm -rf context
ansible-builder build --tag whatever_ee
```

## Verifieer

```bash
ansible-navigator run test_localhost.yml \
  --execution-environment-image whatever_ee --mode stdout
```

<details>
  <summary><kbd>Toon me</kbd></summary>

```bash
(c2) vagrant@c2d-xtop:/ansible-execution-environment$ ansible-navigator run test_localhost.yml \
  --execution-environment-image whatever_ee --mode stdout
-----------------------------------------------------
Overzicht uitvoering omgeving afbeelding en ophaalbeleid
-----------------------------------------------------
Naam uitvoering omgeving afbeelding:     whatever_ee:latest
Label uitvoering omgeving afbeelding:    latest
Ophaalargumenten voor uitvoering omgeving: None
Ophaalbeleid voor uitvoering omgeving:   tag
Ophaling van uitvoering omgeving nodig:  True
-----------------------------------------------------
Bijwerken van de uitvoering omgeving
-----------------------------------------------------
Uitvoeren van het commando: docker pull whatever_ee:latest
[WAARSCHUWING]: Er is geen inventaris geparsed, alleen impliciete localhost is beschikbaar
[WAARSCHUWING]: verstrekte hosts lijst is leeg, alleen localhost is beschikbaar. Let op dat
de impliciete localhost niet overeenkomt met 'all'

PLAY [Verzamel en print lokale Ansible / Python feitjes] ***************************

TASK [Feitjes verzamelen] *********************************************************
ok: [localhost]

TASK [Print Ansible versie] *******************************************************
ok: [localhost] => {
    "ansible_version": {
        "full": "2.15.0",
        "major": 2,
        "minor": 15,
        "revision": 0,
        "string": "2.15.0"
    }
}

TASK [Print Ansible Python] *******************************************************
ok: [localhost] => {
    "ansible_playbook_python": "/usr/bin/python3"
}

TASK [Print Ansible Python versie] ************************************************
ok: [localhost] => {
    "ansible_python_version": "3.11.4"
}

PLAY RECAP *********************************************************************
localhost                  : ok=4    veranderd=0    onbereikbaar=0    gefaald=0    overgeslagen=0    gered=0    genegeerd=0
```

</details>