---
categories: ["Handleiding"]
tags: []
title: "Hoe de C2 Platform te Gebruiken"
linkTitle: "C2"
weight: 19
description: >
  Stapsgewijze instructies met betrekking tot de referentie-implementatie van het C2 Platform (C2).
---

> De inhoud in deze sectie is specifiek gerelateerd aan de referentie-implementatie van het C2 Platform (C2), die binnen het [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) project is opgenomen.