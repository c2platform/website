---
categories: ["Handleiding"]
tags: [gitlab, token]
title: "GitLab Runner Instellen"
linkTitle: "GitLab Runner"
weight: 3
description: >
  Maak GitLab Runner node `c2d-gitlab-runner`.
---

Deze handleiding beschrijft hoe je een lokale GitLab instance
`c2d-gitlab-runner` aanmaakt. De play `plays/dev/gitlab_runner.yml` gebruikt de
Ansible rol {{< external-link url="https://galaxy.ansible.com/robertdebock/gitlab_runner" text="robertdebock.gitlab_runner" >}} om dit te realiseren. De configuratie voor
deze node bevindt zich in `group_vars/gitlab_runner/main.yml`.

## Registratietoken

Navigeer naar het GitLab project {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-robot" text="c2platform/examples/kubernetes/gitlab-robot" >}}, ga vervolgens naar
**Instellingen** → **CI/CD** → **Runners** en kopieer de "registratietoken".
Maak of bewerk het bestand `group_vars/all/local_stuff.yml` en configureer de
token bijvoorbeeld als volgt:

TODO meerdere projecten in tabel

```yaml
c2_gitlab_runner_registration_token: GR13******  # https://gitlab.com/c2projects/cgi/azure/-/settings/ci_cd
```

Zie [Local Stuff]({{< relref "/docs/guidelines/dev/local-stuff" >}} "Richtlijn:
Local Stuff") voor meer informatie over `local_stuff.yml`.

## Inrichting

```bash
vagrant up c2d-gitlab-runner
```

## Verifiëren

```bash
root@c2d-gitlab-runner:~# gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=4060 revision=dcfb4b66 version=15.10.1
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
c2d-gitlab-runner                                   Executor=shell Token=btPy-VV2HF2DxVxh1xzM URL=https://gitlab.com/
root@c2d-gitlab-runner:~#
```

## Verwijderen

```bash
gitlab-runner list
gitlab-runner unregister -c /etc/gitlab-runner/config.toml --url https://gitlab.com/ --token <token>
```

## Links

* {{< external-link url="https://docs.gitlab.com/runner/install/linux-repository.html" text="Installeer GitLab Runner met de officiële GitLab-repositories | GitLab" >}}
* {{< external-link url="https://github.com/robertdebock/ansible-role-gitlab_runner" text="robertdebock/ansible-role-gitlab_runner" >}}