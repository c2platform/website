---
categories: ["Handleiding"]
tags: ["handleiding", "gitlab"]
title: "GitLab"
linkTitle: "GitLab"
weight: 2
description: >
  Maak een GitLab instantie met container registry en een GitLab Runner.
---