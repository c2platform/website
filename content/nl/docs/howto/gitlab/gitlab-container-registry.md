---
categories: ["Handleiding"]
tags: [gitlab, container, registry, ansible, vagrant]
title: "Instellen van GitLab Container Registry"
linkTitle: "GitLab Container Registry"
weight: 2
description: >
  Maak GitLab Container Registry aan op node `c2d-gitlab`.
---

Deze handleiding beschrijft hoe je de [GitLab Container
Registry](https://docs.gitlab.com/ee/user/packages/container_registry/) kan
maken en gebruiken. Deze registry is niet standaard ingeschakeld in **GitLab
CE**, maar is in dit project standaard ingeschakeld door Ansible, zie
`group_vars/gitlab/registry.yml`.

## Vereisten

* [GitLab instellen]({{< relref "gitlab.md" >}} "Handleiding: GitLab instellen")

## Overzicht

In dit project benaderen we de GitLab registry via de URL {{< external-link url="https://registry.c2platform.org" htmlproofer_ignore="true" >}} via een
reverse proxy die draait op `c2d-rproxy`.

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")

Boundary(local, "local", $type="high-end dev laptop") {
    Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1")
    Container(c2d_gitlab, "GitLab", "c2d-gitlab")
}

' Rel(engineer, c2d_rproxy1, "https://gitlab.c2platform.org")
Rel(engineer, c2d_rproxy1, "https://registry.c2platform.org")
Rel_Right(c2d_rproxy1, c2d_gitlab, "")
@enduml
```

## GitLab starten

Start de GitLab-instantie die is aangemaakt met behulp van [GitLab
instellen]({{< relref "gitlab.md" >}} "Handleiding: GitLab instellen").

```bash
vagrant up c2d-gitlab
```

## Verifiëren

Als je een GitLab-project aanmaakt en navigeert naar **Packages and
registries**, zou je drie opties moeten zien, waarvan er één **Container
Registry** is.

### Inloggen en Pushen

Om te verifiëren dat de registry werkt, kun je proberen een afbeelding ernaartoe
te pushen. In het onderstaande voorbeeld gebruiken we de node
`c2d-gitlab-runner` hiervoor.

```bash
vagrant up c2d-gitlab-runner
```

Nu kunnen we testen of we kunnen inloggen op de registry:

```bash
vagrant@c2d-gitlab-runner:~$ sudo su -
root@c2d-gitlab-runner:~# docker login registry.c2platform.org
Username: root
Password:
WAARSCHUWING! Uw wachtwoord wordt onversleuteld opgeslagen in /root/.docker/config.json.
Configureer een credential helper om deze waarschuwing te verwijderen. Zie
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Inloggen geslaagd
root@c2d-gitlab-runner:~#
```

Download een afbeelding, bijvoorbeeld `docker pull ubuntu`, tag deze en push het
naar een GitLab-project. Bijvoorbeeld als we project {{< external-link url="https://gitlab.c2platform.org/c2platform/gitlab-docker-build" text="c2platform/gitlab-docker-build" htmlproofer_ignore="true" >}} hebben
aangemaakt als onderdeel van [GitLab instellen]({{< relref "gitlab.md" >}}
"Handleiding: GitLab instellen").

```
docker tag ubuntu:latest registry.c2platform.org/c2platform/gitlab-docker-build:latest
docker push registry.c2platform.org/c2platform/gitlab-docker-build:latest
```

### Pipeline

Als je de stappen volgt in [Handleiding om GitLab CE-pijplijnen in Kubernetes te
draaien](https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-kubernetes-gitlab-local.md),
wordt hiermee ook bevestigd dat de GitLab Container Registry correct is
ingeschakeld en werkt. TODO

## Links

* [plays/mgmt/gitlab.yml](https://gitlab.com/c2platform/ansible/-/blob/master/plays/mgmt/gitlab.yml)
* [geerlingguy.gitlab](https://galaxy.ansible.com/geerlingguy/ansible)
* [GitLab Container Registry |
GitLab](https://docs.gitlab.com/ee/user/packages/container_registry/) * [GitLab
Container Registry administratie |
GitLab](https://docs.gitlab.com/ee/administration/packages/container_registry.html#configure-container-registry-under-its-own-domain)

* {{< external-link url="https://docs.gitlab.com/runner/install/linux-repository.html" text="Installeer GitLab Runner met behulp van de officiële GitLab-repositories | GitLab" >}}
* {{< external-link url="https://github.com/robertdebock/ansible-role-gitlab_runner" text="robertdebock/ansible-role-gitlab_runner" >}}