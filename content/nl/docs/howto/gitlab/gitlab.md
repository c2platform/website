---
categories: ["Handleiding"]
tags: [gitlab, token]
title: "GitLab Instellen"
linkTitle: "GitLab"
weight: 1
description: >
  Creëer GitLab instantie `c2d-gitlab`.
---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

---
Deze handleiding beschrijft hoe je een lokale GitLab-instantie `c2d-gitlab` kunt
maken. De play `plays/mgmt/gitlab.yml` gebruikt de Ansible-rol {{< external-link url="https://galaxy.ansible.com/geerlingguy/ansible" text="geerlingguy.gitlab" >}} om het te creëren. De configuratie voor deze node bevindt zich in
`group_vars/gitlab/main.yml`. Na het uitvoeren van de stappen in deze
handleiding zou je naar {{< external-link url="https://gitlab.c2platform.org" htmlproofer_ignore="true" >}} moeten kunnen navigeren en inloggen als `root` met
het wachtwoord in `/etc/gitlab/initial_root_password`.

---

<!-- include-start: howto-prerequisites.md -->
## Vereisten

Creëer de reverse en forward proxy `c2d-rproxy1`.

```bash
c2
unset PLAY  # zorg ervoor dat alle plays worden uitgevoerd
vagrant up c2d-rproxy1
```

Voor meer informatie over de verschillende rollen die `c2d-rproxy1` in dit
project uitvoert:

* [Reverse Proxy en CA-server instellen]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [SOCKS proxy instellen]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Servercertificaten beheren als een Certificeringsinstantie]({{< relref path="/docs/howto/c2/certs">}})
* [DNS voor Kubernetes instellen]({{< relref path="/docs/howto/kubernetes/dns" >}})
<!-- include-end -->

## Overzicht

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")

Boundary(local, "lokaal", $type="high-end dev laptop") {
    Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1")
    Container(c2d_gitlab, "GitLab", "c2d-gitlab")
}

Rel(engineer, c2d_rproxy1, "https://gitlab.c2platform.org")
Rel_Right(c2d_rproxy1, c2d_gitlab, "")
@enduml
```

## Installatie

```bash
vagrant up c2d-gitlab
```

## Root-wachtwoord

De GitLab-installateur maakt een wachtwoord voor het `root`-account aan in een
bestand `/etc/gitlab/initial_root_password`. Het bestand wordt na 24 uur
verwijderd.

```bash
vagrant ssh c2d-gitlab -c "sudo cat /etc/gitlab/initial_root_password | grep Password:"
```

Via {{< external-link url="https://gitlab.c2platform.org/-/profile/password/edit" text="Profiel" htmlproofer_ignore="true" >}} wijzig het wachtwoord naar `supersecret`.

## Toegangstoken

Via **Profiel** → {{< external-link url="https://gitlab.c2platform.org/-/profile/personal_access_tokens" text="Toegangstokens" htmlproofer_ignore="true" >}} maak een toegangstoken aan.

## Verifiëren

Start de SOCKS proxy

```bash
ssh c2d_socks
```

Je zou moeten kunnen navigeren naar {{< external-link url="https://gitlab.c2platform.org" htmlproofer_ignore="true" >}} en inloggen
als `root`.

## Links

* {{< external-link url="https://about.gitlab.com/install/" text="Download en installeer GitLab" >}}
* {{< external-link url="https://docs.gitlab.com/ee/topics/offline/quick_start_guide.html" text="Aan de slag met een offline GitLab-installatie" >}}
* {{< external-link url="https://galaxy.ansible.com/geerlingguy/ansible" text="geerlingguy.gitlab" >}}