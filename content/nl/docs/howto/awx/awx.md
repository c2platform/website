---
categories: ["Handleiding"]
tags: [awx, ansible, operator, kubernetes]
title: "Instellen van de Automation Controller (AWX) met Ansible"
linkTitle: "Automation Controller (AWX)"
provisionTime: 9 minuten voorziening
weight: 1
description: >
   Leer hoe je moeiteloos de Ansible Automation Controller (AWX) kunt creëren met behulp van Ansible, met gebruik van de AWX Operator. Deze sectie begeleidt je ook bij het configureren van de controller met Ansible, inclusief het instellen van een organisatie, referenties, een Ansible Execution Environment, en een Job Template, die het inrichten van de Reverse Proxy vergemakkelijkt.
---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}),
[`c2platform.mgmt`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mgmt" >}}),
[`ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})

---
Deze handleiding biedt stapsgewijze instructies voor het maken van een [AWX]({{<
relref path="/docs/concepts/ansible/aap" >}}) instantie op de `c2d-awx1` node
met behulp van de {{< external-link url="https://github.com/ansible/awx-operator/blob/devel/.helm/starter/README.md" text="AWX Operator Helm Chart" htmlproofer_ignore="false" >}} en [Ansible]({{<
relref path="/docs/concepts/ansible" >}}). Hier volgt een overzicht van het
proces:

1. **Kubernetes Cluster Configuratie:** Ansible leidt de configuratie van een
   Kubernetes-instantie met behulp van de `microk8s` rol binnen de
   [`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}) collectie.
2. **Kubernetes Configuratie:** Met het Kubernetes-cluster ingesteld, gaat
   Ansible verder met de configuratie ervan, gebruikmakend van de `kubernetes`
   rol uit dezelfde [`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}) collectie.
3. **AWX Implementatie:** De implementatie van AWX op het Kubernetes-cluster
   wordt gecoördineerd met behulp van de AWX Operator Helm Chart. Ansible
   beheert naadloos dit implementatieproces.
4. **AWX Aanpassing:** Om de installatie te voltooien, wordt de
   [`c2platform.mgmt`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mgmt" >}}) collectie's `awx`
   rol gebruikt om de AWX-instantie naar de gewenste configuratie aan te passen.

Na succesvolle voltooiing van deze stappen, krijg je toegang tot de AWX
interface door de volgende URL te bezoeken: {{< external-link url="https://awx.c2platform.org" htmlproofer_ignore="true" >}}.

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

'LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
'!include <cloudogu/tools/ansible>
!include <logos/kubernetes>
!include <cloudogu/tools/ansible>

AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddRelTag("light", $textColor="#93c7fb")
AddRelTag("ansible", $sprite="ansible", $legendText="ansible provision")
UpdateRelStyle("#042a4f", "#042a4f")

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("xtop", $sprite="workstation", $legendText="virtual-desktop")
AddContainerTag("ansible_container", $sprite="ansible", $legendText="ansible")
AddSystemTag("ansible", $sprite="ansible", $legendText="ansible-awx", $bgColor="#999999")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
Person(engineer, "Engineer")

Person(engineer, "Engineer")

System_Boundary(local, "c2platform.org" ) {
    Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1,lxd", $tags="rproxy")
    Boundary(aap, "Ansible Automation Platform ( AAP )") {
      Container(c2d_awx_k8s, "Kubernetes", "c2d-awx1,lxd", $tags="k8s") {
          Container(awx, "Automation\nController", "", $tags="ansible_container")
      }
    }
}

System_Ext(galaxy_website, "galaxy.ansible.com", "Stores all open source Ansible automation \n( collections, roles )", $tags="ansible")

Rel(engineer, c2d_rproxy1, "Access\nAutomation Controller\nWeb Interface", "awx.c2platform.org,\nhttps,443")
Rel(c2d_rproxy1, awx, "", "")
Rel_R(awx, galaxy_website, "Download collections / roles", "https,port 443")
@enduml
```

<!-- include-start: howto-prerequisites.md -->
## Vereisten

Maak de reverse en forward proxy `c2d-rproxy1`.

```bash
c2
unset PLAY  # zorg ervoor dat alle plays draaien
vagrant up c2d-rproxy1
```

Voor meer informatie over de verschillende rollen die `c2d-rproxy1` in dit
project vervult:

* [Instellen Reverse Proxy en CA server]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [Instellen SOCKS proxy]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Beheer van Server Certificaten als een Certificaat Autoriteit]({{< relref path="/docs/howto/c2/certs">}})
* [DNS instellen voor Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}})
<!-- include-end -->

## Installatie

Om de Kubernetes-instantie te maken en de AWX-instantie te implementeren, voer
je het volgende commando uit:

```bash
vagrant up c2d-awx1
```

{{< alert type="waarschuwing" title="Waarschuwing!" >}} AWX kan enige tijd nodig
hebben om beschikbaar te worden, wat mogelijk kan leiden tot een Ansible-fout
tijdens stap 4, **AWX Aanpassing**. Mocht je een fout ondervinden, controleer
dan of je toegang hebt tot de AWX webinterface. Zodra je het inlogscherm ziet,
kun je het provisieproces opnieuw uitvoeren met het volgende commando:

```bash
vagrant provision c2d-awx1
```
{{< /alert >}}

## Verificatie

Om je AWX-instantie te verifiëren, volg je deze stappen:

### Verbinden

Als je `kubectl` op je host hebt geïnstalleerd, kun je ook toegang krijgen tot
het cluster door de volgende commando's uit te voeren:

```bash
export BOX=c2d-awx1
sudo snap install kubectl --classic
vagrant ssh $BOX -c "microk8s config" > ~/.kube/config
kubectl get all --all-namespaces
```

<p><details>
  <summary><kbd>Laat zien</kbd></summary><p>

```bash
NAMESPACE            NAME                                                  READY   STATUS      RESTARTS   AGE
kube-system          pod/hostpath-provisioner-766849dd9d-jdd5b             1/1     Running     0          17m
kube-system          pod/calico-node-vf72q                                 1/1     Running     0          17m
kube-system          pod/coredns-d489fb88-8xpwb                            1/1     Running     0          17m
kube-system          pod/calico-kube-controllers-d8b9b6478-2dj4s           1/1     Running     0          17m
container-registry   pod/registry-6674bf676f-twgpz                         1/1     Running     0          17m
kube-system          pod/dashboard-metrics-scraper-64bcc67c9c-5hbvz        1/1     Running     0          15m
kube-system          pod/kubernetes-dashboard-74b66d7f9c-8hqgf             1/1     Running     0          15m
metallb-system       pod/controller-56c4696b5-w2xbs                        1/1     Running     0          15m
metallb-system       pod/speaker-pkkpk                                     1/1     Running     0          15m
cert-manager         pod/cert-manager-cainjector-7985fb445b-jdkk6          1/1     Running     0          15m
cert-manager         pod/cert-manager-655bf9748f-6jxbg                     1/1     Running     0          15m
kube-system          pod/metrics-server-6b6844c455-7564v                   1/1     Running     0          15m
cert-manager         pod/cert-manager-webhook-6dc9656f89-br48w             1/1     Running     0          15m
awx                  pod/awx-operator-controller-manager-699d64766-ltpxh   2/2     Running     0          15m
awx                  pod/awx-postgres-15-0                                 1/1     Running     0          14m
awx                  pod/awx-web-6cb68b86cc-dp8tr                          3/3     Running     0          14m
awx                  pod/awx-migration-24.4.0-cljp4                        0/1     Completed   0          13m
awx                  pod/awx-task-5c96c47f86-r7vn7                         4/4     Running     0          14m

NAMESPACE            NAME                                                      TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                  AGE
default              service/kubernetes                                        ClusterIP      10.152.183.1     <none>        443/TCP                  17m
container-registry   service/registry                                          NodePort       10.152.183.38    <none>        5000:32000/TCP           17m
kube-system          service/kube-dns                                          ClusterIP      10.152.183.10    <none>        53/UDP,53/TCP,9153/TCP   17m
kube-system          service/metrics-server                                    ClusterIP      10.152.183.147   <none>        443/TCP                  16m
kube-system          service/kubernetes-dashboard                              ClusterIP      10.152.183.175   <none>        443/TCP                  16m
kube-system          service/dashboard-metrics-scraper                         ClusterIP      10.152.183.61    <none>        8000/TCP                 16m
metallb-system       service/webhook-service                                   ClusterIP      10.152.183.124   <none>        443/TCP                  16m
cert-manager         service/cert-manager                                      ClusterIP      10.152.183.51    <none>        9402/TCP                 15m
cert-manager         service/cert-manager-webhook                              ClusterIP      10.152.183.177   <none>        443/TCP                  15m
kube-system          service/dashboard-lb                                      LoadBalancer   10.152.183.133   1.1.4.16      443:32189/TCP            15m
awx                  service/awx-operator-controller-manager-metrics-service   ClusterIP      10.152.183.195   <none>        8443/TCP                 15m
awx                  service/awx-postgres-15                                   ClusterIP      None             <none>        5432/TCP                 14m
awx                  service/awx-service                                       ClusterIP      10.152.183.88    <none>        80/TCP                   14m
awx                  service/awx-service-lb                                    LoadBalancer   10.152.183.29    1.1.4.17      8052:32477/TCP           11m

NAMESPACE        NAME                         DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kube-system      daemonset.apps/calico-node   1         1         1       1            1           kubernetes.io/os=linux   17m
metallb-system   daemonset.apps/speaker       1         1         1       1            1           kubernetes.io/os=linux   16m

NAMESPACE            NAME                                              READY   UP-TO-DATE   AVAILABLE   AGE
kube-system          deployment.apps/calico-kube-controllers           1/1     1            1           17m
kube-system          deployment.apps/hostpath-provisioner              1/1     1            1           17m
kube-system          deployment.apps/coredns                           1/1     1            1           17m
container-registry   deployment.apps/registry                          1/1     1            1           17m
kube-system          deployment.apps/dashboard-metrics-scraper         1/1     1            1           16m
kube-system          deployment.apps/kubernetes-dashboard              1/1     1            1           16m
metallb-system       deployment.apps/controller                        1/1     1            1           16m
cert-manager         deployment.apps/cert-manager-cainjector           1/1     1            1           15m
cert-manager         deployment.apps/cert-manager                      1/1     1            1           15m
kube-system          deployment.apps/metrics-server                    1/1     1            1           16m
cert-manager         deployment.apps/cert-manager-webhook              1/1     1            1           15m
awx                  deployment.apps/awx-operator-controller-manager   1/1     1            1           15m
awx                  deployment.apps/awx-web                           1/1     1            1           14m
awx                  deployment.apps/awx-task                          1/1     1            1           14m

NAMESPACE            NAME                                                        DESIRED   CURRENT   READY   AGE
kube-system          replicaset.apps/calico-kube-controllers-d8b9b6478           1         1         1       17m
kube-system          replicaset.apps/hostpath-provisioner-766849dd9d             1         1         1       17m
kube-system          replicaset.apps/coredns-d489fb88                            1         1         1       17m
container-registry   replicaset.apps/registry-6674bf676f                         1         1         1       17m
kube-system          replicaset.apps/dashboard-metrics-scraper-64bcc67c9c        1         1         1       15m
kube-system          replicaset.apps/kubernetes-dashboard-74b66d7f9c             1         1         1       15m
metallb-system       replicaset.apps/controller-56c4696b5                        1         1         1       15m
cert-manager         replicaset.apps/cert-manager-cainjector-7985fb445b          1         1         1       15m
cert-manager         replicaset.apps/cert-manager-655bf9748f                     1         1         1       15m
kube-system          replicaset.apps/metrics-server-6b6844c455                   1         1         1       15m
cert-manager         replicaset.apps/cert-manager-webhook-6dc9656f89             1         1         1       15m
awx                  replicaset.apps/awx-operator-controller-manager-699d64766   1         1         1       15m
awx                  replicaset.apps/awx-web-6cb68b86cc                          1         1         1       14m
awx                  replicaset.apps/awx-task-5c96c47f86                         1         1         1       14m

NAMESPACE   NAME                               READY   AGE
awx         statefulset.apps/awx-postgres-15   1/1     14m

NAMESPACE   NAME                             COMPLETIONS   DURATION   AGE
awx         job.batch/awx-migration-24.4.0   1/1           102s       13m
```

</p></details></p>

Raadpleeg [Verbinding maken met een Kubernetes Cluster]({{< relref path="/docs/howto/dev-environment/k8s" >}}) voor meer informatie.

### Kubernetes Dashboard

1. Toegang tot het Kubernetes Dashboard door naar {{< external-link url="https://dashboard-awx.c2platform.org/" htmlproofer_ignore="true" >}} te
   gaan en in te loggen met een token. Verkrijg de token door het commando
   `kubectl` hieronder binnen de `c2d-awx1` node uit te voeren.
2. Eenmaal ingelogd, navigeer naar de {{< external-link url="https://dashboard-awx.c2platform.org/#/workloads?namespace=awx" text="awx" htmlproofer_ignore="true" >}} namespace. Controleer of de
   `awx-web` en `awx-task` pods zonder fouten draaien.

```bash
kubectl -n kube-system describe secret microk8s-dashboard-token
```

Voor meer informatie over het verkrijgen van de token en het instellen van het
Kubernetes Dashboard, raadpleeg de [Kubernetes Dashboard Instellen]({{< relref path="../kubernetes/dashboard" >}}) handleiding.

### AWX

1. Om toegang te krijgen tot de AWX webinterface, ga naar {{< external-link url="https://awx.c2platform.org" htmlproofer_ignore="true" >}}. Gebruik het
   `admin` account met het wachtwoord `secret` om in te loggen.
2. Eenmaal ingelogd, navigeer naar de {{< external-link url="https://awx.c2platform.org/#/templates" text="Templates" htmlproofer_ignore="true" >}} sectie.
3. Van daaruit kun je verschillende plays starten, zoals de reverse proxy play
   `c2-reverse-proxy` of een van de AWX plays `c2-awx` of `c2-awx-config`.

## Review

Om een beter begrip te krijgen van hoe de AWX-instantie wordt gecreëerd met
Ansible, kun je het volgende bekijken:

In het Ansible playbook project [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}):

1. `Vagrantfile.yml`: Dit bestand configureert de `c2d-awx1` node met de
   `mgmt/awx` playbook. Dit bestand wordt gelezen in de `Vagrantfile`.
2. `hosts-dev.ini`: Het inventarisbestand wijst de `c2d-awx1` node toe aan de
   `awx-new` Ansible groep en `localhost` aan de `awx_config` Ansible groep.

### AWX Play

1. `plays/mgmt/awx.yml`: Deze playbook stelt Ansible-rollen in voor de `awx-new`
   Ansible groep.
2. `group_vars/awx_new/main.yml`: Dit bestand bevat de hoofdconfiguratie voor
   Kubernetes, inclusief het inschakelen van add-ons zoals `dashboard` en
   `metallb` load balancer.
3. `group_vars/awx_new/files.yml`: Hier kun je de configuratie vinden voor het
   creëren van de AWX Helm Chart in het `/home/vagrant/awx-values.yml` bestand.
4. `group_vars/awx_new/kubernetes.yml`: Dit bestand bevat de configuratie om het
   Kubernetes-cluster in te stellen en AWX erop te installeren.
5. `group_vars/awx_new/awx.yml`: Dit bestand bevat de configuratie om AWX te
   configureren. Het stelt een organisatie, referenties, een execution
   environment en een job template in om de reverse proxy in te richten.

### AWX Configuratie Play

De **AWX Configuratie Play** is verantwoordelijk voor het beheren van AWX met
behulp van AWX zelf. Het is bedoeld om binnen de AWX-omgeving uitgevoerd te
worden. In tegenstelling tot de standaard [AWX Play](#awx-play), gebruikt deze
play minder rollen – specifiek, het omvat alleen `c2platform.core.secrets` en
`c2platform.mgmt.awx`. De scope is beperkt tot het configureren van AWX, en het
gaat ervan uit dat de AWX-instantie al is opgezet. Deze play dient als een
illustratief voorbeeld van hoe AWX autonoom zijn eigen opzet kan beheren.

1. `plays/mgmt/awx_config.yml`: Deze playbook stelt de Ansible-rollen vast die
   nodig zijn voor de `awx-config` play.
2. `group_vars/awx_config/main.yml`: Dit bestand bevat de
   configuratie-instellingen voor AWX-aanpassing. Het is een directe kopie van
   `group_vars/awx_new/awx.yml`. Opmerking: In een reële situatie zou het
   kopiëren van configuratiecode zoals deze niet worden aanbevolen.

Binnen het Ansible collectie project [c2platform.mw]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}):

1. De `c2platform.mw.microk8s` Ansible rol.
2. De `c2platform.mw.kubernetes` Ansible rol.

Binnen het Ansible collectie project [c2platform.mgmt]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mgmt" >}}):

1. De `c2platform.mgmt.awx` Ansible rol.

### AWX Configuratie

Binnen de AWX-instantie via {{< external-link url="https://awx.c2platform.org" htmlproofer_ignore="true" >}}, kun je alle componenten onderzoeken die zijn
gegenereerd op basis van de configuraties gevonden in
`group_vars/awx_new/awx.yml` of `group_vars/awx_config/main.yml`. Bijvoorbeeld:

1. Job Templates zoals `c2-awx`, `c2-awx-config`, `c2-reverse-proxy`.
2. Referenties zoals `c2-machine` en `c2-vault`.
3. Het Project genaamd `c2d` en de Inventaris genaamd `c2`.

## Handmatige installatie

Als je de voorkeur geeft aan een handmatige installatie van AWX, kun je de
`c2d-awx1` node voorzien zonder de `c2platform.mw.kubernetes` Ansible rol.
Hiervoor schakel je de `c2platform.mw.kubernetes` rol uit of verwijder je deze
in `plays/mgmt/awx.yml`. Door het commando `vagrant up c2d-awx1` uit te voeren,
wordt een "lege" Kubernetes-cluster gemaakt. Daarna kun je de documentatie
volgen om AWX in te stellen.

Hier zijn de relevante bronnen voor het handmatige installatieproces:

* {{< external-link url="https://github.com/ansible/awx-operator/blob/devel/.helm/starter/README.md" text="AWX Operator Helm Chart" htmlproofer_ignore="false" >}}
* {{< external-link url="https://github.com/ansible/awx-operator#helm-install-on-existing-cluster" text="Helm Install op bestaand cluster" htmlproofer_ignore="false" >}}

## Bekende problemen

### Kustomize

Standaard wordt de AWX-instantie gecreëerd met behulp van {{< external-link url="https://kustomize.io/" text="Kustomize" htmlproofer_ignore="false" >}}.
Raadpleeg de {{< external-link url="https://github.com/ansible/awx-operator#basic-install" text="ansible/awx-operator" htmlproofer_ignore="false" >}} documentatie voor
meer details.

Er is echter een probleem bij het gebruik van een Kubernetes-cluster op basis
van MicroK8s. Het commando `kubectl apply` zal falen met het volgende bericht:

> evalsymlink failure on
> '/home/vagrant/github.com/ansible/awx-operator/config/default?ref=2.2.1'

```bash
vagrant@c2d-awx1:~$ kubectl apply -k .
error: accumulating resources: accumulation err='accumulating resources from 'github.com/ansible/awx-operator/config/default?ref=2.2.1': evalsymlink failure on '/home/vagrant/github.com/ansible/awx-operator/config/default?ref=2.2.1' : lstat /home/vagrant/github.com: no such file or directory': git cmd = '/snap/microk8s/5392/usr/bin/git fetch --depth=1 origin 2.2.1': exit status 128
```

Om dit bericht te reproduceren, maak een bestand
`/home/vagrant/kustomization.yaml` met de onderstaande inhoud en voer het
commando `kubectl apply -k .` uit:

```bash
vagrant@c2d-awx1:~$ cat kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
images:
- name: quay.io/ansible/awx-operator
  newTag: 2.2.1
kind: Kustomization
namespace: awx
resources:
- github.com/ansible/awx-operator/config/default?ref=2.2.1
```

Als gevolg van het falen van {{< external-link url="https://kustomize.io/" text="Kustomize" htmlproofer_ignore="false" >}}, werd de Helm Chart optie
gebruikt.

## Links

* {{< external-link url="https://github.com/ansible/awx-operator" text="ansible/awx-operator: Een Ansible AWX operator voor Kubernetes gebouwd met Operator SDK en Ansible." htmlproofer_ignore="false" >}}
  * {{< external-link url="https://github.com/ansible/awx-operator/blob/devel/.helm/starter/README.md" text="AWX Operator Helm Chart" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://github.com/ansible/awx-operator#helm-install-on-existing-cluster" text="Helm Install op bestaand cluster" htmlproofer_ignore="false" >}}
* {{< external-link url="https://docs.ansible.com/" text="Ansible Documentatie" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://docs.ansible.com/automation-controller/4.0.0/html/administration/operator_advanced_configurations.html" text="9. Geavanceerde Configuraties voor AWX Operator" htmlproofer_ignore="false" >}}
* {{< external-link url="https://ansible.readthedocs.io/projects/awx-operator/en/latest/user-guide/advanced-configuration/trusting-a-custom-certificate-authority.html" text="Vertrouwen van een aangepaste certificaatautoriteit - Ansible AWX Operator Documentatie" htmlproofer_ignore="false" >}}
* {{< external-link url="https://github.com/redhat-cop" text="Red Hat Communities of Practice" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://github.com/redhat-cop/controller_configuration" text="redhat-cop/controller_configuration: Een collectie van rollen om Ansible Controller en eerder Ansible Tower te beheren" htmlproofer_ignore="false" >}}