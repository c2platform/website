---
categories: ["Handleiding"]
tags: [ansible, galaxy, kubernetes, debug]
title: "Probleemoplossing Automation Hub (Galaxy NG) Kubernetes-implementaties"
linkTitle: "Probleemoplossing Automation Hub"
weight: 25
description: >
   Verwerf expertise in het implementeren van debugcontainers voor het effectief
   oplossen van problemen en het oplossen van eventuele problemen met betrekking
   tot Ansible Automation Hub (Galaxy NG) Kubernetes-implementaties. Deze sectie
   voorziet je van de hulpmiddelen om configuratie-uitdagingen aan te pakken en
   soepele operaties te garanderen.
---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform/docker/debug`]({{<
relref path="/docs/gitlab/c2platform/docker/debug" >}}),
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})

---
Het Ansible inventarisproject [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) bevat de configuratie voor de
Ansible Automation Hub (Galaxy NG). In dit project vind je instellingen om drie
verschillende soorten debugcontainers te implementeren. Deze containers dienen
als praktische voorbeelden en tonen hoe we, voor het debuggen en oplossen van
problemen, Kubernetes-implementaties aanvullen met extra containers binnen een
specifieke namespace. Deze aanpak verhoogt onze efficiëntie bij het aanpakken
van verschillende taken met betrekking tot debugging en onderhoud.

---

## Overzicht

Visualiseer de architectuur voor het implementeren van debugcontainers in de
Ansible Automation Hub (Galaxy NG) namespace:

```plantuml
@startuml overzicht
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

'LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <logos/kubernetes>
!include <office/Devices/workstation>
!include <cloudogu/tools/ansible>
!include <tupadr3/devicons/terminal>
!include <tupadr3/material/dashboard>

AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddRelTag("licht", $textColor="#93c7fb")
UpdateRelStyle("#042a4f", "#042a4f")

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
AddContainerTag("xtop", $sprite="workstation", $legendText="virtueel-bureau")
AddContainerTag("ansible_container", $sprite="ansible", $legendText="ansible")
AddContainerTag("terminal", $sprite="terminal", $legendText="terminal")
AddContainerTag("dashboard", $sprite="dashboard", $legendText="dashboard")
Persoon(engineer, "Ingenieur")

System_Boundary(local, "c2platform.org") {
   Container(c2d_rproxy1, "Omgekeerde proxy", "c2d-rproxy1,lxd", $tags="rproxy")
   Container(c2d_galaxy_k8s, "Kubernetes", "c2d-galaxy1,lxd", $tags="k8s") {
      Boundary(galaxy_namespace, "galaxy", $type="namespace") {
         Container(galaxy, "Automation Hub", "", $tags="ansible_container")
         Container(debug_gui, "Debug Desktop", "", $tags="xtop")
         Container(dashboard, "K8s Dashboard", "", $tags="dashboard")
         Container(debug_cli, "Debug CLI", "", $tags="terminal")
         Container(ansible_exe, "Uitvoeringsomgeving", "", $tags="ansible_container")
      }
   }
}

Rel(engineer, c2d_rproxy1, "galaxy.c2platform.org", "https,port 443")
Rel(c2d_rproxy1, galaxy, "", "")
Rel(engineer, debug_gui, "Gebruikmakend van RDP-verbinding", "xrdp,3389", $tags="licht")
Rel(engineer, dashboard, "Via Kubernetes Dashboard\ngalaxy-dashboard.c2platform.org", "https,port 443")
Rel(debug_gui, galaxy, "Debug met\nFireFox\n(en CLI-hulpprogramma's)", "", $tags="licht")
Rel(debug_cli, galaxy, "Debug met\nCLI-hulpprogramma's", "", $tags="licht")
Rel(ansible_exe, galaxy, "Debug met\nAnsible", "", $tags="licht")
Rel(dashboard, debug_cli, "Voer in pod uit", $tags="licht")
Rel(dashboard, ansible_exe, "", $tags="licht")

SHOW_LEGEND(false)

@enduml
```