---
categories: ["Voorbeeld"]
tags: [automatisering, hub, ansible, ansible.cfg, galaxy]
title: "Voorbeeld Ansible Configuratiebestand (ansible.cfg) voor Galaxy NG"
linkTitle: "Voorbeeld ansible.cfg voor Galaxy NG"
weight: 2
description: >
    Voorbeeld van een Ansible configuratiebestand `ansible.cfg` dat wordt gebruikt op
    `c2d-xtop` om verbinding te maken met de privé Ansible Automation Hub gebaseerd op Galaxy NG
    die draait op `c2d-galaxy1`.
---

Dit is een illustratief voorbeeld van een `ansible.cfg` configuratiebestand, dat
wordt gebruikt om verbinding te maken met de privé Ansible Automation Hub
gebaseerd op Galaxy NG die draait op de `c2d-galaxy1` server. Let op dat de
`client_id` en URL-instellingen verschillen van die we gebruiken voor verbinding
met de [Ansible Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}})
website.

Het `ansible.cfg` bestand wordt automatisch gegenereerd door Ansible en is te
vinden in de home-directory van de `vagrant` gebruiker op de `c2d-xtop` virtuele
desktop. Voor meer details, raadpleeg het `group_vars/xtop/main.yml` bestand in
het [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) project.

```
[galaxy]
server_list=published, community
validate_certs=no

[galaxy_server.published]
url=https://galaxy.c2platform.org/api/galaxy/
client_id=galaxy-ng
gebruikersnaam=admin
wachtwoord=geheim
validate_certs=no

[galaxy_server.community]
url=https://galaxy.c2platform.org/api/galaxy/content/community
client_id=galaxy-ng
gebruikersnaam=admin
wachtwoord=geheim
validate_certs=no
```