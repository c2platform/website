---
categories: ["Handleiding"]
tags: [awx, aap, galaxy, ansible]
title: "Het gebruik van het Ansible Automation Platform (AAP) vanaf het Virtuele Bureaublad"
linkTitle: "AAP gebruiken op het Virtuele Bureaublad"
weight: 3
description: >
  Ontgrendel het potentieel van AAP door een virtuele bureaubladomgeving te creëren en
  deze te configureren als een Ansible Control Node. Deze configuratie zorgt voor naadloze
  connectiviteit met de Ansible Automation Hub (Galaxy NG) voor efficiënte automatiseringsworkflows.
---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

---

Deze handleiding biedt instructies over hoe je het virtuele bureaublad
`c2d-xtop` kunt gebruiken als een {{< external-link url="https://docs.ansible.com/ansible/latest/network/getting_started/basic_concepts.html#control-node" text="Ansible Control Node" htmlproofer_ignore="false" >}} die gebruik maakt van
de private Automation Hub draaiend op `c2d-galaxy1`. Je kunt verbinden met de
GUI van dit bureaublad via een RDP-verbinding of eenvoudigweg SSH erop uitvoeren
met `vagrant ssh c2d-xtop`. Vanaf daar kun je bijvoorbeeld de `ansible-galaxy`
opdrachtregel gebruiken om collecties te downloaden/publiceren.


```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

'LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
'!include <cloudogu/tools/ansible>
!include <cloudogu/tools/ansible>
!include <logos/kubernetes>

AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddRelTag("light", $textColor="#93c7fb")
AddContainerTag("ansible_container", $sprite="ansible", $legendText="ansible")
AddRelTag("ansible", $sprite="ansible", $legendText="ansible provision")
UpdateRelStyle("#042a4f", "#042a4f")

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("xtop", $sprite="workstation", $legendText="virtueel-bureaublad")
AddSystemTag("ansible", $sprite="ansible", $legendText="ansible-galaxy", $bgColor="#999999")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
Person(engineer, "Engineer")

System_Boundary(local, "c2platform.org") {
   Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1,lxd", $tags="rproxy")
   Container(c2d_xtop, "Virtueel Bureaublad", "c2d-xtop,lxd", $tags="xtop")
   Boundary(aap, "Ansible Automation Platform ( AAP )") {
    Container(c2d_galaxy_k8s, "Kubernetes", "c2d-galaxy1,lxd", $tags="k8s") {
        Container(galaxy, "Automation\nHub", "", $tags="ansible_container")
    }
    Container(c2d_awx_k8s, "Kubernetes", "c2d-awx1,lxd", $tags="k8s") {
        Container(awx, "Automation Controller", "", $tags="ansible_container")
    }
   }
}

System_Ext(galaxy_website, "galaxy.ansible.com", "Slaat alle open-source Ansible automatisering op\n(collecties, rollen)", $tags="ansible")

Rel(engineer, c2d_xtop, "Verbind met\n bureaublad", "xRDP,poort 3389")
Rel(engineer, c2d_xtop, "Verbind\ndoor gebruik van SSH", "vagrant ssh")
Rel_R(c2d_xtop, c2d_rproxy1, "Upload/download collecties", "galaxy.c2platform.org,\nhttps,poort 443")
Rel_R(c2d_xtop, c2d_rproxy1, "Start Jobs", "awx.c2platform.org\nhttps,poort 443")

Rel(c2d_rproxy1, galaxy, "", "")
Rel_R(awx, galaxy, "Download\ngeaccrediteerde/gekureerde\ncollecties/rollen", "")
Rel_R(galaxy, galaxy_website, "Download collecties/rollen", "https,poort 443")
Rel(c2d_rproxy1, awx, "", "")

SHOW_LEGEND(false)
@enduml
```