---
categories: ["Voorbeeld"]
tags: [ansible, python, environment]
title: "Installeer Ansible met Pyenv: Een Bash Script Voorbeeld"
linkTitle: "Voorbeeldscript om Ansible te Installeren"
weight: 1
description: >
    Voorbeeld van een Ansible Configuratiebestand `ansible.cfg` dat wordt gebruikt op
    `c2d-xtop` om verbinding te maken met de private Ansible Automation Hub gebaseerd op Galaxy NG
    draaiende op `c2d-galaxy1`.
---

Dit voorbeeld toont een Bash-script, {{< external-link url="https://gitlab.com/c2platform/ansible/-/blob/master/scripts/penv.sh?ref_type=heads" text="penv.sh" htmlproofer_ignore="false" >}} , dat gebruikmaakt van {{<
external-link
url="https://github.com/pyenv/pyenv#install-python-build-dependencies"
text="pyenv" htmlproofer_ignore="false" >}} om Python en Ansible naadloos te
installeren. Dit script wordt gebruikt binnen de virtuele desktopomgeving
genaamd `c2d-xtop`. Voor gedetailleerde instructies over het gebruik, raadpleeg
de handleiding [Using the Automation Hub (Galaxy NG) from the Virtual
Desktop]({{< relref path="/docs/howto/awx/xtop" >}}).

Het script `penv.sh` maakt gebruik van `pyenv` om het installatieproces van
Python en Ansible te vereenvoudigen. Het is gemaakt voor gebruik in de virtuele
desktopomgeving bekend als `c2d-xtop`. Voor instructies over hoe je dit script
kunt gebruiken, raadpleeg de handleiding getiteld [Using the Automation Hub
(Galaxy NG) from the Virtual Desktop]({{< relref path="/docs/howto/awx/xtop" >}}).

Het {{< external-link url="https://github.com/pyenv/pyenv#install-python-build-dependencies" text="pyenv" htmlproofer_ignore="false" >}} hulpprogramma wordt ook gebruikt om
de [Ansible Execution Environment]({{< relref path="/docs/concepts/ansible/projects/execution-env" >}}) voor Rijkswaterstaat (
RWS) te creëren. Zie [`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}}) voor meer
informatie.