---
categories: []
tags: []
title: "Ansible Automatiseringsplatform ( AAP )"
linkTitle: "AAP"
weight: 9
description: >
  Deze sectie biedt uitgebreide instructies voor het creëren, beheren en benutten van de kracht van het Ansible Automatiseringsplatform (AAP). AAP bestaat uit twee belangrijke componenten: de Automatiseringscontroller (AWX) en de Ansible Automation Hub (Galaxy NG).
---


Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

---

```plantuml
@startuml aap
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

'LAYOUT_WITH_LEGEND()
'SHOW_LEGEND(false)

HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
!include <cloudogu/tools/ansible>
!include <logos/kubernetes>

AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddRelTag("light", $textColor="#93c7fb")
AddRelTag("ansible", $sprite="ansible", $legendText="ansible provision")
UpdateRelStyle("#042a4f", "#042a4f")

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddSystemTag("ansible", $sprite="ansible", $legendText="ansible-galaxy", $bgColor="#999999")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")

System_Boundary(local, "c2platform.org") {
   Boundary(aap, "Ansible Automatiseringsplatform ( AAP )") {
    Container(c2d_galaxy_k8s, "Kubernetes", "c2d-galaxy1,lxd", $tags="k8s") {
        Container(galaxy, "Automatiserings\nHub", "", $tags="")
    }
    Container(c2d_awx_k8s, "Kubernetes", "c2d-awx1,lxd", $tags="k8s") {
        Container(awx, "Automatiseringscontroller", "", $tags="")
    }
   }
}

System_Ext(galaxy_website, "galaxy.ansible.com", "Bevat alle open source Ansible automatisering \n( collecties, rollen )", $tags="ansible")

Rel_R(awx, galaxy, "Download\ngoedgekeurde / geselecteerde\nAnsible automatisering", "")
Rel_R(galaxy, galaxy_website, "Download Ansible automatisering")
@enduml
```