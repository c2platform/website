---
categories: ["Handleiding"]
tags: [awx, galaxy, ansible]
title: Het verbinden van de Automation Controller (AWX) met de Automation Hub (Galaxy NG) met behulp van Ansible
draft: false
linkTitle: Verbind Controller met Hub
weight: 3
description: >
  Leer hoe je AWX opnieuw kunt configureren om in plaats van de openbare Galaxy-website,
  gebruik te maken van de private Galaxy NG automation hub.
---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}),
[`c2platform.mgmt`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mgmt" >}}),
[`ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})

---

{{< under_construction >}}

Deze handleiding legt uit hoe je AWX kunt verbinden, toegankelijk via {{<
external-link url="https://awx.c2platform.org" htmlproofer_ignore="true" >}},
met Galaxy NG, toegankelijk via {{< external-link url="https://galaxy.c2platform.org" htmlproofer_ignore="true" >}}, in plaats van
te vertrouwen op de openbare Galaxy-website op {{< external-link url="https://galaxy.ansible.com" htmlproofer_ignore="false" >}}. Deze
configuratie stelt je in staat om betere controle te hebben over de
Ansible-inhoud die in je omgeving wordt gebruikt.

Door de onderstaande stappen te volgen, kun je ervoor zorgen dat AWX alleen
gebruikmaakt van samengestelde en goedgekeurde rollen van de openbare
Galaxy-site door de introductie van de Automation Hub (Galaxy NG).

```plantuml
@startuml overzicht
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
!include <logos/kubernetes>
!include <cloudogu/tools/ansible>

AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddRelTag("light", $textColor="#93c7fb")
AddRelTag("ansible", $sprite="ansible", $legendText="ansible provision")
UpdateRelStyle("#042a4f", "#042a4f")

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("xtop", $sprite="workstation", $legendText="virtual-desktop")
AddContainerTag("ansible_container", $sprite="ansible", $legendText="ansible")
AddSystemTag("ansible", $sprite="ansible", $legendText="ansible-awx", $bgColor="#999999")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
Person(engineer, "Ingenieur")

Person(engineer, "Ingenieur")

System_Boundary(local, "c2platform.org" ) {
    Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1,lxd", $tags="rproxy")
    Boundary(aap, "Ansible Automation Platform ( AAP )") {
      Container(c2d_galaxy_k8s, "Kubernetes", "c2d-galaxy1,lxd", $tags="k8s") {
          Container(galaxy, "Automation\nHub", "", $tags="ansible_container")
      }
      Container(c2d_awx_k8s, "Kubernetes", "c2d-awx1,lxd", $tags="k8s") {
          Container(awx, "Automation\nController", "", $tags="ansible_container")
      }
    }
}

System_Ext(galaxy_website, "galaxy.ansible.com", "Slaat alle open-source Ansible automatisering op\n(collecties, rollen)", $tags="ansible")

Rel(engineer, c2d_rproxy1, "Toegang tot\nAutomation Controller\nWebinterface", "awx.c2platform.org\ngalaxy.c2platform.org,\nhttps,443")
Rel(c2d_rproxy1, awx, "", "")
Rel_R(awx, galaxy, "Download \nsamengestelde/goedgekeurde collecties/rollen", "https,port 443")
Rel_R(galaxy, galaxy_website, "Download collecties/rollen", "https,port 443")
@enduml
```

---

## Vereisten

* [Installeer de Automation Controller (AWX) met behulp van Ansible]({{< relref path="/docs/howto/awx/awx" >}})
* [Installeer de Automation Hub (Galaxy NG) met behulp van Ansible]({{< relref path="/docs/howto/awx/galaxy" >}})

## Verbinding maken met behulp van Vagrant

Volg deze stappen om de verbinding tussen AWX en Galaxy NG te configureren:
1. Zoek in het Ansible-inventoryproject [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) het bestand
   `group_vars/awx/awx.yml`. Dit bestand bevat de AWX-configuratie.
2. Bewerk de variabele `c2_awx_cred_galaxy_selected` en verander de waarde van
   `Ansible Galaxy` naar `c2-galaxy`:

```yaml
c2_awx_cred_galaxy_selected: c2-galaxy
```

Voer nu de provisioning uit met

```bash
TAGS=config vagrant provision c2d-awx1
```

> Opmerking: Met `TAGS=config` voeren we alleen de Ansible-taken uit voor de
> AWX-configuratie. Dit is optioneel en wordt alleen gebruikt om wat
> provisioning-tijd te besparen.

## Verbinding maken met behulp van AWX

```