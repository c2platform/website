---
categories: ["Handleiding"]
tags: [awx, aap, galaxy, ansible, kubernetes, operator, pulp]
title: "Stel de Automation Hub (Galaxy NG) in met Ansible"
linkTitle: "Automation Hub (Galaxy NG)"
weight: 2
description: >
  Ontdek hoe u de Ansible Automation Hub (Galaxy NG) kunt creëren met behulp van Ansible, in combinatie met de Pulp Operator. Deze handleiding laat ook zien hoe u de externe community repository met collecties configureert, waardoor uw automatiseringsmogelijkheden worden verbeterd.

---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}})

---
Deze stapsgewijze handleiding laat zien hoe u een instance van [Ansible
Galaxy]({{< relref path="/docs/concepts/ansible/aap" >}}) op de `c2d-galaxy1`
node kunt maken met behulp van de
{{< external-link url="https://pulpproject.org/pulp-operator/" text="Pulp Operator" htmlproofer_ignore="false" >}}.
Het {{< external-link url="https://pulpproject.org/" text="Pulp" htmlproofer_ignore="false" >}}
platform is een recent ontwikkelde open-source software die het beheer en de
distributie van softwarepakketten vergemakkelijkt, vergelijkbaar met {{<
external-link url="https://www.sonatype.com/products/sonatype-nexus-repository"
text="Sonatype Nexus Repository" htmlproofer_ignore="false" >}}. Een Galaxy
NG-instance verwijst in wezen naar een Pulp-instance met de {{< external-link url="https://github.com/ansible/galaxy_ng/" text="Galaxy NextGen" htmlproofer_ignore="false" >}} Pulp-plug-in geïnstalleerd.

---

{{< alert title="Let op:" >}} De documentatie voor de Pulp Operator, vooral wat
betreft de integratie met Galaxy NG, is momenteel onvoldoende. Als u van plan
bent de Pulp Operator buiten dit project te gebruiken, wordt aanbevolen om de
{{< external-link url="https://github.com/pulp/pulp-operator" text="Pulp Operator project" htmlproofer_ignore="false" >}} repository lokaal te clonen.
Hierdoor kunt u problemen met de configuratie onderzoeken en oplossen door zowel
de code als de documentatie te bekijken. {{< /alert >}}

```plantuml
@startuml overview
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

'LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
'!include <cloudogu/tools/ansible>
!include <logos/kubernetes>
!include <cloudogu/tools/ansible>

AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddRelTag("light", $textColor="#93c7fb")
AddRelTag("ansible", $sprite="ansible", $legendText="ansible provision")
UpdateRelStyle("#042a4f", "#042a4f")

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("xtop", $sprite="workstation", $legendText="virtual-desktop")
AddContainerTag("ansible_container", $sprite="ansible", $legendText="ansible")
AddSystemTag("ansible", $sprite="ansible", $legendText="ansible-galaxy", $bgColor="#999999")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
Person(engineer, "Engineer")

Person(engineer, "Engineer")

System_Boundary(local, "c2platform.org" ) {
    Container(c2d_rproxy1, "Omgekeerde Proxy", "c2d-rproxy1,lxd", $tags="rproxy")
    Boundary(aap, "Ansible Automation Platform (AAP)") {
      Container(c2d_galaxy_k8s, "Kubernetes", "c2d-galaxy1,lxd", $tags="k8s") {
          Container(galaxy, "Automation\nHub", "", $tags="ansible_container")
      }
    }
}

System_Ext(galaxy_website, "galaxy.ansible.com", "Slaat alle open source Ansible automatisering \n(collecties, rollen) op", $tags="ansible")

Rel(engineer, c2d_rproxy1, "Toegang tot Galaxy NG\nWebinterface", "galaxy.c2platform.org,\nhttps,443")
Rel(c2d_rproxy1, galaxy, "", "")
Rel_R(galaxy, galaxy_website, "Collecties / rollen downloaden", "https,port 443")
@enduml
```

---

## Vereisten

Maak de omgekeerde en doorstuurproxy `c2d-rproxy1`.

```bash
c2
unset PLAY  # zorg ervoor dat alle plays worden uitgevoerd
vagrant up c2d-rproxy1
```

Voor meer informatie over de verschillende rollen die `c2d-rproxy1` in dit
project uitvoert:

* [Stel Omgekeerde Proxy en CA-server in]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [Stel SOCKS-proxy in]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Beheer servercertificaten als een Certificeringsautoriteit]({{< relref path="/docs/howto/c2/certs">}})
* [Stel DNS in voor Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}})

## Installeren

Om de Kubernetes-instance te maken en de Galaxy NG-instance te implementeren,
voert u de volgende opdracht uit:

```bash
vagrant up c2d-galaxy1
```

## Verifiëren

U kunt nu uw AWX-instance verifiëren met behulp van het Kubernetes Dashboard en
de AWX-webinterface via de doorstuurproxy, zoals beschreven in [Aan de slag]({{<
relref path="/docs/howto/dev-environment/setup">}}).

### Kubernetes Dashboard

Om toegang te krijgen tot het Kubernetes Dashboard, navigeert u naar {{<
external-link url="https://dashboard-galaxy.c2platform.org/"
htmlproofer_ignore="true" >}} en logt u in met een token. Verkrijg het token
door de volgende opdracht binnen de `c2d-galaxy1` node uit te voeren:

```bash
kubectl -n kube-system describe secret microk8s-dashboard-token
```

Met behulp van het dashboard kunt u naar de {{< external-link url="https://dashboard-galaxy.c2platform.org/#/workloads?namespace=awx" text="awx" htmlproofer_ignore="true" >}}-namespace navigeren. Daar zou u
draaiende pods zonder fouten moeten zien, zoals `galaxy-web` en
`galaxy-content`.

Voor meer informatie over de token- en dashboardconfiguratie, raadpleegt u [Stel
het Kubernetes Dashboard in]({{< relref path="../kubernetes/dashboard" >}}).

### Galaxy NG

Om toegang te krijgen tot de Galaxy NG-webinterface, gaat u naar {{<
external-link url="https://galaxy.c2platform.org" htmlproofer_ignore="true" >}}.
U kunt inloggen met het `admin`-account met het wachtwoord `secret`.

Dankzij de configuratie van Ansible van de externe `community`-repository met
zijn afhankelijkheden, moet u deze kunnen synchroniseren. Volg hiervoor deze
stappen:

1. Navigeer naar **Collecties** → **Beheer van Repositories** → **Extern**.
2. Klik op de knop **Sync** om het synchronisatieproces te starten.

{{< image filename="/images/docs/galaxy-sync.png?width=1200px" >}}

Nadat u de synchronisatie hebt gestart, gaat u naar de sectie **Taakbeheer**. U
zult een taak zien die wordt uitgevoerd. Wees geduldig en wacht totdat deze is
voltooid. Zodra het klaar is, navigeert u naar de sectie **Namespaces**. Hier
vindt u verschillende namespaces met Ansible-collecties, waaronder de
`c2platform` namespace.

{{< image filename="/images/docs/galaxy-namespaces.png?width=1200px" >}}

## Review

Voor een uitgebreide uitleg over het aanmaken van de Galaxy NG-instance met
Ansible, raadpleegt u de volgende componenten binnen het Ansible playbookproject
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}):

1. `Vagrantfile.yml`: Dit bestand configureert de `c2d_galaxy1` node met de
   `mgmt/galaxy` playbook.
2. `plays/mgmt/awx_galaxy.yml`: Dit playbook stelt Ansible-rollen in voor de
   `galaxy` Ansible-groep.
3. `hosts-dev.ini`: Het inventory-bestand wijst de `c2d_galaxy1` node toe aan de
   `galaxy` Ansible-groep.
4. `group_vars/galaxy/main.yml`: In dit bestand vindt u de primaire
   Kubernetes-configuratie, inclusief de activering van add-ons zoals
   `dashboard` en de `metallb` load balancer.
5. `group_vars/galaxy/kubernetes.yml`: Dit bestand bevat configuraties voor het
   opzetten van het Kubernetes-cluster en de installatie van Galaxy. Het omvat
   drie cruciale instellingen: `ansible_api_hostname`,
   `ansible_content_hostname`, `content_origin`. Deze instellingen moeten
   correct zijn geconfigureerd om overeen te komen met de URL van de Galaxy
   NG-instance op {{< external-link url="https://galaxy.c2platform.org" htmlproofer_ignore="true" >}}, aangezien een onjuiste configuratie de
   correcte werking van Galaxy NG buiten het cluster kan belemmeren. Deze setup
   is cruciaal voor het succesvol uitvoeren van het `ansible-galaxy collection
   install`-commando, zoals gedemonstreerd in [Gebruik van het Ansible
   Automation Platform (AAP) vanaf het Virtuele Desktop]({{< relref path="./awx-galaxy" >}}).
6. `group_vars/galaxy/hub.yml`: Dit bestand bevat de configuratie die wordt
   gebruikt door de {{< external-link url="https://github.com/ansible/galaxy_collection" text="galaxy.galaxy" htmlproofer_ignore="false" >}} om de "vereisten" van de externe
   `community`-collectie te configureren. Het is belangrijk op te merken dat
   deze collectie momenteel actief wordt ontwikkeld en bekend is om een veelheid
   aan problemen.

{{< alert type="warning" title="Waarschuwing!" >}} Op dit moment is het raadzaam
om voorzichtig te zijn bij het gebruik van de `galaxy.galaxy` collectie. Deze
collectie is nog niet officieel uitgebracht op de [Ansible Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}) website en is momenteel in actieve
ontwikkeling met een groot aantal bekende problemen. {{< /alert >}}

Daarnaast kunt u binnen het Ansible collectieproject [`c2platform.mw`]({{<
relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}}) verkennen:

1. De `c2platform.mw.microk8s` Ansible rol.
2. De `c2platform.mw.kubernetes` Ansible rol.

Om toegang te krijgen tot verschillende resources binnen de `c2d` omgeving via
de doorstuurproxy, gebruikt u de volgende URL's:

* {{< external-link url="https://galaxy.c2platform.org" htmlproofer_ignore="true" >}} voor de Automation Hub (Galaxy NG) webinterface.
* {{< external-link url="https://dashboard-galaxy.c2platform.org/" htmlproofer_ignore="true" >}} voor het Kubernetes Dashboard waar Galaxy NG is
  geïmplementeerd in de `galaxy` namespace.
* {{< external-link url="https://galaxy.c2platform.org/api/galaxy/" text="Django REST framework" htmlproofer_ignore="true" >}}
* {{< external-link url="https://galaxy.c2platform.org/api/galaxy/v3/swagger-ui/" text="Automation Hub API" htmlproofer_ignore="true" >}}

## Links

* Voor begeleiding over het gebruik van de Automation Hub (Galaxy NG) vanaf het
  Virtuele Desktop, raadpleeg [Gebruik de Automation Hub (Galaxy NG) vanaf het
  Virtuele Desktop]({{< relref path="/docs/howto/awx/xtop" >}}).
* {{< external-link url="https://github.com/ansible/galaxy_ng/wiki/End-User-Installation" text="End User Installation · ansible/galaxy_ng Wiki" htmlproofer_ignore="false" >}}. Dit document biedt gedetailleerde instructies
  over het installeren van Pulp en Galaxy NG vanuit PyPi-pakketten, samen met
  een Ansible playbook. Hoewel deze methode voor het installeren van Galaxy
  aanvankelijk werd overwogen, is de voorkeur verschoven naar het implementeren
  van Galaxy op Kubernetes door gebruik te maken van de Pulp Operator.
* {{< external-link url="https://pulpproject.org/about-pulp-3/" text="Over Pulp 3 | beheer van software repositories" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://operatorhub.io/" text="OperatorHub.io | Het register voor Kubernetes Operators" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://operatorhub.io/operator/pulp-operator" text="Pulp Project" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://operatorhub.io/operator/galaxy-operator" text="Ansible Galaxy" htmlproofer_ignore="false" >}}
* {{< external-link url="https://github.com/pulp/pulp-operator" text="pulp/pulp-operator: Kubernetes Operator voor Pulp 3. Onder actieve ontwikkeling." htmlproofer_ignore="false" >}}
* {{< external-link url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/" text="Galaxy NG" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/dev/vagrant/#6-create-the-installer-config-file" text="Maak het installer configuratiebestand aan" htmlproofer_ignore="true" >}}
  * {{< external-link url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/usage_guide/installation/" text="Installatiehandleiding" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/usage_guide/collections/" text="Collecties Gids" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/usage_guide/collections/" text="Uitvoeringsomgeving Gids" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/usage_guide/rbac/" text="RBAC en Gebruikersbeheer Gids" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://ansible.readthedocs.io/projects/galaxy-ng/en/latest/community/api_v3/" text="Galaxy V3 API" htmlproofer_ignore="true" >}}
* {{< external-link url="https://docs.pulpproject.org/pulp_ansible/" text="Welkom bij de documentatie van Pulp Ansible!" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://docs.pulpproject.org/pulp_ansible/settings.html" text="Instellingen" htmlproofer_ignore="false" >}} verschillende
    instellingen die kunnen worden doorgegeven aan de Galaxy Operator met behulp
    van `pulp_settings`.
* {{< external-link url="https://github.com/pulp/pulp-operator" text="pulp/pulp-operator: Kubernetes Operator voor Pulp 3. Onder actieve ontwikkeling." htmlproofer_ignore="false" >}}
* {{< external-link url="https://github.com/redhat-cop" text="Red Hat Communities of Practice" htmlproofer_ignore="false" >}}
  * {{< external-link url="https://github.com/redhat-cop/controller_configuration" text="redhat-cop/controller_configuration: Een collectie van rollen om Ansible Controller en vroeger Ansible Tower te beheren" htmlproofer_ignore="false" >}}