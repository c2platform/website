---
title: "GitLab Projects"
linkTitle: "GitLab"
weight: 15
description: >
  Deze sectie geeft een overzicht van C2 Platform GitLab-projecten die onderdeel zijn van het
  {{< external-link url="https://about.gitlab.com/solutions/open-source/join/" text="GitLab Open Source Program" htmlproofer_ignore="false" >}}.
  Deze projecten profiteren daarmee van de volledige mogelijkheden van
  [GitLab Ultimate]({{< relref path="/docs/concepts/gitlab" >}})
---

