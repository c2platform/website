---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-forgerock
tags: ['am', 'ansible', 'c2platform', 'collection', 'ds', 'forgerock', 'ig']
title: "Ansible Collection - c2platform.forgerock"
weight: 16
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-collection-forgerock"><code>c2platform/ansible-collection-forgerock</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-forgerock/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-forgerock/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-forgerock/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-forgerock/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.gis-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/forgerock/)

Roles for {{< external-link url="https://www.forgerock.com/" text="ForgeRock" htmlproofer_ignore="false" >}} platform.

## Roles

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-forgerock/-/blob/master/roles/ds" text="ds" htmlproofer_ignore="false" >}} ForgeRock Directory Services.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-forgerock/-/blob/master/roles/am" text="am" htmlproofer_ignore="false" >}} ForgeRock Access Management.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-forgerock/-/blob/master/roles/ig" text="ig" htmlproofer_ignore="false" >}} ForgeRock Identity Gateway.

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
{{< external-link url="https://galaxy.ansible.com/ui/repo/published/c2platform/forgerock/docs/" text="Ansible Galaxy" htmlproofer_ignore="false" >}}
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.forgerock
ansible-doc -t filter --list c2platform.forgerock
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```
>

---


<!--
id | 39808379
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-collection-forgerock" data-proofer-ignore>c2platform/ansible-collection-forgerock&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
