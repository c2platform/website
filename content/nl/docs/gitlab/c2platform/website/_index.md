---
categories: GitLab Projects
description: "This project contains the sources and content for the C2 Platform website. "
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: website
tags: ['documentation', 'hugo', 'website']
title: "Website"
weight: 24
---

GitLab: <a href="https://gitlab.com/c2platform/website"><code>c2platform/website</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/website/badges/master/pipeline.svg)](https://gitlab.com/c2platform/website/-/commits/master)
[![Latest Release](https://gitlab.com/c2platform/website/-/badges/release.svg)](https://gitlab.com/c2platform/website/-/releases)

Welcome to the repository for the C2 Platform website, available at
{{< external-link url="https://c2platform.org" htmlproofer_ignore="false" >}}. This site is built using
{{< external-link url="https://gohugo.io/" text="Hugo" htmlproofer_ignore="false" >}},
a powerful static site generator, and styled with the with the
{{< external-link url="https://github.com/google/docsy" text="Docsy Theme" htmlproofer_ignore="false" >}},
a documentation theme for technical documentation sites.

- [Development Workflow](#development-workflow)
  - [CI/CD Pipeline](#cicd-pipeline)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Local Development](#local-development)
- [Advanced Setup](#advanced-setup)
  - [PlantUML Server](#plantuml-server)
  - [Hugo in Podman Container](#hugo-in-podman-container)
- [Verification](#verification)
- [PlantUML support](#plantuml-support)
- [Pre-commit Hook](#pre-commit-hook)
- [Troubleshooting](#troubleshooting)
- [Links](#links)


## Development Workflow

### CI/CD Pipeline

Our CI/CD pipeline facilitates automatic builds and deployments:

1. **Production Build:** Commits to the `master` branch trigger a build and
   deployment to the main website at {{< external-link url="https://c2platform.org" htmlproofer_ignore="false" >}}.
2. **Development Build:** Commits to the `development` branch generate a preview
   version accessible at {{< external-link url="https://next.c2platform.org" htmlproofer_ignore="false" >}}.

## Getting Started

### Prerequisites

Ensure you have the following installed:

* Hugo (Extended Version): Required for local development.
* Node.js: Necessary for managing JavaScript dependencies.
* Git: For version control.

### Local Development

To set up your environment and start contributing:

1. **Install Dependencies:** Make sure you have `snapd` and `podman` installed.

    ```bash
    sudo apt install snapd podman -y
    sudo snap install hugo
    ```
2. **Clone the Repository:**

   ```bash
   git clone git@gitlab.com:c2platform/website.git
   cd website
   ```
3. **Install Node Version Manager ( NVM ) and NodeJS:**

   ```bash
   curl -o- {{< external-link url="https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh" htmlproofer_ignore="false" >}} | bash
   nano ~/.bashrc
   source ~/.bashrc
   nvm install 18
   npm install
   ```
4. **Install JavaScript Dependencies:**

   ```bash
   npm install
   ```

6. Start the Hugo Server:
   ```bash
   hugo server
   ```
Now, you can access the site at {{< external-link url="http://localhost:1313" htmlproofer_ignore="false" >}} and see your changes in
real-time.

## Advanced Setup

### PlantUML Server

To use or view PlantUML diagrams locally, start a PlantUML server:

```bash
podman run -d -p 8080:8080 docker.io/plantuml/plantuml-server:tomcat
```

### Hugo in Podman Container

Run the website in a Docker container without installing dependencies:

```bash
sudo apt install podman -y
podman run --rm -it -v $(pwd):/src -p 1313:1313 docker.io/klakegg/hugo:ext-ubuntu server
```

## Verification

After starting the Hugo server or Docker container, visit
`http://localhost:1313` to view the site. Live reload is enabled, so changes you
make will automatically reflect in the browser.

## PlantUML support

This site supports PlantUML and Mermaid diagrams through Docsy. Note that for
reliability, PlantUML diagrams are pre-rendered to PNG files during the CI/CD
process. For more information about PlantUML and other diagrams, visit the
{{< external-link url="https://www.docsy.dev/docs/adding-content/diagrams-and-formulae/#uml-diagrams-with-plantuml" text="Docsy documentation on diagrams" htmlproofer_ignore="false" >}}.

## Pre-commit Hook

```bash
#!/bin/sh
rm -rf public || true
hugo
python3 scripts/htmldataproofer/ignore.py
python3 scripts/htmldataproofer/list.py
cd public
htmlproofer --allow-missing-href --ignore-empty-alt --ignore-missing-alt --no-enforce-https  2>&1 | tee ../htmlproofer.log
if grep -q "following failures were found" ../htmlproofer.log; then
    echo "Failures found. Exiting."
    exit 1
fi
```

## Troubleshooting

If you encounter issues, consult the
{{< external-link url="https://github.com/google/docsy-example" text="Docsy Example" htmlproofer_ignore="false" >}}
project for common troubleshooting tips.

## Links

* {{< external-link url="https://gitlab.com/c2platform/website/-/pipelines" text="Pipelines" htmlproofer_ignore="false" >}}
* {{< external-link url="https://docsy.dev/docs" text="Docsy user guide" htmlproofer_ignore="false" >}}
* {{< external-link url="https://github.com/google/docsy" text="Docsy" htmlproofer_ignore="false" >}}
* {{< external-link url="https://example.docsy.dev" text="example.docsy.dev" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gohugo.io" text="Hugo" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gohugo.io/hugo-modules/use-modules/#use-a-module-for-a-theme" text="Hugo theme module" htmlproofer_ignore="false" >}}


---


<!--
id | 45423570
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/website" data-proofer-ignore>c2platform/website&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
