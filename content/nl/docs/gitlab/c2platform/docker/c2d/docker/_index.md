---
categories: GitLab Projects
description: "Docker image for the development environment `c2d`. This image is intended to be used for GitLab instance running on `c2d-gitlab`."
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: docker
tags: ['dind', 'docker', 'gitlab', 'image']
title: "Docker image for C2 Platform development environment"
weight: 11
---

GitLab: <a href="https://gitlab.com/c2platform/docker/c2d/docker"><code>c2platform/docker/c2d/docker</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![pipeline status](https://gitlab.com/c2platform/docker/c2d/docker/badges/master/pipeline.svg)](https://gitlab.com/c2platform/docker/c2d/docker/-/commits/master) 
[![Latest Release](https://gitlab.com/c2platform/docker/c2d/docker/-/badges/release.svg)](https://gitlab.com/c2platform/docker/c2d/docker/-/releases) 

This project provides a Docker image configuration that extends the base `docker:20.10.16` image by incorporating the C2 Platform development CA Bundle, specifically the `c2.crt` file. The purpose of this Docker image configuration is to enable seamless integration with the C2 Platform by including the necessary CA certificates. By importing the `c2.crt` file, this image ensures secure communication with the C2 Platform's GitLab and Registry components.


---


<!--
id | 44723321
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/docker/c2d/docker" data-proofer-ignore>c2platform/docker/c2d/docker&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
