---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-mw
tags: ['ansible', 'apache', 'c2platform', 'collection', 'docker', 'haproxy', 'keycloak', 'linux', 'middleware', 'nfs', 'postgresql', 'redhat', 'rhel', 'rhn', 'squid', 'sso', 'ubuntu']
title: "Ansible Collection - c2platform.mw"
weight: 18
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-collection-mw"><code>c2platform/ansible-collection-mw</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-mw/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-mw/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-mw/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-mw/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.gis-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/mw/)

C2 Platform middleware roles

## Roles

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/apache" text="apache" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/docker" text="docker" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/haproxy" text="haproxy" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/nfs" text="nfs" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/postgresql" text="postgresql" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/proxy" text="proxy" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/tomcat" text="tomcat" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/keycloak" text="keycloak" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/dnsmasq" text="dnsmasq" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/microk8s" text="microk8s" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/kubernetes" text="kubernetes" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/exim4" text="exim4" htmlproofer_ignore="false" >}}

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
{{< external-link url="https://galaxy.ansible.com/ui/repo/published/c2platform/mw/docs/" text="Ansible Galaxy" htmlproofer_ignore="false" >}}
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.mw
ansible-doc -t filter --list c2platform.mw
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```


---


<!--
id | 39049044
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-collection-mw" data-proofer-ignore>c2platform/ansible-collection-mw&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
