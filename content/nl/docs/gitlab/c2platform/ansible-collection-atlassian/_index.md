---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-atlassian
tags: ['ansible', 'application', 'atlassian', 'bitbucket', 'c2platform', 'collection', 'confluence', 'jira']
title: "Ansible Collection - c2platform.atlassian"
weight: 12
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-collection-atlassian"><code>c2platform/ansible-collection-atlassian</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-atlassian/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-atlassian/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-atlassian/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-atlassian/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.gis-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/atlassian/)

C2 Platform collection for Atlassian Jira, Confluence and Bitbucket

## Roles

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-atlassian/-/blob/master/roles/jira" text="jira" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-atlassian/-/blob/master/roles/confluence" text="confluence" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-atlassian/-/blob/master/roles/bitbucket" text="bitbucket" htmlproofer_ignore="false" >}}

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
{{< external-link url="https://galaxy.ansible.com/ui/repo/published/c2platform/atlassian/docs/" text="Ansible Galaxy" htmlproofer_ignore="false" >}}
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.atlassian
ansible-doc -t filter --list c2platform.atlassian
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```


---


<!--
id | 40315299
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-collection-atlassian" data-proofer-ignore>c2platform/ansible-collection-atlassian&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
