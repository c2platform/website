---
categories: GitLab Projects
description: "Vagrant plugin to run Windows sysprep as a provisioning step"
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: vagrant-windows-sysprep
tags: []
title: "Vagrant Windows Sysprep Provisioner"
weight: 15
---

GitLab: <a href="https://gitlab.com/c2platform/rws/vagrant-windows-sysprep"><code>c2platform/rws/vagrant-windows-sysprep</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


{{< external-link url="https://img.shields.io/gem/v/vagrant-windows-sysprep.svg" text="![Latest version released" htmlproofer_ignore="false" >}}](https://rubygems.org/gems/vagrant-windows-sysprep)
{{< external-link url="https://img.shields.io/gem/dt/vagrant-windows-sysprep.svg" text="![Package downloads count" htmlproofer_ignore="false" >}}](https://rubygems.org/gems/vagrant-windows-sysprep)

This is a Vagrant plugin to sysprep Windows.

**NB** This was only tested with Vagrant 2.2.14 and Windows Server 2016/2019/2022 and Windows 10 1809.

# Installation

```bash
vagrant plugin install vagrant-windows-sysprep
```

# Usage

Add `config.vm.provision "windows-sysprep"` to your `Vagrantfile` to sysprep your
Windows VM during provisioning or manually run the provisioner with:

```bash
vagrant provision --provision-with windows-sysprep
```

To troubleshoot, set the `VAGRANT_LOG` environment variable to `debug`.

## Example

In this repo there's an example {{< external-link url="https://gitlab.com/c2platform/rws/vagrant-windows-sysprep/-/blob/master/Vagrantfile" text="Vagrantfile" htmlproofer_ignore="false" >}}. Use it to launch
an example.

First install the {{< external-link url="https://github.com/rgl/windows-vagrant" text="Base Windows 2019 Box" htmlproofer_ignore="false" >}}.

Then launch the example:

```bash
vagrant up --provider=libvirt # or --provider=virtualbox
```

# Development

To hack on this plugin you need to install {{< external-link url="https://gitlab.com/c2platform/rws/vagrant-windows-sysprep/-/blob/master/http://bundler.io/" text="Bundler" htmlproofer_ignore="false" >}}
and other dependencies. On Ubuntu:

```bash
sudo apt install bundler libxml2-dev zlib1g-dev
```

Then use it to install the dependencies:

```bash
bundle
```

Build this plugin gem:

```bash
rake
```

Then install it into your local vagrant installation:

```bash
vagrant plugin install pkg/vagrant-windows-sysprep-*.gem
```

You can later run everything in one go:

```bash
rake && vagrant plugin uninstall vagrant-windows-sysprep && vagrant plugin install pkg/vagrant-windows-sysprep-*.gem
```


---


<!--
id | 51627561
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/rws/vagrant-windows-sysprep" data-proofer-ignore>c2platform/rws/vagrant-windows-sysprep&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
