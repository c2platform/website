---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-apps
tags: ['ansible', 'application', 'collection', 'desktop', 'docker', 'guacamole', 'jenkins', 'linux', 'nextcloud', 'nexus', 'sonarqube']
title: "Ansible Collection - c2platform.apps"
weight: 11
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-collection-apps"><code>c2platform/ansible-collection-apps</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-apps/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-apps/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-apps/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-apps/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.gis-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/apps/)

C2 Platform applications such as Guacamole, Nextcloud, Jenkins, Nexus, SonarQube

## Roles

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-apps/-/blob/master/roles/desktop" text="desktop" htmlproofer_ignore="false" >}} create a Docker based desktop using {{< external-link url="https://hub.docker.com/r/onknows/desktop" text="onknows/desktop" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-apps/-/blob/master/roles/guacamole" text="guacamole" htmlproofer_ignore="false" >}} install {{< external-link url="https://guacamole.apache.org/" text="Apache Guacamole" htmlproofer_ignore="false" >}} remote desktop gateway for example for {{< external-link url="https://hub.docker.com/r/onknows/desktop" text="onknows/desktop" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-apps/-/blob/master/roles/jenkins" text="jenkins" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-apps/-/blob/master/roles/nextcloud" text="nextcloud" htmlproofer_ignore="false" >}} install {{< external-link url="https://nextcloud.com/" text="Nextcloud" htmlproofer_ignore="false" >}}.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-apps/-/blob/master/roles/nexus" text="nexus" htmlproofer_ignore="false" >}} install {{< external-link url="https://www.sonatype.com/products/repository-pro" text="Nexus Repository" htmlproofer_ignore="false" >}}.

## Plugins


---


<!--
id | 39048988
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-collection-apps" data-proofer-ignore>c2platform/ansible-collection-apps&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
