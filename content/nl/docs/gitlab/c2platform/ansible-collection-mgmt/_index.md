---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-mgmt
tags: ['ansible', 'application', 'awx', 'backup', 'collection', 'harbor', 'monit']
title: "Ansible Collection - c2platform.mgmt"
weight: 17
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-collection-mgmt"><code>c2platform/ansible-collection-mgmt</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-mgmt/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-mgmt/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-mgmt/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-mgmt/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.mgmt-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/mgmt/)

C2 Platform management roles that are typically used within management domain.

## Roles

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mgmt/-/blob/master/roles/backup" text="backup" htmlproofer_ignore="false" >}} used by apps to configure backup and perform restore.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mgmt/-/blob/master/roles/awx" text="awx" htmlproofer_ignore="false" >}} manage [Ansible Automation Platform (AAP) or
  AWX]({{< relref path="/docs/concepts/ansible/aap" >}}).
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-mgmt/-/blob/master/roles/harbor" text="harbor" htmlproofer_ignore="false" >}} provision and manage Harbor.

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
{{< external-link url="https://galaxy.ansible.com/ui/repo/published/c2platform/mgmt/docs/" text="Ansible Galaxy" htmlproofer_ignore="false" >}}
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.mgmt
ansible-doc -t filter --list c2platform.mgmt
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```


---


<!--
id | 39049016
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-collection-mgmt" data-proofer-ignore>c2platform/ansible-collection-mgmt&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
