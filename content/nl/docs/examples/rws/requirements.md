---
title: "Collecties Installeren met een Vereistenbestand"
linkTitle: "Collecties Vereistenbestand"
categories: ["Voorbeeld"]
tags: [ansible, collectie, aap, awx, requirements.yml, ansible-galaxy]
weight: 6
description: >
   Een Ansible vereistenbestand maakt het mogelijk om meerdere collecties met één enkele opdracht te installeren, en dit bestand kan ook door AAP/AWX worden gebruikt.
---

Een [Ansible playbook project]({{< relref path="/docs/concepts/ansible/projects/inventory" >}}) zoals
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) bevat doorgaans een
vereistenbestand `collections\requirements.yml`. De voorbeeldinhoud voor dit
bestand is hieronder weergegeven:

```yaml
---
collections:
  - name: ansible.windows
    version: 1.14.0
  - name: community.windows
    version: 1.13.0
  - name: https://gitlab.com/c2platform/rws/ansible-collection-gis.git
    type: git
    version: 1.0.1
  - name: https://gitlab.com/c2platform/rws/ansible-collection-wincore.git
    type: git
    version: master
```

Op basis van het gegeven Ansible vereistenbestand zal Ansible de volgende acties
uitvoeren:
1. Installeer de collectie `ansible.windows` met versie `1.14.0`.
2. Installeer de collectie `community.windows` met versie `1.13.0`.
3. Clone de Git-repository
   `https://gitlab.com/c2platform/rws/ansible-collection-gis.git` en installeer
   de collectie met versie `1.0.1`.
4. Clone de Git-repository
   `https://gitlab.com/c2platform/rws/ansible-collection-wincore.git` en
   installeer de collectie vanaf de `master`-branch.

Ansible zal deze acties uitvoeren wanneer de opdracht `ansible-galaxy collection
install -r collections/requirements.yml -p .` wordt uitgevoerd. De `-r` vlag
geeft het te gebruiken vereistenbestand aan, en de `-p` vlag geeft het pad aan
waar de collecties geïnstalleerd zullen worden (in dit geval de huidige map).

```bash
ansible-galaxy collection install -r collections/requirements.yml -p .
```

Bovendien kan dit bestand ook worden gebruikt door [Ansible Automation Platform
(AAP) of AWX]({{< relref path="/docs/concepts/ansible/aap" >}}). Voor meer
informatie zie {{< external-link url="https://docs.ansible.com/ansible/latest/collections_guide/collections_installing.html#install-multiple-collections-with-a-requirements-file" text="Collecties installeren — Ansible Documentatie" htmlproofer_ignore="false" >}}