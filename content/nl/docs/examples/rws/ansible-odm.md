---
categories: ["Voorbeeld"]
tags: [rws, odm]
title: "Stimuleren van Innovatie: De Open Aanpak van RWS voor Ansible Automatisering"
linkTitle: "ODM"
weight: 6
description: >
    Rijkswaterstaat omarmt een open aanpak van Ansible engineering en bevordert het hergebruik van ideeën en code. Dit leidt tot meer flexibiliteit en productiviteit, vooral in het open source/internetdomein.
---

Voor meer informatie over deze aanpak, raadpleeg [ODM / OSS]({{< relref path="/docs/concepts/oss" >}}). Deze pagina biedt een uitgebreide uitleg over
hoe deze aanpak wordt gebruikt om het **Rijkswaterstaat (RWS) GIS Platform** te
beheren. Het behandelt drie verschillende Ansible-disciplines, vergezeld van
tekst en diagrammen:

## Ansible engineering

Deze discipline richt zich op het creëren van herbruikbare fundamentele
automatiseringsbouwblokken via [Ansible Collections]({{< relref path="/docs/concepts/ansible/projects/collections" >}}) en [Ansible Roles]({{<
relref path="/docs/concepts/ansible/projects/roles" >}}). Deze activiteiten
vinden voornamelijk plaats in het open source/internetdomein, wat maximale
flexibiliteit en productiviteit oplevert. Het configuratie/playbook-project voor
deze [referentie-implementatie]({{< relref path="/docs/concepts/ri" >}}) is het
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) project. Dit
project maakt de lokale implementatie van een complete en functionele omgeving
van het RWS GIS Platform mogelijk. Hiervoor maakt het project gebruik van de
Ansible collecties [`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}) en
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}).
Deze projecten zijn openbare open-source projecten die deel uitmaken van het
[GitLab Open Source Program]({{< relref path="/docs/concepts/gitlab" >}}).

Het onderstaande diagram illustreert het voorzien van een lokale instantie van
het RWS GIS Platform door een open-source Ansible engineer met behulp van
[Vagrant]({{< relref path="/docs/concepts/dev/vagrant" >}}). Het voorbeeld toont
vier lokale nodes:

1. `gsd-rproxy1`: Een Apache2 instantie die draait in een [LXD]({{< relref path="/docs/concepts/dev/lxd" >}}) container. Voor meer informatie over dit
   type node, raadpleegt u de gids over [Het instellen van een Reverse Proxy en
   CA-server]({{< relref path="/docs/howto/c2/reverse-proxy" >}}).
2. `gsd-agwat1`, `gsd-adserver1`, `gsd-agportal1`: Dit vertegenwoordigt
   respectievelijk ArcGIS Webadapter, ArcGIS Server en ArcGIS Portal.

De engineer beheert de GitLab-projecten opgeslagen in de {{< external-link url="https://gitlab.com/c2platform/rws/" text="c2platform/rws/" htmlproofer_ignore="false" >}} map op {{< external-link url="https://gitlab.com" text="gitlab.com" htmlproofer_ignore="false" >}} door wijzigingen te pushen en
te pullen.

Daarnaast worden de projecten [`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}) en
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})
periodiek gepusht naar de {{< external-link url="https://dev.azure.com/Rijkswaterstaat" text="RWS Azure DevOps Namespace" htmlproofer_ignore="true" >}} om ontwikkeling op de {{< external-link url="https://docs.ansible.com/ansible/latest/network/getting_started/basic_concepts.html#control-node" text="Ansible Control Node" htmlproofer_ignore="false" >}} mogelijk te maken.
Raadpleeg [Ansible ad-hoc]({{< relref path="#ansible-ad-hoc" >}}) voor meer
details.

Open-source projecten die deel uitmaken van het [GitLab Open Source Program]({{<
relref path="/docs/concepts/gitlab" >}}) profiteren van gratis en onbeperkte
CI/CD-workloads. Als gevolg hiervan bevatten de projecten [`c2platform.gis`]({{<
relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})
en [`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}) een
CI/CD-pijplijn die publiceert naar de [Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}) website.

```plantuml
@startuml rws-ansible-engineering
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(ok, "Ansible Engineer")

Boundary(local, "local", $type="high-end dev laptop") {
    Container(git_gis_local, "ansible-gis\nansible-collection-gis\nansible-collection-wincore", "git repo")
    Boundary(gis_local, "gis platform", $type="") {
        Container(gsd_rproxy1, "gsd-rproxy1", "lxd")
        Container(gsd_agserver1, "gsd-agserver1", "virtualbox")
        Container(gsd_agwa1, "gsd-agwa1", "virtualbox")
        Container(gsd_agportal1, "gsd-agportal1", "virtualbox")
    }
    Container(vagrant, "Vagrant", "")
}

Boundary(internet, "internet", $type="") {
    Boundary(rws_saas, "rws", $type="saas") {
        Boundary(azure_devops, "dev.azure.com/Rijkswaterstaat", $type="") {
            Container(git_gis_rws, "nansible-collection-gis\nansible-collection-wincore", "git repo")
        }
    }
    Boundary(oss, "oss", $type="saas") {
        Boundary(gitlab, "gitlab.com/c2platform/rws", $type="") {
            Container(git_gis, "ansible-gis\nansible-collection-gis\nansible-collection-wincore", "git repo")
        }
        Boundary(galaxy, "galaxy.ansible.com/c2platform", $type="") {
            Container(galaxy_collections, "c2platform.gis\nc2platform.wincore", "galaxy collection")
        }
    }
}


Rel(ok, git_gis_local, "Push &\npull changes", "")
Rel(ok, vagrant, "Vagrant up\nprovision\ndestroy", "")
Rel_Right(vagrant, git_gis_local, "", "")
Rel_Down(vagrant, gis_local, "Ansible\nprovision", "")
Rel_Right(git_gis_local, git_gis, "Push & pull\nremote changes", "")
Rel_Right(git_gis, git_gis_rws, "Push & pull\nremote changes", "")

Rel(gsd_rproxy1, gsd_agwa1, "", "https")
Rel(gsd_agwa1, gsd_agserver1, "", "https")
Rel(gsd_agwa1, gsd_agportal1, "", "https")

Rel_Down(git_gis, galaxy_collections, "Release", "gitlab-pipeline")
@enduml
```

## Ansible configuratie

Deze discipline omvat het maken van een Ansible configuratieproject, vaak
aangeduid als een Ansible playbook project. Het omvat inventaris, plays en
configuraties voor de omgevingen van het RWS GIS Platform in het RWS datacenter.
Het is belangrijk op te merken dat deze discipline aanzienlijk verschilt van
[Ansible engineering]({{< relref path="#ansible-engineering" >}}). Daarom is de
primaire rol een **Ansible Gebruiker** in plaats van een **Ansible Engineer**.

De taken in deze discipline zijn over het algemeen eenvoudig en kunnen worden
uitgevoerd via webinterfaces: het {{< external-link url="https://www.ansible.com/products/automation-platform" text="Ansible Automation Platform (AAP)" htmlproofer_ignore="false" >}} en de **Azure Devops**
webinterface. Echter, het wordt sterk aangeraden om [Visual Studio Code]({{<
relref path="/docs/concepts/dev/vscode" >}}) te gebruiken als het
voorkeursmiddel voor het beheren van het Ansible configuratie/playbook project.
Dit is vooral het geval bij grotere en meer complexe projecten zoals {{<
external-link
url="https://dev.azure.com/Rijkswaterstaat/IVP-TB-Geo/_git/ansible-gis"
text="ansible-gis" htmlproofer_ignore="true" >}} en het upstream open-source
referentieproject [`ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}), aangezien
navigeren en beheren zonder een IDE uitdagend kan worden.

```plantuml
@startuml rws-ansible-configuration
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Boundary(internet, "internet", $type="") {
    Boundary(rws_saas, "rws", $type="saas") {
        Boundary(azure_devops, "dev.azure.com/Rijkswaterstaat", $type="") {
            Container(git_gis_rws_config, "ansible-gis", "git repo")
        }
    }
    Boundary(oss, "oss", $type="saas") {
        Boundary(galaxy, "galaxy.ansible.com/c2platform", $type="") {
            Container(galaxy_collections, "c2platform.gis\nc2platform.wincore", "galaxy collection")
        }
    }
}

Boundary(rws, "rws.nl", $type="dc") {
    Person(ansible_user, "Ansible Gebruiker")
    Boundary(rws_lab, "gis platform", $type="lab") {
        Container(netscaler, "Netscaler", "")
        Container(agwebadaptor, "ArcGIS Webadaptor", "")
        Container(agserver, "ArcGIS Server", "")
        Container(agportal, "ArcGIS Portal")
    }
    Boundary(rws_mngt, "beheer", $type="") {
        Container(rws_ansible, "Ansible", "ansible-dev-control-node")
        Container(rws_aap, "AAP", "ansible-control-node")
    }
}

Rel(netscaler, agwebadaptor, "", "https")
Rel(agwebadaptor, agserver, "", "https")
Rel(agwebadaptor, agportal, "", "https")

Rel_Right(rws_aap, galaxy_collections, "Download Ansible\nrollen & collecties", "https")
Rel_Down(rws_aap, git_gis_rws_config, "Download\nGIS Platform\nconfiguratie", "https")
Rel_Down(rws_aap, rws_lab, "Provision", "winrm\nssh")

Rel_Down(ansible_user, rws_aap, "Start / configure\nJob templates", "https")
Rel_Down(ansible_user, rws_ansible, "", "rdp\nssh")

Rel_Down(rws_ansible, git_gis_rws_config, "Push & pull\nconfig wijzigingen", "https")
@enduml
```

## Ansible ad-hoc

Deze discipline draait om het gebruik van Ansible op een {{< external-link url="https://docs.ansible.com/ansible/latest/network/getting_started/basic_concepts.html#control-node" text="Ansible Control Node" htmlproofer_ignore="false" >}} om ad-hoc technische
onderhoudstaken uit te voeren met Ansible. Zie ook [Handleiding: Instellen van
Ansible Control Node]({{< relref path="/docs/howto/rws/control-node" >}}).