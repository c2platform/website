---
categories: ["Voorbeeld"]
tags: [awx, tower, redhat, automatisering, platform, ansible, ri1]
title: "AWX"
linkTitle: "AWX"
weight: 2
description: >
  AWX met Ansible
---

{{< under_construction >}}

Red Hat Automatiseringsplatform

```plantuml
@startuml awx-example

!includeurl https://gitlab.com/dburet/journal/raw/master/plantuml.cfg

rectangle "c2platform.org"  << dmz >>  as lan {
    rectangle "<$reverse_proxy>\nReverse Proxy\nc2d-rproxy1" << server >> als ReverseProxy
    rectangle "<$web_server>\nAWX\nc2d-awx" als AWX
}

rectangle "<$users>Gebruikers" als users

users --> ReverseProxy << https >> : https://awx.c2platform.org
ReverseProxy --> AWX << https >>
@enduml
```