---
title: "CI/CD Pipelines voor Ansible Collections"
linkTitle: "Ansible Collections"
tags: [ci/cd, gitlab, ansible, collection, .gitlab-ci.yml]
weight: 6
description: >
   Optimaliseer het ontwikkel- en releaseproces van Ansible-collecties met een
   CI/CD-pijplijn naar Ansible Galaxy.
---

Om het ontwikkel- en releaseproces van Ansible-collecties te optimaliseren, kun
je een CI/CD-pijplijn maken die codevalidatie en handmatige stappen bevat voor
het uitbrengen van een nieuwe versie van een Ansible-collectie naar [Ansible
Galaxy]({{< relref path="/docs/concepts/ansible/galaxy.md" >}}).

## Beschrijving van de pijplijn

Om het ontwikkel- en releaseproces van Ansible-collecties te stroomlijnen, kun
je Git integreren met CI/CD-pijplijnen. Het voorbeeldbestand
[`.gitlab-ci.yml`]({{< relref path="./gitlab-cicd-collection.md" >}}) dat aan
deze pagina is gekoppeld, biedt een sjabloonpijplijn die codevalidatie en
handmatige stappen omvat voor het uitgeven van een nieuwe versie van een
Ansible-collectie aan Ansible Galaxy.

Het bestand bevat de configuratie voor de pijplijnfasen en -taken:

### Build

De `prepare`-taak in de buildfase extraheert de collectieversie, naam en
namespace uit het `galaxy.yml`-bestand en slaat deze op als omgevingsvariabelen
voor later gebruik. Het maakt een `variables.env`-bestand aan om deze variabelen
als artifacts op te slaan.

De `build`-taak verpakt de Ansible-collectie en genereert het
`README.md`-bestand vanuit `README-GALAXY.md`. De resulterende collectie wordt
als een artifact opgeslagen.

### Linters

De `yamllint`-taak voert het `yamllint`-hulpmiddel uit om de YAML-code te
valideren.

De `ansible-lint`-taak installeert de collectie die in de vorige fase is gebouwd
en voert het `ansible-lint`-hulpmiddel uit om de Ansible-code te valideren.

### Galaxy

De `publish`-taak publiceert de Ansible-collectie naar Ansible Galaxy. Het
verpakt de collectie, genereert het `README.md`-bestand vanuit
`README-GALAXY.md` en publiceert het naar Ansible Galaxy met behulp van de
opgegeven API-sleutel.

### Release

De `gitlab-release`-taak maakt een release in GitLab voor de Ansible-collectie.
Het gebruikt de informatie uit `galaxy.yml`, inclusief de versie en changelog,
om de release te creëren. De release is gekoppeld aan de commit en kan worden
benaderd via de opgegeven release-naam.

## Gebruik

Om deze pijplijn als sjabloon voor jouw Ansible-collecties te gebruiken, volg je
deze stappen:

1. Richt je GitLab-repository in met de benodigde configuratiebestanden:
   `.gitlab-ci.yml` en `galaxy.yml`.
2. Configureer de fasen en taken in het `.gitlab-ci.yml`-bestand op basis van
   jouw specifieke vereisten. Kopieer de bijgewerkte pijplijnconfiguratie
   hieronder.
3. Werk het `galaxy.yml`-bestand bij met de relevante informatie voor jouw
   Ansible-collectie, zoals de namespace, naam, versie, beschrijving en links
   naar documentatie en issues.
4. Zorg ervoor dat je de vereiste afhankelijkheden hebt geïnstalleerd, inclusief
   Python, Pip, Ansible, yq, yamllint en ansible-lint. Pas de
   pakketinstallatiecommando's in de `before_script`-sectie van het
   `.gitlab-ci.yml`-bestand aan indien nodig.
5. Stel de benodigde omgevingsvariabelen en geheimen in je GitLab
   CI/CD-instellingen in, inclusief de `GALAXY_API_KEY` voor publicatie naar
   Ansible Galaxy.
6. Commit en push je wijzigingen om de pijplijn te activeren. De pijplijn draait
   bij merge-aanvragen en op de standaardbranch.

## Voordelen

Door Git met CI/CD-pijplijnen te integreren voor Ansible-collecties, kun je
profiteren van de volgende voordelen:

- Zorg voor codekwaliteit door het valideren van de YAML- en Ansible-code.
- Automatiseer het proces van het bouwen en publiceren van collecties naar
  Ansible Galaxy.
- Volg en creëer releases in GitLab, waarbij ze worden gekoppeld aan specifieke
  commits en changelog-informatie wordt verstrekt.

## Voorbeelden

* [voorbeeld]({{< relref path="./gitlab-cicd-collection.md" >}}) gekoppeld aan
  deze pagina.
* Bekijk het `.gitlab-ci.yml`-bestand van de Ansible-collectie
  [`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}). Dit project
  bevat ook een Azure CI/CD-pijplijn, zie `azure-pipelines.yml`.