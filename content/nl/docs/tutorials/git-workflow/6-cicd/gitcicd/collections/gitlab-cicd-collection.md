---
title: "Voorbeeld CI/CD-pijplijn voor een Ansible-collectie"
linkTitle: "Voorbeeld Ansible-collectie Pijplijn"
categories: ["Voorbeeld"]
tags: [ci/cd, gitlab, ansible, collectie, .gitlab-ci.yml]
weight: 6
description: >
   Voorbeeld CI/CD-pijplijn voor een Ansible-collectie
---

Projecten: [`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

---

Een GitLab-pijplijn voor een [Ansible collectie project]({{< relref path="/docs/concepts/ansible/projects/collections" >}}) wordt gerealiseerd door
het maken van een bestand `.gitlab-ci.yml` in de root van de Git repository.

Voor de meest actuele versie van dit script kun je de bijgeleverde gebruiken in
de [`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}) Ansible-collectie.

```yaml
---
before_script:
  - apt-get update -qy
  - apt-get install -y python3-dev python3-pip jq
  - apt install python3.11-venv -y
  - python3 -m venv /tmp/c2
  - source /tmp/c2/bin/activate
  - pip3 install yq yamllint==1.32.0 ansible-lint==6.17.1
  - pip3 install ansible-core==2.15.0
  - ansible-galaxy collection install -r requirements.yml
  - python3 --version
  - pip3 --version
  - ansible --version
  - ansible-lint --version
  - yamllint --version

workflow:  # voer de pijplijn alleen uit op MRs en standaard branch
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

stages:
  - build
  - linters
  - galaxy
  - release

prepare:
  stage: build
  script:
    - C2_VERSION=$(yq '.version' galaxy.yml | sed 's/"//g')
    - C2_NAME=$(yq '.name' galaxy.yml | sed 's/"//g')
    - C2_NAMESPACE=$(yq '.namespace' galaxy.yml | sed 's/"//g')
    - echo "C2_VERSION=$C2_VERSION" >> variables.env
    - echo "C2_NAME=$C2_NAME" >> variables.env
    - echo "C2_NAMESPACE=$C2_NAMESPACE" >> variables.env
  artifacts:
    reports:
      dotenv: variables.env

build:
  stage: build
  needs: [prepare]
  script:
    - cat README-GALAXY.md > README.md  # readme eigenschap in galaxy.yml wordt genegeerd door galaxy website
    - ansible-galaxy collection build . --force
  artifacts:
    paths:
      - $C2_NAMESPACE-$C2_NAME-$C2_VERSION.tar.gz

yamllint:
  stage: linters
  script:
    - yamllint -c .yamllint .

ansible-lint:
  stage: linters
  needs: [prepare, build]
  script:
    - ansible-galaxy collection install $C2_NAMESPACE-$C2_NAME-$C2_VERSION.tar.gz
    - ansible-lint -c .ansible-lint

publish:
  stage: galaxy
  needs: [prepare, build, yamllint, ansible-lint]
  script:
    - cat README-GALAXY.md > README.md  # readme eigenschap in galaxy.yml wordt genegeerd door galaxy website
    - ansible-galaxy collection build . --force
    - ansible-galaxy collection publish $C2_NAMESPACE-$C2_NAME-$C2_VERSION.tar.gz --api-key $GALAXY_API_KEY
  when: manual

gitlab-release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs: [publish, prepare]
  before_script: []
  script:
    - echo "Maak release voor $C2_NAMESPACE.$C2_NAME $C2_VERSION"
  release:
    name: $C2_NAMESPACE.$C2_NAME $C2_VERSION
    description: './CHANGELOG.md'
    tag_name: $C2_VERSION
    ref: $CI_COMMIT_SHA
    assets:
      links:
        - name: $C2_NAMESPACE.$C2_NAME
          url: https://galaxy.ansible.com/$C2_NAMESPACE/$C2_NAME
```