---
title: "Configuratieproject"
linkTitle: "Configuratieproject"
weight: 2
description: >
  Leg de basis voor het beheren van je Ansible-automatisering door een Ansible-configuratieproject te creëren, ook wel een playbook-project genoemd.
---