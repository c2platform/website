---
categories: ["Slides"]
tags: [gitops, pipeline, ansible-inventory]
title: "GIS Platform GitOps-pijplijn"
linkTitle: "GitOps-pijplijn"
draft: true
weight: 1
description: >
    Presentatie van de GitOps-pijplijn voor het GIS-platform, inclusief inventarisatiestrategie, Git-takstructuur en Ansible-automatisering.
---

<iframe src="https://c2platform.org/slides/oorae4Ko4i/gitops-pipeline/" width="600" height="400" style="border:none;"></iframe>