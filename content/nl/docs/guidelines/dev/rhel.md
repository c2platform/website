---
categories: ["Richtlijn"]
tags: [rhel, redhat, subscription]
title: "Optimaliseren van RHEL-Registratie en Abonnementsautomatisering"
linkTitle: "RHEL Geautomatiseerde Registratie"
weight: 3
description: >
    Leer hoe automatisering van Red Hat Linux-registratie en abonnement de
    C2 Platform Development-omgeving verbetert en de toegang tot noodzakelijke
    bronnen en diensten vereenvoudigt.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---
> Automatiseer de registratie en afmelding van RHEL-machines met Vagrant in
> combinatie met de `vagrant-registration` plugin, wat de ontwikkelingstaken
> vereenvoudigt.

---

## Probleem

Toegang tot Red Hat-diensten en software repositories is cruciaal voor
ontwikkelaars die werken in omgevingen op basis van Red Hat Enterprise Linux
(RHEL). Het automatiseren van de systeemregistratie en
abonnementsbeheerprocessen is essentieel om ononderbroken toegang tot deze
essentiële bronnen te waarborgen.

## Context

In omgevingen die RHEL 9 gebruiken, ontstaat de behoefte aan een efficiënte
mechanisme om systemen te registreren bij Red Hat Subscription Management (RHSM)
en abonnementen te beheren. Dit stelt ontwikkelaars in staat naadloos
softwarepakketten te installeren, updates te ontvangen en Red Hat-diensten te
gebruiken.

## Oplossing

Door gebruik te maken van het Red Hat Developer-abonnement, kunnen ontwikkelaars
kostenbesparend hun RHEL 9-systeemregistraties beheren binnen
ontwikkelomgevingen. Dit omvat het registreren van het systeem bij RHSM en het
koppelen van een ontwikkelaarsabonnement, waardoor toegang tot cruciale Red
Hat-software repositories en diensten wordt vrijgegeven.

Om dit proces binnen de C2Platform ontwikkelomgeving te automatiseren, is de {{<
external-link url="https://github.com/projectatomic/adb-vagrant-registration"
text="vagrant-registration" htmlproofer_ignore="false" >}} vooraf geïnstalleerd.
Dit faciliteert een soepele, geautomatiseerde registratie-ervaring.

## Voorbeelden en implementatie

Het onderstaande voorbeeld illustreert het proces van het registreren van een
RHEL 9-systeem en het beheren van abonnementen via terminalcommando's:

```bash
subscription-manager register --username <gebruikersnaam>
subscription-manager attach
```

Voor gedetailleerde automatiseringsvoorbeelden verwijzen we naar het
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) project. Belangrijke punten
zijn:

1. Het `Vagrantfile` bevat een codefragment voor de registratieafhandeling.
   Indien je je {{< external-link url="https://developers.redhat.com" text="Red Hat Developer" htmlproofer_ignore="true" >}} accountgegevens configureert met
   de variabelen `RHEL_DEV_ACCOUNT` en `RHEL_DEV_SECRET`, zal de setup volledig
   geautomatiseerd zijn. Indien je deze inloggegevens niet configureert, zal
   Vagrant je vragen om de inloggegevens telkens wanneer je een RHEL-gebaseerde
   VM aanmaakt.
2. Het `Vagrantfile.yml` bevat een `rhel9` boxdefinitie met een `registration`
   vlag, die aangeeft dat systemen op basis van deze box automatisch worden
   geregistreerd bij creatie en worden afgemeld bij verwijdering.

Deze setup garandeert dat systemen die worden opgestart met de
rhel9-configuratie automatisch registratie en afmelding afhandelen, wat het
ontwikkelingsproces stroomlijnt en ervoor zorgt dat aan Red Hat's
abonnementsbeheer wordt voldaan.

**Registratie en Afmelding in Actie:**

De volgende terminaluitvoer demonstreert het automatische registratie- en
afmeldproces bij het beheren van een Vagrant-box:

1. Bij het uitvoeren van `vagrant up`, registreert het systeem zich bij Red Hat,
   gefaciliteerd door de vagrant-registration plugin.
2. Het uitvoeren van `vagrant destroy` activeert de afmelding van het systeem,
   waardoor een schone status behouden blijft en geen onnodige abonnementen
   actief blijven.

<p><details>
  <summary><kbd>Laat zien</kbd></summary><p>

Let op de onderstaande regels met "Registering box with vagrant-registration..."
en "Unregistering box with vagrant-registration..."

```bash
[:ansible-gis]└2 master(+15/-1,+1/-1) ± vagrant up gsd-ansible-repo --no-provision
Bringing machine 'gsd-ansible-repo' up with 'virtualbox' provider...
==> gsd-ansible-repo: Importing base box 'generic/rhel9'...
==> gsd-ansible-repo: Matching MAC address for NAT networking...
==> gsd-ansible-repo: Checking if box 'generic/rhel9' version '4.3.12' is up to date...
==> gsd-ansible-repo: Setting the name of the VM: ansible-gis_gsd-ansible-repo_1708418067798_69697
==> gsd-ansible-repo: Fixed port collision for 22 => 2222. Now on port 2206.
==> gsd-ansible-repo: Clearing any previously set network interfaces...
==> gsd-ansible-repo: Preparing network interfaces based on configuration...
    gsd-ansible-repo: Adapter 1: nat
    gsd-ansible-repo: Adapter 2: hostonly
==> gsd-ansible-repo: Forwarding ports...
    gsd-ansible-repo: 22 (guest) => 2206 (host) (adapter 1)
==> gsd-ansible-repo: Running 'pre-boot' VM customizations...
==> gsd-ansible-repo: Booting VM...
==> gsd-ansible-repo: Waiting for machine to boot. This may take a few minutes...
    gsd-ansible-repo: SSH address: 127.0.0.1:2206
    gsd-ansible-repo: SSH username: vagrant
    gsd-ansible-repo: SSH auth method: private key
==> gsd-ansible-repo: Machine booted and ready!
==> gsd-ansible-repo: Registering box with vagrant-registration...
==> gsd-ansible-repo: Checking for guest additions in VM...
==> gsd-ansible-repo: Setting hostname...
==> gsd-ansible-repo: Configuring and enabling network interfaces...
==> gsd-ansible-repo: Mounting shared folders...
    gsd-ansible-repo: /vagrant => /home/ostraaten/git/gitlab/c2/ansible-gis
    gsd-ansible-repo: /arcgis-software-repo => /software/projects/rws
    gsd-ansible-repo: /ansible-dev-collections => /home/ostraaten/git/gitlab/c2/ansible-dev-collections
==> gsd-ansible-repo: Machine not provisioned because `--no-provision` is specified.
θ66° [:ansible-gis]└2 master(+15/-1,+1/-1) ±

```

```bash
[:ansible-gis]└2 master(+15/-1,+1/-1) ± vagrant destroy gsd-ansible-repo
==> gsd-ansible-repo: Unregistering box with vagrant-registration...
    gsd-ansible-repo: Are you sure you want to destroy the 'gsd-ansible-repo' VM? [y/N] y
==> gsd-ansible-repo: Forcing shutdown of VM...
==> gsd-ansible-repo: Destroying VM and associated drives...
[:ansible-gis]└2 master(+15/-1,+1/-1) ±

```

</p></details></p>

De volgende twee handleidingen maken gebruik van Red Hat-boxen met automatische
registratie:

* [Creëer een Simpele Software Repository voor Ansible]({{< relref path="/docs/howto/rws/software" >}})
* [Installeer de Ansible Controle Node]({{< relref path="/docs/howto/rws/control-node" >}})