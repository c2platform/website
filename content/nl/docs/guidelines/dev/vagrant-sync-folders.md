---
categories: ["Richtlijn"]
tags: [vagrant]
title: "Vagrant Synchronisatie Mappen"
linkTitle: "Vagrant Synchronisatie Mappen"
weight: 1
description: >
  Leer hoe je moeiteloos een map van je hostmachine naar de gastmachine kunt synchroniseren.
---


Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---
> Creëer een lokaal en verborgen bestand genaamd `.sync_folders.yml` om
> efficiënt een map van je hostmachine naar de gastmachine te synchroniseren.

---

## Probleem

Tijdens het ontwikkelingsproces is het vaak noodzakelijk om bestanden en mappen
van je hostmachine beschikbaar te maken binnen een gastmachine die met Ansible
is geconfigureerd.

## Context

In de context van ontwikkeling zijn er scenario's waarin je toegang moet hebben
tot bestanden of directories op de hostmachine binnen een gastmachine. Dit komt
meestal voor bij het inrichten van een gastmachine met Ansible.

## Oplossing

{{< external-link url="https://developer.hashicorp.com/vagrant/docs/synced-folders" text="Vagrant Gesynchroniseerde Mappen" htmlproofer_ignore="false" >}} stelt je in staat om
naadloos aan de bestanden van je project op je hostmachine te werken terwijl je
gebruikmaakt van de middelen van de gastmachine voor compilatie of uitvoering.

Standaard deelt Vagrant de projectmap (de directory die de `Vagrantfile` bevat).
Je kunt echter dit gedrag aanpassen door de `Vagrantfile` te wijzigen om extra
mappen te synchroniseren op basis van de configuratie gedefinieerd in een
bestand genaamd `.sync_folders.yml`. Dit bestand wordt bewust genegeerd door Git
om onbedoelde opname in je versiebeheer repository te voorkomen.

## Voorbeelden en implementatie

Voor een praktisch voorbeeld van een gewijzigde `Vagrantfile` die gebruikmaakt
van het `.sync_folders.yml` bestand, raadpleeg het voorbeeld in
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}). Deze Vagrantfile toont hoe
je map synchronisatie kunt configureren. Het `.sync_folders.yml` bestand kan
YAML configuraties bevatten zoals de volgende:

```yaml
---
- src: /software/projects/rws/
  target: /arcgis-software-repo
- src: ../ansible-dev-collections/
  target: /ansible-dev-collections
```

In deze configuratie worden twee mappen gesynchroniseerd. Het eerste item in de
lijst maakt de ArcGIS Software beschikbaar binnen de gastmachine op
`C:\arcgis-software-repo`. Op de hostmachine bevindt deze software zich in de
`/software/projects/rws/` directory.

Daarnaast kun je het `group_vars/all/software-repo.yml` bestand bekijken, dat
een [projectvariabele]({{< relref path="/docs/guidelines/coding/var-prefix" >}})
introduceert genaamd `gs_software_repo`. Deze variabele wordt gebruikt om de
locatie van de software repository te configureren, zodat softwaredownloads
afhankelijk van de omgeving kunnen plaatsvinden:

```yaml
gs_software_repo:
  development_strix: http://192.168.88.5/software
  development: file:///C:/arcgis-software-repo
```

Bovendien behandelt de tweede vermelding in het `.sync_folders.yml` bestand
Ansible Collections die binnen het gast-OS worden aangepast. Het plaatst deze
collecties op het pad `C:\ansible-dev-collections` op MS Windows gasten en
`/ansible-dev-collections` op Linux-systemen. Dit stelt je in staat om ze te
gebruiken voor Ansible inrichting van binnenuit de gastmachine. Voor meer
details over het gebruik van Ansible zonder Vagrant, raadpleeg de pagina
[Ansible gebruiken zonder Vagrant]({{< relref path="/docs/howto/dev-environment/ansible-without-vagrant" >}}).