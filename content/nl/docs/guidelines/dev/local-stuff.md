---
categories: ["Richtlijn"]
tags: [group_vars]
title: "Lokale Dingen"
linkTitle: "Lokale Dingen"
weight: 2
description: >
  Ansible configuratie die lokaal moet blijven en genegeerd moet worden door Git.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---
> In [Ansible Inventory Projecten]({{< relref path="/docs/concepts/ansible/projects/inventory" >}}) maak een bestand
> `group_vars/all/local_stuff.yml` aan voor Ansible configuratie die je niet
> wilt delen met anderen. Dit bestand staat in `.gitignore` zodat je het niet
> per ongeluk toevoegt aan de git-repo.

---

## Probleem

Lokale configuratie wordt per ongeluk gedeeld met teamleden.

## Context

Tijdens de ontwikkeling willen we soms iets lokaal configureren en het niet aan
versiebeheer toevoegen en delen met teamleden. Bijvoorbeeld, we willen Ansible
configureren om een lokale proxyserver te gebruiken.

## Oplossing

Maak een bestand `group_vars/all/local_stuff.yml` aan. Dit bestand staat in
`.gitignore` zodat je het niet per ongeluk toevoegt aan de git-repo.