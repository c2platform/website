---
categories: ["Richtlijn"]
tags: [windows, backslashes, slashes, forward slashes]
title: Schuine Strepen vs. Backslashes bij het Richten op MS Windows Hosts
linkTitle: "Schuine Strepen voor Windows"
weight: 2
description: >
  Gebruik schuine strepen voor alle paden bij het richten op MS Windows hosts.
  Converteer naar backslashes alleen indien nodig.
---

> Bij het richten op MS-hosts, gebruik consequent schuine strepen voor paden. In
> het zeldzame geval dat een module, zoals Ansible's `win_package`, schuine
> strepen niet ondersteunt, pas dan een filter toe om schuine strepen naar
> backslashes te converteren, zoals `c2platform.core.replace_slashes`.

## Probleem

Het gebruik van backslashes bij het richten op Windows-hosts in
Ansible-projecten kan problemen veroorzaken omdat de backslash in Ansible
YAML-code als een escape-teken dient. Dit resulteert in verschillende potentiële
oplossingen, wat leidt tot inconsistentie en verschillende oplossingen in de
codebase zonder een gestandaardiseerde aanpak.

Overweeg een scenario waarin je een variabele, `fme_flow_backup_path`, hebt die
bedoeld is om een back-up te maken, verschillend per DTAP-omgeving.
Bijvoorbeeld, de "test" omgeving back-up moet zich bevinden op `\\backup\test`.
Een projectvariabele, `gs_env`, duidt het type omgeving aan en is gelijk aan
`test` bij het richten op een "test" omgevingsnode.

Een intuïtieve maar onjuiste aanvankelijke oplossing kan eruitzien als volgt:

```yaml
fme_flow_backup_path: "\\backup\{gs_env}\"  # ❌ Geen geldige Ansible / YAML code
```

De code hierboven is onjuist omdat de backslash verkeerd wordt geïnterpreteerd
als een escape-teken. Herformuleren vereist dat elke backslash geëscaped moet
worden:

```yaml
fme_flow_backup_path: "\\\\backup\\{gs_env}\\"
```

Alternatief, escape is niet nodig met enkele aanhalingstekens:

```yaml
fme_flow_backup_path: '\\backup\{gs_env}\'
```

Of vervang backslashes met schuine strepen:

```yaml
fme_flow_backup_path: "//backup/{gs_env}/"
```

Deze voorbeelden tonen verschillende methoden om Windows-paden in
Ansible-projecten te behandelen. Zonder een duidelijke richtlijn kunnen
persoonlijke voorkeuren van ontwikkelaars leiden tot inconsistente benaderingen.

## Oplossing

De aanbevolen aanpak in deze richtlijn is om consequent dubbele aanhalingstekens
en schuine strepen te gebruiken. Het gebruik van schuine strepen als
pad-scheidingstekens is over het algemeen effectief, vooral wanneer code wordt
gemaakt die functioneert op zowel Windows als POSIX (Unix/Linux/macOS) systemen.
Windows voert automatisch pad-normalisatie uit en converteert schuine strepen
naar backslashes[^1]. Voor Ansible-modules waar schuine strepen niet worden
ondersteund, gebruik een filter (bijv. `c2platform.core.replace_slashes`) om
schuine strepen naar backslashes te transformeren.

1. Volg de Ansible-communitypraktijk van het gebruik van dubbele
   aanhalingstekens.
2. Kies voor schuine strepen in plaats van backslashes, aangezien Windows ze
   ondersteunt via padnormalisatie[^1].
3. Gebruik een filter zoals `c2platform.core.replace_slashes` voor modules zoals
   `win_package`[^2] die backslashes vereisen.

Deze aanpak zorgt voor consistent gebruik van schuine strepen, ongeacht het
richten op Windows-hosts, terwijl filters worden toegepast om specifieke
uitzonderingen te behandelen.

## Voorbeeld en Implementatie

Om een Ansible-variabele met een Windows-pad te configureren, zoals in het
"test" omgevingsscenario dat naar een share `\\backup\test` verwijst, gebruik
"schuine strepen" zoals hieronder getoond, met `gs_env` gelijk aan `test`:

```yaml
fme_flow_backup_path: "//backup/{gs_env}/"
```

In gevallen waar schuine strepen niet effectief zijn, zoals bij de `win_package`
module die backslashes vereist[^2], gebruik dan de
`c2platform.core.replace_slashes` filter zoals getoond:

```yaml
---
- name: Windows Pakket
  hosts: windows
  vars:
    my_windows_package_path: //software/arcgis/WebDeploy_amd64_en-US.msi
  tasks:
    - name: Mijn Windows Pakket
      win_package:
        path: "{{ my_windows_package_path | c2platform.core.replace_slashes }}"
```

## Voetnoten

[^1]: Voor meer informatie over Windows-padnormalisatie en de omgang met schuine
    strepen, raadpleeg {{< external-link url="https://learn.microsoft.com/en-us/dotnet/standard/io/file-path-formats" text="Bestandsformaat paden op Windows-systemen - .NET | Microsoft Learn" htmlproofer_ignore="false" >}}

[^2]: Ten tijde van het maken van deze richtlijn werd het `win_package`
    Ansible-module genoteerd als de enige module die geen schuine strepen
    ondersteunt.