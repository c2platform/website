---

categories: ["Richtlijn"]
tags: [group_vars, samenvoegen, hash_gedrag, ansible]
title: "Beheer van Woordeboeksamenvoeging in C2 Platform Ansible Projecten"
linkTitle: "Woordeboeksamenvoeging"
weight: 1
description: >
    Best practices voor het gebruik van woordeboeksamenvoeging in C2 Ansible projectinventarissen.

---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform/rws/ansible-gis`]({{<
relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform/rws/ansible-collection-wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform/rws/ansible-collection-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

In C2 Platform [Ansible projectinventarissen]({{< relref path="/docs/concepts/ansible/projects/inventory" >}}), maakt het gebruik van de
instelling `hash_behaviour = merge` in `ansible.cfg` een flexibelere
datastructurering mogelijk. Zie als voorbeelden de `ansible.cfg` bestanden in
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) en
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}). Deze benadering is
bijzonder nuttig bij het beheren van configuraties voor Windows-systemen met
rollen zoals `c2platform.wincore.win` en `c2platform.win.linux`. Om deze
flexibiliteit volledig te benutten en naadloze woordeboeksamenvoeging te
garanderen, overweeg de volgende verbeterde richtlijnen en voorbeelden.

## Richtlijnen voor Effectieve Woordeboeksamenvoeging

1. **Onderzoek en Documenteer Uw Inventarisstructuur**: Documenteer duidelijk
   waar en hoe `hash_behaviour = merge` wordt toegepast binnen uw Ansible
   projecten. Het is cruciaal om een geactualiseerde inventarisstructuurlijst
   bij te houden, waarbij alle gevallen van samengevoegde woordeboeken worden
   genoteerd.
2. **Gebruik Samenvoeging Oordeelkundig**: Gebruik woordeboeksamenvoeging vooral
   voor host of groepsvariabelen die profiteren van deze methode. Overmatig
   gebruik kan leiden tot complexiteit en ondoorzichtige datarelaties.
3. **Weloverwogen en Unieke Sleutels**: Zorg ervoor dat bij gebruik van
   woordeboeken de sleutels goed gekozen en uniek zijn binnen uw
   inventarisproject.
4. **Strenge Testen**: Test playbooks grondig om te valideren dat samengevoegde
   woordeboeken zich gedragen zoals verwacht. Deze stap is essentieel om
   eventuele samenvoegingproblemen of onbedoelde gevolgen vroeg in de
   ontwikkelingscyclus op te sporen.
5. **Begrijp Variabele Prioriteit**: Maak uzelf vertrouwd met de regels van
   variabele prioriteit in Ansible. Kennis van hoe variabelen van verschillende
   niveaus (bijv. group_vars, host_vars) samensmelten, is cruciaal voor het
   voorspellen van de uiteindelijke configuratietoestand.
6. **Conflictoplossingsstrategie**: Ontwikkel en documenteer strategieën voor
   het omgaan met sleutelsconflicten in samengevoegde woordeboeken. Dit plan
   zorgt voor een consistente afhandeling van conflicten in projecten.
7. **Beveiliging van Gevoelige Gegevens**: Wanneer het samenvoegen van
   woordeboeken gevoelige informatie omvat, gebruik dan versleutelingsmethoden
   zoals Ansible Vault. Deze praktijk zorgt ervoor dat gevoelige gegevens veilig
   worden beheerd, terwijl niet-gevoelige informatie naar behoefte kan worden
   samengevoegd.

## Praktische Voorbeelden

### Samenvoegen van `win_chocolatey` Configuraties

Wanneer u de `win_chocolatey` variabele van de `c2platform.wincore.win` Ansible
rol gebruikt voor verschillende servergroepen binnen uw Ansible inventaris,
zorgt het gebruik van "woordeboeken" voor een meer dynamische en flexibele
benadering in vergelijking met eenvoudige "lijsten". Deze methode zorgt ervoor
dat servers in meerdere groepen alle benodigde pakketten ontvangen zonder
configuratieconflicten.

#### Voorbeeldconfiguratie in `group_vars/windows/main.yml`:

```yaml
win_chocolatey:
  windows:
    - name: nuget.commandline
```

#### Voorbeeldconfiguratie in `group_vars/age_pro/main.yml`:

```yaml
win_chocolatey:
  age_pro:
    - name: firefox
```

Wanneer een server zowel tot de `windows` als de `age_pro` groepen behoort,
omvat de samengevoegde configuratie voor `win_chocolatey` zowel de NuGet command
line als Firefox, wat zorgt voor een uitgebreide pakketbeheer over serverrollen.

#### Samengevoegde Configuratieresultaat

```yaml
win_chocolatey:
  windows:
    - name: nuget.commandline
  age_pro:
    - name: firefox
```

#### Flexibiliteit in Sleutelbenaming

U kunt willekeurige beschrijvende sleutelnamen gebruiken voor het categoriseren
van configuraties binnen woordeboeken, wat de leesbaarheid en organisatorische
duidelijkheid vergroot. We kunnen het voorbeeld wijzigen om sleutels `whatever`
en `some_other_key` te gebruiken.

```yaml
win_chocolatey:
  whatever:
    - name: nuget.commandline
```

```yaml
win_chocolatey:
  some_other_key:
    - name: firefox
```

#### Beperkingen in Sleutelbenaming

U kunt elke gewenste sleutelnaam kiezen, maar houd er rekening mee dat alleen
"woordeboeken" kunnen worden samengevoegd. Eenvoudige "lijsten" kunnen niet
worden samengevoegd.

1. In `group_vars/windows/main.yml`:
   ```yaml
   win_chocolatey:
     tools:
       - name: nuget.commandline
   ```
2. In `group_vars/age_pro/main.yml`:
   ```yaml
   win_chocolatey:
     tools:
       - name: firefox
   ```

{{< alert type="warning" >}} Het samengevoegde resultaat zal `firefox` of
`nuget.commandline` hebben en niet beide! U kunt in uw inventarisproject niet
meerdere keren een variabele hebben zoals `win_chocolatey` met dezelfde sleutel,
in dit geval `tools`. {{< /alert >}}

## Aanvullende Informatie

Voor aanvullende inzichten en richtlijnen:

* De how-to [Woordeboeksamenvoeging in Ansible Projecten Beoordelen]({{< relref path="/docs/howto/rws/merge" >}}) geeft praktische informatie over hoe u
  samengevoegd gedrag in een inventarisproject kunt beoordelen.