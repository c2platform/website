---
categories: ["Richtlijn"]
tags: [group_vars]
title: "Variabele prefix"
linkTitle: "Variabele prefix"
weight: 3
description: >
  Prefix variabele namen met rol of projectvoorvoegsel.

---

> Prefix variabelen in Ansible-rollen / collecties met de ronaam. Bijvoorbeeld
> `harbor_hostname`. Prefix andere / projectvariabelen met een
> projectvoorvoegsel, bijvoorbeeld `c2_cacerts2_ca_dir`.

---

## Probleem

Zonder een project- of rolvoorvoegsel is het niet altijd duidelijk waar
variabelen worden gebruikt. Zonder een voorvoegsel is er ook een kans op
conflicterende of dubbele variabelen.

## Context

In een typisch project worden vaak allerlei soorten variabelen aangemaakt of
gebruikt. Sommige variabelen worden in Ansible-rollen gebruikt, maar andere
niet. Dit kan verwarrend zijn en leiden tot fouten of problemen.

## Oplossing

1. Prefix alle variabelen die in een Ansible-rol worden gebruikt met de naam van
   de rol, bijvoorbeeld `harbor_`.
2. Prefix alle andere / projectvariabelen - die alleen bestaan in de map
   `group_vars` of `host_vars` - met een projectvoorvoegsel, bijvoorbeeld `c2_`.

## Voorbeelden en implementatie

1. Bijvoorbeeld `harbor_hostname` voor de hostname van de Harbor-instantie. Het
  voorvoegsel is `harbor_` omdat dit een variabele is die deel uitmaakt van de
  [`c2platform.mgmt.harbor`]({{< ref "/docs/gitlab/c2platform/ansible-collection-mgmt" >}}) Ansible-rol.
2. In het C2 Inventory-project [`c2platform/ansible`]({{< relref "/docs/gitlab/c2platform/ansible" >}}) wordt `c2_` gebruikt als voorvoegsel,
   zie bijvoorbeeld `c2_cacerts2_ca_dir` in `group_vars/all/smallca.yml`.
3. In het RWS Inventory-project [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) wordt `gs_` gebruikt als
   voorvoegsel. Zie [Configuratie van een Web Proxy Server Voorbeeld voor MS
   Windows Hosts]({{< relref path="/docs/howto/rws/windows/proxy" >}}) voor een
   voorbeeld van hoe `gs_proxy_regedit` wordt gebruikt om een webproxy te
   configureren.