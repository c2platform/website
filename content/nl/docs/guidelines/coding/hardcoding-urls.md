---
categories: ["Richtlijn"]
tags: [ansible, rol]
title: "Vermijd het Hardcoderen van Download URL's en Paden in Taken"
linkTitle: "Vermijd Hardcoderen van Paden en URL's"
weight: 4
description: >
  Richtlijnen en voorbeelden om Ansible flexibel en gebruiksvriendelijk te maken.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}), [`c2platform.gis`]({{<
relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

> Plaats geen pad- of URL-logica/conventies in taken, gebruik een nieuwe
> standaardvariabele.

---

## Probleem

Bij het ontwikkelen van Ansible-rollen is het essentieel om ze flexibel en
gemakkelijk te gebruiken te maken voor een breed scala aan scenario's. Een veel
voorkomende valkuil is het hardcoderen van bestandswegen en
naamgevingsconventies direct in Ansible-taken, waardoor de rol minder aanpasbaar
is.

## Context

In veel Ansible-rollen zijn taken ontworpen om te werken met specifieke
bestandsstructuren en naamgevingsconventies. Het afdwingen van deze conventies
in taken kan echter de bruikbaarheid en herbruikbaarheid van de rol beperken.

## Oplossing

Om flexibele Ansible-rollen te creëren, overweeg de volgende richtlijnen:

1. **Gebruik Variabelen voor Conventies**: In plaats van bestandswegen en
   naamgevingsconventies in taken vast te leggen, definieer ze als variabelen in
   het `defaults/main.yml` bestand van je rol. Dit stelt gebruikers in staat om
   deze parameters gemakkelijk aan te passen aan hun behoeften.
2. **Moedig Overschrijvingen aan**: Moedig gebruikers aan deze
   standaardvariabelen in hun eigen `group_vars` of `host_vars` te overschrijven
   wanneer ze je rol gebruiken. Dit zorgt ervoor dat ze de rol op hun specifieke
   omgeving kunnen afstemmen zonder de rol zelf te wijzigen.
3. **Documenteer de Variabelen**: Documenteer duidelijk de variabelen in de
   README of documentatie van je rol, leg hun doel uit en hoe gebruikers ze
   kunnen wijzigen. Het geven van voorbeelden van veelvoorkomende aanpassingen
   kan bijzonder nuttig zijn.

## Voorbeeld en Implementatie

Laten we deze richtlijnen illustreren met een voorbeeld:

### Vermijd het Hardcoderen van Download URL's in Taken

Overweeg de volgende Ansible-taak, die mogelijk wordt gevonden in een
takenbestand voor de `c2platform.gis.arcgis_server` rol. Het hardcodeert de
download-URL voor een licentiebestand met behulp van drie variabelen. Dit dwingt
een specifieke naamgevingsconventie voor het licentiebestand af, wat de
flexibiliteit beperkt.

```yaml
- name: Download License file
  ansible.windows.win_get_url:
    url: "{{ arcgis_server_base_url }}/{{ arcgis_server_role }}{{ arcgis_server_license_file_extension }}"
    dest: "{{ arcgis_server_install_temp_base }}"
    force: false
```

Deze praktijk beperkt de flexibiliteit en gebruiksvriendelijkheid van de rol.
Gebruikers hebben mogelijk geen toestemming om de rol te wijzigen, of dat kan
ongewenst zijn. Bovendien is het onnodig. Dezelfde functionaliteit kan worden
bereikt door het maken van een nieuwe variabele, bijvoorbeeld
`arcgis_server_license_url`, in het `defaults/main.yml` bestand van de rol:

```yaml
arcgis_server_license_url: "{{ arcgis_server_base_url }}/{{ arcgis_server_role }}{{ arcgis_server_license_file_extension }}"
```

Met deze opzet kunnen we de taak **Download License file** wijzigen om gebruik
te maken van `arcgis_server_license_url`:

```yaml
- name: Download License file
  ansible.windows.win_get_url:
    url: "{{ arcgis_server_license_url }}"
    dest: "{{ arcgis_server_install_temp_base }}"
    force: false
```

Met deze verbeterde aanpak kunnen gebruikers ervoor kiezen om de standaard
naamgevingsconventie voor het licentiebestand te accepteren bij het gebruik van
de Ansible-rol. Ze hebben echter ook de flexibiliteit om een alternatieve
conventie aan te nemen door een andere waarde voor `arcgis_server_license_url`
te definiëren of af te leiden binnen hun [inventarisproject]({{< relref path="/docs/concepts/ansible/projects/inventory" >}}), zonder de Ansible-rol
zelf te hoeven wijzigen.