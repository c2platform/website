---
categories: ["Richtlijn"]
tags: [group_vars]
title: "Lui naamgevingsconventie"
linkTitle: "Lui naamgevingsconventie"
weight: 10
description: >
  Naamgeving met het minste aantal toetsaanslagen.
---

---
> Gebruik alleen kleine letters met `-` om woorden te scheiden. Vermijd het
> gebruik van hoofdletters, underscores `_`, of camelcase omdat ze extra
> toetsaanslagen vereisen.

---

## Probleem

Naamgevingsconventies zijn belangrijk voor het behouden van de leesbaarheid en
consistentie van code. Veel naamgevingsconventies kunnen echter omslachtig zijn
en extra toetsaanslagen vereisen.

## Context

In softwareontwikkeling moeten we bestanden, projecten, branches en andere
entiteiten benoemen.

## Oplossing

Gebruik een naamgevingsconventie die prettig voor het oog is en het minste
aantal toetsaanslagen vereist. Gebruik alleen kleine letters met `-` om woorden
te scheiden. Vermijd het gebruik van hoofdletters, underscores `_`, of camelcase
omdat ze extra toetsaanslagen vereisen.

Deze benadering heeft de volgende voordelen:

1. Namen zijn gemakkelijk te lezen en te onthouden.
2. De inspanning voor typen wordt geminimaliseerd, wat typefouten vermindert en
   tijd bespaart.
3. Deze richtlijn is gemakkelijk te onthouden. Bij twijfel over een naam, kies
   de optie met het minste aantal toetsaanslagen.

## Voorbeeld en implementatie

Voorbeelden van bestandsnamen met deze conventie:

1. `index.html`
2. `style.css`
3. `main.js`

Voorbeelden van projectnamen met deze conventie:

1. `mijn-project`
2. `web-app`
3. `e-commerce`

Voorbeelden van branchnamen met deze conventie:

1. `feature/add-new-feature`
2. `fix/bug-123`
3. `hotfix/security-issue`