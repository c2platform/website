---
categories: [Voorbeeld]
tags: [tags, skip-tags, ansible]
title: Gebruik van Tags met de Linux Play
linkTitle: Voorbeeld Linux Play
weight: 1
toc_hide: true
hide_summary: true
description: >
  Leer hoe je "always", "system" en "install" tags effectief kunt gebruiken met
  de Ansible Linux rol voor geoptimaliseerde uitvoering van playbooks.
---

Projecten:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

---

De Linux rol {{< ansible-role-link role_name="c2platform.core.linux" >}}, vergelijkbaar met de Windows rol {{< ansible-role-link role_name="c2platform.wincore.win" subgroup="rws" >}}, is een veelzijdige, flexibele en generieke rol die ontworpen is voor het doelgericht inzetten op respectievelijk Linux- en Windows-hosts. Dit document beschrijft hoe het tag-schema, zoals beschreven in de richtlijn {{< rellink path="/docs/guidelines/coding/tags" >}}, kan worden toegepast en geïmplementeerd voor de Linux rol.

Bekijk de volgende Ansible play:

```yaml
---
- name: Linux rol
  hosts: localhost
  become: true

  roles:
    - { role: c2platform.core.secrets }  # → always
    - { role: c2platform.core.linux }  # → install, system, linux

  vars:
    linux_resources:
      - name: system_install_task.txt
        dest: /tmp/system_install_task.txt
        type: copy
        content: |
          willekeurig
```

Merk op dat deze play deel uitmaakt van het [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) project, zie `plays/examples/linux.yml`.

## Taken Lijst

Om mogelijke of beschikbare tags te bekijken die met de Ansible play kunnen worden gebruikt, voer de volgende opdracht uit:

```bash
ansible-playbook $PLAY -i $INVENTORY $LIMIT --list-tasks
```

Je zou uitvoer moeten zien die lijkt op het onderstaande. Let op dat de taken die deel uitmaken van de {{< ansible-role-link role_name="c2platform.core.secrets" >}} rol zijn getagd met `always`. Deze rol decodeert de {{< rellink path="/docs/howto/dev-environment/vault" >}} om geheimen beschikbaar te maken. Deze rol gebruikt de `always` tag om ervoor te zorgen dat geheimen consistent beschikbaar worden gemaakt voor de taken.

```txt
playbook: plays/examples/linux.yml

  play #1 (localhost): Linux rol	TAGS: []
    tasks:
      c2platform.core.secrets : Zet feit voor common_secrets_dirs	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Verouderde variabele	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Inclusief geheimen	TAGS: [always, secrets, vault]
      c2platform.core.vagrant_hosts : Zet feit vagrant_hosts_content	TAGS: [config, development, system, vagrant_hosts]
      c2platform.core.vagrant_hosts : Beheer /etc/hosts van Vagrant Host	TAGS: [config, development, system, vagrant_hosts]
      c2platform.core.vagrant_hosts : Beheer hosts bestand van Vagrant Linux gast	TAGS: [config, development, system, vagrant_hosts]
      c2platform.core.vagrant_hosts : Beheer hosts bestand van Vagrant Windows gast	TAGS: [config, development, system, vagrant_hosts]
      c2platform.core.vagrant_hosts : Beheer hosts bestand van Vagrant Windows gast	TAGS: [config, development, system, vagrant_hosts]
      include_tasks	TAGS: []
      include_tasks	TAGS: []
      c2platform.core.apt_repo : Voeg APT sleutel toe	TAGS: [debian, install, linux, system]
      c2platform.core.apt_repo : Voeg APT repository toe	TAGS: [debian, install, linux, system]
      c2platform.core.yum : Voeg YUM repository toe	TAGS: [install, linux, redhat, system, yum]
      c2platform.core.bootstrap : Pip conf	TAGS: [bootstrap, install, linux, system]
      c2platform.core.bootstrap : Pip trust store	TAGS: [bootstrap, install, linux, system]
      c2platform.core.bootstrap : Pip ca bundle	TAGS: [bootstrap, install, linux, system]
      c2platform.core.bootstrap : Inclusief pakket taken	TAGS: [bootstrap, install, linux, system]
      c2platform.core.bootstrap : Verouderingsbericht	TAGS: [bootstrap, install, linux, system]
      c2platform.core.os_trusts : CA distributie (RedHat)	TAGS: [config, linux, os_trusts, system]
      c2platform.core.os_trusts : Voer update-ca-trust uit	TAGS: [config, linux, os_trusts, system]
      c2platform.core.os_trusts : CA distributie (Debian)	TAGS: [config, linux, os_trusts, system]
      c2platform.core.os_trusts : Voer update-ca-certificates (Debian) uit	TAGS: [config, linux, os_trusts, system]
      c2platform.core.secrets : Zet feit voor common_secrets_dirs	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Verouderde variabele	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Stat secret dir	TAGS: [always, secrets, vault]
      c2platform.core.secrets : Inclusief geheimen	TAGS: [always, secrets, vault]
      c2platform.core.lcm : Ansible rollen namen zonder prefix	TAGS: [ansible]
      c2platform.core.lcm : lcm_roles_node	TAGS: [ansible]
      c2platform.core.lcm : Bepaal LCM info feiten	TAGS: [ansible]
      c2platform.core.lcm : LCM info feiten	TAGS: [ansible]
      c2platform.core.lvm : Verkrijg schijven	TAGS: [install, linux, lvm, system]
      c2platform.core.lvm : Verkrijg bruikbare apparaten	TAGS: [install, linux, lvm, system]
      c2platform.core.lvm : Verkrijg nodes lvm rollen	TAGS: [install, linux, lvm, system]
      include_tasks	TAGS: [install, linux, lvm, system]
      c2platform.core.lvm : Volume groep	TAGS: [install, linux, lvm, system]
      include_tasks	TAGS: [install, linux, lvm, system]
      c2platform.core.hosts : Inclusief OS-specifieke variabelen.	TAGS: [hosts, install, linux, network, system]
      include_tasks	TAGS: [hosts, install, linux, network, system]
      include_tasks	TAGS: [hosts, install, linux, network, system]
      c2platform.core.alias : Zet feit node rollen	TAGS: [alias, config, linux, system]
      c2platform.core.alias : Alias bestand	TAGS: [alias, config, linux, system]
      c2platform.core.linux : Inclusief linux_resources_types	TAGS: [install, linux, linux_acl, linux_alternatives, linux_apk, linux_apt, linux_apt_key, linux_apt_repo, linux_apt_repository, linux_apt_rpm, linux_archive, linux_at, linux_authorized_key, linux_blockinfile, linux_bundler, linux_capabilities, linux_command, linux_copy, linux_cpanm, linux_cron, linux_cronvar, linux_crypttab, linux_dconf, linux_deb822_repository, linux_debconf, linux_dnf, linux_dnf5, linux_dnf_versionlock, linux_dpkg_divert, linux_dpkg_selections, linux_easy_install, linux_fail, linux_fetch, linux_file, linux_filesize, linux_filesystem, linux_firewalld, linux_flatpak, linux_flatpak_remote, linux_gconftool2, linux_gconftool2_info, linux_gem, linux_get_url, linux_git, linux_git_config, linux_haproxy, linux_hostname, linux_htpasswd, linux_include_role, linux_iptables, linux_java_cert, linux_java_keystore, linux_known_hosts, linux_lineinfile, linux_locale_gen, linux_lvg, linux_lvg_rename, linux_lvol, linux_lxc_container, linux_lxd_container, linux_lxd_profile, linux_lxd_project, linux_mail, linux_mount, linux_nmcli, linux_npm, linux_package, linux_pacman, linux_pacman_key, linux_pam_limits, linux_pamd, linux_parted, linux_patch, linux_pause, linux_pip, linux_reboot, linux_replace, linux_rhsm_release, linux_rhsm_repository, linux_rpm_key, linux_runit, linux_script, linux_seboolean, linux_sefcontext, linux_selinux, linux_selinux_permissive, linux_selogin, linux_service, linux_shell, linux_shutdown, linux_slurp, linux_snap, linux_snap_alias, linux_ssh_config, linux_sudoers, linux_synchronize, linux_sysctl, linux_syslogger, linux_systemd, linux_systemd_service, linux_sysvinit, linux_tempfile, linux_template, linux_timezone, linux_ufw, linux_unarchive, linux_uri, linux_user, linux_wait_for, linux_wait_for_connection, linux_xml, linux_yum, linux_yum_repository, linux_yum_versionlock, linux_zfs, linux_zfs_delegate_admin, linux_zfs_facts, system]
      c2platform.core.linux : Inclusief linux_resources	TAGS: [install, linux, linux_acl, linux_alternatives, linux_apk, linux_apt, linux_apt_key, linux_apt_repo, linux_apt_repository, linux_apt_rpm, linux_archive, linux_at, linux_authorized_key, linux_blockinfile, linux_bundler, linux_capabilities, linux_command, linux_copy, linux_cpanm, linux_cron, linux_cronvar, linux_crypttab, linux_dconf, linux_deb822_repository, linux_debconf, linux_dnf, linux_dnf5, linux_dnf_versionlock, linux_dpkg_divert, linux_dpkg_selections, linux_easy_install, linux_fail, linux_fetch, linux_file, linux_filesize, linux_filesystem, linux_firewalld, linux_flatpak, linux_flatpak_remote, linux_gconftool2, linux_gconftool2_info, linux_gem, linux_get_url, linux_git, linux_git_config, linux_haproxy, linux_hostname, linux_htpasswd, linux_include_role, linux_iptables, linux_java_cert, linux_java_keystore, linux_known_hosts, linux_lineinfile, linux_locale_gen, linux_lvg, linux_lvg_rename, linux_lvol, linux_lxc_container, linux_lxd_container, linux_lxd_profile, linux_lxd_project, linux_mail, linux_mount, linux_nmcli, linux_npm, linux_package, linux_pacman, linux_pacman_key, linux_pam_limits, linux_pamd, linux_parted, linux_patch, linux_pause, linux_pip, linux_reboot, linux_replace, linux_rhsm_release, linux_rhsm_repository, linux_rpm_key, linux_runit, linux_script, linux_seboolean, linux_sefcontext, linux_selinux, linux_selinux_permissive, linux_selogin, linux_service, linux_shell, linux_shutdown, linux_slurp, linux_snap, linux_snap_alias, linux_ssh_config, linux_sudoers, linux_synchronize, linux_sysctl, linux_syslogger, linux_systemd, linux_systemd_service, linux_sysvinit, linux_tempfile, linux_template, linux_timezone, linux_ufw, linux_unarchive, linux_uri, linux_user, linux_wait_for, linux_wait_for_connection, linux_xml, linux_yum, linux_yum_repository, linux_yum_versionlock, linux_zfs, linux_zfs_delegate_admin, linux_zfs_facts, system]
```

## Provisioneren

Voer de volgende opdracht uit om de playbook te draaien:

```bash
ansible-playbook plays/examples/linux.yml -i hosts.ini
```

Zoals verwacht, moeten zowel de secrets als Linux rollen worden geprovisioneerd. De Linux rol zal het bestand `/tmp/system_install_task.txt` creëren.

<details>
  <summary><kbd>Toon mij</kbd></summary>

```bash
θ97° [:ansible-gis]└2 master(+15/-11)+* ± ansible-playbook $PLAY -i $INVENTORY $LIMIT | tee provision.log

PLAY [Linux rol] **************************************************************

TASK [Facten Verzamelen] *******************************************************
ok: [localhost]

TASK [c2platform.core.secrets : Stat secret dir] *******************************
ok: [localhost] => (item=/home/ostraaten/git/gitlab/c2/ansible-gis/secret_vars/development)
ok: [localhost] => (item=/runner/project/secret_vars/development)

TASK [c2platform.core.secrets : Inclusief geheimen] ****************************
ok: [localhost] => (item=/home/ostraaten/git/gitlab/c2/ansible-gis/secret_vars/development)

TASK [c2platform.core.linux : Inclusief linux_resources] ***********************
included: /home/ostraaten/git/gitlab/c2/ansible-dev-collections/ansible_collections/c2platform/core/roles/linux/tasks/copy.yml for localhost => (item= system_install_task.txt)

TASK [c2platform.core.linux : Kopieer bestanden naar externe locaties] *********
ok: [localhost] => (item=/tmp/system_install_task.txt)

PLAY RECAP *********************************************************************
localhost                  : ok=5    changed=0    unreachable=0    failed=0    skipped=49   rescued=0    ignored=0
```

</details>

## Verificatie

In dit zeer eenvoudige voorbeeld kun je verifiëren dat je de play kunt uitvoeren
met `--tags linux_copy`, `--tags system`, `--tags install` en `--tags linux`. In
elk geval wordt de **Inclusief geheimen** taak van de Ansible Secrets / Vault
rol `c2platform.core.secrets` uitgevoerd.

## Review

1. Als je kijkt naar het bestand `tasks/main.yml` in de
  {{< ansible-role-link role_name="c2platform.core.secrets" >}}
  rol, merk dan op dat de taken zijn getagd met de speciale Ansible tag
  `always`.

## Aanvullende Informatie

* {{< rellink path="/docs/howto/rws/arcgis/fme_flow/tags" desc=true >}}
* {{< external-link url="https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_tags.html" text="Tags — Ansible Community Documentation" htmlproofer_ignore="false" >}}