---
categories: ["Richtlijn"]
tags: [tags]
title: "Ansible Playbooks Verbeteren met Tags"
linkTitle: "Tags in Ansible"
weight: 4
description: >
  Implementeer een gestructureerd taggingsysteem in Ansible om de flexibiliteit,
  onderhoudbaarheid en herbruikbaarheid van taken te verbeteren, waardoor de
  ontwikkelings- en operationele efficiëntie worden geoptimaliseerd.
---

## Probleem

Het beheren van grote Ansible playbooks kan omslachtig en tijdrovend zijn wanneer
het gehele playbook moet worden uitgevoerd voor ontwikkeling of reguliere operaties.
Deze inefficiëntie is vooral merkbaar tijdens de ontwikkelingsfase, waar
snelle tests van specifieke playbook onderdelen noodzakelijk zijn, of in
productiesituaties waar operaties kunnen vragen om variërende uitvoeringsfrequenties.

## Oplossing

Verhoog de flexibiliteit en beheerbaarheid van je playbooks door een
gestructureerd en betekenisvol taggingschema voor Ansible-rollen en -taken te
implementeren. Door een goed gedefinieerde taggingsaanpak te volgen, kun je
efficiënt omgaan met de gebruikelijke installatie- en configuratiestadia
die relevant zijn voor implementatieprocessen.

### Taggingschema

Overweeg de volgende taggingsstrategie voor de timing van taakuitvoer en operationele niveaus:

| Tag Categorie              | Tag          | Beschrijving                                                                                                                                                                             |
|----------------------------|--------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `always` of `never`        | `always`     | Voor taken die consequent moeten worden uitgevoerd, cruciaal voor beveiliging, naleving, of als vereisten (bijv. het ontsleutelen van kluisitems of fundamentele eerste stappen).          |
|                            | `never`      | Voor taken die niet mogen draaien tenzij expliciet gespecificeerd met `--tags never`.                                                                                                     |
| `install` of `config`      | `install`    | Taken die zelden worden uitgevoerd, voornamelijk tijdens de initiële systeemopstelling of applicatie-installatie.                                                                         |
|                            | `config`     | Taken die vaak middelen wijzigen voor updates of optimalisaties.                                                                                                                         |
| `system` of `application`  | `system`     | Taken gericht op het besturingssysteem of systeemniveau configuraties.                                                                                                                   |
|                            | `application`| Taken die betrekking hebben op applicatieniveau configuraties, met directe impact op applicaties op het systeem.                                                                         |
| Configuratie Aanpak        | `config_api` | Taken geconfigureerd via de API van het product.                                                                                                                                         |
| Rol Identificatie          | Rolnaam      | Taken, met uitzondering van `always` of `never` taken[^1], in een rol zijn getagd met de rolnaam, zoals `linux` voor taken in de `c2platform.core.linux` rol.                            |

Aanvullende specifieke module-tags of aangepaste tags kunnen naar behoefte worden toegevoegd.

De voordelen van een duidelijk taggingschema omvatten:

- **Flexibiliteit**: Naadloze uitvoering van specifieke segmenten van
  implementatie- of onderhoudsprocessen.
- **Onderhoudbaarheid**: Vereenvoudig het begrip en onderhoud van playbooks met
  duidelijke tags.
- **Herbruikbaarheid**: Pas rollen aan voor verschillende operationele vereisten
  zonder de rol zelf te wijzigen.

{{< alert type="warning" title="Belangrijke Opmerking Over Levenscyclusbeheer (LCM) Operaties" >}}

Taken gerelateerd aan Levenscyclusbeheer (LCM) operaties zoals upgrades,
back-ups, herstel, downgrades en rollbacks zijn kritiek en potentieel
ontwrichtend. Om onbedoelde opname in bredere taakuitvoeringen te voorkomen
wanneer deze taken uitsluitend met tags worden beheerd:

- Gebruik specifieke configuratievariabelen (met veilige standaardwaarden,
  bijvoorbeeld false) om deze taken te activeren.
- Documenteer veilige uitvoeringsprocedures en bevestig de intentie van de gebruiker.

{{< /alert >}}

## Voorbeeld en Implementatie

Voor implementatievoorbeelden en verdere begeleiding, raadpleeg het
Linux-taggingvoorbeeld
{{< rellink path="/docs/guidelines/coding/tags/linux" desc=false >}}
of voor een uitgebreider voorbeeld zie
{{< rellink path="/docs/howto/rws/arcgis/fme_flow/tags" desc=false >}}.
Voor aanvullende referentie en informatie over Ansible-tags:
{{< external-link url="https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_tags.html" text="Tags — Ansible Community Documentatie" htmlproofer_ignore="false" >}}

## Voetnoten

[^1]: Vermijd het taggen van taken met de `always` tag met de Ansible rolnaam,
    aangezien dit de waarde van de rolnaamtag sterk zou verminderen. In
    dergelijke gevallen zou het gebruik van de rolnaam met `--skip-tags` per
    ongeluk de `always` tags overslaan.