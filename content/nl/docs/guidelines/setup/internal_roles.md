---
categories: [Richtlijn]
tags: [ansible, interne rollen, inventory-project]
title: "Ansible Richtlijn: Gebruik van Interne Rollen"
linkTitle: Gebruik van Interne Rollen
weight: 10
description: >
    Deze richtlijn is op maat gemaakt voor teams die relatief nieuw zijn met Ansible en die ervaring willen opdoen door gebruik te maken van interne rollen.
---

## Inleiding

Deze richtlijn is op maat gemaakt voor teams die relatief nieuw zijn met Ansible
en die ervaring willen opdoen door gebruik te maken van interne rollen. Interne
rollen bestaan binnen het inventory-project naast playbooks en group_vars, wat
een eenvoudige manier biedt om je Ansible-code te organiseren en beheren zonder
de behoefte om Ansible-collecties te onderhouden.

## Voordelen van het Gebruik van Interne Rollen

1. **Vereenvoudigde Structuur**: Interne rollen bevinden zich in dezelfde
   repository als je playbooks en group_vars, wat zorgt voor een samenhangende
   en eenvoudige mapstructuur die gemakkelijk te navigeren is.

2. **Snelle Iteraties**: Zonder de overhead van releasemanagement die gepaard
   gaat met Ansible-collecties, kunnen teams zich richten op het snel itereren
   en verbeteren van hun rollen op basis van directe feedback.

3. **Enkele Repository**: Door al je playbooks, rollen en configuraties in één
   repository te hebben, wordt versiebeheer en samenwerking vereenvoudigd,
   waardoor het gemakkelijker is voor teams om hun werk te coördineren en te
   delen.

4. **Incrementeel Leren**: Teams kunnen stapsgewijs hun kennis van Ansible
   vergroten door kleine en beheersbare wijzigingen aan te brengen, zonder te
   hoeven voldoen aan de hogere eisen die gelden voor herbruikbare collecties.

## Mapstructuur

Hier is een aanbevolen mapstructuur voor een inventory-project dat interne
rollen gebruikt:

```
project-root/
├── group_vars/
│   ├── all.yml
│   └── webservers.yml
├── host_vars/
│   └── web01.yml
├── inventory/
├── playbooks/
│   ├── site.yml
│   └── webserver.yml
└── roles/
    ├── internal/                     # Map voor interne rollen
    │   ├── role1/
    │   ├── role2/
    │   └── ...
    └── external/                     # Map voor externe rollen met lokale aanpassingen/maatwerkrollen
        ├── community_role1/          # Voorbeeld van een externe rol met lokale verbeteringen
        │   ├── tasks/
        │   ├── templates/
        │   ├── files/
        │   ├── vars/
        │   ├── defaults/
        │   └── handlers/
        └── community_role2/          # Nog een externe rol
            ├── tasks/
            └── ...

```

### Uitleg van de Mapstructuur

- **`group_vars/` en `host_vars/`**: Deze mappen bevatten variabele bestanden
  voor groepen hosts en individuele hosts.

- **`inventory/`**: Dit is het inventory-bestand of -map waarin je de hosts en
  groepen hosts definieert die Ansible zal beheren.

- **`playbooks/`**: Deze map bevat je Ansible-playbooks, die de orkestratie van
  taken over je hosts definiëren.

- **`roles/`**: De mappen met rollen bevatten alle rollen die beschikbaar zijn
  voor je playbooks.

  - **`internal/`**: Deze submap is specifiek voor interne rollen. Deze rollen
    zijn bedoeld voor gebruik binnen dit project en zijn niet bedoeld voor
    extern delen of hergebruik. Elke interne rol bevat standaard submappen zoals
    `tasks`, `templates`, `files`, `vars`, `defaults`, en `handlers`.

  - **`roles/external/`**: Deze map is gewijd aan externe rollen, afkomstig van
    bijvoorbeeld Ansible Galaxy of andere repositories. In plaats van deze
    rollen direct te gebruiken, biedt deze map de mogelijkheid voor een
    gecontroleerde, lokale aanpassingsprocedure waarbij rollen kunnen worden
    gepatcht of uitgebreid naar je specifieke behoeften.

    - **Doel**:
      - **Lokale Aanpassingen**: Als een externe rol problemen heeft of bepaalde
        functionaliteit mist, kunnen lokale kopieën worden aangepast om aan deze
        behoeften te voldoen terwijl je de rol blijft gebruiken.
      - **Aangepaste Verbeteringen**: Voeg functies toe of pas het gedrag van de
        rol aan om beter aan je infrastructuurvereisten te voldoen.
      - **Naleving van Beleid**: Voldoe aan bedrijfsbeleid dat het directe
        gebruik van via internet verkregen bronnen kan beperken door een lokale
        versie te onderhouden die is beoordeeld of goedgekeurd.

    - **Versiebeheer**: Door deze rollen in versiebeheer te houden, kun je je
      aanpassingen volgen, waardoor het gemakkelijker wordt om upstream
      wijzigingen te integreren zodra deze beschikbaar zijn.

    - **Parallel Bijdrageproces**: Terwijl je lokaal wijzigingen of
      verbeteringen maakt, kun je ook deelnemen aan een parallel proces van het
      indienen van bugrapporten, functieverzoeken of pull requests naar de
      oorspronkelijke rolbeheerders. Dit helpt mogelijk de upstream rol te
      verbeteren terwijl je rol in zijn aangepaste vorm blijft gebruiken.

## Praktijken voor Interne Rollen

1. **Lokale Ontwikkeling**: Omdat alles zich in één repository bevindt, kun je
   je wijzigingen eenvoudig lokaal testen voordat je ze doorvoert. Gebruik
   `ansible-playbook`-commando's om taken te verifiëren zonder verpakking of
   distributie te hoeven afhandelen.

2. **Consistente Naamgeving**: Gebruik duidelijke en beschrijvende namen voor je
   rollen en playbooks om het je team gemakkelijker te maken hun doel en gebruik
   te begrijpen.

3. **Incrementele Wijzigingen**: Begin met eenvoudige taken en voeg geleidelijk
   complexiteit toe naarmate het team meer vertrouwen krijgt in Ansible.
   Herstructureer en verbeter constant je rollen.

4. **Documentatie en Opmerkingen**: Hoewel deze rollen intern zijn, is het
   nuttig om opmerkingen en korte documentatie toe te voegen binnen je taken en
   rollen om hun functionaliteit te beschrijven.

5. **Focus op Rolfunctionaliteit**: Aangezien deze rollen niet zijn ontworpen
   voor hergebruik in meerdere projecten, moet je je richten op de specifieke
   behoeften van je project. Dit kan betekenen dat rollen meer op maat zijn en
   minder generiek in vergelijking met die in een collectie.

6. **Peer Reviews**: Moedig teamleden aan om elkaar's Ansible-code te
   beoordelen. Dit bevordert leren door gedeelde inzichten en suggesties voor
   verbeteringen.

7. **Versiebeheerpraktijken**: Gebruik branches effectief voor nieuwe functies
   of experimenten, en zorg ervoor dat hoofdlijnen stabiel zijn. Gebruik pull
   requests en code reviews als onderdeel van een samenwerkingsworkflow.

## Overstappen naar Ansible Collecties

Naarmate je team meer vertrouwd raakt met Ansible en de kwaliteit van je rollen
verbetert, overweeg dan de volgende stappen om indien nodig over te stappen naar
collecties:

- **Standaardisatie van Rolontwikkeling**: Begin met het adopteren van best
  practices voor rolontwikkeling en documentatie, meer in lijn met
  gemeenschapsstandaarden.
- **Scheiding van Belangen**: Maak onderscheid tussen rollen die herbruikbaar
  kunnen zijn en rollen die inherent project-specifiek zijn.
- **Vestig Releaseprocessen**: Introduceer versiebeheer en releaseprocessen
  naarmate rollen volwassen worden en verder worden gebruikt dan hun initiële
  scope.

## Conclusie

Voor teams die net beginnen met Ansible, biedt het gebruik van interne rollen
binnen een enkele repository een beheersbare en effectieve manier om
infrastructuurautomatisering te ontwikkelen en te onderhouden. Deze aanpak maakt
snelle iteratie en leren mogelijk, en legt een solide basis voor toekomstige
groei en potentiële uitbreiding naar Ansible-collecties.