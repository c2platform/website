---
categories: ["Richtlijn"]
tags: [klonen, git]
title: "Kloon script"
linkTitle: "Kloon script"
weight: 10
description: >
  Automatiseer de installatie van de ontwikkelomgeving met meerdere Git repositories.

---

 > Gebruik een kloonscript om de installatie van de ontwikkelomgeving te
 > automatiseren met meerdere Git repositories in Ansible-projecten. Dit omvat
 > het standaardiseren van de locatie van repositories, het configureren van
 > Git-instellingen en het afhandelen van pre-commit hooks.

---

## Probleem

Het opzetten van een ontwikkelomgeving met meerdere Git repositories kan
tijdrovend en foutgevoelig zijn. Het vereist het aanmaken van repositories op
een specifieke locatie, het configureren van Git-instellingen, en het uitvoeren
van andere repetitieve taken. Bovendien kan het ervoor zorgen dat alle
ontwikkelaars repositories op dezelfde gestructureerde locatie hebben, wat de
samenwerking verbetert en het makkelijker maakt om samen te werken.

## Oplossing

Creëer een kloonscript dat het proces automatiseert voor het opzetten van de
ontwikkelomgeving voor je Ansible playbook/configuratieprojecten. Het script
moet de volgende taken uitvoeren:

1. Stel de nodige omgevingsvariabelen in:
   * `REPO_DIR`: De directory waar de Git-repositories aangemaakt worden.
   * `REPO_FLDR`: De mapnaam voor de hoofdrepository.
2. Controleer of de omgevingsvariabelen `GIT_USER` en `GIT_MAIL` zijn ingesteld.
   Zo niet, toon een foutmelding en stop het script.
3. Maak de `REPO_DIR` directory aan als deze nog niet bestaat.
4. Definieer een functie, `clone_repo`, om een repository te klonen,
   Git-instellingen te configureren en aanvullende taken uit te voeren:
    * Ga naar de `REPO_DIR/REPO_FLDR` directory.
    * Controleer of de repositorymap niet bestaat.
    * Als de map niet bestaat, kloon de repository, haal de opgegeven branch op,
      check de branch uit, stel de upstream branch in en trek de laatste
      wijzigingen op.
    * Configureer de Git-gebruikersnaam en e-mail met behulp van de `GIT_USER`
      en `GIT_MAIL` omgevingsvariabelen.
    * Als linting ingeschakeld is (`C2_LINTERS` ingesteld op "Y"), kopieer het
      pre-commit hook script naar de `.git/hooks` directory van de repository.
    * Als linting is uitgeschakeld (`C2_LINTERS` ingesteld op "N"), verwijder
      dan het pre-commit hook script uit de `.git/hooks` directory.
5. Roep de `clone_repo` functie aan voor elke repository die je wilt klonen,
   waarbij je de repository-URL, het bestemmingspad van de map en de naam van de
   branch aangeeft.

## Voorbeelden en implementatie

Voor voorbeelden van de implementatie van het kloonscript, kun je verwijzen naar
de `clone.sh` bestanden in de projectrepositories:

* [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})
* [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

Deze voorbeelden laten zien hoe je het kloonscript kunt gebruiken om de
installatie van ontwikkelomgevingen met meerdere Git repositories voor
Ansible-projecten te automatiseren.

Het project [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) kan bijvoorbeeld worden opgezet met
het commando:

```
curl -L https://gitlab.com/c2platform/ansible/-/raw/master/clone.sh | bash
```

Om linters in te stellen, kun je de volgende commando's uitvoeren:

```
export C2_LINTERS=Y
curl -L https://gitlab.com/c2platform/ansible/-/raw/master/clone.sh | bash
```

Om pre-commit hooks te verwijderen, gebruik `export C2_LINTERS=N`.