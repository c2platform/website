---
categories: ["Richtlijn"]
tags: [group_vars, secrets, vault, secret_vars, app, awx]
title: "Geheimen Beheren met Ansible Vault in AAP / AWX"
linkTitle: "Ansible Vault en AAP / AWX"
weight: 3
description: >
  Leer hoe u effectief geheimen beheert met behulp van Ansible Vault in Ansible-projecten, vooral in de context van het Red Hat Automation Platform (AAP) en AWX.
---

---
> Beheer geheimen met Ansible Vault in een aangepaste map `secret_vars`. Creëer
> een generieke Ansible-rol om geheimen uit deze directory te lezen.

---

## Probleem

[AAP]({{< relref path="/docs/concepts/ansible/aap" >}}) heeft geen ingebouwde
ondersteuning voor
{{< external-link url="https://docs.ansible.com/ansible/latest/user_guide/vault.html" text="Ansible Vault" htmlproofer_ignore="false" >}},
wat uitdagingen met zich meebrengt bij het integreren van met Vault versleutelde
bestanden in de `group_vars`-map. Deze beperking beïnvloedt
inventarisatieprojecten die Git als basis gebruiken voor AAP /
AWX-implementaties, wat leidt tot updateproblemen door het onvermogen om een
Ansible Vault geheim voor zulke projecten te configureren. Het is essentieel om
dit probleem aan te pakken en een oplossing te vinden.

{{< alert title="Let op:" >}}
Het implementeren van ingebouwde ondersteuning voor Ansible Vault in AAP / AWX
is complex omdat het geheimen zou blootstellen aan AAP / AWX-gebruikers,
waardoor hun vertrouwelijkheid wordt aangetast.
{{< /alert >}}

## Context

Het beheren van geheimen is een kritieke taak in Ansible-projecten, en Ansible
Vault biedt een standaard en eenvoudige oplossing. Het effectief gebruiken van
Ansible Vault binnen de context van AAP / AWX vereist echter specifieke
instellingen en overwegingen.

## Oplossing

Volg deze stappen om de bovengenoemde uitdagingen te overwinnen:

1. Maak een aangepaste map genaamd `secret_vars` aan om geheimen in op te slaan
   die met `include_vars` kunnen worden opgenomen. Deze map zal dienen als
   alternatief voor het opslaan van geheimen in de standaard `group_vars`-map.
2. Ontwikkel een generieke en flexibele Ansible-rol die de `secret_vars`-map kan
   gebruiken. Deze rol moet compatibel zijn met zowel AAP als de Ansible CLI.

Naast de bovenstaande oplossing wordt aanbevolen om Ansible Vault te gebruiken
tijdens de ontwikkeling, aangezien het beheren van geheimen essentieel is voor
inventarisatieprojecten. De volgende richtlijnen worden voorgesteld:
1. Stel binnen Ansible-rollen het standaard wachtwoord in op `supersecure`.
2. Gebruik `secret` als het standaardwachtwoord voor wachtwoorden in de map
   `secrets_vars` / vault.
3. Als technische wachtwoordvereisten het gebruik van `secret` verhinderen,
   gebruik dan `supersecret`. Als `supersecret` nog steeds niet voldoende sterk
   is, kies dan voor een geldig aangepast wachtwoord.

Door bovenstaande aanpak te implementeren, krijg je de volgende voordelen:
1. Ontwikkelingswachtwoorden zijn eenvoudig, wat tijd bespaart tijdens het
   ontwikkelings- / testproces.
2. Geheimen worden gedocumenteerd bij identificatie, dankzij het gebruik van
   Ansible Vault, zelfs tijdens de ontwikkeling.
3. Eventuele gemiste wachtwoorden of niet-gedocumenteerde geheimen worden
   duidelijk tijdens de ontwikkeling, aangezien het geconfigureerde wachtwoord
   `supersecret` zal zijn in plaats van het verwachte `secret`.
4. De oplossing is compatibel met Red Hat Automation Platform (AAP), AWX en de
   Ansible CLI.

## Voorbeelden en implementatie

Raadpleeg de `secrets`-rol binnen de [`c2platform.core`](<{{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}>) collectie voor een
implementatievoorbeeld. De rol maakt gebruik van de `common_secrets_dirs`-lijst,
die kan worden geconfigureerd met meerdere locaties voor de `secret_vars`-map.
Het volgende voorbeeld werkt zowel voor de Ansible CLI als AAP / AWX. Bij
gebruik van AAP plaatst AAP de vault op de specifieke locatie
`/runner/project/secret_vars/development`.

```yaml
common_secrets_dirs:
  - "{{ inventory_dir }}/secret_vars/development"
  - "/runner/project/secret_vars/development"  # awx / aap
```

Gebruik de volgende opdracht om de geheimen/vault te bekijken of te bewerken:

```bash
EDITOR=nano ANSIBLE_CONFIG=ansible-dev.cfg ansible-vault edit secret_vars/development/main.yml --vault-password-file vpass
```

{{< alert title="Let op:" >}}Het `vpass`-bestand wordt automatisch aangemaakt
door [Vagrant]({{< relref path="/docs/concepts/dev/vagrant" >}}). Zie het
Vagrantfile in het [`c2platform/ansible`](<{{< relref "/docs/gitlab/c2platform/ansible" >}}>) inventarisatieproject voor een
voorbeeld.{{< /alert >}}

Maak voor het gemak een alias aan om het beheer van de vaults te vereenvoudigen
en ervoor te zorgen dat er geen geheimen over het hoofd worden gezien.

```bash
alias c2d-secrets='EDITOR=nano ANSIBLE_CONFIG=ansible-dev.cfg ansible-vault edit secret_vars/development/main.yml --vault-password-file vpass'
```