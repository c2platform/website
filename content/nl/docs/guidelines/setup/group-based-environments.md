---
categories: ["Richtlijn"]
tags: [group_vars]
title: "Omgevingen op basis van Groepen"
linkTitle: "Omgevingen op basis van Groepen"
weight: 2
description: >
  Organiseer je Ansible-inventaris en variabelen voor verschillende omgevingen.
---

## Probleem

Bij het beheren van Ansible-projecten voor verschillende omgevingen zoals
ontwikkeling, test, staging, en productie, is het belangrijk om een
gestructureerde aanpak te hebben om inventaris en variabelen te organiseren.

Ansible stelt je in staat om groepen te definiëren in de inventaris, die kunnen
worden gebruikt om hosts met vergelijkbare rollen of attributen te organiseren.
Groepsvariabelen kunnen ook worden gebruikt om omgevingsspecifieke variabelen te
definiëren die worden toegepast op hosts in die groepen.

## Oplossing

Gebruik een op groepen gebaseerde aanpak om je Ansible-inventaris en variabelen
voor verschillende omgevingen te organiseren. Volg deze richtlijnen:

1. Definieer groepen in je inventarisbestand op basis van omgevingen, zoals
   "dev", "test", "staging" en "prod".
2. Wijs hosts toe aan de juiste groepen op basis van hun rollen of attributen,
   bijvoorbeeld ontwikkelingshosts aan de "dev" groep, productieservers aan de
   "prod" groep.
3. Definieer groepsvariabelen in afzonderlijke variabelenbestanden voor elke
   omgeving, bijvoorbeeld "group_vars/dev.yml", "group_vars/test.yml",
   enzovoort.
4. Definieer omgevingsspecifieke variabelen in de respectievelijke
   groepsvariabelenbestanden, zoals verbindingsgegevens,
   applicatie-instellingen, enzovoort.
5. Gebruik de groepsvariabelen in je playbooks en rollen om hosts te
   configureren op basis van hun omgeving.

Deze aanpak biedt de volgende voordelen:

1. Biedt een duidelijke en georganiseerde manier om inventaris en variabelen
   voor verschillende omgevingen te beheren.
2. Hiermee kun je omgevingsspecifieke configuraties definiëren in
   groepsvariabelen, wat het eenvoudig maakt om instellingen voor elke omgeving
   aan te passen.
3. Vereenvoudigt de ontwikkeling van playbooks en rollen, aangezien je
   groepsvariabelen kunt gebruiken om omgevingsspecifieke details in abstractie
   te trekken uit playbooks en rollen.

## Voorbeelden en implementatie

Hier is een voorbeeld van hoe je de op groepen gebaseerde aanpak kunt
implementeren:

Definieer inventaristgroepen in je inventarisbestand, zoals "dev", "test",
"staging" en "prod":

```ini
[dev]
dev-host-1 ansible_host=192.168.1.10
dev-host-2 ansible_host=192.168.1.11

[test]
test-host-1 ansible_host=192.168.2.10
test-host-2 ansible_host=192.168.2.11

[staging]
staging-host-1 ansible_host=192.168.3.10
staging-host-2 ansible_host=192.168.3.11

[prod]
prod-host-1 ansible_host=192.168.4.10
prod-host-2 ansible_host=192.168.4.11
```

Definieer groepsvariabelen in afzonderlijke variabelenbestanden voor elke
omgeving:

```
group_vars/
  dev.yml
  test.yml
  staging.yml
  prod.yml
```

Definieer omgevingsspecifieke variabelen in de respectievelijke
groepsvariabelenbestanden, bijvoorbeeld in "group_vars/dev.yml":

```yaml
# group_vars/dev.yml
db_host: dev-db.example.com
app_debug: true
```