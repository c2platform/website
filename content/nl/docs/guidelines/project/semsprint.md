---
categories: ["Richtlijn"]
tags: [sprint, SemVer, SemSprint]
title: "Semantische Sprints (SemSprint)"
linkTitle: "SemSprint"
weight: 4
description: >
  Consistente en betekenisvolle naamgeving van sprints, vergelijkbaar met SemVer.
---

> SemSprint is een standaard voor het op een consistente en betekenisvolle
> manier benoemen van sprints, vergelijkbaar met
> {{< external-link url="https://semver.org/" text="Semantische Versiebeheer (SemVer)" htmlproofer_ignore="false" >}}.
> Het stelt projecten en organisaties in staat om sprintnamen toe te wijzen met
> een gedeeld begrip tussen verschillende teams en contexten. Elke sprintnaam
> bestaat uit het jaar en een volgnummer, dat de positie van de sprint binnen
> dat jaar weergeeft. Bijvoorbeeld, `2023-12` duidt op de twaalfde sprint van
> het jaar 2023.

## Probleem

In veel projecten en organisaties ontbreekt consistentie in de naamgeving van
sprints, en deze schieten tekort in het overbrengen van betekenisvolle
informatie. Deze inconsistentie kan leiden tot verwarring, miscommunicatie en
problemen bij het volgen van voortgang tussen teams of projecten. Het wordt
moeilijk om de chronologische volgorde van sprints te bepalen of af te stemmen
op hun duur.

## Context

Sprints zijn tijdgebonden iteraties die vaak worden gebruikt in agile
projectmanagementmethodologieën. Ze bieden een specifieke periode voor
ontwikkeling, testen en levering van specifieke functies of increments. Echter,
zonder een gestandaardiseerde naamgevingsconventie wordt het uitdagend om
sprints nauwkeurig te identificeren en te verwijzen bij verschillende teams,
projecten of organisaties.

## Oplossing

SemSprint introduceert een eenvoudige en intuïtieve standaard voor het benoemen
van sprints, waardoor consistentie en betekenis worden verzekerd in projecten en
organisaties. De belangrijkste aspecten van SemSprint zijn als volgt:

1. **Sprintduur**: Elke sprint heeft een vaste duur van twee weken. Deze
   consistente tijdsperiode maakt betere planning en synchronisatie tussen teams
   mogelijk.
2. **Sprintnaamformaat**: De sprintnaam bestaat uit het jaar en een volgnummer
   gescheiden door een koppelteken. Bijvoorbeeld, `2023-12` vertegenwoordigt de
   twaalfde sprint van het jaar 2023.
3. **Afleiden van de week**: Om de week van de sprint te bepalen, vermenigvuldig
   je het volgnummer met twee. Bijvoorbeeld, in `2023-12` is het volgnummer
   `12`, dus door dit te vermenigvuldigen met twee krijg je 24. Het weekbereik
   kan uit deze berekening worden afgeleid, wat aangeeft dat de sprint begint in
   week 23 (24 - 1) en eindigt in week 24.

Door de SemSprint-standaard te volgen, kunnen teams gemakkelijk sprints
begrijpen en verwijzen op basis van hun namen, wat betere samenwerking en
communicatie bevordert in projecten en organisaties.

## Voorbeeld en implementatie

Hieronder volgen een paar voorbeelden van SemSprint-namen en hun bijbehorende
details:

Sprintnaam: `2023-01`

* Jaar: 2023
* Volgnummer: 1
* Sprintduur: Twee weken
* Start week: 1 (afgeleid van 1 * 2 - 1)
* Eindweek: 2 (afgeleid van 1 * 2)

Sprintnaam: `2023-12`

* Jaar: 2023
* Volgnummer: 12
* Sprintduur: Twee weken
* Start week: 23 (afgeleid van 12 * 2 - 1)
* Eindweek: 24 (afgeleid van 12 * 2)
