---

categories: ["Handleiding"]
tags: [ansible, vagrant]
title: "Aan de Slag"
linkTitle: "Aan de Slag"
weight: 1
description: >
  Leer hoe je een lokale ontwikkelomgeving kunt opzetten en je eerste virtuele
  machine kunt maken met behulp van Vagrant en Ansible.

---

De [ontwikkelomgeving]({{< relref "/docs/concepts/dev" >}} "Begrip:
Ontwikkelomgeving") vormt een fundamenteel element binnen de C2 Platform-aanpak.
Door gebruik te maken van de kracht van Vagrant, LXD en VirtualBox (ideaal voor
op MS Windows gebaseerde VM's), kun je realistische infrastructuren lokaal
simuleren, vergelijkbaar met de visie van Jeff Geerling in zijn boek, [Ansible
for DevOps]({{< relref path="/docs/concepts/dev/vagrant/ansible-for-devops" >}}).

Hoewel de initiële opzet van deze ontwikkelomgeving complex kan lijken, opent
het overwinnen van deze eerste uitdaging een veelzijdig en krachtig hulpmiddel
voor het ontwikkelen en testen van automatiseringsoplossingen.

Bijvoorbeeld, het uitvoeren van een enkele opdracht zo eenvoudig als deze:

```bash
vagrant up c2d-awx1
```

zal een volledig functioneel Kubernetes-cluster op je lokale werkstation creëren
en vervolgens de [Ansible Automation Controller (AWX)]({{< relref path="/docs/concepts/ansible/aap" >}}) erop implementeren en configureren.

Het doel is echter niet alleen eenvoud; het gaat er ook om je de middelen te
bieden om automatiseringsprojecten te begrijpen, implementeren en optimaliseren.
De documentatie speelt een cruciale rol bij het bereiken van dit doel. Neem
bijvoorbeeld de handleiding [Setup the Automation Controller (AWX) met
Ansible]({{< relref path="/docs/howto/awx/awx" >}}), die verder gaat dan het
documenteren van eenvoudige stappen (zoals één opdracht, `vagrant up c2d-awx1`,
het werk doet) om ervoor te zorgen dat je begrijpt hoe deze enkele opdracht het
gewenste resultaat bereikt.

Uitgebreide instructies zijn te vinden in het Engelstalige gedeelte van onze
website, die je begeleiden bij het opzetten, configureren en gebruiken van de
ontwikkelomgeving:

<div class="mx-auto">
	<a class="btn btn-lg btn-primary mr-3 mb-4" href="{{< relref path="/docs/howto/dev-environment/setup" >}}">
		Setup a Development Environment on Ubuntu 22<i class="fas fa-arrow-alt-circle-right ml-2"></i>
	</a>
</div>

{{< alert title="Opmerking:" >}} Hoewel we ervan uitgaan dat je gebruik maakt
van een [high-end ontwikkelaarslaptop]({{< relref "/docs/concepts/dev/laptop" >}} "Begrip: High-end ontwikkelaarslaptop") met Ubuntu 22.04, is het ook
volledig mogelijk om het proces te starten binnen een VirtualBox VM met Ubuntu
22. De standaardconfiguratie van het referentieproject neigt naar [Linux
Containers (LXD)]({{< relref "/docs/concepts/dev/lxd" >}} "Begrip: Linux
Containers (LXD)"), en deze werken naadloos binnen een VirtualBox VM.
{{< /alert >}}