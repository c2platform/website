---
categories: ["Diagram", "PlantUML", "C4", "gis"]
title: "Het engineeringsproces van het GIS-platform met Ansible"
linkTitle: "Engineering van het GIS-platform"
draft: false
weight: 3
description: >
  Dit diagram visualiseert de engineering van Ansible-inhoud met Ansible. Gebruik de
  standaard C2 Platform aanpak, geheel uitgevoerd in het open-source domein,
  met lokale ontwikkeling en een open-source "referentie-implementatie" van het GIS-platform.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

```plantuml
@startuml rws-gis-engineering
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads
!include <office/Devices/workstation>
'!include <tupadr3/font-awesome-5/laptop>
!include <material/laptop>

AddContainerTag("rhel_desktop", $sprite="workstation", $legendText="virtueel-desktop")
AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

title [Container] GIS Platform Automatisering Engineering

'Person(rws_operator, "RWS Ansible Operator", "Voert LCM en TAM taken uit voor het GIS-platform met behulp van de Ansible Controller en Azure DevOps.")
Person(rws_engineer, "Ansible Engineer", "RWS GIS Platform engineer met Ansible kennis")
Person(c2_engineer, "C2 Ansible Engineer", "Open Source Bijdrager \nC2 Platform")

'System_Ext(azure_devops, "Azure DevOps\ndev.azure.com/Rijkswaterstaat", "Herbergt Ansible projecten voor het GIS-platform")
System_Ext(gitlab, "GitLab\ngitlab.com/c2platform/rws", "Herbergt alle open source RWS projecten voor GIS Platform")
System_Ext(vagrant_cloud, "Vagrant Cloud", "Herbergt alle C2 Platform VirtualBox en LXD afbeeldingen")
System_Ext(galaxy,"ANSIBLE_GALAXY_LABEL" , "ANSIBLE_GALAXY_DESC", $tags="Galaxy_System_Ext")

Boundary(c2, "C2 Platform Open Source / Lokale Ontwikkeling", $type="domain") {
  WithoutPropertyHeader()
  AddProperty("Omgeving", "DEV")
  Container(gis_dev, "GIS Platform", "LXD, VirtualBox" , "Biedt GEO-informatie en verwerkingsdiensten", $tags="$arcgis")
  note left : Open Source GIS Platform\n"Referentie-implementatie"
  Container(laptop, "Ontwikkellaptop", "Ubuntu 22, VS Code, VirtualBox, LXD", "Belangrijk en integraal onderdeel van de open C2 Platform aanpak", $tags="$laptop")
  'Container(git_local, "Git", "Git")
  Container(vagrant, "Vagrant + Ansible", "" , "VAGRANT_DESC")
  Boundary(git_local, "Git") {
    ContainerDb(ansible_collections, "GIS_COLLECTIES_LABEL", "GIS_COLLECTIES_TECH", "GIS_COLLECTIES_DESC")
    ContainerDb(ansible_ee, "GIS_EE_LABEL", "GIS_EE_TECH", "GIS_EE_DESC")
    ContainerDb(ansible_inventory, "GIS_INVENTARIS_LABEL", "GIS_INVENTARIS_TECH", "GIS_INVENTARIS_DESC_REF")
  }
}

Rel(rws_engineer,gis_dev,'Lokale ontwikkeling van een GIS-platform "referentie-implementatie"', "")
Rel_R(gis_dev,laptop,"Wat vereist een", "")
Rel_L(vagrant, vagrant_cloud, "Downloadt Windows-, RedHat- en Ubuntu-afbeeldingen van", "HTTPS")
Rel_L(ansible_ee,gitlab,"Bouw en release EE", "CI/CD Pijplijn")
Rel(laptop,vagrant,"Met twee sleutelcomponenten geïnstalleerd", "")
Rel(gis_dev, vagrant, "Gedefinieerd, gecreëerd en onderhouden met behulp van")
Rel(vagrant, ansible_collections, "Gebruikt rollen van verschillende Ansible Collecties","")
Rel_D(ansible_collections, ansible_ee, "Verpakt in","Docker Build")
Rel(vagrant, ansible_inventory, "Gebruikt Ansible Inventaris die de referentiespecificatie definieert","")
Rel_L(git_local, gitlab, "Push / pull wijzigingen", "Git, SSH of HTTPS")
Rel_D(gitlab, galaxy, "Publiceer / release collecties naar", "CI/CD Pijplijn")
Rel_R(rws_engineer, c2_engineer, "is een", "", $tags="optional")

LAYOUT_WITH_LEGEND()
@enduml
```

## Aanvullende Informatie

Voor extra inzichten en begeleiding:

* Het tweede sleutelaspect van de C2 Platform aanpak is de
  [Ontwikkelomgeving]({{< relref "/docs/concepts/dev" >}})
  die flexibiliteit en productiviteit biedt door middel van lokale ontwikkeling.
  Dit
  wordt mogelijk dankzij het eerste sleutelaspect: de
  [Open, tenzij](/docs/concepts/oss) aanpak.
* Ontdek de unieke Ansible projecttypes binnen de C2 Platform aanpak door
  te bezoeken
  [Ansible Projecten]({{< relref path="/docs/concepts/ansible/projects" >}}).
* Begrijp de essentiële verschillen tussen opereren (gebruiken) en engineering
  met Ansible
  [hier]({{< relref path="/docs/concepts/ansible/engineering-vs-configuring" >}}).