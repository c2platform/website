---
categories: ["Diagram", "Project"]
title: "RWS GIS Platform ( 2023 tot heden )"
linkTitle: "RWS"
weight: 2
description: >
  Rijkswaterstaat (RWS) GIS Platform
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}), [`c2platform.gis`]({{<
relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})