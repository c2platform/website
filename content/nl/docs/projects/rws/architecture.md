---
categories: ["Diagram", "Project"]
title: "RWS Architectuur"
linkTitle: "RWS Architectuur"
draft: true
weight: 1
description: >
  Rijkswaterstaat (RWS) GIS Platform Architectuurdiagram
---

```plantuml
@startuml system
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads
'!define C4_NO_STEREOTYPE
AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

title [Container] RWS GIS Platform

System_Boundary(gis, "GIS_LABEL") {
    Container(etl_server, "ETL Server", "FME", "TODO")
    Container(etl_desktop, "ETL Desktop", "FME", "TODO")
    Container(age_server, "GIS Server", "ArcGIS Server, ArcGIS Portal", "TODO")
}

Person(power_user, "Power User", "RWS gebruiker met\nexpertise in GIS-kennis")
'Person(self, "Self Service User", "RWS gebruiker met\nbasiskennis van GIS")
'Person(user, "Gebruiker", "RWS gebruiker met\nbeperkte GIS-kennis")

'Person_Ext(external_user, "Externe Gebruiker", "Externe gebruikers (aannemers) met GIS-kennis")
'Person_Ext(citizen, "Burger", "Gebruikers zonder\nGIS-kennis")


'System(gis, "GIS_LABEL", "GIS_DESC")
'System(gis, "GIS_LABEL", "Biedt GEO-informatie en verwerkingsdiensten")
'
'System_Ext(iam, "IAM", "Identiteit / Toegangsbeheer", "")
'System_Ext(geo_ruimte, "Geo Data", "Geo data als diensten", "fddf")
SystemDb_Ext(file_service, "Bestandsdienst", "FME Repository Opslag en\nGeo data bestanden", "fddf")
'SystemQueue_Ext(esb, "ESB", "Externe gegevensbronnen")
System_Ext(apps, "RWS Applicaties", "Verschillende RWS applicaties", "")
System_Ext(ansible, "AAP", "Ansible\nAutomatiseringsplatform")

Rel(power_user, etl_desktop, "Gebruikt", "desktop,etl,gis")
Rel(etl_desktop, etl_server, "Gebruikt")
Rel(etl_server, file_service, "TODO")

'
'Rel(power_user, gis, "Zorgt voor datakwaliteit, met behulp van ETL-processen voor efficiënt datamanagement en verspreiding.", "desktop,https,rdp, etl")
'Rel(self, gis, "Samenwerken in het veld / offline werk met GeoWeb en eenvoudige kaarten", "browser,https")
'Rel(user, gis, "Samenwerken in het veld / offline werk met GeoWeb", "browser,https")
'Rel(citizen, gis, "Toegang tot kaarten via rws.nl of pdok.nl", "browser,https")
'Rel(external_user, gis, "Samenwerken in het veld / offline / projectwerk, gegevens verzamelen", "browser,'https")
'
'Rel(gis, geo_ruimte, "Gebruikt / verbruikt / transformeert data van", "https, wcs")
'Rel(gis, esb, "(Biedt) toegang tot gegevensbronnen", $tags="optional")
'Rel(gis, file_service, "Publiceer data voor intern\nen extern gebruik", "CIFS/SMB")
''Rel_R(geo_ruimte, file_service, "Opslag", "CIFS/SMB")
'Rel(gis, iam, "Gebruikt voor authenticatie / autorisatie")
'
Rel(apps, age_server, "Toegang tot Geo-diensten, GeoWeb kaarten, ETL", "https,etl")
Rel(ansible, gis, "Automatiseert dienstverlening en beheer", "Infrastructure-As-Code")


LAYOUT_WITH_LEGEND()
@enduml
```