---
categories: ["Diagram", "PlantUML", "C4", "gis"]
title: Engineering met een "Pseudo" Ontwikkelomgeving
linkTitle: "Lichte Engineering"
draft: false
weight: 4
description: >
  Engineering van Ansible-collecties en -rollen in het RWS Domein/DC wordt beschouwd als
  "lichte engineering" vanwege het gebruik van een "pseudo" ontwikkelomgeving. Deze opzet
  kent specifieke uitdagingen zoals beperkte toegang tot resets.
---

De Ansible-ontwikkelomgeving binnen het RWS Domein/DC wordt erkend als een
"pseudo" omgeving om twee belangrijke redenen. Ten eerste zijn het uitvoeren
van resets en snapshots van VM's niet eenvoudig. Deze acties vereisen een formeel
verzoek aan het infrastructuurteam via een TopDesk-verzoek tot wijziging, wat
doorgaans minstens een dag in beslag neemt. Voor intensief Ansible-engineeringwerk is
de mogelijkheid om de omgeving meerdere keren per dag te resetten cruciaal.

Ten tweede wordt deze omgeving gedeeld onder verschillende engineers, waaronder
ook engineers die niet tot het GIS Platform-team behoren. Dit kan resulteren in
aanzienlijke periodes van onbeschikbaarheid, soms tot een week of langer.

```plantuml
@startuml rws-gis-engineering-light
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads
!include <office/Devices/workstation>
!include <material/laptop>

AddContainerTag("rhel_desktop", $sprite="workstation", $legendText="virtual-desktop")
AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

title [Container] GIS Platform Automatisering Lichte Engineering

Person_Ext(rws_infra, "RWS Infra Team", "Biedt infrastructuurdiensten aan RWS-projecten en systeemteams")
Person(rws_engineer_light, "RWS Ansible Engineer Light", "Vergelijkbaar met RWS Ansible Engineer maar gebruikt een pseudo-ontwikkelomgeving.")
System_Ext(azure_devops, "Azure DevOps\ndev.azure.com/Rijkswaterstaat", "Host Ansible projecten voor het GIS Platform")
System_Ext(gitlab, "GitLab\ngitlab.com/c2platform/rws", "Host alle open source RWS-projecten voor GIS Platform")
System_Ext(galaxy,"ANSIBLE_GALAXY_LABEL" , "ANSIBLE_GALAXY_DESC", $tags="Galaxy_System_Ext")

Boundary(aap_gis, "RWS DC", $type="domain") {
  Container(rhel_desktop, "Ansible Dev Desktop", "VDI, RHEL9, RDP, VS Code", $tags="rhel_desktop")
  Container(ansible, "Ansible", "" , "Ansible CLI")
  WithoutPropertyHeader()
  AddProperty("Environment", "LAB")
  Container(gis, "GIS Platform", "dc,vmware", "Biedt GEO-informatie en verwerkingsdiensten", $tags="$arcgis")
  Boundary(git_local, "Git") {
    ContainerDb(ansible_collections, "GIS_COLLECTIONS_LABEL", "GIS_COLLECTIONS_TECH", "GIS_COLLECTIONS_DESC")
    ContainerDb(ansible_inventory, "GIS_INVENTORY_LABEL", "GIS_INVENTORY_TECH", "GIS_INVENTORY_DESC")
  }
}

Rel_U(ansible, gis ,"Wijzigingen in Ansible-rollen en -configuratie implementeren en verifiëren", "")
Rel(rws_engineer_light,rhel_desktop, "Toegang via RDP-in-RDP-verbinding")
Rel(rhel_desktop, ansible, "Waarop geïnstalleerd", "")
Rel(rws_infra, gis, "VM's resetten, snapshot maken/herstellen", "VMWare")
Rel_L(rws_engineer_light, rws_infra, "Vraag reset / herstel van LAB VM's / snapshots", "TopDesk")
Rel(ansible, ansible_collections, "Gebruikt rollen van verschillende Ansible Collecties","")
Rel(ansible, ansible_inventory, "Gebruikt Ansible Inventory voor LAB-omgeving","")
Lay_R(ansible_inventory,ansible_collections)
Rel_L(git_local, azure_devops, "Push / pull wijzigingen", "Git, SSH of HTTPS")
Rel_R(ansible, galaxy, "Download\nAnsible content", "")
Rel_U(azure_devops, gitlab, "OSS Git repo gesynchroniseerd / gespiegeld naar", "Git,HTTPS", $tags="optional")

LAYOUT_WITH_LEGEND()
@enduml
```

## Aanvullende Informatie

Voor aanvullende inzichten en begeleiding:

* Ontdek de unieke Ansible projecttypes binnen de C2 Platform aanpak door te
  bezoeken [Ansible Projecten]({{< relref path="/docs/concepts/ansible/projects" >}}).
* RWS hanteert een ["open, tenzij"]({{< relref path="/docs/concepts/oss" >}})
  beleid, wat ertoe leidt dat de Execution Environment (EE) van het GIS Platform
  is opgenomen in het GitLab Open Source Programma. Dit biedt toegang tot
  [GitLab Ultimate]({{< relref path="/docs/concepts/gitlab" >}}).
* Begrijp de kritieke verschillen tussen opereren (gebruiken) en engineeren met
  Ansible [hier]({{< relref path="/docs/concepts/ansible/engineering-vs-configuring" >}}).
* {{< rellink path="/docs/concepts/dev" desc=true >}}