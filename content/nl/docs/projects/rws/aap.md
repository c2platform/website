---
categories: ["Diagram", "PlantUML", "C4", "gis"]
title: "RWS GIS Automatiseringsplatform"
linkTitle: "GIS Automatiseringsplatform"
draft: false
weight: 1
description: >
  Systeemcontextdiagram voor RWS GIS Automatiseringsplatform dat is gebaseerd op
  de C2 Platformbenadering en draait binnen het RWS-domein op het Red Hat
  Automatiseringsplatform (AAP).
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}})

---

```plantuml
@startuml context-aap
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads
'!define C4_NO_STEREOTYPE
AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

title [Systeemcontext] RWS GIS Automatiseringsplatform

Person(rws_operator, "Ansible Operator", "RWS GIS Platform engineer met basiskennis van Ansible")
Person(rws_engineer, "Ansible Engineer", "RWS GIS Platform engineer met Ansible kennis")
Person(rws_engineer_light, "Ansible Engineer Light", 'Gebruikt een "pseudo" dev omgeving binnen RWS-domein.')
Person_Ext(rws_infra, "RWS Infra Team", "Levert infrastructuurdiensten aan RWS projecten en systeemteams")
Person(c2_engineer, "C2 Ansible Engineer", "Open Source Contributor \nC2 Platform")

'Person_Ext(site_engineer, "Ansible Site Engineer", "Creëert/beheert Ansible rollen/collecties")

System(gis_automation, "GIS_AUTOMATION_LABEL", "GIS_AUTOMATION_DESC")
System(gis, "GIS_LABEL", "Biedt GEO-informatie en verwerkingsdiensten")

'System_Ext(azure_devops, "Azure DevOps\ndev.azure.com/Rijkswaterstaat", "Hosts Ansible projecten van het GIS Platform")
'System_Ext(gitlab, "GitLab\ngitlab.com/c2platform/rws", "Hosts de open source RWS projecten van het GIS Platform")

Rel_U(rws_engineer, c2_engineer, "is een", "", $tags="optional")
Lay_R(rws_operator,rws_engineer)
Lay_R(rws_engineer,rws_engineer_light)
Rel_U(rws_infra, gis_automation, "Levert het platform voor automatiseren op", "RedHat Ansible Automatiseringsplatform (AAP), Ansible Controller (AWX), Ansible Automatiseringshub (Galaxy NG)")
Rel_U(rws_infra, gis, "VM's, netwerk, LB's etc", "VMWare, NetScaler")
'Rel(rws_operator,azure_devops,"Beheert een volledige definitie van het GIS Platform in een Ansible Inventarisproject in", "Browser, Web Edit/IDE, Git Push en Pull")
'Rel(gis_automation, azure_devops, "1) Broncontrole-update en \n2) Inventaris synchronisatie", "https,git,kluis")
'Rel(gis_automation, gitlab, "3) Trek Ansible Execution Environment (EE)", "docker image")
Rel_R(gis_automation, gis, "Automatiseert de levering en het beheer van diensten", "Ansible, Infrastructure-As-Code")
Rel(rws_engineer, gis_automation, "Ontwikkelt GIS Ansible collecties en rollen en een Ansible Uitvoering (EE) voor","C2 Dev Omgeving, Galaxy")
Rel(rws_engineer_light, gis_automation, "Ontwikkelt GIS Ansible collecties en rollen", "Ansible,Citrix VDI → RHEL9 Desktop")
Rel(rws_operator, gis_automation, "Uitvoert LCM en TAM taken met behulp van het Ansible Inventarisproject", "Browser, Web Edit/IDE, Git Push en Pull")
Rel(rws_operator, gis_automation, "Gebruikt Ansible Controller (AWX) om taken te configureren of starten van","Browser")

LAYOUT_WITH_LEGEND()
@enduml
```
