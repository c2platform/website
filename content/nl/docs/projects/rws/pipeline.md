---
categories: [Voorbeeld]
title: "GitOps Pijplijn voor het GIS Platform"
tags: [DTAP, Promotiemodel, GitOps, Ansible-Inventory]
linkTitle: "GitOps Pijplijn"
draft: true
weight: 8
description: >
    De [Groepsgebaseerde Omgevingen]({{< relref path="/docs/guidelines/setup/group-based-environments" >}}) benadering van het GIS Platform Inventory Project maakt een pure GitOps aanpak mogelijk voor het bevorderen en controleren van veranderingen op een gecontroleerde manier met alleen Git Merges.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

---

## Invoering

De C2 Platform benadering voor [Ansible Inventory Projecten]({{< relref path="/docs/concepts/ansible/projects/inventory" >}}) ondersteunt een pure
GitOps aanpak die het bevorderen van wijzigingen alleen met Git Merges mogelijk
maakt; het vereist niet het gebruik van externe merges hulpmiddelen[^1]. Dit is
erg handig omdat met deze opstelling het mogelijk wordt wijzigingen te
bevorderen via een webinterface, gebruikmakend van de functionaliteit die tools
als Azure DevOps en GitLab meestal hebben. Om merge aanvragen te creëren en een
workflow met goedkeuringsprocessen te introduceren.

## Branchstrategie

Het Git diagram hieronder visualiseert de opzet van een inventory project voor
het GIS Platform met drie omgevingen: `development`, `staging` en `production`.
De getoonde opzet is typisch en niets bijzonders, we zien bijvoorbeeld:

1. Een release wordt aangemaakt op de `development` branch met tag `1.0.0`.
2. Deze tag wordt bevorderd / samengevoegd naar `staging` en `production`.
3. Deze tag wordt vervolgens samengevoegd naar de `main` branch. Dit maakt
   duidelijk dat de `main` branch wordt gebruikt voor productieklare code, wat
   de aanbevolen praktijk is.
4. Een hotfix branch `hotfix-12345-fix-something` wordt aangemaakt voor het
   maken van een hotfix voor de productieomgeving.
5. De hotfix wordt getagd als `1.0.1-hotfix-issue-12345` en teruggevoerd naar
   `main`, `development` en `staging`.

```mermaid
gitGraph
    commit id: "Initial commit"

    branch development
    checkout development

    branch staging
    checkout staging

    branch production
    checkout production

    checkout development
    commit
    commit
    commit id: " " tag: "1.0.0"
    checkout staging
    merge development tag: "1.0.0"
    checkout production
    merge staging tag: "1.0.0"
    commit id: "Productierelease 1.0.0"

    checkout main
    merge production tag: "1.0.0"

    checkout production
    branch hotfix-12345-fix-something
    commit id: "Hotfix toegepast" tag: "1.0.1-hotfix-issue-12345"

    checkout main
    merge hotfix-12345-fix-something tag: "1.0.1-hotfix-issue-12345"

    checkout development
    merge hotfix-12345-fix-something tag: "1.0.1-hotfix-issue-12345"

    checkout staging
    merge hotfix-12345-fix-something tag: "1.0.1-hotfix-issue-12345"
```

## Notities

[^1]: Dit is anders dan de Ansible Aanbevolen Aanpak link TODO
[^2]: Er zijn twee smaken bij RWS voor Ansible ontwikkeling: engineering en
    "lichte" engineering. De eerste maakt gebruik van de [open source
    ontwikkelomgeving]({{< relref path="/docs/howto/dev-environment/setup" >}}).
    Voor lichte engineering wordt een "pseudo" ontwikkelomgeving binnen het RWS
    domein gebruikt. Voor aanvullende informatie, raadpleeg:

    * [Engineering van het GIS Platform met Ansible]({{< relref path="/docs/projects/rws/engineering" >}})
    * [Engineering met een "pseudo" Ontwikkelomgeving]({{< relref path="/docs/projects/rws/engineering-light" >}})
    * [Ontwikkelomgeving]({{< relref path="/docs/concepts/dev" >}})