---
categories: ["Voorbeeld"]
tags: [ansible, certificaten, pki, java, keystore, tomcat, sysprep]
title: "Implementeren van PKI voor RWS GIS met Ansible"
linkTitle: "RWS GIS PKI Overzicht"
weight: 5
description: >
   Leer hoe je de generatie en het beheer van RWS SSL/TLS-certificaten en Java
   KeyStores met Ansible kunt automatiseren, en hoe handmatige processen moeiteloos
   worden geïntegreerd.
---

Projecten: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}), [`c2platform.core`]({{<
relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

```plantuml
@startuml overzicht
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads

System_Ext(rws_cert, "RWS CA", "Levert RWS SSL/TLS-certificaten uit")

AddRelTag("optioneel", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

title RWS Ansible PKI Ontwerp
Person(admin, "Ansible Operator")
    System_Boundary(gisBoundaryMngt, "Ansible Automatiseringsplatform (AAP)") {
        Container(linux, "RWS GIS CA", "Linux, RHEL9", "Levert uit, creëert en beheert\nSSL/TLS-certificaten, Java Keystores, etc.", $tags="")
        ContainerDb_Ext(cert_share, "Certificaten", "Windows Share", "Opslaan van alle RWS en GIS CA-certificaten.") {
        }
        Container_Ext(ansible, "Ansible Controller", "AWX, Ansible Control Node", "Beheert infrastructuurautomatisering met Ansible playbooks.")
    }
    System(GIS_System, "GIS_LABEL", "Biedt GEO-informatiediensten")


Rel_L(admin, cert_share, "Upload certificaten")
Rel(ansible, linux, "Beheert certifcaattaken", "")
Rel(ansible, GIS_System, "Implementatie van tijdelijke GIS CA-certificaten")
Rel(ansible, GIS_System, "Actualisering naar RWS-certificaten", $tags="optioneel")
Rel(linux, cert_share, "Toegang tot certificaatopslag", "CIFS")
Rel(admin, rws_cert, "Aanvragen van certificaten", "topdesk")
Rel(rws_cert, admin, "Levert\ncertificaten", "email")

LAYOUT_WITH_LEGEND()
@enduml
```

Conform de gangbare praktijken binnen Nederlandse overheidsorganisaties, wordt
de verantwoordelijkheid voor de creatie van SSL/TLS-certificaten vaak toebedeeld
aan een aparte eenheid. De typische procedure omvat het genereren van een
Certificate Signing Request (CSR), het versturen daarvan naar de aangewezen
eenheid via TopDesk en het ontvangen van een officieel RWS-certificaat terug via
e-mail.

Helaas, in dergelijke gevallen wordt automatisering vaak belemmerd door het
ontbreken van APIs.

Dit gedeelte beschrijft de PKI architectuur voor het RWS GIS Platform, en
benadrukt het gestroomlijnde genereren en beheren van SSL/TLS certificaten en
Java KeyStores. Het proces is ontworpen voor volledige automatisering, waardoor
handmatige interventies tot een minimum worden beperkt.

Hoewel er enkele handmatige stappen betrokken zijn, zijn deze niet essentieel en
vormen ze geen belemmering voor het uitrollen van een volledig geautomatiseerd
GIS Platform. Deze stappen kunnen onafhankelijk worden uitgevoerd ter nakoming
van beveiligings- en conformiteitsstandaarden.

De benadering maakt gebruik van Ansible's `community.crypto` collectie om een
eenvoudige Certificate Authority (CA) op te zetten. Deze CA server is eenvoudig,
met minimale vereisten, en wordt volledig aangedreven door Ansible zonder enige
lopende processen.

Voor gedetailleerde begeleiding, zie de {{< external-link url="https://docs.ansible.com/ansible/latest/collections/community/crypto/docsite/guide_ownca.html" text="Hoe een kleine CA te maken — Ansible Documentatie" htmlproofer_ignore="true" >}}. De `c2platform.core.cacerts2` rol maakt gebruik
van de `community.crypto` collectie om de CA te implementeren.

Voor praktische verkenning, zie:
* [Tomcat SSL/TLS en Java Keystore en TrustStore Configuratie voor Linux en
  Windows Hosts]({{< relref path="/docs/howto/rws/certs" >}})
* [Creëer een Eenvoudige CA Server met Ansible]({{< relref path="/docs/howto/rws/ca" >}})

---

## Stap 1: GIS CA-certificaten

Hieronder staat een sequentiediagram dat het geautomatiseerde voorzieningsproces
toont van SSL/TLS-certificaten en Java KeyStores voor diensten zoals Tomcat op
een FME Core-server. Het proces kan worden gegeneraliseerd naar elke
dienstvoorziening. De Tomcat rol gebruikt de `c2platform.core.cacerts2` rol uit
de `c2platform.core` collectie om de creatie van certificaten en Java KeyStore
over te dragen aan een CA Server.

Elke Linux-server kan dienen als de CA Server, waarbij Linux de voorkeur heeft
vanwege zijn uitgebreide Ansible module-ondersteuning. Deze strategische keuze
centraliseert certificaatbeheer en -uitgifte, en verhoogt de veiligheid door de
privésleutel van de CA te isoleren van doelsystemen.

Het resultaat is een systeem dat klaar is voor gebruik zonder dat er handmatige
stappen vereist zijn.

```plantuml
@startuml stap1

!define ArrowColor #000000
!define BoxColor #A9DCDF
!define ActorColor #FFA500
!define ActorFontSize 14
!define BoxFontSize 14

actor "AAP" as AAP
participant "FME Core" as FME
database "CA Server" as OWNCA
actor "GIS Platform\nTeam" as GEO
actor "RWS CSP" as CSP

'autonumber
'IVP_TB_GEO->AAP: Delegeer beheer GIS\nPlatform beheer
AAP->FME: Voorzie van Tomcat\n(c2platform.gis.tomcat)
FME->OWNCA: Delegeer voorziening\n(c2platform.core.cacerts2)
OWNCA->OWNCA: Creëer eigen CA\nCSR, certificaat,\nJava KeyStore
OWNCA-> FME: Java KeyStore
FME->FME: Configureer HTTPS
@enduml
```

## Stap 2: RWS-certificaten

Deze sequentie schetst het proces van het vervangen van tijdelijke certificaten
door officiële RWS-certificaten, in overeenstemming met RWS beveiligingsbeleid
dat vereist dat alle communicatie via HTTPS en met RWS-certificaten gebeurt.

```plantuml
@startuml stap2

!define ArrowColor #000000
!define BoxColor #A9DCDF
!define ActorColor #FFA500
!define ActorFontSize 14
!define BoxFontSize 14

actor "AAP" as AAP
participant "FME Core" as FME
database "CA Server" as OWNCA
actor "GIS Platform\nTeam" as GEO
actor "RWS CSP" as CSP

'autonumber
OWNCA->GEO: <<scp>> CSR
GEO->CSP: <<e-mail>> CSR
CSP->GEO: <<e-mail>> RWS Certificaat
GEO->OWNCA: <<scp>> RWS Certificaat
AAP->FME: Voorzie van Tomcat\n(c2platform.gis.tomcat)
FME->OWNCA: Delegeer voorziening\n(c2platform.core.cacerts2)
OWNCA->OWNCA: Update Java KeyStore\nmet RWS Certificaat
OWNCA-> FME: Java KeyStore
FME->FME: Configureer HTTPS
@enduml
```

Met de integratie van RWS-certificaten bereikt het platform volledige naleving
en is het klaar voor een veilige productie-uitrol, volledig mogelijk gemaakt
door middel van Ansible-automatisering.