---
categories: ["Diagram", "PlantUML", "C4", "gis"]
title: "Het Bedienen van het GIS Platform met Ansible"
linkTitle: "Bediening van het GIS Platform"
draft: false
weight: 2
description: >
  Het diagram op deze pagina visualiseert hoe LCM en TAM-taken van het GIS-platform worden uitgevoerd met behulp van Ansible. Twee belangrijke componenten zijn het Ansible Inventory Project gehost door Azure DevOps en de Ansible Execution Environment (EE) gehost door GitLab.
---

Projecten: [`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})

---

```plantuml
@startuml rws-gis-automation
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads

title [Container] GIS Platform Automatisering voor Operaties

AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

Person(rws_operator, "RWS Ansible Operator", "RWS GIS Platform ingenieur met basiskennis van Ansible")

System(gis, "GIS_LABEL", "Biedt GEO-informatie en verwerkingsdiensten")
System_Ext(azure_devops, "Azure DevOps\ndev.azure.com/Rijkswaterstaat", "Host Ansible-projecten van het GIS Platform")
System_Ext(gitlab, "GitLab\ngitlab.com/c2platform/rws", "Host de open source RWS-projecten van het GIS Platform")

Boundary(aap_gis, "GIS Platform Automatisering") {
    ContainerDb(gis_inventory, "GIS_INVENTORY_LABEL", "GIS_INVENTORY_TECH", "GIS_INVENTORY_DESC")
    ContainerDb(gis_ee, "GIS_EE_LABEL", "GIS_EE_TECH", "GIS_EE_DESC")
    'ContainerDb(gis_collections, "Ansible Collecties", "Ansible, Git")
  'Boundary(aap_rhel, "RedHat Ansible Automation Platform (AAP)") {
    Container_Ext(aap, "Automatiseringscontroller", "RedHat, K8s, AWX", "Beheert nodes en implementeert Ansible playbooks in omgevingen, biedt een gebruikersinterface, REST API en een taakmotor")
    Container_Ext(app_automation_hub, "Automatiseringshub", "RedHat, K8s, Galaxy NG", "Biedt geverifieerde inhoud\n(collecties, rollen, modules, plugins) om IT-omgevingen te automatiseren")
 '}
  'Container(software, "Software Server", "Linux, RHEL9, Ansible", "Host en serveert alle software-installatieprogramma's, binaries en archieven.")
  'Container(api_automation_server, "API Taken Server", "Linux, RHEL9, Ansible, Python", "Linux-server met geschikte Python-omgeving en Python-bibliotheekafhankelijkheden.")
  'Container(ca_server, "CA Server", "Linux, RHEL9, Ansible", "Geeft uit, creëert en beheert SSL/TLS-certificaten, Java Key Stores, enz.")
}

'Lay(gitlab,gis)
'Lay(rws_operator, rws_engineer)
'Lay_R(azure_devops, gitlab)
'Rel(aap,software,"Downloadt software\nvan","https")
'Rel(aap,api_automation_server,"Delegeer API-automatisering naar","ansible delegate_to")
'Rel(aap,ca_server,"Delegeer certificaat / Java KeyStore taken naar","Ansible delegate_to")

Lay_R(aap, app_automation_hub)
Rel(aap, gis_inventory, "Bij het starten van een (gepland) taak:\n1) Update bronbeheer en \n2) Inventaris Synchronisatie", "https,git,kluis")
Rel(aap, gis_ee, "3) Download\nRWS GIS Ansible EE", "Docker pull")
Rel(aap, gis, "4) Implementeer, update, upgrade services van")
'Rel_U(infra, aap, "Biedt\nals service het RedHat Ansible Automation Platform (AAP)", "")
Rel(rws_operator,gis_inventory,"Voert LCM- en TAM-taken uit door wijzigingen aan te brengen in", "Browser, Web Edit/IDE, Git Push en Pull")
'Rel(c2_engineer,gitlab,"Onderhoudt een open source Ansible Execution Environment (EE)", "git push/pull")
'Rel_R(user,gis, "Toegang tot het GIS platform","")
'Rel-DR(user_ext,gis, "", "")
Rel(rws_operator, aap, "Voer taken uit\nbekijk logs enz.", "browser, https")
'Rel_L(rws_operator, c2_engineer, "Kan een\ndubbele rol hebben als", "", $tags="optional")

Rel(gis_ee, gitlab, "Deel van C2 Platform / GitLab Open Source Programma", "GitLab Ultimate")
Rel(gis_inventory, azure_devops, "In RWS closed-source Git-repository gehost door", "Git")

LAYOUT_WITH_LEGEND()
@enduml
```

## Extra Informatie

Voor aanvullende inzichten en begeleiding:

* Ontdek de unieke Ansible-projecttypes binnen de C2 Platform-aanpak door te
  bezoeken [Ansible Projecten]({{< relref path="/docs/concepts/ansible/projects" >}}).
* RWS hanteert een ["open, tenzij"]({{< relref path="/docs/concepts/oss" >}})
  beleid, waardoor de Uitvoeringsomgeving (EE) van het GIS Platform is opgenomen
  in het GitLab Open Source Program. Dit biedt toegang tot [GitLab Ultimate]({{<
  relref path="/docs/concepts/gitlab" >}}).
* Begrijp de cruciale verschillen tussen het bedienen (gebruiken) en ontwikkelen
  met Ansible [hier]({{< relref path="/docs/concepts/ansible/engineering-vs-configuring" >}}).