---
title: "Projecten"
linkTitle: "Projecten"
weight: 3
description: >
  Een overzicht van projecten van 2016 tot heden die in meer of mindere mate gebruik hebben gemaakt van het C2 Platform. Van de eerste kleine stappen tot de meer geavanceerde benadering die momenteel gaande is.
---

{{< under_construction_nl >}}