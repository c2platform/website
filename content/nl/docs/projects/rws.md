---
title: "RWS GIS Platform ( 2023 t/m heden )"
linkTitle: "RWS"
weight: 2
description: >
  Rijkswaterstaat ( RWS ) GIS Platform
---

{{< under_construction_nl >}}

| Categorie               |   | Tools                                                        |
|-------------------------|---|--------------------------------------------------------------|
| Event-Driven Automation |   |                                                              |
| Orchestration           |   |                                                              |
| Code Pipelines          | ✔ | Azure DevOps                                                 |
| Policy-As-Code          |   |                                                              |
| Configuration-As-Code   | ✔ | [Ansible, AAP]({{< relref path="/docs/concepts/ansible" >}}) |
| Infrastructure-As-Code  | ✔ | [Ansible, AAP]({{< relref path="/docs/concepts/ansible" >}}) |
