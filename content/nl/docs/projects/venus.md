---
title: "SZW Venus ( 2023 tot heden )"
linkTitle: "SZW Venus"
weight: 3
description: >
  Ministerie van Sociale Zaken en Werkgelegenheid ( SZW )
---

{{< under_construction_nl >}}

| Categorie               |   | Tools                          |
|-------------------------|---|--------------------------------|
| Event-Driven Automation |   |                                |
| Orchestratie            | ✔ | Rancher                        |
| Code Pipelines          | ✔ | GitLab (OSS, zelf-gehost) [^1] |
| Policy-As-Code          |   |                                |
| Configuration-As-Code   |   |                                |
| Infrastructure-As-Code  |   |                                |

[^1]: Zelf-gehoste gratis / community editie