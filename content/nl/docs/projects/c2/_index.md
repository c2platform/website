---
categories: ["Diagram", "Project"]
title: "C2 Platform (2022 tot heden)"
linkTitle: "C2"
weight: 2
description: >
  C2 Platform
---

Projecten: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

---

```plantuml
@startuml system
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads
title [Systeem Context] C2 Platform
AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

Person(c2_engineer, "C2 Ansible Engineer", "Open Source Bijdrager \nC2 Platform")
Person_Ext(site_engineer, "Ansible Site Engineer", "Creëert/beheert Ansible rollen/collecties")

Person_Ext(site_operator, "Ansible Site Operator", "")

System(ansible, "C2 Platform", "Open\nAutomatiseringsplatform")
System_Ext(gis, "GIS_LABEL", "Biedt GEO-informatie en verwerkingsdiensten")
System_Ext(suwinet, "Suwinet", "Gegevensuitwisseling UWV, SVB en gemeenten")
System_Ext(cd, "Politie CD Platform", "Continuous Delivery Platform voor 90+ Scrumteams van de Nederlandse politie")

Rel_L(site_engineer, c2_engineer, "Dubbele rol", "", $tags="optional")
Rel(c2_engineer, ansible, "Gebruikt en\ndraagt bij aan", "")
'Rel(site_engineer, ansible, "Gebruikt en\ndraagt bij aan", "")
Rel(site_operator, ansible, "Gebruikt", "")

Rel(ansible, gis, "Automatiseert servicelevering en beheer", "Infrastructure-As-Code")
Rel(ansible, suwinet, "", "")
Rel(ansible, cd, "", "")
Rel(ansible, ansible, "Automatiseert", "")

LAYOUT_WITH_LEGEND()
@enduml
```