---
categories: ["Systeem"]
title: "Systeemschema C2 Platform"
linkTitle: "Systeemschema"
draft: true
weight: 1
description: >
  C2 Platform
---

```plantuml
@startuml overview
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads

'System_Ext(rws_cert, "RWS CA", "Geeft RWS SSL/TLS certificaten uit")

AddRelTag("optional", $textColor=$ARROW_FONT_COLOR, $lineColor=$ARROW_COLOR, $lineStyle=DashedLine())

title [Systeem] C2 Platform
Person(admin, "Ansible Operator")
System_Boundary(c2_platform, "C2 Platform") {
    Container(Galaxy_Container,"Galaxy\ngalaxy.ansible.com" , "website, ansible", "Host alle Ansible content, rolletjes, collecties, modules")
    Container(vagrant_cloud, "Vagrant Cloud\napp.vagrantup.com", "vagrant,virtualbox,lxc", "Host alle C2 Platform afbeeldingen")
    Container(laptop,"Ontwikkelaars Laptop" , "Dell Precision 7670,\n32GB RAM, 1TB SSD", "High-end ontwikkelaars laptop met Vagrant en Ansible en VS Code")

'    Container(Galaxy_Container,"" , "", $tags="")

    ' Container(linux, "RWS GIS CA", "Linux, RHEL9", "Maakt en beheert\nSSL/TLS certificaten, Java Keystores, etc.", $tags="")
    ' ContainerDb_Ext(cert_share, "Certificaten", "Windows Share", "Slaat alle RWS en GIS CA certificaten op..") {
    ' }
    ' Container_Ext(ansible, "Ansible Controller", "AWX, Ansible Control Node", "Beheert infrastructuur automatisering met Ansible playbooks.")
}
System(GIS_System, "GIS_LABEL", "Levert GEO informatiediensten")

' Rel_L(admin, cert_share, "Upload certificaten")
' Rel(ansible, linux, "Beheert certificatentaken", "")
' Rel(ansible, GIS_System, "Deployt tijdelijke GIS CA certificaten")
' Rel(ansible, GIS_System, "Updates naar RWS certificaten", $tags="optional")
' Rel(linux, cert_share, "Toegang tot certificaatopslag", "CIFS")
' Rel(admin, rws_cert, "Aanvragen van certificaten", "topdesk")
' Rel(rws_cert, admin, "Levert\ncertificaten", "email")

LAYOUT_WITH_LEGEND()
@enduml
```