---
title: "UWV ASC ( 2016 tot 2018 )"
linkTitle: "UWV ASC"
weight: 7
description: >
    Bij het Agile Systeemontwikkelingscentrum (ASC) van UWV is gebleken dat het ["open, tenzij"]({{< relref path="docs/concepts/oss" >}}) moet zijn. Anders is er een risico op "per ongeluk IP," met alle bijbehorende gevolgen.
---

{{< under_construction_nl >}}

|Categorie||Tools|
|-----------------------|----|-----------|
|Event-Driven Automation|    |           |
|Orchestration          |    |           |
|Code Pipelines         |    |           |
|Policy-As-Code         |✔   |Chef       |
|Configuration-As-Code  |✔   |Chef       |
|Infrastructure-As-Code |✔   |Chef       |

Het belang van ["open, tenzij"]({{< relref path="docs/concepts/oss" >}}) wordt
ervaren in een {{< external-link url="https://scaledagileframework.com/system-team/" text="SAFe-systeemteam" htmlproofer_ignore="false" >}}, dat de Scrum/Agile/Agifall-teams van
{{< external-link url="https://nl.wikipedia.org/wiki/UWV_WERKbedrijf" text="UWV Werkbedrijf" htmlproofer_ignore="false" >}}
ondersteunde met essentiële tools, waaronder Jenkins, versiebeheer met
Subversion/Git, Confluence, Jira, Nexus en SonarQube. Om het levenscyclusbeheer
van deze hulpmiddelen effectiever en efficiënter te maken, werden ze
geautomatiseerd als onderdeel van een "Infrastructure as Code" (IaC) aanpak.

Helaas ging deze waardevolle automatisering verloren bij veranderingen in
leveranciers en contracten. Dit leidde tot het besef dat er behoefte was aan een
["open, tenzij"]({{< relref path="docs/concepts/oss" >}})-benadering, omdat
zonder zo'n aanpak "per ongeluk IP" ontstaat.

Met "per ongeluk IP" wordt er "onbewust" Intellectual Property (IP) gecreëerd,
waarbij vaak niet duidelijk is wie de eigenaar is van de gegenereerde IP en
welke partij verantwoordelijk is voor de relatie tot de IP.

In de context van projecten bij de Nederlandse overheid moet het ["open,
tenzij"]({{< relref path="docs/concepts/oss" >}}) zijn; het creëren van IP moet
een weloverwogen keuze zijn.

Hierdoor kunnen de inspanningen en investeringen die overheidsinstanties zoals
UWV in automatisering doen, gemakkelijker worden hergebruikt, zowel binnen UWV
als daarbuiten.