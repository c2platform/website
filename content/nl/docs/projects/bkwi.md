---
title: "Bureau Keteninformatisering Werk en Inkomen ( BKWI ) ( 2020 tot 2022)"
linkTitle: "BKWI"
weight: 5
description: >
  Bij Bureau Keteninformatisering Werk en Inkomen (BKWI) vindt er een verfijning van de aanpak plaats gebaseerd op de automatisering van de Politie.
---

{{< under_construction_nl >}}

| Categorie               |   | Tools             |
|-------------------------|---|-------------------|
| Event-Driven Automation | ✔ | Zabbix            |
| Orchestration           | ✔ | Kubernetes        |
| Code Pipelines          | ✔ | GitLab            |
| Policy-As-Code          | ✔ | Ansible, AWX [^1] |
| Configuration-As-Code   | ✔ | Ansible, AWX      |
| Infrastructure-As-Code  | ✔ | Ansible, AWX      |

[^1]: Oudere, open source versie van Ansible Automation Platform ( AAP )