---
title: "UWV TAB ( 2022 tot heden )"
linkTitle: "UWV TAB"
weight: 4
description: >
  UWV Technisch Applicatiebeheer ( TAB )
---

{{< under_construction_nl >}}

| Categorie                     |   | Tools                          |
|-------------------------------|---|--------------------------------|
| Eventgestuurde Automatisering |   |                                |
| Orchestratie                  |   |                                |
| Code Pipelines                |   | Azure DevOps [^1]              |
| Policy-As-Code                |   | Ansible CLI                    |
| Configuratie-Als-Code         |   | Ansible CLI, Azure DevOps [^1] |
| Infrastructuur-Als-Code       |   | Ansible                        |

[^1]: Zelf-gehoste Azure DevOps.