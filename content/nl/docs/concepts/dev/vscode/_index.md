---
categories: ["Begrip"]
tags: [laptop, ubuntu, ide]
title: Visual Studio Code
linkTitle: VS Code
weight: 5
description: >
  Visual Studio Code biedt een rijke set aan functies, waaronder syntax highlighting, linting, debugging, integratie met Git en een uitgebreid assortiment aan extensies. Deze mogelijkheden vereenvoudigen taken zoals het maken van playbooks en het beheren van inventory.
---

{{< under_construction_nl >}}