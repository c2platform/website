---
categories: ["Boek"]
tags: [ansible, devops, geerlingguy, vagrant]
title: Ansible voor DevOps
linkTitle: Ansible voor DevOps
weight: 1
description: >
  Dit boek, Ansible voor DevOps, vormt de technische basis voor de aanpak binnen het C2 Platform.
---

Het boek [Ansible voor DevOps](https://www.ansiblefordevops.com/) van Jeff
Geerling vormt de technische basis voor de aanpak binnen het C2 Platform, met
Vagrant en Ansible.

> Vagrant is een uitstekend hulpmiddel om lokale (of cloud-gebaseerde)
> infrastructuur na te bootsen ... en Ansible is een uitstekend hulpmiddel voor
> het inrichten van servers, het beheren van hun configuratie en het
> implementeren van applicaties, zelfs op mijn lokale werkstation!

[![Ansible voor
DevOps](ansible-for-devops.png?width=200px#float-end)](https://www.ansiblefordevops.com/
"Ansible voor DevOps")