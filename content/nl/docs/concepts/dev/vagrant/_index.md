---
categories: ["Begrip"]
tags: [vagrant, ansible]
title: Vagrant
linkTitle: Vagrant
weight: 2
description: >
  Vagrant biedt een gebruiksvriendelijk platform voor het maken en beheren van
  eenvoudige lokale ontwikkelomgevingen, waardoor het installatieproces wordt
  gestroomlijnd.
---

Vagrant is een tool ontwikkeld door HashiCorp voor het opbouwen en beheren van
virtuele machine-omgevingen in één workflow. Het biedt een eenvoudige workflow
en richt zich op automatisering, waardoor de installatietijd voor
ontwikkelomgevingen lager wordt en de overeenstemming met productieomgevingen
toeneemt.

Hier is hoe Vagrant meestal wordt gebruikt:

1. **Configuratiebeheer**: Vagrant stelt je in staat om lichte, reproduceerbare
   en draagbare ontwikkelomgevingen te maken en te configureren. Het gebruikt
   een eenvoudig configuratiebestand (genaamd Vagrantfile) om het type machine
   dat je wilt, de software die moet worden geïnstalleerd en hoe ze moeten
   worden geconfigureerd, te beschrijven.
2. **Voorziening**: Vagrant kan automatisch software installeren op de virtuele
   machine zodra deze is aangemaakt, met gebruik van provisietools zoals
   shell-scripts, Ansible, Chef of Puppet. Dit is vooral nuttig voor het
   opzetten van complete ontwikkelomgevingen.
3. **Provider Onafhankelijk**: Hoewel Vagrant voornamelijk werkt met VirtualBox,
   ondersteunt het andere providers zoals VMware, AWS en anderen. Deze
   flexibiliteit stelt je in staat om je virtuele machines bij verschillende
   providers te draaien met dezelfde configuratie.
4. **Netwerken**: Vagrant omvat eenvoudig te configureren, ingebouwde
   ondersteuning om netwerkinterfaces op te zetten waarmee je gemakkelijker
   toegang krijgt tot je virtuele machine vanaf je host machine of vanaf andere
   machines in hetzelfde netwerk.
5. **Gesynchroniseerde Mappen**: Vagrant synchroniseert je projectbestanden met
   de virtuele machine, waardoor je door kunt gaan met werken aan je
   projectbestanden op je host machine, maar de resources in de virtuele machine
   kunt gebruiken om je project te compileren of uit te voeren.
6. **Draagbare Werkomgevingen**: Vagrant boxes zijn volledig wegwerpbaar. Als er
   iets misgaat, kun je de omgeving binnen enkele minuten vernietigen en opnieuw
   creëren zonder zorgen.

Vagrant is bijzonder populair onder softwareontwikkelaars, systeembeheerders en
DevOps-engineers omdat het helpt bij het creëren van een consistente omgeving
die gedeeld kan worden binnen teams, en zo helpt om het "het werkt op mijn
machine"-syndroom te verminderen.

* {{< external-link url="https://www.vagrantup.com/" text="Vagrant" htmlproofer_ignore="true" >}}
* Zie het C2 voorbeeldproject [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) voor een voorbeeld van
  een Vagrant-project met gebruik van [LXD]({{< relref "../lxd" >}} "Concept:
  Linux Containers (LXD)"), [VirtualBox]({{< relref "../virtualbox" >}}
  "Concept: VirtualBox"), en [Ansible]({{< relref "../../ansible" >}} "Concept:
  Ansible Automation Platform").
* {{< external-link url="https://docs.ansible.com/ansible/latest/scenario_guides/guide_vagrant.html" text="Vagrant Guide - Ansible Documentation" htmlproofer_ignore="true" >}}