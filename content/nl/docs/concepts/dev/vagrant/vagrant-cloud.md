---
categories: ["Begrip"]
tags: [vagrant, afbeelding]
title: Vagrant Cloud
linkTitle: Vagrant Cloud
weight: 2
description: >
  C2 Platform-afbeeldingen voor LXD, VirtualBox met Ubuntu, RedHat Enterprise Linux en Windows 2022 Server.
---

C2 Platform-afbeeldingen worden gepubliceerd en gedistribueerd via de {{<
external-link url="https://app.vagrantup.com/c2platform/" text="Vagrant Cloud"
htmlproofer_ignore="true" >}}.