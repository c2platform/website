---
categories: ["Begrip"]
tags: [laptop, ubuntu, vagrant, virtualbox]
title: VirtualBox
linkTitle: VirtualBox
weight: 4
description: >
  In gevallen waarin LXD niet haalbaar is, biedt VirtualBox een betrouwbaar alternatief. Het levert vooraf geconfigureerde VM-afbeeldingen, waaronder opties voor Microsoft Windows-doelsystemen, en zorgt voor compatibiliteit in diverse omgevingen.
---

[LXD]({{< relref path="../lxd/" >}}) is de standaard virtualisatietechniek voor
de ontwikkelomgeving, maar in situaties waarin LXD niet beschikbaar is, zoals
bij MS Windows, zijn er ook VirtualBox-afbeeldingen beschikbaar in de [Vagrant
Cloud]