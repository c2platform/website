---
categories: ["Begrip"]
tags: [lxd, laptop, vagrant]
title: LXD
linkTitle: Linux Containers ( LXD )
weight: 3
description: >
    LXD biedt lichte en flexibele virtualisatie van VM's, waarbij de voordelen
    van traditionele VM's worden gecombineerd met de wendbaarheid van Docker-containers.
---

Zie [Vagrant Cloud]({{< relref path="../vagrant/vagrant-cloud.md" >}}) voor
beschikbare afbeeldingen / boxes.
