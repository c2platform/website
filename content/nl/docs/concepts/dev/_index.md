---

categories: ["Begrip"]
tags: [laptop, ubuntu]
title: Ontwikkelomgeving
linkTitle: Ontwikkelomgeving
weight: 2
description: >
  Ervaar ongeëvenaarde flexibiliteit en productiviteit via lokale ontwikkeling,
  een realiteit mogelijk gemaakt door het omarmen van de
  ["open, tenzij"]({{< relref path="/docs/concepts/oss" >}})
  benadering.

---

In de context van automatiseringsinitiatieven voor de Nederlandse overheid wordt
de ontwikkelomgeving vaak over het hoofd gezien. Toch speelt het een cruciale
rol in het succes van elk project, met name die gebruikmakend van [Ansible]({{<
relref "../ansible" >}} "Concept: Ansible Automation Platform"). Het aannemen
van een lokale ontwikkelomgeving, uitgerust met native
virtualisatiemogelijkheden, biedt tal van voordelen vergeleken met het
vertrouwen op datacenter-gebaseerde infrastructuur, waar VM's worden gecreëerd
en beheerd door externe teams.

1. **Verbeterde Iteratiesnelheid**: Een lokale ontwikkelomgeving stelt engineers
   in staat om snel hun oplossingen te herhalen en te testen. Deze autonomie
   elimineert de noodzaak om te vertrouwen op het infrastructuurteam voor
   VM-provisioning en deprovisioning, wat de ontwikkelingsefficiëntie
   aanzienlijk verhoogt.
2. **Isolatie en Beveiliging**: Het gebruik van lokale
   virtualisatietechnologieën zoals LXD en VirtualBox zorgt voor een veilige en
   geïsoleerde omgeving voor Ansible-ontwikkeling. Deze isolatie minimaliseert
   conflicten met andere softwarecomponenten en behoudt controle over de
   testomgeving.
3. **Kostenefficiëntie**: Een lokale ontwikkelomgeving is kostenefficiënt, omdat
   het de noodzaak voor het provisioneren van extra VM's en het maken van extra
   kosten tenietdoet. Deze aanpak optimaliseert het gebruik van middelen en
   budgettoewijzing.
4. **Flexibiliteit**: Ingenieurs krijgen de vrijheid om hun eigen VM's te
   configureren en op te zetten op basis van specifieke projecteisen. Deze
   flexibiliteit maakt uitgebreide tests van verschillende configuraties en
   scenario's mogelijk, wat robuuste ontwikkeling vergemakkelijkt.
5. **Leermogelijkheden**: Het opzetten en beheren van een lokale
   ontwikkelomgeving en VM's biedt ingenieurs waardevolle leerervaringen. Deze
   praktijk in virtualisatie en infrastructuurbeheer verbetert hun expertise en
   maakt hen meer bekwame Ansible-ingenieurs.

Het gebruik van een lokale ontwikkelomgeving in automatiseringsprojecten van de
Nederlandse overheid zorgt ervoor dat ingenieurs productief kunnen werken, snel
kunnen beginnen en onafhankelijk kunnen blijven van toegang tot datacenters en
administratieve obstakels. Deze aanpak bevordert wendbaarheid, kostenbesparingen
en een dieper inzicht in infrastructuurbeheer, wat uiteindelijk bijdraagt aan
het succes van Ansible-automatiseringsinitiatieven.