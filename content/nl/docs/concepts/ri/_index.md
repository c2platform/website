---
categories: ["Begrip"]
tags: []
title: "Referentie-Implementaties"
linkTitle: "Referentie-Implementaties"
weight: 3
description: >
  Ontgrendel de kracht van de ontwikkelomgeving en creëer volledig geïntegreerde,
  allesomvattende systemen. Met slechts een paar eenvoudige commando's kunt u
  bijvoorbeeld de automatisering verkennen die de implementatie en het beheer van
  een GIS-platform bij Rijkswaterstaat (RWS) aanstuurt.
---

In het domein van systeemtechniek en infrastructuur-als-code (IaC) overstijgt de term
"referentie-implementatie" de status van een eenvoudige blauwdruk. Het evolueert tot een
volledig functioneel, naadloos geïntegreerd systeem. Dit voorbeeld dient niet alleen als model, 
maar ook als gestandaardiseerd, goed gedocumenteerd voorbeeld, waarin wordt gedemonstreerd hoe 
een specifiek systeem efficiënt kan worden geïmplementeerd en beheerd, met 
[Ansible]({{< relref path="/docs/concepts/ansible" >}}) in de hoofdrol.

Ansible, een spil in elk 
[automatiserings]({{< relref path="/docs/concepts/automation" >}}) 
initiatief, speelt een cruciale rol in deze context.

Binnen het kader van het C2-platform verbindt een referentie-implementatie twee 
essentiële componenten: het 
[Ansible Inventory Project]({{< relref path="/docs/concepts/ansible/projects/inventory" >}}) 
en het 
[Vagrant Project]({{< relref path="/docs/concepts/dev/vagrant" >}}). 
De synergie van Vagrant en Ansible is bijzonder krachtig en biedt een 
allesomvattende oplossing. Vagrant regisseert de creatie van virtuele machines, 
inclusief netwerkconfiguraties en schijftoewijzingen, terwijl Ansible 
verantwoordelijk is voor het installeren en beheren van diverse knooppunten en 
virtuele machines. In het geval van het GIS-platform omvat dit bijvoorbeeld de 
implementatie van componenten zoals ArcGIS Enterprise Web Adaptor, Portal, 
Server, Datastore, FME en meer.

Zoals afgebeeld in het onderstaande diagram, kan de C2-platformingenieur 
moeiteloos complete omgevingen opbouwen via Vagrant, gebruikmakend van diverse 
cloudservices:

1. [Vagrant Cloud]({{< relref path="/docs/concepts/dev/vagrant/vagrant-cloud" >}})
   herbergt C2 afbeeldingen die compatibel zijn met LXD, VirtualBox en meer.
1. [GitLab.com]({{< relref path="/docs/concepts/gitlab" >}})
   dient als het repository voor alle C2-projecten en biedt tal van middelen.
1. [Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}) fungeert als het 
   repository voor open-source automatiseringsrollen en collecties, inclusief 
   die ontworpen zijn voor het C2-platform in het domein van GIS.

Dit alles is mogelijk dankzij een 
["open, tenzij"]({{< relref path="/docs/concepts/oss" >}}) benadering. 
Deze benadering staat de verkenning en het testen van het GIS-platform toe, 
waarbij het de implementatie in het RWS Data Center weerspiegelt, door gebruik 
te maken van het open-source tegenhanger referentie-implementatie.

In de context van een open-source Ansible-project staat een referentie-implementatie 
als een voornaam voorbeeldproject, dat inzicht biedt in hoe 
[Ansible-collecties]({{< relref path="/docs/concepts/ansible/projects/collections" >}}) 
en [Ansible-rollen]({{< relref path="/docs/concepts/ansible/projects/roles" >}}) 
zijn geconfigureerd binnen een 
[Ansible Inventory Project]({{< relref path="/docs/concepts/ansible/projects/inventory" >}}), 
vaak aangeduid als een playbook-project. Deze configuratie rust gebruikers uit 
om een volledig functioneel systeem te implementeren. De 
referentie-implementatie dient niet alleen als een abstracte referentie, maar 
als een praktisch raamwerk, openlijk toegankelijk voor soortgelijke 
implementaties door klanten en organisaties.

Laten we ingaan op de kernattributen van dit concept:

1. **Uitgebreide Implementatie:** Een referentie-implementatie omvat de 
   volledigheid van een functioneel en geïntegreerd systeem. Het behandelt 
   zorgvuldig alle configuraties, instellingen en onderlinge afhankelijkheden 
   die nodig zijn voor naadloze werking. Deze volledigheid verzekert dat de 
   referentie-implementatie het beoogde systeem volledig weerspiegelt.
1. **Pariteit met Productie:** Bijvoorbeeld, de Referentie-implementatie van het 
   GIS-platform dient als de open-source tegenhanger van het GIS-platform binnen 
   Rijkswaterstaat (RWS). Dit impliceert dat het de productieomgeving nauwkeurig 
   nabootst en zorgt voor afstemming met real-world opstellingen.
1. **Gedetailleerde Documentatie en Beste Praktijken:** Net als conventionele 
   referentie-implementaties beschikt een complete referentie-implementatie over 
   uitgebreide documentatie. Deze documentatie verduidelijkt het doel van het 
   systeem, de configuraties, beste praktijken en beheerrichtlijnen. Het dient 
   als een onschatbare bron voor teamleden die betrokken zijn bij de 
   implementatie en het onderhoud van het systeem.
1. **Training en Consistentie:** De referentie-implementatie dient ook als een 
   waardevolle bron voor training en leren, waarbij teamleden worden geholpen om 
   te begrijpen hoe het hele systeem efficiënt kan worden geïmplementeerd en 
   beheerd. Bovendien zorgt het voor consistentie binnen uw infrastructuur, 
   waardoor het risico op fouten of verkeerde configuraties wordt verminderd.

In de context van Ansible en Vagrant kan een uitgebreide 
referentie-implementatie Ansible-playbooks bevatten voor systeemconfiguratie en 
beheer, samen met Vagrant-bestanden die de VM-infrastructuur beschrijven. Deze 
bronnen zijn grondig gedocumenteerd en rigoureus getest om ervoor te zorgen dat 
ze het beoogde systeem nauwkeurig vertegenwoordigen.

Samengevat, een referentie-implementatie is geen eenvoudige schets, maar een 
volledig functioneel, volledig geïntegreerd systeem. Het garandeert efficiënt en 
consistent beheer van infrastructuurcomponenten terwijl het in lijn is met de 
realiteit van productieomgevingen. Dit is met name belangrijk in situaties zoals 
de Referentie-implementatie van het GIS-platform, waar het tot doel heeft een 
bestaand systeem binnen organisaties zoals Rijkswaterstaat (RWS) te repliceren.

Het doel van de referentie-implementatie strekt zich uit tot het verduidelijken 
van hoe software kan worden toegepast en geconfigureerd, en dient als een 
lanceerplatform voor het aanpassen en configureren van software om aan 
specifieke eisen te voldoen. Door gebruik te maken van open-source bouwstenen, 
zoals Ansible-rollen, kunnen organisaties het werk van anderen benutten, 
waardoor de ontwikkeling en implementatie van software wordt versneld. Dit 
bevordert uniformiteit in implementaties en stimuleert samenwerkingsinspanningen 
tussen organisaties die dezelfde software gebruiken.

Een voorbeeld van een referentie-implementatie is het 
[`ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}) 
configuratie- en playbook-project voor het Rijkswaterstaat GIS-platform. Dit 
project vergemakkelijkt de lokale implementatie van een uitgebreide, functionele 
omgeving voor het Rijkswaterstaat GIS-platform. Om dit te bereiken maakt het 
project gebruik van Ansible-collecties zoals 
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}) 
en 
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}).

```plantuml
@startuml ri-gis-platform
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads

LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()

Person(engineer, "Engineer")

Boundary(laptop, "high-end developer laptop", $type="ubuntu-22") {
  System(gsd, "GIS_LABEL", "GIS_DESC", "", $tags="GIS_System") {
  }
  Container(vagrant, "Vagrant", "", $tags="Vagrant_Container")
  Container(ansible_local, "Ansible", "", $tags="Ansible_Container")
}

System_Ext(galaxy_website, "ANSIBLE_GALAXY_LABEL", "ANSIBLE_GALAXY_DESC", $tags="Ansible_System_Ext")
System_Ext(gitlab, "GITLAB_LABEL", "GITLAB_DESC", $tags="Gitlab_System_Ext") {
   Container_Ext(gis_inventory, "GIS_REF_IMPL_LABEL", "GIS_REF_IMPL_DESC", $tags="GIS_Container_Ext")
}
System_Ext(vagrant_cloud, "VAGRANT_CLOUD_LABEL", "VAGRANT_CLOUD_DESC", $tags="Vagrant_System_Ext")

Rel(ansible_local, gsd, "Provision ArcGIS Portal, WebAdaptor, Server, Datastore, FME, etc", "")
Rel(vagrant, gsd, "Create VM's\nnetwork, disks etc", "")
Rel(engineer, vagrant, "Create VM's, provision services using Vagrant CLI", "")
Rel_D(vagrant, ansible_local, "", "")
Rel_R(ansible_local, galaxy_website, "Download GIS automation / collection", "")
Rel(ansible_local, gitlab, "", "")
Rel_R(vagrant, vagrant_cloud, "Download Vagrant \nboxes ( images )", "")
@enduml
```