---
categories: ["Bron"]
tags: [odm, oss, bzk]
title: "Opensourcewerken: voorbij de vrijblijvendheid"
linkTitle: "Rapport Opensourcestrategie BZK"
weight: 3
description: >
    Rapport over de opensourcestrategie van het Ministerie van Binnenlandse Zaken en Koninkrijksrelaties (BZK).
---

{{< external-link url="https://www.digitaleoverheid.nl/nieuws/opensourcewerken-de-vrijblijvendheid-voorbij/" text="Opensourcewerken: de vrijblijvendheid voorbij (Nederlands)" htmlproofer_ignore="false" >}}