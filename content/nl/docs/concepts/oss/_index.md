---
categories: ["Begrip"]
tags: [odm, oss, begrip]
title: "Open, tenzij"
linkTitle: "Open, tenzij"
weight: 1
description: >
  Een open aanpak vergemakkelijkt het hergebruik van ideeën en code, niet alleen binnen projecten, maar zelfs tussen verschillende organisaties.
---

Open Source is meer dan alleen een licentie; het is ook een manier van werken.
Het aannemen van een "open" benadering is cruciaal in de context van
automatiseringsprojecten voor de Nederlandse overheid om de noodzakelijke
productiviteit en flexibiliteit te bereiken.

Een open benadering binnen automatiseringsprojecten bevordert niet alleen het
delen van code en ideeën binnen een project, maar ook tussen verschillende
organisaties. Dit versterkt de samenwerking en maakt het mogelijk gezamenlijk te
werken aan gemeenschappelijke uitdagingen en oplossingen.

Binnen de Nederlandse overheid speelt open source software (OSS) een cruciale
rol bij het bevorderen van een open benadering. Door gebruik te maken van open
source software kunnen organisaties profiteren van bestaande oplossingen,
bijdragen aan de gemeenschap en samenwerken aan de ontwikkeling van robuuste en
betrouwbare systemen.

Het C2 Platform, als een open automatiseringsplatform voor de Nederlandse
overheid, ondersteunt en stimuleert het gebruik van open source software en een
open benadering. Door kennis en ervaringen te delen, samenwerking te bevorderen
en het hergebruik van code te stimuleren, draagt het platform bij aan de groei
en innovatie van automatiseringsprojecten binnen de overheid.

Met een open benadering kunnen automatiseringsprojecten evolueren van
[Infrastructure-as-Code naar Event-driven Automation]({{< relref path="/docs/concepts/automation" >}}), waardoor de Nederlandse overheid
systematisch kan automatiseren en de levering en het beheer van IT-diensten kan
verbeteren. Het resultaat is een efficiëntievere en flexibelere
IT-infrastructuur die beter kan inspelen op veranderingen en aan de behoeften
van de burgers en organisaties die door de overheid worden bediend.