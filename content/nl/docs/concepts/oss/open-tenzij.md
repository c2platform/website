---
categories: ["Bron"]
tags: [odm, oss]
title: "Kabinetsbeleid: Open, Tenzij"
linkTitle: "Kabinetsbeleid"
weight: 1
description: >
    De overheid streeft naar open source software, tenzij het om wettelijke
    redenen niet mogelijk is. Dit vergroot transparantie, samenwerking en
    softwareveiligheid, bevordert efficiëntie en innovatie, en vermindert
    leveranciersafhankelijkheid.
---

Het kabinetsbeleid, ook wel 'Open, Tenzij' genoemd, heeft tot doel om software
die door of voor de overheid is ontwikkeld, zoveel mogelijk open source te
maken. Dit betekent dat de broncode van de software toegankelijk en herbruikbaar
moet zijn voor iedereen, tenzij er wettelijke beperkingen zijn onder de Wet Open
Overheid.

Het open source beleid van de overheid biedt diverse voordelen. Ten eerste
verhoogt het de transparantie en het vertrouwen in de overheid, omdat iedereen
inzage kan krijgen in hoe de software werkt. Ten tweede bevordert het de
samenwerking, zowel binnen de overheid als met externe partners, wat leidt tot
effectiever en efficiënter openbaar bestuur. Bovendien draagt open source bij
aan de veiligheid van software doordat anderen de code kunnen inspecteren op
fouten en risico's.

Daarnaast stimuleert open source innovatie, verlaagt het de kosten van licenties
en versnelt het de ontwikkelingsprocessen. Het vergroot de flexibiliteit in het
gebruik van software en vermindert de afhankelijkheid van grote commerciële
leveranciers.

Kortom, het 'Open, Tenzij' beleid van de overheid bevordert transparantie,
samenwerking, veiligheid, efficiëntie, innovatie en onafhankelijkheid binnen de
softwareontwikkelingsprocessen van de overheid.

{{< external-link url="https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/open-source/beleid/" text="Beleid Open Source - Digitale Overheid" htmlproofer_ignore="false" >}}