---
categories: ["Begrip"]
tags: [ansible, awx, execution-environment, pyenv, python, winrm, kerberos, windows]
title: "Ansible Execution Environment Project"
linkTitle: "Uitvoeringsomgeving"
weight: 5
description: >
    Het Ansible Execution Environment-project biedt een gestandaardiseerde omgeving
    voor het uitvoeren van Ansible playbooks en rollen.
---

Het Ansible Execution Environment-project biedt een gestandaardiseerde omgeving
voor het uitvoeren van Ansible playbooks en rollen. Het omvat alle benodigde
afhankelijkheden en configuraties om consistente en betrouwbare uitvoering
over verschillende systemen heen te waarborgen.

Door gebruik te maken van de Ansible Execution Environment kunnen gebruikers
zorgen voor consistente uitvoering van hun automatiseringsworkflows op
verschillende systemen, ongeacht het onderliggende besturingssysteem of platform.
Dit elimineert de noodzaak om handmatig afhankelijkheden te installeren en
configureren, waardoor potentiële compatibiliteitsproblemen worden verminderd
en de algehele efficiëntie wordt verbeterd.

Bijvoorbeeld in het
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})
project:

1. Verbeterde controle over Python- en Ansible-versies met behulp van `pyenv`.
1. Ondersteuning voor Kerberos en WinRM, die standaard niet beschikbaar zijn in CentOS 8.
1. Eenvoudige aanpassing en bijdrage van nieuwe versies van de uitvoeringsomgeving.
