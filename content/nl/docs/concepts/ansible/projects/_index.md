---
categories: ["Begrip"]
tags: [ansible, git, project]
title: "Ansible Projecten"
linkTitle: "Projecten"
weight: 3
description: >
  Ansible Inventory, Ansible Collection, Ansible Role en Ansible Execution Environment zijn verschillende soorten projecten gerelateerd aan Ansible. Deze sectie biedt een overzicht van elk projecttype en hun betekenis binnen het Ansible-ecosysteem.
---