---
categories: ["Begrip"]
tags: [ansible, collection, galaxy, .gitlab-ci.yml]
title: "Ansible Collectieproject"
linkTitle: "Collectie"
weight: 3
description: >
  Een Ansible Collectieproject is een uitgebreide eenheid die modules, plugins, rollen en documentatie combineert om de automatiseringstaal te verbeteren en infrastructuren te beheren. Het dient als een herbruikbaar en verspreidbaar pakket van Ansible-inhoud.
---

Een Ansible Collectie verzamelt modules, plugins en rollen om de mogelijkheden
van Ansible voor infrastructuurbeheer uit te breiden. Het is een
gestandaardiseerd formaat voor het verpakken en delen van Ansible-inhoud. Deze
collecties kunnen worden benaderd en gedeeld via de [Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}) website.

Voorbeelden van dergelijke projecten zijn [`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}) en
[c2platform.gis]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}).

Deze projecten zijn ontworpen om het gebruikers gemakkelijk te maken om Ansible
Collecties te gebruiken en te integreren in hun werkprocessen. Voor een
volledige lijst van [C2 Platform Collecties]({{< ref path="/tags/collection" >}}), bezoek de

[Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}) website: {{<
external-link url="https://galaxy.ansible.com/ui/search/?keywords=c2platform"
text="C2 Platform Collecties op de Galaxy-website" htmlproofer_ignore="false"
>}}.

Meer informatie over Ansible collecties is beschikbaar op de Ansible-website,
zie {{< external-link url="https://docs.ansible.com/ansible/latest/dev_guide/developing_collections.html" text="Ontwikkelen van collecties - Ansible Documentatie" htmlproofer_ignore="true" >}}

Het [`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}) project bestaat uit
de volgende componenten:

1. `galaxy.yml`: metadata-bestand dat de collectie beschrijft, inclusief naam,
   versie, afhankelijkheden en ondersteunde platforms.
1. `meta` directory: slaat collectiespecifieke metadata-bestanden op, zoals
   afhankelijkheden en tags.
1. `plugins` directory: bevat aangepaste plugins of modules die voor de
   collectie zijn ontwikkeld.
1. `roles` directory: omvat herbruikbare rollen die binnen playbooks kunnen
   worden gebruikt.
1. `tests` directory: omvat tests om de functionaliteit van de collectie te
   waarborgen.
1. `CHANGELOG.md`: een bestand dat de versiegeschiedenis en aangebrachte
   wijzigingen in de collectie documenteert.
1. `README.md`: biedt essentiële informatie over de collectie, inclusief
   installarie-instructies, gebruiksvoorbeelden en aanvullende bronnen.
1. `.gitlab-ci.yml`: zie [CI/CD Pipelines voor Ansible Collecties]({{< relref path="/docs/tutorials/git-workflow/6-cicd/gitcicd/collections" >}}) voor
    meer informatie.

Deze projecten zijn ontworpen om Ansible Collecties te integreren en gebruikers
in staat te stellen vooraf gebouwde automatiseringsinhoud voor verschillende
toepassingen te benutten.
