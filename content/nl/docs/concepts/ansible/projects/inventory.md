---
categories: ["Begrip"]
tags: [ansible, inventory, playbook, configuratie, secret_vars, group_vars, host_vars, requirements.yml, galaxy]
title: "Ansible Inventory Project"
linkTitle: "Inventory"
weight: 1
description: >
  Een Ansible Inventory-project bevat inventarisbestanden, plays, hostconfiguraties, groepsvariabelen en kluisbestanden. Het wordt ook wel aangeduid als een playbook-project of configuratieproject.
---

Voorbeelden van zulke projecten zijn [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) en [`c2platform/ansible-gis`]({{<
relref path="/docs/gitlab/c2platform/ansible" >}}). Deze projecten zijn
gestructureerd om te worden gebruikt en geconsumeerd door [AAP / AWX]({{< relref path="../aap.md" >}}).

Binnen [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) vind je:

1. `hosts-dev.ini`: een bestand met hostconfiguraties.
1. `group_vars` directory: slaat groepsvariabelen op.
1. `plays` directory: bevat Ansible plays/playbooks.
1. `secret_vars` directory: een speciale locatie voor het opslaan van geheimen.
   Voor meer details over het beheren van geheimen met [Ansible Vault en AAP /
   AWX]({{< relref path="/docs/guidelines/setup/secrets" >}}), raadpleeg de
   documentatie.
1. `collections/requirements.yml` bestand: gebruikt door [AAP / AWX]({{< relref path="../aap.md" >}}) om [Ansible Collections]({{< relref path="./collections" >}}) van [Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}) te installeren.
1. `roles/requirements.yml` bestand: vergelijkbaar met
   collections/requirements.yml, wordt dit bestand gebruikt door [AAP / AWX]({{<
   relref path="../aap.md" >}}), specifiek voor het installeren van [Ansible
   Roles]({{< relref path="./roles" >}}) van [Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}).

Voor verdere referentie, verken de volgende richtlijnen:

* [GitOps Pipeline voor een Execution Environment (EE) met Ansible
  Collections]({{< relref path="/docs/howto/rws/aap/gitops" >}})
* [Variabele Prefix]({{< relref path="/docs/guidelines/coding/var-prefix" >}}
  "Richtlijn: Variabele prefix")
* [Groepsgebaseerde omgevingen]({{< relref path="/docs/guidelines/setup/group-based-environments" >}} "Richtlijn:
  Groepsgebaseerde omgevingen")
* [Clone Script]({{< relref path="/docs/guidelines/setup/clone" >}} "Richtlijn:
  Clone Script")
* [Beheren van Dictionary Samenvoegen]({{< relref path="/docs/guidelines/coding/merge" >}})