---
categories: [Begrip]
tags: [ansible, concept]
title: Ansible Automatiseringsplatform
linkTitle: Ansible
weight: 5
description: >
  Allesomvattende open source oplossing voor IT-automatisering
---

Ansible Automatiseringsplatform is een complete oplossing voor IT-automatisering,
orkestratie en governance, waardoor efficiënte beheer en schaalvergroting van
infrastructuur mogelijk wordt gemaakt.
