---
categories: ["Begrip"]
tags: [ansible, galaxy, ansible-galaxy, collection, role]
title: "Ansible Galaxy"
linkTitle: "Galaxy"
weight: 2
description: >
  Galaxy is een centrale plek voor het ontdekken en delen van Ansible-inhoud.
---

De {{< external-link url="https://galaxy.ansible.com" text="Ansible Galaxy" htmlproofer_ignore="true" >}} website wordt gebruikt voor het verspreiden van
[Ansible Collections]({{< relref path="./projects/collections" >}}) en
[Rollen]({{< relref path="./projects/roles.md" >}}), die de fundamentele
bouwstenen zijn van een Ansible-automatiseringsproject. Hiermee kun je eenvoudig
vooraf gebouwde Collections en Rollen vinden die zijn gemaakt door de
Ansible-gemeenschap. Deze herbruikbare componenten bieden een handige manier om
bestaande automatiseringsoplossingen te benutten en de ontwikkeling van je eigen
project te versnellen.

Het Galaxy-platform stelt je in staat om te zoeken naar inhoud op basis van
verschillende criteria zoals populariteit, relevantie en kwaliteit. Je kunt een
breed scala aan Collections en Rollen verkennen die zijn bijgedragen door
gemeenschapsleden, waardoor het gemakkelijker wordt om te profiteren van hun
expertise en tijd te besparen tijdens de ontwikkeling.

Om inhoud van Ansible Galaxy te gebruiken, kun je deze handmatig downloaden of
rechtstreeks integreren in je Ansible-playbooks met behulp van de
`ansible-galaxy` CLI. Deze tool vereenvoudigt de installatie en het beheer van
Collections en Rollen, zodat je ze naadloos kunt integreren in je
automatiseringsworkflows.

Door gebruik te maken van de kracht van Ansible Galaxy kun je profiteren van een
uitgebreid ecosysteem van door de gemeenschap gedreven inhoud, samenwerken met
andere gebruikers en voortbouwen op bestaande oplossingen om efficiënte en
schaalbare automatisering te bereiken.

<!-- TODO link / explain ansible configuration, playbook project -->
<!-- TODO requirements.yml -->

* [C2 Platform Collections]({{< ref path="/categories/ansible-collection" >}})
* {{< external-link url="https://galaxy.ansible.com/ui/search/?keywords=c2platform" text="C2 Platform Collections op de Galaxy-website" htmlproofer_ignore="true" >}}