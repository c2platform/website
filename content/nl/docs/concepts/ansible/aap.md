---
categories: ["Begrip"]
tags: [ansible,platform,awx,aap,redhat]
title: "Ansible Automatiseringsplatform ( AAP )"
linkTitle: "Automatiseringsplatform"
weight: 1
description: >
  AAP is een krachtig en open-source automatiseringsplatform dat bestaat uit twee belangrijke
  componenten: de Automation Controller (AWX) en de Ansible Automation Hub
  (Galaxy NG).
---

{{< under_construction >}}

<!-- TODO afmaken -->

* Handleiding sectie [Ansible Automatiseringsplatform ( AAP ) en AWX]({{< relref path="/docs/howto/awx" >}})
* {{< external-link url="https://www.redhat.com/en/technologies/management/ansible" text="Red Hat Ansible Automatiseringsplatform" htmlproofer_ignore="false" >}}
* {{< external-link url="https://www.ansible.com/products/awx-project/faq" text="Het AWX Project" htmlproofer_ignore="false" >}}
