import csv
import gitlab
import os
import yaml
import requests
import re
from urllib.parse import urljoin
from jinja2 import Template
import shutil

MARKDOWN_PATH = "~/git/gitlab/c2/website/content/nl/docs/gitlab"

script_dir = os.path.dirname(os.path.abspath(__file__))


def download_readme(group, project):
    url = f"https://gitlab.com/{project.path_with_namespace}/-/raw/master/README.md"
    tmp_download = os.path.join(
        "/tmp", project.path_with_namespace.replace("/", "-") + "README"
    )
    try:
        response = requests.get(url)
        response.raise_for_status()  # Raise an exception for non-2xx status codes
        with open(tmp_download, "wb") as file:
            file.write(response.content)
        return response.content.decode("utf-8")
    except requests.exceptions.HTTPError as e:
        if response.status_code == 404:
            print(f"Warning: README.md not found {url}")
        else:
            print(f"Error downloading {url}: {e}")
        return ""
    except Exception as e:
        print(f"{indentation}An error occurred: {e}")
        return ""


def generate_markdown(doc, file, csv_writer, template="project.md.j2"):
    template_file = os.path.join(script_dir, template)
    template = Template(open(template_file).read())
    output = template.render({"doc": doc})
    updated_output = update_markdown_links(output, doc, csv_writer)
    with open(file, "w") as f:
        f.write(updated_output)


def link_text_replace_tilde(link_text):
    if "`" in link_text:
        link_text = link_text.replace("`", "")
    return link_text


def update_markdown_links(markdown_content, doc, csv_writer):
    pattern_en = r"\[([a-zA-Z0-9\s\-_]*?)\]\(https://c2platform\.org/en/(.*?)\)"
    pattern_nl = r"\[([a-zA-Z0-9\s\-_]*?)\]\(https://c2platform\.org/nl/(.*?)\)"
    # pattern_plantuml = r'```plantuml\s*@startuml\s+([a-zA-Z0-9\s\-_]+)\s+(.*?)\s+@enduml\s*```'
    pattern_urls = r"(?:\s|^)(https?://\S+?)(?:\s|\.\s|$)"
    # pattern_plantuml = re.compile(pattern_plantuml, re.DOTALL)
    # updated_content = re.sub(pattern_plantuml, r'{{< plantuml id="\1" >}}\n\2\n{{< /plantuml >}}', markdown_content)
    updated_content = markdown_content
    pattern = r"\[([^\]]+)\]\(([^\)]+)\)"
    links = re.findall(pattern, updated_content)
    for link_text, link_url in links:
        link = f"[{link_text}]({link_url})"
        if "badges" in link_url:
            continue
        if "img.shields.io/badge" in link_url:
            continue
        if link_url.startswith("#"):
            continue
        if "https://c2platform.org/en/" in link_url:
            link_url_new = link_url.replace("https://c2platform.org/en", "")
            link_url_new = '{{< relref path="' + link_url_new.rstrip("/") + '" >}}'
            link_new = f"[{link_text}]({link_url_new})"
        elif "https://c2platform.org/nl/" in link_url:
            link_url_new = link_url.replace("https://c2platform.org/nl", "")
            link_url_new = (
                '{{< relref path="' + link_url_new.rstrip("/") + '" lang="nl" >}}'
            )
            link_new = f"[{link_text}]({link_url_new})"
        elif link_url.startswith("https://"):
            link_text = link_text_replace_tilde(link_text)
            link_new = (
                '{{< external-link url="'
                + link_url
                + '" text="'
                + link_text
                + '" htmlproofer_ignore="false" >}}'
            )
        else:
            base_url = doc["project"]["web_url"]
            # new_url = os.path.normpath(os.path.join(base_url, '-/blob/master' ,link_url))  # not suitable, creates https:/ instead of https://
            new_url = "{}/{}/{}".format(base_url, "-/blob/master", link_url)
            new_url = new_url.replace("/./", "/")  # urljoin does not sanitize this
            link_text = link_text_replace_tilde(link_text)
            link_new = (
                '{{< external-link url="'
                + new_url
                + '" text="'
                + link_text
                + '" htmlproofer_ignore="false" >}}'
            )
        updated_content = updated_content.replace(link, link_new)
        csv_writer.writerow([doc["path"], doc["title"], link, link_new])
    links = re.finditer(pattern_urls, updated_content)
    for match in links:
        link_match = match[0]
        link_url = match[1]
        if link_url in dev_urls:
            htmlproofer = "true"
        else:
            htmlproofer = "false"
        if (
            link_url
            == "https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml"
        ):
            continue
        else:
            link_url_new = (
                '{{< external-link url="'
                + link_url
                + '" htmlproofer_ignore="'
                + htmlproofer
                + '" >}}'
            )
            link_match_new = link_match.replace(link_url, link_url_new)
            updated_content = updated_content.replace(link_match, link_match_new)
            csv_writer.writerow([doc["path"], doc["title"], link_url, link_url_new])
    return updated_content


def galaxy_yaml(project):
    galaxy_url = (
        f"https://gitlab.com/{project.path_with_namespace}/-/raw/master/galaxy.yml"
    )
    response = requests.head(galaxy_url)
    if response.status_code == 200:
        response = requests.get(galaxy_url)
        return yaml.safe_load(response.text)
    return None


def project_to_markdown_table(data):
    markdown = "| Key | Value |\n| --- | --- |\n"
    for key, value in data.items():
        markdown += f"| {key} | {value} |\n"
    return markdown


def write_csv_row(csv_writer, file, title, find, replace):
    csv_writer.writerow([file, title, find, replace])


gitlab_api_token = os.getenv("GITLAB_API_TOKEN")
if gitlab_api_token is None:
    raise ValueError("GITLAB_API_TOKEN is not defined")
else:
    print(f"GITLAB_API_TOKEN: {gitlab_api_token}")
gl = gitlab.Gitlab("https://gitlab.com", private_token=gitlab_api_token)


def get_project_title(group, project, readme):
    pattern = r"^# (.*)$"
    match = re.search(pattern, readme, re.MULTILINE)
    if match:
        return match.group(1)
    else:
        warning = f"WARNING: {project.path_with_namespace} README.md does not contain first level heading"
        print(warning)
        return project.name


def get_projects(group, project=None, weight=10, csv_writer=None):
    if project:
        indentation = group.full_path.count("/")
        markdown_path = os.path.expanduser(
            os.path.join(MARKDOWN_PATH, project.path_with_namespace)
        )
        markdown_file = os.path.join(markdown_path, "_index.md")
        os.makedirs(markdown_path, exist_ok=True)
        readme = download_readme(group, project)
        title = get_project_title(group, project, readme)
        readme_no_heading = readme.replace("# " + title, "")
        doc = {
            "categories": "GitLab Projects",
            "readme": readme,
            "readme_no_heading": readme_no_heading,
            "title": title,
            "linkTitle": project.name,
            "path": project.path_with_namespace,
            "weight": weight + 10,
            "description": project.description,
            "icon": "fa-brands fa-square-gitlab",
            "layout": "project",
            "tags": sorted(project.topics),
            "project": {
                "id": project.id,
                "description": project.description,
                "name": project.name,
                "path": project.path,
                "path_with_namespace": project.path_with_namespace,
                "created_at": project.created_at,
                "default_branch": project.default_branch,
                "ssh_url_to_repo": project.ssh_url_to_repo,
                "http_url_to_repo": project.http_url_to_repo,
                "web_url": project.web_url,
                "readme_url": project.readme_url,
                "forks_count": project.forks_count,
                "avatar_url": project.avatar_url,
            },
        }

        gyml = galaxy_yaml(project)
        if gyml:
            doc["galaxy"] = gyml
            doc["tags"].extend(gyml["tags"])
            doc["tags"].append("ansible")
            doc["tags"].append("collection")
            doc["categories"] = ["GitLab Projects", "Ansible Collection"]

        doc["tags"] = sorted(set(doc["tags"]))
        if project.path_with_namespace in ignore_projects:
            if os.path.exists(markdown_path):
                shutil.rmtree(markdown_path)
                print(
                    f"The project directory {markdown_path} has been deleted and will be ignored."
                )
        else:
            generate_markdown(doc, markdown_file, csv_writer)
    else:
        projects = group.projects.list(get_all=True, archived=False)
        projects = sorted(projects, key=lambda x: x.name)
        for project_index, project in enumerate(projects):
            if project.marked_for_deletion_on is None:
                get_projects(group, project, project_index, csv_writer)


def get_group(group, weight=1, csv_writer=None):
    markdown_path = os.path.expanduser(os.path.join(MARKDOWN_PATH, group.full_path))
    markdown_file = os.path.join(markdown_path, "_index.md")
    subgroups = None
    os.makedirs(markdown_path, exist_ok=True)
    doc = {}
    doc["categories"] = "GitLab Groups"
    doc["title"] = group.name
    doc["linkTitle"] = group.name
    doc["path"] = group.full_path
    doc["description"] = group.description
    doc["weight"] = weight
    doc["group"] = {}
    doc["group"]["id"] = group.id
    doc["group"]["web_url"] = group.web_url
    doc["group"]["name"] = group.name
    doc["group"]["path"] = group.path
    doc["group"]["description"] = group.description
    doc["group"]["visibility"] = group.visibility
    doc["group"]["share_with_group_lock"] = group.share_with_group_lock
    doc["group"][
        "require_two_factor_authentication"
    ] = group.require_two_factor_authentication
    doc["group"]["two_factor_grace_period"] = group.two_factor_grace_period
    doc["group"]["project_creation_level"] = group.project_creation_level
    doc["group"]["auto_devops_enabled"] = group.auto_devops_enabled
    doc["group"]["subgroup_creation_level"] = group.subgroup_creation_level
    doc["group"]["emails_disabled"] = group.emails_disabled
    doc["group"]["mentions_disabled"] = group.mentions_disabled
    doc["group"]["lfs_enabled"] = group.lfs_enabled
    doc["group"]["default_branch_protection"] = group.default_branch_protection
    doc["group"]["avatar_url"] = group.avatar_url
    doc["group"]["request_access_enabled"] = group.request_access_enabled
    doc["group"]["full_name"] = group.full_name
    doc["group"]["full_path"] = group.full_path
    doc["group"]["created_at"] = group.created_at
    doc["group"]["parent_id"] = group.parent_id
    if "marked_for_deletion_on" in doc["group"]:
        doc["group"]["marked_for_deletion_on"] = group.marked_for_deletion_on
    doc["group"]["wiki_access_level"] = group.wiki_access_level

    generate_markdown(doc, markdown_file, csv_writer, "group.md.j2")

    if hasattr(group, "subgroups"):
        subgroups = group.subgroups.list(get_all=True)
        subgroups = sorted(subgroups, key=lambda x: x.name)
        for group_index, subgroup in enumerate(subgroups):
            group2 = gl.groups.get(subgroup.full_path)
            if not subgroup.full_path in ignore_groups:
                get_group(group2, group_index + 1, csv_writer)
    get_projects(group, None, 10, csv_writer)


gitlab_api_token = os.getenv("GITLAB_API_TOKEN")
if gitlab_api_token is None:
    raise ValueError("GITLAB_API_TOKEN is not defined")
else:
    print(f"GITLAB_API_TOKEN: {gitlab_api_token}")
gl = gitlab.Gitlab("https://gitlab.com", private_token=gitlab_api_token)

namespaces = ["c2platform"]
ignore_groups = []
ignore_projects = [
    "c2platform/rws/ansible-collection-chocolatey",
    "c2platform/pol/ansible",
]  # TODO enable POL
with open("scripts/dev_urls.yml", "r") as file:
    yaml_data = yaml.safe_load(file)
dev_urls = yaml_data[
    "urls"
]  # Assumes 'yaml_data' is defined after reading 'dev_urls.yml'

with open("replacements.csv", "w", newline="") as csv_file:
    csv_writer = csv.writer(csv_file)
    csv_writer.writerow(["File", "Title", "Find", "Replace"])

    for ns_index, ns in enumerate(namespaces):
        namespace = gl.namespaces.get(ns)
        group = gl.groups.get(ns)
        get_group(group, csv_writer=csv_writer)
