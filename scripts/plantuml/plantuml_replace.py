import os
import re
import argparse

# Configuration
content_dir = "content"  # Base directory for Markdown files
images_base_dir = "/images/plantuml"  # Absolute base path for images

# Regular expression to find PlantUML blocks
plantuml_block_regex = re.compile(
    r"^\s*(```plantuml\s*\n(?:.*?\n)?\s*```)$", re.MULTILINE | re.DOTALL
)


def get_diagram_name(plantuml_block):
    lines = plantuml_block.split("\n")
    if len(lines) < 3 or not lines[1].startswith("@startuml "):
        return None  # No valid diagram name line found
    # Extract the diagram name from the line following @startuml
    diagram_name = lines[1].split(" ", 1)[1].strip()
    if not diagram_name:
        return None  # No name specified after '@startuml'
    return diagram_name


def replace_plantuml_with_image(md_path):
    with open(md_path, "r") as file:
        content = file.read()

    def replacement(match):
        plantuml_block = match.group(1)
        diagram_name = get_diagram_name(plantuml_block)

        if not diagram_name:
            # No diagram name found, skip this file
            return plantuml_block  # Return the original block to leave it unchanged

        # Construct the image filename using the Markdown file's path and the diagram name
        rel_path = os.path.relpath(md_path, content_dir)
        base_path = os.path.splitext(rel_path)[0]  # Remove file extension
        image_filename = f"{base_path}/{diagram_name}.png"

        # Construct the absolute path for the image
        absolute_image_path = os.path.join(images_base_dir, image_filename)

        # TODO
        # Use the absolute image path in the replacement string
        # return f'{{{{< image filename="{absolute_image_path}" >}}}}'
        return f'{{{{< figure src="{absolute_image_path}" >}}}}'
        # return f'{{{{< image filename="{absolute_image_path}" >}}}}'

    # Replace PlantUML code blocks with the specific shortcode format
    new_content, num_replacements = plantuml_block_regex.subn(replacement, content)

    if num_replacements > 0 and new_content != content:
        # Write the modified content back to the file only if changes were made
        with open(md_path, "w") as file:
            file.write(new_content)
        print(f"Replaced {num_replacements} PlantUML code blocks in: {md_path}")
    elif num_replacements > 0:
        print(f"Skipped file due to missing diagram names: {md_path}")


def process_directory():
    for root, dirs, files in os.walk(content_dir):
        for file in files:
            if file.endswith(".md"):
                md_path = os.path.join(root, file)
                replace_plantuml_with_image(md_path)


# Argument parsing
parser = argparse.ArgumentParser(
    description="Process Markdown files to replace PlantUML code with a specific shortcode, using absolute paths for images."
)
args = parser.parse_args()

# Recursively process all Markdown files in the content directory
process_directory()
