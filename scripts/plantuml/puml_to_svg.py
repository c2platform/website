import hashlib
import os
import subprocess
import requests
import argparse

# Configuration
plantuml_jar_url = "https://github.com/plantuml/plantuml/releases/download/v1.2024.0/plantuml-1.2024.0.jar"
plantuml_jar_path = "/tmp/plantuml-1.2024.0.jar"  # PlantUML jar file will be saved here
search_directory = "static/images/plantuml"  # Default directory to search for .puml files

# Download PlantUML jar if it doesn't exist
if not os.path.exists(plantuml_jar_path):
    response = requests.get(plantuml_jar_url)
    with open(plantuml_jar_path, "wb") as f:
        f.write(response.content)
    print(f"Downloaded PlantUML jar to {plantuml_jar_path}")

# Function to calculate SHA256 hash of a file
def calculate_sha256(file_path):
    sha256_hash = hashlib.sha256()
    with open(file_path, "rb") as f:
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)
    return sha256_hash.hexdigest()

# Function to read the hash from the corresponding .sha256 file
def read_hash(file_path):
    hash_file_path = f"{file_path}.sha256"
    if os.path.exists(hash_file_path):
        with open(hash_file_path, "r") as f:
            return f.read().strip()
    return None

# Function to write the hash to the corresponding .sha256 file
def write_hash(file_path, hash_value):
    with open(f"{file_path}.sha256", "w") as f:
        f.write(hash_value)

# Function to process a single .puml file
def process_puml_file(full_path):
    hash_value = calculate_sha256(full_path)
    old_hash = read_hash(full_path)

    # Check if the file has changed
    if old_hash != hash_value:
        # File has changed, generate SVG and PNG
        subprocess.run(["java", "-jar", plantuml_jar_path, "-tsvg", full_path])
        subprocess.run(["java", "-jar", plantuml_jar_path, "-tpng", full_path])

        # Update hash
        write_hash(full_path, hash_value)
        print(f"Exported {full_path} to svg and png")

# Argument parsing
parser = argparse.ArgumentParser(description='Process PlantUML files to generate SVG and PNG.')
parser.add_argument('puml_file', nargs='?', help='Path to a specific PlantUML file to process')
args = parser.parse_args()

if args.puml_file:
    # Process single PlantUML file
    process_puml_file(args.puml_file)
else:
    # Search for .puml files and process them
    for root, dirs, files in os.walk(search_directory):
        for file in files:
            if file.endswith(".puml"):
                full_path = os.path.join(root, file)
                process_puml_file(full_path)

# print("Processing complete.")
