import os
import re
import argparse

# Configuration
content_dir = "content"  # Base directory for Markdown files
output_base_dir = "static/images/plantuml"  # Base directory for output .puml files

# Regular expression to match PlantUML blocks
plantuml_block_regex = re.compile(
    r"^\s*```plantuml\s*\n(.*?)\n\s*```\s*$", re.MULTILINE | re.DOTALL
)

# Regular expression to match @startuml followed by a name
startuml_name_regex = re.compile(r"^\s*@startuml\s+(\S+)$")


def extract_and_save_plantuml(md_path, rel_path):
    with open(md_path, "r") as file:
        content = file.read()

    blocks = plantuml_block_regex.findall(content)
    for block in blocks:
        # Check the first line of the block for @startuml and a name
        first_line = block.split("\n", 1)[0]
        startuml_match = startuml_name_regex.match(first_line)

        if startuml_match:
            diagram_name = startuml_match.group(1)
            output_dir = os.path.join(
                output_base_dir,
                os.path.dirname(rel_path),
                f"{os.path.splitext(os.path.basename(rel_path))[0]}",
            )
            # output_file = os.path.join(output_dir, f"{os.path.splitext(os.path.basename(rel_path))[0]}-{diagram_name}.puml")
            output_file = os.path.join(output_dir, f"{diagram_name}.puml")

            # Create the output directory if it doesn't exist
            os.makedirs(output_dir, exist_ok=True)

            # Write the PlantUML content to the .puml file
            with open(output_file, "w") as out_file:
                out_file.write(block.strip())

            print(f"Extracted PlantUML to: {output_file}")
        else:
            print(
                f"Warning: '@startuml' without a name in file {md_path}. The block will be skipped."
            )


def process_single_file(md_path):
    if md_path.startswith(content_dir):
        rel_path = os.path.relpath(md_path, content_dir)
    else:
        rel_path = os.path.basename(md_path)
    extract_and_save_plantuml(md_path, rel_path)


def process_directory():
    for root, dirs, files in os.walk(content_dir):
        for file in files:
            if file.endswith(".md"):
                md_path = os.path.join(root, file)
                rel_path = os.path.relpath(md_path, content_dir)
                extract_and_save_plantuml(md_path, rel_path)


# Argument parsing
parser = argparse.ArgumentParser(
    description="Process Markdown files to extract PlantUML code."
)
parser.add_argument(
    "markdown_file", nargs="?", help="Path to a specific Markdown file to process"
)
args = parser.parse_args()

if args.markdown_file:
    # Process single Markdown file
    process_single_file(args.markdown_file)
else:
    # Recursively process all Markdown files in the content directory
    process_directory()

# print("Processing complete.")
