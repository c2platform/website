import sys
import os
import hashlib
from pathlib import Path
import re
import subprocess
import requests

# Configuration
plantuml_jar_url = "https://github.com/plantuml/plantuml/releases/download/v1.2024.0/plantuml-1.2024.0.jar"
plantuml_jar_path = "/tmp/plantuml-1.2024.0.jar"  # PlantUML jar file will be saved here

# Configuration for the output base directory for .puml files
output_base_dir = "static/images/plantuml"


images_base_dir = "/images/plantuml"

# Regular expression to find PlantUML blocks
plantuml_block_regex = re.compile(
    r"^\s*```plantuml\s*\n(.*?)\s*\n\s*```$", re.MULTILINE | re.DOTALL
)

# Regular expression to find PlantUML blocks for replacement
plantuml_block_regex_replace = re.compile(
    r"^\s*(```plantuml\s*\n(?:.*?\n)?\s*```)$", re.MULTILINE | re.DOTALL
)

# Regular expression to match @startuml followed by a name
startuml_name_regex = re.compile(r"^\s*@startuml\s+(\S+)$")


def up_to_date(file_path, hash_file_path):
    if not os.path.exists(hash_file_path) or not os.path.exists(file_path):
        return False
    with open(hash_file_path, "r") as file:
        existing_hash = file.read().strip()
    return existing_hash == compute_sha256(file_path)


def write_hash(file_path, hash_file_path):
    hash_val = compute_sha256(file_path)
    with open(hash_file_path, "w") as file:
        file.write(hash_val)


def download_plantuml_jar():
    if not os.path.exists(plantuml_jar_path):
        response = requests.get(plantuml_jar_url)
        with open(plantuml_jar_path, "wb") as f:
            f.write(response.content)
        print(f"Downloaded PlantUML jar to {plantuml_jar_path}")


def get_diagram_name(plantuml_block):
    lines = plantuml_block.split("\n")
    if len(lines) < 3 or not lines[1].startswith("@startuml "):
        return None  # No valid diagram name line found
    diagram_name = lines[1].split(" ", 1)[1].strip()
    return diagram_name if diagram_name else None  # Ensure a non-empty name


def generate_diagram(file_path):
    # Ensure PlantUML jar is downloaded
    download_plantuml_jar()

    hash_file_path = f"{file_path}.sha256"
    if not up_to_date(file_path, hash_file_path):
        # Generate SVG and PNG
        subprocess.run(["java", "-jar", plantuml_jar_path, "-tsvg", file_path])
        subprocess.run(["java", "-jar", plantuml_jar_path, "-tpng", file_path])

        # Update hash
        write_hash(file_path, hash_file_path)
        print(f"Exported {file_path} to svg and png")


def extract_and_generate_from_md(md_path):
    with open(md_path, "r") as file:
        content = file.read()

    blocks = plantuml_block_regex.findall(content)
    for block in blocks:
        # Check the first line of the block for @startuml and a name
        first_line = block.split("\n", 1)[0]
        startuml_match = startuml_name_regex.match(first_line)

        if startuml_match:
            diagram_name = startuml_match.group(1)
            rel_path = os.path.relpath(md_path, os.path.dirname(md_path))
            output_dir = os.path.join(output_base_dir, os.path.dirname(rel_path))
            output_file = os.path.join(output_dir, f"{diagram_name}.puml")

            # Create the output directory if it doesn't exist
            os.makedirs(output_dir, exist_ok=True)

            # Write the PlantUML content to the .puml file
            with open(output_file, "w") as out_file:
                out_file.write(block.strip())

            print(f"Extracted PlantUML to: {output_file}")

            # After extracting and saving the PlantUML, generate the diagrams
            generate_diagram(output_file)
        else:
            print(
                f"Warning: '@startuml' without a name in file {md_path}. The block will be skipped."
            )


def replace_in_md(md_path):
    with open(md_path, "r") as file:
        content = file.read()

    def replacement(match):
        plantuml_block = match.group(1)
        diagram_name = get_diagram_name(plantuml_block)

        if not diagram_name:
            return (
                plantuml_block  # Return the original block if no diagram name is found
            )

        # Construct the image filename using the diagram name
        image_filename = f"{diagram_name}.png"

        # Construct the relative path for the image link
        relative_image_path = os.path.join(images_base_dir, image_filename)

        # Return the markdown image link
        return f"![{diagram_name}]({relative_image_path})"

    # Replace PlantUML blocks with markdown image links
    new_content, num_replacements = plantuml_block_regex_replace.subn(
        replacement, content
    )

    if num_replacements > 0 and new_content != content:
        # Write the modified content back to the file if changes were made
        with open(md_path, "w") as file:
            file.write(new_content)
        print(
            f"Replaced {num_replacements} PlantUML blocks with image links in: {md_path}"
        )


def process_files(operation, file_paths):
    for file_path in file_paths:
        if operation == "md" or operation == "all":
            extract_and_generate_from_md(file_path)
        elif operation == "puml":
            generate_diagram(file_path)
        elif operation == "replace":
            replace_in_md(file_path)


def main():
    args = sys.argv[1:]
    if not args:
        print("Usage: script.py <operation> or <file_path>")
        sys.exit(1)

    arg = args[0]
    if arg in ["md", "puml", "replace", "all"]:
        operation = arg
        if operation == "md" or operation == "all":
            md_files = list(Path(".").glob("**/*.md"))
            process_files(operation, md_files)
        if operation == "puml" or operation == "all":
            puml_files = list(Path(".").glob("**/*.puml"))
            process_files(operation, puml_files)
        if operation == "replace" or operation == "all":
            md_files = list(Path(".").glob("**/*.md"))
            process_files(operation, md_files)
    else:
        file_path = arg
        if not os.path.exists(file_path):
            print(f"Error: File {file_path} does not exist.")
            sys.exit(1)
        if file_path.endswith(".puml"):
            generate_diagram(file_path)
        elif file_path.endswith(".md"):
            extract_and_generate_from_md(file_path)
            replace_in_md(file_path)
        else:
            print(f"Unsupported file type for {file_path}.")
            sys.exit(1)


if __name__ == "__main__":
    main()
