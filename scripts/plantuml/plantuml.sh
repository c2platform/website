#!/bin/bash

DIRECTORY_TO_WATCH="content"
DIRECTORY_TO_WATCH_2="static/images/plantuml"
PYTHON_SCRIPT="scripts/plantuml/plantuml_from_md.py"
PYTHON_SCRIPT_2="scripts/plantuml/puml_to_svg.py"

inotifywait -m -e close_write -e moved_to --format '%w%f' -r $DIRECTORY_TO_WATCH $DIRECTORY_TO_WATCH_2 | while read FILE
do
    if [[ "$FILE" == *.md ]]; then  # Check if the changed file is a Markdown file
        # echo "Detected change in $FILE, executing Python script..."
        python3 $PYTHON_SCRIPT "$FILE"
    fi
    if [[ "$FILE" == *.puml ]]; then  # Check if the changed file is a PlantUML file
        # echo "Detected change in $FILE, executing Python script..."
        python3 $PYTHON_SCRIPT_2 "$FILE"
    fi
done

