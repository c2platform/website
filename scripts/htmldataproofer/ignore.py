from bs4 import BeautifulSoup
import os
import re

# Define the directory to start the search from
start_dir = 'public/'

# Classes of <a> elements to target
target_classes = ['td-offset-anchor', 'td-page-meta--view', 'td-page-meta--edit', 'td-page-meta--child', 'td-page-meta--issue']

# Specific URL patterns to target, using regular expressions
specific_url_patterns = [
    r'https://gitlab\.com/c2platform/website/-/issues/service_desk$',
    r'https://c2platform\.org$',
    r'https://next\.c2platform\.org$',
    r'https://gitlab\.com/c2platform/?$',
    r'https://gohugo\.io/templates/404/$',
    r'https://gitlab\.com/c2platform/website/commit/.*'  # Pattern to match all URLs starting with the specified path
]

# Compile the regular expressions for the specific URL patterns
compiled_url_patterns = [re.compile(pattern) for pattern in specific_url_patterns]

# Log file for replacement details
log_filename = 'replacement_details.log'

# Initialize the count for added tags
total_added_tags = 0

with open(log_filename, 'w') as log_file:
    print("Adding HTMLProofer 'data-proofer-ignore' tag to HTML files")

    # Walk through the directory tree
    for dirpath, dirnames, filenames in os.walk(start_dir):
        for filename in filenames:
            # Check if the file is an HTML file
            if filename.endswith('.html'):
                filepath = os.path.join(dirpath, filename)

                # Read the file content
                with open(filepath, 'r', encoding='utf-8') as file:
                    soup = BeautifulSoup(file, 'html.parser')

                # Flag to track if changes were made
                changes_made = False
                file_added_tags = 0

                # Iterate over each target class
                for target_class in target_classes:
                    # Find all <a> elements with the specific class
                    for a_tag in soup.find_all('a', class_=target_class):
                        # Add 'data-proofer-ignore' attribute
                        if 'data-proofer-ignore' not in a_tag.attrs:
                            a_tag['data-proofer-ignore'] = ''
                            changes_made = True
                            file_added_tags += 1

                # Iterate over specific URL patterns
                for pattern in compiled_url_patterns:
                    # Find all <a> elements that match the specific URL pattern
                    for a_tag in soup.find_all('a', href=True):
                        if pattern.match(a_tag['href']) and 'data-proofer-ignore' not in a_tag.attrs:
                            a_tag['data-proofer-ignore'] = ''
                            changes_made = True
                            file_added_tags += 1

                # If changes were made, write back to the file and log the changes
                if changes_made:
                    with open(filepath, 'w', encoding='utf-8') as file:
                        file.write(str(soup))
                    log_file.write(f"File: {filepath} - Added 'data-proofer-ignore' to {file_added_tags} elements.\n")
                    total_added_tags += file_added_tags

    log_file.write(f"Total 'data-proofer-ignore' tags added: {total_added_tags}\n")
print(f"Replacement completed. Added {total_added_tags} 'data-proofer-ignore' tags. Details are in: {log_filename}")
