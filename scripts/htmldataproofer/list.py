from bs4 import BeautifulSoup
import os

# Define the directory to start the search from
start_dir = 'public'

# Prefix to match in the links for external URLs
link_prefix = 'https://'

# Log file for the external links details
external_links_log = 'external_links_details.log'

# Initialize the count for external links
total_external_links = 0

with open(external_links_log, 'w') as log_file:
    print("List of external links validated by HTMLProofer")

    # Walk through the directory tree
    for dirpath, dirnames, filenames in os.walk(start_dir):
        for filename in filenames:
            # Check if the file is an HTML file
            if filename.endswith('.html'):
                filepath = os.path.join(dirpath, filename)

                # Read the file content
                with open(filepath, 'r', encoding='utf-8') as file:
                    soup = BeautifulSoup(file, 'html.parser')

                # Find all <a> elements without 'data-proofer-ignore' that start with 'https://'
                for a_tag in soup.find_all('a', href=True):
                    href = a_tag['href']
                    if href.startswith(link_prefix) and 'data-proofer-ignore' not in a_tag.attrs:
                        log_file.write(f"File: {filepath}, Link: {href}\n")
                        total_external_links += 1

print(f"Processing completed. Total external links found: {total_external_links}. Details in {external_links_log}")
