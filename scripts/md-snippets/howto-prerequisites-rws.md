## Prerequisites

Create the reverse and forward proxy `gsd-rproxy1`.

```bash
vagrant up gsd-rproxy1
```

For more information about the various roles that `gsd-rproxy1` performs in this project:

* [Setup Reverse Proxy and CA server]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [Setup SOCKS proxy]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Managing Server Certificates as a Certificate Authority]({{< relref path="/docs/howto/c2/certs">}})
* [ArcGIS Server software and licenses]({{< relref path="/docs/howto/rws/dev-environment/setup/software" >}}).
