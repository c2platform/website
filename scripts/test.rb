require 'nokogiri'
require 'html-proofer'
require 'open-uri'

class GitLab < HTMLProofer::Check
    def gitlab_link?
        if @link.href
            @link.href.start_with?("https://gitlab.com/c2platform/") # https://gitlab.com/c2platform/
        else
            false
        end
    end

    def run
      #puts @html
      #exit # if @html.include? 'jira'

      @html.css("a").each do |node|
        #puts node# if node.include? 'service_desk'
        #xit
        @link = create_element(node)
        if @link.href and @link.href.include? 'jira' and @link.href != '/en/tags/jira/' # if @link.url.include? 'jira'
            puts @link.href
            if gitlab_link?
                #response = check_http_status(@link.href)
                #puts response # barf
                check_for_did_not_exist_on(@link.href)
            end
        end
        next if @link.ignore?
        # puts "not ignored"
        if gitlab_link?
            # puts "Yup"
            # puts "GitLab: " + @link.href
    #       response = check_http_status(@link.href)
    #       if response && response.code != 404
    #         check_for_did_not_exist_on(@link.href)
    #       end
        end
       end
    end

    def check_for_did_not_exist_on(url)
      nokogiri = Nokogiri::HTML(URI.open(url))
      #nokogiri = Nokogiri::HTML(doc)
      puts nokogiri
      nokogiri.css('.gl-alert-content').each do |alert|
        content = alert.css('.gl-alert-body').text
        if content.include?("did not exist on")
          add_failure("Link to #{url} has a 'did not exist' error: #{content}", element: @link)
        end
      end
    end
end

# options = {
#     ignore_missing_alt: true,
#     ignore_empty_alt: true,
#     allow_missing_href: true,
#     enforce_https: false,
#     checks: ["Links", "Images", "Scripts"]
# }
options = {
    ignore_missing_alt: true,
    ignore_empty_alt: true,
    allow_missing_href: true,
    enforce_https: false,
    checks: ['GitLab']
    # checks: ['Links', 'Images', 'Scripts']
}
#HTMLProofer.check_directory("./public", options).run
# HTMLProofer.check_directories("./public/en/docs/gitlab", { checks: ["GitLabLinkCheck"] })
# HTMLProofer.check_directories(["out/"], { checks: ["MailToOctocat"] })
#HTMLProofer.check_directory("./public")
#HTMLProofer.check_directories("./public/en/docs/gitlab", { checks: ["GitLabLinkCheck"] })
HTMLProofer.check_directory("./public", options).run


# HTMLProofer.check_directories(["/home/ostraaten/git/gitlab/c2/website/public"], { checks: ["GitLabLinkCheck"] })
# HTMLProofer.check_directory("./out").run
# HTMLProofer.check_directories(["out/"], { checks: ["MailToOctocat"] })

# htmlproofer --allow-missing-href --ignore-empty-alt --ignore-missing-alt --no-enforce-https --ignore-urls https://vsmarketplacebadge.apphb.com/*  2>&1 | tee ../htmlproofer.log
# if grep -q "following failures were found" ../htmlproofer.log; then
#     echo "Failures found. Exiting."
#     exit 1
# fi
