module github.com/google/docsy-example

go 1.12

require (
	github.com/google/docsy v0.8.0 // indirect
	github.com/google/docsy/dependencies v0.7.2 // indirect
	github.com/hugomods/images v0.8.3 // indirect
	github.com/hugomods/search v0.11.1 // indirect
)
