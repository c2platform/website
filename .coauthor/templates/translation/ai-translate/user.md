# Dutch version of Hugo page {{{{ task['path-modify-event'] }}}}

Translate page content of one of the pages of my Hugo website from
English to Dutch.

{{{{% set path_nl = task["path-modify-event"] | replace('content/en','content/nl') %}}}}
{{{{% if path_nl | file_exists %}}}}

CURRENT DUTCH VERSION STORED IN {{{{ path_nl }}}}

I already have a version of the Dutch text. So please review it carefully,
compare it to the original English text, so you can make necessary changes to
ensure the contents of the Dutch translation correspond to the English source
document.

Current Dutch version of the text:
```markdown
{{{{ path_nl | include_file_content }}}}
```

This Dutch version might have outstanding changes that, together with
outstanding changes in the English source document, might help you understand
what I am working on.

The outstanding change my Dutch version of the document are:

```text
{{{{ path_nl | get_git_diff }}}}
```
{{{{% endif %}}}}

CURRENT ENGLISH VERSION STORED IN {{{{ task["path-modify-event"] }}}}

Current English version of the text:

```markdown
{{{{ task["path-modify-event"] | include_file_content }}}}
```

This English version might also have outstanding changes that, together with
outstanding changes in the Dutch document, might help you better understand what
I am working on.

The outstanding change my English version of the document are:

```text
{{{{ task["path-modify-event"] | get_git_diff }}}}
```
