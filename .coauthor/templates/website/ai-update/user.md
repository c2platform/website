# Coauthor Hugo Markdown File {{{{ task['path-modify-event'] }}}}

Current English version of the text:

```markdown
{{{{ task['path-modify-event'] | include_file_content }}}}
```

This English version might also have outstanding changes that, might help you
better understand what I am working on. If there are outstanding changes, you
should also focus on those changes.

The outstanding change of my English version of the document are:

```text
{{{{ task['path-modify-event'] | get_git_diff }}}}
```
